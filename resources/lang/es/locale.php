<?php

return [
    'wrong_credentials' => 'Nombre de usuario o contraseña equivocada, por favor intente otra vez !',
    'user_not_exist' => 'El usuario no existe',
    'no_school_account' => 'No tienes una cuenta de la escuela',
    'no_permission' => 'No tienes permiso para realizar esta operación',
    'subscription_not_exist' => 'No existe la suscripción',
    'distributor_code_invalid' => 'El código de verificación no es válido',
    'subscription_renew_success' => 'Suscripción renovada con éxito',
    'plan_not_exist' => 'Plan no encontrado',
    'updated_plan_not_exist' => 'Última versión del plan no encontrada',
    'subscription_upgrade_success' => 'La suscripción se actualizó correctamente',
    'no_downgrade' => 'No puede rebajar su plan',
    'distributor_not_exist' => 'El distribuidor no existe',
    'distributor_change_success' => 'El distribuidor cambió correctamente',

    "teacher_school_mismatch" => 'Este profesor no está en la misma escuela',
    "teacher_no_school" => "Este profesor no tiene una escuela adjunta a él / ella",
    "teacher_delete_successfull" => 'El profesor se ha eliminado correctamente',
    "teachers_delete_successfull" => 'Los maestros han sido eliminados correctamente',
    "teacher_wrong_role" => "Este profesor no tiene el rol de maestro",

    "student_doesn't_exist" => 'No hay ningún estudiante con este id',
    "student_school_mismatch" => 'Este estudiante no está en la misma escuela',
    "student_no_school" => "Este estudiante no tiene una escuela adjunta a él / ella",
    "student_delete_successfull" => 'El estudiante ha sido eliminado correctamente',
    "students_delete_successfull" => 'Los estudiantes han sido eliminados correctamente',
    "student_wrong_role" => "Este estudiante no tiene el papel de Estudiante",

    "school_update_successfull" => "La información de la escuela ha sido actualizada con éxito",
    "child_added" => "El niño agregó correctamente",
    "user_email_exists" => "El usuario con este correo electrónico ya existe",
    "username_exists" => "Ya existe un usuario con el mismo nombre",
    "parent_not_exist" => "El padre no existe",
    "parent_account_not_family" => "Esta cuenta principal no es del tipo family",
    "supervisor_email_not_child" => "Supervisor de correo electrónico no está establecido para el niño",
    "classroom_not_exist" => "El aula elegida no existe",
    "email_exists" => "El Email ya existe",
    "trial_period_end" => "Su período de prueba ha terminado y su cuenta ha sido suspendida",
    "customer_deleted" => 'Cliente eliminado con éxito',
    "hello_world" => "HOLA MUNDO",
    "customer_not_exist" => "El cliente no existe",
    "account_not_found" => "Cuenta no encontrada",
    "child_not_exist" => "El niño no existe",
    "account_name_not_found" => "Cuenta con este nombre no encontrado",
    "plan_missing" => "Falta el plan",
    "user_missing" => "Falta el usuario",
    "user_account_not_found" => "Cuenta de usuario no encontrada",
    "plan_not_found" => "Plan no encontrado",
    "version_plan_not_found" => "Última versión del plan no encontrada",
    "subscribed_plan" => "Ya está suscrito a un plan",
    "plan_period_missing" => 'Falta el período del plan',
    'plan_period_not_exist' => 'El período del plan no existe',
    'plan_period_wrong' => 'El período del plan es incorrecto',
    "not_add_journey" => "No puedes agregar más viajes",
    "journey_not_exist" => "El viaje no existe",
    "user_enrolled_journey" => "El usuario ya está inscrito en este viaje",
    "password_not_match" => "La contraseña antigua no coincide",
    "subscription_cancel_failed" => "Error al cancelar la suscripción",
    "not_downgrade_plan" => "No puede rebajar su plan",
    "subscription_upgrading_failed" => "Error al actualizar la suscripción",
    "subscription_upgraded" => "Suscripción actualizada con éxito",
    "account_type_not_found" => "Tipo de cuenta no encontrado",
    "subscription_not_invitation" => "La suscripción actual no es la suscripción a la invitación",
    "subscription_period_ended" => "Su período de suscripción ha finalizado. Suscríbase a otro plan",
    "notification_not_exist" => "Notificación no existe",
    "notification_confirmed" => "Notificación confirmada",
    "notification_delayed" => "Notificación retrasada",
    "quiz_not_exist" => "El cuestionario no existe",
    "position_updated" => "Posición actualizada con éxito",
    "role_not_found" => "Función no encontrada",
    "payment_info_updated" => "Información de pago actualizada correctamente",
    "account_type_not_supported" => "El tipo de cuenta aún no es compatible",
    "subscription_ended" => "Su período de suscripción ha terminado",
    "not_add_child" => "No puede agregar un hijo mientras se cancela la suscripción a su cuenta. Suscríbase de nuevo.",
    "parent_of_child" => "Usted ya es el padre de este niño",
    "not_family_account" => "La cuenta no es una cuenta familiar",
    "have_credit_token" => "Debes tener una tarjeta de crédito",
    "ccount_not_individual" => "La cuenta de usuario no es individual",
    "item_plan_not_found" => "Artículo Plan no encontrado",
    "account_type_changed" => "Tipo de cuenta cambiado correctamente",
    "not_have_customer_id" => "El usuario no tiene un ID de cliente",
    "parent_email_not_found" => "Correo electrónico de los padres no encontrado",
    "request_send" => "Solicitud enviada correctamente",
    "contact_email_sent" => "Contáctenos email enviado",
    "support_email_sent" => "Correo electrónico de soporte enviado",
    "no_user_with_email" => "No hay usuario con este correo electrónico",
    "reset_password_email_sent" => "Restablecer contraseña Correo electrónico enviado",
    "password_reset" => "Redefinição de senha com sucesso",
    "no_account_customer" => "No hay cuenta para este cliente",
    "no_user_id" => "No hay ningún usuario con este ID",
    "new_country_same" => "El nuevo país es el mismo que el viejo país",
    "no_subscriptions_account" => "No hay suscripciones para esta cuenta",
    "subscription_robopal_active" => "Tu suscripción a RoboPal sigue activa",
    "subscription_robopal_end" => "Su suscripción a RoboPal terminará en",
    "subscription_robopal_ended" => "Su suscripción a RoboPal ha terminado",
    "subscription_not_include_robopal" => "Su suscripción no incluye el plan RoboPal",
    "you_have_item" => "Usted ya tiene este objeto",
    "not_add_extra_item_plans" => "No puede agregar un elemento adicional a este tipo de planes. Suscríbase a otro plan",
    "extra_item_added" => "Elemento adicional agregado correctamente",
    "subscription_not_include_plan" => "Su suscripción no incluye este plan",
    "already_unsubscribed_plan" => "Ya te has dado de baja de este plan",
    "extra_removed" => "Elemento extra eliminado correctamente",


    "customer_not_found" => "Cliente no encontrado",
    "customer_deletion_failed" => "Error al eliminar el cliente",
    "wrong_username_password" => "¡Nombre de usuario o contraseña equivocada, por favor intente otra vez !",
    "account_not_activated" => "Su cuenta aún no está activada, por favor revise su correo electrónico",
    "account_have_suspended" => "Su cuenta ha sido suspendida, por favor revise a sus padres para confirmarle",
    "account_is_suspended" => "Su cuenta está suspendida, por favor revise a sus padres para confirmarle",
    "account_is_activated" => "Su cuenta aún no está activada, por favor, consulte a su padre",
    "could_not_create_token" => "No se ha podido crear un token",
    "account_type_unrecognized" => "Tipo de cuenta no reconocido",
    "token_expired" => "Token Expired",
    "invalid_token" => "Simbolo no valido",
    "token_used" => "Token ya se han utilizado",
    "user_has_no_email" => "Este usuario no tiene correo electrónico",
    "add_child" => "Añadir hijo",
    "upgrade_account_add_child" => "Actualizar a la cuenta de la familia y agregar",
    "register_robogarden_add_child" => "Regístrese en RoboGarden y agregue su hijo",

    "classroom_doesn't_exist" => "No hay aula con este id",
    "classroom_doesn't_exist_school" => "Este aula no esta en esta escuela",
    "course_with_journey_exist" => "Ya hay un curso con este viaje en esta clase",
    "end_date_start_date_min_time" => "La fecha de finalización debe ser mayor que la hora de inicio",
    "journey_not_in_classroom" => "Este viaje no está en este aula",
    "hi" => "Hola",
    "welcome_to_robogarden" => "Bienvenido a RoboGarden!!",
    "email_confirmation_subject" => "RoboGarden: confirmación de correo electrónico",
    "email_confirmation_body_1" => "Este mensaje es para verificar que su correo electrónico se use para registrarse en ",
    "email_confirmation_body_2" => "Por favor, haga clic en el botón de abajo para confirmar su correo electrónico",
    "confirm_email" => "Confirmar correo electrónico",
    "once_you_do" => "Una vez que lo haga, su cuenta se activará.",
    "contact_us" => "Contáctenos",
    "phone" => "Teléfono",
    "email" => "Email",
    "renewal_fail_subject" => "RoboGarden: Error en la renovación automática",
    "renewal_fail_body_1" => "Error en la transacción de pago automático para la renovación de la suscripción. Actualice la información de su tarjeta de crédito.",
    "this_is_trial" => "Esto es un juicio",
    "renewal_fail_body_2" => "De 4 ensayos durante la semana que viene.",
    "renewal_fail_body_3" => "Tenga en cuenta que se anulará la suscripción automáticamente en el cuarto intento fallido.",
    "no_reply" => "No responda a este correo electrónico automatizado.",
    "reset_password_subject" => "Equipo de RoboGarden: Password Reset",
    "reset_password_body_1" => "Recientemente ha solicitado restablecer su contraseña para su cuenta de Robogarden. Haga clic en el botón de abajo para continuar con el proceso de restablecimiento de contraseña.",
    "reset_your_password" => "Restablecer su contraseña",
    "subscribe_add_child" => "RoboGarden: Suscríbase y Añada su hijo",
    "a_child" => "Un niño",
    "requested_to_register" => "Ha solicitado registrarse en RoboGarden. Para obtener más información acerca de Robogarden, visite robogarden.ca",
    "start_subscription_process" => "En caso de que decida suscribirse a robogarden.ca y agregue al niño a su cuenta familiar, puede hacer clic en el botón de abajo para iniciar el proceso de suscripción.",
    "parent" => "Padre",
    "add_new_child_subject" => "RoboGarden: Añadir un nuevo niño",
    "requested_to_be_added" => " ha solicitado que se agregue a su cuenta",
    "upgrade_individual_to_family" => "Para confirmar esta solicitud y actualizar su cuenta individual a una cuenta familiar, puede hacer clic en el botón de abajo",
    "add_to_family_account" => "Para confirmar esta solicitud y añadirla a su cuenta familiar, puede hacer clic en el botón de abajo.",
    "unsubscription_email_subject" => "RoboGarden: Correo electrónico de la cancelación de la suscripción",
    "successfully_unsubscribed" => "Has cancelado la suscripción de ",
    "subscription_active_till" => "Su Suscripción permanecerá activa hasta ",
    "account_will_suspend" => "A continuación, su cuenta se convertirá en un plan gratuito",
    "new_subscription_subject" => "Equipo de RoboGarden: Nueva Suscripción",
    "successfully_subscribed" => "Se ha suscrito correctamente a ",
    "have_trial_days" => "Usted tendrá un período de prueba de 14 días. Puede darse de baja dentro de este periodo sin ser cargado.",
    "after_trial" => "Después del período de prueba, se realizará una facturación automática en su cuenta con la cantidad de la suscripción de un mes y la suscripción se renovará automáticamente cada mes.",
    "for_more_information" => "Para obtener más información sobre RoboGarden y cómo usarlo, visite RoboGarden",
    "here" => "aquí",
    "automatic_charging" => "Se aplicará un cargo automático de una suscripción de un mes a su cuenta y la suscripción se renovará automáticamente cada mes",
    "contact_us_confirm_subject" => "Equipo de RoboGarden: Contáctenos Confirmación",
    "thanks_for_contact" => "Gracias por ponerse en contacto con RoboGarden !",
    "contact_us_confirm_body_1" => "Este correo electrónico es una confirmación de que recibimos sus comentarios y un especialista de atención al cliente le responderá en breve",
    "contact_us_confirm_body_2" => "No responda a este correo electrónico automatizado. Una respuesta adecuada le será enviada en breve por el equipo de RoboGarden",
    "support_email_subject" => "RoboGarden: confirmación de correo electrónico de asistencia",
    "thanks_for_contact_support" => "Gracias por ponerse en contacto con el equipo de soporte de RoboGarden.",
    "support_email_body_1" => "Este correo electrónico es una confirmación de que hemos recibido sus comentarios y un especialista de atención al cliente le responderá en breve.",
    "course_doesn't_exist" => "No hay curso con este id",
    "course_doesn't_exist_school" => "Este curso no esta en esta escuela",
    "course_started" => "Este curso ya ha comenzado",
    "course_ended" => "Este curso ha terminado",
    "adventure_doesn't_exist" => "No hay aventura con este id",
    "adventure_doesn't_match_course_journey" => "Esta aventura no está en el viaje del curso",
    "student_not_in_group" => "El estudiante no está en el grupo",
    "teacher_not_student_group" => "El profesor no está en el grupo de clase del estudiante",
    "teacher_not_mainteacher" => "Este profesor no es un maestro principal en esta clase",
    "journey_adventure_deadlines_size_mismatch" => "Los plazos de todas las aventuras deben fijarse",
    "not_member_in_course" => "Este usuario no es miembro en el aula",
    "course_not_exist" => "El curso no existe",
    "adventure_not_exist" => "La aventura no existe",
    "mission_not_exist" => "La misión no existe",
    "course_not_started" => "El curso no ha comenzado todavía",
    "user_not_have_course" => "El usuario no tiene este curso",
    "journey_not_published" => "Este viaje aún no ha sido publicado",
    "question_not_exist" => "La pregunta no existe",
    "choice_not_exist" => "La opción no existe",
    "pre_adventure_not_exist" => "La aventura anterior no existe",
    "grade_doesn't_exist" => "No hay grado con este id",
    "grade_no_in_school" => "Esta calificación no está en esta escuela",
    "no_score_in_classroom" => "Nadie ha participado aún en este aula",
    "classroom_no_students" => "No hay estudiantes en esta clase",
    "teacher_not_in_classroom" => "Este Maestro no está en esta clase",
    "student_doesn't_take_course" => "Este estudiante no toma este curso",
    "student_click_mission_stored_successfully" => "La misión de clic de estudiante se guardó correctamente",
    "students_progressed_in_classroom" => "Los estudiantes de este salón han progresado en uno o más de los cursos presenciales",
    "student_can't_unenroll_progress" => "Uno o más de los estudiantes no inscriptos ha progresado en este salón de clases",
    "students_classroom_force" => "Hay estudiantes en esta clase. ¿Estás seguro de que quieres cambiar el grado del aula ?",
    "students_progressed_in_course" => "Hay estudiantes que ya han progresado en este curso",
    "classroom_name_exist_in_school" => "Ya hay un aula con este nombre",
    "course_exist_name_in_classroom" => "Ya hay un curso con este nombre en el aula seleccionada",
    "not_have_subscription" => "El usuario no tiene esta suscripción",
    "no_robogarden_emails" => "No puedes elegir uno de los correos electrónicos de Robogarden",
    "student_has_progress" => "Um ou mais alunos excluídos já progrediram em um curso",
    "main_teacher_delete" => "No puede borrar el (los) maestro (s) principal (es). Por favor, anótelas primero",
    "no_language" => "El lenguaje no existe",
    "user_language_update" => "Idioma actualizado con éxito",
    "account_not_activated_verfication_code" => "Tu cuenta de la escuela aún no está activada, por favor ingresa el código de verificación",
    "account_not_activated_check_email" => "Su cuenta aún no está activada, por favor revise su correo electrónico",
    "account_suspended_parent_confirm" => "Su cuenta ha sido suspendida, por favor revise a sus padres para confirmarle",
    "not_school_account" => "No tienes una cuenta de la escuela",
    "no_school_attached" => "Esta cuenta no tiene una escuela adjunta a ella",
    "no_permission_operation" => "No tienes permiso para realizar esta operación",
    "grade_not_in_school" => "Esta calificación no está en la escuela",
    "grade_name_not_in_school" => "No hay grado con este nombre para esta escuela",
    "user_teacher_not_provided" => "El usuario proporcionado no es un profesor",
    "can_not_update_another_teacher_data" => "No puede actualizar datos de otro profesor",
    "student_not_exist" => "El estudiante no existe",
    "classroom_deleted" => "Aula suprimida con éxito",
    "classroom_not_same_school_user" => "this classroom is not in the same school as the user",
    "no_role_teacher" => "Este aula no está en la misma escuela que el usuario",
    "no_role_main_teacher" => "No hay ningún papel con el nombre main_teacher",
    "teacher_not_exist" => "El maestro no existe",
    "classroom_doesn't_contain_teacher" => "Este aula no contiene este profesor",
    "teacher_already_main_teacher" => "El maestro ya es un maestro principal",
    "classroom_must_have_one_main_teacher" => "El aula debe tener por lo menos un maestro principal",
    "subscription_perios_ended" => "Su período de suscripción ha finalizado. Por favor renovar su suscripción",
    "school_plan_information_missing" => "Falta la información de su plan escolar",
    "reached_max_classroom_journey" => "Ha alcanzado el número máximo de viajes en el aula permitido",
    "journey_id" => "El viaje con id ",
    "doesn't_exist" => " No existe",
    "journey_already_in_classroom" => "El viaje ya está en esta clase",
    "teacher_name" => "Profesor con nombre ",
    "not_in_classroom" => " no está en el aula",
    "cant_unassign_main_teacher" => "no se puede desasignar a un profesor del aula cuando él / ella tiene maestro principal de rol",
    "no_role_with_this_name" => "no hay ningún rol con este nombre",
    "no_default_group_member_with_specs" => "no hay ningún miembro del grupo por defecto del aula con esta especificación",
    "student_name" => "estudiante con nombre ",
    "students_unassigned_successfully" => "Estudiantes sin asignar correctamente",
    "enrollment_done" => "inscripción realizada",
    "assignment_done" => "asignación realizada",
    "status_not_exist" => "El estado no existe",
    "country_not_exist" => "País no existe",
    "payment_info_required" => "Información de pago es obligatorio",
    "school_account_created" => "El distribuidor lo contactará pronto para procesar su pago.",
    "classroom_id" => "Aula con id ",
    "user_in_classroom_with_id" => "El usuario ya está en el aula con id ",
    "student_max_reached" => "Has alcanzado el número máximo de estudiantes permitido",
    "student_must_have_grade" => "El estudiante debe tener un grado",
    "classroom_grade_doesn't_match_student_grade" => "El grado del aula no coincide con el grado del estudiante",
    "file_empty" => "el archivo está vacío",
    "first_row_need_params_student_csv" => "el archivo debe contener nombre, apellido, nombre de usuario, contraseña como primera fila",
    "csv_data_exceed_student_count" => "este archivo CSV contiene más estudiantes que su número permitido de estudiantes",
    "account_already_activiated" => "Su cuenta ya está activada",
    "invitation_not_exist" => "La invitación no existe",
    "invitation_reached_max_users" => "La invitación ha alcanzado su límite máximo de usuarios",
    "user_not_student" => "El usuario proporcionado no es un estudiante",
    "cant_update_another_student_data" => "No se pueden actualizar datos de otros estudiantes",
    "student_is_assigned_classroom_unassign_first" => "Este estudiante es asignado a la clase, no asignarlo primero",
    "next_adventure_not_exist" => "La siguiente aventura no existe",
    "next_task_not_exist" => "La siguiente tarea no existe",
    "previous_task_doesn't_exists" => "Orden de tareas anterior no existe",
    "unsubscription" => "cancelación de suscripción",
    "account_expire_at" => "tu cuenta expirará en ",
    "parent_confirmation_requested" => "Confirmación del padre solicitada",
    "remind_parent_add_account" => "por favor recuerdale a tus padres que te agreguen a su cuenta para poder acceder a los viajes ten en cuenta que tu cuenta se suspenderá en ",
    "parent_not_confirm" => " si tu padre no te confirmó",

    "account_type_not_exist" => "El tipo de cuenta no existe",
    "invitation_with_code_exists" => "La invitación con este código ya existe",
    "invitation_expired" => "La invitación ha caducado",
    "teacher_changed" => "El maestro cambió con éxito",
    "invitation_end" => "Final de invitación",
    "invited_account_expire_at" => "Su cuenta invitada caducará en",
    "invitation_must_individual" => "La invitación debe ser del tipo=> Individual",
    "invitation_must_school" => "La invitación debe ser del tipo=> Escuela",
    "supervisor_must_registered" => "Un supervisor debe registrarse primero",
    "user_created" => "Usuario creado correctamente",
    "no_account_customer_id" => "No hay cuenta con este ID de cliente",
    "no_subscription_with_id" => "No hay suscripción con este ID",
    "charge_amount_zero" => "El importe de la carga era 0",
    "no_user_customer" => "No hay usuario para este cliente",
    "webhook_handled" => "Webhook manejado con éxito",
    "email_field_missing" => "Falta el campo de correo electrónico",
    "school_subscription_ended_contact_teacher" => "El período de suscripción de la Escuela ha terminado. Póngase en contacto con su profesor",
    "school_subscription_ended_contact_school" => "El período de suscripción de la Escuela ha terminado. Póngase en contacto con la administración de su escuela",
    "school_subscription_ended_contact_distributor" => "El período de suscripción de la Escuela ha terminado. Póngase en contacto con su distribuidor para renovar su suscripción",
    "distributor_not_found" => "Distribuidor no encontrado",
    "not_have_access_journey" => "No tienes acceso a este viaje",
    "students_not_use_service" => "Los estudiantes no pueden usar este servicio",
    "no_journey_with_id" => "No hay viaje con este id",
    "code_same_name_exists" => "Ya existe un código guardado con el mismo nombre",
    "mission_saving_not_exist" => "El ahorro de misión no existe",
    "mission_not_mcq_mission" => "La misión no es mcq misión",
    "html_mission_not_exist" => "Misión Html no existe",
    "not_add_teacher_classroom" => "No está autorizada para agregar maestro al aula",
    "teacher_with_id" => "El maestro con id",
    "already_in_classroom" => "Ya está en el aula",
    "not_authorized_add_student_classroom" => "No está autorizado para agregar a un alumno al aula",
    "number_students_exceeds_max" => "Este número de estudiantes excede el máximo permitido permitido máximo",
    "student_with_id" => "El estudiante con id",
    "student_grade_match_classroom" => "El grado del estudiante debe coincidir con el grado del salón de clases",
    "student_assigned_one_classroom" => "El estudiante sólo puede ser asignado a un aula",
    "no_role_same_student" => "No hay ningún papel con el nombre estudiante",
    "teachers_added" => "Maestros añadidos con éxito",
    "students_added" => "Los estudiantes agregaron con éxito",
    "journeys_added" => "Viajes agregados con éxito",
    "journeys_unassigned" => "Viajes sin asignar correctamente",
    "classrooms_deleted" => "Aulas eliminadas con éxito",
    "teachers_unassigned" => "Maestros no asignados con éxito",
    "user_not_permission_create_teacher" => "Este usuario no tiene permiso para crear un profesor",
    "account_activated" => "Cuenta activada correctamente",
    "course_deleted" => "Curso eliminado correctamente",
    "main_teacher_with_name" => "No se puede borrar el profesor con nombre",
    "students_progressed" => "Porque es maestro principal en un aula donde los estudiantes han progresado",

    "account_type_must_individual_family" => "El tipo de cuenta debe ser Individual o Familiar",
    "success" => "Éxito",
    "failed" => "Ha fallado",
    "failure" => "Fracaso",
    "school_creation_failed" => "La escuela falló",
    "admin_creation_failed" => "Error en la creación de administradores",
    "account_creation_failed" => "Error en la creación de la cuenta",
    "user_not_found" => "Usuario no encontrado",
    "code_invalid" => "El código no es válido",
    "done" => "Hecho",
    "xml_input_format" => "La entrada XML debe estar en formato XML",
    "token_incorrect" => "El símbolo es incorrecto",
    "upgrade_max_students" => "Su cuenta tiene más estudiantes que los estudiantes permitidos para este plan. Seleccione otro plan",
    "upgrade_max_courses" => "Su cuenta tiene más cursos que los cursos permitidos para este plan. Seleccione otro plan",
    'bug_reported' => 'Error ha sido reportado correctamente',
    "model_answer_already_unlocked" => "Respuesta modelo ya desbloqueada",
    "model_answer_unlocked" => "Respuesta modelo desbloqueada con éxito",
    "not_enough_coins" => "No hay suficientes monedas",
    "subscribe_to_add" => "Suscríbase primero a un plan pago para agregar más hijos",
    "email_not_exist" => "El correo electrónico no existe",
    "no_or_bundle_required" => "Se requiere número de estudiante o número de aula o paquete",

    "there_is_already_an_active_subscription " => " no puede suscribirse cuando ya hay una suscripción activa ",
    "bundle_not_exist" => "este paquete no existe",
    "bundle_no_student_must_equal_or_exceed" => "debe elegir un paquete con una cantidad igual o mayor que la cantidad de estudiantes que tiene",
    "bundle_no_classroom_journey_must_equal_or_exceed" => "debe elegir un paquete con una cantidad igual o mayor al número de viajes de clase que tenga",
    "student_main_component_not_found" => "componente principal del alumno no encontrado",
    "classroom_journey_main_component_not_found" => "componente principal del recorrido del aula no encontrado",
    "cant_change_from_disributor_to_stripe" => "no puede cambiar del pago del distribuidor al pago en banda",
    "cant_upgrade_no_stripe" => "no se puede actualizar cuando no hay carga escolar",
    "plan_is_same" => "no puede actualizar al plan en el que ya está conectado",
    "differance_in_days_less_than_equal_zero" => "differance en días no puede ser menor o igual a cero",
    "account_type_must_family" => "tipo de cuenta debe ser familia",
    "no_school_prices" => "no se encontraron precios de escuela",
    "main_price_component_0" => "los componentes del precio principal no pueden ser cero",
    "school_upgrade_successfull" => "escuela actualizada con éxito",
    "school_account_created_stripe" => "la escuela se suscribió con éxito",
    "something_wrong" => "Algo salió mal",
    "cant_use_sub_code_when_active_sub" => "no puede usar un código de suscripción cuando ya tiene una suscripción activa",
    "no_upgrades_found" => "No se encontraron actualizaciones",
    "upgrade_activated" => "Mejorado con éxito",
    "cant_change_from_to_stripe_disributor" => "No puede cambiar del pago por franjas al pago del socio local",
    "email_updated" => "Correo electrónico actualizado con éxito",
    "subscribe_for_child" => "Tómese un segundo para suscribirse para que su hijo pueda jugar",
    "requested_to_play" => "ha solicitado jugar un viaje",
    "child_want_subscribe" => "Tu hijo quiere suscribirse",
    "requested_to_play_activity" => "ha solicitado acceso para la actividad ",
    "reached_max_children" => "Ha alcanzado el número máximo de hijos que debe tener, suscríbase a un plan para agregar más hijos",
    "add_more_children" => "Debe agregar una cantidad de niños mayor o igual a la que tiene",
    "late" => "Tarde",
    "current" => "Corriente",
    "upcoming" => "Próximo",
    "with" => "con",
    "student" => "Estudiante",
    "classroom_journey" => "Aula / Jornadas",
    "upgraded_to" => "Ha actualizado exitosamente a",
    "contact_distributor" => "Comuníquese con su socio local para gestionar el proceso de pago.",
    "subscription_not_renewed" => "Tenga en cuenta que su suscripción no se renovará automáticamente todos los años",
    "invitation" => "Invitación",
    "invitation_expired_register" => "Su período de invitación ha terminado. Debes crear una nueva cuenta y agregar a tus hijos.",
    "school_invitation_expired_register" => "Su período de invitación ha terminado. Necesitas crear una nueva cuenta.",
    "individual_invitation_expired_register" => "Su período de invitación ha terminado. Por favor, suscríbete.",
    "cant_upgrade_to_same_plan" => "No puede actualizar al mismo plan que usted.",
    "currently_upgraded" => "Actualmente, tienes",
    "no_children_required" => "Número de niños es requerido",
    "grade" => "Grado",
    "classrooms_no_error" => "El número de las aulas no es válido",
    "plan_in_static" => "El nombre de este plan está reservado a RoboGarden",
    "county_not_found" => "País no encontrado",
    "match_not_found" => "Match not found",
    "user_data_updated" => "Datos de usuario actualizados con éxito",
    "no_matches" => "Sin coincidencias para predecir",
    "no_predict" => "No puedes predecir en el tiempo de los partidos",
    "predict_successfully" => "Predicción del usuario guardada con éxito",
    "parameter_not_found" => "Parámetro no encontrado",
    "adventure" => "Adventure",
    "journey" => "Journey",
    "mission" => "Mission",
    "not_a_mission" => "Not a mission",
    "plan_in_static" => "El nombre de este plan está reservado a RoboGarden",
    "camp_not_exist" => "El campamento no existe",
    "not_active_camp" => "Este campamento no está activo",
    "has_ended_camp" => "Este campamento ha terminado",
    "camp_cannot_delete" => "Este campamento está activo y no puede ser eliminado.",
    "camp_deleted" => "Campamento eliminado con éxito",
    "no_camp_subscription" => "No hay suscripción al campamento activo",
    "camp_name_exist_in_school" => "Ya hay un campamento con este nombre",
    "activity_id" => "La actividad con id ",
    "activity_already_in_camp" => "La actividad ya está en este campamento",
    "already_in_camp" => "já está no acampamento",
    "user_in_camp_with_id" => "El usuario ya está en el campamento con id ",
    "active_camp" => "No se puede agregar estudiantes en el campamento activo",
    "ended_camp" => "No se puede agregar estudiantes en el campamento final",
    "no_operation_in_active_camp" => "No se puede realizar ninguna operación en el campo activo",
    "no_operation_in_ended_camp" => "No se puede realizar ninguna operación en el campamento final",
    "student_updated_successfully" => "Estudiantes actualizados con éxito",
    "teacher_updated_successfully" => "Maestros actualizados con éxito",
    "camp_subscription_subject" => "Equipo RoboGarden: New Camp",
    "added_new_camp" => "Agregaste un nuevo campamento con ",
    "activity" => "Actividad",
    "will_start" => "Comenzará el ",
    "to" => "a ",
    "distributor_contact_
    you" => "Nuestro socio local se comunicará con usted lo antes posible para obtener más información",
    "activity_not_exist" => "La actividad no existe",
    "account_type_must_be_individual_or_homeschooler" => "tipo de cuenta debe ser individual o homeschooler",
    "camp_doesnot_exist_school" => "Este campamento no está en esta escuela",
    "teacher_not_in_camp" => "El maestro no está asignado en este campamento",
    "activity_not_in_camp" => "Esta actividad no está en este campamento",
    "student_not_in_camp" => "El estudiante no está asignado a este campamento",
    "activity_price_not_exist" => "el precio de la actividad no existe",
    "unlockable_doesn't_exist" => "desbloqueable no existe",
    "already_have_access_to_activity" => "ya tienes acceso a la actividad",
    "camps_deleted" => "Campamentos eliminados con éxito",
    "student_has_progress_camp" => "Uno o más de los estudiantes eliminados ya han progresado en un campamento",
    "max_children_to_add_is_five" => "la cantidad máxima de niños para la cuenta de homeschooler es de cinco",
    "not_have_access_activity" => "No tienes acceso a esta actividad",
    "camp_activated" => "Campamento activado con éxito",
    "journeys" => "Viajes",
    "have_access_to_activity" => "Ya tienes acceso a esta actividad.",
    "no_enough_xp" => "No hay suficiente Xp",
    "activity_unlocked" => "Actividad desbloqueada con éxito",

];