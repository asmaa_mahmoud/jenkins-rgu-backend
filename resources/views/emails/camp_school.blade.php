@extends('includes.mail_format')

@section('content')
    <h1>@lang('locale.hi') {{$username}},</h1>
    <br/>
    <h3>@lang('locale.welcome_to_robogarden')</h3>
    <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;">
        @lang('locale.added_new_camp') ({{$students}} @lang('locale.student'), {{$activities}} @lang('locale.activity')), @lang('locale.will_start') {{$startDate}} @lang('locale.to') {{$endDate}}.
    </p>
    <br>
    <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;">
        @lang('locale.distributor_contact_you')
    </p>
    <br/>
@stop
