@extends('includes.mail_format')

@section('content')
    <h1>Hi {{$username}},</h1>
    <br/>
    <h3>New Camp !!</h3>
    <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;">{{$admin_name}}
        ({{$admin_email}}) has created a new camp with name ({{$campName}})
        for {{$school_name}} school with ({{$students}} Student, {{$activities}} Activity) from {{$startDate}} to {{$endDate}}.
    </p>
    <br>

    <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;">
        Please contact {{$admin_name}} to handle payment operation and send him
        this code {{$code}} to activate the camp.
    </p>
    <br/>
    <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;">
        <strong>Please Note: </strong> All previous codes have been terminated for this camp and this code is the only active one.
    </p>
    <br/>
@stop
