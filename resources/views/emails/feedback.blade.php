<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>*|MC:SUBJECT|*</title>
    <style type="text/css">
        #outlook a{padding:0;}
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
        img{-ms-interpolation-mode:bicubic;}
        body{margin:0; padding:0;}
        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
        table{border-collapse:collapse !important;}
        body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}



        #bodyCell{padding:20px;}
        #templateContainer{width:600px;}

        body, #bodyTable{
            background-color:#DEE0E2;
        }


        #bodyCell{
            border-top:4px solid #BBBBBB;
        }

        #templateContainer{
            border:1px solid #BBBBBB;
        }


        h1{
            color:#202020 !important;
            display:block;
            font-family:Helvetica;
            font-size:26px;
            font-style:normal;
            font-weight:bold;
            line-height:100%;
            letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }

        .button {
            background-color: #339933; /* Red */
            border: none;
            color: white;
            padding: 10px 24px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 4px;
        }


        h2{
            color:#404040 !important;
            display:block;
            font-family:Helvetica;
            font-size:20px;
            font-style:normal;
            font-weight:bold;
            line-height:100%;
            letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }


        h3{
            color:#606060 !important;
            display:block;
            font-family:Helvetica;
            font-size:16px;
            font-style:italic;
            font-weight:normal;
            line-height:100%;
            letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }


        h4{
            color:#808080 !important;
            display:block;
            font-family:Helvetica;
            font-size:14px;
            font-style:italic;
            font-weight:normal;
            line-height:100%;
            letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }


        #templatePreheader{
            background-color:#F4F4F4;
            border-bottom:1px solid #CCCCCC;
        }


        .preheaderContent{
            color:#808080;
            font-family:Helvetica;
            font-size:10px;
            line-height:125%;
            text-align:left;
        }


        .preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{
            color:#606060;
            font-weight:normal;
            text-decoration:underline;
        }


        #templateHeader{
            background-color:#F4F4F4;
            border-top:1px solid #FFFFFF;
            border-bottom:1px solid #CCCCCC;
        }


        .headerContent{
            color:#505050;
            font-family:Helvetica;
            font-size:20px;
            font-weight:bold;
            line-height:100%;
            padding-top:0;
            padding-right:0;
            padding-bottom:0;
            padding-left:0;
            text-align:left;
            vertical-align:middle;
        }


        .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
            color:#EB4102;
            font-weight:normal;
            text-decoration:underline;
        }

        #headerImage{
            height:auto;
            max-width:600px;
        }

        #templateBody{
            background-color:#F4F4F4;
            border-top:1px solid #FFFFFF;
            border-bottom:1px solid #CCCCCC;
        }


        .bodyContent{
            color:#505050;
            font-family:Helvetica;
            font-size:14px;
            line-height:150%;
            padding-top:20px;
            padding-right:20px;
            padding-bottom:20px;
            padding-left:20px;
            text-align:left;
        }


        .bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
            color:#EB4102;
            font-weight:normal;
            text-decoration:underline;
        }

        .bodyContent img{
            display:inline;
            height:auto;
            max-width:560px;
        }

        #templateFooter{
            background-color:#F4F4F4;
            border-top:1px solid #FFFFFF;
        }


        .footerContent{
            color:#808080;
            font-family:Helvetica;
            font-size:10px;
            line-height:150%;
            padding-top:20px;
            padding-right:20px;
            padding-bottom:20px;
            padding-left:20px;
            text-align:left;
        }

        .footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{
            color:#606060;
            font-weight:normal;
            text-decoration:underline;
        }



        @media only screen and (max-width: 480px){

            body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;}
            body{width:100% !important; min-width:100% !important;}
            #bodyCell{padding:10px !important;}

            #templateContainer{
                max-width:600px !important;
                width:100% !important;
            }

            h1{
                font-size:24px !important;
                line-height:100% !important;
            }

            h2{
                font-size:20px !important;
                line-height:100% !important;
            }

            h3{
                font-size:18px !important;
                line-height:100% !important;
            }

            h4{
                font-size:16px !important;
                line-height:100% !important;
            }

            /* ======== Header Styles ======== */

            #templatePreheader{display:none !important;}
            #headerImage{
                height:auto !important;
                max-width:600px !important;
                width:100% !important;
            }

            .headerContent{
                font-size:20px !important;
                line-height:125% !important;
            }


            .bodyContent{
                font-size:18px !important;
                line-height:125% !important;
            }

            .footerContent{
                font-size:14px !important;
                line-height:115% !important;
            }

            .footerContent a{display:block !important;}
        }
        #logofooter{
            max-width: 150px;
            height: 60px;
            max-height: 60px;
            width: 150px!important;
            float: right;
            margin-top: 20px;
        }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN HEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                <tr>
                                    <td valign="top" class="headerContent">
                                        <img src="https://s3-us-west-2.amazonaws.com/robogarden/email_template/header600px.png" style="max-width:600px;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                    </td>
                                </tr>
                            </table>
                            <!-- // END HEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN BODY // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600px;" id="templateBody">
                                <tr>
                                    <td valign="top" class="bodyContent" mc:edit="body_content">
                                        <h1>Dear RoboGarden Support,</h1>
                                        <br/>
                                        <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;"> <strong>From: </strong> {{ $name ? $name : 'RoboGarden User' }}</p>
                                        <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;"> <strong>Feeling: </strong> {{ $feeling ? $feeling : 'No Thing' }}</p>
                                        <p style="color: #000000;font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;line-height: 150%;text-align: left;"> <strong>Message: </strong> {{ $msg }}</p>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END BODY -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN FOOTER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                <tr>
                                    <td valign="top" class="footerContent" mc:edit="footer_content00">
                                        <table>
                                            <tr>
                                                <td style="padding:10px;">
                                                    <a href="https://www.facebook.com/RoboGardenInc"><img src="https://robogarden.s3.amazonaws.com/email_template/facebook.png" width="40px;" height="40px;">
                                                        <a href="https://www.linkedin.com/company/12899767?trk=tyah&trkInfo=clickedVertical%3Acompany%2CentityType%3AentityHistoryName%2CclickedEntityId%3Acompany_company_company_12899767%2Cidx%3A5"><img src="https://s3-us-west-2.amazonaws.com/robogarden/email_template/linkedin.png" width="40px;" height="40px;">
                                                            <a href="https://www.youtube.com/channel/UCHIqV0a1Nex3vX43V7qnF4w"><img src="https://s3-us-west-2.amazonaws.com/robogarden/email_template/youtube.png" width="40px;" height="40px;">
                                                            </a>
                                                        </a>
                                                    </a>
                                                </td>


                                                <td style="padding:10px;">
                                                    <h3>@lang('locale.contact_us'):</h3>
                                                    <h4>@lang('locale.phone'):  +1 (403) 457-3112</h4>
                                                    <h4>@lang('locale.email'):  Support@robogarden.ca</h4>
                                                    <br />
                                                </td>

                                                <td style="padding:10px;">
                                                    <img alt="" id="logofooter" src="https://s3-us-west-2.amazonaws.com/robogarden/logo.png">
                                                </td>

                                            </tr>
                                        </table>


                                    </td>
                                </tr>
                            </table>
                            <!-- // END FOOTER -->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>