#!/usr/bin/env python
import sys
import pandas
from pandas.plotting import scatter_matrix
from sklearn import model_selection
from sklearn import tree
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.externals.six import StringIO
import pydot

# Load dataset
counter = 1
no_of_matches = int(sys.argv[counter])
counter = counter + 1
predictor_type = str(sys.argv[counter])
counter = counter + 1
gid_team1 = []
gid_team2 = []
fifa_rank1 = []
fifa_rank2 = []
fifa_rank_weight = []
goal1 = []
goal2 = []
goal_weight = []
history = []
history_weight = []
matches_played1 = []
matches_played2 = []
matches_played_weight = []
participation1 = []
participation2 = []
participation_weight = []
ratio_winning1 = []
ratio_winning2 = []
ratio_winning_weight = []
win1= []
win2 = []
win_weight = []

for x in range(no_of_matches):
    gid_team1.append(float(sys.argv[counter]))
    counter = counter + 1
    gid_team2.append(float(sys.argv[counter]))
    counter = counter + 1

    fifa_rank1.append(float(sys.argv[counter]))
    counter = counter + 1
    fifa_rank2.append(float(sys.argv[counter]))
    counter = counter + 1
    fifa_rank_weight.append(float(sys.argv[counter]))
    counter = counter + 1

    goal1.append(float(sys.argv[counter]))
    counter = counter + 1
    goal2.append(float(sys.argv[counter]))
    counter = counter + 1
    goal_weight.append(float(sys.argv[counter]))
    counter = counter + 1

    history.append(float(sys.argv[counter]))
    counter = counter + 1
    history_weight.append(float(sys.argv[counter]))
    counter = counter + 1

    matches_played1.append(float(sys.argv[counter]))
    counter = counter + 1
    matches_played2.append(float(sys.argv[counter]))
    counter = counter + 1
    matches_played_weight.append(float(sys.argv[counter]))
    counter = counter + 1

    participation1.append(float(sys.argv[counter]))
    counter = counter + 1
    participation2.append(float(sys.argv[counter]))
    counter = counter + 1
    participation_weight.append(float(sys.argv[counter]))
    counter = counter + 1

    ratio_winning1.append(float(sys.argv[counter]))
    counter = counter + 1
    ratio_winning2.append(float(sys.argv[counter]))
    counter = counter + 1
    ratio_winning_weight.append(float(sys.argv[counter]))
    counter = counter + 1

    win1.append(float(sys.argv[counter]))
    counter = counter + 1
    win2.append(float(sys.argv[counter]))
    counter = counter + 1
    win_weight.append(float(sys.argv[counter]))
    counter = counter + 1


url_fifaRank = "FIFA Rank-Table 1.csv"
dataset_fifaRank = pandas.read_csv(url_fifaRank)

url_Goals = "Goals-Table 1.csv"
dataset_Goals = pandas.read_csv(url_Goals)

url_History = "History between teams-Table 1.csv"
dataset_History = pandas.read_csv(url_History)

url_MatchesPlayed = "Matches Played-Table 1.csv"
dataset_MatchesPlayed = pandas.read_csv(url_MatchesPlayed)

url_Participation = "Participation-Table 1.csv"
dataset_Participation = pandas.read_csv(url_Participation)

url_RatioWinning = "Ratio of Winning-Table 1.csv"
dataset_RatioWinning = pandas.read_csv(url_RatioWinning)

url_Win = "Win-Table 1.csv"
dataset_Win = pandas.read_csv(url_Win)


Cols = 4
Cols_History = 3



# scatter plot matrix
##scatter_matrix(dataset)
##plt.show()
# Split-out validation dataset
array_fifaRank = dataset_fifaRank.values
X_fifaRank = array_fifaRank[:,0:Cols]
Y_fifaRank = array_fifaRank[:,Cols]

array_Goals = dataset_Goals.values
X_Goals = array_Goals[:,0:Cols]
Y_Goals = array_Goals[:,Cols]

array_History = dataset_History.values
X_History = array_History[:,0:Cols_History]
Y_History = array_History[:,Cols_History]

array_MatchesPlayed = dataset_MatchesPlayed.values
X_MatchesPlayed = array_MatchesPlayed[:,0:Cols]
Y_MatchesPlayed = array_MatchesPlayed[:,Cols]

array_Participation = dataset_Participation.values
X_Participation = array_Participation[:,0:Cols]
Y_Participation = array_Participation[:,Cols]

array_RatioWinning = dataset_RatioWinning.values
X_RatioWinning = array_RatioWinning[:,0:Cols]
Y_RatioWinning = array_RatioWinning[:,Cols]

array_Win = dataset_Win.values
X_Win = array_Win[:,0:Cols]
Y_Win = array_Win[:,Cols]


validation_size = 0.20
seed = 7
scoring = 'accuracy'
X_train_fifaRank, X_validation_fifaRank, Y_train_fifaRank, Y_validation_fifaRank = model_selection.train_test_split(X_fifaRank, Y_fifaRank, test_size=validation_size, random_state=seed)
X_train_Goals, X_validation_Goals, Y_train_Goals, Y_validation_Goals = model_selection.train_test_split(X_Goals, Y_Goals, test_size=validation_size, random_state=seed)
X_train_History, X_validation_History, Y_train_History, Y_validation_History = model_selection.train_test_split(X_History, Y_History, test_size=validation_size, random_state=seed)
X_train_MatchesPlayed, X_validation_MatchesPlayed, Y_train_MatchesPlayed, Y_validation_MatchesPlayed = model_selection.train_test_split(X_MatchesPlayed, Y_MatchesPlayed, test_size=validation_size, random_state=seed)
X_train_Participation, X_validation_Participation, Y_train_Participation, Y_validation_Participation = model_selection.train_test_split(X_Participation, Y_Participation, test_size=validation_size, random_state=seed)
X_train_RatioWinning, X_validation_RatioWinning, Y_train_RatioWinning, Y_validation_RatioWinning = model_selection.train_test_split(X_RatioWinning, Y_RatioWinning, test_size=validation_size, random_state=seed)
X_train_Win, X_validation_Win, Y_train_Win, Y_validation_Win = model_selection.train_test_split(X_Win, Y_Win, test_size=validation_size, random_state=seed)


model_fifaRank = None
model_Goals = None
model_History = None
model_MatchesPlayed = None
model_Participation = None
model_RatioWinning = None
model_Win = None
if(predictor_type == "LR"):
    model_fifaRank = LogisticRegression()
    model_Goals = LogisticRegression()
    model_History = LogisticRegression()
    model_MatchesPlayed = LogisticRegression()
    model_Participation = LogisticRegression()
    model_RatioWinning = LogisticRegression()
    model_Win = LogisticRegression()
elif(predictor_type == "LDA"):
    model_fifaRank = LinearDiscriminantAnalysis()
    model_Goals = LinearDiscriminantAnalysis()
    model_History = LinearDiscriminantAnalysis()
    model_MatchesPlayed = LinearDiscriminantAnalysis()
    model_Participation = LinearDiscriminantAnalysis()
    model_RatioWinning = LinearDiscriminantAnalysis()
    model_Win = LinearDiscriminantAnalysis()
elif(predictor_type == "KNN"):
    model_fifaRank = KNeighborsClassifier()
    model_Goals = KNeighborsClassifier()
    model_History = KNeighborsClassifier()
    model_MatchesPlayed = KNeighborsClassifier()
    model_Participation = KNeighborsClassifier()
    model_RatioWinning = KNeighborsClassifier()
    model_Win = KNeighborsClassifier()
elif(predictor_type == "CART"):
    model_fifaRank = DecisionTreeClassifier()
    model_Goals = DecisionTreeClassifier()
    model_History = DecisionTreeClassifier()
    model_MatchesPlayed = DecisionTreeClassifier()
    model_Participation = DecisionTreeClassifier()
    model_RatioWinning = DecisionTreeClassifier()
    model_Win = DecisionTreeClassifier()
elif(predictor_type == "NB"):
    model_fifaRank = GaussianNB()
    model_Goals = GaussianNB()
    model_History = GaussianNB()
    model_MatchesPlayed = GaussianNB()
    model_Participation = GaussianNB()
    model_RatioWinning = GaussianNB()
    model_Win = GaussianNB()
elif(predictor_type == "SVM"):
    model_fifaRank = SVC()
    model_Goals = SVC()
    model_History = SVC()
    model_MatchesPlayed = SVC()
    model_Participation = SVC()
    model_RatioWinning = SVC()
    model_Win = SVC()
else:
    model_fifaRank = LinearDiscriminantAnalysis()
    model_Goals = LinearDiscriminantAnalysis()
    model_History = LinearDiscriminantAnalysis()
    model_MatchesPlayed = LinearDiscriminantAnalysis()
    model_Participation = LinearDiscriminantAnalysis()
    model_RatioWinning = LinearDiscriminantAnalysis()
    model_Win = LinearDiscriminantAnalysis()


model_fifaRank.fit(X_train_fifaRank, Y_train_fifaRank)
model_Goals.fit(X_train_Goals, Y_train_Goals)
model_History.fit(X_train_History, Y_train_History)
model_MatchesPlayed.fit(X_train_MatchesPlayed, Y_train_MatchesPlayed)
model_Participation.fit(X_train_Participation, Y_train_Participation)
model_RatioWinning.fit(X_train_RatioWinning, Y_train_RatioWinning)
model_Win.fit(X_train_Win, Y_train_Win)

predictions_fifaRank = model_fifaRank.predict(X_validation_fifaRank)
predictions_Goals = model_Goals.predict(X_validation_Goals)
predictions_History = model_History.predict(X_validation_History)
predictions_MatchesPlayed = model_MatchesPlayed.predict(X_validation_MatchesPlayed)
predictions_Participation = model_Participation.predict(X_validation_Participation)
predictions_RatioWinning = model_RatioWinning.predict(X_validation_RatioWinning)
predictions_Win = model_Win.predict(X_validation_Win)


#print(accuracy_score(Y_validation, predictions))
#print(confusion_matrix(Y_validation, predictions))
#print(classification_report(Y_validation, predictions))
XT_fifaRank = []
XT_Goals = []
XT_History = []
XT_MatchesPlayed = []
XT_Participation = []
XT_RatioWinning = []
XT_Win = []

for x in range(no_of_matches):
    XT_fifaRank.append([gid_team1[x],fifa_rank1[x],gid_team2[x],fifa_rank2[x]])
    XT_Goals.append([gid_team1[x],goal1[x],gid_team2[x],goal2[x]])
    XT_History.append([gid_team1[x],gid_team2[x],history[x]])
    XT_MatchesPlayed.append([gid_team1[x],matches_played1[x],gid_team2[x],matches_played2[x]])
    XT_Participation.append([gid_team1[x],participation1[x],gid_team2[x],participation2[x]])
    XT_RatioWinning.append([gid_team1[x],ratio_winning1[x],gid_team2[x],ratio_winning1[x]])
    XT_Win.append([gid_team1[x],win1[x],gid_team2[x],win2[x]])

pXT_fifaRank = model_fifaRank.predict(XT_fifaRank)
pXT_Goals = model_Goals.predict(XT_Goals)
pXT_History = model_History.predict(XT_History)
pXT_MatchesPlayed = model_MatchesPlayed.predict(XT_MatchesPlayed)
pXT_Participation = model_Participation.predict(XT_Participation)
pXT_RatioWinning = model_RatioWinning.predict(XT_RatioWinning)
pXT_Win= model_Win.predict(XT_Win)

counter = [0]*no_of_matches
sum = [0]*no_of_matches
toBeReturned = [0]*no_of_matches

for x in range(no_of_matches):
    if(fifa_rank_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (fifa_rank_weight[x]*pXT_fifaRank[x])
    if(goal_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (goal_weight[x]*pXT_Goals[x])
    if(history_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (history_weight[x]*pXT_History[x])
    if(matches_played_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (matches_played_weight[x]*pXT_MatchesPlayed[x])
    if(participation_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (participation_weight[x]*pXT_Participation[x])
    if(ratio_winning_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (ratio_winning_weight[x]*pXT_RatioWinning[x])
    if(win_weight[x] != 0):
        counter[x] = counter[x] + 1
        sum[x]  = sum[x] + (win_weight[x]*pXT_Win[x])


    sum[x] = sum[x]/counter[x]
    if(sum[x] > 0.1):
        toBeReturned[x] = str(1)
    elif(sum[x] < -0.1):
        toBeReturned[x] = str(-1)
    else:
        toBeReturned[x] = str(0)

print ','.join(toBeReturned)

#dot_data = StringIO()
#tree.export_graphviz(knn, out_file=dot_data)
#graph = pydot.graph_from_dot_data(dot_data.getvalue())
#graph[0].write_pdf("model_2000.pdf")

