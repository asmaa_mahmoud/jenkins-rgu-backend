<?php

use Aws\Laravel\AwsServiceProvider;

return [
    'credentials' => [
        'key'    =>  env('S3_KEY'),
        'secret' => env('S3_SECRET')
    ],
    'region' => 'us-west-2',
    'version' => 'latest',

    // You can override settings for specific services
    'Ses' => [
        'region' => 'us-east-1',
    ],
];
