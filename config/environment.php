<?php

return [

    'url' => [
        'vpl' => env('VPL_URL'),
        'portal' => env('PORTAL_URL'),
    ],
];
