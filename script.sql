create table `unlockable` (`id` bigint unsigned not null auto_increment primary key, `xp_to_unlock` int not null, `cost_to_unlock` double(8, 2) not null, `coins_to_unlock` int not null, `discount` varchar(255) not null, `is_plan` tinyint(1) not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `journey_unlockable` (`id` bigint unsigned not null auto_increment primary key, `journey_id` bigint unsigned not null, `unlockable_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `journey_unlockable` add constraint `journey_unlockable_journey_id_foreign` foreign key (`journey_id`) references `journey` (`id`) on delete cascade on update cascade;
alter table `journey_unlockable` add constraint `journey_unlockable_unlockable_id_foreign` foreign key (`unlockable_id`) references `unlockable` (`id`) on delete cascade on update cascade;
create table `activity_unlockable` (`id` bigint unsigned not null auto_increment primary key, `activity_id` bigint unsigned not null, `unlockable_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_unlockable` add constraint `activity_unlockable_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `activity_unlockable` add constraint `activity_unlockable_unlockable_id_foreign` foreign key (`unlockable_id`) references `unlockable` (`id`) on delete cascade on update cascade;
create table `unlockable_method` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null) default character set utf8 collate utf8_unicode_ci;
create table `account_unlockable` (`id` int unsigned not null auto_increment primary key, `charge_id` varchar(255) not null, `unlockable_id` bigint unsigned not null, `account_id` bigint unsigned not null, `method_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `account_unlockable` add constraint `account_unlockable_unlockable_id_foreign` foreign key (`unlockable_id`) references `unlockable` (`id`) on delete cascade on update cascade;
alter table `account_unlockable` add constraint `account_unlockable_account_id_foreign` foreign key (`account_id`) references `account` (`id`) on delete cascade on update cascade;
alter table `account_unlockable` add constraint `account_unlockable_method_id_foreign` foreign key (`method_id`) references `unlockable_method` (`id`) on delete cascade on update cascade;

create table `plan_type` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null) default character set utf8 collate utf8_unicode_ci;
create table `school_charge` (`id` int unsigned not null auto_increment primary key, `charge_id` varchar(255) not null, `cost` double(8, 2) not null, `no_of_students` int not null, `no_of_classroom` int not null, `description` text null, `subscription_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_charge` add constraint `school_charge_subscription_id_foreign` foreign key (`subscription_id`) references `subscription` (`id`) on delete cascade on update cascade;
create table `school_charge_upgrade` (`id` int unsigned not null auto_increment primary key, `charge_id` varchar(255) not null, `cost` double(8, 2) not null, `no_of_students` int not null, `no_of_classroom` int not null, `description` text null, `school_charge_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_charge_upgrade` add constraint `school_charge_upgrade_school_charge_id_foreign` foreign key (`school_charge_id`) references `school_charge` (`id`) on delete cascade on update cascade;
create table `plan_unlockable` (`id` int unsigned not null auto_increment primary key, `plan_history_id` bigint unsigned not null, `unlockable_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `plan_unlockable` add constraint `plan_unlockable_plan_history_id_foreign` foreign key (`plan_history_id`) references `plan_history` (`id`) on delete cascade on update cascade;
alter table `plan_unlockable` add constraint `plan_unlockable_unlockable_id_foreign` foreign key (`unlockable_id`) references `unlockable` (`id`) on delete cascade on update cascade;
create table `school_charge_details` (`id` int unsigned not null auto_increment primary key, `no_of_students` int not null, `no_of_classroom` int not null, `school_charge_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_charge_details` add constraint `school_charge_details_school_charge_id_foreign` foreign key (`school_charge_id`) references `school_charge` (`id`) on delete cascade on update cascade;
create table `extra` (`id` bigint unsigned not null auto_increment primary key, `name` varchar(255) not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `extra_translation` (`id` bigint unsigned not null auto_increment primary key, `extra_id` bigint unsigned not null, `language_code` varchar(255) not null, `description` text null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `extra_translation` add constraint `extra_translation_extra_id_foreign` foreign key (`extra_id`) references `extra` (`id`) on delete cascade on update cascade;
create table `extra_unlockable` (`id` bigint unsigned not null auto_increment primary key, `extra_id` bigint unsigned not null, `unlockable_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `extra_unlockable` add constraint `extra_unlockable_extra_id_foreign` foreign key (`extra_id`) references `extra` (`id`) on delete cascade on update cascade;
alter table `extra_unlockable` add constraint `extra_unlockable_unlockable_id_foreign` foreign key (`unlockable_id`) references `unlockable` (`id`) on delete cascade on update cascade;
ALTER TABLE user CHANGE first_name first_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE user CHANGE last_name last_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE school CHANGE country_id country_id BIGINT(20) UNSIGNED NULL;
alter table `distributor` add `image` varchar(255) null;
create table `school_prices` (`id` int unsigned not null auto_increment primary key, `type` varchar(255) not null, `quantity` int not null, `price` double(8, 2) not null, `discount` int not null default '0', `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `distributor_charge` (`id` int unsigned not null auto_increment primary key, `cost` double(8, 2) null, `no_of_students` int not null, `no_of_classroom` int not null, `description` text null, `distributor_subscription_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `distributor_charge` add constraint `distributor_charge_distributor_subscription_id_foreign` foreign key (`distributor_subscription_id`) references `distributor_subscription` (`id`) on delete cascade on update cascade;
create table `distributor_charge_details` (`id` int unsigned not null auto_increment primary key, `no_of_students` int not null, `no_of_classroom` int not null, `distributor_charge_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `distributor_charge_details` add constraint `distributor_charge_details_distributor_charge_id_foreign` foreign key (`distributor_charge_id`) references `distributor_charge` (`id`) on delete cascade on update cascade;
create table `distributor_charge_upgrade` (`id` int unsigned not null auto_increment primary key, `cost` double(8, 2) null, `no_of_students` int not null, `no_of_classroom` int not null, `description` text null, `distributor_charge_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `distributor_charge_upgrade` add constraint `distributor_charge_upgrade_distributor_charge_id_foreign` foreign key (`distributor_charge_id`) references `distributor_charge` (`id`) on delete cascade on update cascade;
create table `school_bundles` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) null, `price` double(8, 2) not null, `students` int not null, `classrooms` int not null, `discount` double(8, 2) not null default '0') default character set utf8 collate utf8_unicode_ci;
create table `school_bundle_translation` (`id` int unsigned not null auto_increment primary key, `language_code` varchar(255) not null, `description` text null, `school_bundle_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_bundle_translation` add constraint `school_bundle_translation_school_bundle_id_foreign` foreign key (`school_bundle_id`) references `school_bundles` (`id`) on delete cascade on update cascade;

INSERT INTO `unlockable_method` (`id`, `name`) VALUES
(1, 'coins'),
(2, 'xp'),
(3, 'charge');

INSERT INTO `difficulty` (`id`, `name`) VALUES
(1, 'Beginner'),
(2, 'Intermediate'),
(3, 'Advanced');

INSERT INTO `plan_type` (`id`, `name`) VALUES
(1, 'paid'),
(2, 'free');

INSERT INTO `extra` (`id`, `name`) VALUES
(1, 'RoboPal');

INSERT INTO `status` (`name`) VALUES
('wait_activation');
ALTER TABLE `plan` ADD `type_id` INT UNSIGNED NULL AFTER `public`;
ALTER TABLE `plan` ADD FOREIGN KEY (`type_id`) REFERENCES `plan_type`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `plan` DROP FOREIGN KEY `plan_ibfk_1`; ALTER TABLE `plan` ADD CONSTRAINT `plan_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `plan_type`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
INSERT INTO `plan` (`id`, `name`, `public`, `type_id`) VALUES
(20, 'free_learner', 0, 2),
(21, 'free_homeschooler', 0, 2),
(22, 'free_school', 0, 2),
(23, 'Custom', 0, 2);

INSERT INTO `plan_history` (`id`, `plan_id`, `new_name`, `credit_per_month`, `credit_per_quarter`, `credit_per_year`, `account_type_id`, `trial_period`, `is_extra`) VALUES
(23, 20, 'Free', 0.00, 0.00, 0.00, 7, 0, 0),
(24, 21, 'Free', 0.00, 0.00, 0.00, 10, 0, 0),
(25, 22, 'Free', 0.00, 0.00, 0.00, 8, 0, 0),
(26, 23, 'Custom', 0.00, 0.00, 0.00, 8, 0, 0);

INSERT INTO `school_plan` (`id`, `name`, `max_students`, `max_courses`, `plan_history_id`) VALUES
(4, 'free', 10, 1, 25);

INSERT INTO `school_prices` (`type`, `quantity`, `price`) VALUES
('student', 10, 5),
('classroom', 1, 5);
create table `school_charges_log` (`id` int unsigned not null auto_increment primary key, `cost` int null, `discount` double(8, 2) not null default '0', `from` timestamp null, `to` timestamp null, `description` text null, `school_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_charges_log` add constraint `school_charges_log_school_id_foreign` foreign key (`school_id`) references `school` (`id`) on delete cascade on update cascade;
ALTER TABLE `school_charge` ADD `bundle_id` INT(10) UNSIGNED NULL AFTER `deleted_at`;
ALTER TABLE `school_charge_upgrade` ADD `bundle_id` INT(10) UNSIGNED NULL AFTER `deleted_at`;
alter table `school_charge_upgrade` add constraint `school_charge_upgrade_bundle_id_foreign` foreign key (`bundle_id`) references `school_bundles` (`id`) on delete cascade on update cascade;
alter table `school_charge` add constraint `school_charge_bundle_id_foreign` foreign key (`bundle_id`) references `school_bundles` (`id`) on delete cascade on update cascade;

ALTER TABLE `distributor_charge` ADD `bundle_id` INT(10) UNSIGNED NULL AFTER `deleted_at`;
ALTER TABLE `distributor_charge_upgrade` ADD `bundle_id` INT(10) UNSIGNED NULL AFTER `deleted_at`;
alter table `distributor_charge_upgrade` add constraint `distributor_charge_upgrade_bundle_id_foreign` foreign key (`bundle_id`) references `school_bundles` (`id`) on delete cascade on update cascade;
alter table `distributor_charge` add constraint `distributor_charge_bundle_id_foreign` foreign key (`bundle_id`) references `school_bundles` (`id`) on delete cascade on update cascade;
INSERT INTO `school_bundles` (`id`, `name`, `price`, `students`, `classrooms`, `discount`) VALUES (NULL, 'Bronze', '1999', '350', '10', '0.00'), (NULL, 'Silver', '2999', '700', '20', '0.00');
INSERT INTO `school_bundles` (`id`, `name`, `price`, `students`, `classrooms`, `discount`) VALUES (NULL, 'Golden', '3999', '1050', '30', '0.00');
ALTER TABLE `school_charge_details` ADD `name` VARCHAR(255) NULL DEFAULT NULL AFTER `deleted_at`;
ALTER TABLE `distributor_charge_details` ADD `name` VARCHAR(255) NULL AFTER `deleted_at`;
ALTER TABLE `school_charge_upgrade` CHANGE `charge_id` `charge_id` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `school_charges_log` ADD `tax` DOUBLE(8,2) NOT NULL DEFAULT '0.00' AFTER `deleted_at`;

alter table `distributor_charge_upgrade` add `upgrade_code` varchar(255) null, add `used` tinyint(1) not null default '0';
ALTER TABLE `distributor_charge_upgrade` ADD `plan_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `used`;
INSERT INTO `status` (`id`, `name`) VALUES (NULL, 'wait_subscription_approval'), (NULL, 'wait_upgrade_approval');

INSERT INTO `unlockable` (`id`, `xp_to_unlock`, `cost_to_unlock`, `coins_to_unlock`, `discount`, `is_plan`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '0', '0', '0', '0', '1', NULL, NULL, NULL), (NULL, '0', '0', '0', '0', '1', NULL, NULL, NULL);
INSERT INTO `journey_unlockable` (`id`, `journey_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '1', '1', NULL, NULL, NULL);
INSERT INTO `extra_unlockable` (`id`, `extra_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '1', '2', NULL, NULL, NULL);

INSERT INTO `plan_unlockable` (`id`, `plan_history_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '23', '1', NULL, NULL, NULL), (NULL, '24', '1', NULL, NULL, NULL);
INSERT INTO `plan_unlockable` (`id`, `plan_history_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '25', '1', NULL, NULL, NULL);
ALTER TABLE `school_charges_log` ADD `returned` TINYINT(1) NULL DEFAULT '0' AFTER `tax`;

alter table mission_saving add timetaken int null;
alter table student_progress add blocks_number int null, add task_duration int null;
alter table task_progress add first_success int null, add unlocked_by_coins tinyint(1) not null default '0';
ALTER TABLE mission DROP COLUMN mission_state_id;
alter table mission add mission_state_id bigint unsigned null;
alter table mission add constraint mission_mission_state_id_foreign foreign key (mission_state_id) references mission_state (id) on delete cascade on update cascade;
INSERT INTO mission_state(`name`, ratio) VALUES ("normal",1.0);
INSERT INTO mission_state(`name`, ratio) VALUES ("super",2.0);
UPDATE mission SET mission_state_id = 1;
UPDATE mission SET weight = 10;
UPDATE task_progress SET first_success = 1;
UPDATE task_progress SET number_of_trials = 1;
INSERT INTO user_score (xp,coins,user_id)
(select CASE WHEN a.xp IS NULL THEN 0 ELSE a.xp END as xp,CASE WHEN a.coins IS NULL THEN 0 ELSE a.coins END as coins,user.id from user
left join
((SELECT  sum(score) as xp,(sum(score))/5 as coins,user_id as id FROM task_progress where deleted_at is null GROUP by user_id)
UNION
(SELECT  sum(score) as xp,(sum(score))/5 as coins,student_id as id FROM student_progress GROUP by student_id)) a
ON a.id = user.id);
DELETE FROM `distributor` WHERE `distributor`.`id` = 2;
DELETE FROM `distributor_country` WHERE `distributor_country`.`id` = 1;
UPDATE `distributor` SET `image` = 'https://s3-us-west-2.amazonaws.com/robogarden/logo_with_slogan.png' WHERE `distributor`.`id` = 1;
UPDATE `distributor` SET `image` = 'https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/19260597_124993561418929_6646447649087633366_n.png?oh=18e8031b453fb5866ed76bcedaaf9228&oe=5ADEA418' WHERE `distributor`.`id` = 5;
UPDATE `distributor_subscription` SET `distributor_id` = '1' WHERE `distributor_subscription`.`id` = 10;
UPDATE `distributor_subscription` SET `distributor_id` = '1' WHERE `distributor_subscription`.`id` = 129;
alter table task_progress add `best_blocks_number` int(11) DEFAULT NULL,add `best_task_duration` int(11) DEFAULT NULL;
alter table student_progress add `best_blocks_number` int(11) DEFAULT NULL,add `best_task_duration` int(11) DEFAULT NULL;
update question set weight = 20;


$2y$10$JiZuoOnLs0tE1UuX0d4zIefwQ3ivS257FoyCwY9DP9F9QkG6d7J46