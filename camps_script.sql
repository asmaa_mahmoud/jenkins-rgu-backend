SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE `difficulty`, `activity`, `activity_task`, `activity_translation`, `activity_progress`;
SET FOREIGN_KEY_CHECKS = 1;

create table `difficulty` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `activity` (`id` bigint unsigned not null auto_increment primary key, `difficulty_id` int unsigned not null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity` add constraint `activity_difficulty_id_foreign` foreign key (`difficulty_id`) references `difficulty` (`id`) on delete cascade on update cascade;
create table `activity_task` (`id` bigint unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `task_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_task` add constraint `activity_task_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `activity_task` add constraint `activity_task_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
create table `activity_translation` (`id` bigint unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `language_code` varchar(255) not null, `name` varchar(255) not null, `description` text null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_translation` add constraint `activity_translation_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
create table `camp_activity_progress` (`id` bigint unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `user_id` bigint unsigned not null, `task_id` bigint unsigned not null, `no_of_blocks` int not null, `task_duration` varchar(255) not null, `score` int not null, `no_of_trials` int not null, `first_success` int not null, `best_task_duration` int not null, `best_blocks_number` int not null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `camp_activity_progress` add constraint `camp_activity_progress_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `camp_activity_progress` add constraint `camp_activity_progress_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `camp_activity_progress` add constraint `camp_activity_progress_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;

create table `camp` (`id` bigint unsigned not null auto_increment primary key, `name` varchar(255) not null, `description` text null, `icon_url` varchar(255) null, `age_from` smallint null, `age_to` smallint null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity` add `age_from` smallint null, add `age_to` smallint null, add `icon_url` varchar(255) null, add `image_url` varchar(255) null, add `creator_id` bigint unsigned null, add `created_at` timestamp null, add `updated_at` timestamp null;
alter table `activity` add constraint `activity_creator_id_foreign` foreign key (`creator_id`) references `user` (`id`) on delete cascade on update cascade;
create table `default_activity` (`id` bigint unsigned not null auto_increment primary key, `difficulty_id` int unsigned not null, `age_from` smallint null, `age_to` smallint null, `icon_url` varchar(255) null, `published` BOOLEAN NULL DEFAULT TRUE, `image_url` varchar(255) null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `default_activity` add constraint `default_activity_difficulty_id_foreign` foreign key (`difficulty_id`) references `difficulty` (`id`) on delete cascade on update cascade;
alter table `activity_task` add `order` int null, add `default_activity_id` bigint unsigned null;
alter table `activity_task` add constraint `activity_task_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `activity_translation` add `video_url` varchar(255) null, add `summary` text null, add `concepts` text null, add `technical_details` text null, add `prerequisites` text null, add `default_activity_id` bigint unsigned null;
alter table `activity_translation` add constraint `activity_translation_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `camp_activity_progress` add `unlocked_by_coins` tinyint(1) not null default '0', add `camp_id` bigint unsigned null, add `default_activity_id` bigint unsigned null;
alter table `camp_activity_progress` add constraint `camp_activity_progress_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `camp_activity_progress` add constraint `camp_activity_progress_camp_id_foreign` foreign key (`camp_id`) references `camp` (`id`) on delete cascade on update cascade;
create table `camp_activity` (`id` int unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null, `camp_id` bigint unsigned not null, `order` int null, `creator_type` varchar(255) null, `creator_id` bigint unsigned null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `camp_activity` add constraint `camp_activity_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `camp_activity` add constraint `camp_activity_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `camp_activity` add constraint `camp_activity_camp_id_foreign` foreign key (`camp_id`) references `camp` (`id`) on delete cascade on update cascade;
create table `school_camp` (`id` bigint unsigned not null auto_increment primary key, `school_id` bigint unsigned not null, `camp_id` bigint unsigned not null, `no_of_students` int null, `no_of_activities` int null, `creator_type` varchar(255) null, `creator_id` bigint unsigned null, `starts_at` DATE null, `ends_at` DATE null, `locked` tinyint(1) not null default '0', `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_camp` add constraint `school_camp_school_id_foreign` foreign key (`school_id`) references `school` (`id`) on delete cascade on update cascade;
alter table `school_camp` add constraint `school_camp_camp_id_foreign` foreign key (`camp_id`) references `camp` (`id`) on delete cascade on update cascade;
create table `school_distributor_camp` (`id` bigint unsigned not null auto_increment primary key, `school_camp_id` bigint unsigned not null, `distributor_id` bigint unsigned not null, `activation_code` varchar(255) null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_distributor_camp` add constraint `school_distributor_camp_school_camp_id_foreign` foreign key (`school_camp_id`) references `school_camp` (`id`) on delete cascade on update cascade;
alter table `school_distributor_camp` add constraint `school_distributor_camp_distributor_id_foreign` foreign key (`distributor_id`) references `distributor` (`id`) on delete cascade on update cascade;
create table `school_distributor_camp_upgrade` (`id` bigint unsigned not null auto_increment primary key, `school_camp_id` bigint unsigned not null, `description` text null, `no_of_students` int null, `no_of_activities` int null, `ends_at` DATE null, `activation_code` varchar(255) null, `used` tinyint(1) not null default '0', `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_distributor_camp_upgrade` add constraint `school_distributor_camp_upgrade_school_camp_id_foreign` foreign key (`school_camp_id`) references `school_camp` (`id`) on delete cascade on update cascade;
create table `camp_user` (`id` bigint unsigned not null auto_increment primary key, `camp_id` bigint unsigned not null, `user_id` bigint unsigned not null, `role_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `camp_user` add constraint `camp_user_camp_id_foreign` foreign key (`camp_id`) references `camp` (`id`) on delete cascade on update cascade;
alter table `camp_user` add constraint `camp_user_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `camp_user` add constraint `camp_user_role_id_foreign` foreign key (`role_id`) references `role` (`id`) on delete cascade on update cascade;
create table `school_camp_details` (`id` bigint unsigned not null auto_increment primary key, `school_camp_id` bigint unsigned not null, `no_of_students` int null, `no_of_activities` int null, `ends_at` DATE null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `school_camp_details` add constraint `school_camp_details_school_camp_id_foreign` foreign key (`school_camp_id`) references `school_camp` (`id`) on delete cascade on update cascade;
create table `activity_period` (`id` int unsigned not null auto_increment primary key, `period` int not null, `initial_price` int not null default '0') default character set utf8 collate utf8_unicode_ci;
create table `activity_period_translation` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null, `language_code` varchar(255) not null, `period_id` int unsigned not null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_period_translation` add constraint `activity_period_translation_period_id_foreign` foreign key (`period_id`) references `activity_period` (`id`) on delete cascade on update cascade;
create table `activity_price` (`id` bigint unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `period_id` int unsigned not null, `price` double not null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_price` add constraint `activity_price_period_id_foreign` foreign key (`period_id`) references `activity_period` (`id`) on delete cascade on update cascade;
alter table `activity_price` add constraint `activity_price_activity_id_foreign` foreign key (`activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
create table `camp_discount` (`id` bigint unsigned not null auto_increment primary key, `no_of_activities` int null, `no_of_students` int null, `discount` double(8, 2) not null default '0', `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `tags` (`id` int unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `activity_tag` (`id` int unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null, `tag_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_tag` add constraint `activity_tag_tag_id_foreign` foreign key (`tag_id`) references `tags` (`id`) on delete cascade on update cascade;
alter table `activity_tag` add constraint `activity_tag_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `activity_tag` add constraint `activity_tag_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
create table `tag_translation` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null, `language_code` varchar(255) not null, `tag_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `tag_translation` add constraint `tag_translation_tag_id_foreign` foreign key (`tag_id`) references `tags` (`id`) on delete cascade on update cascade;
create table `activity_screenshots` (`id` int unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null, `screenshot_url` varchar(255) not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_screenshots` add constraint `activity_screenshots_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `activity_screenshots` add constraint `activity_screenshots_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
create table `camp_template` (`id` bigint unsigned not null auto_increment primary key, `icon_url` varchar(255) null, `age_from` smallint null, `age_to` smallint null, `starts_at` DATE null, `ends_at` DATE null, `published` BOOLEAN NULL DEFAULT TRUE, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
create table `camp_template_translation` (`id` bigint unsigned not null auto_increment primary key, `title` varchar(255) not null, `language_code` varchar(255) not null, `description` text null, `video_url` varchar(255) null, `summary` text null, `concepts` text null, `technical_details` text null, `prerequisites` text null, `camp_template_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `camp_template_translation` add constraint `camp_template_translation_camp_template_id_foreign` foreign key (`camp_template_id`) references `camp_template` (`id`) on delete cascade on update cascade;
create table `activity_pre_next` (`id` bigint unsigned not null auto_increment primary key, `main_default_activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null, `pre_or_next` tinyint(1) not null, `order` int null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
-- alter table `activity_pre_next` add constraint `activity_pre_next_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
-- alter table `activity_pre_next` add constraint `activity_pre_next_main_default_activity_id_foreign` foreign key (`main_default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
-- alter table `activity_pre_next` add constraint `activity_pre_next_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
create table `camp_template_activity` (`id` bigint unsigned not null auto_increment primary key, `default_activity_id` bigint unsigned not null, `camp_template_id` bigint unsigned not null, `order` int null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `camp_template_activity` add constraint `camp_template_activity_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `camp_template_activity` add constraint `camp_template_activity_camp_template_id_foreign` foreign key (`camp_template_id`) references `camp_template` (`id`) on delete cascade on update cascade;

create table `difficulty_translation` (`id` bigint unsigned not null auto_increment primary key, `difficulty_id` int unsigned not null, `title` varchar(255) not null, `language_code` varchar(255) not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `difficulty_translation` add constraint `difficulty_translation_difficulty_id_foreign` foreign key (`difficulty_id`) references `difficulty` (`id`) on delete cascade on update cascade;
INSERT INTO `difficulty_translation` (`id`, `difficulty_id`, `title`, `language_code`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Beginner', 'en', NULL, NULL), (NULL, '2', 'Intermediate', 'en', NULL, NULL), (NULL, '3', 'Advanced', 'en', NULL, NULL), (NULL, '1', 'Principiante', 'pt', NULL, NULL), (NULL, '2', 'Intermediário', 'pt', NULL, NULL), (NULL, '3', 'Avançado', 'pt', NULL, NULL), (NULL, '1', '初学者', 'zh', NULL, NULL), (NULL, '2', '中间', 'zh', NULL, NULL), (NULL, '3', '高级', 'zh', NULL, NULL);

<!-- admin portal editing -->

INSERT INTO `difficulty` (`id`, `name`) VALUES (1, 'Beginner'), (2, 'Intermediate'), (3, 'Advanced');
-- INSERT INTO `adpermissions`(`id`, `title`, `name`) VALUES (10, 'Activity', 'activity');
-- INSERT INTO `adpermission_subs` (`id`, `title`, `name`, `permission_id`, `basic`) VALUES (30, 'Add', 'add', '10', '0'), (31, 'Edit', 'edit', '10', '0'), (32, 'Delete', 'delete', '10', '1'), (33, 'View', 'view', '10', '1');
-- INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '31', '1', b'0', NULL, NULL, NULL), (NULL, '2', '31', '2', b'0', NULL, NULL, NULL), (NULL, '2', '31', '3', b'0', NULL, NULL, NULL), (NULL, '2', '31', '4', b'0', NULL, NULL, NULL), (NULL, '2', '31', '5', b'0', NULL, NULL, NULL), (NULL, '2', '31', '6', b'0', NULL, NULL, NULL), (NULL, '2', '31', '7', b'0', NULL, NULL, NULL), (NULL, '2', '31', NULL, b'1', NULL, NULL, NULL);
-- INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '30', '1', b'0', NULL, NULL, NULL), (NULL, '2', '30', '2', b'0', NULL, NULL, NULL), (NULL, '2', '30', '3', b'0', NULL, NULL, NULL), (NULL, '2', '30', '4', b'0', NULL, NULL, NULL), (NULL, '2', '30', '5', b'0', NULL, NULL, NULL), (NULL, '2', '30', '6', b'0', NULL, NULL, NULL), (NULL, '2', '30', '7', b'0', NULL, NULL, NULL), (NULL, '2', '30', NULL, b'1', NULL, NULL, NULL);
-- INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '32', NULL, b'1', NULL, NULL, NULL), (NULL, '2', '33', NULL, b'1', NULL, NULL, NULL);
INSERT INTO `activity_period` (`id`, `period`, `initial_price`) VALUES (1, '0', '1000'), (2, '1', '5'), (3, '7', '10'), (4, '30', '15'), (5, '90', '45'), (6, '180', '90'), (7, '365', '180');
INSERT INTO `activity_period_translation` (`id`, `name`, `language_code`, `period_id`) VALUES (NULL, 'Life Time', 'en', '1'), (NULL, '1 Day', 'en', '2'), (NULL, '1 Week', 'en', '3'), (NULL, '1 Month', 'en', '4'), (NULL, '3 Month', 'en', '5'), (NULL, '6 Month', 'en', '6'), (NULL, '1 Year', 'en', '7');
-- INSERT INTO `adpermissions` (`id`, `title`, `name`) VALUES (11, 'Camp', 'camp');
-- INSERT INTO `adpermission_subs` (`id`, `title`, `name`, `permission_id`, `basic`) VALUES (34, 'Add', 'add', '11', '0'), (35, 'Edit', 'edit', '11', '0'), (36, 'Delete', 'delete', '11', '1');
-- INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '34', NULL, b'1', NULL, NULL, NULL), (NULL, '2', '35', NULL, b'1', NULL, NULL, NULL), (NULL, '2', '36', NULL, b'1', NULL, NULL, NULL);
INSERT INTO `tags` (`id`, `created_at`, `updated_at`) VALUES (1, NULL, NULL), (2, NULL, NULL), (3, NULL, NULL), (4, NULL, NULL);
INSERT INTO `tag_translation` (`id`, `name`, `language_code`, `tag_id`, `created_at`, `updated_at`) VALUES (NULL, 'Science', 'en', '1', NULL, NULL), (NULL, 'Math', 'en', '2', NULL, NULL), (NULL, 'Technology', 'en', '3', NULL, NULL), (NULL, 'Engineering', 'en', '4', NULL, NULL);
-- INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '34', '1', b'0', NULL, NULL, NULL), (NULL, '2', '34', '2', b'0', NULL, NULL, NULL), (NULL, '2', '34', '3', b'0', NULL, NULL, NULL), (NULL, '2', '34', '4', b'0', NULL, NULL, NULL), (NULL, '2', '34', '5', b'0', NULL, NULL, NULL), (NULL, '2', '34', '6', b'0', NULL, NULL, NULL), (NULL, '2', '34', '7', b'0', NULL, NULL, NULL);
-- INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '35', '1', b'0', NULL, NULL, NULL), (NULL, '2', '35', '2', b'0', NULL, NULL, NULL), (NULL, '2', '35', '3', b'0', NULL, NULL, NULL), (NULL, '2', '35', '4', b'0', NULL, NULL, NULL), (NULL, '2', '35', '5', b'0', NULL, NULL, NULL), (NULL, '2', '35', '6', b'0', NULL, NULL, NULL), (NULL, '2', '35', '7', b'0', NULL, NULL, NULL);

alter table `activity_period` add `account_type_id` bigint unsigned null;
alter table `activity_period` add constraint `activity_period_account_type_id_foreign` foreign key (`account_type_id`) references `account_type` (`id`) on delete cascade on update cascade;
create table `activity_progress` (`id` bigint unsigned not null auto_increment primary key, `activity_id` bigint unsigned null, `user_id` bigint unsigned not null, `task_id` bigint unsigned not null, `no_of_blocks` int not null, `task_duration` varchar(255) not null, `score` int not null, `no_of_trials` int not null, `first_success` int not null, `best_task_duration` int not null, `best_blocks_number` int not null, `unlocked_by_coins` tinyint(1) not null default '0', `default_activity_id` bigint unsigned null, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `activity_progress` add constraint `activity_progress_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `activity_progress` add constraint `activity_progress_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `activity_progress` add constraint `activity_progress_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
alter table `activity_progress` add constraint `activity_progress_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `activity_unlockable` drop foreign key `activity_unlockable_activity_id_foreign`;
ALTER TABLE activity_unlockable CHANGE activity_id activity_id BIGINT UNSIGNED DEFAULT NULL;
alter table `activity_unlockable` add constraint `activity_unlockable_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `activity_unlockable` add `default_activity_id` bigint unsigned null;
alter table `activity_unlockable` add constraint `activity_unlockable_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;

create table `invitation_activity` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `invitation_id` bigint unsigned not null, `progress_lock` tinyint(1) not null, `activity_id` bigint unsigned not null) default character set utf8 collate utf8_unicode_ci;
alter table `invitation_activity` add constraint `invitation_activity_invitation_id_foreign` foreign key (`invitation_id`) references `invitation` (`id`) on delete cascade on update cascade;
alter table `invitation_activity` add constraint `invitation_activity_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
create table `invitation_default_activity` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `invitation_id` bigint unsigned not null, `progress_lock` tinyint(1) not null, `default_activity_id` bigint unsigned not null) default character set utf8 collate utf8_unicode_ci;
alter table `invitation_default_activity` add constraint `invitation_default_activity_invitation_id_foreign` foreign key (`invitation_id`) references `invitation` (`id`) on delete cascade on update cascade;
alter table `invitation_default_activity` add constraint `invitation_default_activity_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `activity_price` add `discount` double not null default '0';
ALTER TABLE `activity_progress` CHANGE `best_blocks_number` `best_blocks_number` INT(11) NULL DEFAULT NULL;
ALTER TABLE `activity_progress` CHANGE `no_of_blocks` `no_of_blocks` INT(11) NULL DEFAULT NULL;
ALTER TABLE `activity_progress` CHANGE `task_duration` `task_duration` INT(11) NULL DEFAULT NULL;
ALTER TABLE `activity_progress` CHANGE `score` `score` INT(11) NULL DEFAULT NULL;
ALTER TABLE `activity_progress` CHANGE `no_of_trials` `no_of_trials` INT(11) NULL DEFAULT NULL;
ALTER TABLE `activity_progress` CHANGE `first_success` `first_success` INT(11) NULL DEFAULT NULL;
ALTER TABLE `activity_progress` CHANGE `best_task_duration` `best_task_duration` INT(11) NULL DEFAULT NULL;
create table `mission_saving_activity` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `name` varchar(255) not null, `timetaken` int null, `mission_id` bigint unsigned not null, `user_id` bigint unsigned not null, `xml` longtext not null, `default_activity_id` bigint unsigned null, `activity_id` bigint unsigned null) default character set utf8 collate utf8_unicode_ci;
alter table `mission_saving_activity` add constraint `mission_saving_activity_mission_id_foreign` foreign key (`mission_id`) references `mission` (`id`) on delete cascade on update cascade;
alter table `mission_saving_activity` add constraint `mission_saving_activity_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `mission_saving_activity` add constraint `mission_saving_activity_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `mission_saving_activity` add constraint `mission_saving_activity_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;


<!-- admin portal editing -->

INSERT INTO `difficulty` (`id`, `name`) VALUES (1, 'Beginner'), (2, 'Intermediate'), (3, 'Advanced');
INSERT INTO `adpermissions`(`id`, `title`, `name`) VALUES (10, 'Activity', 'activity');
INSERT INTO `adpermission_subs` (`id`, `title`, `name`, `permission_id`, `basic`) VALUES (30, 'Add', 'add', '10', '0'), (31, 'Edit', 'edit', '10', '0'), (32, 'Delete', 'delete', '10', '1'), (33, 'View', 'view', '10', '1');
INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '31', '1', b'0', NULL, NULL, NULL), (NULL, '2', '31', '2', b'0', NULL, NULL, NULL), (NULL, '2', '31', '3', b'0', NULL, NULL, NULL), (NULL, '2', '31', '4', b'0', NULL, NULL, NULL), (NULL, '2', '31', '5', b'0', NULL, NULL, NULL), (NULL, '2', '31', '6', b'0', NULL, NULL, NULL), (NULL, '2', '31', '7', b'0', NULL, NULL, NULL), (NULL, '2', '31', NULL, b'1', NULL, NULL, NULL);
INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '30', '1', b'0', NULL, NULL, NULL), (NULL, '2', '30', '2', b'0', NULL, NULL, NULL), (NULL, '2', '30', '3', b'0', NULL, NULL, NULL), (NULL, '2', '30', '4', b'0', NULL, NULL, NULL), (NULL, '2', '30', '5', b'0', NULL, NULL, NULL), (NULL, '2', '30', '6', b'0', NULL, NULL, NULL), (NULL, '2', '30', '7', b'0', NULL, NULL, NULL), (NULL, '2', '30', NULL, b'1', NULL, NULL, NULL);
INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '32', NULL, b'1', NULL, NULL, NULL), (NULL, '2', '33', NULL, b'1', NULL, NULL, NULL);
INSERT INTO `activity_period` (`id`, `period`, `initial_price`) VALUES (1, '0', '1000'), (2, '1', '5'), (3, '7', '10'), (4, '30', '15'), (5, '90', '45'), (6, '180', '90'), (7, '365', '180');
INSERT INTO `activity_period_translation` (`id`, `name`, `language_code`, `period_id`) VALUES (NULL, 'Life Time', 'en', '1'), (NULL, '1 Day', 'en', '2'), (NULL, '1 Week', 'en', '3'), (NULL, '1 Month', 'en', '4'), (NULL, '3 Month', 'en', '5'), (NULL, '6 Month', 'en', '6'), (NULL, '1 Year', 'en', '7');
INSERT INTO `adpermissions` (`id`, `title`, `name`) VALUES (11, 'Camp', 'camp');
INSERT INTO `adpermission_subs` (`id`, `title`, `name`, `permission_id`, `basic`) VALUES (34, 'Add', 'add', '11', '0'), (35, 'Edit', 'edit', '11', '0'), (36, 'Delete', 'delete', '11', '1');
INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '34', NULL, b'1', NULL, NULL, NULL), (NULL, '2', '35', NULL, b'1', NULL, NULL, NULL), (NULL, '2', '36', NULL, b'1', NULL, NULL, NULL);
INSERT INTO `tags` (`id`, `created_at`, `updated_at`) VALUES (1, NULL, NULL), (2, NULL, NULL), (3, NULL, NULL), (4, NULL, NULL);
INSERT INTO `tag_translation` (`id`, `name`, `language_code`, `tag_id`, `created_at`, `updated_at`) VALUES (NULL, 'Science', 'en', '1', NULL, NULL), (NULL, 'Math', 'en', '2', NULL, NULL), (NULL, 'Technology', 'en', '3', NULL, NULL), (NULL, 'Engineering', 'en', '4', NULL, NULL);
INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '34', '1', b'0', NULL, NULL, NULL), (NULL, '2', '34', '2', b'0', NULL, NULL, NULL), (NULL, '2', '34', '3', b'0', NULL, NULL, NULL), (NULL, '2', '34', '4', b'0', NULL, NULL, NULL), (NULL, '2', '34', '5', b'0', NULL, NULL, NULL), (NULL, '2', '34', '6', b'0', NULL, NULL, NULL), (NULL, '2', '34', '7', b'0', NULL, NULL, NULL);
INSERT INTO `adrole_adpermission` (`id`, `role_id`, `permission_id`, `language_id`, `basic_info`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '2', '35', '1', b'0', NULL, NULL, NULL), (NULL, '2', '35', '2', b'0', NULL, NULL, NULL), (NULL, '2', '35', '3', b'0', NULL, NULL, NULL), (NULL, '2', '35', '4', b'0', NULL, NULL, NULL), (NULL, '2', '35', '5', b'0', NULL, NULL, NULL), (NULL, '2', '35', '6', b'0', NULL, NULL, NULL), (NULL, '2', '35', '7', b'0', NULL, NULL, NULL);


ALTER TABLE `school_camp` CHANGE `starts_at` `starts_at` BIGINT NULL DEFAULT NULL;
ALTER TABLE `school_camp` CHANGE `ends_at` `ends_at` BIGINT NULL DEFAULT NULL;
ALTER TABLE `school_camp_details` CHANGE `ends_at` `ends_at` BIGINT NULL DEFAULT NULL;

--don't use this
-- CREATE TABLE `extra` (
--   `id` bigint(20) UNSIGNED NOT NULL,
--   `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `extra`
--   ADD PRIMARY KEY (`id`);
-- ALTER TABLE `extra`
--   MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
-- CREATE TABLE `extra_translation` (
--   `id` bigint(20) UNSIGNED NOT NULL,
--   `extra_id` bigint(20) UNSIGNED NOT NULL,
--   `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `description` text COLLATE utf8_unicode_ci,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `extra_translation`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `extra_translation_extra_id_foreign` (`extra_id`);
-- ALTER TABLE `extra_translation`
--   MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
-- ALTER TABLE `extra_translation`
--   ADD CONSTRAINT `extra_translation_extra_id_foreign` FOREIGN KEY (`extra_id`) REFERENCES `extra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
-- CREATE TABLE `unlockable` (
--   `id` bigint(20) UNSIGNED NOT NULL,
--   `xp_to_unlock` int(11) NOT NULL,
--   `cost_to_unlock` double(8,2) NOT NULL,
--   `coins_to_unlock` int(11) NOT NULL,
--   `discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `is_plan` tinyint(1) NOT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `unlockable`
--   ADD PRIMARY KEY (`id`);
-- ALTER TABLE `unlockable`
--   MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
-- CREATE TABLE `account_unlockable` (
--   `id` int(10) UNSIGNED NOT NULL,
--   `charge_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `unlockable_id` bigint(20) UNSIGNED NOT NULL,
--   `account_id` bigint(20) UNSIGNED NOT NULL,
--   `method_id` int(10) UNSIGNED NOT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `account_unlockable`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `account_unlockable_unlockable_id_foreign` (`unlockable_id`),
--   ADD KEY `account_unlockable_account_id_foreign` (`account_id`),
--   ADD KEY `account_unlockable_method_id_foreign` (`method_id`);

-- CREATE TABLE `activity_unlockable` (
--   `id` bigint(20) UNSIGNED NOT NULL,
--   `activity_id` bigint(20) UNSIGNED NOT NULL,
--   `unlockable_id` bigint(20) UNSIGNED NOT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
-- ALTER TABLE `activity_unlockable`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `activity_unlockable_activity_id_foreign` (`activity_id`),
--   ADD KEY `activity_unlockable_unlockable_id_foreign` (`unlockable_id`);
-- ALTER TABLE `activity_unlockable`
--   MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
-- ALTER TABLE `activity_unlockable`
--   ADD CONSTRAINT `activity_unlockable_activity_id_foreign` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `activity_unlockable_unlockable_id_foreign` FOREIGN KEY (`unlockable_id`) REFERENCES `unlockable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `activity_unlockable` ADD `default_activity_id` BIGINT UNSIGNED NULL AFTER `deleted_at`;
-- ALTER TABLE `activity_unlockable`
--   ADD CONSTRAINT `activity_unlockable_default_activity_id_foreign` FOREIGN KEY (`default_activity_id`) REFERENCES `default_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `activity_unlockable` CHANGE `activity_id` `activity_id` BIGINT(20) UNSIGNED NULL;
ALTER TABLE `activity_unlockable` CHANGE `default_activity_id` `default_activity_id` BIGINT(20) UNSIGNED NULL;
--
-- CREATE TABLE `extra_unlockable` (
--   `id` bigint(20) UNSIGNED NOT NULL,
--   `extra_id` bigint(20) UNSIGNED NOT NULL,
--   `unlockable_id` bigint(20) UNSIGNED NOT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `extra_unlockable`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `extra_unlockable_extra_id_foreign` (`extra_id`),
--   ADD KEY `extra_unlockable_unlockable_id_foreign` (`unlockable_id`);
--
-- CREATE TABLE `journey_unlockable` (
--   `id` bigint(20) UNSIGNED NOT NULL,
--   `journey_id` bigint(20) UNSIGNED NOT NULL,
--   `unlockable_id` bigint(20) UNSIGNED NOT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL,
--   `deleted_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `journey_unlockable`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `journey_unlockable_journey_id_foreign` (`journey_id`),
--   ADD KEY `journey_unlockable_unlockable_id_foreign` (`unlockable_id`);
--
-- ALTER TABLE `journey_unlockable`
--   MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
-- ALTER TABLE `journey_unlockable`
--   ADD CONSTRAINT `journey_unlockable_journey_id_foreign` FOREIGN KEY (`journey_id`) REFERENCES `journey` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `journey_unlockable_unlockable_id_foreign` FOREIGN KEY (`unlockable_id`) REFERENCES `unlockable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
-- CREATE TABLE `unlockable_method` (
--   `id` int(10) UNSIGNED NOT NULL,
--   `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE `unlockable_method`
--   ADD PRIMARY KEY (`id`);
-- ALTER TABLE `unlockable_method`
--   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;