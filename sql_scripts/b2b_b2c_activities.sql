ALTER TABLE `default_activity` ADD `is_b2b` BOOLEAN NOT NULL DEFAULT FALSE AFTER `type`, ADD `is_b2c` BOOLEAN NOT NULL DEFAULT FALSE AFTER `is_b2b`;
ALTER TABLE `activity` ADD `is_b2b` BOOLEAN NOT NULL DEFAULT TRUE AFTER `updated_at`, ADD `is_b2c` BOOLEAN NOT NULL DEFAULT FALSE AFTER `is_b2b`;

UPDATE `default_activity` SET `is_b2b` = true WHERE `id` not in (38, 39);
UPDATE `default_activity` SET `is_b2c` = true WHERE `id` in (35, 36, 37, 38, 39);
INSERT INTO `plan_unlockable` (`id`, `plan_history_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '24', '209', NULL, NULL, NULL), (NULL, '23', '208', NULL, NULL, NULL), (NULL, '23', '209', NULL, NULL, NULL), (NULL, '24', '208', NULL, NULL, NULL);