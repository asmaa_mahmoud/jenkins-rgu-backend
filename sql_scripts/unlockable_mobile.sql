ALTER TABLE `unlockable` ADD `is_mobile` BOOLEAN NULL DEFAULT FALSE AFTER `is_plan`;

INSERT INTO `unlockable` (`id`, `xp_to_unlock`, `cost_to_unlock`, `coins_to_unlock`, `discount`, `is_plan`, `is_mobile`, `created_at`, `updated_at`, `deleted_at`) VALUES (208, '0', '0.00', '0', '0', '0', '1', '2018-06-19 10:56:57', '2018-06-19 10:56:57', NULL);

INSERT INTO `activity_unlockable` (`id`, `activity_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`, `default_activity_id`) VALUES (NULL, NULL, '208', NULL, NULL, NULL, '35');

INSERT INTO `plan_unlockable` (`id`, `plan_history_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '23', '208', NULL, NULL, NULL);

INSERT INTO `plan_unlockable` (`id`, `plan_history_id`, `unlockable_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '24', '208', NULL, NULL, NULL);

