START TRANSACTION ;
create table `analysis_category` (`id` bigint unsigned not null auto_increment primary key,`starting_question_id`  bigint unsigned  null,
`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null)
 default character set utf8 collate utf8_unicode_ci;

create table `analysis_category_translations` (`id` bigint unsigned not null auto_increment primary key,
	`language_code` varchar(255) not null,`name` varchar(255) not null,
	`analysis_category_id` bigint unsigned not null,
	`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;

alter table `analysis_category_translations` add constraint `analysis_category_translations_analysis_category_id_foreign` foreign key (`analysis_category_id`) references `analysis_category` (`id`) on delete cascade on update cascade;


create table `analysis_questions` (`id` bigint unsigned not null auto_increment primary key,
`analysis_category_id` bigint unsigned not null,
`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null)
 default character set utf8 collate utf8_unicode_ci;
 alter table `analysis_questions` add constraint `analysis_questions_analysis_category_id_foreign` foreign key (`analysis_category_id`) references `analysis_category` (`id`) on delete cascade on update cascade;

alter table `analysis_category` add constraint `analysis_category_analysis_question_id_foreign` foreign key (`starting_question_id`) references `analysis_questions` (`id`) on delete cascade on update cascade;


create table `analysis_questions_translations` (`id` bigint unsigned not null auto_increment primary key,
	`language_code` varchar(255) not null,`question_description` varchar(255) not null,
	`question_id` bigint unsigned not null,
	`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;

alter table `analysis_questions_translations` add constraint `analysis_questions_translations_analysis_questions_id_foreign` foreign key (`question_id`) references `analysis_questions` (`id`) on delete cascade on update cascade;


create table `analysis_answers` (`id` bigint unsigned not null auto_increment primary key,`factor` FLOAT  null,`deduction` varchar(255)  null,
`question_id` bigint unsigned not null,`next_question_id` bigint unsigned  null,
`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null)
 default character set utf8 collate utf8_unicode_ci;
 alter table `analysis_answers` add constraint `analysis_answers_analysis_questions_id_foreign` foreign key (`question_id`) references `analysis_questions` (`id`) on delete cascade on update cascade;
 alter table `analysis_answers` add constraint `analysis_answers_analysis_questions_next_id_foreign` foreign key (`next_question_id`) references `analysis_questions` (`id`) on delete cascade on update cascade;

create table `analysis_answers_translations` (`id` bigint unsigned not null auto_increment primary key,
	`language_code` varchar(255) not null,`answer_description` varchar(255) not null,
	`answer_id` bigint unsigned not null,
	`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;

alter table `analysis_answers_translations` add constraint `analysis_answers_translations_analysis_answers_id_foreign` foreign key (`answer_id`) references `analysis_answers` (`id`) on delete cascade on update cascade;


create table `submissions` (`id` bigint unsigned not null auto_increment primary key,`final_result` varchar(255)  null,
`user_id` bigint unsigned  not null,
`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null)
 default character set utf8 collate utf8_unicode_ci;
 alter table `submissions` add constraint `submissions_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;


create table `analysis_qa` (`id` bigint unsigned not null auto_increment primary key,
`question_id` bigint unsigned not null,`answer_id` bigint unsigned not null,
`submission_id` bigint unsigned not null,`created_at` timestamp null,`updated_at` timestamp null,`deleted_at` timestamp null)
 default character set utf8 collate utf8_unicode_ci;
 alter table `analysis_qa` add constraint `analysis_qa_analysis_question_id_foreign` foreign key (`question_id`) references `analysis_questions` (`id`) on delete cascade on update cascade;
 alter table `analysis_qa` add constraint `analysis_qa_answer_id_foreign` foreign key (`answer_id`) references `analysis_answers` (`id`) on delete cascade on update cascade;
 alter table `analysis_qa` add constraint `analysis_qa_user_submission_id_foreign` foreign key (`submission_id`) references `submissions` (`id`) on delete cascade on update cascade;

COMMIT ;







