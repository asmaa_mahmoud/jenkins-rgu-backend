CREATE TABLE `question_task` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `html_step_id` bigint(20) UNSIGNED  NULL,
  `quiz_id` bigint(20) UNSIGNED  NULL,
  `editor_id` bigint(20) UNSIGNED  NULL
) ;

#alter table `question_task` add constraint `question_task_question_id_foreign` foreign key (`question_id`) references `question` (`id`) on delete cascade on update cascade;
#alter table `question_task` add constraint `question_task_quiz_id_foreign` foreign key (`quiz_id`) references `quiz` (`id`) on delete cascade on update cascade;
alter table `question_task` add constraint `question_task_editor_id_foreign` foreign key (`editor_id`) references `mission_editor` (`id`) on delete cascade on update cascade;
alter table `question_task` add constraint `question_task_html_step_id_foreign` foreign key (`html_step_id`) references `mission_html_steps` (`id`) on delete cascade on update cascade;




ALTER TABLE `question` ADD `question_type` VARCHAR(255) NOT NULL AFTER `emulator_type_id`;

ALTER TABLE `mission_html_steps` ADD `step_type` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `editor_text`;
update `mission_html_steps`set step_type="free" where step_type is null;
ALTER TABLE `mission_html_steps_translation` ADD `step_editor` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `editor_text`;
CREATE TABLE `mcq_question` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `weight` int(11) NOT NULL,
  `main_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `initial_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_editor` smallint(6) NOT NULL DEFAULT '0',
  `editor_code` longtext COLLATE utf8_unicode_ci,
  `emulator_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `selectMany` BOOLEAN NOT NULL DEFAULT 0 
) ;

alter table `mcq_question` add constraint `mcq_question_emulator_type_id_foreign` foreign key (`emulator_type_id`) references `emulator_type` (`id`) on delete cascade on update cascade;
alter table `mcq_question` add constraint `mcq_question_question_id_foreign` foreign key (`question_id`) references `question` (`id`) on delete cascade on update cascade;




CREATE TABLE `mcq_question_translation` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `question_id` bigint(20)unsigned  NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL
) ;

alter table `mcq_question_translation` add constraint `mcq_question_translation_question_id_foreign` foreign key (`question_id`) references `mcq_question` (`id`) on delete cascade on update cascade;


CREATE TABLE `drag_question` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `question_id`bigint(20) UNSIGNED NOT NULL
) ;
alter table `drag_question` add constraint `drag_question_question_id_foreign` foreign key (`question_id`) references `question` (`id`) on delete cascade on update cascade;

CREATE TABLE `drag_choice` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NULL,
  `drag_question_id`bigint(20) UNSIGNED NOT NULL
) ;

alter table `drag_choice` add constraint `drag_question_drag_question_id_foreign` foreign key (`drag_question_id`) references `drag_question` (`id`) on delete cascade on update cascade;

CREATE TABLE `drag_choice_translation` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `drag_choice_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_answer` VARCHAR(255) NOT NULL,
  `explanation` VARCHAR(255) NOT NULL
) ;
alter table `drag_choice_translation` add constraint `drag_choice_translation_drag_choice_id_foreign` foreign key (`drag_choice_id`) references `drag_choice` (`id`) on delete cascade on update cascade;


CREATE TABLE `drag_cell` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `drag_question_id`bigint(20) UNSIGNED NOT NULL,
  `show_answer` BOOLEAN NOT NULL DEFAULT FALSE,
  `order` int(11) NULL,
  `drag_choice_id`bigint(20) UNSIGNED NOT NULL
) ;
alter table `drag_cell` add constraint `drag_cell_drag_question_id_foreign` foreign key (`drag_question_id`) references `drag_question` (`id`) on delete cascade on update cascade;

alter table `drag_cell` add constraint `drag_cell_drag_choice_id_foreign` foreign key (`drag_choice_id`) references `drag_choice` (`id`) on delete cascade on update cascade;


CREATE TABLE `dropdown_question` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `question_id`bigint(20) UNSIGNED NOT NULL
);
alter table `dropdown_question` add constraint `dropdown_question_question_id_foreign` foreign key (`question_id`) references `question` (`id`) on delete cascade on update cascade;


CREATE TABLE `dropdown_question_translation` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `dropdown_question_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_description` VARCHAR(255) NOT NULL
) ;
alter table `dropdown_question_translation` add constraint `dropdown_question_translation_dropdown_question_id_foreign` foreign key (`dropdown_question_id`) references `dropdown_question` (`id`) on delete cascade on update cascade;


CREATE TABLE `dropdown` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `dropdown_question_id` bigint(20) UNSIGNED NOT NULL
) ;
alter table `dropdown` add constraint `dropdown_foreign` foreign key (`dropdown_question_id`) references `dropdown_question` (`id`) on delete cascade on update cascade;


CREATE TABLE `dropdown_choice` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `dropdown_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(11) NULL,
  `is_correct` BOOLEAN NOT NULL DEFAULT FALSE
) ;
alter table `dropdown_choice` add constraint `dropdown_choice_dropdown_id_foreign` foreign key (`dropdown_id`) references `dropdown` (`id`) on delete cascade on update cascade;


CREATE TABLE `dropdown_choice_translation` (
  `id` bigint(20)unsigned PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `dropdown_choice_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `choice` VARCHAR(255) NOT NULL,
  `explanation` VARCHAR(255) NOT NULL
) ;
alter table `dropdown_choice_translation` add constraint `dropdown_choice_translation_dropdown_choice_id_foreign` foreign key (`dropdown_choice_id`) references `dropdown_choice` (`id`) on delete cascade on update cascade;


CREATE TABLE `match_question` (
  `id` bigint(20)unsigned  PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `question_id`bigint(20) UNSIGNED NOT NULL
) ;
alter table `match_question` add constraint `match_question_question_id_foreign` foreign key (`question_id`) references `question` (`id`) on delete cascade on update cascade;


CREATE TABLE `match_value` (
  `id` bigint(20)unsigned  PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `match_question_id`bigint(20) UNSIGNED NOT NULL,
  `order` int(11) NULL
) ;
alter table `match_value` add constraint `match_value_match_question_id_foreign` foreign key (`match_question_id`) references `match_question` (`id`) on delete cascade on update cascade;


CREATE TABLE `match_value_translation` (
  `id` bigint(20)unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `match_value_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  `explanation` VARCHAR(255) NOT NULL
) ;
alter table `match_value_translation` add constraint `match_value_translation_match_value_id_foreign` foreign key (`match_value_id`) references `match_value` (`id`) on delete cascade on update cascade;


CREATE TABLE `match_key` (
  `id` bigint(20)unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `match_question_id`bigint(20) UNSIGNED NOT NULL,
  `order` int(11) NULL,
  `match_value_id`bigint(20) UNSIGNED NOT NULL
) ;
alter table `match_key` add constraint `match_key_match_question_id_foreign` foreign key (`match_question_id`) references `match_question` (`id`) on delete cascade on update cascade;

alter table `match_key` add constraint `match_key_match_value_id_foreign` foreign key (`match_value_id`) references `match_value` (`id`) on delete cascade on update cascade;


CREATE TABLE `match_key_translation` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `match_key_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` VARCHAR(255) NOT NULL
) ;
alter table `match_key_translation` add constraint `match_key_translation_match_key_id_foreign` foreign key (`match_key_id`) references `match_key` (`id`) on delete cascade on update cascade;
##########################################################################################################


ALTER TABLE `choice_translation` ADD `explnation` VARCHAR(255) NOT NULL;


ALTER TABLE `class` ADD `lessonType` bigint(20) UNSIGNED NOT NULL;
update `mission_html` set lesson_type="normal";
  
CREATE TABLE `campus_welcome_modules` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `class_id` bigint(20) UNSIGNED NOT NULL,
  `introductory_video_url` varchar(255) NOT NULL,
  `orientation_video_url` VARCHAR(255) NOT NULL
  
);
alter table `campus_welcome_modules` add constraint `campus_welcome_modules_class_id_foreign` foreign key (`class_id`) references `class` (`id`) on delete cascade on update cascade;

CREATE TABLE `campus_welcome_module_translation` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  
  `campus_welcome_module_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `introductory_text` VARCHAR(255) NOT NULL
) ;
alter table `campus_welcome_module_translation` add constraint `campus_welcome_translation_campus_welcome_module_id_foreign` foreign key (`campus_welcome_module_id`) references `campus_welcome_modules` (`id`) on delete cascade on update cascade;


CREATE TABLE `course_objectives` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus_id` bigint(20) UNSIGNED NOT NULL
);
alter table `course_objectives` add constraint `course_objectives_campus_id_foreign` foreign key (`campus_id`) references `campus` (`id`) on delete cascade on update cascade;


CREATE TABLE `course_objectives_translation` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `course_objectives_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `objective` VARCHAR(255) NOT NULL
) ;
alter table `course_objectives_translation` add constraint `course_objectives_translation_course_objectives_id_foreign` foreign key (`course_objectives_id`) references `course_objectives` (`id`) on delete cascade on update cascade;


CREATE TABLE `course_modules_objectives` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `class_id` bigint(20) UNSIGNED NOT NULL,
  `objective_id` bigint(20) UNSIGNED NOT NULL
);
alter table `course_modules_objectives` add constraint `course_modules_objectives_class_id_foreign` foreign key (`class_id`) references `class` (`id`) on delete cascade on update cascade;

alter table `course_modules_objectives` add constraint `course_modules_objectives_objective_id_foreign` foreign key (`objective_id`) references `course_objectives` (`id`) on delete cascade on update cascade;


ALTER TABLE `activity_task` 
ADD COLUMN `poster_task_id` bigint(20) UNSIGNED,
ADD COLUMN `isPoster` BOOLEAN NOT NULL DEFAULT 0 ,
ADD COLUMN `difficulty_id` int(11) UNSIGNED  NULL,
ADD COLUMN  `task_module_type` int(11) UNSIGNED DEFAULT NULL; #Overview or Summary or normal
                                 
alter table `activity_task` add constraint `activity_task_difficulty_id_foreign` foreign key (`difficulty_id`) references `difficulty` (`id`) on delete cascade on update cascade;                                 
alter table `activity_task` add constraint `activity_task_poster_task_id_foreign` foreign key (`poster_task_id`) references `task` (`id`) on delete cascade on update cascade;                                 

ALTER TABLE `activity_task` ADD COLUMN `Poster_type` int(11) UNSIGNED  DEFAULT 0;
#################Section######################

CREATE TABLE `html_section` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_html_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(11)  NULL
);
alter table `html_section` add constraint `html_section_mission_id_foreign` foreign key (`mission_html_id`) references `mission_html` (`id`) on delete cascade on update cascade;

CREATE TABLE `html_section_translation` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `html_section_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` VARCHAR(255) NOT NULL
) ;
alter table `html_section_translation` add constraint `html_section_translation_html_section_id_foreign` foreign key (`html_section_id`) references `html_section` (`id`) on delete cascade on update cascade;

CREATE TABLE `html_section_step` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_html_step_id` bigint(20) UNSIGNED NOT NULL,
  `html_section_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(11)  NULL 
);
alter table `html_section_step` add constraint `html_section_mission_html_step_id_foreign` foreign key (`mission_html_step_id`) references `mission_html_steps` (`id`) on delete cascade on update cascade;
alter table `html_section_step` add constraint `html_section_step_html_section_id_foreign` foreign key (`html_section_id`) references `html_section` (`id`) on delete cascade on update cascade;



ALTER TABLE `mission_html` ADD COLUMN `lesson_type` varchar(100) 
                         , ADD COLUMN `with_steps` BINARY DEFAULT 0 ;

                    
#########SCRIPT#######################
INSERT INTO question_task (question_id, quiz_id)
SELECT id,quiz_id
FROM question
where question.quiz_id is not null ;

##pb

INSERT INTO mcq_question (id, created_at, updated_at,deleted_at,weight,main_image,initial_image,is_editor,editor_code,
  emulator_type_id,question_id) 
SELECT id, created_at, updated_at,deleted_at,weight,main_image,initial_image,is_editor,editor_code,emulator_type_id,id
 FROM question;

 ##pb
ALTER TABLE `choice` ADD `mcq_question_id` bigint(20) UNSIGNED NULL;
#alter table `choice` add constraint `choice_mcq_question_id_foreign` foreign key (`mcq_question_id`) references `mcq_question` (`id`) on delete cascade on update cascade;

UPDATE choice set mcq_question_id=question_id;
#alter table `choice` add constraint `choice_mcq_question_id_foreign` foreign key (`mcq_question_id`) references `mcq_question` (`id`) on delete cascade on update cascade;


Insert into question_task(question_id,editor_id)
SELECT distinct question.id,mission_editor.id FROM `mission_editor` inner join quiz on mission_editor.quiz_id=quiz.id
inner join question on question.quiz_id=quiz.id ;

update question set question_type="question";

#########END SCRIPT#######################

############################################### BACKEND ################################################



CREATE TABLE `campus_activity_question` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus_activity_progresss_id`bigint(20) UNSIGNED NOT NULL,
  `question_id`bigint(20) UNSIGNED NOT NULL
);
alter table `campus_activity_question` add constraint `campus_activity_question_question_id_foreign` foreign key (`question_id`) references `question` (`id`) on delete cascade on update cascade;

alter table `campus_activity_question` add constraint `campus_activity_question_campus_activity_progresss_id_foreign` foreign key (`campus_activity_progresss_id`) references `campus_activity_progress` (`id`) on delete cascade on update cascade;


CREATE TABLE `mcq_question_answers` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus_activity_question_id` bigint(20) UNSIGNED NOT NULL,
  `choice_id`bigint(20) UNSIGNED NOT NULL,
  `confidence` VARCHAR(255) NOT NULL
);
alter table `mcq_question_answers` add constraint `mcq_question_answers_campus_activity_question_id_foreign` foreign key (`campus_activity_question_id`) references `campus_activity_question` (`id`) on delete cascade on update cascade;
alter table `mcq_question_answers` add constraint `mcq_question_answers_choice_id_foreign` foreign key (`choice_id`) references `choice` (`id`) on delete cascade on update cascade;


CREATE TABLE `drag_question_answers` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus_activity_question_id`bigint(20) UNSIGNED NOT NULL,
  `drag_cell_id`bigint(20) UNSIGNED NOT NULL,
  `drag_choice_id`bigint(20) UNSIGNED NOT NULL
);
alter table `drag_question_answers` add constraint `drag_question_answers_campus_activity_question_id_foreign` foreign key (`campus_activity_question_id`) references `campus_activity_question` (`id`) on delete cascade on update cascade;
alter table `drag_question_answers` add constraint `drag_question_answers_drag_cell_id_foreign` foreign key (`drag_cell_id`) references `drag_cell` (`id`) on delete cascade on update cascade;
alter table `drag_question_answers` add constraint `drag_question_answers_drag_choice_id_foreign` foreign key (`drag_choice_id`) references `drag_choice` (`id`) on delete cascade on update cascade;


CREATE TABLE `match_question_answers` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus_activity_question_id`bigint(20) UNSIGNED NOT NULL,
  `value_id`bigint(20) UNSIGNED NOT NULL,
  `key_id`bigint(20) UNSIGNED NOT NULL
);
alter table `match_question_answers` add constraint `match_question_answers_campus_activity_question_id_foreign` foreign key (`campus_activity_question_id`) references `campus_activity_question` (`id`) on delete cascade on update cascade;
alter table `match_question_answers` add constraint `match_question_answers_value_id_foreign` foreign key (`value_id`) references `match_value` (`id`) on delete cascade on update cascade;
alter table `match_question_answers` add constraint `match_question_answers_key_id_foreign` foreign key (`key_id`) references `match_key` (`id`) on delete cascade on update cascade;


CREATE TABLE `dropdown_question_answers` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus_activity_question_id`bigint(20) UNSIGNED NOT NULL,
  `dropdown_id`bigint(20) UNSIGNED NOT NULL,
  `dropdown_choice_id`bigint(20) UNSIGNED NOT NULL
);
alter table `dropdown_question_answers` add constraint `dropdown_question_answers_campus_activity_question_id_foreign` foreign key (`campus_activity_question_id`) references `campus_activity_question` (`id`) on delete cascade on update cascade;
alter table `dropdown_question_answers` add constraint `dropdown_question_answers_dropdown_id_foreign` foreign key (`dropdown_id`) references `dropdown` (`id`) on delete cascade on update cascade;
alter table `dropdown_question_answers` add constraint `dropdown_question_answers_dropdown_choice_id_foreign` foreign key (`dropdown_choice_id`) references `dropdown_choice` (`id`) on delete cascade on update cascade;


ALTER TABLE `campus_user` ADD `hasSeen` ENUM('00','01','10','11') NOT NULL DEFAULT '00' AFTER `role_id`;


ALTER TABLE `help_center_article_campus` ADD `article_type` CHAR(50) NOT NULL AFTER `campus_id`;


CREATE TABLE `campus_user_streaks` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id`bigint(20) UNSIGNED NOT NULL,
  `date`bigint(20)  NOT NULL,
  `xp_gained`bigint(20)
);


alter table `campus_user_streaks` add constraint `user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;

ALTER TABLE `task_current_step` ADD `current_section` INT(11)  NULL DEFAULT '0' AFTER `current_step`;
ALTER TABLE `task_current_step` ADD `section_id` bigint(20) UNSIGNED NULL AFTER `current_section`;
Alter table `task_current_step` add constraint `section_id_foreign` foreign key (`section_id`) references `html_section` (`id`) on delete cascade on update cascade;

ALTER TABLE `choice_translation` CHANGE `explnation` `explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `match_value_translation` CHANGE `explanation` `explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `drag_choice_translation` CHANGE `explanation` `explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `dropdown_choice_translation` CHANGE `explanation` `explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;


ALTER TABLE `choice_translation` CHANGE `explnation` `explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

UPDATE `question` SET `question_type`='dropdown' WHERE `question_type` ='drop_down';
ALTER TABLE `campus_user` CHANGE `hasSeen` `hasSeen` BOOLEAN NOT NULL DEFAULT FALSE;



ALTER TABLE `user` ADD `showDayTip` BOOLEAN NOT NULL DEFAULT TRUE AFTER `user_lti_id`;


ALTER TABLE `help_center_article` ADD summary LONGTEXT;


ALTER TABLE task_current_step ADD section_id bigint(20) UNSIGNED NULL AFTER current_section;
Alter table task_current_step add constraint section_id_foreign foreign key (section_id) references html_section (id) on delete cascade on update cascade;

