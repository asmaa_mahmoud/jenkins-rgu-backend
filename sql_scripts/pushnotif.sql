CreatePushSubscriptionsTable: create table `push_subscriptions` (`id` int unsigned not null auto_increment primary key, `user_id` int unsigned not null, `endpoint` varchar(500) not null, `public_key` varchar(255) null, `auth_token` varchar(255) null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci
CreatePushSubscriptionsTable: alter table `push_subscriptions` add constraint `push_subscriptions_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete cascade
CreatePushSubscriptionsTable: alter table `push_subscriptions` add index `push_subscriptions_user_id_index`(`user_id`)
CreatePushSubscriptionsTable: alter table `push_subscriptions` add unique `push_subscriptions_endpoint_unique`(`endpoint`)
