START TRANSACTION ;
alter table `mission_html` add `practice_task_id` bigint unsigned null;
alter table `mission_html` add constraint `mission_html_practice_task_id_foreign` foreign key (`practice_task_id`) references `task` (`id`) on update cascade;
COMMIT ;