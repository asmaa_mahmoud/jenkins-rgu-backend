alter table `theme_translation` add `icon_description` longtext null;

# Fix Volcanic Isalns typo

UPDATE `theme_translation`
SET title = 'Volcanic Islands'
WHERE id = 3;

# English

UPDATE `theme_translation`
SET icon_description = 'Birthday world is a great world where the students will start their coding journey with a set of missions that will teach them different coding concepts. A great party needs a lot of preparation, and the missions involve getting things ready for Robo and his friends.'
WHERE id = 1;

UPDATE `theme_translation`
SET icon_description = 'RoboGarden''s  moon world introduces students to space, where they can experience everything from the calculations required to launch rockets to the challenge of maintaining the appropriate speed and weight ratio inside the space shuttle. Students will be introduced to the different tools and equipment that the astronauts use during their journeys.'
WHERE id = 2;

UPDATE `theme_translation`
SET icon_description = 'Volcanic world where the student will start their coding journey through a set of missions that will teach them different coding concepts. Robo leads an expedition to a volcanic island in a quest for hidden treasure. Expeditions are a lot of work.'
WHERE id = 3;

UPDATE `theme_translation`
SET icon_description = 'RoboGarden’s hero robot will start getting to know the Ice World by familiarizing himself with the field setup. Robo begins by planning his fishing and exploration trips. When he finds a group of scientists conducting experiments, he helps them out with his knowledge of mathematics. As Robo assists the scientists in their research, he will learn a lot about math, arrays, and sequence solving. He will also learn better methods of dividing work between himself and his friends.'
WHERE id = 4;

UPDATE `theme_translation`
SET icon_description = 'The Golden Mountain World where the student will start their coding journey through a set of missions that will teach different coding concepts. Robo plans for a paintball and a dance party, but there are poisonous snakes and preying wolves lurking in the woods.'
WHERE id = 5;

UPDATE `theme_translation`
SET icon_description = 'Winter World is built around a virtual Christmas celebration. Every year at Christmas time, a special lake becomes enchanted and the town’s people find beautiful ornaments that have magically appeared on its frozen surface. Picking ornaments up off the lake’s surface is a dangerous adventure. Robo decides to help the townspeople collect the enchanted ornaments to decorate the town’s Christmas tree. He will need you to guide him since the icy lake can be treacherous. Help Robo skate across the frozen surface to safely collect the ornaments.'
WHERE id = 6;

# Portuguese

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (1, 'pt', 'Birthday Party', 'O Mundo da Festa de Aniversário é o lugar onde o aluno iniciará sua Jornada de codificação através de um conjunto de missões que ensinarão diferentes conceitos de codificação. Uma grande festa exige uma série de preparativos e as missões se baseiam em preparar todos os detalhes para que Robô e seus amigos possam se divertir na festa.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (2, 'pt', 'Moon World', 'A Jornada para a Lua RoboGarden leva os alunos para o espaço, onde eles realizarão os cálculos necessários para o lançamento do foguete e serão desafiados a manter a velocidade e o peso adequado dentro do ônibus espacial. Os alunos também conhecerão as diferentes ferramentas e equipamentos que os astronautas utilizam em suas viagens.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (3, 'pt', 'Volcanic Islands', 'Na Jornada da Ilha Vulcânica os alunos aprenderão codificação através de um conjunto de missões que abordarão diferentes conceitos. O Robô está liderando uma expedição a uma Ilha Vulcânica em busca de tesouros perdidos, mas expedições são muito trabalhosas e ele precisa de ajuda.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (4, 'pt', 'Snowman', 'O Robô RoboGarden iniciará a sua Jornada na Terra do Gelo explorando o local e planejando suas viagens para pescar e conhecer a área. Quando ele encontrar um grupo de cientistas conduzindo experimentos,  os ajudará com seu conhecimento em cálculos matemáticos. Enquanto ele ajuda os cientistas em suas pesquisas vai aprender sobre matemática, arrays e lógica. Também vai aprender a dividir o trabalho entre os seus amigos.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (5, 'pt', 'Golden Mountain', 'Na Jornada da Montanha Dourada os alunos aprenderão codificação através de um conjunto de missões que abordarão diferentes conceitos. O Robô vai planejar um jogo de paintball e uma festa dançante, mas deve tomar cuidado com as serpentes venenosas e os lobos a espreita pela floresta.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (6, 'pt', 'Winter World', 'O Mundo de Inverno é construído em torno da celebração do Natal. Todo ano na época do Natal, esse lago congelado se torna encantado e o povo da cidade encontra bonitos enfeites de Natal aparecendo magicamente em sua superfície. O Robô decidiu ajudar coletando esses enfeites para decorar a árvore de Natal da cidade; mas coletar cada enfeite da superfície congelada do lago é uma Aventura perigosa e o Robô precisa da sua ajuda para guiá-lo. Ajude o Robô a patinar de maneira segura sobre o lago para coletar os enfeites.');

# Arabic

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (1, 'ar', 'Birthday Party', 'عالم عيد الميلاد هو عالم كبير حيث يبدأ الطلاب رحلتهم للبرمجة مع مجموعة من المهام التي ستعلمهم مفاهيم البرمجة المختلفة. يحتاج الحفل الكبير إلى الكثير من الاستعدادات، وتشمل المهام الحصول على أشياء جاهزة لروبو وأصدقائه.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (2, 'ar', 'Moon World', 'يقدم/يعرف عالم القمر في RoboGarden الطلاب إلى الفضاء، حيث يمكنهم تجربة كل شيء بدءً من الحسابات المطلوبة لإطلاق الصواريخ، إلى التحدي المتمثل في الحفاظ على السرعة المناسبة والوزن المناسب داخل مكوك الفضاء. سيتم تعريف الطلاب على الأدوات والمعدات المختلفة التي يستخدمها رواد الفضاء خلال رحلاتهم.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (3, 'ar', 'Volcanic Islands', 'العالم البركاني هو المكان الذي سيبدأ فيه الطالب رحلة البرمجة من خلال مجموعة من المهام التي ستعلمهم مفاهيم البرمجة مختلفة. يقود Robo رحلة استكشافية إلى جزيرة بركانية من أجل البحث عن كنز مخفي. الرحلات الاستكشافية بها الكثير من العمل.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (4, 'ar', 'Snowman', 'سيبدأ الروبوت بطل RoboGarden في التعرف على عالم الجليد Ice World من خلال التعرف على الإعداد الميداني. يبدأ روبو بتخطيط رحلاته للصيد والاستكشاف. عندما يجد مجموعة من العلماء يجرون التجارب، فإنه يساعدهم على معرفة علم الرياضيات. وبما أن Robo يساعد العلماء في أبحاثهم، فإنه سيتعلم الكثير عن الرياضيات والمصفوفات وحل المشكلات. كما سيتعلم أساليب أفضل لتقسيم العمل بينه وبين أصدقائه.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (5, 'ar', 'Golden Mountain', 'عالم الجبل الذهبي هو المكان الذي يبدأ فيه الطالب رحلة البرمجة من خلال مجموعة من المهام التي ستعلم مفاهيم البرمجة المختلفة. يخطط روبو لحفلة الألوان والرقص، ولكن هناك ثعابين سامة والذئاب المفترسة الكامنة في الغابة.');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (6, 'ar', 'Winter World', 'تم بناء عالم الشتاء حول احتفال افتراضي بعيد الميلاد. في كل عام في أعياد الكريسماس، تصبح البحيرة الخاصة مسحورة ويجد سكان المدينة زخارف جميلة ظهرت بشكل سحري على سطحها المتجمد. يعد جمع الزينة من على سطح البحيرة مغامرة خطيرة. يقرر روبو مساعدة سكان البلدة على جمع الزخارف الساحرة لتزيين شجرة عيد الميلاد في المدينة. سيحتاجك لتوجيهه لأن البحيرة الجليدية يمكن أن تكون خائنة. ساعد Robo في التزلج على سطح متجمد لجمع الزينة بأمان.');

# Chinese

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (1, 'zh', 'Birthday Party', '生日世界是一个伟大的世界，学生将通过一系列任务开始他们的编程之旅，教会他们不同的编程概念。一个好的聚会需要做很多准备，为Robo和他的朋友准备好各种聚会用品。');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (2, 'zh', 'Moon World', 'RoboGarden的月球世界向学生介绍太空，在那里他们可以体验从发射火箭所需的复杂计算到在航天飞机内保持适当的速度和重量比的挑战。学生将被介绍宇航员在旅途中使用的不同工具和设备。');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (3, 'zh', 'Volcanic Islands', '火山世界是学生通过一系列任务开始编程之旅的地方，这些任务将教会他们不同的编程概念。 Robo带领一支探险队前往火山岛寻求隐藏的宝藏。远征包含很多工作。');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (4, 'zh', 'Snowman', 'RoboGarden的英雄Robo将通过熟悉现场设置开始了解冰雪世界。Robo首先计划他的钓鱼和探索之旅。当他找到一组科学家进行实验时，他用他的数学知识帮助他们。当Robo协助科学家进行研究时，他将学习很多关于数学，数组和序列求解的知识。他还将学习更好的方法，将自己和朋友之间的工作分配好。');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (5, 'zh', 'Golden Mountain', '金山世界是学生将通过一系列任务开始编程之旅的地方，这些任务将教授不同的编程概念。 Robo计划举办彩弹和舞会，但是要注意潜伏在森林中的毒蛇和掠食狼。');

INSERT INTO `theme_translation` (`theme_id`,  `language_code`, `title` ,`icon_description`)
VALUES (6, 'zh', 'Winter World', '冬季世界围绕虚拟圣诞节庆祝活动而建。每年圣诞节期间，一个特殊的湖泊会变得非常迷人，镇上的人们会发现美丽的装饰品，它们在冰冻的表面上神奇地出现了。在湖面上收集装饰品是一次危险的冒险， Robo决定帮助市民收集装饰品来装饰小镇的圣诞树。他需要你来引导他，因为冰冷的湖泊可是很危险的。帮助Robo滑过冰冻的表面，安全地收集装饰品。');