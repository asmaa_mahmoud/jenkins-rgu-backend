START TRANSACTION ;
###LAST SEEN
create table `campus_activity_last_seen` (`id` bigint unsigned not null auto_increment primary key,
                                          `user_id` bigint unsigned not  null, `task_id` bigint unsigned not null, `campus_id` bigint unsigned not null,
                                          `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null,
                                          `last_seen` bigint not null,
                                          `created_at` timestamp null, `updated_at` timestamp null,`deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_campus_id_foreign` foreign key (`campus_id`) references `campus` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;

###CAMPUS ARTICLE

###TIME ESTIMATE

CREATE TABLE task_time_estimate(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  time INT,
  created_at DATETIME NULL ,
  updated_at DATETIME NULL ,
  deleted_at DATETIME NULL
);

ALTER TABLE class ADD
  time_estimate INT NULL ;

INSERT INTO task_time_estimate(name,time) VALUES ('quiz', 10);
INSERT INTO task_time_estimate(name,time) VALUES ('tutorial', 10);
INSERT INTO task_time_estimate(name,time) VALUES ('editor_mission', 20);
INSERT INTO task_time_estimate(name,time) VALUES ('mini_project', 60);
INSERT INTO task_time_estimate(name,time) VALUES ('emulator', 10);
INSERT INTO task_time_estimate(name,time) VALUES ('coding_mission', 20);
INSERT INTO task_time_estimate(name,time) VALUES ('mission', 15);
INSERT INTO task_time_estimate(name,time) VALUES ('html_mission', 30);


###TASK CURRENT STEP
CREATE TABLE task_current_step(
  id BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  user_id BIGINT(20) UNSIGNED,
  task_id BIGINT(20) UNSIGNED,
  activity_id BIGINT(20) UNSIGNED,
  default_activity_id BIGINT(20) UNSIGNED,
  round_id BIGINT(20) UNSIGNED,
  current_step INT DEFAULT 0,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,

  FOREIGN KEY (user_id) REFERENCES user(id),
  FOREIGN KEY (task_id) REFERENCES task(id),
  FOREIGN KEY (activity_id) REFERENCES activity(id),
  FOREIGN KEY (default_activity_id) REFERENCES default_activity(id),
  FOREIGN KEY (round_id) REFERENCES campus_round(id)
);

COMMIT ;



BEGIN ;

CREATE TABLE help_center_article_campus(
  id BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  article_id INT(11) ,
  campus_id BIGINT(20) UNSIGNED,
  created_at DATETIME NULL ,
  updated_at DATETIME NULL ,
  deleted_at DATETIME NULL ,

  FOREIGN KEY (article_id) REFERENCES help_center_article(id),
  FOREIGN KEY (campus_id) REFERENCES campus(id)
);






INSERT INTO `role_permission`(`role_id`, `permission_id`) VALUES ((SELECT `id` FROM `role` WHERE `name`='super_admin') , (SELECT `id` FROM `permission` WHERE `name`='get_AdminDashboard'));

INSERT INTO `role_permission`(`role_id`, `permission_id`) VALUES ((SELECT `id` FROM `role` WHERE `name`='super_admin') , (SELECT `id` FROM `permission` WHERE `name`='get_TeacherDashboard'));

COMMIT ;





