START TRANSACTION ;

/* Analysis Up Category */
CREATE TABLE analysis_up_category(
  id INT PRIMARY KEY AUTO_INCREMENT,
  iconUrl VARCHAR(255) NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL
);
CREATE TABLE analysis_up_category_translation(
  id INT PRIMARY KEY AUTO_INCREMENT,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  language_code VARCHAR(255),
  title VARCHAR(255),
  description VARCHAR(255),
  up_category_id INT
);




/* Analysis Category */
ALTER TABLE analysis_category ADD
  is_random TINYINT NULL;

ALTER TABLE analysis_category ADD
  up_category_id INT NULL;

ALTER TABLE analysis_category ADD
  order_in_up_category INT NULL;

ALTER TABLE analysis_category ADD
  iconUrl VARCHAR(255) NULL;

ALTER TABLE analysis_category ADD
  category_code VARCHAR(255) NULL;

ALTER TABLE analysis_category_translations ADD
  description VARCHAR(255) NULL;




/* Analysis Questions */
ALTER TABLE analysis_questions ADD
  is_multiple TINYINT NULL;

ALTER TABLE analysis_questions ADD
  visual_format VARCHAR(255) NULL;

ALTER TABLE analysis_questions ADD
  order_in_category INT NULL;




/* Analysis Answer */
ALTER TABLE analysis_answers ADD
  iconUrl VARCHAR(255) NULL;

COMMIT ;
