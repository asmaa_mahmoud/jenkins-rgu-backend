START TRANSACTION ;

ALTER TABLE `learning_path_activity` ADD `stage_image_url` TEXT NULL;
INSERT INTO `learning_path` (`id`, `created_at`, `updated_at`, `deleted_at`, `learning_path_type_id`, `name`, `description`) VALUES
  (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 'Game Development on Android', NULL),
  (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 'Prediction in Machine Learning', NULL),
  (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 'Front-End Web Development from University of Calgary', NULL),
  (4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 'Python for Image Processing', NULL),
  (5, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 'Javascript for Web Development', NULL),
  (6, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 'Introduction to Web Development', NULL);
INSERT INTO `learning_path_activity` (`created_at`, `updated_at`, `deleted_at`, `learning_path_id`, `default_activity_id`, `stage_number`, `stage_name`, `order`, `stage_image_url`) VALUES
  #Game Development on Android
  #stage 1
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 102, 1, 'Blocks for Beginners', 1, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 103, 1, 'Blocks for Beginners', 2, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 105, 1, 'Blocks for Beginners', 3, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 61, 1, 'Blocks for Beginners', 4, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 101, 1, 'Blocks for Beginners', 5, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 78, 1, 'Blocks for Beginners', 6, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 84, 1, 'Blocks for Beginners', 7, NULL),
  #stage 2
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 202, 2, 'Game Development using Programming Blocks', 1, NULL),
  #stage 3
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, 219, 3, 'Game Development on Android using MIT App Inventor', 1, NULL),

  #Prediction in Machine Learning
  #stage 1
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 147, 1, 'Python for Beginners', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 171, 1, 'Python for Beginners', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 166, 1, 'Python for Beginners', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 180, 1, 'Python for Beginners', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 187, 1, 'Python for Beginners', 5, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  #stage 2
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 126, 2, 'Introduction to Machine Learning', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 127, 2, 'Introduction to Machine Learning', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 128, 2, 'Introduction to Machine Learning', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 129, 2, 'Introduction to Machine Learning', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning.jpeg'),
  #stage 3
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, 199, 3, 'Predicting Annual Sales', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Project.jpeg'),

  #Front-End Web Development from University of Calgary
  #stage 1
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 146, 1, 'JavaScript for beginners', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 186, 1, 'JavaScript for beginners', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 178, 1, 'JavaScript for beginners', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 167, 1, 'JavaScript for beginners', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 169, 1, 'JavaScript for beginners', 5, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  #stage 2
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 148, 2, 'Front-End Web Development', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Course.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 150, 2, 'Front-End Web Development', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Course.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 153, 2, 'Front-End Web Development', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Course.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 155, 2, 'Front-End Web Development', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Course.jpeg'),
  #stage 3
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, 198, 3, 'Interactive Forms using JavaScript', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Project.jpeg'),

  #Python for Image Processing
  #stage 1
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 147, 1, 'Python for Beginners', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 171, 1, 'Python for Beginners', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 166, 1, 'Python for Beginners', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 180, 1, 'Python for Beginners', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 187, 1, 'Python for Beginners', 5, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/Machine+Learning+Tutorial.jpeg'),
  #stage 2
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 213, 2, 'Introduction to Image Processing', 1, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 214, 2, 'Introduction to Image Processing', 2, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 215, 2, 'Introduction to Image Processing', 3, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 216, 2, 'Introduction to Image Processing', 4, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 217, 2, 'Introduction to Image Processing', 5, NULL),
  #stage 3
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, 220, 3, 'Enhancing noisy/distorted image', 1, NULL),

  #Javascript for Web Development
  #stage 1
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 146, 1, 'JavaScript for Beginners', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 186, 1, 'JavaScript for Beginners', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 178, 1, 'JavaScript for Beginners', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 167, 1, 'JavaScript for Beginners', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 169, 1, 'JavaScript for Beginners', 5, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  #stage 2
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 148, 2, 'Introduction to Web Development using Node js', 1, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 203, 2, 'Introduction to Web Development using Node js', 2, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 204, 2, 'Introduction to Web Development using Node js', 3, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 205, 2, 'Introduction to Web Development using Node js', 4, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 206, 2, 'Introduction to Web Development using Node js', 5, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 207, 2, 'Introduction to Web Development using Node js', 6, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 210, 2, 'Introduction to Web Development using Node js', 7, NULL),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 211, 2, 'Introduction to Web Development using Node js', 8, NULL),
  #stage 3
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 5, 218, 3, 'Building Customer Feedback web page', 1, NULL),

  #Introduction to Web Development
  #stage 1
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 146, 1, 'JavaScript for Beginners', 1, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 186, 1, 'JavaScript for Beginners', 2, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 178, 1, 'JavaScript for Beginners', 3, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 167, 1, 'JavaScript for Beginners', 4, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 169, 1, 'JavaScript for Beginners', 5, 'https://s3-us-west-2.amazonaws.com/robogarden-new/Professional/University+of+Calgary+-+Tutorial.jpeg'),
  #stage 2
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 148, 2, 'Front-End Web Development', 1, NULL),
  #stage 3
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 6, 198, 3, 'Interactive Forms using JavaScript', 1, NULL);

COMMIT ;