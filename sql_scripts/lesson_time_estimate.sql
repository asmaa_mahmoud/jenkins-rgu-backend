START TRANSACTION ;

CREATE TABLE task_time_estimate(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  time INT,
  created_at DATETIME NULL ,
  updated_at DATETIME NULL ,
  deleted_at DATETIME NULL
);

ALTER TABLE class ADD
  time_estimate INT NULL ;

INSERT INTO task_time_estimate(name,time) VALUES ('quiz', 10);
INSERT INTO task_time_estimate(name,time) VALUES ('tutorial', 10);
INSERT INTO task_time_estimate(name,time) VALUES ('editor_mission', 20);
INSERT INTO task_time_estimate(name,time) VALUES ('mini_project', 60);
INSERT INTO task_time_estimate(name,time) VALUES ('emulator', 10);
INSERT INTO task_time_estimate(name,time) VALUES ('coding_mission', 20);
INSERT INTO task_time_estimate(name,time) VALUES ('mission', 15);
INSERT INTO task_time_estimate(name,time) VALUES ('html_mission', 30);

COMMIT ;