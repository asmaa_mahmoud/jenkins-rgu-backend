START TRANSACTION ;

CREATE TABLE help_center_article(
  id INT PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL ,
  body LONGTEXT NOT NULL ,
  iconUrl VARCHAR(255) NULL,
  isRecommended TINYINT NULL ,
  nLikes INT DEFAULT 0,
  nDislikes INT DEFAULT 0
);

CREATE TABLE help_center_topic(
  id INT PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL ,
  description TEXT NOT NULL ,
  iconUrl VARCHAR(255) NULL
);

CREATE TABLE help_center_tag(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL ,
  iconUrl VARCHAR(255) NULL
);

CREATE TABLE help_center_article_topic(
  id INT PRIMARY KEY AUTO_INCREMENT,
  article_id INT NOT NULL ,
  topic_id INT NOT NULL ,
  FOREIGN KEY (article_id) REFERENCES help_center_article(id) ON DELETE CASCADE,
  FOREIGN KEY (topic_id) REFERENCES help_center_topic(id) ON DELETE CASCADE
);

CREATE TABLE help_center_article_tag(
  id INT PRIMARY KEY AUTO_INCREMENT,
  article_id INT NOT NULL ,
  tag_id INT NOT NULL ,
  FOREIGN KEY (article_id) REFERENCES help_center_article(id) ON DELETE CASCADE,
  FOREIGN KEY (tag_id) REFERENCES help_center_tag(id) ON DELETE CASCADE
);

ALTER TABLE help_center_topic MODIFY COLUMN description TEXT NULL;

COMMIT ;
