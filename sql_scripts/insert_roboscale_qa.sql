START TRANSACTION ;

INSERT INTO `analysis_category` (`id`, `starting_question_id`) VALUES
(1, null),
(2, null),
(3, null);

INSERT INTO `analysis_category_translations` (`id`,`language_code`,`name`, `analysis_category_id`) VALUES
(1,'en', 'Programming Level',1 ),
(2,'en', 'Personal Interests',2 ),
(3,'en', 'Career Interests',3);


INSERT INTO `analysis_questions` (`id`,`analysis_category_id`) VALUES
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),

(8,2),
(9,2),
(10,2),
(11,2),
(12,2),

(13,3),
(14,3),
(15,3),
(16,3),
(17,3);


UPDATE `analysis_category` SET `starting_question_id` = 1 WHERE `id`=1;
UPDATE `analysis_category` SET `starting_question_id` = 8 WHERE `id`=2;
UPDATE `analysis_category` SET `starting_question_id` = 13 WHERE `id`=3;

INSERT INTO `analysis_questions_translations` (`id`,`language_code`,`question_description`, `question_id`) VALUES

(1,'en','Did you write code before?',1),
(2,'en','How many years have you been programming?',2),
(3,'en','Describe your connection and belonging to other developers?',3),
(4,'en','What is the size of your largest program?',4),
(5,'en','Did you contribute in open source projects before?',5),
(6,'en','Did you take any course related to programming and technologies?',6),
(7,'en','Did you see any programming syntax before?',7),

(8,'en','What do you think about future of Artificial Intelligent?',8),
(9,'en','What time do you wake up?',9),
(10,'en','How much time do you spend on computer?',10),
(11,'en','How often do you skip meals to be productive?',11),
(12,'en','Did you take any online course before?',12),

(13,'en','what subjects are you interested in?',13),
(14,'en','What is your educational attainment?',14),
(15,'en','What type of jobs do you prefer?',15),
(16,'en','What do you hope to be doing in the next 5 years?',16),
(17,'en','Are you a fan of technology and always search for new updates?',17);



INSERT INTO `analysis_answers` (`id`,`factor`,`deduction`,`question_id`,`next_question_id`) VALUES

(1,0,null ,1,2),/*yes to q1*/



(2,0,null,2,3),
(3,0.2,null,2,3),
(4,0.8,null ,2,3),

(5,0.2,null,3,4),
(6,0,null,3,4),
(7,-0.2,null,3,4),

(8,-0.3,null,4,5),
(9,0,null,4,5),
(10,0.1,null,4,5),
(11,0.5,null,4,5),

(12,0.8,null ,5,null),
(13,0,null,5,null),

(14,-9.5,null ,1,6),/*no to q1*/
(15,0.1,null ,6,7),
(16,0,null ,6,7),
(17,0.1,null ,7,null),
(18,0,null,7,null),


(19,0,null,8,9),
(20,0,null,8,9),
(21,0,null,8,9),

(22,0,null,9,10), 
(23,0,null,9,10), 
(24,0,null,9,10), 
(25,0,null,9,10), 
(26,0,null,9,10),

(27,-0.9,null,10,11),
(28,-0.7,null,10,11),
(29,-0.3,null,10,11),
(30,-0.2,null,10,11),
(31,-0.1,null,10,11),

(32,0,null,11,12),
(33,0,null,11,12),
(34,0,null,11,12),
(35,0,null,11,12),

(36,0,null,12,null),
(37,0,null,12,null),

(38,-0.9,null,13,14),
(39,-0.9,null,13,14),
(40,-0.2,null,13,14),
(41,-0.5,null,13,14),

(42,0,null,14,15),
(43,0,null,14,15),
(44,0,null,14,15),
(45,0,null,14,15),
(46,0,null,14,15),

(47,0,null,15,16),
(48,0,null,15,16),
(49,0,null,15,16),

(50,0,null,16,17),
(51,0,null,16,17),
(52,0,null,16,17),

(53,0,null,17,null),
(54,0,null,17,null),
(55,0,null,17,null);
 
/*answers tr*/
INSERT INTO `analysis_answers_translations` (`id`,`language_code`,`answer_description`,`answer_id`) VALUES

(1,'en','Yes' ,1),/*yes to q1*/



(2,'en','0 - 2 years' ,2),
(3,'en','3 - 5 years' ,3),
(4,'en','More than 5' ,4),

(5,'en','I feel a sense of kinship or connection to other developers' ,5),
(6,'en','I think of myself as competing with my peers' ,6),
(7,'en','I am not as good in programming as most of my peers',7),

(8,'en','<100 lines/statements',8),
(9,'en','>=100 lines and <1000 lines',9),
(10,'en','>=1000 lines and <10000 lines',10),
(11,'en','>= 10000 lines',11),

(12,'en','Yes' ,12),
(13,'en','No' ,13),

(14,'en','No' ,14),/*no to q1*/
(15,'en','Yes' ,15),
(16,'en','No' ,16),
(17,'en','Yes' ,17),
(18,'en','No' ,18),


(19,'en','I am excited about the possibilities more than worried about the dangers.',19),
(20,'en','I am worried about the dangers more than I am excited about the possibilities.',20),
(21,'en','I do not care about it, or I have not thought about it.',21),

(22,'en','Before 6:00 AM',22),
(23,'en','Between 6:01 AM and 8:00 AM',23),
(24,'en','Between 8:01 AM and 10:00 AM',24),
(25,'en','Between 10:01 AM and 12:00 PM',25),
(26,'en','After 12:01 PM',26),

(27,'en','Less than 1 hour',27),
(28,'en','1 - 4 hours',28),
(29,'en','5 - 8 hours',29),
(30,'en','9 - 12 hours',30),
(31,'en','Over 12 hours',31),

(32,'en','Never',32),
(33,'en','1 - 2 times per week',33),
(34,'en','3 - 4 times per week',34),
(35,'en','Daily or almost every day',35),

(36,'en','Yes',36),
(37,'en','No',37),

(38,'en','Mathematics or statistics',38),
(39,'en','A business discipline (ex. accounting, finance, marketing)',39),
(40,'en','Fine arts or performing arts (ex. graphic design, music, studio art)',40),
(41,'en','Design games',41),

(42,'en','I never completed any formal education',42),
(43,'en','Primary/elementary school',43),
(44,'en','Secondary school',44),
(45,'en','Some college/university study without earning a degree',45),
(46,'en','Degree of bachelor',46),

(47,'en','Full-time job',47),
(48,'en','Part-time jobs',48),
(49,'en','Freelancing jobs',49),

(50,'en','Working in a different or more specialized role than the one I am in now',50),
(51,'en','Working as a founder or co-founder of my own company',51),
(52,'en','Doing the same work',52),

(53,'en','Extremely fan',53),
(54,'en','Normal',54),
(55,'en','Not interested',55);

COMMIT ;