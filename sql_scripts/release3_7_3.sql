CREATE TABLE `confidence` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `value` int(10) NOT NULL
);


CREATE TABLE `confidence_translation` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `confidence_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` VARCHAR(255) NOT NULL
) ;

alter table `confidence_translation` add constraint `confidence_id_foreign` foreign key (`confidence_id`) references `confidence` (`id`) on delete cascade on update cascade;

INSERT INTO `confidence` (`id`, `created_at`, `updated_at`, `deleted_at`, `value`) VALUES
(1, NULL, NULL, NULL, -1),
(2, NULL, NULL, NULL, 0),
(3, NULL, NULL, NULL, 1);

INSERT INTO `confidence_translation` (`id`, `created_at`, `updated_at`, `deleted_at`, `confidence_id`, `language_code`, `name`) VALUES
(1, NULL, NULL, NULL, 1, 'en', 'Not Confident'),
(2, NULL, NULL, NULL, 2, 'en', 'Somewhat Confident'),
(3, NULL, NULL, NULL, 3, 'en', 'Very Confident');
