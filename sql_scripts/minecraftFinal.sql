create table `minecraft_mission_steps` (`id` bigint unsigned not null auto_increment primary key, `image_url` varchar(255) null, `mission_id` bigint unsigned not null, `order` int null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `minecraft_mission_steps` add constraint `minecraft_mission_steps_mission_id_foreign` foreign key (`mission_id`) references `mission` (`id`) on delete cascade on update cascade;
create table `minecraft_mission_step_translations` (`id` bigint unsigned not null auto_increment primary key, `title` varchar(255) not null, `description` text null, `language_code` varchar(255) null, `video_url` varchar(255) null, `mission_step_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `minecraft_mission_step_translations` add constraint `minecraft_mission_step_translations_mission_step_id_foreign` foreign key (`mission_step_id`) references `minecraft_mission_steps` (`id`) on delete cascade on update cascade;
alter table `default_activity` add `type` smallint not null default '1' comment '1 -> normal , 2 -> minecraft';
INSERT INTO `type` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`) VALUES (5, NULL, NULL, NULL, 'minecraft');
ALTER TABLE `mission` CHANGE `model_answer` `model_answer` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `mission` CHANGE `vpl_id` `vpl_id` BIGINT(20) NULL;
ALTER TABLE `mission` CHANGE `scene_name` `scene_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

INSERT INTO `block_category` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `order`, `default`) VALUES (87, NULL, NULL, NULL, 'Minecraft', NULL, '0');
INSERT INTO `block` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`) VALUES (262, NULL, NULL, NULL, 'minecraft_agent_move'), (263, NULL, NULL, NULL, 'minecraft_agent_turn'), (264, NULL, NULL, NULL, 'minecraft_destroy'), (265, NULL, NULL, NULL, 'minecraft_place'), (266, NULL, NULL, NULL, 'minecraft_collect'), (267, NULL, NULL, NULL, 'minecraft_destroy_and_collect'), (268, NULL, NULL, NULL, 'minecraft_detect'), (269, NULL, NULL, NULL, 'minecraft_inspect'), (270, NULL, NULL, NULL, 'minecraft_tptoplayer'), (271, NULL, NULL, NULL, 'minecraft_agent_move'), (272, NULL, NULL, NULL, 'minecraft_agent_turn'), (273, NULL, NULL, NULL, 'minecraft_destroy'), (274, NULL, NULL, NULL, 'minecraft_place'), (275, NULL, NULL, NULL, 'minecraft_collect'), (276, NULL, NULL, NULL, 'minecraft_destroy_and_collect'), (277, NULL, NULL, NULL, 'minecraft_detect'), (278, NULL, NULL, NULL, 'minecraft_inspect'), (279, NULL, NULL, NULL, 'minecraft_tptoplayer');
INSERT INTO `block_category_block` (`id`, `block_category_id`, `block_id`) VALUES (NULL, '87', '262'), (NULL, '87', '263'), (NULL, '87', '264'), (NULL, '87', '265'), (NULL, '87', '266'), (NULL, '87', '267'), (NULL, '87', '268'), (NULL, '87', '269'), (NULL, '87', '270'), (NULL, '87', '271'), (NULL, '87', '272'), (NULL, '87', '273'), (NULL, '87', '274'), (NULL, '87', '275'), (NULL, '87', '276'), (NULL, '87', '277'), (NULL, '87', '278'), (NULL, '87', '279');

INSERT INTO plan_unlockable (id, plan_history_id, unlockable_id, created_at, updated_at, deleted_at) VALUES (NULL, '23', '129', NULL, NULL, NULL), (NULL, '23', '130', NULL, NULL, NULL), (NULL, '24', '129', NULL, NULL, NULL), (NULL, '24', '130', NULL, NULL, NULL);

ALTER TABLE `activity_unlockable` ADD `default_activity_id` BIGINT UNSIGNED NULL AFTER `deleted_at`;
ALTER TABLE `activity_unlockable`
  ADD CONSTRAINT `activity_unlockable_default_activity_id_foreign` FOREIGN KEY (`default_activity_id`) REFERENCES `default_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `activity_unlockable` CHANGE `activity_id` `activity_id` BIGINT(20) UNSIGNED NULL;
ALTER TABLE `activity_unlockable` CHANGE `default_activity_id` `default_activity_id` BIGINT(20) UNSIGNED NULL;

ALTER TABLE `activity_translation` CHANGE `activity_id` `activity_id` BIGINT(20) UNSIGNED NULL;
ALTER TABLE `activity_task` CHANGE `activity_id` `activity_id` BIGINT(20) UNSIGNED NULL;

ALTER TABLE mission ADD `blocks_type` VARCHAR(255) NULL DEFAULT NULL AFTER `mission_state_id`;
ALTER TABLE mission ADD `roboinmind_video_url` VARCHAR(255) NULL DEFAULT NULL AFTER `blocks_type`;

ALTER TABLE `activity_progress` ADD `unlocked_by_coins` TINYINT(1) NOT NULL DEFAULT '0' AFTER `best_blocks_number`, ADD `default_activity_id` BIGINT UNSIGNED NULL AFTER `unlocked_by_coins`;
ALTER TABLE `activity_progress` ADD INDEX(`default_activity_id`);
ALTER TABLE `activity_progress` ADD FOREIGN KEY (`default_activity_id`) REFERENCES `default_activity`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `activity_progress` CHANGE `activity_id` `activity_id` BIGINT(20) UNSIGNED NULL;

