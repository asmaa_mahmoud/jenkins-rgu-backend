ALTER TABLE `product` 
    ADD `url` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `name`, 
    ADD `logo_image` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `url`, 
    ADD `side_image` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `logo_image`;

###################SUB MODULES######################3
ALTER TABLE `campus` ADD `has_sub_modules` TINYINT NOT NULL DEFAULT '0' AFTER `xps`;    

CREATE TABLE `sub_module` 
( `id` bigint unsigned not null auto_increment primary key , `class_id` BIGINT unsigned NOT NULL , `order` INT NOT NULL , `created_at` TIMESTAMP NULL ,
 `updated_at` TIMESTAMP NULL , `deleted_at` TIMESTAMP NULL ) ENGINE = InnoDB;
alter table `sub_module` add constraint `sub_module_class_id_foreign` foreign key (`class_id`)
references `class` (`id`) on delete cascade on update cascade;

CREATE TABLE `sub_module_translation` 
( `id` bigint unsigned not null auto_increment primary key , `sub_module_id` BIGINT unsigned NOT NULL ,  `language_code` varchar(255) not null, 
	`name` varchar(255) not null, `description` varchar(255) null, `created_at` TIMESTAMP NULL ,
 `updated_at` TIMESTAMP NULL , `deleted_at` TIMESTAMP NULL ) ENGINE = InnoDB;


alter table `sub_module_translation` add constraint `sub_module_translation_sub_module_id_foreign` foreign key (`sub_module_id`)
references `sub_module` (`id`) on delete cascade on update cascade;

CREATE TABLE `sub_module_task` 
( `id` bigint unsigned not null auto_increment primary key , `sub_module_id` BIGINT unsigned NOT NULL ,
	`task_id` BIGINT unsigned NOT NULL ,
 `order` INT NOT NULL , `created_at` TIMESTAMP NULL ,
 `updated_at` TIMESTAMP NULL , `deleted_at` TIMESTAMP NULL ) ENGINE = InnoDB;
alter table `sub_module_task` add constraint `sub_module_task_sub_module_id_foreign` foreign key (`sub_module_id`)
references `sub_module` (`id`) on delete cascade on update cascade;
alter table `sub_module_task` add constraint `sub_module_task_task_id_foreign` foreign key (`task_id`)
references `task` (`id`) on delete cascade on update cascade;


ALTER TABLE `activity_translation` ADD `course_module_objectives` LONGTEXT NULL AFTER `prerequisites`;
ALTER TABLE `activity_translation` ADD `module_objectives` LONGTEXT NULL AFTER `prerequisites`;

###################PDF Mission######################
CREATE TABLE `mission_html_pdf_render_translations`
( 
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT , 
    `mission_html_id` BIGINT(20) UNSIGNED NOT NULL , 
    `language_code` VARCHAR(255) NOT NULL , 
    `pdf_link` VARCHAR(255) NOT NULL , 
    `created_at` TIMESTAMP NULL , 
    `updated_at` TIMESTAMP NULL , 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;
ALTER TABLE `mission_html_pdf_render_translations` ADD CONSTRAINT `mission_html_pdf_render_translations_mission_html_id`
FOREIGN KEY (`mission_html_id`) REFERENCES `mission_html`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `mission_html` ADD `quiz_id` BIGINT(20) UNSIGNED NULL AFTER `task_id`;

CREATE TABLE `mission_html_type`
(
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL ,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;
INSERT INTO `mission_html_type` (`id`, `name`) VALUES (NULL, 'normal'), (NULL, 'handout');
ALTER TABLE `mission_html` ADD `mission_html_type_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `mission_html` ADD CONSTRAINT `mission_html_mission_html_type_fk1` FOREIGN KEY (`mission_html_type_id`) REFERENCES `mission_html_type`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

####################GIVE UP############################3
ALTER TABLE `campus_activity_progress` ADD `has_given_up` TINYINT NOT NULL DEFAULT '0' AFTER `user_code`;

ALTER TABLE `sub_module_translation` CHANGE `description` `description` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;