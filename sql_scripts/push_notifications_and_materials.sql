START TRANSACTION ;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


-- --------------------------------------------------------

--
-- Table structure for table `push_subscriptions`
--

CREATE TABLE `push_subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(10) UNSIGNED DEFAULT NULL,
  `endpoint` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `push_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`user_id`,`endpoint`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `push_subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;


-- --------------------------------------------------------

--
-- Table structure for table `material`
--
CREATE TABLE `material` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campus_round_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `material_campus_round_id_foreign` (`campus_round_class_id`);


ALTER TABLE `material`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

ALTER TABLE `material`
  ADD CONSTRAINT `material_campus_round_id_foreign` FOREIGN KEY (`campus_round_class_id`) REFERENCES `campus_round_class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

ALTER TABLE `room`
  ADD COLUMN `file_id` bigint(20) UNSIGNED DEFAULT NULL;


ALTER TABLE `room`
  ADD KEY `room_file_id_foreign` (`file_id`);


ALTER TABLE `room`
  ADD CONSTRAINT `room_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT ;
