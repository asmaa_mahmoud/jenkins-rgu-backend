START TRANSACTION ;
INSERT INTO `tags` (`id`, `created_at`, `updated_at`)
  VALUES (10, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ;

INSERT INTO `tag_translation` (`language_code`, `tag_id`, `name`, `created_at`, `updated_at`)
    VALUES ('en', 10, 'Professional', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

COMMIT ;