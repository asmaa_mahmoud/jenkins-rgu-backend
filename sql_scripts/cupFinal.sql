create table `parameters` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null) default character set utf8 collate utf8_unicode_ci;
create table `parameter_translations` (`id` int unsigned not null auto_increment primary key, `title` varchar(255) not null, `language_code` varchar(255) not null, `parameter_id` int unsigned not null) default character set utf8 collate utf8_unicode_ci;
alter table `parameter_translations` add constraint `parameter_translations_parameter_id_foreign` foreign key (`parameter_id`) references `parameters` (`id`) on delete cascade on update cascade;
create table `world_countries` (`id` int unsigned not null auto_increment primary key, `flag_image` varchar(255) null, `gid` int not null, `goals_score` int not null default '0', `games_won` int not null default '0', `participation` int not null default '0', `matches_played` int not null default '0', `points` int not null default '0', `rank` int not null default '0') default character set utf8 collate utf8_unicode_ci;
create table `countries_translations` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null, `language_code` varchar(255) not null, `country_id` int unsigned not null) default character set utf8 collate utf8_unicode_ci;
alter table `countries_translations` add constraint `countries_translations_country_id_foreign` foreign key (`country_id`) references `world_countries` (`id`) on delete cascade on update cascade;
create table `world_match` (`id` int unsigned not null auto_increment primary key, `first_country_id` int unsigned not null, `second_country_id` int unsigned not null, `match_time` timestamp not null, `round_no` smallint null, `winner` smallint null, `featured` tinyint(1) not null default '0', `win` int not null default '0', `draw` int not null default '0', `lose` int not null default '0', `first_win_image` varchar(255) NULL, `second_win_image` varchar(255) NULL, `draw_image` varchar(255) NULL, `default_image` varchar(255) NULL) default character set utf8 collate utf8_unicode_ci;
create table `user_predictions` (`id` int unsigned not null auto_increment primary key, `user_id` bigint unsigned not null, `match_id` int unsigned not null, `winner` smallint not null default '0', `prediction_counter` int null, `scored` tinyint(1) not null default '0', `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `user_predictions` add constraint `user_predictions_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `user_predictions` add constraint `user_predictions_match_id_foreign` foreign key (`match_id`) references `world_match` (`id`) on delete cascade on update cascade;
alter table `world_match` add constraint `world_match_first_country_id_foreign` foreign key (`first_country_id`) references `world_countries` (`id`) on delete cascade on update cascade;
alter table `world_match` add constraint `world_match_second_country_id_foreign` foreign key (`second_country_id`) references `world_countries` (`id`) on delete cascade on update cascade;
create table `world_user_details` (`id` bigint unsigned not null auto_increment primary key, `name` varchar(255) null, `user_id` bigint unsigned not null, `address` varchar(255) null, `phone` varchar(255) null, `image` varchar(255) null, `predictor` varchar(255) NULL) default character set utf8 collate utf8_unicode_ci;
alter table `world_user_details` add constraint `world_user_details_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
create table `world_user_score` (`id` bigint unsigned not null auto_increment primary key, `user_id` bigint unsigned not null, `score_one` bigint null, `final_score` bigint null) default character set utf8 collate utf8_unicode_ci;
alter table `world_user_score` add constraint `world_user_score_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;

create table `user_parameters` (`id` int unsigned not null auto_increment primary key, `user_id` bigint unsigned not null, `parameter_id` int unsigned not null, `weight` int not null) default character set utf8 collate utf8_unicode_ci;
alter table `user_parameters` add constraint `user_parameters_parameter_id_foreign` foreign key (`parameter_id`) references `parameters` (`id`) on delete cascade on update cascade;
alter table `user_parameters` add constraint `user_parameters_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;

INSERT INTO `world_countries` (`id`, `flag_image`, `gid`, `goals_score`, `games_won`, `participation`, `matches_played`, `points`, `rank`) VALUES
(1, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/argentina.png', 1986, 131, 42, 16, 77, 1254, 5),
(2, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/australia.png', 1742, 11, 2, 4, 13, 700, 40),
(3, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/belgium.png', 1933, 52, 14, 12, 41, 1346, 3),
(4, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/brazil.png', 2136, 221, 70, 20, 104, 1384, 2),
(5, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/colombia.png', 1928, 26, 7, 5, 18, 989, 16),
(6, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/costa_rica.png', 1750, 17, 5, 4, 15, 858, 25),
(7, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/croatia.png', 1853, 21, 7, 4, 16, 975, 18),
(8, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/denmark.png', 1856, 27, 8, 4, 16, 1054, 12),
(9, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/egypt.png', 1646, 3, 0, 2, 4, 636, 46),
(10, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/england.png', 1948, 79, 26, 14, 62, 1040, 13),
(11, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/france.png', 1987, 106, 28, 14, 59, 1166, 7),
(12, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/germany.png', 2077, 224, 66, 18, 106, 1544, 1),
(13, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/iceland.png', 1764, 0, 0, 0, 0, 930, 22),
(14, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/iran.png', 1789, 7, 3, 4, 12, 727, 36),
(15, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/japan.png', 1666, 14, 4, 5, 17, 528, 60),
(16, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/mexico.png', 1850, 57, 14, 15, 53, 1008, 15),
(17, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/morocco.png', 1733, 12, 2, 4, 13, 681, 42),
(18, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/nigeria.png', 1681, 20, 5, 5, 18, 635, 47),
(19, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/panama.png', 1659, 0, 0, 0, 0, 574, 55),
(20, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/peru.png', 1915, 19, 4, 4, 5, 1106, 11),
(21, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/poland.png', 1829, 44, 15, 7, 31, 1128, 10),
(22, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/portugal.png', 1970, 43, 13, 6, 26, 1306, 4),
(23, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/russia.png', 1678, 66, 17, 10, 40, 493, 66),
(24, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/saudi_arabia.png', 1591, 9, 2, 4, 13, 462, 67),
(25, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/senegal.png', 1735, 7, 2, 1, 5, 825, 28),
(26, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/serbia.png', 1777, 64, 17, 11, 43, 732, 35),
(27, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/south_korea.png', 1729, 31, 5, 9, 31, 520, 61),
(28, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/spain.png', 2044, 92, 29, 14, 59, 1162, 8),
(29, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/sweden.png', 1795, 74, 16, 11, 46, 889, 23),
(30, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/switzerland.png', 1890, 45, 11, 10, 33, 1179, 6),
(31, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/tunisia.png', 1657, 8, 4, 4, 12, 1012, 14),
(32, 'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/flags/uruguay.png', 1894, 80, 20, 12, 51, 976, 17);

INSERT INTO `countries_translations`(`id`, `name`, `language_code`, `country_id`) VALUES
(1, 'Argentina', 'en', 1),
(2, 'Australia', 'en', 2),
(3, 'Belgium', 'en', 3),
(4, 'Brazil', 'en', 4),
(5, 'Colombia', 'en', 5),
(6, 'Costa Rica', 'en', 6),
(7, 'Croatia', 'en', 7),
(8, 'Denmark', 'en', 8),
(9, 'Egypt', 'en', 9),
(10, 'England', 'en', 10),
(11, 'France', 'en', 11),
(12, 'Germany', 'en', 12),
(13, 'Iceland', 'en', 13),
(14, 'Iran', 'en', 14),
(15, 'Japan', 'en', 15),
(16, 'Mexico', 'en', 16),
(17, 'Morocco', 'en', 17),
(18, 'Nigeria', 'en', 18),
(19, 'Panama', 'en', 19),
(20, 'Peru', 'en', 20),
(21, 'Poland', 'en', 21),
(22, 'Portugal', 'en', 22),
(23, 'Russia', 'en', 23),
(24, 'Saudi Arabia', 'en', 24),
(25, 'Senegal', 'en', 25),
(26, 'Serbia', 'en', 26),
(27, 'South Korea', 'en', 27),
(28, 'Spain', 'en', 28),
(29, 'Sweden', 'en', 29),
(30, 'Switzerland', 'en', 30),
(31, 'Tunisia', 'en', 31),
(32, 'Uruguay', 'en', 32);

INSERT INTO `world_match` (`id`, `first_country_id`, `second_country_id`, `match_time`, `round_no`, `winner`, `featured`, `win`, `draw`, `lose`,
`first_win_image`, `second_win_image`, `draw_image`, `default_image`) VALUES

(1, '23', '24', '2018-06-14 15:00:00', '1', NULL, '1', 0, 0, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Russia-Russia-Saudia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Russia-Saudia-Saudia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Russia-Draw-Saudia.png',
NULL),

(2, '9', '32', '2018-06-15 12:00:00', '1', NULL, '0', 0, 0, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Egypt-Egypt.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Uruguay-Egypt.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Draw-Egypt.png',
NULL),

(3, '17', '14', '2018-06-15 15:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Iran-Morocco-Morocco.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Iran-Iran-Morocco.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Iran-Draw.Morocco.png',
NULL),

(4, '22', '28', '2018-06-15 18:00:00', '1', NULL, '1', 6, 13, 17,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Portugal-Portugal-Spain.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Portugal-Spain-Spain.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Portugal-Draw-Spain.png',
NULL),

(5, '11', '2', '2018-06-16 10:00:00', '1', NULL, '1', 1, 1, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/France-France-Australia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/France-Australia-Australia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/France-Draw-Australia.png',
NULL),

(6, '1', '13', '2018-06-16 13:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Argentina-Argentina-Iceland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Argentina-Iceland-Iceland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Argentina-Draw-Iceland.png',
NULL),

(7, '20', '8', '2018-06-16 16:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Peru-Peru-Denmark.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Peru-Denmark-Denmark.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Peru-Draw-Denmark.png',
NULL),

(8, '7', '18', '2018-06-16 19:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Croatia-Croatia-Nigeria.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Croatia-Nigeria-Nigeria.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Croatia-Draw-Nigeria.png',
NULL),

(9, '6', '26', '2018-06-17 12:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Costa-Rica-Costa-Rica-Serbia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Costa-Rica-Serbia-Serbia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Costa-Rica-Draw-Serbia.png',
NULL),

(10, '12', '16', '2018-06-17 15:00:00', '1', NULL, '1', 2, 3, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Germany-Germany-Mexico.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Germany-Mexico-Mexico.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Germany-Draw-Mexico.png',
NULL),

(11, '4', '30', '2018-06-17 18:00:00', '1', NULL, '0', 1, 1, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Brazil-Brazil-Switzerland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Brazil-Switzerland-Switzerland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Brazil-Draw-Switzerland.png',
NULL),

(12, '29', '27', '2018-06-18 12:00:00', '1', NULL, '0', 1, 2, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Sweden-Sweden-KoreaRepublic.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Sweden-KoreaRepublic-KoreaRepublic.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Sweden-Draw-KoreaRepublic.png',
NULL),

(13, '3', '19', '2018-06-18 15:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Belgium-Belgium-Panama.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Belgium-Panama-Panama.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Belgium-Draw-Panama.png',
NULL),

(14, '31', '10', '2018-06-18 18:00:00', '1', NULL, '1', 0, 1, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Tunisia-Tunisia-England.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Tunisia-England-England.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Tunisia-Draw-England.png',
NULL),

(15, '5', '15', '2018-06-19 12:00:00', '1', NULL, '0', 2, 1, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Colombia-Colombia-Japan.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Colombia-Japan-Japan.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Colombia-Draw-Japan.png',
NULL),

(16, '21', '25', '2018-06-19 15:00:00', '1', NULL, '1', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Poland-Poland-Senegal.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Poland-Senegal-Senegal.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Poland-Draw-Senegal.png',
NULL),

(17, '23', '9', '2018-06-19 18:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Russia-Russia-Egypt.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Russia-Egypt-Egypt.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Russia-Draw-Egypt.png',
NULL),

(18, '22', '17', '2018-06-20 12:00:00', '1', NULL, '0', 0, 0, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Portugal-Portugal-Morocco.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Portugal-Morocco-Morocco.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Portugal-Draw-Morocco.png',
NULL),

(19, '32', '24', '2018-06-20 15:00:00', '1', NULL, '0', 0, 1, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Uruguay-Saudi-Arabia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Saudi-Arabia-Saudi-Arabia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Draw-Saudi-Arabia.png',
NULL),

(20, '14', '28', '2018-06-20 18:00:00', '1', NULL, '1', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/IR-Iran-IR-lran-Spain.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/IR-Iran-Spain-Spain.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/IR-Iran-Draw-Spain.png',
NULL),

(21, '8', '2', '2018-06-21 12:00:00', '1', NULL, '0', 2, 0, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Denmark-Denmark-Australia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Denmark-Australia-Australia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Denmark-Draw-Australia.png',
NULL),

(22, '11', '20', '2018-06-21 15:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/France-France-Peru.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/France-Peru-Peru.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/France-Draw-Peru.png',
NULL),

(23, '1', '7', '2018-06-21 18:00:00', '1', NULL, '1', 2, 1, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Argentina-Arentina-Croatia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Argentina-Croatia-Croatia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Argentina-Draw-Croatia.png',
NULL),

(24, '4', '6', '2018-06-22 12:00:00', '1', NULL, '1', 6, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Brazil-Brazil-Costa-Rica.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Brazil-Costa-Rica-Costa-Rica.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Brazil-Draw-Costa-Rica.png',
NULL),

(25, '18', '13', '2018-06-22 15:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Nigeria-Nigeria-Iceland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Nigeria-Iceland-Iceland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Nigeria-Draw-Iceland.png',
NULL),

(26, '26', '30', '2018-06-22 18:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Serbia-Serbia-Switzerland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Serbia-Switzerland-Switzerland.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Serbia-Draw-Switzerland.png',
NULL),

(27, '3', '31', '2018-06-23 12:00:00', '1', NULL, '0', 1, 1, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Belgium-Belgium-Tunisia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Belgium-Tunisia-Tunisia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Belgium-Draw-Tunisia.png',
NULL),

(28, '27', '16', '2018-06-23 15:00:00', '1', NULL, '0', 2, 2, 2,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Korea-Republic-Korea-Republic-Mexico.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Korea-Republic-Mexico-Mexico.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Korea-Republic-Draw-Mexico.png',
NULL),

(29, '12', '29', '2018-06-23 18:00:00', '1', NULL, '1', 10, 4, 8,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Germany-Germany-Sweden.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Germany-Sweden-Sweden.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Germany-Draw-Sweden.png',
NULL),

(30, '10', '19', '2018-06-24 12:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/England-England-Panama.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/England-Panama-Panama.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/England-Draw-Panama.png',
NULL),

(31, '15', '25', '2018-06-24 15:00:00', '1', NULL, '1', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Japan-Japan-Senegal.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Japan-Senegal-Senegal.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Japan-Draw-Senegal.png',
NULL),

(32, '21', '5', '2018-06-24 18:00:00', '1', NULL, '0', 0, 0, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Poland-Poland-Colombia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Poland-Colombia-Colombia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Poland-Draw-Colombia.png',
NULL),

(33, '24', '9', '2018-06-25 14:00:00', '1', NULL, '0', 1, 0, 2,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Saudi-Saudi-Egypt.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Saudi-Egypt-Egypt.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Saudi-Draw-Egypt.png',
NULL),

(34, '32', '23', '2018-06-25 14:00:00', '1', NULL, '0', 0, 1, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Uruguay-Russia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Russia-Russia.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Uruguay-Draw-Russia.png',
NULL),

(35, '14', '22', '2018-06-25 18:00:00', '1', NULL, '1', 0, 0, 1,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/IR-lran-IR-lran-Portugal.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/IR-lran-Portugal-Portugal.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/IR-lran-Drawl-Portugal.png',
NULL),

(36, '28', '17', '2018-06-25 18:00:00', '1', NULL, '0', 0, 0, 0,
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Spain-Spain-Morocco.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Spain-Morocco-Morocco.png',
'https://s3-us-west-2.amazonaws.com/robogarden-world-cup/Matches/Spain-Draw-Morocco.png',
NULL),

(37, '2', '20', '2018-06-26 14:00:00', '1', NULL, '0', 0, 0, 0, NULL, NULL, NULL, NULL),
(38, '8', '11', '2018-06-26 14:00:00', '1', NULL, '0', 4, 1, 8, NULL, NULL, NULL, NULL),
(39, '18', '1', '2018-06-26 18:00:00', '1', NULL, '1', 2, 1, 5, NULL, NULL, NULL, NULL),
(40, '13', '7', '2018-06-26 18:00:00', '1', NULL, '0', 1, 1, 4, NULL, NULL, NULL, NULL),
(41, '27', '12', '2018-06-27 14:00:00', '1', NULL, '0', 1, 0, 2, NULL, NULL, NULL, NULL),
(42, '16', '29', '2018-06-27 14:00:00', '1', NULL, '1', 1, 1, 2, NULL, NULL, NULL, NULL),
(43, '30', '6', '2018-06-27 18:00:00', '1', NULL, '0', 1, 0, 1, NULL, NULL, NULL, NULL),
(44, '26', '4', '2018-06-27 18:00:00', '1', NULL, '0', 0, 0, 1, NULL, NULL, NULL, NULL),
(45, '25', '5', '2018-06-28 14:00:00', '1', NULL, '0', 0, 1, 0, NULL, NULL, NULL, NULL),
(46, '15', '21', '2018-06-28 14:00:00', '1', NULL, '0', 1, 0, 0, NULL, NULL, NULL, NULL),
(47, '10', '3', '2018-06-28 18:00:00', '1', NULL, '1', 2, 4, 0, NULL, NULL, NULL, NULL),
(48, '19', '31', '2018-06-28 18:00:00', '1', NULL, '0', 0, 0, 0, NULL, NULL, NULL, NULL);

INSERT INTO `parameters` (`id`, `name`) VALUES
(1, 'history'),
(2, 'games_win'),
(3, 'participation'),
(4, 'fifa_rank'),
(5, 'world_goals'),
(6, 'ratio_winning'),
(7, 'matches_played');

INSERT INTO `parameter_translations` (`id`, `title`, `language_code`, `parameter_id`) VALUES
(1, 'History of results between participating teams', 'en', '1'),
(2, 'Number of games won in world cup before', 'en', '2'),
(3, 'Number of participations in world cup before', 'en', '3'),
(4, 'FIFA rank', 'en', '4'),
(5, 'Number of goals scored in world cup before', 'en', '5'),
(6, 'Ratio of winning in world cup', 'en', '6'),
(7, 'Number of matches played in world cup before', 'en', '7'),
(8, 'Histórico dos resultados de partidas entre os times', 'pt', '1'),
(9, 'Número de partidas ganhas em Copas do Mundo anteriores', 'pt', '2'),
(10, 'Número de participações em Copas do Mundo anteriores', 'pt', '3'),
(11, 'Ranking da FIFA', 'pt', '4'),
(12, 'Número de gols marcados em Copas do Mundo anteriores', 'pt', '5'),
(13, 'Taxa de vitórias em Copas do Mundo', 'pt', '6'),
(14, 'Número de partidas disputadas em Copas do Mundo anteriores', 'pt', '7'),
(15, '参赛队之间的历史比赛战绩', 'zh', '1'),
(16, '在以前世界杯中赢得的比赛数量', 'zh', '2'),
(17, '以前参加世界杯的次数', 'zh', '3'),
(18, 'FIFA排名', 'zh', '4'),
(19, '在以前世界杯中的进球数', 'zh', '5'),
(20, '在以前世界杯中的取胜率', 'zh', '6'),
(21, '在以前世界杯中参加的比赛数量', 'zh', '7');
