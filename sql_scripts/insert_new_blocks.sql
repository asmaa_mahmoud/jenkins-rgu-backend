START TRANSACTION ;

#insert new blocks
INSERT INTO `block` (`name`, `created_at`) VALUES
  ('alert', CURRENT_TIMESTAMP);

#get inserted id
SET @alert_block = (SELECT MAX(id) FROM `block`); #LAST_INSERT_ID();

INSERT INTO `block` (`name`, `created_at`) VALUES
  ('getElementById', CURRENT_TIMESTAMP) ;

#get inserted id
SET @element_id_block = (SELECT MAX(id) FROM `block`);

#insert new category
INSERT INTO `block_category` (`name`, `order`, `default`, `created_at`) VALUES
  ('Web', 23, 1, CURRENT_TIMESTAMP) ;

#get inserted id
SET @category_id = (SELECT MAX(id) FROM `block_category`);

#insert block_category_block
INSERT INTO `block_category_block` (`block_category_id`, `block_id`) VALUES
  (@category_id , @alert_block),
  (@category_id, @element_id_block) ;

COMMIT ;