

CREATE TABLE `campus_user_status` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `campus_id` bigint(20) UNSIGNED NOT NULL,
  `rank_level_id` bigint(20) UNSIGNED NOT NULL,
  `xp` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `campus_user_status`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `campus_id` (`campus_id`),
  ADD KEY `rank_level_id` (`rank_level_id`);

ALTER TABLE `campus_user_status`
  ADD CONSTRAINT `campus_user_status_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campus_user_status_ibfk_2` FOREIGN KEY (`campus_id`) REFERENCES `campus` (`id`),
  ADD CONSTRAINT `campus_user_status_ibfk_3` FOREIGN KEY (`rank_level_id`) REFERENCES `rank_level` (`id`);



##########################################################################

-- ALTER TABLE `campus_user_status` CHANGE `hide_blocks` `hide_blocks` BOOLEAN NOT NULL DEFAULT TRUE;
ALTER TABLE `campus_user_status` ADD `hide_blocks` BOOLEAN NOT NULL DEFAULT TRUE AFTER `xp`;

ALTER TABLE `campus_user_status` ADD UNIQUE `unique_index`(`user_id`, `campus_id`);


INSERT INTO `campus_user_status` (user_id,campus_id,rank_level_id,xp)
SELECT DISTINCT cUsers.user_id , rounds.campus_id , 1 as rank_level_id , 0 as xp
FROM campus_user cUsers , campus_round rounds  
WHERE rounds.id = cUsers.campus_round_id and cUsers.role_id==11 ;

ALTER TABLE `campus_user_status` ADD `streak_xp` BIGINT(10) NOT NULL AFTER `xp`;
ALTER TABLE `campus_user_status` CHANGE `streak_xp` `streak_xp` BIGINT(10) NOT NULL DEFAULT '0';
ALTER TABLE `campus_user_status` CHANGE `xp` `xp` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `campus_user_status` CHANGE `hide_blocks` `hide_blocks` TINYINT(1) NOT NULL DEFAULT '0';


###########################################################################
################        SCORING AND LEVELING        #######################

-- Create table to hold all constants related to scoring

CREATE TABLE scoring_constants(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) UNIQUE ,
  value DOUBLE
);

INSERT INTO scoring_constants (name, value) VALUES ('fullmark_constant', 0.08);
INSERT INTO scoring_constants (name, value) VALUES ('time_constant', 0.3);
INSERT INTO scoring_constants (name, value) VALUES ('trials_constant', 0.5);
INSERT INTO scoring_constants (name, value) VALUES ('blocklyTrials_constant', 0.05);
INSERT INTO scoring_constants (name, value) VALUES ('pythonTrials_constant', 0.05);
INSERT INTO scoring_constants (name, value) VALUES ('defaultQuizQuestion_time', 30);
INSERT INTO scoring_constants (name, value) VALUES ('defaultQuizThreshold_xps', 4);
INSERT INTO scoring_constants (name, value) VALUES ('usedBlocks_score_high', 1);
INSERT INTO scoring_constants (name, value) VALUES ('usedBlocks_score_medium', 0.75);
INSERT INTO scoring_constants (name, value) VALUES ('usedBlocks_score_low', 0);


-- Edit confidence values according to the new scoring system [1/4 | 1/2 | 1]
-- First, let the value column hold decimal values instead of integers
ALTER TABLE confidence MODIFY value DOUBLE NOT NULL;

UPDATE confidence SET value = 0.25 WHERE id = 1;
UPDATE confidence SET value = 0.5 WHERE id = 2;
UPDATE confidence SET value = 1 WHERE id = 3;

##############################################################################################

ALTER TABLE `campus_round_class` ADD UNIQUE( `class_id`, `campus_round_id`);

ALTER TABLE `user_notifications` ADD `is_read` BOOLEAN NOT NULL DEFAULT FALSE AFTER `data`;


#################################################################################################

CREATE TABLE `mission_angular` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED  NULL,
  `weight` int(11) NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
);
alter table `mission_angular` add constraint `mission_angular_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
alter table `mission_angular` add constraint `mission_angular_quiz_id_foreign` foreign key (`quiz_id`) references `quiz` (`id`) on delete cascade on update cascade;

CREATE TABLE `mission_angular_translation` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_angular_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` Text COLLATE utf8_unicode_ci NOT NULL,
  `description_speech` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
);
alter table `mission_angular_translation` add constraint `mission_angular_translation_mission_angular_id_foreign` foreign key (`mission_angular_id`) references `mission_angular` (`id`) on delete cascade on update cascade;


CREATE TABLE `mission_angular_file` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_angular_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` Text COLLATE utf8_unicode_ci NOT NULL,
  `is_model` BOOLEAN NOT NULL DEFAULT 0,
  `model_answer` Text COLLATE utf8_unicode_ci NOT NULL
);
alter table `mission_angular_file` add constraint `mission_angular_file_mission_angular_id_foreign` foreign key (`mission_angular_id`) references `mission_angular` (`id`) on delete cascade on update cascade;


CREATE TABLE `mission_angular_screenshot` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_angular_id` bigint(20) UNSIGNED NOT NULL,
  `screenshot_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
);
alter table `mission_angular_screenshot` add constraint `mission_angular_screenshot_mission_angular_id_foreign` foreign key (`mission_angular_id`) references `mission_angular` (`id`) on delete cascade on update cascade;


CREATE TABLE `mission_angular_dependency` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_angular_id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
);
alter table `mission_angular_dependency` add constraint `mission_angular_dependency_mission_angular_id_foreign` foreign key (`mission_angular_id`) references `mission_angular` (`id`) on delete cascade on update cascade;



CREATE TABLE `mission_angular_user_answer` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `campus_activity_progress_id`bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` Text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
);
alter table `mission_angular_user_answer` add constraint `campus_activity_question_campus_activity_progress_id_foreign` foreign key (`campus_activity_progress_id`) references `campus_activity_progress` (`id`) on delete cascade on update cascade;

#################################XPS################################################3
ALTER TABLE `campus_activity_task` ADD `xp_lesson_1_blockly_1` INT(11) NULL DEFAULT NULL AFTER `campus_id`;
ALTER TABLE `campus_activity_task` ADD `xp_lesson_1_blockly_0` INT(11) NULL DEFAULT NULL AFTER `campus_id`;
ALTER TABLE `campus_activity_task` ADD `xp_lesson_0_blockly_1` INT(11) NULL DEFAULT NULL AFTER `campus_id`;
ALTER TABLE `campus_activity_task` ADD `xp_lesson_0_blockly_0` INT(11) NULL DEFAULT NULL AFTER `campus_id`;


ALTER TABLE `class` ADD `xps` INT(11) NULL DEFAULT NULL AFTER `lessonType`;


ALTER TABLE `campus` ADD `xps` INT(11) NULL DEFAULT NULL AFTER `no_of_classes`;
ALTER TABLE `campus` CHANGE `xps` `xps` INT(11) NULL DEFAULT '25000';
UPDATE `campus` SET `xps`=25000;
##############################################################Explanation#########################
CREATE TABLE `dropdown_translation` (
  `id` bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT  NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `dropdown_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `right_explanation` longtext  NULL,
  `wrong_explanation` longtext  NULL
);
alter table `dropdown_translation` add constraint `dropdown_translation_dropdown_id` foreign key (`dropdown_id`) references `dropdown` (`id`) on delete cascade on update cascade;

insert into dropdown_translation(dropdown_id,language_code,right_explanation,wrong_explanation)
SELECT dropdown.id,'en',explanation, explanation FROM `dropdown_choice_translation` INNER join dropdown_choice on dropdown_choice_translation.dropdown_choice_id= dropdown_choice.id 
INNER join dropdown on dropdown_choice.dropdown_id =dropdown.id;


ALTER TABLE `campus_user_streaks` ADD `campus_id` BIGINT(20) UNSIGNED NULL AFTER `xp_gained`;
ALTER TABLE `campus_user_streaks` ADD CONSTRAINT `campus_id_foreign` FOREIGN KEY (`campus_id`) REFERENCES `campus` (`id`);


ALTER TABLE `mission_html_steps_translation` ADD `right_explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `step_editor`;
ALTER TABLE `mission_html_steps_translation` ADD `wrong_explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `right_explanation`;
ALTER TABLE `choice_translation` ADD `wrong_explanation` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `explanation`;

UPDATE `choice_translation` SET `wrong_explanation`=explanation ;

CREATE TABLE `test` ( `id` BIGINT(20) NOT NULL , `explanation` LONGTEXT NOT NULL ) ENGINE = InnoDB;
insert into test (id,explanation)
select mission_html_steps.id,drag_choice_translation.explanation from mission_html_steps_translation 
INNER join mission_html_steps on mission_html_steps_translation.mission_html_steps_id=mission_html_steps.id 
INNER join question_task on mission_html_steps.id=question_task.html_step_id
INNER join question on question_task.question_id=question.id 
INNER join drag_question on question.id=drag_question.question_id 
INNER join drag_choice on drag_question.id=drag_choice.drag_question_id 
INNER join drag_choice_translation on drag_choice.id=drag_choice_translation.drag_choice_id ;


################

insert into test (id,explanation)
select mission_html_steps.id,match_value_translation.explanation from mission_html_steps_translation 
INNER join mission_html_steps on mission_html_steps_translation.mission_html_steps_id=mission_html_steps.id 
INNER join question_task on mission_html_steps.id=question_task.html_step_id
INNER join question on question_task.question_id=question.id 
INNER join match_question on question.id=match_question.question_id 
INNER join match_value on match_question.id=match_value.match_question_id 
INNER join match_value_translation on match_value.id=match_value_translation.match_value_id ;


update mission_html_steps_translation set right_explanation = (
  select test.explanation from test 
  where mission_html_steps_translation.mission_html_steps_id=test.id limit 1
);

update mission_html_steps_translation set wrong_explanation = right_explanation;


#######################
SELECT * FROM `match_value_translation` INNER join match_value on match_value.id =match_value_translation.match_value_id
where match_value.match_question_id=53

###########################################################################
######### Delete user stuck notification in mini project ##################
DELETE FROM user_notifications
WHERE user_notifications.type LIKE '%StudentStuck'
AND user_notifications.data LIKE '%"task_type":"mini_project"%';

###########################################################################
############ Add isPeriodStart column in campus_user_streaks table ########
ALTER TABLE `campus_user_streaks` ADD `isPeriodStart` TINYINT NOT NULL DEFAULT '0' AFTER `campus_id`;


####################################################################
######## Remove duplicates from help_center_article_topic ##########
CREATE TABLE temp_help_center_article_topic(
  article_id INT,
  topic_id INT);

INSERT INTO temp_help_center_article_topic (article_id, topic_id)
  SELECT DISTINCT help_center_article_topic.article_id, help_center_article_topic.topic_id
  FROM help_center_article_topic;

DELETE FROM help_center_article_topic;

INSERT INTO help_center_article_topic(article_id, topic_id)
  SELECT * FROM temp_help_center_article_topic;

DROP TABLE temp_help_center_article_topic;

############################################################################
####### Remove isPeriodStart column from campus_user_streaks table #########
####### And add latestStreakStart column in campus_user_status table #######
ALTER TABLE campus_user_streaks DROP COLUMN isPeriodStart;
ALTER TABLE campus_user_status ADD latestStreakStartId BIGINT(20) NULL;

ALTER TABLE `help_center_article_topic` ADD `article_order` INT(20) NOT NULL DEFAULT 0 AFTER `topic_id`;
ALTER TABLE `help_center_article_topic` ADD UNIQUE( `article_id`, `topic_id`);









#######################################
ALTER TABLE `material` CHANGE `file_link` `file_link` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `material` CHANGE `name` `name` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;


ALTER TABLE `campus_user_status` ADD   `streaks_lastseen` bigint(20) DEFAULT NULL AFTER `hide_blocks`;
ALTER TABLE `campus_user_status` ADD `course_seen` BOOLEAN NULL DEFAULT NULL AFTER `streaks_lastseen`;
######################################
ALTER TABLE `mission_editor` ADD `tests` LONGTEXT NULL AFTER `model_answer`;


###################### Search Vault ############################
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE `search_vault` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `campus_id` bigint(20) UNSIGNED NOT NULL,
  `activity_id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `section_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `search_vault`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campus_id` (`campus_id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `activity_id` (`activity_id`);
ALTER TABLE `search_vault` ADD FULLTEXT KEY `search_vault_fulltext_index` (`title`,`description`);

ALTER TABLE `search_vault`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;


ALTER TABLE `search_vault`
  ADD CONSTRAINT `search_vault_ibfk_1` FOREIGN KEY (`campus_id`) REFERENCES `campus` (`id`),
  ADD CONSTRAINT `search_vault_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `search_vault_ibfk_3` FOREIGN KEY (`section_id`) REFERENCES `html_section` (`id`),
  ADD CONSTRAINT `search_vault_ibfk_4` FOREIGN KEY (`activity_id`) REFERENCES `default_activity` (`id`);
COMMIT;




ALTER TABLE school ADD `product_id` bigint(20) DEFAULT 1 after `account_id`;
ALTER TABLE school ADD `host_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'robogarden.ca' AFTER `product_id`;

################### Product ext. ######################

CREATE TABLE IF NOT EXISTS `product` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL UNIQUE,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)

CREATE TABLE `admin-professional`.`campus_product` (
 `campus_id` BIGINT(20) NOT NULL ,
 `product_id` BIGINT(20) NOT NULL ,
 `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
 `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
 `deleted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ) ENGINE = InnoDB;
 
 ALTER TABLE `campus_product` CHANGE `campus_id` `campus_id` BIGINT(20) UNSIGNED NOT NULL;
ALTER TABLE `campus_product` CHANGE `product_id` `product_id` BIGINT(20) UNSIGNED NOT NULL;


ALTER TABLE `campus_product` ADD FOREIGN KEY (`campus_id`) REFERENCES `campus`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `campus_product` ADD FOREIGN KEY (`product_id`) REFERENCES `product`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;