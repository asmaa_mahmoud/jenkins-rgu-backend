create table `campus_activity_last_seen` (`id` bigint unsigned not null auto_increment primary key,
  `user_id` bigint unsigned not  null, `task_id` bigint unsigned not null, `campus_id` bigint unsigned not null,
  `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null,
  `last_seen` bigint not null,
  `created_at` timestamp null, `updated_at` timestamp null,`deleted_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_campus_id_foreign` foreign key (`campus_id`) references `campus` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `campus_activity_last_seen` add constraint `campus_activity_last_seen_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
