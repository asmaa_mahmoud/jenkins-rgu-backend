START TRANSACTION;
ALTER TABLE quiz_type MODIFY COLUMN id bigint unsigned auto_increment primary key;
ALTER TABLE choice MODIFY COLUMN id bigint unsigned auto_increment ;

INSERT INTO quiz_type (name) VALUES ('emulator');

ALTER TABLE question ADD COLUMN main_image varchar(255) NULL;
ALTER TABLE question ADD COLUMN initial_image varchar(255) NULL;
ALTER TABLE question ADD COLUMN is_editor SMALLINT DEFAULT 0 NOT NULL;
ALTER TABLE question ADD COLUMN editor_code longtext  NULL;


ALTER TABLE choice ADD COLUMN image varchar(255) NULL;

CREATE TABLE `emulator_type` ( `id`  bigint unsigned not null auto_increment primary key , `name` VARCHAR(255) NOT NULL ) ;


INSERT INTO `emulator_type` (`id`, `name`) VALUES (NULL, 'ipad'), (NULL, 'desktop'), (NULL, 'mobile');

ALTER TABLE question ADD COLUMN emulator_type_id BIGINT unsigned  NULL;
alter table `question` add constraint `question_emulator_type_id_foreign` foreign key (`emulator_type_id`) references `emulator_type` (`id`) on delete cascade on update cascade;

COMMIT;