START TRANSACTION ;

#CreateNotificationsTable:
create table `user_notifications` (`id` char(36) not null, `type` varchar(255) not null, `user_id` bigint unsigned not null, `data` text not null, `read_at` timestamp null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
#alter table `user_notifications` add index `user_notifications_user_id_index`(`user_id`);
alter table `user_notifications` add constraint `user_notifications_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `user_notifications` add primary key `notifications_id_primary`(`id`);

COMMIT ;
