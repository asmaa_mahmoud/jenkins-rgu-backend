ALTER TABLE  campus_translation ADD FULLTEXT campus_fulltext_index (name, description);
ALTER TABLE  activity_translation ADD FULLTEXT activity_fulltext_index (name, description);
ALTER TABLE `activity_task` CHANGE `poster_task_id` `booster_task_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `activity_task` CHANGE `Poster_type` `booster_type` INT(11) UNSIGNED NULL DEFAULT '0';


/* Ranks & Levels */

CREATE TABLE `rank` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
);

INSERT INTO `rank` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, NULL),
(2, NULL, NULL, NULL),
(3, NULL, NULL, NULL),
(4, NULL, NULL, NULL),
(5, NULL, NULL, NULL),
(6, NULL, NULL, NULL);


CREATE TABLE `rank_translation` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `rank_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` VARCHAR(255) NOT NULL
) ;

INSERT INTO `rank_translation` (`id`, `created_at`, `updated_at`, `deleted_at`, `rank_id`, `language_code`, `name`) VALUES
(1, NULL, NULL, NULL, 1, 'en', 'Beginner'),
(2, NULL, NULL, NULL, 2, 'en', 'Junior'),
(3, NULL, NULL, NULL, 3, 'en', 'Intermediate'),
(4, NULL, NULL, NULL, 4, 'en', 'Senior'),
(5, NULL, NULL, NULL, 5, 'en', 'Advanced'),
(6, NULL, NULL, NULL, 6, 'en', 'Champion');


CREATE TABLE `level` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `level` int(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
);

INSERT INTO `level` (`id`,`level`,`created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL);

CREATE TABLE `level_translation` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `level_id` bigint(20) UNSIGNED NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` VARCHAR(255) NOT NULL
) ;

INSERT INTO `level_translation` (`id`, `created_at`, `updated_at`, `deleted_at`, `level_id`, `language_code`, `name`) VALUES 
(NULL, NULL, NULL, NULL, '1', 'en', 'Level 1'),
(NULL, NULL, NULL, NULL, '2', 'en', 'Level 2'), 
(NULL, NULL, NULL, NULL, '3', 'en', 'Level 3'), 
(NULL, NULL, NULL, NULL, '4', 'en', 'Level 4');

CREATE TABLE `rank_level` (
  `id` bigint(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `rank_id` bigint(20) UNSIGNED NOT NULL,
  `level_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
);

INSERT INTO `rank_level` (`id`, `rank_id`, `level_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(2, 1, 2, NULL, NULL, NULL),
(3, 1, 3, NULL, NULL, NULL),
(4, 2, 1, NULL, NULL, NULL),
(5, 2, 2, NULL, NULL, NULL),
(6, 2, 3, NULL, NULL, NULL),
(7, 3, 1, NULL, NULL, NULL),
(8, 3, 2, NULL, NULL, NULL),
(9, 3, 3, NULL, NULL, NULL),
(10, 4, 1, NULL, NULL, NULL),
(11, 4, 2, NULL, NULL, NULL),
(12, 4, 3, NULL, NULL, NULL),
(13, 5, 1, NULL, NULL, NULL),
(14, 5, 2, NULL, NULL, NULL),
(15, 5, 3, NULL, NULL, NULL),
(16, 5, 4, NULL, NULL, NULL),
(17, 6, 1, NULL, NULL, NULL),
(18, 6, 2, NULL, NULL, NULL),
(19, 6, 3, NULL, NULL, NULL),
(20, 6, 4, NULL, NULL, NULL);

alter table `rank_translation` add constraint `rank_id_foreign` foreign key (`rank_id`) references `rank` (`id`) on delete cascade on update cascade;
alter table `level_translation` add constraint `level_id_foreign` foreign key (`level_id`) references `level` (`id`) on delete cascade on update cascade;
alter table `rank_level` add constraint `rank_level_id_foreign` foreign key (`rank_id`) references `rank` (`id`) on delete cascade on update cascade;
alter table `rank_level` add constraint `level_rank_id_foreign` foreign key (`level_id`) references `level` (`id`) on delete cascade on update cascade;
alter table `user_score` ADD `rank_level_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '1' AFTER `coins`;
alter table `user_score` add constraint `user_rank_level_id_foreign` foreign key (`rank_level_id`) references `rank_level` (`id`) on delete cascade on update cascade;

/* End Rank & Level */

SET GLOBAL log_bin_trust_function_creators=1;
DROP FUNCTION IF EXISTS fnStripTags;
DELIMITER |
CREATE FUNCTION fnStripTags( Dirty LONGTEXT )
RETURNS LONGTEXT
DETERMINISTIC 
BEGIN
  DECLARE iStart, iEnd, iLength int;
    WHILE Locate( '<', Dirty ) > 0 And Locate( '>', Dirty, Locate( '<', Dirty )) > 0 DO
      BEGIN
        SET iStart = Locate( '<', Dirty ), iEnd = Locate( '>', Dirty, Locate('<', Dirty ));
        SET iLength = ( iEnd - iStart) + 1;
        IF iLength > 0 THEN
          BEGIN
            SET Dirty = Insert( Dirty, iStart, iLength, '');
          END;
        END IF;
      END;
    END WHILE;
    RETURN Dirty;
END;


###############################################

ALTER TABLE mission_translation ADD FULLTEXT mission_fulltext_index (title , description);
ALTER TABLE mission_html_translation ADD FULLTEXT mission_html_fulltext_index (title);
ALTER TABLE quiz_translation ADD FULLTEXT quiz_fulltext_index (title);
ALTER TABLE mission_coding_translation ADD FULLTEXT mission_coding_fulltext_index (title , description);
ALTER TABLE mission_editor_translation ADD FULLTEXT mission_editor_fulltext_index (title , description);
ALTER TABLE mission_html_steps_translation ADD FULLTEXT steps_fulltext_index (step_editor , title);
ALTER TABLE mcq_question_translation ADD FULLTEXT mcq_fulltext_index (body);
ALTER TABLE choice_translation ADD FULLTEXT choice_fulltext_index (body);
ALTER TABLE drag_choice_translation ADD FULLTEXT drag_fulltext_index (cell_answer);
ALTER TABLE match_key_translation ADD FULLTEXT match_fulltext_index (`key`);
ALTER TABLE match_value_translation ADD FULLTEXT match_value_fulltext_index (value);
ALTER TABLE dropdown_question_translation ADD FULLTEXT dropdown_value_fulltext_index (title_description);
ALTER TABLE dropdown_choice_translation ADD FULLTEXT dropdown_choice_value_fulltext_index (choice);

###############################################
ALTER TABLE `user` ADD `first_login_time` BIGINT(20) NULL DEFAULT NULL AFTER `showDayTip`;


