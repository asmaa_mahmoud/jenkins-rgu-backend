


ALTER TABLE `task_current_step` ADD `campus_id`  BIGINT(20) UNSIGNED AFTER `default_activity_id`;
alter table `task_current_step` add constraint `task_current_step_campus_id_foreign` foreign key (`campus_id`)
references `campus` (`id`) on delete cascade on update cascade;

DELIMITER $$
CREATE PROCEDURE fixCurrentStep()
  BEGIN
    DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE task_current_step_id varchar(100) DEFAULT "";
    DECLARE campus_id_var varchar(100) DEFAULT "";

    -- declare cursor for employee email
    DEClARE task_current_step_cursor CURSOR FOR
      SELECT id FROM task_current_step ;

    -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER
    FOR NOT FOUND SET v_finished = 1;
    OPEN task_current_step_cursor;


    tasks_loop: LOOP
      FETCH task_current_step_cursor INTO task_current_step_id;
      IF v_finished = 1 THEN
        LEAVE tasks_loop;
      END IF;




      SET @roundId =(SELECT round_id from task_current_step where id=task_current_step_id);
      SET @campusId =(SELECT campus_id from campus_round where id=@roundId);
      SET campus_id_var =(SELECT campus_id from campus_round where id=@roundId);



      UPDATE `task_current_step` set campus_id=campus_id_var where id=task_current_step_id;
    END LOOP tasks_loop;
    CLOSE task_current_step_cursor;
  END$$
DELIMITER ;
CALL fixCurrentStep();
ALTER TABLE `task_current_step`  DROP FOREIGN KEY `task_current_step_ibfk_5`;
ALTER TABLE `task_current_step` DROP `round_id`;



ALTER TABLE `password_resets`
  DROP COLUMN id,
   ADD COLUMN id INT NOT NULL PRIMARY KEY AUTO_INCREMENT ;

alter TABLE user DROP INDEX username_unique;

ALTER TABLE user
ADD CONSTRAINT unique_ids
UNIQUE (username, deleted_at)


############################################################################################################

ALTER TABLE `campus_round` ADD round_name varchar(255);

UPDATE `campus_round` SET `round_name`=( SELECT name FROM campus_translation WHERE campus_translation.campus_id = campus_round.campus_id AND campus_translation.deleted_at is null AND campus_translation.language_code ='en' )


ALTER TABLE room_user  ADD CONSTRAINT unique_room_user UNIQUE (user_id, room_id);

