START TRANSACTION ;

#MissionEditor:
create table `mission_editor` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `task_id` bigint unsigned not null, `weight` int unsigned null, `icon_url` varchar(255) null, `type` varchar(255) not null default '["html", "css", "js"]', `html` longtext null, `css` longtext null, `js` longtext null) default character set utf8 collate utf8_unicode_ci;
#MissionEditor:
alter table `mission_editor` add constraint `mission_editor_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
#MissionCoding:
create table `mission_coding` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `task_id` bigint unsigned not null, `vpl_id` bigint unsigned not null, `programming_language_type_id` bigint unsigned not null, `weight` int unsigned null, `icon_url` varchar(255) null, `duration` int unsigned null, `code` longtext null) default character set utf8 collate utf8_unicode_ci;
alter table `mission_coding` add constraint `mission_coding_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
alter table `mission_coding` add constraint `mission_coding_programming_language_type_id_foreign` foreign key (`programming_language_type_id`) references `programming_language_type` (`id`) on update cascade;
#Resource:
create table `resource` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `path` varchar(255) not null, `is_url` tinyint(1) not null default '1') default character set utf8 collate utf8_unicode_ci;
#TaskResources:
create table `task_resource` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `task_id` bigint unsigned not null, `resource_id` bigint unsigned not null) default character set utf8 collate utf8_unicode_ci;
alter table `task_resource` add constraint `task_resource_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;
alter table `task_resource` add constraint `task_resource_resource_id_foreign` foreign key (`resource_id`) references `resource` (`id`) on delete cascade on update cascade;
#MissionEditorTranslations:
create table `mission_editor_translation` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `mission_editor_id` bigint unsigned not null, `language_code` varchar(255) not null, `title` varchar(255) not null, `description` varchar(255) null, `description_speech` varchar(255) null, `video_url` varchar(255) null) default character set utf8 collate utf8_unicode_ci;
alter table `mission_editor_translation` add constraint `mission_editor_translation_mission_editor_id_foreign` foreign key (`mission_editor_id`) references `mission_editor` (`id`) on delete cascade on update cascade;
#MissionCodingTranslations:
create table `mission_coding_translation` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `mission_coding_id` bigint unsigned not null, `language_code` varchar(255) not null, `title` varchar(255) not null, `description` varchar(255) null, `description_speech` varchar(255) null, `video_url` varchar(255) null) default character set utf8 collate utf8_unicode_ci;
alter table `mission_coding_translation` add constraint `mission_coding_translation_mission_coding_id_foreign` foreign key (`mission_coding_id`) references `mission_coding` (`id`) on delete cascade on update cascade;

COMMIT ;


START TRANSACTION;

CREATE TABLE `mission_coding_screenshot` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_id` bigint(20) UNSIGNED NOT NULL,
  `screenshot_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `mission_coding_screenshot`
--
ALTER TABLE `mission_coding_screenshot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mission_coding_screenshot_mission_coding_id_foreign` (`mission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mission_coding_screenshot`
--
ALTER TABLE `mission_coding_screenshot`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;


START TRANSACTION;

CREATE TABLE `mission_editor_screenshot` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mission_id` bigint(20) UNSIGNED NOT NULL,
  `screenshot_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `mission_editor_screenshot`
--
ALTER TABLE `mission_editor_screenshot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mission_editor_screenshot_mission_editor_id_foreign` (`mission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mission_editor_screenshot`
--
ALTER TABLE `mission_editor_screenshot`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `mission_coding` ADD `model_answer` LONGTEXT NULL;

ALTER TABLE `mission_editor` ADD `model_answer` LONGTEXT NULL;

COMMIT;