START TRANSACTION ;

#CreateTableLearningPathType:
create table `learning_path_type` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `name` varchar(255) not null, `no_of_stages` int unsigned not null) default character set utf8 collate utf8_unicode_ci;

#CreateTableLearningPath:
create table `learning_path` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `learning_path_type_id` bigint unsigned not null, `name` varchar(255) not null, `description` varchar(255) null) default character set utf8 collate utf8_unicode_ci;
alter table `learning_path` add constraint `learning_path_learning_path_type_id_foreign` foreign key (`learning_path_type_id`) references `learning_path_type` (`id`) on delete cascade on update cascade;

#CreateTableLearningPathUser:
create table `learning_path_user` (`id` bigint unsigned not null auto_increment primary key, `learning_path_id` bigint unsigned not null, `user_id` bigint unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8 collate utf8_unicode_ci;
alter table `learning_path_user` add constraint `learning_path_user_learning_path_id_foreign` foreign key (`learning_path_id`) references `learning_path` (`id`) on delete cascade on update cascade;
alter table `learning_path_user` add constraint `learning_path_user_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;


#CreateTableLearningPathActivity:
create table `learning_path_activity` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `learning_path_id` bigint unsigned not null, `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null, `stage_number` int unsigned not null, `stage_name` varchar(255) not null, `order` int null) default character set utf8 collate utf8_unicode_ci;
alter table `learning_path_activity` add constraint `learning_path_activity_learning_path_id_foreign` foreign key (`learning_path_id`) references `learning_path` (`id`) on delete cascade on update cascade;
alter table `learning_path_activity` add constraint `learning_path_activity_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `learning_path_activity` add constraint `learning_path_activity_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;

#CreateTableLearningPathActivityProgress:
create table `learning_path_activity_progress` (`id` bigint unsigned not null auto_increment primary key, `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, `user_id` bigint unsigned not null, `learning_path_id` bigint unsigned not null, `activity_id` bigint unsigned null, `default_activity_id` bigint unsigned null, `task_id` bigint unsigned not null, `no_of_blocks` int unsigned null, `best_blocks_number` int unsigned null, `task_duration` int unsigned null, `best_task_duration` int unsigned null, `score` int unsigned not null, `no_of_trials` int unsigned null, `first_success` int unsigned null) default character set utf8 collate utf8_unicode_ci;
alter table `learning_path_activity_progress` add constraint `learning_path_activity_progress_user_id_foreign` foreign key (`user_id`) references `user` (`id`) on delete cascade on update cascade;
alter table `learning_path_activity_progress` add constraint `learning_path_activity_progress_learning_path_id_foreign` foreign key (`learning_path_id`) references `learning_path` (`id`) on delete cascade on update cascade;
alter table `learning_path_activity_progress` add constraint `learning_path_activity_progress_activity_id_foreign` foreign key (`activity_id`) references `activity` (`id`) on delete cascade on update cascade;
alter table `learning_path_activity_progress` add constraint `learning_path_activity_progress_default_activity_id_foreign` foreign key (`default_activity_id`) references `default_activity` (`id`) on delete cascade on update cascade;
alter table `learning_path_activity_progress` add constraint `learning_path_activity_progress_task_id_foreign` foreign key (`task_id`) references `task` (`id`) on delete cascade on update cascade;

COMMIT ;