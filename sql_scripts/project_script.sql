ALTER TABLE `class` ADD `isProject` BOOLEAN NOT NULL DEFAULT FALSE AFTER `campus_id`;
CREATE TABLE `project` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `icon_url` VARCHAR(255) NULL , `video_url` VARCHAR(255) NULL , `task_id` BIGINT(20) UNSIGNED NOT NULL , `created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null,PRIMARY KEY (`id`)) default character set utf8 collate utf8_unicode_ci;
ALTER TABLE `project` ADD CONSTRAINT `project_task_foreign_key` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE TABLE `project_translations` ( `id` INT NOT NULL AUTO_INCREMENT , `language_code` VARCHAR(255) NOT NULL , `title` VARCHAR(255) NOT NULL , `description` TEXT NOT NULL ,`project_id` INT UNSIGNED NOT NULL,`created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, PRIMARY KEY (`id`)) default character set utf8 collate utf8_unicode_ci;
ALTER TABLE `project_translations` ADD CONSTRAINT `project_translation_foreign_key` FOREIGN KEY (`project_id`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE TABLE `project_resources` ( `id` INT NOT NULL AUTO_INCREMENT , `title` VARCHAR(255) NOT NULL , `resource_url` VARCHAR(255) NOT NULL , `project_id` INT UNSIGNED NOT NULL ,`created_at` timestamp null, `updated_at` timestamp null, `deleted_at` timestamp null, PRIMARY KEY (`id`)) default character set utf8 collate utf8_unicode_ci;
ALTER TABLE `project_resources` ADD CONSTRAINT `project_resource_foreign_key` FOREIGN KEY (`project_id`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `project` ADD `class_id` BIGINT UNSIGNED NULL DEFAULT NULL AFTER `task_id`;
ALTER TABLE `project` ADD CONSTRAINT `project_class_foreign_key` FOREIGN KEY (`class_id`) REFERENCES `class`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
