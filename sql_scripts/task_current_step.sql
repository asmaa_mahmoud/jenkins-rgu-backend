BEGIN ;

CREATE TABLE task_current_step(
  id BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  user_id BIGINT(20) UNSIGNED,
  task_id BIGINT(20) UNSIGNED,
  activity_id BIGINT(20) UNSIGNED,
  default_activity_id BIGINT(20) UNSIGNED,
  round_id BIGINT(20) UNSIGNED,
  current_step INT DEFAULT 0,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,

  FOREIGN KEY (user_id) REFERENCES user(id),
  FOREIGN KEY (task_id) REFERENCES task(id),
  FOREIGN KEY (activity_id) REFERENCES activity(id),
  FOREIGN KEY (default_activity_id) REFERENCES default_activity(id),
  FOREIGN KEY (round_id) REFERENCES campus_round(id)
);

COMMIT ;