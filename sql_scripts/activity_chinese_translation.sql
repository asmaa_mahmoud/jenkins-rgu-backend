UPDATE `difficulty_translation`
SET `title` = '初级'
WHERE `difficulty_id` = 1 AND `language_code` = 'zh';

UPDATE `difficulty_translation`
SET `title` = '中级'
WHERE `difficulty_id` = 2 AND `language_code` = 'zh';