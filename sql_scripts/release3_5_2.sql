START TRANSACTION ;
CREATE TABLE temp(
    id INT
);
INSERT INTO temp
    SELECT max(id)
    FROM campus_activity_progress AS c
    GROUP BY c.user_id, c.task_id, c.campus_id, c.default_activity_id;

DELETE FROM campus_activity_progress WHERE id NOT IN (
    SELECT id
    FROM temp
);

DROP TABLE temp;

ALTER TABLE campus_activity_progress
ADD CONSTRAINT unique_ids
UNIQUE (user_id, task_id, campus_id, default_activity_id);
COMMIT ;

ALTER TABLE `submissions` ADD `category_code` VARCHAR(255) NULL AFTER `user_id`;
ALTER TABLE `submissions` ADD `visual_profile` LONGTEXT NULL AFTER `category_code`;