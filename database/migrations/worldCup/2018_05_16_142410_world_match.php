<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorldMatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('world_match', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('first_country_id')->unsigned();
            $table->integer('second_country_id')->unsigned();
            $table->timestamp('match_time')->nallable();
            $table->smallInteger('round_no')->nullable();
            $table->smallInteger('winner')->nullable();
            $table->boolean('featured')->default(0);
            $table->integer('win')->default(0);
            $table->integer('draw')->default(0);
            $table->integer('lose')->default(0);
            $table->foreign('first_country_id')
                ->references('id')->on('world_countries')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('second_country_id')
                ->references('id')->on('world_countries')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('world_match');
    }
}
