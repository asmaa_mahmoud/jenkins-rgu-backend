<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorldCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('world_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('flag_image')->nullable();
            $table->integer('goals_score')->default(0);
            $table->integer('games_won')->default(0);
            $table->integer('participation')->default(0);
            $table->integer('matches_played')->default(0);
            $table->integer('points')->default(0);
            $table->integer('rank')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('world_countries');
    }
}
