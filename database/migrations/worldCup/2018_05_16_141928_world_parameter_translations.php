<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorldParameterTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('language_code');
            $table->integer('parameter_id')->unsigned();
            $table->foreign('parameter_id')
                ->references('id')->on('parameters')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_translations');
    }
}
