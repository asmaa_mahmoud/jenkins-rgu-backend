<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPredictions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_predictions', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned();
            $table->integer('match_id')->unsigned();
            $table->integer('parameter_id')->unsigned();
            $table->integer('weight')->nallable();
            $table->smallInteger('winner')->default(0);
            $table->integer('prediction_counter')->nullable();
            $table->boolean('scored')->default(0);
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('match_id')
                ->references('id')->on('world_match')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parameter_id')
                ->references('id')->on('parameters')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries_translations');
    }
}
