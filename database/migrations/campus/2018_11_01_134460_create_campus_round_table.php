<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusRoundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::create('campus_round', function (Blueprint $table) {
            $table->bigIncrements('id');

             $table->bigInteger('campus_id')->unsigned();
            $table->foreign('campus_id')
                ->references('id')->on('campus')
                ->onDelete('cascade')->onUpdate('cascade');

                $table->integer('no_of_students')->unsigned()->nullable();


          $table->bigInteger('starts_at');
          $table->bigInteger('ends_at');
          
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
