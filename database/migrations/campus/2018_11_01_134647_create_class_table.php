<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        //
          Schema::create('class', function (Blueprint $table) {
            $table->bigIncrements('id');   

             $table->bigInteger('activity_id')->unsigned()->nullable();
            $table->foreign('activity_id')
                ->references('id')->on('activity')
                ->onDelete('cascade')->onUpdate('cascade');

             $table->bigInteger('default_activity_id')->unsigned()->nullable();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');

              

            $table->bigInteger('campus_id')->unsigned();
            $table->foreign('campus_id')
                ->references('id')->on('campus')
                ->onDelete('cascade')->onUpdate('cascade');

        $table->string('name');
        $table->text('description')->nullable();
         $table->integer('order')->unsigned();
           $table->string('icon_url')->nullable();
        
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('class');
    }
}
