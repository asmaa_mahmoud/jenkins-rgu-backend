<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolCampusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('school_campus', function (Blueprint $table) {
            $table->bigIncrements('id');     

            $table->bigInteger('school_id')->unsigned();
            $table->foreign('school_id')
                ->references('id')->on('school')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('campus_id')->unsigned();
            $table->foreign('campus_id')
                ->references('id')->on('campus')
                ->onDelete('cascade')->onUpdate('cascade');


           

            $table->integer('no_of_classes')->unsigned()->nullable();
            
             $table->string('creator_type')->nullable();


            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->boolean('locked')->default(0);


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('school_campus');
    }
}
