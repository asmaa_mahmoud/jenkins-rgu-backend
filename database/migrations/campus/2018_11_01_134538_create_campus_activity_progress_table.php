<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusActivityProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        //
          Schema::create('campus_activity_progress', function (Blueprint $table) {
            $table->bigIncrements('id');   

             $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');


             $table->bigInteger('task_id')->unsigned();
            $table->foreign('task_id')
                ->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');      

            $table->bigInteger('campus_id')->unsigned();
            $table->foreign('campus_id')
                ->references('id')->on('campus')
                ->onDelete('cascade')->onUpdate('cascade');

             $table->bigInteger('activity_id')->unsigned()->nullable();
            $table->foreign('activity_id')
                ->references('id')->on('activity')
                ->onDelete('cascade')->onUpdate('cascade');

             $table->bigInteger('default_activity_id')->unsigned()->nullable();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');

         $table->integer('no_of_blocks')->unsigned()->nullable();
         $table->integer('task_duration')->unsigned()->nullable();

         $table->integer('score')->unsigned();
         $table->integer('evaluation')->unsigned()->nullable();

         $table->integer('no_of_trials')->unsigned()->nullable();
         $table->integer('first_success')->unsigned()->nullable();
         $table->integer('best_task_duration')->unsigned()->nullable();
         $table->integer('best_blocks_number')->unsigned()->nullable();

         $table->boolean('unlocked_by_coins')->unsigned()->default(0);
         $table->boolean('is_evaluated')->unsigned()->default(0);

       
        $table->longText('user_code')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('campus_activity_progress');
    }
}
