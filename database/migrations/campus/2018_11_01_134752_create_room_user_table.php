<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        //
          Schema::create('room_user', function (Blueprint $table) {
            $table->bigIncrements('id');   

             $table->bigInteger('room_id')->unsigned();
            $table->foreign('room_id')
                ->references('id')->on('room')
                ->onDelete('cascade')->onUpdate('cascade');


            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('role_id')->unsigned();
            $table->foreign('role_id')
                ->references('id')->on('role')
                ->onDelete('cascade')->onUpdate('cascade');

          
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('room_user');
    }
}
