<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusRoundActivityTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        //
          Schema::create('campus_round_activity_task', function (Blueprint $table) {
            $table->bigIncrements('id');   

             
             $table->bigInteger('campus_activity_task_id')->unsigned();
            $table->foreign('campus_activity_task_id')
                ->references('id')->on('campus_activity_task')
                ->onDelete('cascade')->onUpdate('cascade');      

            $table->bigInteger('campus_round_id')->unsigned();
            $table->foreign('campus_round_id')
                ->references('id')->on('campus_round')
                ->onDelete('cascade')->onUpdate('cascade');

         

         $table->integer('weight')->unsigned();
         $table->integer('weight_overdue')->unsigned()->nullable();

          $table->bigInteger('due_date')->nullable();

        
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('campus_round_activity_task');
    }
}
