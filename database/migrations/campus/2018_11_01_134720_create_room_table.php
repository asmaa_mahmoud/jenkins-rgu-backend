<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('campus_round_class_id')->unsigned();
            $table->foreign('campus_round_id')
                ->references('id')->on('campus_round_class')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->string('bbb_id')->nullable();
            $table->bigInteger('starts_at')->unsigned();
            $table->bigInteger('ended_at')->unsigned()->nullable();
            $table->string('name');
            $table->boolean('is_running')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('room');
    }
}
