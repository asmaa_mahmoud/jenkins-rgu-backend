<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusRoundClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        //
          Schema::create('campus_round_class', function (Blueprint $table) {
            $table->bigIncrements('id');   

             $table->bigInteger('class_id')->unsigned();
            $table->foreign('class_id')
                ->references('id')->on('class')
                ->onDelete('cascade')->onUpdate('cascade');

          

            $table->bigInteger('campus_round_id')->unsigned();
            $table->foreign('campus_round_id')
                ->references('id')->on('campus_round')
                ->onDelete('cascade')->onUpdate('cascade');

        $table->integer('weight')->unsigned();
        
        $table->bigInteger('starts_at');
          $table->bigInteger('ends_at');
          $table->bigInteger('due_date')->nullable();
        
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('campus_round_class');
    }
}
