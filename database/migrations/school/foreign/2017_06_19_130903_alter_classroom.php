<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClassroom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classroom',function($table){
            $table->foreign('school_id')
                ->references('id')->on('school')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('grade_id')
                ->references('id')->on('grade')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classroom',function($table){
            $table->dropForeign(['school_id']);
            $table->dropForeign(['grade_id']);

        });
    }
}
