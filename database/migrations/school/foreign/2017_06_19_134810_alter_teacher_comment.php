<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTeacherComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('teacher_comment',function($table){
            $table->foreign('task_id')
                ->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('teacher_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('student_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('teacher_comment',function($table){
            $table->dropForeign(['task_id','teacher_id','student_id']);
        });
    }
}
