<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssignment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('assignment',function($table){
            $table->foreign('classroom_id')
                ->references('id')->on('classroom')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('teacher_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('assignment',function($table){
            $table->dropForeign(['classroom_id','teacher_id']);
        });
    }
}
