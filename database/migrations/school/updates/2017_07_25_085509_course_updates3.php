<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseUpdates3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group',function($table){
            $table->boolean('is_default')->default(0);
        });

        Schema::create('assignment_task', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('assignment_id')->unsigned();
            $table->foreign('assignment_id')->references('id')->on('assignment')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('adventure_id')->unsigned();
            $table->foreign('adventure_id')->references('id')->on('default_adventure')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('course')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('student_progress', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('no_trials')->default(0);
            $table->integer('score')->default(0);
            $table->integer('first_success')->nullable();
            $table->bigInteger('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('adventure_id')->unsigned();
            $table->foreign('adventure_id')->references('id')->on('default_adventure')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('course')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
