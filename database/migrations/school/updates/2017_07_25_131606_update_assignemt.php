<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssignemt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course',function($table){
            $table->dropColumn('period');
            $table->dateTime('ends_at')->nullable();
        });

        Schema::table('assignment',function($table){
            $table->dateTime('starts_at');
            $table->dateTime('ends_at');
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
