<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseAdventureDeadlines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('course_adventure_deadlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('adventure_id')->unsigned();

            $table->foreign('course_id')
                ->references('id')->on('course')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adventure_id')
                ->references('id')->on('default_adventure')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->dateTime('starts_at');
            $table->dateTime('ends_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('course_adventure_deadlines');
    }
}
