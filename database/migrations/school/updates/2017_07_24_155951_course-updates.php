<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course',function($table){
            $table->dropForeign(['classroom_id']);
            $table->dropColumn('classroom_id');
            $table->dateTime('starts_at');
            $table->integer('period');
            $table->boolean('is_default')->default(0);

            $table->bigInteger('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('journey_id')->unsigned();
            $table->foreign('journey_id')->references('id')->on('journey')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('group_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('group')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('assignment',function($table){
            $table->dropForeign(['classroom_id']);
            $table->dropColumn('classroom_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_user');
    }
}
