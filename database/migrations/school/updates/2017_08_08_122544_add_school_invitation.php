<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchoolInvitation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_invitation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("max_students")->nullable();
            $table->integer("max_courses")->nullable();
            $table->bigInteger('invitation_id')->unsigned();
            $table->foreign('invitation_id')
                ->references('id')->on('invitation')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_invitation');
    }
}
