<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_position', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('adventure_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('course_id')
                ->references('id')->on('course')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adventure_id')
                ->references('id')->on('default_adventure')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->integer('position_x');
            $table->integer('position_y');
            $table->integer('index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_position');
    }
}
