<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClassroomJourney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('classroom_journey', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('journey_id')->unsigned();
            $table->foreign('journey_id')->references('id')->on('journey')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('classroom_id')->unsigned();
            $table->foreign('classroom_id')->references('id')->on('classroom')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('classroom_journey');
    }
}
