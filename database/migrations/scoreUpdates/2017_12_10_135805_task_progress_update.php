<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskProgressUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_progress',function($table){
            $table->integer('first_success')->nullable();
            $table->boolean('unlocked_by_coins')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_progress',function($table){
            $table->dropColumn('unlocked_by_coins');
            $table->dropColumn('first_success');
        });
    }
}
