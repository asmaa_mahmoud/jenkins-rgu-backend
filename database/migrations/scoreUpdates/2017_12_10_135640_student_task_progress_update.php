<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentTaskProgressUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_progress',function($table){
            $table->integer('blocks_number')->nullable();
            $table->integer('task_duration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_progress',function($table){
            $table->dropColumn('blocks_number');
            $table->dropColumn('task_duration');
        });
    }
}
