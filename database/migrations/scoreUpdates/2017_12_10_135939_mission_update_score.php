<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MissionUpdateScore extends Migration
{

    public function up()
    {
        Schema::table('mission',function($table){
            $table->integer('golden_time')->nullable();
            $table->integer('coins_to_unlock')->nullable();
            $table->bigInteger('mission_state_id')->unsigned()->nullable();
            $table->foreign('mission_state_id')->references('id')->on('mission_state')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mission',function($table){
            $table->dropForeign(['mission_state_id']);
            $table->dropColumn('golden_time');
            $table->dropColumn('coins_to_unlock');
        });
    }
}
