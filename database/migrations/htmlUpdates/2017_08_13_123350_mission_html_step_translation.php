<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MissionHtmlStepTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mission_html_steps_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('video_url')->nullable();
            $table->string('image_url')->nullable();
            $table->text("description")->nullable();
            $table->text("title")->nullable();
            $table->string("language_code")->nullable();
            $table->bigInteger('mission_html_steps_id')->unsigned();
            $table->foreign('mission_html_steps_id')
                ->references('id')->on('mission_html_steps')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('mission_html_steps_translation');
    }
}
