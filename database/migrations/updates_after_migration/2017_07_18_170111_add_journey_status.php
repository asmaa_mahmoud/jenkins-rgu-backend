<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJourneyStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journey_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });
        Schema::table('journey',function($table){
            $table->bigInteger('journey_status_id')->unsigned()->nullable();
            $table->foreign('journey_status_id')
                ->references('id')->on('journey_status')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journey_status');
    }
}
