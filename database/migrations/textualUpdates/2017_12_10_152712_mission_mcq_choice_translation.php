<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MissionMcqChoiceTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_mcq_choice_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('choice_id')->unsigned();
            $table->string('language_code');
            $table->text('message');
            $table->foreign('choice_id')
                ->references('id')->on('mission_mcq_choices')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_mcq_choice_translation');

    }
}
