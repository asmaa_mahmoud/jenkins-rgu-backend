<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('plan_id')->unsigned();
            $table->string('new_name')->unique();
            $table->float('credit_per_month')->nullable();
            $table->float('credit_per_quarter')->nullable();
            $table->float('credit_per_year')->nullable();
            $table->bigInteger('account_type_id')->unsigned();
            $table->integer('number_of_journeys')->nullable();
            $table->date('trial_ends_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_history');
    }
}
