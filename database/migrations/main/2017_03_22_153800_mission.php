<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('weight')->nullable();
            $table->string('icon_url');
            $table->longText('model_answer');
            $table->string('hint')->nullable();
            $table->bigInteger('vpl_id');
            $table->string('scene_name');
            $table->integer('blocks_maximum_number')->nullable();
            $table->bigInteger('task_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission');
    }
}
