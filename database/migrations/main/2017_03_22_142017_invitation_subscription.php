<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvitationSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_subscription', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('invitation_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_subscription');
    }
}
