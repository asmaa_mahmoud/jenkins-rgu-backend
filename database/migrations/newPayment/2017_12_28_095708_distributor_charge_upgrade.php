<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DistributorChargeUpgrade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_charge_upgrade', function (Blueprint $table) {
            $table->increments('id');
            $table->float('cost')->nullable();
            $table->integer('no_of_students');
            $table->integer('no_of_classroom');
            $table->text('description')->nullable();
            $table->integer('distributor_charge_id')->unsigned();
            $table->foreign('distributor_charge_id')
                ->references('id')->on('distributor_charge')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_charge_upgrade');
    }
}
