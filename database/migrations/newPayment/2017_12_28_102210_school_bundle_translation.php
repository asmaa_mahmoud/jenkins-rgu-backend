<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolBundleTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_bundle_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('language_code');
            $table->text('description')->nullable();
            $table->integer('school_bundle_id')->unsigned();
            $table->foreign('school_bundle_id')
                ->references('id')->on('school_bundles')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_bundle_translation');
    }
}
