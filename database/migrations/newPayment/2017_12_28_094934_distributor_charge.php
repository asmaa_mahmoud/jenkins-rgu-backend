<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DistributorCharge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_charge', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cost')->nullable();
            $table->integer('no_of_students');
            $table->integer('no_of_classroom');
            $table->text('description')->nullable();
            $table->bigInteger('distributor_subscription_id')->unsigned();
            $table->foreign('distributor_subscription_id')
                ->references('id')->on('distributor_subscription')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_charge');
    }
}
