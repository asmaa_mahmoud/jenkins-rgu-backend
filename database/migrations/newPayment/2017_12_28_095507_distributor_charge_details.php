<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DistributorChargeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_charge_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_of_students');
            $table->integer('no_of_classroom');
            $table->integer('distributor_charge_id')->unsigned();
            $table->string('name')->nullable();
            $table->foreign('distributor_charge_id')
                ->references('id')->on('distributor_charge')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_charge_details');
    }
}
