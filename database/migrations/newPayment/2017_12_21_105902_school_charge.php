<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolCharge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_charge', function (Blueprint $table) {
            $table->increments('id');
            $table->string('charge_id');
            $table->integer('cost');
            $table->integer('no_of_students');
            $table->integer('no_of_classroom');
            $table->text('description')->nullable();
            $table->bigInteger('subscription_id')->unsigned();
            $table->foreign('subscription_id')
                ->references('id')->on('subscription')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->integer('bundle_id')->unsigned();
            $table->foreign('bundle_id')
                ->references('id')->on('school_bundles')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_charge');
    }
}
