<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountUnlockable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_unlockable', function (Blueprint $table) {
            $table->increments('id');
            $table->string('charge_id');
            $table->bigInteger('unlockable_id')->unsigned();
            $table->bigInteger('account_id')->unsigned();
            $table->integer('method_id')->unsigned();
            $table->foreign('unlockable_id')
                ->references('id')->on('unlockable')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('account_id')
                ->references('id')->on('account')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('method_id')
                ->references('id')->on('unlockable_method')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_unlockable');
    }
}
