<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtraTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('extra_id')->unsigned();
            $table->string('language_code');
            $table->text('description')->nullable();
            $table->foreign('extra_id')
                ->references('id')->on('extra')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_translation');
    }
}
