<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolChargesLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('school_charges_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cost')->nullable();
            $table->float('discount')->default(0);
            $table->float('tax')->default(0);
            $table->timestamp("from")->nullable();
            $table->timestamp("to")->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('school_id')->unsigned();
            $table->foreign('school_id')
                ->references('id')->on('school')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_charges_log');
    }
}
