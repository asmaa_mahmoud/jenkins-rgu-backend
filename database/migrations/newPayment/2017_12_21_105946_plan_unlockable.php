<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanUnlockable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_unlockable', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('plan_history_id')->unsigned();
            $table->bigInteger('unlockable_id')->unsigned();
            $table->foreign('plan_history_id')
                ->references('id')->on('plan_history')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('unlockable_id')
                ->references('id')->on('unlockable')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_unlockable');
    }
}
