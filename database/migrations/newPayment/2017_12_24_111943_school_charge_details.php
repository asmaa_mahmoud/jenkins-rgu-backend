<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolChargeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_charge_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_of_students');
            $table->integer('no_of_classroom');
            $table->integer('school_charge_id')->unsigned();
            $table->string('name')->nullable();
            $table->foreign('school_charge_id')
                ->references('id')->on('school_charge')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_charge_details');
    }
}
