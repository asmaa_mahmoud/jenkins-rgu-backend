<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtraUnlockable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_unlockable', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('extra_id')->unsigned();
            $table->bigInteger('unlockable_id')->unsigned();
            $table->foreign('extra_id')
                ->references('id')->on('extra')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('unlockable_id')
                ->references('id')->on('unlockable')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_unlockable');
    }
}
