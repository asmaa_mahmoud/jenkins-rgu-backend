<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Unlockable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unlockable', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('xp_to_unlock')->default(0);
            $table->float('cost_to_unlock')->default(0);
            $table->integer('coins_to_unlock')->default(0);
            $table->string('discount')->nullable();
            $table->boolean('is_plan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unlockable');
    }


}
