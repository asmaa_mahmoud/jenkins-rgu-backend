<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThemeJourney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theme_journey', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('theme_id')->unsigned();
            $table->bigInteger('journey_id')->unsigned();
            $table->foreign('theme_id')
                ->references('id')->on('theme')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('journey_id')
                ->references('id')->on('journey')
                ->onDelete('cascade')->onUpdate('cascade');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme_journey');
    }
}
