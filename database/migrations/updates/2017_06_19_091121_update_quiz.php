<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuiz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz',function($table){
            $table->bigInteger('quiz_type_id')->unsigned()->nullable();
            $table->foreign('quiz_type_id')
                ->references('id')->on('quiz_type')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz',function($table){
            $table->dropForeign(['quiz_type_id']);
            $table->dropColumn('quiz_type_id');
        });
    }
}
