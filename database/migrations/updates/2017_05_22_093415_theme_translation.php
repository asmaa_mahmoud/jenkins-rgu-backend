<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThemeTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theme_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('theme_id')->unsigned();
            $table->foreign('theme_id')
                ->references('id')->on('theme')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('language_code');
            $table->string('title')->nullable();
            $table->longText('body')->nullable();
            $table->integer('order')->nullable();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme_translation');
    }
}
