<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubscriptionItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('plan_history_id')->unsigned();
            $table->bigInteger('subscription_id')->unsigned();
            $table->string('stripe_id');
            $table->foreign('plan_history_id')
                ->references('id')->on('plan_history')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subscription_id')
                ->references('id')->on('subscription')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_item');
    }
}
