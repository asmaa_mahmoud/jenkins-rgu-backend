<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mission_concept');
        Schema::table('task',function($table){
            $table->dropColumn('position_x');
            $table->dropColumn('position_y');
        });
        Schema::table('task_order',function($table){
            $table->bigInteger('position_x')->nullable();
            $table->bigInteger('position_y')->nullable();
        });
        Schema::table('mission_translation',function($table){
            $table->longText('concept')->nullable();
        });
        // Schema::create('languages', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('name');
        //     $table->string('code');
        //     $table->boolean('is_active')->default(1);        
        // });
        Schema::create('scenes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('title');       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
        Schema::dropIfExists('scenes');
    }
}
