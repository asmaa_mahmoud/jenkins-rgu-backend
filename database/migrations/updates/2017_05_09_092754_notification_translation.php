<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications',function($table){
            $table->dropColumn('title');
            $table->dropColumn('message');
             $table->renameColumn('ends_at', 'starts_at');
        });

        Schema::create('notifications_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('notification_id')->unsigned();
            $table->foreign('notification_id')
                ->references('id')->on('notifications')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('language_code');
            $table->string('title')->nullable();
            $table->string('message')->nullable();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications_translation');
    }
}
