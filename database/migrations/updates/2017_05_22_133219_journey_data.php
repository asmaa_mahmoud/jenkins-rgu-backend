<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JourneyData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journey_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('journey_id')->unsigned();
            $table->foreign('journey_id')
                ->references('id')->on('journey')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('language_code');
            $table->string('grade')->nullable();
            $table->string('code_category')->nullable();
            $table->string('code_sub_category')->nullable();
            $table->string('code_grade')->nullable();
            $table->string('code_offered_journeys')->nullable();
            $table->string('meaning')->nullable();
            $table->string('shorten_title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('technical_details')->nullable();
            $table->longText('concepts')->nullable();
            $table->longText('prequisites')->nullable();
            $table->string('language')->nullable();
            $table->longText('content')->nullable();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journey_data');
    }
}
