<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFamilyInviation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_invitation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("max_children");
            $table->bigInteger('invitation_id')->unsigned();
            $table->foreign('invitation_id')
                ->references('id')->on('invitation')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_invitation');
    }
}
