<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FreeJourneysUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_journey', function (Blueprint $table) {
            $table->bigIncrements('id');
        });
        Schema::create('free_journey_task', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('free_journey_id')->unsigned()->nullable();
            $table->foreign('free_journey_id')
                ->references('id')->on('free_journey')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('task_id')->unsigned()->nullable();
            $table->foreign('task_id')
                ->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
