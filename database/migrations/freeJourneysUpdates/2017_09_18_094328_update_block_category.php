<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBlockCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('block',function($table){
            $table->dropColumn('block_category_id');
        });

        Schema::create('block_category_block', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('block_category_id')->unsigned();
            $table->foreign('block_category_id')
                ->references('id')->on('block_category')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('block_id')->unsigned();
            $table->foreign('block_id')
                ->references('id')->on('block')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
