<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampActivityPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camp_activity_price', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('activity_id')->nullable()->unsigned();
            $table->integer('period_id')->unsigned();
            $table->double('price');
            $table->foreign('period_id')
                ->references('id')->on('activity_period')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camp_activity_price');
    }
}
