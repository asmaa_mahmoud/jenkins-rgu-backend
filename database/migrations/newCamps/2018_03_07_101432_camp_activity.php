<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camp_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('activity_id')->unsigned();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->bigInteger('camp_id')->unsigned();
            $table->integer('order')->nullable();
            $table->string('creator_type')->nullable();
            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->foreign('activity_id')
                ->references('id')->on('activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('camp_id')
                ->references('id')->on('camp')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camp_activity');
    }
}
