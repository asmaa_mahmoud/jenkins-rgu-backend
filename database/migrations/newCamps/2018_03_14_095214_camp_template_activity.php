<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampTemplateActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camp_template_activity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('default_activity_id')->unsigned();
            $table->bigInteger('camp_template_id')->unsigned();
            $table->integer('order')->nullable();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('camp_template_id')
                ->references('id')->on('camp_template')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camp_template_activity');
    }
}
