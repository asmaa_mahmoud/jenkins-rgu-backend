<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampPeriodTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_period_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('language_code');
            $table->integer('period_id')->unsigned();
            $table->foreign('period_id')
                ->references('id')->on('activity_period')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_period_translation');
    }
}
