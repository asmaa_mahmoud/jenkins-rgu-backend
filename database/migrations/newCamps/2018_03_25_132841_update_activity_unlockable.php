<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActivityUnlockable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('activity_unlockable', function ($table) {
            $table->dropForeign(['activity_id']);
            $table->bigInteger('activity_id')->nullable()->unsigned()->change();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('activity_id')
                ->references('id')->on('activity')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
