<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampTemplateTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camp_template_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('language_code');
            $table->text('description')->nullable();
            $table->string('video_url')->nullable();
            $table->text('summary')->nullable();
            $table->text('concepts')->nullable();
            $table->text('technical_details')->nullable();
            $table->text('prerequisites')->nullable();
            $table->bigInteger('camp_template_id')->unsigned();
            $table->timestamps();
            $table->foreign('camp_template_id')
                ->references('id')->on('camp_template')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camp_template_translation');
    }
}
