<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityTranslationAdds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_translation', function ($table) {
            $table->string('video_url')->nullable();
            $table->text('summary')->nullable();
            $table->text('concepts')->nullable();
            $table->text('technical_details')->nullable();
            $table->text('prerequisites')->nullable();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
