<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolCamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_camp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('camp_id')->unsigned();
            $table->integer('no_of_students')->nullable();
            $table->integer('no_of_activities')->nullable();
            $table->string('creator_type')->nullable();
            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('ends_at')->nullable();
            $table->boolean('locked')->default(0);
            $table->foreign('school_id')
                ->references('id')->on('school')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('camp_id')
                ->references('id')->on('camp')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_camp');
    }
}
