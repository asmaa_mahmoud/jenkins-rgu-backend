<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityPreNext extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_pre_next', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('main_default_activity_id')->nullable()->unsigned();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->boolean('pre_or_next');
            $table->integer('order')->nullable();
            $table->timestamps();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('main_default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camp_template_translation');
    }
}
