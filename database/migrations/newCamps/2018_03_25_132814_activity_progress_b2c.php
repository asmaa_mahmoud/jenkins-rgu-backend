<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityProgressB2c extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('activity_progress', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('activity_id')->nullable()->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('task_id')->unsigned();
            $table->integer('no_of_blocks');
            $table->string('task_duration');
            $table->integer('score');
            $table->integer('no_of_trials');
            $table->integer('first_success');
            $table->integer('best_task_duration');
            $table->integer('best_blocks_number');
            $table->boolean('unlocked_by_coins')->default(false);
            $table->integer('no_of_blocks')->nullable()->change();
            $table->string('task_duration')->nullable()->change();
            $table->integer('score')->nullable()->change();
            $table->integer('no_of_trials')->nullable()->change();
            $table->integer('best_blocks_number')->nullable()->change();
            $table->smallInteger('first_success')->nullable()->change();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->foreign('activity_id')
                ->references('id')->on('activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('task_id')
                ->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');;
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
