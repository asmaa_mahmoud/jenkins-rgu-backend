<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolDistributorCamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_distributor_camp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_camp_id')->unsigned();
            $table->bigInteger('distributor_id')->unsigned();
            $table->string('activation_code')->nullable();
            $table->foreign('school_camp_id')
                ->references('id')->on('school_camp')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('distributor_id')
                ->references('id')->on('distributor')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_distributor_camp');
    }
}
