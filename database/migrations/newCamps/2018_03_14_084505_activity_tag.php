<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('activity_id')->nullable()->unsigned();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->foreign('tag_id')
                ->references('id')->on('tags')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('activity_id')
                ->references('id')->on('activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_tag');
    }
}
