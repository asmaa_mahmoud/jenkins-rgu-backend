<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolCampDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_camp_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_camp_id')->unsigned();
            $table->integer('no_of_students')->nullable();
            $table->integer('no_of_activities')->nullable();
            $table->timestamp('ends_at')->nullable();
            $table->foreign('school_camp_id')
                ->references('id')->on('school_camp')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_camp_details');
    }
}
