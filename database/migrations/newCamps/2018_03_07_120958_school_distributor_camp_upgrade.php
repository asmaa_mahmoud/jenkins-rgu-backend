<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolDistributorCampUpgrade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_distributor_camp_upgrade', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_camp_id')->unsigned();
            $table->text('description')->nullable();
            $table->integer('no_of_students')->nullable();
            $table->integer('no_of_activities')->nullable();
            $table->timestamp('ends_at')->nullable();
            $table->string('activation_code')->nullable();
            $table->boolean('used')->default(0);
            $table->foreign('school_camp_id')
                ->references('id')->on('school_camp')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_distributor_camp_upgrade');
    }
}
