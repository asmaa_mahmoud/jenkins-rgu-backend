<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityProgressAdds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('camp_activity_progress', function ($table) {
            $table->boolean('unlocked_by_coins')->default(false);
            $table->integer('no_of_blocks')->nullable()->change();
            $table->string('task_duration')->nullable()->change();
            $table->integer('score')->nullable()->change();
            $table->integer('no_of_trials')->nullable()->change();
            $table->integer('best_blocks_number')->nullable()->change();
            $table->smallInteger('first_success')->nullable()->change();
            $table->bigInteger('camp_id')->nullable()->unsigned();
            $table->bigInteger('default_activity_id')->nullable()->unsigned();
            $table->foreign('default_activity_id')
                ->references('id')->on('default_activity')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('camp_id')
                ->references('id')->on('camp')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
