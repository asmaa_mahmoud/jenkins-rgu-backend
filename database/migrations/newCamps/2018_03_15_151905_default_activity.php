<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_activity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('difficulty_id')->unsigned();
            $table->smallInteger('age_from')->nullable();
            $table->smallInteger('age_to')->nullable();
            $table->string('icon_url')->nullable();
            $table->string('image_url')->nullable();
            $table->boolean('published')->nullable()->default(1);
            $table->foreign('difficulty_id')
                ->references('id')->on('difficulty')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_activity');
    }
}
