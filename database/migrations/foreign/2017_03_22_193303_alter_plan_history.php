<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_history',function($table){
            $table->foreign('plan_id')
                ->references('id')->on('plan')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('account_type_id')
                ->references('id')->on('account_type')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_history',function($table){
            $table->dropForeign(['plan_id']);

        });
    }
}
