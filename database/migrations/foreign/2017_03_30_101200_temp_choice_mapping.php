<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempChoiceMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_choice_mapping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('journey_id');
            $table->integer('old_choice_id');
            $table->integer('new_choice_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_choice_mapping');
    }
}
