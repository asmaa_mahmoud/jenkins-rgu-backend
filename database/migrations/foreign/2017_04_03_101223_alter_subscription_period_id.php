<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSubscriptionPeriodId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stripe_subscription',function($table){
            $table->dropColumn('period');
            $table->bigInteger('period_id')->unsigned()->nullable();
            $table->foreign('period_id')
                ->references('id')->on('plan_period')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stripe_subscription',function($table){
            $table->dropForeign(['period_id']);
        });
    }
}
