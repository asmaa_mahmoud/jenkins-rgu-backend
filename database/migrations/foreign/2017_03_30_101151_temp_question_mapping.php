<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempQuestionMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_question_mapping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('journey_id');
            $table->integer('old_question_id');
            $table->integer('new_question_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_question_mapping');
    }
}
