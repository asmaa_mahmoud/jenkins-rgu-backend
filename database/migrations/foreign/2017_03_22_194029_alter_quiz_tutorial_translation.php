<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuizTutorialTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_tutorial_translation',function($table){
            $table->foreign('quiz_tutorial_id')
                ->references('id')->on('quiz_tutorial')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_tutorial_translation',function($table){
            $table->dropForeign(['quiz_tutorial_id']);

        });
    }
}
