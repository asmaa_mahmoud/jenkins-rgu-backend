<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdventureTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adventure_task', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('adventure_id')->unsigned();
            $table->foreign('adventure_id')->references('id')->on('default_adventure')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('task')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adventure_task');
    }
}
