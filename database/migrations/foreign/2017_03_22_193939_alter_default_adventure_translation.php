<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDefaultAdventureTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('default_adventure_translation',function($table){
            $table->foreign('default_adventure_id')
                ->references('id')->on('default_adventure')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('default_adventure_translation',function($table){
            $table->dropForeign(['default_adventure_id']);

        });
    }
}
