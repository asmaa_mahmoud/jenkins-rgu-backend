<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription',function($table){
            $table->foreign('plan_history_id')
                ->references('id')->on('plan_history')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('account_id')
                ->references('id')->on('account')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription',function($table){
            $table->dropForeign(['plan_history_id']);
            $table->dropForeign(['account_id']);

        });
    }
}
