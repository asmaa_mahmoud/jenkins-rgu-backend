<?php

use App\Infrastructure\Models\Customer;

$factory->define(Customer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('arjunphp'),
        'remember_token' => str_random(10),
    ];
});

