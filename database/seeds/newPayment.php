<?php

use Illuminate\Database\Seeder;

class newPayment extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared("
            INSERT INTO `unlockable_method` (`id`, `name`) VALUES
            (1, 'coins'),
            (2, 'xp'),
            (3, 'charge');
            
            INSERT INTO `difficulty` (`id`, `name`) VALUES
            (1, 'easy'),
            (2, 'hard'),
            (3, 'medium');
            
            INSERT INTO `plan_type` (`id`, `name`) VALUES
            (1, 'paid'),
            (2, 'free');
        ");
    }


}
