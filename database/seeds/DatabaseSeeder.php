<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\Gees\MissionsController;
use App\Gees\MissionProgress;
use App\Gees\QuizProgress;
use App\Gees\Classroom;
use Illuminate\Database\Eloquent\Model;
use App\Infrastructure\Repositories\UserRepository;
use Faker\Factory as Faker;
//use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,10) as $index)
        {
            DB::table('distributor_account_codes')->insert([
                'code' => $faker->randomNumber(6),
                'given' => 1,
                'used' => 0
            ]);
        }


//    	Model::unguard();
//
//        DB::table('users')->delete();
//
//        $users = array(
//                ['name' => 'Ryan Chenkie', 'email' => 'ryanchenkie@gmail.com', 'password' => Hash::make('secret')],
//                ['name' => 'Chris Sevilleja', 'email' => 'chris@scotch.io', 'password' => Hash::make('secret')],
//                ['name' => 'Holly Lloyd', 'email' => 'holly@scotch.io', 'password' => Hash::make('secret')],
//                ['name' => 'Adnan Kukic', 'email' => 'adnan@scotch.io', 'password' => Hash::make('secret')],
//        );
//
//        // Loop through each user above and create the record for them in the database
//        foreach ($users as $user)
//        {
//            UserRepository::create($user);
//        }
//
//        Model::reguard();
        // $this->call(UsersTableSeeder::class);
    }


    public function Students($accountId,$schoolName,$adminId)
    {
        $max_students = 0;
        $controller = new \App\Http\Controllers\Gees\SchoolAdminController();
        $plan = $controller->getPlanByAdminId($adminId);
        if($plan->NewName == 'Primary')
        {
            $max_students = $controller->primary_students;
        }
        else if($plan->NewName == 'Advanced')
        {
            $max_students = $controller->advanced_students;
        }
        else if($plan->NewName == 'Junior')
        {
            $max_students = $controller->junior_students;
        }
        else if($plan->NewName == 'Combined')
        {
            $max_students = $controller->combined_students;
        }


        $faker = Faker::create();
        $classrooms = \App\Gees\ClassRoomMembers::where('user_id',$adminId)->get();
        foreach(range(1,120) as $index)
        {
            if($index == $max_students)break;
            $user = new \App\Gees\User();
            $user->FirstName = $faker->firstNameMale;
            $user->LastName = $faker->lastName;
            $user->Account_Id = $accountId;
            $user->ProfileImageUrl = 'https://randomuser.me/api/portraits/men/'.$faker->randomNumber(2).'.jpg';
            $user->Email = $faker->email;
            $user->password = bcrypt('12345678');
            $user->username = $schoolName.'_student_'.$index;
            $user->gender = 1;

            if($user->save())
            {
                $user->Role()->attach(2);
                $user->Grade()->attach(rand(1,9));
                $user->Classroom()->attach($classrooms[rand(0,sizeof($classrooms)-1)]->classroom_id);
            }
        }
    }

    public function Teachers($accountId,$schoolName,$adminId)
    {
        $faker = Faker::create();
        $classrooms = \App\Gees\ClassRoomMembers::where('user_id',$adminId)->get();
        foreach(range(1,12) as $index)
        {
            $user = new \App\Gees\User();
            $user->FirstName = $faker->firstNameMale;
            $user->LastName = $faker->lastName;
            $user->Account_Id = $accountId;
            $user->ProfileImageUrl = 'https://randomuser.me/api/portraits/men/'.$faker->randomNumber(2).'.jpg';
            $user->Email = $faker->email;
            $user->password = bcrypt('12345678');
            $user->username = $schoolName.'_teacher_'.$index;
            $user->gender = 1;

            if($user->save())
            {
                $user->Role()->attach(3);
                $user->Grade()->attach($index);
                $user->Classroom()->attach($classrooms[rand(0,sizeof($classrooms)-1)]->classroom_id);
            }
        }
    }

    public function Classrooms($request)
    {
        $counter = 0;
        $faker = Faker::create();
        $max_courses = 0;
        $journeys = \App\Gees\Journey::all()->all();
        $controller = new \App\Http\Controllers\Gees\SchoolAdminController();
        $plan = $controller->getPlanByAdminId($request['adminId']);
        if($plan->NewName == 'Primary')
        {
            $max_courses = $controller->primary_courses;
        }
        else if($plan->NewName == 'Advanced')
        {
            $max_courses = $controller->advanced_courses;
        }
        else if($plan->NewName == 'Junior')
        {
            $max_courses = $controller->junior_courses;
        }
        else if($plan->NewName == 'Combined')
        {
            $max_courses = $controller->combined_courses;
        }

        foreach(range(1,$max_courses) as $index)
        {
            $classroom = new \App\Gees\Classroom();
            $classroom->name = 'Classroom_'.$index;
            if($classroom->save())
            {

                $classroom->Members()->attach($request['adminId']);
                $classroom->Grade()->attach(rand(1,9));
                foreach($journeys as $journey)
                {
                    $journeyName = $controller->getJourneyName($journey->id)['title'];
                    //return $classroom->Grade()->first();
                    $classroomGrade = $classroom->Grade()->first()['name'];
                    //return $classroomGrade;
                    if($counter == $max_courses) {break;}
                    if($classroomGrade === 'K 1')
                    {//return $journeyName;
                        if($journeyName === 'Birthday 1'){
                            //return 'I am in Birthday 1';
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }else if($classroomGrade == 'K 2')
                    {
                        if($journeyName == 'Birthday 2'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 3')
                    {
                        if($journeyName == 'The Moon 1'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 4')
                    {
                        if($journeyName == 'The Moon 2'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 5')
                    {
                        if($journeyName == 'Snowman 1'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 6')
                    {
                        if($journeyName == 'Snowman 2'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 7')
                    {
                        if($journeyName == 'Winter'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 8')
                    {
                        if($journeyName == 'Winter-Python'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                    else if($classroomGrade == 'K 9')
                    {
                        if($journeyName == 'Winter-Javascript'){
                            $classroom->Journeys()->attach($journey->id);
                            $counter++;
                        }
                    }
                }
            }

        }

        $this->Teachers($request['accountId'],$request['schoolName'],$request['adminId']);
        $this->Students($request['accountId'],$request['schoolName'],$request['adminId']);
        $this->Missions($request['accountId'],$request['schoolName'],$request['adminId']);

    }

    public function Missions($accountId,$schoolName,$adminId)
    {
        $classroomsIds = \App\Gees\ClassRoomMembers::where('user_id',$adminId)->pluck("classroom_id");

        //return \App\Gees\Classroom::with('Journeys.Adventures.Missions')->get();
        for($i=0; $i < sizeof($classroomsIds); $i++){
            $studentsIds = MissionsController::getStudentInClass($classroomsIds[$i]);
            $missionsIds = MissionsController::getMissionsByClassroom($classroomsIds[$i]);
            $quizIds = MissionsController::getQuizzesByClassroom($classroomsIds[$i]);
            //$rangeOfAddingScore = intval(sizeof($studentsIds) * .75);
            for($i=0; $i < sizeof($studentsIds); $i++)
            {
                $randomStudentId = $studentsIds[rand(0,sizeof($studentsIds)-1)];
                $randomMissionId = $missionsIds[rand(0,sizeof($missionsIds)-1)];
                $randomQuizId = $quizIds[rand(0,sizeof($quizIds)-1)];

                $missionsWithUser = MissionProgress::where('Mission_Id', '=', $randomMissionId)
                    ->where('User_Id', '=', $randomStudentId)->get()->toArray();

                $QuizWithUser = QuizProgress::where('Quiz_Id', '=', $randomQuizId)
                    ->where('User_Id', '=', $randomStudentId)->get()->toArray();

                if (empty($missionsWithUser)){
                    $missionScore = new \App\Gees\MissionProgress();
                    $missionScore->User_Id = $randomStudentId;
                    $missionScore->Mission_Id = $randomMissionId;
                    $missionScore->MissionScore = 10 ;
                    $missionScore->save();
                }

                if (empty($QuizWithUser)){
                    $quizScore = new \App\Gees\QuizProgress();
                    $quizScore->User_Id = $randomStudentId;
                    $quizScore->Quiz_Id = $randomQuizId;
                    $quizScore->QuizScore = rand(1,10) ;
                    $quizScore->NoOfTrials = rand(1,10) ;
                    $quizScore->save();
                }
            }
        }
    }
}
