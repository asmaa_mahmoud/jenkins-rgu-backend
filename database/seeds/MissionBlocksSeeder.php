<?php

use Illuminate\Database\Seeder;

class MissionBlocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $missions = DB::table('mission')->get();
        foreach ($missions as $mission) {
        	$mission_block = DB::table('mission_category')->where('mission_id',$mission->id)->where('block_category_id',8)->first();
        	if($mission_block == null)
        		DB::table('mission_category')->insert([
        			'mission_id'=>$mission->id,
        			'block_category_id'=>8
        			]);
        }
    }
}
