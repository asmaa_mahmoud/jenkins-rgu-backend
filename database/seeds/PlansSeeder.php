<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plan')->insert([
		    ['Name' => 'taylor@example.com', 'CreditPerMonth' => 0,'CreditPerQuarter' => 0,'CreditPerYear' => 0,'AccountType_Id' => 0,'NoOfJourneys' => 0,'ProgressUnlocked' => 0,'created_at' => 0,'updated_at' => 0],
		    ['email' => 'dayle@example.com', 'votes' => 0]
		]);
    }
}
