<?php

use Illuminate\Database\Seeder;

class DatabaseBasicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_id = DB::table('category')->insertGetId([
                'name' => 'Learning'
            ]);

        DB::table('category_translation')->insert([
                'category_id' => $category_id,
                'language_code'=>'en',
                'title'=>'Learning'
            ]);
        DB::table('category_translation')->insert([
                'category_id' => $category_id,
                'language_code'=>'ar',
                'title'=>'تعليمي'
            ]);
        $plan_id = DB::table('plan')->insertGetId([
                'name' => 'RoboGarden_Free'
            ]);
        DB::table('plan_history')->insert([
                'plan_id' => $plan_id,
                'new_name'=>'RoboGarden_Free',
                'credit_per_month'=> 0,
                'credit_per_quarter'=>0,
                'credit_per_year'=>0,
                'account_type_id'=>7,
                'number_of_journeys'=>1
            ]);
        $plan_id = DB::table('plan')->insertGetId([
                'name' => 'RoboGarden_Silver'
            ]);
        DB::table('plan_history')->insert([
                'plan_id' => $plan_id,
                'new_name'=>'RoboGarden_Silver',
                'credit_per_month'=> 8.99,
                'credit_per_quarter'=>18.99,
                'credit_per_year'=>98.99,
                'account_type_id'=>7,
                'number_of_journeys'=>3
            ]);
        $plan_id = DB::table('plan')->insertGetId([
                'name' => 'RoboGarden_Golden'
            ]);
        DB::table('plan_history')->insert([
                'plan_id' => $plan_id,
                'new_name'=>'RoboGarden_Golden',
                'credit_per_month'=> 9.99,
                'credit_per_quarter'=>19.99,
                'credit_per_year'=>99.99,
                'account_type_id'=>7,
                'number_of_journeys'=>5
            ]);

        DB::table('plan_period')->insert([
            'name' => 'monthly'
        ]);
        DB::table('plan_period')->insert([
            'name' => 'yearly'
        ]);
        DB::table('plan_period')->insert([
            'name' => 'quarterly'
        ]);
        DB::table('role')->insert([
            'name' => 'user',
            'account_type_id'=>7
        ]);
    }
}
