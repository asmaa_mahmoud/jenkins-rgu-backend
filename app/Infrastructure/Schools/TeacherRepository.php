<?php

namespace App\Infrastructure\Schools;

use App\DomainModelLayer\Accounts\User;
use Analogue;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\ClassroomMember;
use Carbon\Carbon;
use DB;

class TeacherRepository
{
    public function getAllTeachers($account_id,$limit = null,$grade_id = null,$search = null, $count = false)
    {
        $userMapper= Analogue::mapper(User::class);
        //$query = $userMapper->query()
        //    ->where('account_id',$account_id)->whereIn('id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','teacher')->pluck('id')->toArray()))->pluck('user_id')->toArray()));

        $query =  $userMapper->query()->where('account_id',$account_id)->whereIn('id',function($queryx){
           $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
               ->whereIn('role_id',function($queryy){
                   $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','teacher');
               });
        });
        if($grade_id != null){
            //$query = $query->whereIn('id',DB::table('group_user')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->whereIn('classroom_id',DB::table('classroom')->whereIn('grade_id',[$grade_id])->pluck('id')->toArray())->pluck('id')->toArray())->pluck('user_id')->toArray());
            $query = $query->whereIn('id',function($query1) use ($grade_id) {
                $query1->select('user_id')->from('group_user')->whereIn('group_id',function($query2) use ($grade_id) {
                    $query2->select('id')->from('group')->whereNull('deleted_at')->whereIn('classroom_id',function($query3) use ($grade_id) {
                        $query3->select('id')->from('classroom')->where('grade_id',$grade_id);
                    });
                });
            });
            //$query = $query->whereIn('id',DB::table('classroom_user')->whereIn('classroom_id',DB::table('classroom')->whereNull('deleted_at')->where('grade_id',$grade_id)->pluck('id')->toArray())->pluck('user_id')->toArray());
//            $query = $query->whereIn('id',function($queryx) use($grade_id){
//               $queryx->select('user_id')->from('classroom_user')->whereIn('classroom_id',function($queryy) use($grade_id){
//                   $queryy->select('id')->from('classroom')->whereNull('deleted_at')->where('grade_id',$grade_id);
//               });
//            });
        }
        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%')
                    ->orWhere('last_name','like','%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->orderBy('username','asc')->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->orderBy('username','asc')->get();
        }
        return $query;
    }


    public function getTeacherClassrooms($user,$limit = null,$grade_id = null,$search = null,$count = false){
        $classroomMapper = Analogue::mapper(Classroom::class);
        //$query = $classroomMapper->query()->whereIn('id',DB::table('group')->whereNull('deleted_at')->where('is_default',1)->whereIn('id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('classroom_id')->toArray());
        $query = $classroomMapper->query()->whereIn('id',function($queryx) use($user){
           $queryx->select('classroom_id')->from('group')->whereNull('deleted_at')->where('is_default',1)->whereIn('id',function($queryy) use($user){
               $queryy->select('group_id')->from('group_user')->where('user_id',$user->getId());
           });
        });
        if($grade_id != null) {
            $query = $query->where('grade_id', $grade_id);
        }

        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null){
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function getTeacherCourses($user,$limit,$classroom_id,$search,$count = false){
        $courseMapper = Analogue::mapper(Course::class);
        $query = $courseMapper->query();
        if($classroom_id == null){
            //$query->whereIn('id',DB::table('course_group')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->whereIn('id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('id')->toArray())->pluck('course_id')->toArray());
            $query->whereIn('id',function($queryx) use($user){
               $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($user){
                  $queryy->select('id')->from('group')->whereNull('deleted_at')->whereIn('id',function($queryz) use($user){
                     $queryz->select('group_id')->from('group_user')->where('user_id',$user->getId());
                  });
               });
            });
        }

        else{
            //$query->whereIn('id',DB::table('course_group')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->where('classroom_id',$classroom_id)->whereIn('id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('id')->toArray())->pluck('course_id')->toArray());
            $query->whereIn('id',function($queryx) use($user,$classroom_id){
                $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($user,$classroom_id){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')->where('classroom_id',$classroom_id)
                        ->whereIn('id',function($queryz) use($user){
                        $queryz->select('group_id')->from('group_user')->where('user_id',$user->getId());
                    });
                });
            });
        }


        if($search != null){

            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null){
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;

    }


    public function getCourseAboutToStartForTeacher(User $user){
        //$classroom_ids = DB::table('group')->whereNull('deleted_at')->where('is_default',1)->whereIn('id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('classroom_id')->toArray();
        $courseMapper = Analogue::mapper(Course::class);
        //$courseMapper =  $courseMapper->query()->whereIn('id',DB::table('course_group')
        //    ->whereIn('group_id',DB::table('group')->whereNull('deleted_at')
        //        ->where('is_default',1)->whereIn('classroom_id',$classroom_ids)->pluck("id")->toArray())
        //    ->pluck('course_id')->toArray())
        //    ->whereDate('starts_at','>',Carbon::now());
        $courseMapper = $courseMapper->query()->whereIn('id',function($queryx) use($user){
            $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($user){
               $queryy->select('id')->from('group')->whereNull('deleted_at')
                   ->where('is_default',1)->whereIn('classroom_id',function($queryz) use($user){
                      $queryz->select('classroom_id')->from('group')->whereNull('deleted_at')
                          ->where('is_default',1)->whereIn('id',function($queryr) use($user){
                              $queryr->select('group_id')->from('group_user')->where('user_id',$user->getId());
                          });
                   });
            });
        })->whereDate('starts_at','>',Carbon::now());

        return $courseMapper->get();

    }

    public function getCourseEndedInRangeForTeacher(User $user,$from_time = null,$to_time = null){
//        $classroom_ids = DB::table('group')->whereNull('deleted_at')->where('is_default',1)->whereIn('id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('classroom_id')->toArray();
//        $courseMapper = Analogue::mapper(Course::class);
//        $courseMapper =  $courseMapper->query()->whereIn('id',DB::table('course_group')
//            ->whereIn('group_id',DB::table('group')->whereNull('deleted_at')
//                ->where('is_default',1)->whereIn('classroom_id',DB::table('classroom')
//                    ->whereIn('id',$classroom_ids)
//                    ->whereNull('deleted_at')->pluck('id')->toArray())->pluck("id")->toArray())
//            ->pluck('course_id')->toArray());
        $courseMapper = Analogue::mapper(Course::class);
        $courseMapper = $courseMapper->query()->whereIn('id',function($queryx) use($user){
            $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($user){
                $queryy->select('id')->from('group')->whereNull('deleted_at')
                    ->where('is_default',1)->whereIn('classroom_id',function($queryz) use($user){
                        $queryz->select('classroom_id')->from('group')->whereNull('deleted_at')
                            ->where('is_default',1)->whereIn('id',function($queryr) use($user){
                                $queryr->select('group_id')->from('group_user')->where('user_id',$user->getId());
                            });
                    });
            });
        });
        if($from_time != null && $to_time != null)
            return $courseMapper->whereDate('ends_at', '<', Carbon::now())->whereBetween('ends_at',[$from_time,$to_time])->get();
        else
            return $courseMapper->whereDate('ends_at', '<', Carbon::now())->get();
    }

    public function getStudentsFinishCourseForTeacher(User $user){
        //$classroom_ids = DB::table('group')->whereNull('deleted_at')->where('is_default',1)->whereIn('id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('classroom_id')->toArray();
//        return DB::table('student_finish_course')
//            ->select(DB::raw('`student_finish_course`.`user_id` as student_id,concat(`user`.`first_name`,`user`.`last_name`) as name,classroom.name as classroom_name,course.name as course_name,student_finish_course.time as time'))
//            ->leftjoin('group_user','group_user.user_id','=','student_finish_course.user_id')
//            ->leftjoin('group','group.id','=','group_user.group_id')
//            ->leftjoin('classroom','classroom.id','=','group.classroom_id')
//            ->leftJoin('user','user.id','=','student_finish_course.user_id')
//            ->leftJoin('course','course.id','=','student_finish_course.course_id')
//            ->whereIn('student_finish_course.user_id',DB::table('group_user')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')
//                ->whereIn('classroom_id',$classroom_ids)->pluck('id')->toArray())->pluck('user_id')->toArray())
//            ->where('group.is_default',1)->get();
        return DB::table('student_finish_course')
            ->select(DB::raw('`student_finish_course`.`user_id` as student_id,concat(`user`.`first_name`,`user`.`last_name`) as name,classroom.name as classroom_name,course.name as course_name,student_finish_course.time as time'))
            ->leftjoin('group_user','group_user.user_id','=','student_finish_course.user_id')
            ->leftjoin('group','group.id','=','group_user.group_id')
            ->leftjoin('classroom','classroom.id','=','group.classroom_id')
            ->leftJoin('user','user.id','=','student_finish_course.user_id')
            ->leftJoin('course','course.id','=','student_finish_course.course_id')
            ->whereIn('student_finish_course.user_id',function($queryx) use($user){
                $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($user){
                   $queryy->select('id')->from('group')->whereNull('deleted_at')
                       ->whereIn('classroom_id',function($queryz) use($user){
                          $queryz->select('classroom_id')->from('group')->whereNull('deleted_at')
                              ->where('is_default',1)->whereIn('id',function($queryr) use($user){
                                 $queryr->select('group_id')->from('group_user')->where('user_id',$user->getId());
                              });
                       });
                });
            })->where('group.is_default',1)->get();

    }
}