<?php

namespace App\Infrastructure\Schools;

use App\ApplicationLayer\Schools\Dtos\ChargeDto;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\SchoolDistributorCamp;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Schools\CampActivity;
use App\DomainModelLayer\Schools\CampActivityProgress;
use App\DomainModelLayer\Schools\CampUser;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;
use App\DomainModelLayer\Schools\DistributorCharge;
use App\DomainModelLayer\Schools\DistributorChargeDetails;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\SchoolCamp;
use App\DomainModelLayer\Schools\StudentPosition;
use App\DomainModelLayer\Schools\StudentProgress;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\StudentClickMission;
use App\DomainModelLayer\Schools\StudentFinishCourse;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\Group;
use App\DomainModelLayer\Schools\DistributorSubscription;
use App\DomainModelLayer\Schools\SchoolCharge;
use App\DomainModelLayer\Schools\SchoolChargesLog;
use App\DomainModelLayer\Schools\SchoolChargeDetails;
use App\DomainModelLayer\Schools\SchoolChargeUpgrade;
use App\DomainModelLayer\Schools\DistributorChargeUpgrade;
use Analogue;
use App\DomainModelLayer\Schools\SchoolCampDetail;
use App\Infrastructure\Journeys\ActivityRepository;

class SchoolMainRepository implements ISchoolMainRepository
{
    public function addSchool(School $school){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->addSchool($school);
    }

    public function getGradeByName(School $school,$name)
    {
        $gradeRepository = new GradeRepository;
        return $gradeRepository->getGradeByName($school,$name);
    }

    public function storeClassroom(Classroom $classroom)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->storeClassroom($classroom);
    }

    public function getStudentProgressInClassroom($student_id, $classroom_id)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getStudentProgressInClassroom($student_id, $classroom_id);
    }

    public function getClassroomByName($name)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getClassroomByName($name);
    }

    public function getClassroomById($id)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getClassroomById($id);
    }

    public function getDistributors()
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getDistributors();
    }

    public function getDefaultDistributors()
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getDefaultDistributors();
    }

    public function getDistributorById($id)
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getDistributorById($id);
    }

    public function getCountryByCode($country_code)
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getCountryByCode($country_code);
    }

    public function getCountries()
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getCountries();
    }

    public function getDistributorSubscriptionByCode($user_id,$code)
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getDistributorSubscriptionByCode($user_id,$code);
    }

    public function emailDistributor($distributor_name,$plan_name,$students,$classrooms,$distributor_email,$admin_name,$admin_email,$school_name,$code,$upcomingCode, $upgrade = false)
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->emailDistributor($distributor_name,$plan_name,$students,$classrooms,$distributor_email,$admin_name,$admin_email,$school_name,$code,$upcomingCode,$upgrade);
    }

    public function emailSubscription($account_name,$plan_name,$students,$classroom,$account_email,$plan = true, $upgrade = null)
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->emailSubscription($account_name,$plan_name,$students,$classroom,$account_email,$plan,$upgrade);
    }

    public function getAllClassrooms($school_id,$limit = null,$grade_id = null,$search = null,$count = false)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getAllClassrooms($school_id,$limit,$grade_id,$search,$count);
    }

    public function getAllTeachers($account_id,$limit = null,$grade_id = null,$search = null,$count = false)
    {
        $teacherRepository = new TeacherRepository;
        return $teacherRepository->getAllTeachers($account_id,$limit,$grade_id,$search,$count);
    }

    public function getAllStudents($account_id,$limit = null,$grade_id = null,$search = null,$count = false)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->getAllStudents($account_id,$limit,$grade_id,$search,$count);
    }

    public function getAllStudentsInClassroom($classroom_id,$limit = null,$search = null,$count = false){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getAllStudentsInClassroom($classroom_id,$limit,$search,$count);
    }

    public function getAllTeachersInClassroom($classroom_id,$limit = null,$search = null,$count = false){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getAllTeachersInClassroom($classroom_id,$limit,$search,$count);
    }

    public function getClassRoomMember(Classroom $classroom,User $user,Role $role){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getClassRoomMember($classroom,$user,$role);
    }

    public function deleteClassroomMember(ClassroomMember $classroomMember){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->deleteClassroomMember($classroomMember);
    }

    public function storeClassroomMember(ClassroomMember $classroomMember){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->storeClassroomMember($classroomMember);
    }

    public function storeDistributorSubscription(DistributorSubscription $distributor_subscription)
    {
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeDistributorSubscription($distributor_subscription);

    }

    public function emailAlreadyExist($email){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->emailAlreadyExist($email);
    }

    public function usernameAlreadyExist($username){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->usernameAlreadyExist($username);
    }

    public function addDefaultGrades(School $school){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->addDefaultGrades($school);
    }

    public function getClassroomJourney($classroom_id,$journey_id)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getClassroomJourney($classroom_id,$journey_id);
    }

    public function deleteClassroomJourney(ClassroomJourney $classroomJourney)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->deleteClassroomJourney($classroomJourney);
    }

    public function getUnEnrolledStudents(User $user,Classroom $classroom,$limit,$search){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getUnEnrolledStudents($user,$classroom,$limit,$search);
    }

    public function getUnAssignedTeachers(User $user,Classroom $classroom,$limit,$search){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getUnAssignedTeachers($user,$classroom,$limit,$search);
    }

    public function deleteClassroom(Classroom $classroom){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->deleteClassroom($classroom);
    }

    public function deleteClassroomMembers(User $user){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->deleteClassroomMembers($user);
    }

    public function getTeacherClassrooms($user,$limit = null,$grade_id = null,$search = null,$count = false){
        $teacherRepository = new TeacherRepository;
        return $teacherRepository->getTeacherClassrooms($user,$limit,$grade_id,$search,$count);
    }

    public function getCourseById($id){
        $courseRepository = new CourseRepository;
        return $courseRepository->getCourseById($id);
    }

    public function getStudentProgrss($task_id,$user_id,$course_id,$adventure_id){
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentProgrss($task_id,$user_id,$course_id,$adventure_id);
    }

    public function storeStudentProgress(StudentProgress $student_progress){
        $studentRepository = new StudentRepository;
        return $studentRepository->storeStudentProgress($student_progress);
    }

    public function deleteStudentProgress(StudentProgress $student_progress){
        $studentRepository = new StudentRepository;
        return $studentRepository->deleteStudentProgress($student_progress);
    }

    public function storeStudentPosition(StudentPosition $student_position)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->storeStudentPosition($student_position);
    }

    public function deleteStudentPosition(StudentPosition $student_position)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->deleteStudentPosition($student_position);
    }

    public function getStudentProgressByOrder($user_id, $order,$course_id,$adventure_id)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentProgressByOrder($user_id, $order,$course_id,$adventure_id);
    }

    public function getTeacherCourses($user,$limit,$classroom_id,$search,$count = false){
        $teacherRepository = new TeacherRepository;
        return $teacherRepository->getTeacherCourses($user,$limit,$classroom_id,$search,$count);
    }

    public function storeCourse(Course $course){
        $courseRepository = new CourseRepository;
        return $courseRepository->storeCourse($course);
    }

    public function storeGroup(Group $group){
        $groupRepository = new GroupRepository;
        return $groupRepository->storeGroup($group);
    }

    public function getDefaultGroupMember(Classroom $classroom,User $user,Role $role){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getDefaultGroupMember($classroom,$user,$role);
    }

    public function deleteGroupMember(GroupMember $groupMember){
        $groupRepository = new GroupRepository;
        return $groupRepository->deleteGroupMember($groupMember);
    }
    public function deleteGroupMembers(User $user){
        $groupRepository = new GroupRepository;
        return $groupRepository->deleteGroupMembers($user);
    }

    public function storeGroupMember(GroupMember $groupMember){
        $groupRepository = new GroupRepository;
        return $groupRepository->storeGroupMember($groupMember);
    }

    public function deleteCourse(Course $course){
        $courseRepository = new CourseRepository;
        return $courseRepository->deleteCourse($course);
    }
    public function deleteCourseAdventureDeadlines(Course $course){
        $courseRepository = new CourseRepository;
        return $courseRepository->deleteCourseAdventureDeadlines($course);
    }

    public function getStudentProgressInAdventure($user_id,$adventure_id,$course_id)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentProgressInAdventure($user_id,$adventure_id,$course_id);
    }

    public function getStudentsInGroup(Group $group,$limit,$search,$count = false){
        $groupRepository = new GroupRepository;
        return $groupRepository->getStudentsInGroup($group,$limit,$search,$count);
    }

    public function getCompletedMissionsInCourse($course_id){
        $courseRepository = new CourseRepository;
        return $courseRepository->getCompletedMissionsInCourse($course_id);
    }

    public function getNeverPlayedMissionsInCourse($course_id){
        $courseRepository = new CourseRepository;
        return $courseRepository->getNeverPlayedMissionsInCourse($course_id);
    }

    public function getTopStudents(Account $account,Grade $grade,$limit = 10){
        $studentRepository = new StudentRepository;
        return $studentRepository->getTopStudents($account,$grade,$limit);
    }

    public function getGradeById($grade_id){
        $gradeRepository = new GradeRepository;
        return $gradeRepository->getGradeById($grade_id);
    }

    public function getTopStudentsInClassroom(Account $account,Classroom $classroom,$limit=10){
        $studentRepository = new StudentRepository;
        return $studentRepository->getTopStudentsInClassroom($account,$classroom,$limit);
    }

    public function getCourseCreatedInRange(School $school,$from_time,$to_time){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getCourseCreatedInRange($school,$from_time,$to_time);
    }

    public function getCourseEndedInRange(School $school,$from_time,$to_time){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getCourseEndedInRange($school,$from_time,$to_time);
    }

    public function getCoursesFinishedByStudents(School $school,$from_time,$to_time){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getCoursesFinishedByStudents($school,$from_time,$to_time);
    }

    public function storeStudentClickMission(StudentClickMission $studentClickMission){
        $studentRepository = new StudentRepository;
        return $studentRepository->storeStudentClickMission($studentClickMission);
    }

    public function getStoreStudentClickMissionByTimeAndUser(User $user,$time){
        $studentRepository = new StudentRepository;
        return $studentRepository->getStoreStudentClickMissionByTimeAndUser($user,$time);
    }

    public function getStudentMissionClickByTime(Account $account,$from_time,$to_time,$classroom_ids = array()){
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentMissionClickByTime($account,$from_time,$to_time,$classroom_ids);
    }

    public function getStudentSolvedMissions(Account $account,$from_time,$to_time,$classroom_ids = array()){
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentSolvedMissions($account,$from_time,$to_time,$classroom_ids);
    }

    public function getCourseAboutToStartForTeacher(User $user){
        $teacherRepository = new TeacherRepository;
        return $teacherRepository->getCourseAboutToStartForTeacher($user);
    }

    public function getCourseEndedInRangeForTeacher(User $user,$from_time = null,$to_time = null){
        $teacherRepository = new TeacherRepository;
        return $teacherRepository->getCourseEndedInRangeForTeacher($user,$from_time,$to_time);
    }

    public function getIfStudentsProgressedInClassroom(Classroom $classroom,$students){
        $studentRepository = new StudentRepository;
        return $studentRepository->getIfStudentsProgressedInClassroom($classroom,$students);
    }

    public function checkClassroomNameUniqueInSchool(School $school,$classroomName){
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->checkClassroomNameUniqueInSchool($school,$classroomName);
    }

    public function checkCourseNameUniqueInClassroom(Classroom $classroom,$courseName){
        $courseRepository = new CourseRepository;
        return $courseRepository->checkCourseNameUniqueInClassroom($classroom,$courseName);
    }

    public function storeStudentFinishCourse(StudentFinishCourse $studentFinishCourse){
        $studentRepository = new StudentRepository;
        return $studentRepository->storeStudentFinishCourse($studentFinishCourse);
    }

    public function getStudentsFinishedCourse(Course $course){
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentsFinishedCourse($course);
    }

    public function getFinishedCourseByAllStudentsInSchool(School $school){
        $studentRepository = new StudentRepository;
        return $studentRepository->getFinishedCourseByAllStudentsInSchool($school);
    }

    public function getStudentBlockingTasks($student_id)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentBlockingTasks($student_id);
    }

    public function getClassroomsForAdmin(School $school,Grade $grade = null){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getClassroomsForAdmin($school,$grade);
    }

    public function getStudentLastPlayedTask($student_id)
    {
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentLastPlayedTask($student_id);
    }

    public function getStudentsFinishCourseForTeacher(User $user){
        $teacherRepository = new TeacherRepository;
        return $teacherRepository->getStudentsFinishCourseForTeacher($user);
    }

    public function getStudentAdventureDeadlinePassed(User $user){
        $studentRepository = new StudentRepository;
        return $studentRepository->getStudentAdventureDeadlinePassed($user);
    }

    public function getCourseAdventureDeadline($courseAdventureDeadlineId){
        $courseRepository = new CourseRepository;
        return $courseRepository->getCourseAdventureDeadline($courseAdventureDeadlineId);
    }

    public function handleStripeCharge(ChargeDto $chargeDto){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->handleStripeCharge($chargeDto);
    }

    public function getSchoolPlanComponentCost($type, $quantity){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getSchoolPlanComponentCost($type, $quantity);
    }

    public function getBundleById($bundle_id){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getBundleById($bundle_id);
    }

    public function storeSchoolCharge(SchoolCharge $schoolCharge){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeSchoolCharge($schoolCharge);
    }

    public function storeSchoolChargeDetails(SchoolChargeDetails $schoolChargeDetails){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeSchoolChargeDetails($schoolChargeDetails);
    }

    public function getNearestDiscount($type, $quantity){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getNearestDiscount($type, $quantity);
    }

    public function storeSchoolChargesLog(SchoolChargesLog $schoolChargesLog){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeSchoolChargesLog($schoolChargesLog);
    }

    public function storeSchoolChargeUpgrade(SchoolChargeUpgrade $schoolChargeUpgrade){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeSchoolChargeUpgrade($schoolChargeUpgrade);
    }

    public function getLatestBundleUpgrade(SchoolCharge $schoolCharge){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getLatestBundleUpgrade($schoolCharge);
    }

    public function getAllUpgradesAfter($id,SchoolCharge $schoolCharge){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getAllUpgradesAfter($id,$schoolCharge);
    }

    public function storeDistributorCharge(DistributorCharge $distributorCharge){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeDistributorCharge($distributorCharge);
    }

    public function storeDistributorChargeDetails(DistributorChargeDetails $distributorChargeDetails){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeDistributorChargeDetails($distributorChargeDetails);
    }

    public function getSchoolInvoicesLog($school_id,$limit){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getSchoolInvoicesLog($school_id,$limit);
    }

    public function getSchoolPlanBundles(){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getSchoolPlanBundles();
    }

    public function getSchoolPrices($type = null){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getSchoolPrices($type);
    }

    public function getMaxNoOfStudentsAndClassrooms($account_id){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getMaxNoOfStudentsAndClassrooms($account_id);
    }

    public function storeDistributorChargeUpgrade(DistributorChargeUpgrade $distributorChargeUpgrade){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->storeDistributorChargeUpgrade($distributorChargeUpgrade);
    }

    public function deleteUnusedDistributorUpgrades($distributor_charge_id){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->deleteUnusedDistributorUpgrades($distributor_charge_id);
    }

    public function getDistributorLastUpgrade($distributor_charge_id){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getDistributorLastUpgrade($distributor_charge_id);
    }

    public function getEnrolledStudent($account_id, $count = null)
    {
        $classroomRepository = new ClassroomRepository;
        return $classroomRepository->getEnrolledStudent($account_id, $count);
    }

    // activities part
    public function getDefaultActivityById($id){
        $activityRepository = new ActivityRepository;
        return $activityRepository->getDefaultActivityById($id);
    }

    // camps part
    public function getCampById($id)
    {
        $campRepository = new CampRepository;
        return $campRepository->getCampById($id);
    }

    public function checkIfActiveCamp($camp_id)
    {
        $campRepository = new CampRepository;
        return $campRepository->checkIfActiveCamp($camp_id);
    }

    public function getRunningCampsInSchool($school_id, $count = null, $teacher_id = null)
    {
        $campRepository = new CampRepository;
        return $campRepository->getRunningCampsInSchool($school_id, $count, $teacher_id);
    }

    public function getCountOfPeopleInActiveCamps($school_id, $roleName)
    {
        $campRepository = new CampRepository;
        return $campRepository->getCountOfPeopleInActiveCamps($school_id, $roleName);
    }

    public function getUpcomingCampsInSchool($school_id, $count = null, $teacher_id = null)
    {
        $campRepository = new CampRepository;
        return $campRepository->getUpcomingCampsInSchool($school_id, $count, $teacher_id);
    }

    public function getInActiveCampsInSchool($school_id, $count = null)
    {
        $campRepository = new CampRepository;
        return $campRepository->getInActiveCampsInSchool($school_id, $count);
    }

    public function getActiveCampsInSchool($school_id, $count = null, $camp_ids = null)
    {
        $campRepository = new CampRepository;
        return $campRepository->getActiveCampsInSchool($school_id, $count, $camp_ids);
    }

    public function getTopStudentsInCamp(Account $account, $camp_id, $limit=10)
    {
        $campRepository = new CampRepository;
        return $campRepository->getTopStudentsInCamp($account, $camp_id, $limit);
    }

    public function getStudentSolveTaskByTime(Account $account, $from_time, $to_time, $camp_ids = null){
        $campRepository = new CampRepository;
        return $campRepository->getStudentSolveTaskByTime($account, $from_time, $to_time, $camp_ids);
    }

    public function getTaskSolvedByStudentByTime(Account $account, $from_time, $to_time, $camp_ids = null){
        $campRepository = new CampRepository;
        return $campRepository->getTaskSolvedByStudentByTime($account, $from_time, $to_time, $camp_ids);
    }

    public function getLastProgressInCamp($camp_id){
        $campRepository = new CampRepository;
        return $campRepository->getLastProgressInCamp($camp_id);
    }

    public function getAllCampsByStatus($school_id, $status, $limit = null, $search = null, $count = false, $camp_ids = null){
        $campRepository = new CampRepository;
        return $campRepository->getAllCampsByStatus($school_id, $status, $limit, $search, $count, $camp_ids);
    }

    public function deleteCamp(Camp $camp){
        $campRepository = new CampRepository;
        return $campRepository->deleteCamp($camp);
    }

    public function getAllStudentsInCamp($camp_id, $limit = null, $search = null, $count = false){
        $campRepository = new CampRepository;
        return $campRepository->getAllStudentsInCamp($camp_id, $limit, $search, $count);
    }

    public function getAllTeachersInCamp($camp_id, $limit = null, $search = null, $count = false){
        $campRepository = new CampRepository;
        return $campRepository->getAllTeachersInCamp($camp_id, $limit, $search, $count);
    }

    public function checkCampNameUniqueInSchool(School $school, $campName){
        $campRepository = new CampRepository;
        return $campRepository->checkCampNameUniqueInSchool($school, $campName);
    }

    public function getCampActivity($camp_id, $activity_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getCampActivity($camp_id, $activity_id, $is_default);
    }

    public function storeCampActivity(CampActivity $campActivity){
        $campRepository = new CampRepository;
        return $campRepository->storeCampActivity($campActivity);
    }

    public function getLastOrderInActivityCamp($camp_id){
        $campRepository = new CampRepository;
        return $campRepository->getLastOrderInActivityCamp($camp_id);
    }

    public function storeCamp(Camp $camp){
        $campRepository = new CampRepository;
        return $campRepository->storeCamp($camp);
    }

    public function storeSchoolCamp(SchoolCamp $schoolCamp){
        $campRepository = new CampRepository;
        return $campRepository->storeSchoolCamp($schoolCamp);
    }

    public function storeSchoolCampDistributor(SchoolDistributorCamp $distributorCamp){
        $campRepository = new CampRepository;
        return $campRepository->storeSchoolCampDistributor($distributorCamp);
    }

    public function storeSchoolCampDetail(SchoolCampDetail $campDetail){
        $campRepository = new CampRepository;
        return $campRepository->storeSchoolCampDetail($campDetail);
    }

    public function storeCampUser(CampUser $campUser){
        $campRepository = new CampRepository;
        return $campRepository->storeCampUser($campUser);
    }

    public function getAllActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null){
        $activityRepository = new ActivityRepository;
        return $activityRepository->getAllActivities($limit, $search, $count, $is_b2b, $is_b2c);
    }

    public function getAllStudentsInCamps($school_id, $grade_id = null, $limit = null, $search = null, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getAllStudentsInCamps($school_id, $grade_id, $limit, $search, $count);
    }

    public function getAllTeachersInCamps($school_id, $limit = null, $search = null, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getAllTeachersInCamps($school_id, $limit, $search, $count);
    }

    public function getAssignedStudentsInCamp($camp_id, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getAssignedStudentsInCamp($camp_id, $count);
    }

    public function checkIfPaidCamp($camp_id){
        $campRepository = new CampRepository;
        return $campRepository->checkIfPaidCamp($camp_id);
    }

    public function checkIfEndedCamp($camp_id){
        $campRepository = new CampRepository;
        return $campRepository->checkIfEndedCamp($camp_id);
    }

    public function getCampStatus($camp_id){
        $campRepository = new CampRepository;
        return $campRepository->getCampStatus($camp_id);
    }

    public function deleteCampActivity(CampActivity $campActivity){
        $campRepository = new CampRepository;
        return $campRepository->deleteCampActivity($campActivity);
    }

    public function deleteCampActivities($camp_id){
        $campRepository = new CampRepository;
        return $campRepository->deleteCampActivities($camp_id);
    }

    public function getActivityTaskOrder($activity_id, $task_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getActivityTaskOrder($activity_id, $task_id, $is_default);
    }

    public function getUserProgressByOrderInActivity($user_id, $order, $camp_id, $activity_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getUserProgressByOrderInActivity($user_id, $order, $camp_id, $activity_id, $is_default);
    }

    public function deleteCampUsers(CampUser $campUser){
        $campRepository = new CampRepository;
        return $campRepository->deleteCampUsers($campUser);
    }

    public function getCampUser($camp_id, $userId){
        $campRepository = new CampRepository;
        return $campRepository->getCampUser($camp_id, $userId);
    }

    public function emailCampDistributor($distributor_name, $campName, $students, $activities, $distributor_email, $admin_name, $admin_email, $school_name, $code, $startDate, $endDate, $upgrade = false){
        $campRepository = new CampRepository;
        return $campRepository->emailCampDistributor($distributor_name, $campName, $students, $activities, $distributor_email, $admin_name, $admin_email, $school_name, $code, $startDate, $endDate, $upgrade);
    }

    public function emailCampSchool($account_name, $students, $activities, $startDate, $endDate, $account_email, $upgrade = false){
        $campRepository = new CampRepository;
        return $campRepository->emailCampSchool($account_name, $students, $activities, $startDate, $endDate, $account_email, $upgrade);
    }

    public function getTeacherCamps($school_id, $teacher_id){
        $campRepository = new CampRepository;
        return $campRepository->getTeacherCamps($school_id, $teacher_id);
    }

    public function checkIfTeacherInCamp($camp_id, $teacher_id){
        $campRepository = new CampRepository;
        return $campRepository->checkIfTeacherInCamp($camp_id, $teacher_id);
    }

    public function checkIfStudentInCamp($camp_id, $student_id){
        $campRepository = new CampRepository;
        return $campRepository->checkIfStudentInCamp($camp_id, $student_id);
    }

    public function getAllDefaultActivitiesInCamp($camp_id, $limit = null, $search = null, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getAllDefaultActivitiesInCamp($camp_id, $limit, $search, $count);
    }

    public function checkIfActivityInCamp($camp_id, $activity_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->checkIfActivityInCamp($camp_id, $activity_id, $is_default);
    }

    public function getRemainingTasksInActivityInCamp($camp_id, $activity_id, $student_id, $count = null, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getRemainingTasksInActivityInCamp($camp_id, $activity_id, $count, $is_default);
    }

    public function getCompletedTasksInActivityInCamp($camp_id, $activity_id, $student_id, $count = null, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getCompletedTasksInActivityInCamp($camp_id, $activity_id, $student_id, $count, $is_default);
    }

    public function getStudentBlockingTasksInCamp($camp_id, $student_id){
        $campRepository = new CampRepository;
        return $campRepository->getStudentBlockingTasksInCamp($camp_id, $student_id);
    }

    public function getStudentLastPlayedTaskInCamp($camp_id, $student_id){
        $campRepository = new CampRepository;
        return $campRepository->getStudentLastPlayedTaskInCamp($camp_id, $student_id);
    }

    public function getCompletedTasksInCampActivity($camp_id, $activity_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getCompletedTasksInCampActivity($camp_id, $activity_id, $is_default);
    }

    public function getStudentRunningCamps($student_id, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getStudentRunningCamps($student_id, $count);
    }

    public function getCampDefaultActivities($camp_id, $limit = null, $search = null, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getCampDefaultActivities($camp_id, $limit, $search, $count);
    }

    public function getStudentSolvedActivityInCamp($camp_id, $activity_id, $student_id, $count = null){
        $campRepository = new CampRepository;
        return $campRepository->getStudentSolvedActivityInCamp($camp_id, $activity_id, $student_id, $count);
    }

    public function campsHasProgress($school_id, $camp_ids = null){
        $campRepository = new CampRepository;
        return $campRepository->campsHasProgress($school_id, $camp_ids);
    }

    public function getUserTaskProgressActivityCamp(Task $task, $user_id, $camp_id, $activity_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getUserTaskProgressActivityCamp($task, $user_id, $camp_id, $activity_id, $is_default);
    }

    public function storeCampActivityProgress(CampActivityProgress $campActivityProgress){
        $campRepository = new CampRepository;
        return $campRepository->storeCampActivityProgress($campActivityProgress);
    }

    public function deleteCampActivityProgress(CampActivityProgress $campActivityProgress){
        $campRepository = new CampRepository;
        return $campRepository->deleteCampActivityProgress($campActivityProgress);
    }

    public function getLastSolvedTaskInActivity($user_id, $camp_id, $activity_id, $is_default = true){
        $campRepository = new CampRepository;
        return $campRepository->getLastSolvedTaskInActivity($user_id, $camp_id, $activity_id, $is_default = true);
    }

    public function deleteCampUser(CampUser $campUser){
        $campRepository = new CampRepository;
        return $campRepository->deleteCampUser($campUser);
    }

    public function checkIfCampInSchool($camp_id, $school_id){
        $campRepository = new CampRepository;
        return $campRepository->checkIfCampInSchool($camp_id, $school_id);
    }

    public function getLastDistributorSubscription($user_id){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->getLastDistributorSubscription($user_id);
    }

    public function getOrderedStudentsInCamp($camp_id, $limit = null, $search = null, $count = false){
        $campRepository = new CampRepository();
        return $campRepository->getOrderedStudentsInCamp($camp_id, $limit, $search, $count);
    }
    
    public function sendCustomSubscriptionMail($school_name, $school_email, $plan_name = null, $students, $classrooms, $upgrade = false){
        $schoolRepository = new SchoolRepository;
        return $schoolRepository->sendCustomSubscriptionMail($school_name, $school_email, $plan_name, $students, $classrooms, $upgrade);
    }

}