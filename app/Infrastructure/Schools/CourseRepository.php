<?php

namespace App\Infrastructure\Schools;

use Analogue;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;
use App\DomainModelLayer\Schools\Classroom;
use DB;


class CourseRepository
{

    public function getCourseById($id)
    {
    	$courseMapper= Analogue::mapper(Course::class);
        return $courseMapper->query()->whereId($id)->first();
    }

    public function storeCourse(Course $course){
        $courseMapper= Analogue::mapper(Course::class);
        $courseMapper->store($course);
    }
    public function deleteCourse(Course $course){
        $courseMapper= Analogue::mapper(Course::class);
        $courseMapper->delete($course);
    }

    public function deleteCourseAdventureDeadlines(Course $course){
        $adventureDeadlines = $course->getAdventureDeadlines();

        foreach ($adventureDeadlines as $adventureDeadline){
            $adventureDeadlineMapper = Analogue::mapper(CourseAdventureDeadlines::class);
            $adventureDeadlineMapper->delete($adventureDeadline);
        }
    }

    public function getCompletedMissionsInCourse($course_id){
//        $missionMapper = Analogue::mapper(Mission::class);
//        return $missionMapper->query()->whereIn('task_id',(DB::table('student_progress')));
        return DB::table('student_progress')->where('course_id',$course_id)->whereNotNull('first_success')->get();
    }

    public function getNeverPlayedMissionsInCourse($course_id){
        //$missionMapper = Analogue::mapper(Mission::class);
        return DB::table('student_progress')->where('course_id',$course_id)->get();
    }

    public function checkCourseNameUniqueInClassroom(Classroom $classroom,$courseName){
        $courseMapper= Analogue::mapper(Course::class);
//        $queryOutput= $courseMapper->query()->whereNull('deleted_at')->where('name',$courseName)->whereIn('id',DB::table('course_group')
//            ->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->where('is_default',1)->where('classroom_id',$classroom->getId())->pluck('id')->toArray())->pluck('course_id')->toArray())
//            ->first();
        $queryOutput = $courseMapper->query()->whereNull('deleted_at')
            ->where('name',$courseName)->whereIn('id',function($queryx) use($classroom){
                $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($classroom){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')
                        ->where('is_default',1)->where('classroom_id',$classroom->getId());
                });
            })->first();

        if($queryOutput == null)
            return true;
        else
            return false;
    }

    public function getCourseAdventureDeadline($courseAdventureDeadlineId){
        $adventureDeadlineMapper = Analogue::mapper(CourseAdventureDeadlines::class);
        return $adventureDeadlineMapper->query()->whereId($courseAdventureDeadlineId)->first();
    }
}