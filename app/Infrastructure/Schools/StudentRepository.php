<?php

namespace App\Infrastructure\Schools;

use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\TaskOrder;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Schools\School;
use Analogue;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\StudentFinishCourse;
use App\DomainModelLayer\Schools\StudentPosition;
use App\DomainModelLayer\Schools\StudentClickMission;
use App\DomainModelLayer\Schools\StudentProgress;
use Carbon\Carbon;
use DB;

class StudentRepository
{

    public function getAllStudents($account_id,$limit = null,$grade_id = null,$search = null,$count = false)
    {
        $userMapper= Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id',$account_id)->whereIn('id',function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')->whereIn('role_id',function($queryy){
                $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
            });
        });
        if($grade_id != null){
            $query = $query->whereIn('id',function($queryx) use($grade_id){
                $queryx->select('user_id')->from('student_grade')
                    ->where('grade_id',$grade_id);
            });
        }
        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name','like','%'.$search.'%')
                    ->orWhere('last_name','like','%'.$search.'%')
                    ->orWhereIn('id',DB::table('group_user')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->where('is_default',1)->whereIn('classroom_id',
                        DB::table('classroom')->whereNull('deleted_at')->where('name','like','%'.$search.'%')->pluck('id')->toArray())->pluck('id')->toArray())->pluck('user_id')->toArray());
            });
        }
        if($limit != null){
            $query = $query->orderBy('username','asc')->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->orderBy('username','asc')->get();
        }
        return $query;
    }

    public function getStudentProgrss($task_id,$user_id,$course_id,$adventure_id){
        $progressMapper= Analogue::mapper(StudentProgress::class);
        return $progressMapper->query()->where('task_id',$task_id)->where('student_id',$user_id)->where('course_id',$course_id)->where('adventure_id',$adventure_id)->first();
    }

    public function storeStudentProgress(StudentProgress $student_progress){
        $progressMapper= Analogue::mapper(StudentProgress::class);
        $progressMapper->store($student_progress);
    }

    public function deleteStudentProgress(StudentProgress $student_progress){
        $progressMapper= Analogue::mapper(StudentProgress::class);
        $progressMapper->delete($student_progress);
    }

    public function storeStudentPosition(StudentPosition $student_position)
    {
        $progressMapper= Analogue::mapper(StudentPosition::class);
        DB::table('student_position')->insert(["created_at" => Carbon::now(),"updated_at"=>Carbon::now(),'user_id'=>$student_position->user->getId(),
            'course_id'=> $student_position->getCourse()->getId(),'adventure_id' => $student_position->adventure->getId(),
            'position_x' => $student_position->getPositionX() , 'position_y' => $student_position->getPositionY(), 'index' => $student_position->getIndex()]);
        //$progressMapper->store($student_position);
    }
    public function deleteStudentPosition(StudentPosition $student_position)
    {
        $progressMapper= Analogue::mapper(StudentPosition::class);
        $progressMapper->delete($student_position);
    }

    public function getStudentProgressByOrder($user_id, $order,$course_id,$adventure_id)
    {
        $courseMapper = Analogue::mapper(Course::class);
        $course = $courseMapper->query()->whereId($course_id)->first();
        $taskOrderMapper = Analogue::mapper(TaskOrder::class);
        $taskOrder = $taskOrderMapper->query()->where('order',$order)->where('journey_id',$course->getJourney()->getId())->where('adventure_id',$adventure_id)->first();
        if($taskOrder == null)
            throw new BadRequestException(trans("locale.previous_task_doesn't_exists"));
        $task = $taskOrder->getTask();
        $studentProgressMapper = Analogue::mapper(StudentProgress::class);
        $studentProgress = $studentProgressMapper->query()->where('task_id',$task->getId())->where('course_id',$course_id)->where('adventure_id',$adventure_id)->where('student_id',$user_id)->whereNotNull('first_success')->first();
        return $studentProgress;
    }

    public function getStudentProgressInAdventure($user_id,$adventure_id,$course_id)
    {
        $progressMapper= Analogue::mapper(StudentProgress::class);
        return $progressMapper->query()->where('student_id',$user_id)->where('course_id',$course_id)->where('adventure_id',$adventure_id)->get();
    }

    public function getTopStudents(Account $account,Grade $grade,$limit=10){
        $data =  DB::table('user')->leftJoin('student_progress','user.id','=','student_progress.student_id')
            ->select(DB::raw('user.id,user.first_name,user.last_name,COALESCE(sum(student_progress.score),0) as score,ceil(COALESCE(sum(student_progress.score),0)/20)as coins,classroom_data.name as classroomname'))
            ->leftJoin(DB::raw('(select classroom.name as name,group_user.user_id as user_id from classroom as classroom left join `group` on `group`.`classroom_id` = classroom.id left join `group_user` on group_user.group_id = `group`.`id` where `group`.`is_default` = 1 ) classroom_data'),function($join){
                $join->on('classroom_data.user_id','user.id');
            })
            ->whereNull('user.deleted_at')
            ->where('account_id',$account->getId())
            //->whereIn('user.id',DB::table('student_grade')->where('grade_id',$grade->getId())->pluck('user_id')->toArray())
            ->whereIn('user.id',function($queryx) use($grade){
                $queryx->select('user_id')->from('student_grade')
                    ->where('grade_id',$grade->getId());
            })
            //->whereIn('user.id',DB::table('user_role')->whereIn('role_id',DB::table('role')->where('name','student')->pluck('id')->toArray())->pluck('user_id')->toArray())
            ->whereIn('user.id',function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                    });
            })
            ->groupBy('user.id','user.first_name','user.last_name','classroom_data.name')->orderBy(DB::raw('score'),'DESC')->limit($limit)
            ->get();

        return $data;

    }

    public function getTopStudentsInClassroom(Account $account,Classroom $classroom,$limit=10){
        $data =  DB::table('user')->leftJoin('student_progress','user.id','=','student_progress.student_id')
            ->select(DB::raw('user.id,user.first_name,user.last_name,COALESCE(sum(student_progress.score),0) as score,ceil(COALESCE(sum(student_progress.score),0)/20)as coins'))
            ->whereNull('user.deleted_at')
            ->where('account_id',$account->getId())
            // ->whereIn('user.id',DB::table('group_user')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->where('classroom_id',$classroom->getId())->pluck('id')->toArray())->pluck('user_id')->toArray())
            ->whereIn('user.id',function($queryx) use($classroom){
                $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($classroom){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')
                        ->where('classroom_id',$classroom->getId());
                });
            })
            //->whereIn('user.id',DB::table('user_role')->whereIn('role_id',DB::table('role')->where('name','student')->pluck('id')->toArray())->pluck('user_id')->toArray())
            ->whereIn('user.id',function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                    });
            })
            ->groupBy('user.id','user.first_name','user.last_name')->orderBy(DB::raw('score'),'DESC');
        if($limit != null){
            $data = $data->limit($limit);
        }
        $data = $data->get();
        return $data;
    }

    public function storeStudentClickMission(StudentClickMission $studentClickMission){
        $studentMissionClickMapper= Analogue::mapper(StudentClickMission::class);
        $studentMissionClickMapper->store($studentClickMission);
    }

    public function storeStudentFinishCourse(StudentFinishCourse $studentFinishCourse){
        $studentFinishCourseMapper= Analogue::mapper(StudentFinishCourse::class);
        $studentFinishCourseMapper->store($studentFinishCourse);
    }
    public function getStoreStudentClickMissionByTimeAndUser(User $user,$time){
        $studentMissionClickMapper= Analogue::mapper(StudentClickMission::class);
        return $studentMissionClickMapper->query()->where('user_id',$user->getId())->where('time',$time)->first();
    }

    public function getStudentMissionClickByTime(Account $account,$from_time,$to_time,$classroom_ids = array()){

        $studentMissionClickMapper = DB::table('student_click_mission')
            ->select(DB::raw('count(`student_click_mission`.`user_id`) as number,time'))
            //->whereIn('student_click_mission.user_id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','student')->pluck('id')->toArray()))->pluck('user_id')->toArray()))
            ->whereIn('student_click_mission.user_id',function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                    });
            })
            //->whereIn('student_click_mission.user_id',DB::table('user')->whereNull('deleted_at')->where('account_id',$account->getId())->pluck('id')->toArray());
            ->whereIn('student_click_mission.user_id',function($queryx) use($account){
                $queryx->select('id')->from('user')->whereNull('deleted_at')
                    ->where('account_id',$account->getId());
            });
        if(sizeof($classroom_ids)>0){
            //$studentMissionClickMapper = $studentMissionClickMapper->whereIn('student_click_mission.user_id',DB::table('group_user')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->whereIn('classroom_id',$classroom_ids)->pluck('id')->toArray())->pluck('user_id')->toArray());
            $studentMissionClickMapper = $studentMissionClickMapper->whereIn('student_click_mission.user_id',function($queryx) use($classroom_ids){
                $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($classroom_ids){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')
                        ->whereIn('classroom_id',$classroom_ids);
                });
            });
        }


        return $studentMissionClickMapper->whereBetween('time',[$from_time,$to_time])->groupBy('time')->get();
    }

    public function getStudentSolvedMissions(Account $account,$from_time,$to_time,$classroom_ids = array()){

        $studentProgressMapper = DB::table('student_progress')
            ->select(DB::raw('count(`student_progress`.`student_id`) as number, Date(updated_at) as time'))
            //->whereIn('student_progress.student_id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','student')->pluck('id')->toArray()))->pluck('user_id')->toArray()))
            ->whereIn('student_id',function($queryx){
                $queryx->select('student_progress.student_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                    });
            })
            //->whereIn('student_progress.student_id',DB::table('user')->whereNull('deleted_at')->where('account_id',$account->getId())->pluck('id')->toArray());
            ->whereIn('student_progress.student_id',function($queryx) use($account){
                $queryx->select('id')->from('user')->whereNull('deleted_at')
                    ->where('account_id',$account->getId());
            });
        if(sizeof($classroom_ids)>0){
            //$studentProgressMapper = $studentProgressMapper->whereIn('student_progress.student_id',DB::table('group_user')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->whereIn('classroom_id',$classroom_ids)->pluck('id')->toArray())->pluck('user_id')->toArray());
            $studentProgressMapper = $studentProgressMapper->whereIn('student_progress.student_id',function($queryx) use($classroom_ids){
                $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($classroom_ids){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')
                        ->whereIn('classroom_id',$classroom_ids);
                });
            });
        }


        return $studentProgressMapper->whereNotNull('first_success')->whereBetween(DB::raw('Date(updated_at)'),[$from_time,$to_time])->groupBy(DB::raw('Date(updated_at)'))->get();
    }

    public function getIfStudentsProgressedInClassroom(Classroom $classroom,$students){
        //$groupIds = DB::table('group')->where('is_default',1)->whereNull('deleted_at')->where('classroom_id',$classroom->getId())->pluck('id')->toArray();
        //$studentsIds = DB::table('group_user')->whereIn('group_id',$groupIds)->pluck('user_id')->toArray();
        //$courseIds = DB::table('course_group')->whereIn('group_id',$groupIds)->pluck('course_id')->toArray();
        //$count =  DB::table('student_progress')->whereIn('student_id',$students)->whereIn('course_id',$courseIds)->count();
        $count = DB::table('student_progress')->whereIn('student_id',$students)
            ->whereIn('course_id',function($queryx) use($classroom){
                $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($classroom){
                    $queryy->select('id')->from('group')->where('is_default',1)->whereNull('deleted_at')
                        ->where('classroom_id',$classroom->getId());
                });
            })->count();
        if($count >0)
            return true;
        else
            return false;
    }

    public function getStudentsFinishedCourse(Course $course){
        $studentMapper = Analogue::mapper(User::class);
        return $studentMapper->query()->where('course_id',$course->getId())->get();
    }

    public function getFinishedCourseByAllStudentsInSchool(School $school){
        return DB::table('course')
            ->select(DB::raw('`course`.`id` as course_id,`course`.`name` as course_name,Max(`student_finish_course`.`time`) as time ,count(`student_finish_course`.`course_id`) as studentFinishCourse,count(`group_user`.`user_id`) as studentsInCourse'))
            ->leftJoin('student_finish_course','course.id','=','student_finish_course.course_id')
            ->leftJoin('course_group','course.id','=','course_group.course_id')
            ->leftJoin('group_user','course_group.group_id','=','group_user.group_id')
            ->whereNull('course.deleted_at')
            //->whereIn('course.id',DB::table('course_group')->whereIn('group_id',DB::table('group')->whereNull('deleted_at')->whereIn('classroom_id',DB::table('classroom')
            //    ->whereNull('deleted_at')->whereIn('school_id',[$school->getId()])->pluck('id')->toArray())->pluck('id')->toArray())->pluck('course_id')->toArray())
            ->whereIn('course.id',function($queryx) use($school){
                $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($school){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')
                        ->whereIn('classroom_id',function($queryz) use($school){
                            $queryz->select('id')->from('classroom') ->whereNull('deleted_at')
                                ->whereIn('school_id',[$school->getId()]);
                        });
                });
            })
            //->whereIn('group_user.user_id',DB::table('user_role')->whereIn('role_id',DB::table('role')->where('name','student')->pluck('id')->toArray())->pluck('user_id')->toArray())
            ->whereIn('group_user.user_id',function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                    });
            })
            ->groupBy('course.id','course.name')
            ->get();
    }

    public function getStudentBlockingTasks($student_id)
    {
        $progressMapper = Analogue::mapper(StudentProgress::class);
        return $progressMapper->query()->where('student_id',$student_id)->where('no_trials','>',3)
            ->whereNull('first_success')->get();
    }

    public function getStudentAdventureDeadlinePassed(User $user){
        //$courseIds = DB::table('course')->whereNull('deleted_at')
        //->whereIn('id',DB::table('course_group')->whereIn('group_id',DB::table('group_user')->where('user_id',$user->getId())->pluck('group_id')->toArray())->pluck('course_id')->toArray())->pluck('id')->toArray();
        //return DB::table('course_adventure_deadlines')
        //    ->whereIn('course_id',$courseIds)
        //    ->whereDate('course_adventure_deadlines.ends_at','<',Carbon::now())->get(['course_adventure_deadlines.id']);
        return DB::table('course_adventure_deadlines')
            ->whereIn('course_id',function($queryx) use($user){
                $queryx->select('id')->from('course')->whereNull('deleted_at')
                    ->whereIn('id',function($queryy)use($user){
                        $queryy->select('course_id')->from('course_group')
                            ->whereIn('group_id',function($queryz) use($user){
                                $queryz->select('group_id')->from('group_user')->where('user_id',$user->getId());
                            });
                    });
            })
            ->whereDate('course_adventure_deadlines.ends_at','<',Carbon::now())->get(['course_adventure_deadlines.id']);
    }

    public function getStudentLastPlayedTask($student_id)
    {
        $lastMapper = Analogue::mapper(StudentClickMission::class);
        return $lastMapper->query()->where('user_id',$student_id)->orderBy('time','DESC')->first();
    }



}