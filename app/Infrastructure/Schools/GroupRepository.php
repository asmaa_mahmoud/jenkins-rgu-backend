<?php
 namespace App\Infrastructure\Schools;

 use Analogue;
 use App\DomainModelLayer\Schools\Group;
 use App\DomainModelLayer\Schools\GroupMember;
 use App\DomainModelLayer\Accounts\User;
 use DB;

 class GroupRepository
 {

     public function storeGroup(Group $group){
         $classroomMapper= Analogue::mapper(Group::class);
         $classroomMapper->store($group);
     }

     public function storeGroupMember(GroupMember $groupMember){
        $groupMemberMapper = Analogue::mapper(GroupMember::class);
        $groupMemberMapper->store($groupMember);
     }

     public function deleteGroupMember(GroupMember $groupMember){
        $groupMemberMapper = Analogue::mapper(GroupMember::class);
        $groupMemberMapper->delete($groupMember);
     }

     public function deleteGroupMembers(User $user){
        $groupMembers  = $user->getAllGroupMembers();
        foreach($groupMembers as $groupMember){
            $this->deleteGroupMember($groupMember);
        }
     }

     public function getStudentsInGroup(Group $group,$limit,$search,$count = false){
        $userMapper = Analogue::mapper(User::class);
        //$query = $userMapper->query()
        //            ->whereIn('id', DB::table('group_user')->where('group_id',$group->getId())->pluck('user_id')->toArray())
        //            ->whereIn('id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','student')->pluck('id')->toArray()))->pluck('user_id')->toArray()));


        $query = $userMapper->query()->whereIn('id',function($queryx) use($group){
            $queryx->select('user_id')->from('group_user')->where('group_id',$group->getId());
        })->whereIn('id',function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')->whereIn('role_id',function($queryy){
                $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
            });
        });
        if($search != null){
        $query = $query->where(function ($innerQuery) use($search) {
             $innerQuery->where('username', 'like', '%'.$search.'%')
             ->orWhere('first_name','like','%'.$search.'%')
             ->orWhere('last_name','like','%'.$search.'%');
        });
        }
        if($limit != null){
             $query = $query->paginate($limit);
        }
        else if($count == true){
                    $query = $query->count();
                }
        else{
             $query = $query->get();
        }
        return $query;
     }

 }