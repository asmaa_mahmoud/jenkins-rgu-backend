<?php

namespace App\Infrastructure\Schools;

use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Journeys\ActivityTask;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Schools\CampActivity;
use App\DomainModelLayer\Schools\CampActivityProgress;
use App\DomainModelLayer\Schools\CampUser;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use Analogue;
use App\DomainModelLayer\Schools\SchoolCamp;
use App\DomainModelLayer\Schools\SchoolCampDetail;
use App\DomainModelLayer\Schools\SchoolDistributorCamp;
use App\Helpers\PaymentModule;
use Mail;
use Carbon\Carbon;
use App\Framework\Exceptions\BadRequestException;
use DB;

class CampRepository
{
    public function getCampById($id){
        $campMapper = Analogue::mapper(Camp::class);
        return $campMapper->query()->whereId($id)->first();
    }

    public function deleteCamp($camp){
        $classroomMapper = Analogue::mapper(Camp::class);
        $classroomMapper->delete($camp);
    }

    public function getAllCampsByStatus($school_id, $status, $limit = null, $search = null, $count = null, $camp_ids = null){
        $campMapper = Analogue::mapper(Camp::class);
        if($status == 'locked'){
            $query = $campMapper->query()->whereIn('id', function ($q) use ($school_id, $status) {
                $q->select('camp_id')
                    ->from('school_camp')
                    ->where('school_id', $school_id)
                    ->where('locked', 0);
            });
        }
        else if ($status == 'ended'){
            $query = $campMapper->query()->whereIn('id', function ($q) use ($school_id, $status) {
                $q->select('camp_id')
                    ->from('school_camp')
                    ->where('school_id', $school_id)
                    ->where('locked', 1)
                    ->where('ends_at', '<', Carbon::now()->copy()->endOfDay()->timestamp);
            });
        }
        else if ($status == 'upcoming'){
            $query = $campMapper->query()->whereIn('id', function ($q) use ($school_id, $status) {
                $q->select('camp_id')
                    ->from('school_camp')
                    ->where('school_id', $school_id)
                    ->where('locked', 1)
                    ->where('starts_at', '>', Carbon::now()->copy()->startOfDay()->timestamp);
            });
        }
        else {
            $query = $campMapper->query()->whereIn('id', function ($q) use ($school_id, $status) {
                $q->select('camp_id')
                    ->from('school_camp')
                    ->where('school_id', $school_id)
                    ->where('locked', 1)
                    ->where('starts_at', '<=', Carbon::now()->copy()->startOfDay()->timestamp)
                    ->where('ends_at', '>', Carbon::now()->copy()->endOfDay()->timestamp);
            });
        }

        if($camp_ids != null){
            $query = $query->whereIn('id', $camp_ids);
        }

        if($search != null){
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getInActiveCampsInSchool($school_id, $count = null){
        $schoolCampMapper = Analogue::mapper(Camp::class);
        $query = $schoolCampMapper->query()->whereIn('id', function ($q) use ($school_id) {
            $q->select('camp_id')->from('school_camp')
                ->where('school_id', $school_id)
                ->where('locked', 0);
        });
        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function getActiveCampsInSchool($school_id, $count = null, $camp_ids = null){
        $schoolCampMapper = Analogue::mapper(Camp::class);
        $query = $schoolCampMapper->query()->whereIn('id', function ($q) use ($school_id) {
            $q->select('camp_id')->from('school_camp')
                ->where('school_id', $school_id)
                ->where('locked', 1)
                ->where('ends_at', '>=', Carbon::now()->timestamp);
        });
        if($camp_ids != null){
            $query = $query->whereIn('id', $camp_ids);
        }

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function checkIfActiveCamp($camp_id){
        $schoolCampMapper = Analogue::mapper(SchoolCamp::class);
        $schoolCamp = $schoolCampMapper->query()->where('camp_id', $camp_id)->first();
        if($schoolCamp->locked == 0 || $schoolCamp->starts_at > Carbon::now()->timestamp || $schoolCamp->ends_at < Carbon::now()->timestamp)
            return false;
        else
            return true;
    }

    public function getRunningCampsInSchool($school_id, $count = null, $teacher_id = null){
        $schoolCampMapper = Analogue::mapper(Camp::class);
        $query = $schoolCampMapper->query()->whereIn('id', function ($q) use ($school_id) {
            $q->select('camp_id')->from('school_camp')
                ->where('school_id', $school_id)
                ->where('locked', 1)
                ->where('starts_at', '<=', Carbon::now()->timestamp)
                ->where('ends_at', '>=', Carbon::now()->timestamp);
        });
        if($teacher_id != null){
            $query->whereIn('id', function ($q) use ($teacher_id) {
                $q->select('camp_id')->from('camp_user')->where('user_id', $teacher_id);
            });
        }

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function getCountOfPeopleInActiveCamps($school_id, $roleName){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->whereIn('id', function($q) use ($school_id){
            $q->select('user_id')->from('camp_user')->whereIn('camp_id', function($q) use ($school_id) {
                $q->select('camp_id')->from('school_camp')->where('school_id', $school_id)
                    ->where('locked', 1)
                    ->where('ends_at', '>=', Carbon::now()->timestamp);
                });
            })->whereIn('id', function($qu) use ($roleName){
                $qu->select('user_id')->from('user_role')->whereIn('role_id', function ($query) use ($roleName) {
                    $query->select('id')->from('role')->whereNull('deleted_at')->where('name', $roleName);
                });
            });
        return $query->count();
    }

    public function getUpcomingCampsInSchool($school_id, $count = null, $teacher_id = null){
        $schoolCampMapper = Analogue::mapper(Camp::class);
        $query = $schoolCampMapper->query()->whereIn('id', function ($q) use ($school_id){
            $q->select('camp_id')->from('school_camp')
                ->where('school_id', $school_id)
                ->where('locked', 1)
                ->where('starts_at', '>', Carbon::now()->timestamp);
        });
        if($teacher_id != null){
            $query->whereIn('id', function ($q) use ($teacher_id) {
                $q->select('camp_id')->from('camp_user')->where('user_id', $teacher_id);
            });
        }

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function getTopStudentsInCamp($account, $camp_id, $limit){
        $data = DB::table('user')->leftJoin('camp_activity_progress', 'user.id', '=', 'camp_activity_progress.user_id')
            ->select(DB::raw('user.id, user.first_name, user.last_name, COALESCE(sum(camp_activity_progress.score),0) as score, ceil(COALESCE(sum(camp_activity_progress.score),0)/20)as coins'))
            ->where('camp_activity_progress.camp_id', $camp_id)
            ->whereNull('user.deleted_at')
            ->where('account_id', $account->getId())
            ->whereIn('user.id', function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->groupBy('user.id', 'user.first_name', 'user.last_name')->orderBy(DB::raw('score'), 'DESC')->limit($limit)
            ->get();
        return $data;
    }

    public function getStudentSolveTaskByTime(Account $account, $from_time, $to_time, $camp_ids = null){
        $studentProgressMapper = DB::table('camp_activity_progress')
            ->select(DB::raw('count(DISTINCT(`user_id`)) as number, Date(updated_at) as timer'))
            ->whereIn('user_id', function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->whereIn('user_id', function($queryx) use ($account){
                $queryx->select('id')->from('user')->whereNull('deleted_at')->where('account_id', $account->getId());
            });
        if($camp_ids != null){
            $studentProgressMapper = $studentProgressMapper->whereIn('camp_id', $camp_ids);
        }
        return $studentProgressMapper->whereNull('deleted_at')->whereNotNull('first_success')
            ->whereBetween(DB::raw('Date(updated_at)'), [$from_time, $to_time])->groupBy(DB::raw('Date(`updated_at`)'))->get();
    }

    public function getTaskSolvedByStudentByTime(Account $account, $from_time, $to_time, $camp_ids = null){
        $studentProgressMapper = DB::table('camp_activity_progress')
            ->select(DB::raw('count(DISTINCT(`task_id`)) as number, Date(updated_at) as timer'))
            ->whereIn('user_id', function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->whereIn('camp_activity_progress.user_id', function($queryx) use ($account){
                $queryx->select('id')->from('user')->whereNull('deleted_at')->where('account_id', $account->getId());
            });
        if($camp_ids != null){
            $studentProgressMapper = $studentProgressMapper->whereIn('camp_activity_progress.camp_id', $camp_ids);
        }
        return $studentProgressMapper->whereNull('deleted_at')->whereNotNull('first_success')
            ->whereBetween(DB::raw('Date(updated_at)'), [$from_time, $to_time])->groupBy(DB::raw('Date(updated_at)'))->get();
    }

    public function getAllStudentsInCamp($camp_id, $limit = null, $search = null, $count = false){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()
            ->whereIn('id', function($queryx) use ($camp_id){
                $queryx->select('user_id')->from('camp_user')->where('camp_id', $camp_id);
            })->whereIn('id', function($quer){
                $quer->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            });
        if($search != null){
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function getOrderedStudentsInCamp($camp_id, $limit = null, $search = null, $count = false){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->whereIn('user.id', function($queryx) use ($camp_id){
            $queryx->select('camp_user.user_id')->from('camp_user')->where('camp_user.camp_id', $camp_id)->whereIn('camp_user.role_id', function($queryy){
                $queryy->select('role.id')->from('role')->whereNull('role.deleted_at')->where('role.name', 'student');
            });
        })->leftJoin('camp_activity_progress', 'user.id', '=', 'camp_activity_progress.user_id')
            ->where('camp_activity_progress.camp_id', $camp_id)
            ->groupBy('user.id')
            ->orderBy(DB::raw('camp_score'), 'DESC');

        if($search != null){
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit, [DB::raw('SUM(camp_activity_progress.score) as camp_score'),'user.*','user.id as pop']);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function getAllTeachersInCamp($camp_id, $limit = null, $search = null, $count = false){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->whereIn('id', function($queryx) use($camp_id){
            $queryx->select('user_id')->from('camp_user')->where('camp_id', $camp_id);
        })->whereIn('id', function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id',function($queryy){
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
                });
        });
        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function checkCampNameUniqueInSchool(School $school, $campName){
        $campMapper = Analogue::mapper(Camp::class);
        $queryOutput = $campMapper->query()->whereNull('deleted_at')->whereIn('id', function($q) use ($school){
            $q->select('camp_id')->from('school_camp')->where('school_id', $school->getId());
        })->where('name', $campName)->first();
        if($queryOutput == null)
            return true;
        else
            return false;
    }

    public function getCampActivity($camp_id, $activity_id, $is_default = true){
        $campActivityMapper = Analogue::mapper(CampActivity::class);
        if($is_default)
            return $campActivityMapper->query()->where('camp_id', $camp_id)->where('default_activity_id', $activity_id)->first();
        else
            return $campActivityMapper->query()->where('camp_id', $camp_id)->where('activity_id', $activity_id)->first();
    }

    public function getLastOrderInActivityCamp($camp_id){
        $campActivityMapper = Analogue::mapper(CampActivity::class);
        return $campActivityMapper->query()->where('camp_id', $camp_id)->orderBy('order', 'DESC')->first();
    }

    public function storeCamp(Camp $camp){
        $campMapper = Analogue::mapper(Camp::class);
        $campMapper->store($camp);
    }

    public function getAllStudentsInCamps($school_id, $grade_id = null, $limit = null, $search = null, $count = null){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', function ($q) use ($school_id) {
            $q->select('account_id')->from('school')->where('id', $school_id);
        })->whereIn('id', function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id', function($queryy){
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                });
        });

        if($grade_id != null){
            $query = $query->whereIn('id', function($queryx) use ($grade_id) {
                $queryx->select('user_id')->from('student_grade')->where('grade_id', $grade_id);
            });
        }

        if($search != null){
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getAllTeachersInCamps($school_id, $limit = null, $search = null, $count = null){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', function ($q) use ($school_id) {
            $q->select('account_id')->from('school')->where('id', $school_id);
        })->whereIn('id', function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id', function($queryy){
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
                });
        });

        if($search != null){
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getAssignedStudentsInCamp($camp_id, $count = null){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->whereIn('id', function($q) use ($camp_id) {
            $q->select('user_id')->from('camp_user')->where('camp_id', $camp_id);
        })
        ->whereIn('role_id', function ($query) {
            $query->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
        })
        ->groupBy('user_id');

        if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function checkIfPaidCamp($camp_id){
        $schoolCampMapper = Analogue::mapper(SchoolCamp::class);
        $schoolCamp = $schoolCampMapper->query()->where('camp_id', $camp_id)->first();
        if($schoolCamp->locked == 0)
            return false;
        else
            return true;
    }

    public function checkIfEndedCamp($camp_id){
        $schoolCampMapper = Analogue::mapper(SchoolCamp::class);
        $schoolCamp = $schoolCampMapper->query()->where('camp_id', $camp_id)->first();
        if($schoolCamp->ends_at < Carbon::now()->timestamp)
            return false;
        else
            return true;
    }

    public function getCampStatus($camp_id){
        $schoolCampMapper = Analogue::mapper(SchoolCamp::class);
        $schoolCamp = $schoolCampMapper->query()->where('camp_id', $camp_id)->first();
        if($schoolCamp->locked == 0){
            $status = 'locked';
        }
        else{
            if($schoolCamp->ends_at < Carbon::now()->timestamp)
                $status = 'ended';
            else if ($schoolCamp->starts_at > Carbon::now()->timestamp)
                $status = 'upcoming';
            else
                $status = 'running';
        }
        return $status;
    }

    public function deleteCampActivity(CampActivity $campActivity){
        $campActivityMapper = Analogue::mapper(CampActivity::class);
        $campActivityMapper->delete($campActivity);
    }

    public function deleteCampUsers(CampUser $campUser){
        $campUserMapper = Analogue::mapper(CampUser::class);
        $campUserMapper->delete($campUser);
    }

    public function getCampUser($camp_id, $userId){
        $campUserMapper = Analogue::mapper(CampUser::class);
        return $campUserMapper->query()->where('camp_id', $camp_id)->where('user_id', $userId)->first();
    }

    public function deleteCampUser(CampUser $campUser){
        $campUserMapper = Analogue::mapper(CampUser::class);
        $campUserMapper->delete($campUser);
    }

    public function emailCampDistributor($distributorName, $campName, $students, $activities, $distributorEmail, $admin_name, $admin_email, $school_name, $code, $startDate, $endDate, $upgrade = false){
        if($distributorEmail != null || $distributorEmail != '') {
            try {
                Mail::send('emails.camp_distributor', [
                    'username' => $distributorName,
                    'campName' => $campName,
                    'students' => $students,
                    'activities' => $activities,
                    'admin_name' => $admin_name,
                    'admin_email' => $admin_email,
                    'school_name' => $school_name,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'code' => $code,
                    'upgrade' => $upgrade
                ], function ($m) use ($distributorEmail) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($distributorEmail)->subject('RoboGarden team: New Camp');
                });
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
    }

    public function emailCampSchool($account_name, $students, $activities, $startDate, $endDate, $account_email, $upgrade = false){
        if($account_email != null || $account_email != '') {
            try {
                Mail::send('emails.camp_school', [
                    'username' => $account_name,
                    'students' => $students,
                    'activities' => $activities,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'upgrade' => $upgrade,
                ], function ($m) use ($account_email) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($account_email)->subject(trans('locale.camp_subscription_subject'));
                });

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
    }

    public function getTeacherCamps($school_id, $teacher_id){
        $campUserMapper = Analogue::mapper(Camp::class);
        $campUser = $campUserMapper->query()
            ->whereIn('id', function ($q) use ($school_id){
                $q->select('camp_id')->from('school_camp')->where('school_id', $school_id);
            })
            ->whereIn('id', function ($q) use ($teacher_id) {
                $q->select('camp_id')->from('camp_user')
                    ->where('user_id', $teacher_id)
                    ->whereIn('role_id', function ($query) {
                        $query->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
                    });
            })->get();
        return $campUser;
    }

    public function checkIfTeacherInCamp($camp_id, $teacher_id){
        $campUserMapper = Analogue::mapper(CampUser::class);
        $campUser = $campUserMapper->query()->where('camp_id', $camp_id)->where('user_id', $teacher_id)->whereIn('role_id', function ($q){
            $q->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
        })->first();

        if($campUser != null)
            return true;

        return false;
    }

    public function checkIfStudentInCamp($camp_id, $student_id){
        $campUserMapper = Analogue::mapper(CampUser::class);
        $campUser = $campUserMapper->query()->where('camp_id', $camp_id)->where('user_id', $student_id)->whereIn('role_id', function ($q){
            $q->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
        })->first();

        if($campUser != null)
            return true;

        return false;
    }

    public function getAllDefaultActivitiesInCamp($camp_id, $limit = null, $search = null, $count = null){
        $activityMapper = Analogue::mapper(DefaultActivity::class);
        $query = $activityMapper->query()->whereIn('id', function ($q) use ($camp_id) {
            $q->select('default_activity_id')->from('camp_activity')->where('camp_id', $camp_id)->whereNull('deleted_at');
        });

        if($search != null){
            $query = $query->whereIn('id', function ($innerQuery) use ($search) {
                $innerQuery->select('default_activity_id')->from('activity_translation')->where('name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function checkIfActivityInCamp($camp_id, $activity_id, $is_default = true){
        $campActivityMapper = Analogue::mapper(CampActivity::class);
        $query = $campActivityMapper->query()->where('camp_id', $camp_id);
        if($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);
        return $query->first();
    }

    public function getRemainingTasksInActivityInCamp($camp_id, $activity_id, $student_id, $count = null, $is_default = true){
        $taskMapper = Analogue::mapper(Task::class);
        $query = $taskMapper->query()->whereIn('id', function($q) use ($camp_id, $student_id){
            $q->select('task_id')->from('camp_activity_progress')->where('camp_id', $camp_id)->where('user_id', $student_id)->whereNull('first_success');
        });
        if($is_default)
            $query = $query->whereIn('id', function($q) use ($activity_id){
                $q->select('task_id')->from('camp_activity_progress')->where('default_activity_id', $activity_id);
            });
        else
            $query = $query->whereIn('id', function($q) use ($activity_id){
                $q->select('task_id')->from('camp_activity_progress')->where('activity_id', $activity_id);
            });

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getCompletedTasksInActivityInCamp($camp_id, $activity_id, $student_id, $count = null, $is_default = true){
        $taskMapper = Analogue::mapper(Task::class);
        $query = $taskMapper->query()->whereIn('id', function($q) use ($camp_id, $student_id){
            $q->select('task_id')->from('camp_activity_progress')->where('camp_id', $camp_id)->where('user_id', $student_id)->whereNotNull('first_success');
        });
        if($is_default)
            $query = $query->whereIn('id', function($q) use ($activity_id){
                $q->select('task_id')->from('camp_activity_progress')->where('default_activity_id', $activity_id);
            });
        else
            $query = $query->whereIn('id', function($q) use ($activity_id){
                $q->select('task_id')->from('camp_activity_progress')->where('activity_id', $activity_id);
            });

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getStudentBlockingTasksInCamp($camp_id, $student_id){
        $progressMapper = Analogue::mapper(CampActivityProgress::class);
        return $progressMapper->query()->where('camp_id', $camp_id)->where('user_id', $student_id)->where('no_of_trials', '>', 3)->whereNull('first_success')->get();
    }

    public function getStudentLastPlayedTaskInCamp($camp_id, $student_id){
        $lastMapper = Analogue::mapper(CampActivityProgress::class);
        return $lastMapper->query()->where('user_id', $student_id)->where('camp_id', $camp_id)->orderBy('updated_at', 'DESC')->first();
    }

    public function getCompletedTasksInCampActivity($camp_id, $activity_id, $is_default = true){
        if($is_default)
            return DB::table('camp_activity_progress')->where('camp_id', $camp_id)->where('default_activity_id', $activity_id)->whereNotNull('first_success')->get();
        else
            return DB::table('camp_activity_progress')->where('camp_id', $camp_id)->where('activity_id', $activity_id)->whereNotNull('first_success')->get();
    }

    public function getStudentRunningCamps($student_id, $count = null){
        $campMapper = Analogue::mapper(Camp::class);
        $query = $campMapper->query()->whereIn('id', function ($q){
            $q->select('camp_id')->from('school_camp')->where('locked', 1)
                ->where('starts_at', '<=', Carbon::now()->timestamp)->where('ends_at', '>=', Carbon::now()->timestamp);
        })->whereIn('id', function ($qu) use ($student_id) {
            $qu->select('camp_id')->from('camp_user')->where('user_id', $student_id);
        });

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getCampDefaultActivities($camp_id, $limit = null, $search = null, $count = null){
        $activityMapper = Analogue::mapper(DefaultActivity::class);
        $query = $activityMapper->query()->whereIn('id', function ($q) use ($camp_id) {
            $q->select('default_activity_id')->from('camp_activity')->where('camp_id', $camp_id);
        });

        if($search != null){
            $query = $query->whereIn('id', function ($innerQuery) use ($search) {
                $innerQuery->select('default_activity_id')->from('activity_translation')->where('name', 'like', '%'.$search.'%');
            });
        }

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getStudentSolvedActivityInCamp($camp_id, $activity_id, $student_id, $count = null){
        $taskMapper = Analogue::mapper(Task::class);
        $query = $taskMapper->query()->whereIn('id', function ($q) use ($camp_id, $activity_id, $student_id) {
            $q->select('task_id')->from('camp_activity_progress')->where('camp_id', $camp_id)
                ->where('default_activity_id', $activity_id)->where('user_id', $student_id)->whereNotNull('first_success');
        });

        if($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function campsHasProgress($school_id, $camp_ids = null){
        $campMapper = Analogue::mapper(Camp::class);
        $query = $campMapper->query()->whereIn('id', function ($que) use ($school_id) {
            $que->select('camp_id')->from('school_camp')->where('school_id', $school_id);
        })->whereIn('id', function($q){
            $q->select('camp_id')->from('camp_activity_progress')->whereIn('user_id', function($q){
                $q->select('user_id')->from('user_role')->whereIn('role_id', function ($qu){
                    $qu->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                });
            });
        });

        if($camp_ids != null)
            $query = $query->whereIn('id', $camp_ids);

        return $query->get();
    }

    public function getUserTaskProgressActivityCamp(Task $task, $user_id, $camp_id, $activity_id, $is_default = true){
        $progressMapper = Analogue::mapper(CampActivityProgress::class);
        //dd($task->getId(),$camp_id,$user_id,$activity_id);
        $taskProgress = $progressMapper->query()->where('camp_id', $camp_id)->where('task_id', $task->getId())->where('user_id', $user_id);
        if($is_default)
            $taskProgress = $taskProgress->where('default_activity_id', $activity_id)->first();
        else
            $taskProgress = $taskProgress->where('activity_id', $activity_id)->first();
        return $taskProgress;
    }

    public function storeCampActivityProgress(CampActivityProgress $campActivityProgress){
        $campActivityProgressMapper = Analogue::mapper(CampActivityProgress::class);
        $campActivityProgressMapper->store($campActivityProgress);
    }

    public function deleteCampActivityProgress(CampActivityProgress $campActivityProgress){
        $campActivityProgressMapper = Analogue::mapper(CampActivityProgress::class);
        $campActivityProgressMapper->delete($campActivityProgress);
    }

    public function getLastSolvedTaskInActivity($user_id, $camp_id, $activity_id, $is_default = true){
        $campProgressActivityMapper = Analogue::mapper(CampActivityProgress::class);
        if($is_default)
            $taskProgressActivity = $campProgressActivityMapper->where('default_activity_id', $activity_id);
        else
            $taskProgressActivity = $campProgressActivityMapper->where('activity_id', $activity_id);

        $taskProgressActivities = $taskProgressActivity->where('camp_id', $camp_id)->where('user_id', $user_id)->whereNotNull('first_success')->get();
        $task_ids = [];
        foreach ($taskProgressActivities as $taskProgressActivityOne){
            array_push($task_ids, $taskProgressActivityOne->getTask()->getId());
        }

        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id', $activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id', $activity_id);

        $activityTask = $activityTask->whereIn('task_id', $task_ids)->orderBy('order', 'desc')->first();
        return $activityTask;
    }

    public function storeCampUser(CampUser $campUser){
        $campUserMapper = Analogue::mapper(CampUser::class);
        $campUserMapper->store($campUser);
    }

    public function storeSchoolCamp(SchoolCamp $schoolCamp){
        $schoolCampMapper = Analogue::mapper(SchoolCamp::class);
        $schoolCampMapper->store($schoolCamp);
    }

    public function storeSchoolCampDetail(SchoolCampDetail $schoolCampDetail){
        $schoolCampDetailMapper = Analogue::mapper(SchoolCampDetail::class);
        $schoolCampDetailMapper->store($schoolCampDetail);
    }

    public function storeSchoolCampDistributor(SchoolDistributorCamp $schoolDistributorCamp){
        $schoolDistributorCampMapper = Analogue::mapper(SchoolDistributorCamp::class);
        $schoolDistributorCampMapper->store($schoolDistributorCamp);
    }

    public function checkIfCampInSchool($camp_id, $school_id){
        $schoolCampMapper = Analogue::mapper(SchoolCamp::class);
        $schoolCamp = $schoolCampMapper->query()->where('camp_id', $camp_id)->where('school_id', $school_id)->first();
        if($schoolCamp != null)
            return true;

        return false;
    }

    public function storeCampActivity(CampActivity $campActivity){
        $campActivityMapper = Analogue::mapper(CampActivity::class);
        $campActivityMapper->store($campActivity);
    }

    public function deleteCampActivities($camp_id){
        return DB::table('camp_activity')->where('camp_id', $camp_id)->delete();
    }

    public function getActivityTaskOrder($activity_id, $task_id, $is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        $query = $activityTaskMapper->query()->where('task_id', $task_id);
        if($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        return $query->first();
    }

    public function getUserProgressByOrderInActivity($user_id, $order, $camp_id, $activity_id, $is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        $activityTask = $activityTaskMapper->query()->where('order', $order);
        if($is_default)
            $taskOrder = $activityTask->where('default_activity_id', $activity_id)->first();
        else
            $taskOrder = $activityTask->where('activity_id', $activity_id)->first();

        if($taskOrder == null)
            throw new BadRequestException(trans("locale.previous_task_doesn't_exists"));

        $task = $taskOrder->getTask();
        $campActivityProgressMapper = Analogue::mapper(CampActivityProgress::class);
        $progress = $campActivityProgressMapper->query()->where('task_id', $task->getId())->where('camp_id', $camp_id)->where('user_id', $user_id)->whereNotNull('first_success');
        if($is_default)
            $query = $progress->where('default_activity_id', $activity_id);
        else
            $query = $progress->where('activity_id', $activity_id);

        return $query->first();
    }

    public function getLastProgressInCamp($camp_id){
        $campActivityProgressMapper = Analogue::mapper(CampActivityProgress::class);
        return $campActivityProgressMapper->query()->where('camp_id', $camp_id)->orderBy('updated_at', 'DESC')->first();
    }



}