<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/24/2017
 * Time: 10:23 AM
 */

namespace App\Infrastructure\Repositories;


use App\DomainModelLayer\Classrooms\IClassroomUsersRepository;
use App\Infrastructure\Repositories\UserRolesRepository;
use Illuminate\Database\Eloquent\Model;

class ClassroomUsersRepository extends Model implements IClassroomUsersRepository
{
    protected $table = 'classroom_members';
    protected $fillable = ['classroom_id','member_id', 'member_role_id'];

    private $user_roles;
    function __construct(){
        parent::__construct();
        $this->user_roles = new UserRolesRepository();
    }

    public function getAssignedStudentsCount($id){
        $role_id = $this->user_roles->getRoleByName('student');
        return static::where('classroom_id',$id)->where('member_role_id',$role_id)->count();
    }

    public function getAssignedAdmins($id){
        $role_id = $this->user_roles->getRoleByName('teacher');
        return static::where('classroom_id',$id)->where('member_role_id',$role_id)->count();
    }
}