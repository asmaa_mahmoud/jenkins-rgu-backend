<?php

namespace App\Infrastructure\Schools;

use App\ApplicationLayer\Schools\Dtos\ChargeDto;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\InvitationSubscription;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\DistributorCharge;
use App\DomainModelLayer\Schools\DistributorChargeDetails;
use App\DomainModelLayer\Schools\DistributorChargeUpgrade;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Schools\Distributor;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\DomainModelLayer\Schools\DistributorSubscription;
use App\DomainModelLayer\Schools\Country;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Schools\SchoolCharge;
use App\DomainModelLayer\Schools\SchoolChargeDetails;
use Analogue;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Schools\SchoolBundle;
use App\DomainModelLayer\Schools\SchoolInvitation;
use App\DomainModelLayer\Schools\SchoolPrices;
use App\DomainModelLayer\Schools\SchoolChargesLog;
use App\DomainModelLayer\Schools\SchoolChargeUpgrade;
use App\Helpers\PaymentModule;
use Mail;
use Carbon\Carbon;
use DB;

class SchoolRepository
{

    public function addSchool(School $school){
        $schoolMapper = Analogue::mapper(School::class);
        $schoolMapper->store($school);
    }

    public function storeGrade(Grade $grade){
        $gradeMapper = Analogue::mapper(Grade::class);
        $gradeMapper->store($grade);
    }

    public function storeDistributorSubscription(DistributorSubscription $distributor_subscription){
        $disMapper = Analogue::mapper(DistributorSubscription::class);
        $disMapper->store($distributor_subscription);
    }

    public function getDefaultDistributors()
    {
    	$disMapper = Analogue::mapper(Distributor::class);
        return $disMapper->query()->where('is_default',1)->get();
    }

    public function getDistributors()
    {
    	$disMapper = Analogue::mapper(Distributor::class);
        return $disMapper->query()->get();
    }

    public function getDistributorById($id)
    {
    	$disMapper = Analogue::mapper(Distributor::class);
        return $disMapper->query()->whereId($id)->first();
    }

    public function getCountryByCode($country_code)
    {
    	$countryMapper = Analogue::mapper(Country::class);
        return $countryMapper->query()->where('country_code',$country_code)->first();
    }

    public function getCountries()
    {
    	$countryMapper = Analogue::mapper(Country::class);
        return $countryMapper->query()->get();
    }

    public function getDistributorSubscriptionByCode($user_id,$code)
    {
        $disMapper = Analogue::mapper(DistributorSubscription::class);
        return $disMapper->query()->where('current_code',$code)->whereIn('subscription_id',function($queryx) use($user_id){
           $queryx->select('id')->from('subscription')->whereIn('account_id',function($queryy) use($user_id){
              $queryy->select('account_id')->from('user')->where('id',$user_id);
           });
        })->first();
    }

    public function getLastDistributorSubscription($user_id)
    {
        $disMapper = Analogue::mapper(DistributorSubscription::class);
        return $disMapper->query()->whereIn('subscription_id', function($queryx) use($user_id){
           $queryx->select('id')->from('subscription')->whereIn('account_id', function($queryy) use($user_id){
              $queryy->select('account_id')->from('user')->where('id', $user_id);
           });
        })->orderBy('created_at', 'DESC')->first();
    }

    public function emailDistributor($distributor_name,$plan_name,$students,$classrooms,$distributor_email,$admin_name,$admin_email,$school_name,$code,$upcomingCode,$upgrade = false)
    {
        if($distributor_email != null || $distributor_email != '')
        {
            try {
                Mail::send('emails.distributor', [
                    'planName' => $plan_name,
                    'username' => $distributor_name,
                    'students' => $students,
                    'classrooms' => $classrooms,
                    'admin_name' => $admin_name,
                    'admin_email' => $admin_email,
                    'school_name' => $school_name,
                    'code' => $code,
                    'upcomingCode' => $upcomingCode,
                    'upgrade' => $upgrade
                    ], function ($m) use ($distributor_email) {
                    $m->from('info@robogarden.ca','The RoboGarden Team');
                    $m->to($distributor_email)->subject('RoboGarden Team: New School Account');
                });
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
    }

    public function sendCustomSubscriptionMail($school_name, $school_email, $plan_name = null, $students, $classrooms, $upgrade = false)
    {
        if($school_email != null || $school_email != '')
        {
            try {
                Mail::send('emails.subscribeschool', [
                    'planName' => $plan_name,
                    'username' => $school_name,
                    'students' => $students,
                    'classrooms' => $classrooms,
                    'upgrade' => $upgrade
                    ], function ($m) use ($school_email) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($school_email)->subject(trans('locale.new_subscription_subject'));
                });
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
    }

    public function emailAlreadyExist($email){
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('email',$email)->first();
        if($user == null)
            return false;
        return true;
    }

    public function usernameAlreadyExist($username){
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('username',$username)->first();
        if($user == null)
            return false;
        return true;
    }

    public function emailSubscription($account_name,$plan_name,$students,$classroom,$account_email,$plan = true, $upgrade = null)
    {
        if($account_email != null || $account_email != '')
        {
            try {
                Mail::send('emails.schoolsubscribe', [
                    'planName' => $plan_name,
                    'username' => $account_name,
                    'students' => $students,
                    'classrooms' => $classroom,
                    'plan' => $plan,
                    'upgrade' => $upgrade,
                ], function ($m) use ($account_email) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($account_email)->subject(trans('locale.new_subscription_subject'));
                });

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());

            }
        }
    }

    public function addDefaultGrades(School $school){
        for($i = 1;$i<13;$i++){
            $gradeDto = new GradeDto();
            if($i >= 0 && $i<=9)
                $gradeDto->name = "G0".$i;
            else
                $gradeDto->name = "G".$i;
            $grade = new Grade($school,$gradeDto);
            $this->storeGrade($grade);
        }
    }

    public function getCourseCreatedInRange(School $school,$from_time,$to_time){

        $courseMapper = Analogue::mapper(Course::class);
//        $courseMapper =  $courseMapper->query()->whereIn('id',DB::table('course_group')
//            ->whereIn('group_id',DB::table('group')->whereNull('deleted_at')
//                ->where('is_default',1)->whereIn('classroom_id',DB::table('classroom')
//                    ->whereIn('school_id',DB::table('school')->whereNull('deleted_at')
//                        ->where('id',$school->getId())->pluck('id')->toArray())
//                    ->whereNull('deleted_at')->pluck('id')->toArray())->pluck("id")->toArray())
//            ->pluck('course_id')->toArray());
        $courseMapper = $courseMapper->query()->whereIn('id',function($queryx) use($school){
            $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($school){
                $queryy->select('id')->from('group')->whereNull('deleted_at')->where('is_default',1)
                    ->whereIn('classroom_id',function($queryz) use($school){
                        $queryz->select('id')->from('classroom')->whereIn('school_id',function($queryr) use($school){
                            $queryr->select('id')->from('school')->whereNull('deleted_at')
                                ->whereIn('id',[$school->getId()]);
                        });
                    });
            });
        });

        if($from_time != null && $to_time != null)
            return $courseMapper->whereBetween('created_at',[$from_time,$to_time])->get();
        else
            return $courseMapper->get();

    }

    public function getCourseEndedInRange(School $school,$from_time,$to_time){
        $courseMapper = Analogue::mapper(Course::class);
//        $courseMapper =  $courseMapper->query()->whereIn('id',DB::table('course_group')
//            ->whereIn('group_id',DB::table('group')->whereNull('deleted_at')
//                ->where('is_default',1)->whereIn('classroom_id',DB::table('classroom')
//                    ->whereIn('school_id',DB::table('school')->whereNull('deleted_at')
//                        ->whereIn('id',[$school->getId()])->pluck('id')->toArray())
//                    ->whereNull('deleted_at')->pluck('id')->toArray())->pluck("id")->toArray())
//            ->pluck('course_id')->toArray());
        $courseMapper = $courseMapper->query()->whereIn('id',function($queryx) use($school){
            $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($school){
               $queryy->select('id')->from('group')->whereNull('deleted_at')->where('is_default',1)
                   ->whereIn('classroom_id',function($queryz) use($school){
                      $queryz->select('id')->from('classroom')->whereIn('school_id',function($queryr) use($school){
                          $queryr->select('id')->from('school')->whereNull('deleted_at')
                              ->whereIn('id',[$school->getId()]);
                      });
                   });
            });
        });
        if($from_time != null && $to_time != null)
            return $courseMapper->whereDate('ends_at', '<', Carbon::now())->whereBetween('ends_at',[$from_time,$to_time])->get();
        else
            return $courseMapper->whereDate('ends_at', '<', Carbon::now())->get();
    }

    public function getCoursesFinishedByStudents(School $school,$from_time,$to_time){
        $courseMapper = Analogue::mapper(Course::class);
//        return $courseMapper->query()
//            ->select(DB::raw('course.id,COALESCE(count(student_progress.id),0) as noofstudentsFinished'))
//            ->join('student_progress', function ($join) {
//                $join->on('course.id', '=', 'student_progress.course_id')
//                    ->where('task.id', '=',DB::table('task_order')->where('journey_id',DB::raw('course.journey_id'))->order_by('order', 'desc')->first()->task_id)
//                    ->whereNotNull('first_success');
//            })
//            ->whereIn('id',DB::table('course_group')
//                ->whereIn('group_id',DB::table('group')->whereNull('deleted_at')
//                    ->where('is_default',1)->whereIn('classroom_id',DB::table('classroom')
//                        ->whereIn('school_id',DB::table('school')->whereNull('deleted_at')
//                            ->whereIn('id',[$school->getId()])->pluck('id')->toArray())
//                    ->whereNull('deleted_at')->pluck('id')->toArray())->pluck("id")->toArray())
//                ->pluck('id')->toArray())
//            ->groupBy('course_id,')
//            ->whereBetween('ends_at',[$from_time,$to_time])->get();

        return $courseMapper->query()
            ->select(DB::raw('course.id,COALESCE(count(student_progress.id),0) as noofstudentsFinished'))
            ->join('student_progress', function ($join) {
                $join->on('course.id', '=', 'student_progress.course_id')
                    ->where('task.id', '=',DB::table('task_order')->where('journey_id',DB::raw('course.journey_id'))->order_by('order', 'desc')->first()->task_id)
                    ->whereNotNull('first_success');
            })
            ->whereIn('id',function($queryx) use($school){
                $queryx->select('course_id')->from('course_group')->whereIn('group_id',function($queryy) use($school){
                    $queryy->select('id')->from('group')->whereNull('deleted_at')->where('is_default',1)
                        ->whereIn('classroom_id',function($queryz) use($school){
                            $queryz->select('id')->from('classroom')->whereIn('school_id',function($queryr) use($school){
                                $queryr->select('id')->from('school')->whereNull('deleted_at')
                                    ->whereIn('id',[$school->getId()]);
                            });
                        });
                });
            })->groupBy('course_id')->whereBetween('ends_at',[$from_time,$to_time])->get();

    }

    public function getClassroomsForAdmin(School $school,Grade $grade = null){
        $classroomMapper = Analogue::mapper(Classroom::class);
        $classroomMapper = $classroomMapper->query()->where('school_id',$school->getId());
        if($grade != null){
            $classroomMapper = $classroomMapper->where('grade_id',$grade->getId());
        }
        return $classroomMapper->get();
    }

    public function getSchoolPlanComponentCost($type, $quantity){
        $pricesMapper = Analogue::mapper(SchoolPrices::class);
        return $pricesMapper->query()->where('type', $type)->where('quantity', $quantity)->first();
    }

    public function handleStripeCharge(ChargeDto $chargeDto){
        $paymentModule = new PaymentModule();
        return $paymentModule->addCharge($chargeDto->amount,$chargeDto->customer_id,$chargeDto->description);
    }

    public function getBundleById($bundle_id){
        $bundleMapper = Analogue::mapper(SchoolBundle::class);
        return $bundleMapper->query()->where('id',$bundle_id)->first();
    }

    public function storeSchoolCharge(SchoolCharge $schoolCharge){
        $schoolChargeMapper = Analogue::mapper(SchoolCharge::class);
        $schoolChargeMapper->store($schoolCharge);
    }

    public function storeSchoolChargeDetails(SchoolChargeDetails $schoolChargeDetails){
        $schoolChargeDetailsMapper = Analogue::mapper(SchoolChargeDetails::class);
        $schoolChargeDetailsMapper->store($schoolChargeDetails);
    }

    public function getNearestDiscount($type, $quantity){
        $pricesMapper = Analogue::mapper(SchoolPrices::class);
        return $pricesMapper->query()->where('type', $type)->where('quantity','<=', $quantity)->orderBy('quantity','desc')->first()->getDiscount();
    }

    public function storeSchoolChargesLog(SchoolChargesLog $schoolChargesLog){
        $schoolChargesLogMapper = Analogue::mapper(SchoolChargesLog::class);
        return $schoolChargesLogMapper->store($schoolChargesLog);
    }

    public function storeSchoolChargeUpgrade(SchoolChargeUpgrade $schoolChargeUpgrade){
        $schoolChargeUpgradeMapper = Analogue::mapper(SchoolChargeUpgrade::class);
        return $schoolChargeUpgradeMapper->store($schoolChargeUpgrade);
    }

    public function getLatestBundleUpgrade(SchoolCharge $schoolCharge){
        $schoolChargeUpgradeMapper = Analogue::mapper(SchoolChargeUpgrade::class);
        return $schoolChargeUpgradeMapper->query()->whereNotNull('bundle_id')->where('school_charge_id', $schoolCharge->getId())->orderBy('created_at', "DESC")->first();
    }

    public function getAllUpgradesAfter($id, SchoolCharge $schoolCharge){
        $schoolChargeUpgradeMapper = Analogue::mapper(SchoolChargeUpgrade::class);
        return $schoolChargeUpgradeMapper->query()->where('id', '>=' ,$id)->where('school_charge_id', $schoolCharge->getId())->get();
    }

    public function storeDistributorCharge(DistributorCharge $distributorCharge){
        $distributorChargeMapper = Analogue::mapper(DistributorCharge::class);
        $distributorChargeMapper->store($distributorCharge);
    }

    public function storeDistributorChargeDetails(DistributorChargeDetails $distributorChargeDetails){
        $distributorChargeDetailsMapper = Analogue::mapper(DistributorChargeDetails::class);
        $distributorChargeDetailsMapper->store($distributorChargeDetails);
    }

    public function getSchoolInvoicesLog($school_id,$limit)
    {
        $schoolChargeMapper = Analogue::mapper(SchoolChargesLog::class);
        return $schoolChargeMapper->query()->where('school_id', $school_id)->paginate($limit);
    }

    public function getSchoolPlanBundles(){
        $schoolBundleMapper = Analogue::mapper(SchoolBundle::class);
        return $schoolBundleMapper->query()->whereIn('name',['Bronze',"Silver","Golden"])->get();
    }

    public function getSchoolPrices($type = null){
        $schoolPricesMapper = Analogue::mapper(SchoolPrices::class);
        if ($type == null)
            return $schoolPricesMapper->query()->get()->toArray();
        else
            return $schoolPricesMapper->query()->where('type',$type)->get()->toArray();
    }

    public function getMaxNoOfStudentsAndClassrooms($account_id){
        $maxStudents = 0;
        $maxClassrooms = 0;
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $activeSubscription = $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->whereNull('ends_at')->orWhere('ends_at', '>', Carbon::now());
        })->orderBy('ends_at', 'DESC')->first();

        if ($activeSubscription != null){
            $distributorMapper = Analogue::mapper(DistributorSubscription::class);
            $distributor = $distributorMapper->query()->where('subscription_id', $activeSubscription->id)->first();

            $invitation = false;
            $invitationSubscription = $activeSubscription->getInvitationSubscription()->first();
            if($invitationSubscription){
                $invitation = $invitationSubscription->getInvitation();
            }

            if($distributor){
                $distributorChargeMapper = Analogue::mapper(DistributorCharge::class);
                $distributorCharge = $distributorChargeMapper->query()->where('distributor_subscription_id', $distributor->id)->first();
                if($distributorCharge){
                    $distributorChargeDetailsMapper = Analogue::mapper(DistributorChargeDetails::class);
                    $distributorChargeDetails = $distributorChargeDetailsMapper->query()->where('distributor_charge_id', $distributorCharge->id)->first();
                    $maxStudents = $distributorChargeDetails->getNoStudents();
                    $maxClassrooms = $distributorChargeDetails->getNoClassroom();
                }
            }
            else if ($invitation){
                $schoolInvitationMapper = Analogue::mapper(SchoolInvitation::class);
                $schoolInvitation = $schoolInvitationMapper->query()->where('invitation_id', $invitation->getId())->first();
                $maxStudents = $schoolInvitation->getMaxNumberOfStudents();
                $maxClassrooms = $schoolInvitation->getMaxNumberOfCourses();
            }
            else {
                $schoolChargeMapper = Analogue::mapper(SchoolCharge::class);
                $schoolCharge = $schoolChargeMapper->query()->where('subscription_id', $activeSubscription->id)->first();
                if($schoolCharge){
                    $schoolChargeDetailsMapper = Analogue::mapper(SchoolChargeDetails::class);
                    $schoolChargeDetails = $schoolChargeDetailsMapper->query()->where('school_charge_id', $schoolCharge->id)->first();
                    $maxStudents = $schoolChargeDetails->getNoStudents();
                    $maxClassrooms = $schoolChargeDetails->getNoClassroom();
                }
            }
        }
        return ['maxStudents' => $maxStudents, "maxClassrooms" => $maxClassrooms];
    }

    public function storeDistributorChargeUpgrade(DistributorChargeUpgrade $distributorChargeUpgrade){
        $distributorChargeUpgradeMapper = Analogue::mapper(DistributorChargeUpgrade::class);
        $distributorChargeUpgradeMapper->store($distributorChargeUpgrade);
    }

    public function deleteUnusedDistributorUpgrades($distributor_charge_id){
        $distributorChargeUpgradeMapper = Analogue::mapper(DistributorChargeUpgrade::class);
        $distributorUpgrades = $distributorChargeUpgradeMapper->query()->where('distributor_charge_id', $distributor_charge_id)->where('used', 0)->get();
        return $distributorChargeUpgradeMapper->delete($distributorUpgrades);
    }

    public function getDistributorLastUpgrade($distributor_charge_id){
        $distributorChargeUpgradeMapper = Analogue::mapper(DistributorChargeUpgrade::class);
        return $distributorChargeUpgradeMapper->query()->where('distributor_charge_id', $distributor_charge_id)->where('used', 0)->orderBy('created_at', 'DESC')->first();
    }


}