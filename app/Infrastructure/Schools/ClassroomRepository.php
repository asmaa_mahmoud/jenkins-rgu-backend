<?php

namespace App\Infrastructure\Schools;

use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Schools\ClassroomJourney;
use App\DomainModelLayer\Schools\StudentProgress;
use Illuminate\Support\Facades\DB;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\School;
use Analogue;

class ClassroomRepository
{

    public function storeClassroom(Classroom $classroom)
    {
        $classroomMapper= Analogue::mapper(Classroom::class);
        $classroomMapper->store($classroom);
    }


    public function getClassroomById($id)
    {
        $classroomMapper= Analogue::mapper(Classroom::class);
        return $classroomMapper->query()->whereId($id)->first();
    }

    public function deleteClassroom(Classroom $classroom)
    {
        $classroomMapper= Analogue::mapper(Classroom::class);
        $classroomMapper->delete($classroom);
    }

    public function deleteClassroomMember(ClassroomMember $classroomMember){
        $classroomMemberMapper = Analogue::mapper(ClassroomMember::class);
        $classroomMemberMapper->delete($classroomMember);
    }

    public function storeClassroomMember(ClassroomMember $classroomMember){
        $classroomMemberMapper = Analogue::mapper(ClassroomMember::class);
        $classroomMemberMapper->store($classroomMember);
    }

    public function getClassRoomMember(Classroom $classroom,User $user,Role $role){
        $classroomMemberMapper= Analogue::mapper(ClassroomMember::class);
        return $classroomMemberMapper->query()->where('classroom_id',$classroom->getId())
            ->where('user_id',$user->getId())->where('role_id',$role->getId())->first();
    }
    public function getDefaultGroupMember(Classroom $classroom,User $user,Role $role){
        $groupMemberMapper = Analogue::mapper(GroupMember::class);
        $defaultGroup = $classroom->getDefaultGroup();
        return $groupMemberMapper->query()->where('group_id',$defaultGroup->getId())
            ->where('user_id',$user->getId())->where('role_id',$role->getId())->first();

    }
    public function getClassroomJourney($classroom_id,$journey_id)
    {
        $classroomJourneyMapper= Analogue::mapper(ClassroomJourney::class);
        return $classroomJourneyMapper->query()->where('classroom_id',$classroom_id)->where('journey_id',$journey_id)->first();
    }

    public function deleteClassroomJourney(ClassroomJourney $classroomJourney)
    {
        $classroomJourneyMapper = Analogue::mapper(ClassroomJourney::class);
        $classroomJourneyMapper->delete($classroomJourney);
    }

    public function getClassroomByName($name)
    {
        $classroomMapper= Analogue::mapper(Classroom::class);
        return $classroomMapper->query()->whereName($name)->first();
    }

    public function getAllClassrooms($school_id,$limit = null,$grade_id = null,$search = null,$count = false){
        $classroomMapper = Analogue::mapper(Classroom::class);
        $query = $classroomMapper->query()->where('school_id',$school_id);

        if($grade_id != null){
            $query = $query->where('grade_id',$grade_id);
        }
        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('name', 'like', '%'.$search.'%');
            });
        }
        if($limit !=null)
            $query =  $query->paginate($limit);
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }

        return $query;
    }

    public function getAllStudentsInClassroom($classroom_id,$limit = null,$search = null,$count = false){
        $userMapper= Analogue::mapper(User::class);
        $query = $userMapper->query()->whereIn('id',function($queryx) use($classroom_id){
            $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($classroom_id) {
                $queryy->select('id')->from('group')->whereNull("deleted_at")
                    ->where('is_default',1)->where("classroom_id",$classroom_id);
            });
        })->whereIn('id',function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id',function($queryy){
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                });
        });

        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name','like','%'.$search.'%')
                    ->orWhere('last_name','like','%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function getAllTeachersInClassroom($classroom_id,$limit = null,$search = null,$count = false){
        $userMapper= Analogue::mapper(User::class);
//        $query = $userMapper->query()
//            ->whereIn('id', DB::table('group_user')->whereIn('group_id',DB::table("group")->whereNull("deleted_at")->where('is_default',1)->where("classroom_id",$classroom_id)->pluck('id')->toArray())->pluck('user_id')->toArray())
//            ->whereIn('id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','teacher')->pluck('id')->toArray()))->pluck('user_id')->toArray()));

        $query = $userMapper->query()->whereIn('id',function($queryx) use($classroom_id){
            $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($classroom_id) {
                $queryy->select('id')->from('group')->whereNull("deleted_at")
                    ->where('is_default',1)->where("classroom_id",$classroom_id);
            });
        })->whereIn('id',function($queryx){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id',function($queryy){
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','teacher');
                });
        });

        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%')
                    ->orWhere('last_name','like','%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function getUnEnrolledStudents(User $user,Classroom $classroom,$limit,$search){
        $userMapper= Analogue::mapper(User::class);

//        $query = $userMapper->query()
//            ->whereNotIn('id', DB::table('group_user')->whereIn('group_id',DB::table("group")->whereNull("deleted_at")->where('is_default',1)->pluck('id')->toArray())->pluck('user_id')->toArray())
//            ->whereIn('id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','student')->pluck('id')->toArray()))->pluck('user_id')->toArray()))
//            ->whereIn('id',(DB::table('student_grade')->where('grade_id',$classroom->getGrade()->getId())->pluck('user_id')->toArray()))
//            ->where('account_id',$user->getAccount()->getId());
        $query = $userMapper->query()
            ->whereNotIn('id',function($queryx){
                $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy){
                    $queryy->select('id')->from('group')->whereNull("deleted_at")
                        ->where('is_default',1);
                });
            })->whereIn('id',function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','student');
                    });
            })->whereIn('id',function($queryx) use($classroom){
                $queryx->select('user_id')->from('student_grade')->where('grade_id',$classroom->getGrade()->getId());
            })->where('account_id',$user->getAccount()->getId());

        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name','like','%'.$search.'%')
                    ->orWhere('last_name','like','%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit);
        }
        else{
            $query = $query->get();
        }

        return $query;
    }
    public function getUnAssignedTeachers(User $user,$classroom,$limit,$search){
        $userMapper= Analogue::mapper(User::class);
//        $query = $userMapper->query()
//            ->whereNotIn('id', DB::table('group_user')->whereIn('group_id',DB::table("group")->whereNull("deleted_at")->where('is_default',1)->where("classroom_id",$classroom->getId())->pluck('id')->toArray())->pluck('user_id')->toArray())
//            ->whereIn('id',(DB::table('user_role')->whereNull('deleted_at')->whereIn('role_id',(DB::table('role')->whereNull('deleted_at')->where('name','teacher')->pluck('id')->toArray()))->pluck('user_id')->toArray()))
//            ->where('account_id',$user->getAccount()->getId());

        $query = $userMapper->query()
            ->whereNotIn('id',function($queryx) use($classroom){
                $queryx->select('user_id')->from('group_user')->whereIn('group_id',function($queryy) use($classroom){
                    $queryy->select('id')->from('group')->whereNull("deleted_at")
                        ->where('is_default',1)->where("classroom_id",$classroom->getId());
                });
            })->whereIn('id',function($queryx){
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id',function($queryy){
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name','teacher');
                    });
            })->where('account_id',$user->getAccount()->getId());

        if($search != null){
            $query = $query->where(function ($innerQuery) use($search) {
                $innerQuery->where('username', 'like', '%'.$search.'%')
                    ->orWhere('first_name','like','%'.$search.'%')
                    ->orWhere('last_name','like','%'.$search.'%');
            });
        }
        if($limit != null){
            $query = $query->paginate($limit);
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function deleteClassroomMembers(User $user){
        $classRoomMembers  = $user->getClassRoomMembers();
        foreach ($classRoomMembers as $classRoomMember){
            $this->deleteClassroomMember($classRoomMember);
        }
    }

    public function checkClassroomNameUniqueInSchool(School $school,$classroomName){
        $classroomMapper= Analogue::mapper(Classroom::class);
        $queryOutput = $classroomMapper->query()->whereNull('deleted_at')->where('school_id',$school->getId())->where('name',$classroomName)->first();
        if($queryOutput == null)
            return true;
        else
            return false;
    }

    public function getStudentProgressInClassroom($student_id, $classroom_id){
        $studentProgressMapper= Analogue::mapper(StudentProgress::class);
        $queryOutput = $studentProgressMapper->query()->whereIn('course_id', function($queryx) use($classroom_id){
            $queryx->select('course_id')->from('course_group')->whereIn('group_id', function($queryy) use($classroom_id){
                $queryy->select('id')->from('group')->whereNull("deleted_at")->where("classroom_id", $classroom_id);
            });
        })->where('student_id', $student_id)->sum('score');
        return $queryOutput;
    }

    public function getEnrolledStudent($account_id, $count = null){
        $userMapper= Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', $account_id)->whereIn('id', function($queryx) {
            $queryx->select('user_id')->from('classroom_user')->whereIn('role_id', function($queryy){
                $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
            });
        });
        if($count == true)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

}