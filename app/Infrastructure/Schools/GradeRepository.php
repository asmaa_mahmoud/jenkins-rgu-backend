<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/24/2017
 * Time: 3:56 PM
 */

namespace App\Infrastructure\Schools;

use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use Analogue;

class GradeRepository
{
    protected $table = 'grade';

    public function getGradeByName(School $school,$name){
        $gradeMapper = Analogue::mapper(Grade::class);
        $grade = $gradeMapper->query()->whereName($name)->where('school_id',$school->getId())->first();
        return $grade;
    }

    public function getAllGrades()
    {
    	$gradeMapper = Analogue::mapper(Grade::class);
        $grades = $gradeMapper->query()->get();
        return $grades;
    }

    public function getGradeById($grade_id){
        $gradeMapper = Analogue::mapper(Grade::class);
        $grade = $gradeMapper->query()->where('id',$grade_id)->first();
        return $grade;
    }
}