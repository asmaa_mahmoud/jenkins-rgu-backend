<?php

namespace App\Infrastructure\Repositories;

use App\DomainModelLayer\Classrooms\IClassroomCurriculumRepository;
use Illuminate\Database\Eloquent\Model;

class ClassroomCurriculumRepository extends Model implements IClassroomCurriculumRepository
{
    protected $table = 'classroom_curriculum';
    protected $fillable = ['classroom_id','curriculum_id'];

    public function getAssignedCurriculumCount($id){
        return static::where('classroom_id',$id)->count();
    }
}