<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:22 PM
 */

namespace App\Infrastructure\Journeys;


use Analogue;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\Question;

class QuizRepository 
{
    public function getquiz($id)
   {
       //need to be changes to be dto mapping
        $quizMapper = Analogue::mapper(Quiz::class);
        $quiz = $quizMapper->query()->whereId($id)->first();
        return $quiz;
    }

    public function submitquiz($id)
    {
        dd($id);
    }

    public function getprogressforquiz($quiz_id, $user_id,$journey_id,$adventure_id)
    {
        $quiz = $this->getquiz($quiz_id);
        $task = $quiz->getTask();
        $quizProgressMapper = Analogue::mapper(TaskProgress::class);
        $quiz_progress = $quizProgressMapper->query()->where('task_id', $task->getId())->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->where('user_id', $user_id)->first();
        return $quiz_progress;
    }

    public function storequizprogress($quiz_progress)
    {
        $quizMapper = Analogue::mapper(QuizProgress::class);
        $quizMapper->store($quiz_progress);
    }
}