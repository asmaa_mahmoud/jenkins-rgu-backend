<?php

namespace App\Infrastructure\Journeys;

use App\ApplicationLayer\Schools\Dtos\ChargeDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\ActivityPrice;
use App\DomainModelLayer\Journeys\ActivityPeriod;
use App\DomainModelLayer\Journeys\ActivityTask;
use App\DomainModelLayer\Journeys\ActivityUnlockable;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\MissionSavingActivity;
use App\DomainModelLayer\Journeys\TaskProgressActivity;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Subscription;
use Analogue;
use App\Helpers\PaymentModule;
use Mail;
use Carbon\Carbon;
use LaravelLocalization;
use DB;

class ActivityRepository
{
    public function getDefaultActivityById($id){
        $defaultActivityMapper = Analogue::mapper(DefaultActivity::class);
        return $defaultActivityMapper->query()->whereId($id)->first();
    }

    public function getActivityById($id){
        $activityMapper = Analogue::mapper(Activity::class);
        return $activityMapper->query()->whereId($id)->first();
    }

    public function getAllActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null){
        $activityMapper = Analogue::mapper(DefaultActivity::class);
        $query = $activityMapper->query()->where('type', 1);

        if($is_b2b != null)
            $query = $query->where('is_b2b', true);

        if($is_b2c != null)
            $query = $query->where('is_b2c', true);

        if($search != null){
            $query = $query->whereIn('id', function ($innerQuery) use($search) {
                $innerQuery->select('default_activity_id')->from('activity_translation')
                    ->where('name', 'like', '%'.$search.'%')->whereNotNull('default_activity_id')
                    ->where('language_code',LaravelLocalization::getCurrentLocale());
            })->orWhereIn('id',function ($innerQuery) use($search) {
                $innerQuery->select('default_activity_id')->from('activity_tag')
                    ->whereIn('tag_id',function ($innerQuery2) use($search) {
                        $innerQuery2->select('id')->from('tags')
                            ->whereIn('id',function ($innerQuery3) use($search) {
                                $innerQuery3->select('tag_id')->from('tag_translation')
                                    ->where('name', 'like', '%'.$search.'%')
                                    ->where('language_code',LaravelLocalization::getCurrentLocale());
                            });
                    });
            });
        }

        $query = $query->orderBy('difficulty_id')->orderBy('age_from');

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();
        return $query;
    }

    public function getAllMineCraftActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null){
        $activityMapper = Analogue::mapper(DefaultActivity::class);
        $query = $activityMapper->query()->where('type', 2);

        if($is_b2b != null)
            $query = $query->where('is_b2b', true);

        if($is_b2c != null)
            $query = $query->where('is_b2c', true);

        if($search != null){
            $query = $query->whereIn('id', function ($innerQuery) use($search) {
                $innerQuery->select('default_activity_id')->from('activity_translation')
                    ->where('name', 'like', '%'.$search.'%')->whereNotNull('default_activity_id')
                    ->where('language_code', LaravelLocalization::getCurrentLocale());
            })->orWhereIn('id', function ($innerQuery) use($search) {
                $innerQuery->select('default_activity_id')->from('activity_tag')
                    ->whereIn('tag_id', function ($innerQuery2) use($search) {
                        $innerQuery2->select('id')->from('tags')
                            ->whereIn('id', function ($innerQuery3) use($search) {
                                $innerQuery3->select('tag_id')->from('tag_translation')
                                    ->where('name', 'like', '%'.$search.'%')
                                    ->where('language_code', LaravelLocalization::getCurrentLocale());
                            });
                    });
            });
        }

        if($limit != null)
            $query = $query->paginate($limit);

        else if($count == true)
            $query = $query->count();

        else
            $query = $query->get();
        return $query;
    }

    public function getUserTaskProgressActivity(Task $task,$user_id,$activity_id,$is_default = true){
        $taskProgressMapper= Analogue::mapper(TaskProgressActivity::class);
        $taskProgress = $taskProgressMapper->query()->where('task_id',$task->getId())->where('user_id',$user_id);
        if($is_default)
            $taskProgress = $taskProgress->where('default_activity_id',$activity_id)->first();
        else
            $taskProgress = $taskProgress->where('activity_id',$activity_id)->first();
        return $taskProgress;

    }

    public function storeTaskProgressActivity(TaskProgressActivity $taskProgressActivity){
        $taskProgressMapper = Analogue::mapper(TaskProgressActivity::class);
        $taskProgressMapper->store($taskProgressActivity);
    }

    public function deleteTaskProgressActivity(TaskProgressActivity $taskProgressActivity){
        $taskProgressMapper = Analogue::mapper(TaskProgressActivity::class);
        $taskProgressMapper->delete($taskProgressActivity);
    }

    public function getFirstTaskInActivity($activity_id,$is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);

        $activityTask = $activityTask->orderBy('order','asc')->first();

        return $activityTask->getTask();

    }

    public function getActivityTaskOrder(Task $task,$activity_id,$is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);

        $activityTask = $activityTask->where('task_id',$task->getId())->first();
        return $activityTask;
    }

    public function getLastSolvedTaskinActivity($user_id,$activity_id,$is_default= true){
        $taskProgressActivityMapper = Analogue::mapper(TaskProgressActivity::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id',$activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id',$activity_id);

        $taskProgressActivities = $taskProgressActivity->where('user_id',$user_id)->whereNotNull('first_success')->get();
        $task_ids = [];
        foreach ($taskProgressActivities as $taskProgressActivityOne){
            array_push($task_ids,$taskProgressActivityOne->getTask()->getId());
        }

        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);

        $activityTask = $activityTask->whereIn('task_id',$task_ids)->orderBy('order','desc')->first();
        return $activityTask;
    }

    public function getLastSolvedProgress($user_id,$activity_id,$is_default= true){

    }

    public function getStuckProgressActivity($user_id,$activity_id,$is_default = true){
        $taskProgressActivityMapper = Analogue::mapper(TaskProgressActivity::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id', $activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id', $activity_id);

        $taskProgressActivity = $taskProgressActivity->where('user_id', $user_id)
            ->whereNull('first_success')->where('no_of_trials', '>', 3)->get();
        return $taskProgressActivity;
    }

    public function getLastPlayedProgressActivity($user_id,$activity_id,$is_default = true){
        $taskProgressActivityMapper = Analogue::mapper(TaskProgressActivity::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id',$activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id',$activity_id);

        $taskProgressActivity = $taskProgressActivity->where('user_id',$user_id)->orderBy('updated_at','desc')->first();
        return $taskProgressActivity;
    }

    public function getSolvedTasksInActivity($user_id,$activity_id,$is_default= true){
        $taskProgressActivityMapper = Analogue::mapper(TaskProgressActivity::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id',$activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id',$activity_id);

        $taskProgressActivities = $taskProgressActivity->where('user_id',$user_id)->whereNotNull('first_success')->get();
        $task_ids = [];
        foreach ($taskProgressActivities as $taskProgressActivityOne){
            array_push($task_ids,$taskProgressActivityOne->getTask()->getId());
        }

        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTasks = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTasks = $activityTaskMapper->where('activity_id',$activity_id);

        $activityTasks = $activityTasks->whereIn('task_id',$task_ids)->orderBy('order','asc')->get();
        return $activityTasks;
    }

    public function getSolvedActivityProgress($user_id,$activity_id,$is_default= true){
        $taskProgressActivityMapper = Analogue::mapper(TaskProgressActivity::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id',$activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id',$activity_id);

        $taskProgressActivities = $taskProgressActivity->where('user_id',$user_id)->whereNotNull('first_success')->get();
        return $taskProgressActivities;
    }

    public function getLastTaskInActivity($activity_id,$is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);
        $activityTask = $activityTask->orderBy('order','desc')->first();

        return $activityTask->getTask();

    }

    public function getLastMainTaskInActivity($activity_id,$is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);
        $activityTask = $activityTask->where('booster_type','!=','1')->orderBy('order','desc')->first();

        return $activityTask->getTask();

    }

    public function getTaskInActivityByOrder($order,$activity_id,$is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);
        $activityTask = $activityTask->whereNull('deleted_at')->where('order',$order)->first();
        if($activityTask){
            return $activityTask->getTask();
        }else{
            return null;
        }
    }

    public function getAllTasksInActivity($activity_id, $count = null, $is_default = true){
        $taskMapper = Analogue::mapper(Task::class);
        if($is_default)
            $activityTask = $taskMapper->query()->whereIn('id', function ($q) use ($activity_id){
                $q->select('task_id')->from('activity_task')->where('default_activity_id', $activity_id);
            });
        else
            $activityTask = $taskMapper->query()->whereIn('id', function ($q) use ($activity_id){
                $q->select('task_id')->from('activity_task')->where('activity_id', $activity_id);
            });

        if($count != null)
            $activityTask = $activityTask->count();

        else
            $activityTask = $activityTask->get();

        return $activityTask;
    }

    public function getActivityPeriodsByAccountTypeId($account_type_id,$period = 0){
        $activityPeriodsMapper = Analogue::mapper(ActivityPeriod::class);
        return $activityPeriodsMapper->where('period',$period)->where('account_type_id',$account_type_id)->get();
    }

    public function getActivityPrice($account_type_id,$activity_id,$is_default = true){
        $periods = $this->getActivityPeriodsByAccountTypeId($account_type_id,0);
        $ids = [];
        foreach ($periods as $period){
            array_push($ids,$period->getId());
        }
        $activityPriceMapper = Analogue::mapper(ActivityPrice::class);
        if($is_default)
            $activityPriceMapper = $activityPriceMapper->where('activity_id',$activity_id);
        else
            $activityPriceMapper = $activityPriceMapper->where('activity_id',$activity_id);

        return $activityPriceMapper->whereIn('period_id',$ids)->first();
    }

    public function getActivityProgressForTask($user_id,$task_id,$activity_id,$is_default= true){
        $taskProgressActivityMapper = Analogue::mapper(TaskProgressActivity::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id',$activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id',$activity_id);

        $taskProgressActivity = $taskProgressActivity->where('task_id',$task_id)->where('user_id',$user_id)->first();

        return $taskProgressActivity;
    }

    public function getActivityTasksInOrder($activity_id,$is_default = true){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTask = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTask = $activityTaskMapper->where('activity_id',$activity_id);
        $activityTask = $activityTask->orderBy('order','asc')->get();

        return $activityTask;
    }

    public function getUserSavedCodeActivity(Mission $mission,$user_id,$activity_id,$is_default=true){
        $missionSavingActivityMapper = Analogue::mapper(MissionSavingActivity::class);
        if($is_default)
            $missionSavingActivityMapper = $missionSavingActivityMapper->where('default_activity_id',$activity_id);
        else
            $missionSavingActivityMapper = $missionSavingActivityMapper->where('activity_id',$activity_id);
        $missionSavingsActivity =  $missionSavingActivityMapper->where('user_id',$user_id)->where('mission_id',$mission->getId())->get();
        return $missionSavingsActivity;
    }

    public function getUserSavedCodeByNameActivity(Mission $mission,$user_id,$name,$activity_id,$is_default = true){
        $missionSavingActivityMapper = Analogue::mapper(MissionSavingActivity::class);
        if($is_default)
            $missionSavingActivityMapper = $missionSavingActivityMapper->where('default_activity_id',$activity_id);
        else
            $missionSavingActivityMapper = $missionSavingActivityMapper->where('activity_id',$activity_id);

        $missionSavingActivity =  $missionSavingActivityMapper->where('user_id',$user_id)
            ->where('mission_id',$mission->getId())->where('name',$name)->first();
        return $missionSavingActivity;
    }

    public function removeMissionSaving(MissionSavingActivity $missionSavingActivity){
        $missionSavingActivityMapper = Analogue::mapper(MissionSavingActivity::class);
        $missionSavingActivityMapper->delete($missionSavingActivity);
    }

    public function getTasksInActivity($activity_id, $count = null, $is_default = true){
        $taskMapper = Analogue::mapper(Task::class);
        if($is_default){
            $query = $taskMapper->query()->whereIn('id', function ($q) use ($activity_id) {
                $q->select('task_id')->from('activity_task')->where('default_activity_id', $activity_id)->orderBy('order');
            });
        }
        else{
            $query = $taskMapper->query()->whereIn('id', function ($q) use ($activity_id) {
                $q->select('task_id')->from('activity_task')->where('activity_id', $activity_id)->orderBy('order');
            });
        }

        if($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getFirstTaskIdInActivity($activity_id){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        return $activityTaskMapper->query()->where('default_activity_id', $activity_id)->orderBy('order')->first();
    }

    public function getDefaultActivityUnlockable($activity_id, $is_mobile = true){
        $activityUnlockableMapper = Analogue::mapper(ActivityUnlockable::class);
        return $activityUnlockableMapper->query()->where('default_activity_id', $activity_id)->whereIn('unlockable_id', function($q) use ($is_mobile){
            $q->select('id')->from('unlockable')->where('is_mobile', $is_mobile);
        })->first();
    }
    public function getTaskById($task_id){
        $activityUnlockableMapper = Analogue::mapper(Task::class);
        return $activityUnlockableMapper->query()->whereId($task_id)->first();
    }


}