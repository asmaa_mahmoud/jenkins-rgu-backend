<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 12:40 PM
 */

namespace App\Infrastructure\Repositories;


use Analogue;
use App\DomainModelLayer\Blockly\Blockly;
use App\DomainModelLayer\Blockly\IBlocklyRepository;

class BlocklyRepository implements IBlocklyRepository
{
    function getMissionBlocklyToolbox($missionId){
        $missionToolboxMapper = Analogue::mapper(Blockly::class);
        $missionToolbox = $missionToolboxMapper->query()->where('missionId',$missionId)->first();
        return $missionToolbox;
    }
}