<?php


namespace App\Infrastructure\Journeys;


use Analogue;
use App\DomainModelLayer\Journeys\FreeJourney;
use App\DomainModelLayer\Journeys\FreeJourneyTask;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\MissionCoding;
use App\DomainModelLayer\Journeys\MissionEditor;
use App\DomainModelLayer\Journeys\MissionAngular;
use App\DomainModelLayer\Journeys\MissionAngularFile;
use App\DomainModelLayer\Journeys\MissionMcq;
use App\DomainModelLayer\Journeys\MissionMcqChoose;
use App\DomainModelLayer\Journeys\MissionMcqQuestion;
use App\DomainModelLayer\Journeys\MissionText;
use App\DomainModelLayer\Journeys\MissionTextQuestion;
use App\DomainModelLayer\Journeys\MissionHtmlSteps;
use App\DomainModelLayer\Journeys\MissionHtmlSections;
use DB;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\MissionSavings;
use App\DomainModelLayer\Journeys\BlocksDefinitions;
use App\DomainModelLayer\Journeys\Blockly;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;
use App\DomainModelLayer\Journeys\Task;
use Mail;
use App;

class MissionRepository
{
    public function storeMission(Mission $mission)
    {
      $missionMapper= Analogue::mapper(Mission::class);
      $missionMapper->store($mission);
    }

    public function getBlocklyCategory($mission_id)
    {
      $blocklyMapper = Analogue::mapper(Blockly::class);
      $blockly = $blocklyMapper->query()->whereIn('id',(DB::table('mission_category')->where('mission_id',$mission_id)->pluck('block_category_id')->toArray()))->orderBy('order','asc')->get();
      return $blockly;
    }

    // public function updateMissionProgress(Mission $mission)
    // {
    //   $missionMapper= Analogue::mapper(Mission::class);
    //   $missionMapper->store($mission);
    // }

    // // Not Implemented Yet
    // public function updateMissionSavings(Mission $mission)
    // {
    //   $missionMapper= Analogue::mapper(Mission::class);
    //   $missionMapper->store($mission);
    // }

    public function getMissionbyId($id)
    {
      $missionMapper= Analogue::mapper(Mission::class);
      $mission = $missionMapper->query()->whereId($id)->first();
      return $mission;
    }

    public function getCodingMissionById($id)
    {
        $missionMapper= Analogue::mapper(MissionCoding::class);
        $mission = $missionMapper->query()->whereId($id)->first();
        return $mission;
    }

    public function getMissionEditorById($id)
    {
        $missionMapper= Analogue::mapper(MissionEditor::class);
        $mission = $missionMapper->query()->whereId($id)->first();
        return $mission;
    }

    public function getMissionAngularById($id)
    {
        
        $missionMapper= Analogue::mapper(MissionAngular::class);
        $mission = $missionMapper->query()->whereId($id)->first();
        return $mission;
    }

    public function getAngulaFileByIdAndName($id,$name)
    {
        
        $missionMapper= Analogue::mapper(MissionAngularFile::class);
        $mission = $missionMapper->query()->where('mission_angular_id',$id)->whereName($name)->first();
        return $mission;
    }

    public function getUserSavedCode(Mission $mission,$user_id,$journey_id,$adventure_id)
    {
      $missionSavingsMapper= Analogue::mapper(MissionSavings::class);
      $missionsavings = $missionSavingsMapper->query()->where('mission_id',$mission->getId())->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->where('user_id',$user_id)->get();
      return $missionsavings;
    }

    public function getUserMissionProgrss(Mission $mission,$user_id,$journey_id,$adventure_id)
    {
      $taskProgressMapper= Analogue::mapper(TaskProgress::class);
      $taskProgress = $taskProgressMapper->query()->where('task_id',$mission->getTask()->getId())->where('user_id',$user_id)->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->first();
      return $taskProgress;
    }
    public function getUserTaskProgress(Task $task,$user_id,$journey_id,$adventure_id){
        $taskProgressMapper= Analogue::mapper(TaskProgress::class);
        $taskProgress = $taskProgressMapper->query()->where('task_id',$task->getId())->where('user_id',$user_id)->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->first();
        return $taskProgress;
    }
    public function getUserSavedCodeByName(Mission $mission,$user_id,$name,$journey_id,$adventure_id)
    {
      $missionSavingsMapper= Analogue::mapper(MissionSavings::class);
      $missionsaving = $missionSavingsMapper->query()->where('mission_id',$mission->getId())->where('user_id',$user_id)->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->where('name',$name)->first();
      return $missionsaving;
    }

    public function deleteMissionSavings(MissionSavings $missionsaving)
    {
      $missionSavingsMapper= Analogue::mapper(MissionSavings::class);
      $missionSavingsMapper->delete($missionsaving);
    }

    public function getBlocksDefinitions()
    {
      $blocksDefinitionsMapper = Analogue::mapper(BlocksDefinitions::class);
      $blocksDefinitions = $blocksDefinitionsMapper->query()->get();
      return $blocksDefinitions;
    }

    public function getAllCategories()
    {
      $blocklyMapper = Analogue::mapper(Blockly::class);
      $blockly = $blocklyMapper->query()->get();
      return $blockly;
    }

    public function getMissionMcqQuestions(MissionMcq $missionMcq){
        $McqMapper = Analogue::mapper(MissionMcqQuestion::class);
        $mcq = $McqMapper->query()->where('mission_mcq_id',$missionMcq->getId())->get();
        return $mcq;
    }

    public function getMissionTextQuestions(MissionText $missionMcq){
        $TextMapper = Analogue::mapper(MissionTextQuestion::class);
        $text = $TextMapper->query()->where('mission_text_id',$missionMcq->getId())->get();
        return $text;
    }

    public function getMcqQuestions($id){
        $McqMapper = Analogue::mapper(MissionMcqQuestion::class);
        $question = $McqMapper->query()->whereId($id)->first();
        return $question;
    }

    public function getMcqCorrectChooseChoice($question_id){
        $McqMapper = Analogue::mapper(MissionMcqChoose::class);
        $choice = $McqMapper->query()->where('question_id',$question_id)->where('is_correct',1)->first();
        return $choice;
    }

    public function getMcqChoice($id){
        $McqMapper = Analogue::mapper(MissionMcqChoose::class);
        $choice = $McqMapper->query()->whereId($id)->first();
        return $choice;
    }

    public function getHtmlMissionbyId($html_mission_id){
        $missionHtmlMapper = Analogue::mapper(MissionHtml::class);
        return $missionHtmlMapper->query()->whereId($html_mission_id)->first();
    }

    public function getAllFreeJourneys()
    {
        $FreeJourneyMapper = Analogue::mapper(FreeJourney::class);
        return $FreeJourneyMapper->query()->where('type', 1)->get();
    }

    public function getFreeJourneyTasks($free_journey_id)
    {
      $FreeTaskMapper = Analogue::mapper(FreeJourneyTask::class);
      return $FreeTaskMapper->query()->where('free_journey_id', $free_journey_id)->orderBy('order','asc')->get();
    }

    public function getFreeJourneysTasksById($free_journey_id){
        $FreeTaskMapper = Analogue::mapper(FreeJourneyTask::class);
        return $FreeTaskMapper->query()->where('free_journey_id', $free_journey_id)->orderBy('order')->get();
    }

    public function isFreeTask($task_id)
    {
        $FreeTaskMapper = Analogue::mapper(FreeJourneyTask::class);
        $tasks = $FreeTaskMapper->query()->where('task_id',$task_id)->get();
        return (count($tasks) > 0? true:false);
    }

    public function sendCertificateEmail($email,$pdf_file,$level)
    {
        Mail::send('emails.certificate', ['level' => $level], function($message) use($email,$pdf_file,$level)
        {
            $message->from('info@robogarden.ca','The RoboGarden Team');
            $message->to($email, 'Robogarden Visitor')->subject(trans('locale.certificate_email_subject'));
            $message->attachData(base64_decode($pdf_file), 'Certificate.pdf');

        });
    }

    public function uploadCertificate($image_file)
    {
        $new_name = \Carbon\Carbon::now()->timestamp . "_child_certificate";

        $s3 = APP::make('aws')->createClient('s3');
        $s3->putObject(array(
            'Bucket' => 'robogarden-certificates',
            'Key' => $new_name,
            'Body' => base64_decode($image_file),
            'ACL' => 'public-read',
        ));

        return "https://robogarden-certificates.s3.amazonaws.com/" . $new_name;
    }

    public function getModelAnswerUnlocked($mission_id,$user_id){
        $modelAnswerUnlockedMapper= Analogue::mapper(ModelAnswerUnlocked::class);
        $modelAnswerUnlocked = $modelAnswerUnlockedMapper->query()
            ->where('mission_id',$mission_id)->where('user_id',$user_id)->first();
        return $modelAnswerUnlocked;
    }


    public function getMissionStepsCount($mission_id){

        $missionStepsCount = Analogue::mapper(MissionHtmlSteps::class);

        $missionStepsCount = $missionStepsCount->query()->where('mission_html_id', $mission_id)->count();

        return $missionStepsCount;
    }


    public function getMissionSectionsCount($mission_id,$count=true){
        $missionSectionsCount = Analogue::mapper(MissionHtmlSections::class);
        $missionSectionsCount = $missionSectionsCount->query()->where('mission_html_id', $mission_id)->orderBy('order','ASC');
        if($count != true){
            $missionSectionsCount = $missionSectionsCount->get();
        }else{
            $missionSectionsCount = $missionSectionsCount->count();
        }
        return $missionSectionsCount;
    }


    public function getMissionSection($mission_id,$section_id){

        $missionSection = Analogue::mapper(MissionHtmlSections::class);

        $missionSection = $missionSection->query()->where('mission_html_id', $mission_id)->where('id', $section_id)->get();

        return $missionSection;
    }


    public function getSectionWithOrder($mission_id,$order){

        $missionSection = Analogue::mapper(MissionHtmlSections::class);

        $missionSection = $missionSection->query()->where('mission_html_id', $mission_id)->where('order', $order)->first();

        return $missionSection;
    }

    public function getSectionById($id){
        $missionSection = Analogue::mapper(MissionHtmlSections::class);
        $missionSection = $missionSection->query()->whereId($id)->first();
        return $missionSection;
    }

}