<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:22 PM
 */

namespace App\Infrastructure\Journeys;


use Analogue;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\McqQuestion;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\DropdownChoice;
use App\DomainModelLayer\Journeys\Confidence;
use App\DomainModelLayer\Journeys\ConfidenceTranslation;
use Analogue\ORM\Analogue as AnalogueAnalogue;

class QuestionRepository
{
    public function getQuestionsforQuiz($quiz_id){
        $questionMapper = Analogue::mapper(Question::class);
        $questions = $questionMapper->query()->where('quiz_id', $quiz_id)->get();
        return ($questions);
    }

    public function getQuestionById($id)
    {
    	$questionMapper = Analogue::mapper(Question::class);
        $question = $questionMapper->query()->whereId($id)->first();
        return $question;
    }
    public function getQuestionMcqById($id)
    {
        $questionMapper = Analogue::mapper(McqQuestion::class);
        $question = $questionMapper->query()->whereId($id)->first();
        return $question;
    }

    public function checkQuestionInLesson($mission_id,$question_id){
        $questionMapper = Analogue::mapper(Question::class);
        $question = $questionMapper->query()->whereId($question_id)->whereIn('id',function($q)use($mission_id){
            $q->select('question_id')->from('question_task')->whereIn('html_step_id',function($qq)use($mission_id){
                $qq->select('id')->from('mission_html_steps')->where('mission_html_id',$mission_id);
            });
        })->first();
        return $question;

    }

    public function getDragCellById($id)
    {
        $dragCellMapper = Analogue::mapper(DragCell::class);
        $dragCell = $dragCellMapper->query()->whereId($id)->first();
        return $dragCell;
    }

    public function getDragChoiceById($id)
    {
        $dragChoiceMapper = Analogue::mapper(DragChoice::class);
        $dragChoice = $dragChoiceMapper->query()->whereId($id)->first();
        return $dragChoice;
    }

    public function getDropdownById($id)
    {
        $dropdownMapper = Analogue::mapper(Dropdown::class);
        $dropdown = $dropdownMapper->query()->whereId($id)->first();
        return $dropdown;
    }

    public function getDropdownCorrectAnswer($id)
    {
        $dropdownChoiceMapper = Analogue::mapper(DropdownChoice::class);
        $dropdownChoice = $dropdownChoiceMapper->query()->where('dropdown_id',$id)->where('is_correct','1')->first();
        return $dropdownChoice;
    }

    public function getDropdownChoiceById($id)
    {
        $dropdownChoiceMapper = Analogue::mapper(DropdownChoice::class);
        $dropdownChoice = $dropdownChoiceMapper->query()->whereId($id)->first();
        return $dropdownChoice;
    }

    public function getQuestionsByQuiz($quiz){
        $questionsIds = [];
        $mcqQuestionMapper = Analogue::mapper(MCQQuestion::class);
        //Questions IDs
        foreach ($quiz->getQuestionTasks()->all() as $task) {
            array_push($questionsIds,$task->question_id);
        }
        $mcqQuestions = $mcqQuestionMapper->query()->whereIn('question_id',$questionsIds)->get();
        return ($mcqQuestions);

    }

    public function getConfidenceByValue($value)
    {
        $confidenceTranslationMapper = Analogue::mapper(ConfidenceTranslation::class);
        $query = $confidenceTranslationMapper->query()->whereIn('id',function($q) use($value){
            $q->select('id')->from('confidence')->where('value',$value);
        });
    
        return $query->first();
    }
}