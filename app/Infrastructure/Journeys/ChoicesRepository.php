<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:22 PM
 */

namespace App\Infrastructure\Journeys;


use Analogue;
use App\DomainModelLayer\Journeys\Choice;

class ChoicesRepository
{
    public function getChoicesforQuestion($question_id){
        $choiceMapper = Analogue::mapper(Choice::class);
        $choices = $choiceMapper->query()->where('question_id', $question_id)->get();
        return ($choices);
    }

    public function getChoiceModelforQuestion($question_id){
        $choiceMapper = Analogue::mapper(Choice::class);
        $choice = $choiceMapper->query()->where('question_id', $question_id)->where('is_model_answer', 1)->get();
        return ($choice);
    }
    public function getChoiceModelforMcqQuestion($question_id){
        $choiceMapper = Analogue::mapper(Choice::class);
        $choice = $choiceMapper->query()->where('mcq_question_id', $question_id)->where('is_model_answer', 1)->get();
        return ($choice);
    }

    public function getChoiceById($id)
    {
        $choiceMapper = Analogue::mapper(Choice::class);
        $choice = $choiceMapper->query()->whereId($id)->first();
        return $choice;
    }

   
    public function getChoicesforQuestionById($question_id)
    {
        $choiceMapper = Analogue::mapper(Choice::class);
        $query = $choiceMapper->query()->where('question_id',$question_id)->get();
        return $query;
    }

    public function getChoicesforMcqQuestionById($question_id)
    {
        $choiceMapper = Analogue::mapper(Choice::class);
        $query = $choiceMapper->query()->where('mcq_question_id',$question_id)->get();
        return $query;
    }
}