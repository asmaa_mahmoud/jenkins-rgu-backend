<?php

namespace App\Infrastructure\Journeys;


use Analogue;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\TaskOrder;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\Framework\Exceptions\BadRequestException;
use DB;

class TaskRepository
{
	public function getTaskProgressByOrder($user_id,$order,$journey_id,$adventure_id)
	{
    $taskOrderMapper = Analogue::mapper(TaskOrder::class);
    $taskOrder = $taskOrderMapper->query()->where('order',$order)->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->first();
    if($taskOrder == null)
        throw new BadRequestException("Previous Task Order doesn't Exist");
    $task = $taskOrder->getTask();
		$taskProgressMapper = Analogue::mapper(TaskProgress::class);
    $taskProgress = $taskProgressMapper->query()->where('task_id',$task->getId())->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->where('user_id',$user_id)->whereNotNull('first_success')->first();
        return $taskProgress;
	}

	public function storeTask(Task $task)
    {
      $taskMapper= Analogue::mapper(Task::class);
      $taskMapper->store($task);
    }

    public function deleteProgress(TaskProgress $task_progress)
    {
      $taskProgressMapper = Analogue::mapper(TaskProgress::class);
      $taskProgressMapper->delete($task_progress);
    }

    public function getTaskOrder(Task $task,$journey_id,$adventure_id)
    {
      $taskOrderMapper = Analogue::mapper(TaskOrder::class);
      $taskOrder = $taskOrderMapper->query()->where('task_id',$task->getId())->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->first();
      if($taskOrder == null)
          throw new BadRequestException("Previous Task Order doesn't Exist");
      return $taskOrder;
    }

    public function getTaskByOrder($order,$journey_id,$adventure_id)
    {
      $taskOrderMapper = Analogue::mapper(TaskOrder::class);
      $taskOrder = $taskOrderMapper->query()->where('order',$order)->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->first();
      if($taskOrder == null)
          throw new BadRequestException("Task Order doesn't Exist");
      $task = $taskOrder->getTask();
      return $task;
    }

    public function getLastTaskInAdventure($journey_id,$adventure_id)
    {
      $taskOrderMapper = Analogue::mapper(TaskOrder::class);
      $taskOrder = $taskOrderMapper->query()->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->orderBy('order','desc')->first();
      if($taskOrder == null)
          throw new BadRequestException("Task Order doesn't Exist");
      $task = $taskOrder->getTask();
      return $task;
    }

    public function getFirstTaskInAdventure($journey_id,$adventure_id)
    {
      $taskOrderMapper = Analogue::mapper(TaskOrder::class);
      $taskOrder = $taskOrderMapper->query()->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->orderBy('order','asc')->first();
      if($taskOrder == null)
          throw new BadRequestException("Task Order doesn't Exist");
      $task = $taskOrder->getTask();
      return $task;
    }

    public function getFirstTaskInJourney($journey_id)
    {
      $adMapper = Analogue::mapper(Adventure::class);
      $ad = $adMapper->query()->whereIn('id',(DB::table('journey_adventure')->where('journey_id',$journey_id)->pluck('adventure_id')->toArray()))->orderBy('order', 'asc')->first();
      if($ad == null)
          throw new BadRequestException("First adventure doesn't Exist");

      $taskOrderMapper = Analogue::mapper(TaskOrder::class);
      $taskOrder = $taskOrderMapper->query()->where('journey_id',$journey_id)->where('adventure_id',$ad->getId())->orderBy('order','asc')->first();
      if($taskOrder == null)
          throw new BadRequestException("Task Order doesn't Exist");
      $task = $taskOrder->getTask();
      return $task;
    }

    public function getTasksByOrder($journey_id,$adventure_id)
    {
      $tasks = [];

      $taskOrderMapper = Analogue::mapper(TaskOrder::class);
      $taskOrders = $taskOrderMapper->query()->where('journey_id',$journey_id)->where('adventure_id',$adventure_id)->orderBy('order','asc')->get();
      
      foreach ($taskOrders as $taskOrder) {
        $tasks[] = $taskOrder->getTask();
      }
      return $tasks;
    }

    public function getLastTaskInJourney(Journey $journey){
        $adMapper = Analogue::mapper(Adventure::class);
        $ad = $adMapper->query()->whereIn('id',(DB::table('journey_adventure')->where('journey_id',$journey->getId())->pluck('adventure_id')->toArray()))->orderBy('order', 'desc')->first();
        return $this->getLastTaskInAdventure($journey->getId(),$ad->getId());
    }

    public function removeTaskProgress(TaskProgress $taskProgress){
        $taskMapper= Analogue::mapper(TaskProgress::class);
        $taskMapper->delete($taskProgress);
    }
    public function storeTaskProgress(TaskProgress $taskProgress){
        $taskMapper= Analogue::mapper(TaskProgress::class);
        $taskMapper->store($taskProgress);
    }
}