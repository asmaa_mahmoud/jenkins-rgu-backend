<?php

namespace App\Infrastructure\Journeys;

use Analogue;
use App\DomainModelLayer\Accounts\UserPosition;
use App\DomainModelLayer\Journeys\FreeJourney;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\TaskProgress;
use DB;

class JourneyRepository
{
    public function getAllJourneys(){
        $journeyMapper = Analogue::mapper(Journey::class);
        return $journeyMapper->query()->where('journey_status_id', function ($q){
            $q->select('id')->from('journey_status')->where('name', 'published');
        })->get();
    }

    public function GetJourneysCatalogue(){
        $journeyMapper = Analogue::mapper(Journey::class);
        $journeys = $journeyMapper->query()->get();
        return $journeys;
    }

   	public function getJourneyById($id)
    {
        $journeyMapper = Analogue::mapper(Journey::class);
        $journey = $journeyMapper->query()->whereId($id)->first();
        return $journey;
    }

    public function getJourneyNotInIds($ids){
        $journeyMapper = Analogue::mapper(Journey::class);
        $journeys = $journeyMapper->query()->whereNotIn('id',$ids)->get();
        return $journeys;
    }

    public function getAdventureById($id)
    {
        $adMapper = Analogue::mapper(Adventure::class);
        $ad = $adMapper->query()->whereId($id)->first();
        return $ad;
    }

    public function getJourneyData(Journey $journey,$user_id)
    {
        $journey_data = [];
        $counter = 0;
        $adventures = DB::table('adventure')->where('Journey_Id',$journey->getId())->get();
        foreach ($adventures as $adventure) {
            $adMapper = Analogue::mapper(Adventure::class);
            $ad = $adMapper->query()->whereId($adventure->id)->first();
            $journey_data[$counter]['name'] = $ad->getTitle();
            $journey_data[$counter]['description'] = $ad->getDescription();
            $journey_data[$counter]['order'] = $adventure->order;
            $journey_data[$counter]['roadmapImageURL'] = $adventure->roadmapImageURL;
            $missions = DB::table('mission')->where('Adventure_id',$adventure->id)->get();
            $counter_mission = 0;
            foreach ($missions as $mission) {
                $misMapper = Analogue::mapper(Mission::class);
                $mis = $misMapper->query()->whereId($mission->id)->first();
                $journey_data[$counter]['missions'][$counter_mission]['id'] = $mission->id;
                $journey_data[$counter]['missions'][$counter_mission]['name'] = $mis->getName();
                $journey_data[$counter]['missions'][$counter_mission]['order'] = $mission->Order;
                $journey_data[$counter]['missions'][$counter_mission]['description'] = $mis->getDescription();
                $journey_data[$counter]['missions'][$counter_mission]['iconURL'] = $mission->iconURL;
                $journey_data[$counter]['missions'][$counter_mission]['position_x'] = $mission->position_x;
                $journey_data[$counter]['missions'][$counter_mission]['position_y'] = $mission->position_y;
                $journey_data[$counter]['missions'][$counter_mission]['vpl_id'] = $mission->vpl_id;
                $locked = true;
                if($mission->Order == 1)
                {
                    $quiz_progress = DB::table('quizprogress')->where('User_Id',$user_id)->whereIn('Quiz_Id',(DB::table('quiz')->where('Adventure_id',$adventure->id)->whereIn('QuizType_Id',(DB::table('quiztype')->where('Name','Tutorial')->pluck('id')->toArray()))->pluck('id')->toArray()))->first();
                    $locked = ($quiz_progress == null? true:false);
                    $journey_data[$counter]['missions'][$counter_mission]['locked'] = $locked;
                }
                else
                {
                    $mission_progress = DB::table('missionprogress')->where('User_Id',$user_id)->Where(function ($query) use($mission,$adventure){
                        $query->where('Mission_Id',$mission->id)->orWhereIn('Mission_Id',(DB::table('mission')->where('Adventure_id',$adventure->id)->where('Order',($mission->Order)-1)->pluck('id')->toArray()));
                    })->first();
                    $locked = ($mission_progress == null? true:false);
                    $journey_data[$counter]['missions'][$counter_mission]['locked'] = $locked;
                }
                $counter_mission++;
            }
            $quizzes = DB::table('quiz')->where('Adventure_id',$adventure->id)->get();
            $counter_quiz = 0;
            foreach ($quizzes as $quiz) {
                $qMapper = Analogue::mapper(Quiz::class);
                $q = $qMapper->query()->whereId($quiz->id)->first();
                $journey_data[$counter]['quizzes'][$counter_quiz]['id'] = $quiz->id;
                $journey_data[$counter]['quizzes'][$counter_quiz]['Title'] = $q->getTitle();
                $journey_data[$counter]['quizzes'][$counter_quiz]['iconURL'] = $quiz->iconURL;
                $journey_data[$counter]['quizzes'][$counter_quiz]['position_x'] = $quiz->position_x;
                $journey_data[$counter]['quizzes'][$counter_quiz]['position_y'] = $quiz->position_y;
                $quiz_progress = DB::table('quizprogress')->where('User_Id',$user_id)->where('Quiz_Id',$quiz->id)->first();
                if($quiz_progress != null)
                    $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = false;
                else
                {
                    $quiz_type = DB::table('quiztype')->where('id',$quiz->QuizType_Id)->first();
                    if($quiz_type->Name == 'Tutorial')
                    {
                        if($adventure->order == 1)
                        {
                           $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = false;
                        }
                        else
                        {                            
                            $previous_adventure = DB::table('adventure')->where('Journey_Id',$journey->getId())->where('order', ($adventure->order)-1)->first();
                            $previous_quiz = DB::table('quiz')->where('Adventure_id',$previous_adventure->id)->whereIn('QuizType_Id',(DB::table('quiztype')->where('Name','Quiz')->pluck('id')->toArray()))->first();
                            $previous_quiz_progress = DB::table('quizprogress')->where('User_Id',$user_id)->where('Quiz_Id',$previous_quiz->id)->first();
                            $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = ($previous_quiz_progress == null? true:false);
                        }
                    }
                    else
                    {
                        $mission_count = DB::table('mission')->where('Adventure_id',$adventure->id)->count();
                        $mission_progress = DB::table('missionprogress')->where('User_Id',$user_id)->WhereIn('Mission_Id',(DB::table('mission')->where('Adventure_id',$adventure->id)->where('Order',$mission_count)->pluck('id')->toArray()))->first();
                        $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = ($mission_progress == null? true:false);

                    }
                }
                $counter_quiz++;
            }
            $counter++;

        }

        return ["scene_name"=>$journey->getSceneName(),"journey_data"=>$journey_data];
    }

    public function getAdventuresByOrder($journey_id)
    {
        $adMapper = Analogue::mapper(Adventure::class);
        $ad = $adMapper->query()->whereIn('id',(DB::table('journey_adventure')->where('journey_id',$journey_id)->pluck('adventure_id')->toArray()))->orderBy('order', 'asc')->get();
        return $ad;
    }

    public function getAdventureByOrder($order,$journey_id)
    {
        $adMapper = Analogue::mapper(Adventure::class);
        $ad = $adMapper->query()->whereIn('id',(DB::table('journey_adventure')->where('journey_id',$journey_id)->pluck('adventure_id')->toArray()))->where('order', $order)->first();
        return $ad;
    }

    public function getLastSolvedTask($journey_id, $child_id){
        $adMapper = Analogue::mapper(TaskProgress::class);
        $ad = $adMapper->query()->where('journey_id', $journey_id)->where('user_id', $child_id)->whereNotNull('first_success')->orderBy('updated_at', 'DESC')->first();
        return $ad;
    }

    public function getLastPlayedTask($journey_id, $child_id){
        $adMapper = Analogue::mapper(UserPosition::class);
        $ad = $adMapper->query()->where('journey_id', $journey_id)->where('user_id', $child_id)->orderBy('updated_at', 'DESC')->first();
        return $ad;
    }

    public function getRoboClubFreeJourneys(){
        $FreeJourneyMapper = Analogue::mapper(FreeJourney::class);
        return $FreeJourneyMapper->query()->where('type', 2)->get();
    }


}