<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:22 PM
 */

namespace App\Infrastructure\Journeys;


use Analogue;
use App\DomainModelLayer\Journeys\MissionMcq;
use App\DomainModelLayer\Journeys\MissionSavingActivity;
use App\DomainModelLayer\Journeys\MissionText;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Confidence;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\TaskProgressActivity;
use App\DomainModelLayer\Journeys\MissionSavings;
use App\DomainModelLayer\Journeys\Quiz;
use DB;

class JourneyMainRepository implements IJourneyMainRepository
{
    

   	public function getJourneyById($id)
    {
        $journeyRepository = new JourneyRepository;
        return $journeyRepository->getJourneyById($id);
    }

    public function GetJourneysCatalogue()
    {
        $journeyRepository = new JourneyRepository;
        return $journeyRepository->GetJourneysCatalogue();
    }

    public function getTaskProgressByOrder($user_id,$order,$journey_id,$adventure_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getTaskProgressByOrder($user_id,$order,$journey_id,$adventure_id);
    }

    public function getJourneyData(Journey $journey,$user_id)
    {
        $journey_data = [];
        $counter = 0;
        //$adventures = DB::table('adventure')->where('Journey_Id',$journey->getId())->get();
        $adventures = DB::table('adventure')->where('Journey_Id',$journey->getId())->get();
        foreach ($adventures as $adventure) {
            $adMapper = Analogue::mapper(Adventure::class);
            $ad = $adMapper->query()->whereId($adventure->id)->first();
            $journey_data[$counter]['name'] = $ad->getTitle();
            $journey_data[$counter]['description'] = $ad->getDescription();
            $journey_data[$counter]['order'] = $adventure->order;
            $journey_data[$counter]['roadmapImageURL'] = $adventure->roadmapImageURL;
            $missions = DB::table('mission')->where('Adventure_id',$adventure->id)->get();
            $counter_mission = 0;
            foreach ($missions as $mission) {
                $misMapper = Analogue::mapper(Mission::class);
                $mis = $misMapper->query()->whereId($mission->id)->first();
                $journey_data[$counter]['missions'][$counter_mission]['id'] = $mission->id;
                $journey_data[$counter]['missions'][$counter_mission]['name'] = $mis->getName();
                $journey_data[$counter]['missions'][$counter_mission]['order'] = $mission->Order;
                $journey_data[$counter]['missions'][$counter_mission]['description'] = $mis->getDescription();
                $journey_data[$counter]['missions'][$counter_mission]['iconURL'] = $mission->iconURL;
                $journey_data[$counter]['missions'][$counter_mission]['position_x'] = $mission->position_x;
                $journey_data[$counter]['missions'][$counter_mission]['position_y'] = $mission->position_y;
                $journey_data[$counter]['missions'][$counter_mission]['vpl_id'] = $mission->vpl_id;
                $locked = true;
                if($mission->Order == 1)
                {
                    $quiz_progress = DB::table('quizprogress')->where('User_Id',$user_id)->whereIn('Quiz_Id',(DB::table('quiz')->where('Adventure_id',$adventure->id)->whereIn('QuizType_Id',(DB::table('quiztype')->where('Name','Tutorial')->pluck('id')->toArray()))->pluck('id')->toArray()))->first();
                    $locked = ($quiz_progress == null? true:false);
                    $journey_data[$counter]['missions'][$counter_mission]['locked'] = $locked;
                }
                else
                {
                    $mission_progress = DB::table('missionprogress')->where('User_Id',$user_id)->Where(function ($query) use($mission,$adventure){
                        $query->where('Mission_Id',$mission->id)->orWhereIn('Mission_Id',(DB::table('mission')->where('Adventure_id',$adventure->id)->where('Order',($mission->Order)-1)->pluck('id')->toArray()));
                    })->first();
                    $locked = ($mission_progress == null? true:false);
                    $journey_data[$counter]['missions'][$counter_mission]['locked'] = $locked;
                }
                $counter_mission++;
            }
            $quizzes = DB::table('quiz')->where('Adventure_id',$adventure->id)->get();
            $counter_quiz = 0;
            foreach ($quizzes as $quiz) {
                $qMapper = Analogue::mapper(Quiz::class);
                $q = $qMapper->query()->whereId($quiz->id)->first();
                $journey_data[$counter]['quizzes'][$counter_quiz]['id'] = $quiz->id;
                $journey_data[$counter]['quizzes'][$counter_quiz]['Title'] = $q->getTitle();
                $journey_data[$counter]['quizzes'][$counter_quiz]['iconURL'] = $quiz->iconURL;
                $journey_data[$counter]['quizzes'][$counter_quiz]['position_x'] = $quiz->position_x;
                $journey_data[$counter]['quizzes'][$counter_quiz]['position_y'] = $quiz->position_y;
                $quiz_progress = DB::table('quizprogress')->where('User_Id',$user_id)->where('Quiz_Id',$quiz->id)->first();
                if($quiz_progress != null)
                    $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = false;
                else
                {
                    $quiz_type = DB::table('quiztype')->where('id',$quiz->QuizType_Id)->first();
                    if($quiz_type->Name == 'Tutorial')
                    {
                        if($adventure->order == 1)
                        {
                           $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = false;
                        }
                        else
                        {                            
                            $previous_adventure = DB::table('adventure')->where('Journey_Id',$journey->getId())->where('order', ($adventure->order)-1)->first();
                            $previous_quiz = DB::table('quiz')->where('Adventure_id',$previous_adventure->id)->whereIn('QuizType_Id',(DB::table('quiztype')->where('Name','Quiz')->pluck('id')->toArray()))->first();
                            $previous_quiz_progress = DB::table('quizprogress')->where('User_Id',$user_id)->where('Quiz_Id',$previous_quiz->id)->first();
                            $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = ($previous_quiz_progress == null? true:false);
                        }
                    }
                    else
                    {
                        $mission_count = DB::table('mission')->where('Adventure_id',$adventure->id)->count();
                        $mission_progress = DB::table('missionprogress')->where('User_Id',$user_id)->WhereIn('Mission_Id',(DB::table('mission')->where('Adventure_id',$adventure->id)->where('Order',$mission_count)->pluck('id')->toArray()))->first();
                        $journey_data[$counter]['quizzes'][$counter_quiz]['locked'] = ($mission_progress == null? true:false);

                    }
                }
                $counter_quiz++;
            }
            $counter++;

        }

        return ["scene_name"=>$journey->getSceneName(),"journey_data"=>$journey_data];
    }

    public function getMissionbyId($id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getMissionbyId($id);
    }
    public function getCodingMissionById($id){
         $missionRepository = new MissionRepository;
        return $missionRepository->getCodingMissionById($id);

    }
    public function getMissionEditorById($id){
         $missionRepository = new MissionRepository;
        return $missionRepository->getMissionEditorById($id);

    }

    public function getMissionAngularById($id){
        $missionRepository = new MissionRepository;
       return $missionRepository->getMissionAngularById($id);
   }

   public function getAngulaFileByIdAndName($id,$name){
    $missionRepository = new MissionRepository;
   return $missionRepository->getAngulaFileByIdAndName($id,$name);
   }

    public function getFreeJourneysTasksById($free_journey_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getFreeJourneysTasksById($free_journey_id);
    }

    public function getUserSavedCode(Mission $mission,$user_id,$journey_id,$adventure_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getUserSavedCode($mission,$user_id,$journey_id,$adventure_id);
    }

    public function getUserSavedCodeByName(Mission $mission,$user_id,$name,$journey_id,$adventure_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getUserSavedCodeByName($mission,$user_id,$name,$journey_id,$adventure_id);
    }

    public function storeMission(Mission $mission)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->storeMission($mission);
    }

    public function deleteMissionSavings(MissionSavings $missionsaving)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->deleteMissionSavings($missionsaving);
    }

    public function getUserMissionProgrss(Mission $mission,$user_id,$journey_id,$adventure_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getUserMissionProgrss($mission,$user_id,$journey_id,$adventure_id);
    }

    public function storeTask(Task $task)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->storeTask($task);
    }

    public function deleteProgress(TaskProgress $task_progress)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->deleteProgress($task_progress);
    }

    public function getquiz($id)
    {
        $quizRepository = new QuizRepository;
        return $quizRepository->getquiz($id);
    }

    public function getQuestionsforQuiz($quiz_id)
    {
        $questionRepository = new QuestionRepository;
        return $questionRepository->getQuestionsforQuiz($quiz_id);
    }

    public function getQuestionById($id)
    {
        $questionRepository = new QuestionRepository;
        return $questionRepository->getQuestionById($id);
    }

   public function getQuestionMcqById($id)
    {
        $questionRepository = new QuestionRepository;
        return $questionRepository->getQuestionMcqById($id);
    }

    public function getChoiceById($id)
    {
        $choicRepository = new ChoicesRepository;
        return $choicRepository->getChoiceById($id);
    }

    public function getConfidenceByValue($value)
    {
        $questionRepository = new QuestionRepository;
        return $questionRepository->getConfidenceByValue($value);
    }

    public function getChoiceModelforQuestion($question_id)
    {
        $choicRepository = new ChoicesRepository;
        return $choicRepository->getChoiceModelforQuestion($question_id);
    }
    public function getChoiceModelforMcqQuestion($question_id)
    {
        $choicRepository = new ChoicesRepository;
        return $choicRepository->getChoiceModelforMcqQuestion($question_id);
    }

    public function getChoicesforQuestionById($question_id)
    {
        $choicRepository = new ChoicesRepository;
        return $choicRepository->getChoicesforQuestionById($question_id);
    }

    public function getChoicesforMcqQuestionById($question_id)
    {
        $choicRepository = new ChoicesRepository;
        return $choicRepository->getChoicesforMcqQuestionById($question_id);
    }

    public function getprogressforquiz($quiz_id, $user_id,$journey_id,$adventure_id)
    {
        $quizRepository = new QuizRepository;
        return $quizRepository->getprogressforquiz($quiz_id, $user_id,$journey_id,$adventure_id);
    }

    public function getAllCategories()
    {
        $JourneyCategoryRepository = new JourneyCategoryRepository;
        return $JourneyCategoryRepository->getAllCategories();
    }

    public function getTaskOrder(Task $task,$journey_id,$adventure_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getTaskOrder($task,$journey_id,$adventure_id);
    }

    public function getTaskByOrder($order,$journey_id,$adventure_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getTaskByOrder($order,$journey_id,$adventure_id);
    }

    public function getLastTaskInAdventure($journey_id,$adventure_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getLastTaskInAdventure($journey_id,$adventure_id);
    }

    public function getFirstTaskInAdventure($journey_id,$adventure_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getFirstTaskInAdventure($journey_id,$adventure_id);
    }

    public function getFirstTaskInJourney($journey_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getFirstTaskInJourney($journey_id);
    }

    public function getAdventuresByOrder($journey_id)
    {
        $journeyRepository = new JourneyRepository;
        return $journeyRepository->getAdventuresByOrder($journey_id);
    }

    public function getAdventureByOrder($order,$journey_id)
    {
        $journeyRepository = new JourneyRepository;
        return $journeyRepository->getAdventureByOrder($order,$journey_id);
    }

    public function getBlocklyCategory($mission_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getBlocklyCategory($mission_id);
    }

    public function getAdventureById($id)
    {
        $journeyRepository = new JourneyRepository;
        return $journeyRepository->getAdventureById($id);
    }

    public function getTasksByOrder($journey_id,$adventure_id)
    {
        $taskRepository = new TaskRepository;
        return $taskRepository->getTasksByOrder($journey_id,$adventure_id);
    }


    public function getMissionMcqQuestions(MissionMcq $missionMcq)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getMissionMcqQuestions($missionMcq);
    }

    public function getMissionTextQuestions(MissionText $missionText)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getMissionTextQuestions($missionText);
    }

    public function getMcqQuestionById($id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getMcqQuestions($id);
    }

    public function getMcqChoiceById($id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getMcqChoice($id);
    }

    public function getMcqCorrectChooseChoice($question_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getMcqCorrectChooseChoice($question_id);
    }

    public function getLastTaskInJourney(Journey $journey){
        $taskRepository = new TaskRepository;
        return $taskRepository->getLastTaskInJourney($journey);
    }

    public function getHtmlMissionbyId($html_mission_id){
        $missionRepository = new MissionRepository;
        return $missionRepository->getHtmlMissionbyId($html_mission_id);
    }
    public function getUserTaskProgress(Task $task,$user_id,$journey_id,$adventure_id){
        $missionRepository = new MissionRepository;
        return $missionRepository->getUserTaskProgress( $task,$user_id,$journey_id,$adventure_id);
    }

    public function getAllFreeJourneys()
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getAllFreeJourneys();
    }

    public function isFreeTask($task_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->isFreeTask($task_id);
    }

    public function getFreeJourneyTasks($free_journey_id)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->getFreeJourneyTasks($free_journey_id);
    }

    public function sendCertificateEmail($email,$pdf_file,$level)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->sendCertificateEmail($email,$pdf_file,$level);
    }

    public function uploadCertificate($image_file)
    {
        $missionRepository = new MissionRepository;
        return $missionRepository->uploadCertificate($image_file);
    }

    public function getLastPlayedTask($journey_id, $child_id)
    {
        $journeyRepository = new JourneyRepository();
        return $journeyRepository->getLastPlayedTask($journey_id, $child_id);
    }

    public function getLastSolvedTask($journey_id, $child_id)
    {
        $journeyRepository = new JourneyRepository();
        return $journeyRepository->getLastSolvedTask($journey_id, $child_id);
    }

    public function getModelAnswerUnlocked($mission_id,$user_id){
        $missionRepository = new MissionRepository;
        return $missionRepository->getModelAnswerUnlocked($mission_id,$user_id);
    }
    public function removeTaskProgress(TaskProgress $taskProgress){
        $taskRepository = new TaskRepository;
        return $taskRepository->removeTaskProgress($taskProgress);
    }
    public function storeTaskProgress(TaskProgress $taskProgress){
        $taskRepository = new TaskRepository;
        return $taskRepository->storeTaskProgress($taskProgress);
    }

    public function getJourneyNotInIds($ids){
        $journeyRepository = new JourneyRepository();
        return $journeyRepository->getJourneyNotInIds($ids);
    }


    public function getUserTaskProgressActivity(Task $task,$user_id,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getUserTaskProgressActivity( $task,$user_id,$activity_id,$is_default);
    }

    public function getTasksInActivity($activity_id, $count = null, $is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getTasksInActivity($activity_id, $count, $is_default);
    }

    public function getDefaultActivityById($id){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getDefaultActivityById($id);
    }

    public function getActivityById($id){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getActivityById($id);
    }

    public function storeTaskProgressActivity(TaskProgressActivity $taskProgressActivity){
        $activityRepository = new ActivityRepository();
        return $activityRepository->storeTaskProgressActivity($taskProgressActivity);
    }

    public function deleteTaskProgressActivity(TaskProgressActivity $taskProgressActivity){
        $activityRepository = new ActivityRepository();
        return $activityRepository->deleteTaskProgressActivity($taskProgressActivity);
    }

    public function getFirstTaskInActivity($activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getFirstTaskInActivity($activity_id,$is_default);
    }

    public function getActivityTaskOrder(Task $task,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getActivityTaskOrder($task,$activity_id,$is_default);
    }

    public function getLastSolvedTaskinActivity($user_id,$activity_id,$is_default= true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getLastSolvedTaskinActivity($user_id,$activity_id,$is_default);
    }

    public function getLastTaskInActivity($activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getLastTaskInActivity($activity_id,$is_default);
    }

    public function getLastMainTaskInActivity($activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getLastMainTaskInActivity($activity_id,$is_default);
    }

    public function getTaskInActivityByOrder($order,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getTaskInActivityByOrder($order,$activity_id,$is_default);
    }

    public function getAllActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getAllActivities($limit, $search, $count, $is_b2b, $is_b2c);
    }

    public function getAllMineCraftActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getAllMineCraftActivities($limit,$search,$count,$is_b2b,$is_b2c);
    }

    public function getActivityPeriodsByAccountTypeId($account_type_id,$period= 0){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getActivityPeriodsByAccountTypeId($account_type_id,$period);
    }

    public function getActivityPrice($account_type_id,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getActivityPrice($account_type_id,$activity_id,$is_default);
    }

    public function getSolvedTasksInActivity($user_id,$activity_id,$is_default= true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getSolvedTasksInActivity($user_id,$activity_id,$is_default);
    }

    public function getAllTasksInActivity($activity_id, $count = null, $is_default = true){
        $activityRepository = new ActivityRepository;
        return $activityRepository->getAllTasksInActivity($activity_id, $count, $is_default);
    }

    public function getSolvedActivityProgress($user_id,$activity_id,$is_default= true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getSolvedActivityProgress($user_id,$activity_id,$is_default);
    }

    public function getActivityProgressForTask($user_id,$task_id,$activity_id,$is_default= true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getActivityProgressForTask($user_id,$task_id,$activity_id,$is_default);
    }

    public function getLastPlayedProgressActivity($user_id,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getLastPlayedProgressActivity($user_id,$activity_id,$is_default);
    }

    public function getStuckProgressActivity($user_id,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getStuckProgressActivity($user_id,$activity_id,$is_default);
    }

    public function getActivityTasksInOrder($activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getActivityTasksInOrder($activity_id,$is_default);
    }

    public function getUserSavedCodeActivity(Mission $mission,$user_id,$activity_id,$is_default=true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getUserSavedCodeActivity($mission,$user_id,$activity_id,$is_default);
    }
    public function getUserSavedCodeByNameActivity(Mission $mission,$user_id,$name,$activity_id,$is_default = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getUserSavedCodeByNameActivity($mission,$user_id,$name,$activity_id,$is_default);
    }

    public function removeMissionSaving(MissionSavingActivity $missionSavingActivity){
        $activityRepository = new ActivityRepository();
        return $activityRepository->removeMissionSaving($missionSavingActivity);
    }

    public function getRoboClubFreeJourneys(){
        $journeyRepository = new JourneyRepository();
        return $journeyRepository->getRoboClubFreeJourneys();
    }

    public function getAllJourneys(){
        $journeyRepository = new JourneyRepository();
        return $journeyRepository->getAllJourneys();
    }

    public function getFirstTaskIdInActivity($activity_id){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getFirstTaskIdInActivity($activity_id);
    }

    public function getDefaultActivityUnlockable($activity_id, $is_mobile = true){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getDefaultActivityUnlockable($activity_id,$is_mobile);
    }
    public function getTaskById($task_id){
        $activityRepository = new ActivityRepository();
        return $activityRepository->getTaskById($task_id);

    }

    public function checkQuestionInLesson($mission_id,$question_id){
        $questionRepository = new QuestionRepository();
        return $questionRepository->checkQuestionInLesson($mission_id,$question_id);
    }
    public function getDragCellById($cell_id){
        $questionRepository = new QuestionRepository();
        return $questionRepository->getDragCellById($cell_id);

    }
    public function getDragChoiceById($choice_id){
        $questionRepository = new QuestionRepository();
        return $questionRepository->getDragChoiceById($choice_id);

    }

    public function getDropdownById($dropdown_id){
        $questionRepository = new QuestionRepository();
        return $questionRepository->getDropdownById($dropdown_id);

    }

    public function getDropdownChoiceById($choice_id){
        $questionRepository = new QuestionRepository();
        return $questionRepository->getDropdownChoiceById($choice_id);

    }

    public function getDropdownCorrectAnswer($dropdown_id){
        $questionRepository = new QuestionRepository();
        return $questionRepository->getDropdownCorrectAnswer($dropdown_id);

    }

    public function getQuestionsByQuiz(Quiz $quiz){
        $questionRepository = new QuestionRepository();
        return $questionRepository->getQuestionsByQuiz($quiz);
    }

    public function getConfidence(){
        
        $confidenceMapper = Analogue::mapper(Confidence::class);
        $query =  $confidenceMapper->query()->get();

        return $query;
    }
}