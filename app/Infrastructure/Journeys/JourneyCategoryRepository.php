<?php

namespace App\Infrastructure\Journeys;


use App\DomainModelLayer\Journeys\JourneyCategory;
use App\Framework\Exceptions\BadRequestException;
use Analogue;

class JourneyCategoryRepository
{
  public function getAllCategories()
  {
    $journeycategoryMapper = Analogue::mapper(JourneyCategory::class);
    $journeycategories = $journeycategoryMapper->query()->get();
    return $journeycategories;
  }

  public function getCategoryJournies($id)
  {
    $journeycategoryMapper = Analogue::mapper(JourneyCategory::class);
    $journeycategory = $journeycategoryMapper->query()->whereId($id)->first();
    if($journeycategory == null)
            throw new BadRequestException("Journey Category doesn't Exist");
    return $journeycategory->getJourneys();
  }
  
}