<?php


namespace App\Infrastructure\Accounts;


use App\DomainModelLayer\Accounts\Plan;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\PlanPeriod;
use Analogue;
use Carbon\Carbon;

class PlanRepository
{

    public function getPlansByAccountType($account_type_id){
        //return static::where('name',$name)->first();
        $planMapper = Analogue::mapper(Plan::class);
        $plans = $planMapper->query()->get();
        $updated_plans = [];
        foreach ($plans as $plan) {
            $updated_plan = $this->getlastupdated($plan,$account_type_id);
            if($updated_plan != null)
            {
                //dd($plan->getPublicPermission());
                if(($updated_plan->account_type_id == $account_type_id) && $plan->getPublicPermission() && !$updated_plan->isExtra())
                   $updated_plans[] = $updated_plan;
           }            
        }
        //$plans = $planMapper->query()->where('account_type_id',$account_type_id)->get();
        return $updated_plans;
    }

    public function getExtraPlans($account_type_id)
    {
        $planMapper = Analogue::mapper(Plan::class);
        $plans = $planMapper->query()->get();
        $updated_plans = [];
        foreach ($plans as $plan) {
            $updated_plan = $this->getlastupdated($plan,$account_type_id);

            if($updated_plan != null)
            {
                if(($updated_plan->account_type_id == $account_type_id) && $plan->getPublicPermission() && $updated_plan->isExtra())
                   $updated_plans[] = $updated_plan;
           }            
        }
        return $updated_plans;
    }

    public function getPlanById($plan_id)
    {
    	$planMapper = Analogue::mapper(Plan::class);
        $plan = $planMapper->query()->whereId($plan_id)->first();
        return $plan;
    }

    public function getPlanByName($plan_name)
    {
        $planMapper = Analogue::mapper(Plan::class);
        $plan = $planMapper->query()->whereName($plan_name)->first();
        return $plan;
    }

    public function getlastupdated(Plan $plan,$account_type_id)
    {
    	$historyMapper =  Analogue::mapper(PlanHistory::class);
        $updated = $historyMapper->query()->where('plan_id',$plan->getId())->where('account_type_id',$account_type_id)->orderBy('created_at','ASC')->first();
        return $updated;
    }

    public function checkSubscription(PlanHistory $updated_plan, Account $account)
    {
        $subscriptionMapper =  Analogue::mapper(Subscription::class);
        $check = $subscriptionMapper->query()->where('plan_history_id', $updated_plan->getId())->where('account_id',$account->getId())->where(function($query){
                  $query->whereNull('ends_at');
                })->first();
        return ($check != null ? true : false);
    }

    public function getAvailblePlanPeriods()
    {
        $periodMapper = Analogue::mapper(PlanPeriod::class);
        return $periodMapper->query()->get();
    }

    public function getPlanPeriodByName($name)
    {
        $periodMapper =  Analogue::mapper(PlanPeriod::class);
        return $periodMapper->query()->whereName($name)->first();
    }

    public function getAllPlanNames(){
        $planHistoryMapper = Analogue::mapper(PlanHistory::class);
        return $planHistoryMapper->query()->get();
    }

}