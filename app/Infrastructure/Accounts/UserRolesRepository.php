<?php

namespace App\Infrastructure\Repositories;


use App\DomainModelLayer\Customers\IUserRolesRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class UserRolesRepository extends Model implements IUserRolesRepository
{
    protected $table = 'user_roles';
    protected $fillable = ['user_id', 'role_id'];

    function AddRole($role_id, $user_id){
        DB::beginTransaction();
        $this->user_id = $user_id;
        $this->role_id = $role_id;

        if ($this->save()){
            DB::commit();
        }
        else{
            DB::rollBack();
            return $this->internalError(['Saving Role Failed']);
        }
    }
}