<?php

namespace App\Infrastructure\Accounts;
use App\DomainModelLayer\Accounts\GuestData;
use Mail;
use Analogue;

class GuestRepository
{
    public function sendFeedbackMail($name, $feeling, $email, $msg){
        try{
            Mail::send('emails.feedback', [
                'name' => $name,
                'feeling' => $feeling,
                'msg' => $msg
            ], function ($m) use ($name, $email) {
                $m->from($email ? $email : config('app.MAIN_MAIL'), $name ? $name : 'RoboGarden User')
                    ->to(config('app.MAIN_MAIL'))
                    ->subject("Feedback From: " . $email);
            });
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getUserByIpAddress($ip_address){
        $guestDataMapper = Analogue::mapper(GuestData::class);
        return $guestDataMapper->query()->where('ip_address', $ip_address)->first();
    }

    public function storeGuestData(GuestData $guestData){
        $guestDataMapper = Analogue::mapper(GuestData::class);
        return $guestDataMapper->store($guestData);
    }
}