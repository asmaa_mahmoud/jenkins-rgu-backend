<?php

namespace App\Infrastructure\Accounts;


use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\DomainModelLayer\Accounts\Account;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\CustomException;
use Analogue;
use Mail;
use DB;
use Carbon\Carbon;
use App\Helpers\PaymentModule;
use Config;

class SubscriptionRepository
{
    public function getSubscriptionById($id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        return $subscriptionMapper->query()->whereId($id)->first();
    }

    public function getChildItem(Subscription $subscription)
    {
        $subscriptionItemMapper = Analogue::mapper(SubscriptionItem::class);
        return $subscriptionItemMapper->query()
            ->where('subscription_id', $subscription->getId())
            ->whereNotIn('plan_history_id', DB::table('plan_history')->where('is_extra', 1)->pluck('id')->toArray())->first();
    }

    public function getChildItems($subscription)
    {
        $subscriptionItemMapper = Analogue::mapper(SubscriptionItem::class);
        return $subscriptionItemMapper->query()
            ->where('subscription_id', $subscription->getId())
            ->whereNotIn('plan_history_id', DB::table('plan_history')->where('is_extra', 1)->pluck('id')->toArray())->get();
    }

    public function getStripeSubsriptionByStripeId($stripeId)
    {
        $subscriptionMapper = Analogue::mapper(StripeSubscription::class);
        return $subscriptionMapper->query()->where('stripe_id', $stripeId)->first();
    }

    public function storeSubscription(Subscription $subscription)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscriptionMapper->store($subscription);
    }

    public function handlePaidSubscription($token = null, $plan_name, $plan_period, $account_type_name, $stripe_id = null, $trial, $country, $noOfChildren = 0,$coupon_id = null)
    {
        $payment_module = new PaymentModule;
        $mode = config('services.payment.mode');
        if ($plan_name != "default")
            $stripe_plan = $plan_name . '_' . $account_type_name . '_' . $plan_period . '_' . $mode;
        else
            $stripe_plan = $plan_name . '_plan_unconfirmed_child_' . $mode;
        $customer_id = null;

        if ($stripe_id == null) {
            if ($token == null)
                throw new CustomException('Please update your billing information to be able to subscribe', 223);
            else {
                $customer = $payment_module->createCustomer($token);
                $customer_id = $customer->id;
            }
        } else {
            if ($token == null)
                $customer_id = $stripe_id;
            else {
                $customer = $payment_module->updateCard($stripe_id, $token);
                $customer_id = $customer->id;
            }
        }
        $childPlan = null;
        if ($noOfChildren > 0) {
            $childPlan = $plan_name . "_child";
            if (strpos($childPlan, 'child') !== false) {
                $childPlan = str_replace('_', '_' . 'monthly' . '_', $childPlan);
                $childPlan = $childPlan . '_' . $mode;
            } else
                $childPlan = $childPlan . '_' . $account_type_name . '_' . 'monthly' . '_' . $mode;
        }

        return $payment_module->subscribe($stripe_plan, $customer_id, $trial, $country, $noOfChildren, $childPlan,$coupon_id);
    }

    public function handleCancelPaidSubscription(StripeSubscription $stripeSubscription)
    {
        $payment_module = new PaymentModule;
        $stripe_id = $stripeSubscription->getStripeId();
        $subscription = $payment_module->unsubscribeAtPeriodEnd($stripe_id);
        return ["cancel_at_period_end" => $subscription->cancel_at_period_end, 'period_end' => $subscription->current_period_end];
    }

    public function handleUpgradeSubscription(StripeSubscription $stripeSubscription, $plan_name, $period, $account_type_name)
    {
        $payment_module = new PaymentModule;
        $mode = config('services.payment.mode');
        $stripe_id = $stripeSubscription->getStripeId();
        $stripe_plan = $plan_name . '_' . $account_type_name . '_' . $period . '_' . $mode;
        $subscription = $payment_module->changePlan($stripe_id, $stripe_plan);
        return ["plan_id" => $subscription->plan->id];
    }

    public function emailSubscription($account_name, $plan_name, $amount, $currency, $interval, $quantity, $account_email, $trial = false)
    {
        if ($account_email != null || $account_email != '') {
            try {
                Mail::send('emails.subscribe', ['planName' => $plan_name,
                    'username' => $account_name,
                    'amount' => $amount,
                    'currency' => $currency,
                    'interval' => $interval,
                    'quantity' => $quantity,
                    'trial' => $trial,
                ], function ($m) use ($account_email) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($account_email)->subject(trans('locale.new_subscription_subject'));
                });

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());

            }
        }

    }

    public function emailUnsubscription($account_name, $plan_name, $end_date, $account_email)
    {
        if ($account_email != null || $account_email != '') {
            try {
                Mail::send('emails.unsubscribe', ['planName' => $plan_name,
                    'username' => $account_name,
                    'end_date' => $end_date,
                ], function ($m) use ($account_email) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($account_email)->subject(trans('locale.unsubscription_email_subject'));
                });

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());

            }
        }
    }

    public function emailUpgrade($account_name, $new_plan_name, $old_plan_name, $account_email)
    {
        if ($account_email != null || $account_email != '') {
            try {
                Mail::send('emails.upgrade', ['new_planName' => $new_plan_name,
                    'username' => $account_name,
                    'old_planName' => $old_plan_name,
                ], function ($m) use ($account_email) {
                    $m->from('info@robogarden.ca', 'The RoboGarden Team');
                    $m->to($account_email)->subject('Upgrading Email');
                });

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());

            }
        }
    }

    public function getLastSubscription(Account $account)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscription = $subscriptionMapper->query()->where('account_id', $account->getId())->orderBy('created_at', 'DESC')->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->first();
        return $subscription;
    }

    public function getLastSubscriptionById($account_id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscription = $subscriptionMapper->query()->where('account_id', $account_id)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->first();
        return $subscription;
    }

    public function getActiveSubscriptions($account_id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscriptions = $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->whereNull('ends_at')->orWhere('ends_at', '>', Carbon::now());
        })->get();
        return $subscriptions;
    }

    public function getActiveSubscriptionsWithoutFree($account_id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscriptions = $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->whereNull('ends_at')->orWhere('ends_at', '>', Carbon::now());
        })->whereNotIn('plan_history_id', DB::table('plan_history')->where('new_name', 'Free')->pluck('id')->toArray())
            ->orderBy('created_at', 'DESC')->get();
        return $subscriptions;

    }

    public function getActiveFreeSubscription($account_id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        return $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->whereNull('ends_at')->orWhere('ends_at', '>', Carbon::now());
        })->whereIn('plan_history_id', DB::table('plan_history')->where('new_name', 'Free')->pluck('id')->toArray())->first();

    }

    public function getNotCancelledSubscriptions($account_id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscriptions = $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->whereNull('ends_at');
        })->get();
        return $subscriptions;
    }

    public function getNotCancelledSubscriptionsWithoutFree($account_id)
    {
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscription = $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->whereNull('ends_at');
        })->whereNotIn('plan_history_id', DB::table('plan_history')->where('new_name', 'Free')->pluck('id')->toArray())->first();
        return $subscription;
    }

    public function getCancelledSubscription($account_id){
        $subscriptionMapper = Analogue::mapper(Subscription::class);
        $subscription = $subscriptionMapper->query()->where('account_id', $account_id)->where(function ($query) {
            $query->where('ends_at', '>', Carbon::now());
        })->whereNotIn('plan_history_id', DB::table('plan_history')->where('new_name', 'Free')->pluck('id')->toArray())->first();
        return $subscription;
    }

    public function createStripeCustomer($token = null)
    {
        $payment_module = new PaymentModule;
        return $payment_module->createCustomer($token)->id;
    }

    public function updateStripeCustomer($customerId, $token)
    {
        $payment_module = new PaymentModule;
        return $payment_module->updateCard($customerId, $token)->id;
    }

    public function addSubscriptionItem(StripeSubscription $stripeSubscription, $plan_name, $period, $account_type_name = 'Individual', $end_trial = true, $quantity = 1)
    {
        $mode = config('services.payment.mode');
        if (strpos($plan_name, 'child') !== false) {
            $plan = str_replace('_', '_' . $period . '_', $plan_name);
            $plan = $plan . '_' . $mode;
        } else
            $plan = $plan_name . '_' . $account_type_name . '_' . $period . '_' . $mode;

        $payment_module = new PaymentModule;
        return $payment_module->addSubscriptionItem($stripeSubscription->getStripeId(), $plan, $end_trial, $quantity);
    }

    public function changeSubscriptionItemQuantity($stripe_id, $new_quantity = 1)
    {
        $payment_module = new PaymentModule;
        return $payment_module->changeSubscriptionItemQuantity($stripe_id, $new_quantity);
    }

    public function getTrialEnd(StripeSubscription $stripeSubscription)
    {
        $payment_module = new PaymentModule;
        $subscription = $payment_module->retrieveSubscription($stripeSubscription->getStripeId());
        return date('Y-m-d H:i:s', $subscription->trial_end);
    }

    public function removeSubscriptionItem($item_id, $prorate = true)
    {
        $payment_module = new PaymentModule;
        return $payment_module->removeSubscriptionItem($item_id, $prorate);
    }

    public function deleteSubscriptionItem(SubscriptionItem $subscriptionItem)
    {
        $subscriptionItemMapper = Analogue::mapper(SubscriptionItem::class);
        $subscriptionItemMapper->delete($subscriptionItem);
    }

    public function getLastRoboPalItem($subscription_id)
    {
        $subscriptionItemMapper = Analogue::mapper(SubscriptionItem::class);
        $subscriptionItem = $subscriptionItemMapper->query()->where('subscription_id', $subscription_id)->whereIn('plan_history_id', (DB::table('plan_history')->where('new_name', 'RoboPal')->pluck('id')->toArray()))->orderBy('created_at', 'DESC')->first();
        return $subscriptionItem;
    }

    public function retrieveSubscription($subscription_id)
    {
        $payment_module = new PaymentModule;
        return $payment_module->retrieveSubscription($subscription_id);
    }

    public function changeSubscriptionTax($subscriptionId, $newCountry)
    {
        $payment_module = new PaymentModule;
        return $payment_module->changeCountryForSubscription($subscriptionId, $newCountry);
    }

    public function charge($amount, $customer_id, $description)
    {
        $mode = config('services.payment.mode');

        $payment_module = new PaymentModule;
        return $payment_module->addCharge($amount, $customer_id, $description);
    }
}