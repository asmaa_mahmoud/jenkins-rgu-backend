<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/24/2017
 * Time: 3:56 PM
 */

namespace App\Infrastructure\Accounts;
use Analogue;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use App\DomainModelLayer\Accounts\GuestData;
use App\DomainModelLayer\Accounts\PushNotification;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\InvoiceDetailsDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\Plan;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;
use App\DomainModelLayer\Accounts\StripeEvent;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use App\DomainModelLayer\Accounts\WorldUserDetails;
use App\DomainModelLayer\Accounts\WorldUserParameter;
use App\DomainModelLayer\Accounts\WorldUserPrediction;
use App\DomainModelLayer\Accounts\WorldUserScore;

class AccountMainRepository implements IAccountMainRepository
{

    public function activate($token_url){
        $accountRepository = new AccountRepository;
        return $accountRepository->activate($token_url);
    }

    public function validate_url($token_url)
    {
        $accountRepository = new AccountRepository;
        return $accountRepository->validate_url($token_url);
    }

    public function getAccountByUser($user)
    {
        $AccountMapper = Analogue::mapper(Account::class);
        return $AccountMapper->query()->where('id', $user->account_id)->first();
    }

    public function LoginUser($credentials,$gmt_difference)
    {
        $userRepository = new UserRepository;
        return $userRepository->LoginUser($credentials,$gmt_difference);
    }

    public function GetUser($username)
    {
        $userRepository = new UserRepository;
        return $userRepository->GetUser($username);
    }

    public function getUserById($id)
    {
       $userRepository = new UserRepository;
        return $userRepository->getUserById($id); 
    }

    public function getLastSubscription(Account $account)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getLastSubscription($account);
    }

    public function getLastSubscriptionById($account_id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getLastSubscriptionById($account_id);
    }

    public function storeSubscription(Subscription $subscription)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->storeSubscription($subscription);
    }

    public function getAccountTypeByName($name)
    {
        $accountTypeRepository = new AccountTypeRepository;
        return $accountTypeRepository->getAccountTypeByName($name);
    }

    public function getAccountByAccountType($accountType){
        $accountRepository = new AccountRepository();
        return $accountRepository->getAccountByAccountType($accountType);
    }

//    public function AlreadyRegistered(UserDto $userDto)
    public function AlreadyRegistered($userDto)
    {
        $userRepository = new UserRepository;
        return $userRepository->AlreadyRegistered($userDto);
    }


//    public function UserNameAlreadyExist(UserDto $userDto)
   // public function UserNameAlreadyExist($userDto)

    public function isAlreadyRegistered($userEmail)
    {
        $userRepository = new UserRepository;
        return $userRepository->isAlreadyRegistered($userEmail);
    }

    public function UserNameAlreadyExist(UserDto $userDto)

    {
        $userRepository = new UserRepository;
        return $userRepository->UserNameAlreadyExist($userDto);
    }

    public function isUserNameAlreadyExist($username)
    {
        $userRepository = new UserRepository;
        return $userRepository->isUserNameAlreadyExist($username);
    }

    public function getRoleByName($roleName)
    {
        $LUTRoleRepository = new LUTRoleRepository;
        return $LUTRoleRepository->getRoleByName($roleName);
    }

    public function AddUser(User $user)
    {
        $userRepository = new UserRepository;
        return $userRepository->AddUser($user);
    }

    public function getPlanByName($plan_name)
    {
        $planRepository = new PlanRepository;
        return $planRepository->getPlanByName($plan_name);
    }

    public function getlastupdated(Plan $plan,$account_type_id)
    {
        $planRepository = new PlanRepository;
        return $planRepository->getlastupdated($plan,$account_type_id);
    }

    public function checkSubscription(PlanHistory $updated_plan, Account $account)
    {
        $planRepository = new PlanRepository;
        return $planRepository->checkSubscription($updated_plan, $account);
    }

    public function getAvailblePlanPeriods()
    {
        $planRepository = new PlanRepository;
        return $planRepository->getAvailblePlanPeriods();
    }

    public function getPlanPeriodByName($name)
    {
        $planRepository = new PlanRepository;
        return $planRepository->getPlanPeriodByName($name);
    }

    public function getPlansByAccountType($account_type_id)
    {
        $planRepository = new PlanRepository;
        return $planRepository->getPlansByAccountType($account_type_id);
    }

    public function emailSubscription($account_name,$plan_name,$amount,$currency,$interval,$quantity,$account_email,$trial = false)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->emailSubscription($account_name,$plan_name,$amount,$currency,$interval,$quantity,$account_email,$trial);
    }

    public function emailUnsubscription($account_name,$plan_name,$end_date,$account_email)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->emailUnsubscription($account_name,$plan_name,$end_date,$account_email);
    }

    public function emailUpgrade($account_name,$new_plan_name,$old_plan_name,$account_email)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->emailUpgrade($account_name,$new_plan_name,$old_plan_name,$account_email);
    }

    public function handlePaidSubscription($token = null , $plan_name,$plan_period,$account_type_name,$stripe_id = null,$trial,$country = null,$noOfChildren = 0,$coupon_id= null)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->handlePaidSubscription($token, $plan_name,$plan_period,$account_type_name,$stripe_id,$trial,$country,$noOfChildren,$coupon_id);
    }

    public function getActiveSubscriptions($account_id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getActiveSubscriptions($account_id);
    }

    public function getNotCancelledSubscriptions($account_id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getNotCancelledSubscriptions($account_id);
    }

    public function storeUser(User $user)
    {
        $userRepository = new UserRepository;
        return $userRepository->storeUser($user);
    }

    public function saveProfileImage(User $user,$image_name,$image_data)
    {
        $userRepository = new UserRepository;
        return $userRepository->saveProfileImage($user,$image_name,$image_data);
    }

    public function getAllTypes()
    {
        $accounttypeRepository = new AccountTypeRepository;
        return $accounttypeRepository->getAllTypes();
    }

    public function getSubscriptionById($id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getSubscriptionById($id);
    }

    public function handleCancelPaidSubscription(StripeSubscription $stripeSubscription)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->handleCancelPaidSubscription($stripeSubscription);
    }

    public function handleUpgradeSubscription(StripeSubscription $stripeSubscription,$plan_name,$period,$account_type_name)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->handleUpgradeSubscription($stripeSubscription,$plan_name,$period,$account_type_name);
    }

    public function getFacebookProfileResponse($params)
    {
        $userRepository = new UserRepository;
        return $userRepository->getFacebookProfileResponse($params);
    }

    public function getGoogleProfileResponse($params)
    {
        $userRepository = new UserRepository;
        return $userRepository->getGoogleProfileResponse($params);
    }

    public function checkSocialUserRegistered($socialId,$provider)
    {
        $userRepository = new UserRepository;
        return $userRepository->checkSocialUserRegistered($socialId,$provider);
    }

    public function requestTwitterTokenOauth($consumerKey,$consumerSecret,$redirectUri)
    {
        $userRepository = new UserRepository;
        return $userRepository->requestTwitterTokenOauth($consumerKey,$consumerSecret,$redirectUri);
    }

    public function getTwitterAccessTokenOauth($consumerKey,$consumerSecret,$oauthToken,$oauthVerifier)
    {
        $userRepository = new UserRepository;
        return $userRepository->getTwitterAccessTokenOauth($consumerKey,$consumerSecret,$oauthToken,$oauthVerifier);
    }

    public function AlreadyRegisteredAnotherUser($email, User $user)
    {
        $userRepository = new UserRepository;
        return $userRepository->AlreadyRegisteredAnotherUser($email, $user);
    }

    public function UserNameAlreadyExistAnotherUser($username, User $user)
    {
        $userRepository = new UserRepository;
        return $userRepository->UserNameAlreadyExistAnotherUser($username, $user);
    }

    public function getTokenfromUser($user_id)
    {
        $userRepository = new UserRepository;
        return $userRepository->getTokenfromUser($user_id);
    }

    public function storePosition(UserPosition $position)
    {
        $userRepository = new UserRepository;
        return $userRepository->storePosition($position);
    }

    public function deletePosition(UserPosition $position)
    {
        $userRepository = new UserRepository;
        return $userRepository->deletePosition($position);
    }

    public function storeInvitation(Invitation $Invitation)
    {
        $invitationRepository = new InvitationRepository;
        return $invitationRepository->storeInvitation($Invitation);
    }

    public function getInvitationByCode($code)
    {
        $invitationRepository = new InvitationRepository;
        return $invitationRepository->getInvitationByCode($code);
    }

    public function getUserNotifications($user_id)
    {
        $notificationRepository = new NotificationRepository;
        return $notificationRepository->getUserNotifications($user_id);
    }

    public function storeNotification(Notification $notification)
    {
        $notificationRepository = new NotificationRepository;
        return $notificationRepository->storeNotification($notification);
    }

    public function getNotificationById($id)
    {
        $notificationRepository = new NotificationRepository;
        return $notificationRepository->getNotificationById($id);
    }

    public function getParentChildren($id)
    {
        $userRepository = new UserRepository;
        return $userRepository->getParentChildren($id);
    }


    public function getDefaultAccount(){

        $accountRepository = new AccountRepository;
        return $accountRepository->getDefaultAccount();
    }

    public function isRegisteredSupervisor($supervisorEmail){
        $userRepository = new UserRepository;
        return $userRepository->isRegisteredSupervisor($supervisorEmail);
    }

    public function supervisorEmailExists($email)
    {
        $userRepository = new UserRepository;
        return $userRepository->supervisorEmailExists($email);
    }

    public function createStripeCustomer($token = null)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->createStripeCustomer($token);
    }
    public function updateStripeCustomer($customerId,$token)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->updateStripeCustomer($customerId,$token);
    }
    public function childConfirmEmail(UserDto $childUserDto,$parentName,$parentAccountTypeName){
        $userRepository = new UserRepository;
        return $userRepository->childConfirmEmail($childUserDto,$parentName,$parentAccountTypeName);
    }
    public function getUserByEmail($email){
        $userRepository = new UserRepository;
        return $userRepository->getUserByEmail($email);
    }
    public function getFamilyAccountType(){
        $accountTypeRepository = new AccountTypeRepository;
        return $accountTypeRepository->getFamilyAccountType();
    }

    public function storeAccount(Account $account){
        $accountRepository = new AccountRepository;
        return $accountRepository->storeAccount($account);
    }

    public function deleteAccount(Account $account)
    {
        $accountRepository = new AccountRepository;
        return $accountRepository->deleteAccount($account);
    }

    public function addSubscriptionItem(StripeSubscription $stripeSubscription,$plan_name,$period, $account_type_name = 'Individual',$end_trial = true,$quantity = 1)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->addSubscriptionItem($stripeSubscription,$plan_name,$period,$account_type_name,$end_trial,$quantity);
    }
    public function changeSubscriptionItemQuantity($stripe_id,$new_quantity = 1)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->changeSubscriptionItemQuantity($stripe_id,$new_quantity);
    }

    public function storeStripeEvent(StripeEvent $stripeEvent){
        $stripeRepository = new StripeRepository;
        return $stripeRepository->storeStripeEvent($stripeEvent);
    }
    
    public function getAccountByCustomerId($customerId){
        $accountRepository = new AccountRepository;
        return $accountRepository->getAccountByCustomerId($customerId);
    }

    public function getAccountById($accountId){
        $accountRepository = new AccountRepository;
        return $accountRepository->getAccountById($accountId);
    }
    
    public function sendPaymentFailedEmail(User $user,$trialNumber){
        $userRepository = new UserRepository;
        return $userRepository->sendPaymentFailedEmail($user,$trialNumber); 
    }
    public function getStripeSubsriptionByStripeId($stripeId){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getStripeSubsriptionByStripeId($stripeId);
    }
    public function getChildsBySupervisorEmail($supervisorEmail){
        $userRepository = new UserRepository;
        return $userRepository->getChildsBySupervisorEmail($supervisorEmail); 
    }
    public function customerInfo($customerId){
        $userRepository = new UserRepository;
        return $userRepository->customerInfo($customerId);
    }
    public function customerInvoices($customerId){
        $userRepository = new UserRepository;
        return $userRepository->customerInvoices($customerId);
    }
    public function retrieveInvoiceDetails($invoice_id){
        $userRepository = new UserRepository;
        return $userRepository->retrieveInvoiceDetails($invoice_id);
    }

    public function requestParentEmail(User $user, $parent_email,$account_type, $parent_name)
    {
        $userRepository = new UserRepository;
        return $userRepository->requestParentEmail($user, $parent_email,$account_type, $parent_name);
    }
    public function sendContactUsEmail(SupportEmailDto $supportEmailDto){
        $userRepository = new UserRepository;
        return $userRepository->sendContactUsEmail($supportEmailDto);
    }

    public function sendSupportEmail(User $user,SupportEmailDto $supportEmailDto){
        $userRepository = new UserRepository;
        return $userRepository->sendSupportEmail($user,$supportEmailDto);
    }
    public function getAllUsers()
    {
        $userRepository = new UserRepository;
        return $userRepository->getAllUsers();
    }

    public function createPasswordReset($email, $username){
        $userRepository = new UserRepository;
        return $userRepository->createPasswordReset($email, $username);
    }

    public function sendResetPasswordEmail(PasswordResetDto $passwordResetDto){
        $userRepository = new UserRepository;
        return $userRepository->sendResetPasswordEmail($passwordResetDto);
    }
    public function confirmResetPassword(PasswordResetDto $resetPasswordDto,$newPassword){
        $userRepository = new UserRepository;
        return $userRepository->confirmResetPassword($resetPasswordDto,$newPassword);
    }
    public function requestParentToSubscribe(User $user,User $parent,$name,$is_plan){
        $userRepository = new UserRepository;
        return $userRepository->requestParentToSubscribe($user,$parent,$name,$is_plan);
    }
    public function getUserUnreadNotications($user_id){
        $notificationRepository = new NotificationRepository;
        return $notificationRepository->getUserUnreadNotications($user_id);
    }

     public function resetChildsSupervisorEmail($supervisorEmail,$old_email){
        $userRepository = new UserRepository;
        return $userRepository->resetChildsSupervisorEmail($supervisorEmail,$old_email);
     }

    public function beginDatabaseTransaction()
    {
        $userRepository = new UserRepository;
        return $userRepository->beginDatabaseTransaction();
    }

    public function commitDatabaseTransaction()
    {
        $userRepository = new UserRepository;
        return $userRepository->commitDatabaseTransaction();
    }

    public function rollBackDatabaseTransaction()
    {
        $userRepository = new UserRepository;
        return $userRepository->rollBackDatabaseTransaction();
    }

    public function getTrialEnd(StripeSubscription $stripeSubscription)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getTrialEnd($stripeSubscription);
    }

    public function sendInvoiceEmail(InvoiceDetailsDto $invoiceDetails,User $user)
    {
        $userRepository = new UserRepository;
        return $userRepository->sendInvoiceEmail($invoiceDetails, $user);
    }

    public function requestCountryChange(User $user,$newCountry){
        $userRepository = new UserRepository;
        return $userRepository->requestCountryChange($user,$newCountry);
    }

    public function getRbf($code)
    {
       $userRepository = new UserRepository;
        return $userRepository->getRbf($code); 
    }

    public function isAuthorized($permission,User $user){
        $userRepository = new UserRepository;
        return $userRepository->isAuthorized($permission,$user);

    }

    public function getUserByUsername($username){
        $userRepository = new UserRepository;
        return $userRepository->getUserByUsername($username);
    }

    public function getExtraPlans($account_type_id)
    {
        $planRepository = new PlanRepository;
        return $planRepository->getExtraPlans($account_type_id);
    }

    public function getChildItem(Subscription $subscription)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getChildItem($subscription);
    }

    public function removeSubscriptionItem($item_id,$prorate = true)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->removeSubscriptionItem($item_id,$prorate);
    }

    public function deleteSubscriptionItem(SubscriptionItem $subscriptionItem)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->deleteSubscriptionItem($subscriptionItem);
    }

    public function retrieveSubscription($subscription_id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->retrieveSubscription($subscription_id);
    }

    public function getLastRoboPalItem($subscription_id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getLastRoboPalItem($subscription_id);
    }
    
    public function changeCountryForUser(User $user,$newCountry){
        $userRepository = new UserRepository;
        return $userRepository->changeCountryForUser($user,$newCountry);
    }

    public function changeSubscriptionTax($subscriptionId,$newCountry){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->changeSubscriptionTax($subscriptionId,$newCountry);
    }

    public function getStatusByName($name)
    {
        $accountRepository = new AccountRepository;
        return $accountRepository->getStatusByName($name);
    }

    public function deleteUser(User $user){
        $userRepository = new UserRepository;
        return $userRepository->deleteUser($user);
    }

    public function getNumberOfUsersWithRole(Account $account,$roleName){
        $userRepository = new UserRepository;
        return $userRepository->getNumberOfUsersWithRole($account,$roleName);
    }

    public function getLanguages()
    {
        $userRepository = new UserRepository;
        return $userRepository->getLanguages();
    }
    public function getLanguageById($language_id)
    {
        $userRepository = new UserRepository;
        return $userRepository->getLanguageById($language_id);
    }

    public function reportBug($error){
        $userRepository = new UserRepository;
        return $userRepository->reportBug($error);
    }

    public function sendFeedbackMail($name, $feeling, $email, $msg){
        $guestRepository = new GuestRepository;
        return $guestRepository->sendFeedbackMail($name, $feeling, $email, $msg);
    }

    public function UserSubscription($user_id){
        $userRepository = new UserRepository;
        return $userRepository->UserSubscription($user_id);
    }

    public function translateNotification($title, $message){
        $notificationRepository = new NotificationRepository;
        return $notificationRepository->translateNotification($title, $message);
    }

    public function getChildItems($subscription){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getChildItems($subscription);
    }

    public function getCountriesUsersCount(){
        $accountRepository = new AccountRepository;
        return $accountRepository->getCountriesUsersCount();
    }
    public function addUserScore(UserScore $userScore){
        $userRepository = new UserRepository;
        return $userRepository->addUserScore($userScore);
    }
    public function removeUserScore(User $user){
        $userRepository = new UserRepository;
        return $userRepository->removeUserScore($user);
    }
    public function getUserModelAnswerUnlocked($user_id,$mission_id){
        $userRepository = new UserRepository;
        return $userRepository->getUserModelAnswerUnlocked($user_id,$mission_id);
    }

    public function storeUserModelAnswerUnlocked(ModelAnswerUnlocked $modelAnswerUnlocked){
        $userRepository = new UserRepository;
        return $userRepository->storeUserModelAnswerUnlocked($modelAnswerUnlocked);
    }
    public function getUserModelAnswers($user_id,$mission_id){
        $userRepository = new UserRepository;
        return $userRepository->getUserModelAnswers($user_id,$mission_id);
    }
    public function storeUserModelAnswer(UserModelAnswer $userModelAnswer){
        $userRepository = new UserRepository;
        return $userRepository->storeUserModelAnswer($userModelAnswer);
    }

    public function getExtrasNotInIds($extras_ids){
        $userRepository = new UserRepository;
        return $userRepository->getExtrasNotInIds($extras_ids);
    }

    public function getActiveSubscriptionsWithoutFree($account_id){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getActiveSubscriptionsWithoutFree($account_id);
    }

    public function getActiveFreeSubscription($account_id)
    {
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getActiveFreeSubscription($account_id);

    }
    public function getTax(Account $account){
        $accountRepository = new AccountRepository;
        return $accountRepository->getTax($account);
    }

    public function getUserScore($user_id){
        $userRepository = new UserRepository;
        return $userRepository->getUserScore($user_id);
    }

    public function storeUserScore(UserScore $userScore){
        $userRepository = new UserRepository;
        return $userRepository->storeUserScore($userScore);
    }

    public function getNotCancelledSubscriptionsWithoutFree($account_id){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getNotCancelledSubscriptionsWithoutFree($account_id);
    }

    public function getCancelledSubscription($account_id){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->getCancelledSubscription($account_id);
    }

    public function getAllPlanNames()
    {
        $planRepository = new PlanRepository;
        return $planRepository->getAllPlanNames();
    }

    // world cup functions
    public function getMatchById($id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getMatchById($id);
    }

    public function getParameterById($id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getParameterById($id);
    }

    public function getMatchesByRoundNo($roundNo){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getMatchesByRoundNo($roundNo);
    }

    public function getFeaturedMatchInDay($firstOfDay, $lastOfDay){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getFeaturedMatchInDay($firstOfDay, $lastOfDay);
    }

    public function getUsersPredictionInMatch($match_id, $winner, $count = false){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getUsersPredictionInMatch($match_id, $winner, $count);
    }

    public function getMatchesInDay($firstOfDay, $lastOfDay){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getMatchesInDay($firstOfDay, $lastOfDay);
    }

    public function getNoSubmission($match_id = null){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getNoSubmission($match_id);
    }

    public function getWorldUsersScore($roundNo, $user_id = null, $limit = null, $count = false){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getWorldUsersScore($roundNo, $user_id, $limit, $count);
    }

    public function getUsersScore($roundNo){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getUsersScore($roundNo);
    }

    public function getNextMatchDate(){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getNextMatchDate();
    }

    public function getAllParameters(){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getAllParameters();
    }

    public function getLastPredictionOfUser($user_id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getLastPredictionOfUser($user_id);
    }

    public function getUserPredictionInMatches($user_id, $match_ids){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getUserPredictionInMatches($user_id, $match_ids);
    }

    public function getWorldUserData($user_id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getWorldUserData($user_id);
    }

    public function storeWorldUserDetails(WorldUserDetails $worldUserDetails){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->storeWorldUserDetails($worldUserDetails);
    }

    public function storeWorldUserScore(WorldUserScore $worldUserScore){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->storeWorldUserScore($worldUserScore);
    }

    public function storeUserPrediction(WorldUserPrediction $worldUserPrediction){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->storeUserPrediction($worldUserPrediction);
    }

    public function getUnScoredPredictions(){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getUnScoredPredictions();
    }

    public function getUserParameters($user_id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getUserParameters($user_id);
    }

    public function getTomorrowMatches($dateTime){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getTomorrowMatchDate($dateTime);
    }

    public function getMatchPrediction($user_id, $match_id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getMatchPrediction($user_id, $match_id);
    }

    public function getLastMatchInDay($date){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getLastMatchInDay($date);
    }

    public function countUserPredictInRound($user_id, $roundNo){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->countUserPredictInRound($user_id, $roundNo);
    }

    public function deleteUserParameters($user_id){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->deleteUserParameters($user_id);
    }

    public function storeUserParameter(WorldUserParameter $userParameter){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->storeUserParameter($userParameter);
    }

    public function getNextMatch(){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getNextMatch();
    }

    public function getMatchesByRoundNoFromDate($startDate, $roundNo){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getMatchesByRoundNoFromDate($startDate, $roundNo);
    }

    public function getUserParameterWithName($user_id, $parameterName){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getUserParameterWithName($user_id, $parameterName);
    }

    public function getNoWorldUsers(){
        $worldCupRepository = new WorldCupRepository;
        return $worldCupRepository->getNoWorldUsers();
    }

    public function getAccountUnlockables($account_id){
        $accountRepository = new AccountRepository;
        return $accountRepository->getAccountUnlockables($account_id);
    }

    public function charge($amount, $customer_id, $description){
        $subscriptionRepository = new SubscriptionRepository;
        return $subscriptionRepository->charge($amount, $customer_id, $description);
    }

    public function getUnlockableMethodByName($name){
        $accountRepository = new AccountRepository;
        return $accountRepository->getUnlockableMethodByName($name);
    }
    public function storeAccountUnlockable(AccountUnlockable $accountUnlockable){
        $accountRepository = new AccountRepository;
        return $accountRepository->storeAccountUnlockable($accountUnlockable);
    }

    public function customerCharges($customerId){
        $userRepository = new UserRepository;
        return $userRepository->customerCharges($customerId);
    }

    public function retrieveChargeDetails($chargeId){
        $userRepository = new UserRepository;
        return $userRepository->retrieveChargeDetails($chargeId);
    }

    public function getCategoryByName($categoryName){
        $accountRepository = new AccountRepository;
        return $accountRepository->getCategoryByName($categoryName);
    }
    public function getPlanByCategoryAndAccountType($categoryId,$accountTypeId){
        $accountRepository = new AccountRepository;
        return $accountRepository->getPlanByCategoryAndAccountType($categoryId,$accountTypeId);
    }

    public function getAllFreePlans(){
        $accountRepository = new AccountRepository;
        return $accountRepository->getAllFreePlans();
    }

    public function getPlanUnlockable($planHistoryId, $unlockableId){
        $accountRepository = new AccountRepository;
        return $accountRepository->getPlanUnlockable($planHistoryId, $unlockableId);
    }

    public function getUserByIpAddress($ip_address){
        $guestRepository = new GuestRepository();
        return $guestRepository->getUserByIpAddress($ip_address);
    }

    public function storeGuestData(GuestData $guestData){
        $guestRepository = new GuestRepository();
        return $guestRepository->storeGuestData($guestData);
    }

    public function getAccountUnlockable($account_id, $unlockable_id){
        $userRepository = new UserRepository();
        return $userRepository->getAccountUnlockable($account_id, $unlockable_id);
    }

    public function getUserNotificationById($notification_id){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->getUserNotificationById($notification_id);
    }

    public function storeUserNotification(UserNotification $notification){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->storeUserNotification($notification);
    }

    public function deleteUserNotification(UserNotification $notification){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->deleteUserNotification($notification);
    }

    public function markNotificationAsRead(UserNotification $notification){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->markNotificationAsRead($notification);
    }


    public function storePushNotificationSub(PushNotification $notification){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->storePushNotificationSub($notification);
    }

    public function deletePushNotificationSub(PushNotification $notification){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->deletePushNotificationSub($notification);
    }

    public function getPushNotificationSub($user_id) {
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->getPushNotificationSub($user_id);
    }

    public function getPushNotificationSubByEndpoint($endpoint) {
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->getPushNotificationSubByEndpoint($endpoint);
    }

    public function getNotifications($user_id, $type = null){
        $notificationRepository = new NotificationRepository();
        return $notificationRepository->getNotifications($user_id, $type);
    }

    public function getColorPalette($colorPaletteId){
        $accountRepository = new AccountRepository;
        return $accountRepository->getColorPalette($colorPaletteId);
    }

    
    public function getSchoolAdmin($account_id){
        $accountRepository = new AccountRepository;
        return $accountRepository->getSchoolAdmin($account_id);
    }
}

