<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/24/2017
 * Time: 1:08 PM
 */

namespace App\Infrastructure\Accounts;


use Analogue;
use App\DomainModelLayer\Accounts\Role;
use Illuminate\Database\Eloquent\Model;

class LUTRoleRepository
{

    public function getRoleByName($roleName)
    {
        $roleMapper = Analogue::mapper(Role::class);
        $role = $roleMapper->query()->whereName($roleName)->first();
        return $role;
    }
}