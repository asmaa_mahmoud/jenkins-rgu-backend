<?php


namespace App\Infrastructure\Accounts;
use Analogue;
use App\DomainModelLayer\Accounts\Invitation;
use Carbon\Carbon;

class InvitationRepository
{
    public function storeInvitation(Invitation $Invitation)
    {
    	$invitationMapper= Analogue::mapper(Invitation::class);
        $invitationMapper->store($Invitation);
    }

    public function getInvitationByCode($code)
    {
    	$invitationMapper= Analogue::mapper(Invitation::class);
    	return $invitationMapper->query()->whereCode($code)->first();
    }
}