<?php

namespace App\Infrastructure\Accounts;

use Analogue;
use App\DomainModelLayer\Accounts\WorldMatch;
use App\DomainModelLayer\Accounts\WorldParameter;
use App\DomainModelLayer\Accounts\WorldUserDetails;
use App\DomainModelLayer\Accounts\WorldUserParameter;
use App\DomainModelLayer\Accounts\WorldUserPrediction;
use App\DomainModelLayer\Accounts\WorldUserScore;
use Carbon\Carbon;

class WorldCupRepository
{
    public function getMatchById($id){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->find($id);
    }

    public function getMatchesByRoundNo($roundNo){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('round_no', $roundNo)->get();
    }

    public function getMatchesByRoundNoFromDate($startDate, $roundNo){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('match_time', '>=', $startDate)->where('round_no', $roundNo)->get();
    }

    public function getParameterById($id){
        $worldParameterMapper = Analogue::mapper(WorldParameter::class);
        return $worldParameterMapper->query()->find($id);
    }

    public function getFeaturedMatchInDay($firstOfDay, $lastOfDay){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('featured', 1)
            ->where('match_time', '>=', $firstOfDay)->where('match_time', '<=', $lastOfDay)->orderBy('match_time')->first();
    }

    public function getMatchesInDay($firstOfDay, $lastOfDay){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('match_time', '>=', $firstOfDay)->where('match_time', '<=', $lastOfDay)->orderBy('match_time')->get();
    }

    public function getUsersPredictionInMatch($match_id, $winner, $count = false){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        $query = $worldUserPredictionMapper->query()->where('match_id', $match_id)->where('winner', $winner);
        if($count == true)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getNoSubmission($match_id = null){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        $query = $worldUserPredictionMapper->query();
        if($match_id != null)
            $query = $query->where('match_id', $match_id);

        return $query->sum('prediction_counter');
    }

    public function getWorldUsersScore($roundNo, $user_id = null, $limit = null, $count = false){
        $worldUserScoreMapper = Analogue::mapper(WorldUserScore::class);
        $query = $worldUserScoreMapper->query();
        if($user_id != null){
            $query = $query->where('user_id', $user_id);
        }
        if($roundNo == 1){
            $query = $query->whereNotNull('score_one')->orderBy('score_one', 'DESC');
        } else {
            $query = $query->whereNotNull('final_score')->orderBy('final_score', 'DESC');
        }
        $query = $query->orderBy('id', 'DESC');

        if($limit != null) {
            $query = $query->paginate($limit);
        }
        else if($count == true){
            $query = $query->count();
        }
        else{
            $query = $query->get();
        }
        return $query;
    }

    public function getUsersScore($roundNo){
        $worldUserScoreMapper = Analogue::mapper(WorldUserScore::class);
        $query = $worldUserScoreMapper->query();
        if($roundNo == 1){
            $query = $query->whereNotNull('score_one')->orderBy('score_one', 'DESC');
        } else{
            $query = $query->whereNotNull('final_score')->orderBy('final_score', 'DESC');
        }
        return $query->orderBy('id', 'DESC')->lists('user_id')->toArray();
    }

    public function getNextMatchDate(){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('match_time', '>=', Carbon::now('UTC')->startOfDay())
            ->orderBy('match_time')->first();
    }

    public function getAllParameters(){
        $worldParameterMapper = Analogue::mapper(WorldParameter::class);
        return $worldParameterMapper->query()->get();
    }

    public function getLastPredictionOfUser($user_id){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        return $worldUserPredictionMapper->query()->where('user_id', $user_id)->orderBy('updated_at', 'DESC')->first();
    }

    public function getUserPredictionInMatches($user_id, $match_ids){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        return $worldUserPredictionMapper->query()->where('user_id', $user_id)->whereIn('match_id', $match_ids)->get();
    }

    public function getUserParameters($user_id){
        $worldUserParameterMapper = Analogue::mapper(WorldUserParameter::class);
        return $worldUserParameterMapper->query()->where('user_id', $user_id)->get();
    }

    public function getWorldUserData($user_id){
        $worldUserDetailsMapper = Analogue::mapper(WorldUserDetails::class);
        return $worldUserDetailsMapper->query()->where('user_id', $user_id)->first();
    }

    public function storeWorldUserDetails(WorldUserDetails $worldUserDetails){
        $worldUserDetailsMapper = Analogue::mapper(WorldUserDetails::class);
        $worldUserDetailsMapper->store($worldUserDetails);
    }

    public function storeWorldUserScore(WorldUserScore $worldUserScore){
        $worldUserScoreMapper = Analogue::mapper(WorldUserScore::class);
        $worldUserScoreMapper->store($worldUserScore);
    }

    public function storeUserPrediction(WorldUserPrediction $worldUserPrediction){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        $worldUserPredictionMapper->store($worldUserPrediction);
    }

    public function getUnScoredPredictions(){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        return $worldUserPredictionMapper->query()->where('scored', 0)->get();
    }

    public function getTomorrowMatches($dateTime){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('match_time', '>', $dateTime)->orderBy('match_time')->get();
    }

    public function getMatchPrediction($user_id, $match_id){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        return $worldUserPredictionMapper->query()->where('user_id', $user_id)->where('match_id', $match_id)->first();
    }

    public function getLastMatchInDay($date){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->whereDate('match_time', '=', $date)->orderBy('match_time', 'DESC')->first();
    }

    public function countUserPredictInRound($user_id, $roundNo){
        $worldUserPredictionMapper = Analogue::mapper(WorldUserPrediction::class);
        return $worldUserPredictionMapper->query()->where('user_id', $user_id)->whereIn('match_id', function($q) use ($roundNo) {
            $q->select('id')->from('world_match')->where('round_no', $roundNo);
        })->count();
    }

    public function deleteUserParameters($user_id){
        $worldUserParameterMapper = Analogue::mapper(WorldUserParameter::class);
        $userParameter = $worldUserParameterMapper->query()->where('user_id', $user_id)->get();
        $worldUserParameterMapper->delete($userParameter);
    }

    public function storeUserParameter(WorldUserParameter $userParameter){
        $worldUserParameterMapper = Analogue::mapper(WorldUserParameter::class);
        $worldUserParameterMapper->store($userParameter);
    }

    public function getNextMatch(){
        $worldMatchMapper = Analogue::mapper(WorldMatch::class);
        return $worldMatchMapper->query()->where('match_time', '>=', Carbon::now('UTC'))->orderBy('match_time')->first();
    }

    public function getUserParameterWithName($user_id, $parameterName){
        $worldUserParameterMapper = Analogue::mapper(WorldUserParameter::class);
        return $worldUserParameterMapper->query()->where('user_id', $user_id)->where('parameter_id', function ($q) use ($parameterName) {
            $q->select('id')->from('parameters')->where('name', $parameterName);
        })->first();
    }

    public function getNoWorldUsers(){
        $worldUserDetailsMapper = Analogue::mapper(WorldUserDetails::class);
        return $worldUserDetailsMapper->count();
    }


}