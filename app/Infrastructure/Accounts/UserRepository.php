<?php

namespace App\Infrastructure\Accounts;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\InvoiceDetailsDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Extra;
use App\DomainModelLayer\Accounts\PasswordReset;
use DB;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use Carbon\Carbon;
use JWTFactory;
use JWTAuth;
use Illuminate\Support\Facades\Response;
use Analogue;
use App;
use Mail;
use \GuzzleHttp;
use \GuzzleHttp\Subscriber\Oauth\Oauth1;
use App\DomainModelLayer\Accounts\UserPosition;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;
use App\Helpers\PaymentModule;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use App\Helpers\HttpMethods;
use App\DomainModelLayer\Accounts\Language;
use App\DomainModelLayer\Accounts\GuestData;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class UserRepository extends Authenticatable implements JWTSubject
{
    protected $table = 'user';

    //region Properties
    private $customerEntityList = array();
    use SoftDeletes, Notifiable;
    protected $dates = ['deleted_at'];
    //endregion

    public $default_emails = ['info@robogarden.ca','support@robogarden.ca'];

    //region Interface Functions
    function FindById($id){
        //return   $this->customerEntityList[$id];

        //return   $this->customerEntityList[$id];
        $customer_object =  static::where('id',$id)->first();
        if($customer_object == null){
//                dd(new BadRequestException("Customer Not Found"));
            return $this->badRequest([trans('locale.customer_not_found')]);
            throw new BadRequestException(trans('locale.customer_not_found'));

        }
        $customer = new User();
        $customer->setFirstName($customer_object->fname);
        $customer->setLastName($customer_object->lname);
        $customer->setEmail($customer_object->email);
        $customer->setCountryId($customer_object->image_link);
        $customer->setId($customer_object->id);
        $customer->setGender($customer_object->gender);
        return $customer;

    }

    public function addUserScore(UserScore $userScore){
        $userScoreMapper= Analogue::mapper(UserScore::class);
        $userScoreMapper->store($userScore);
    }

    public function removeUserScore(User $user){
        $userScoreMapper= Analogue::mapper(UserScore::class);
        $userScore = $userScoreMapper->query()->where('user_id',$user->getId())->first();
        $userScoreMapper->delete($userScore);
    }


    public function AddUser(User $user){
        $userMapper= Analogue::mapper(User::class);
        $userMapper->store($user);
    }

    public function storeUser(User $user){
        $userMapper= Analogue::mapper(User::class);
        $userMapper->store($user);
    }

    public function addstudent(User $user)
    {
        $userMapper= Analogue::mapper(User::class);
        $userMapper->store($user);
    }

    public function getChildsBySupervisorEmail($supervisorEmail){
        $userMapper = Analogue::mapper(User::class);
        $childs = $userMapper->query()->where('supervisor_email', $supervisorEmail)->get();
        return $childs;
    }
    function RemoveCustomer($id){

//            $customer = $this->FindById($id);
//
//            if($customer == null){
//                return $this->internalError(['Customer deletion failed']);
//            }
//            else {
//                DB::beginTransaction();
//
//                if ($customer->delete()){
//                    DB::commit();
//                    $key = array_search($id, $this->customerEntityList);
//                    unset($this->customerEntityList[$key]);
//                }
//                else{
//                    DB::rollBack();
//                    return $this->internalError(['Customer deletion failed']);
//                }
//            }


    }

    function UpdateCustomer($id, UserDto $CustomerDto){
//            $customer = $this->FindById($id);
//            $customer->setFirstName($CustomerDto->FirstName);
//            $customer->setLastName($CustomerDto->LastName);
//            $customer->setEmail($CustomerDto->Email);
//            $customer->setCountryId($CustomerDto->CountryId);
    }
    //endregion

    //region Custom Functions
    function M_GetModelCustomers(){
        return static::all();
    }
    function M_GetModelCustomerById($id){
//            return Customer::where('Id', $id)->get();
    }
//    public function AlreadyRegistered(UserDto $userDto){
    public function AlreadyRegistered($userDto){
        $this->checkDefaultEmails($userDto->email);
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('email', $userDto->email)->first();
        return $user;
    }
    public function isAlreadyRegistered($userEmail){
        $this->checkDefaultEmails($userEmail);
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('email', $userEmail)->first();
        return $user;
    }

//    public function UserNameAlreadyExist(UserDto $userDto){
    public function UserNameAlreadyExist($userDto){
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('username', $userDto->username)->first();
        return $user;
    }

    public function isUserNameAlreadyExist($username){
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('username',$username)->first();
        return $user;
    }

    public function AlreadyRegisteredAnotherUser($email, User $user)
    {
        $this->checkDefaultEmails($email);
        $userMapper= Analogue::mapper(User::class);
        $old_email = $user->getEmail();
        $user = $userMapper->query()->where('email','!=',$old_email)->where('email', $email)->first();
        return $user;
    }

    public function UserNameAlreadyExistAnotherUser($username, User $user)
    {
        $userMapper= Analogue::mapper(User::class);
        $old_username = $user->getUsername();
        $user = $userMapper->query()->where('username','!=',$old_username)->where('username', $username)->first();
        return $user;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respond(array $data,array $headers = []){
        // $headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type";
        return Response::json($data,$this->statusCode);
    }

    public function unauthorized($data = ['Unauthorized Access']){
        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_UNAUTHORIZED)->respond($data);
    }

    function LoginUser($credentials,$gmt_difference)
    {
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('username', $credentials->username)->first();
        if($user == null)
        {
            $user = $userMapper->query()->where('social_id', $credentials->username)->where('social_track',$credentials->password)->first();
            if($user == null) throw new UnauthorizedException(trans('locale.wrong_credentials'));

        }
        if($user->first_login_time ==null)
        {
            $user->first_login_time=Carbon::now()->addHours($gmt_difference)->startOfDay()->timestamp;
            $this->storeUser($user);
        }

        $accountMapper = Analogue::mapper(Account::class);
        $login_credentials = ['username' => $credentials->username, 'password' => $credentials->password];
        try {
            //dd(static::where('username',$credentials->username)->first());
            if (! $token = JWTAuth::attempt($login_credentials)) {
                if($user->social_id != null && $user->social_track != null)
                {
                    if (!$token=JWTAuth::fromUser($user)) {
                        throw new UnauthorizedException(trans('locale.wrong_credentials'));
                    }
                }else{
                    throw new UnauthorizedException(trans('locale.wrong_credentials'));
                }
            }
        } catch (JWTException $e) {
            throw new BadRequestException("could_not_create_token");
        }
        //return Response::json($data,$this->statusCode,$headers);
        $status = $user->getAccount()->getStatus();
        if($status == 'suspended')
        {
            $userRole = $user->getRoles()->first()['name'];
            if($userRole == "parent" || $userRole == "user")
                throw new UnauthorizedException(trans('locale.account_not_activated_check_email'));
            else if($userRole == "child")
            {
                $usersInAccount = $user->getAccount()->getUsers();
                if($user->getAccount()->getStripeId() == "default")
                    throw new UnauthorizedException(trans('locale.account_is_suspended'));
                else if(sizeof($usersInAccount) == 1)
                    throw new UnauthorizedException(trans('locale.account_suspended_parent_confirm'));
                else
                    throw new UnauthorizedException(trans('locale.account_is_activated'));
            }
            else if($user->getAccount()->getAccountType()->getName() == 'School')
            {
                throw new UnauthorizedException(trans('locale.account_not_activated_verfication_code'));
            }
            else{
                throw new UnauthorizedException(trans('locale.account_not_activated_check_email'));
            }
        }
        // if no errors are encountered we can return a JWT
        return (compact('token'));
    }
    
    function GetUser($username)
    {
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('username', $username)->first();
        if($user == null)
        {
            $user = $userMapper->query()->where('social_id', $username)->first();
        }
        $user['role'] = $user->getRoles()->first()['name'];
        if($user['role'] !== 'super_admin'&& $user->getAccount()->getSchool()!==null){
            $user['languageCode'] = $user->getLanguageCode();
            $user['colorPalette'] = $user->getAccount()->getSchool()->getColorPalette();
            $user['schoolLogoUrl'] = $user->getAccount()->getSchool()->getSchoolLogoUrl();

        }
        return $user;
    }


    function GetUserEnrolledJourneys($userId)
    {
        $userJourneyMapper = Analogue::mapper(UserJourney::class);
        $userJourneys = $userJourneyMapper->query()->where('subscription_id',$userId)->get();
        return $userJourneys;
    }


    function getParentChildren($id)
    {
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('id', $id)->first();
        $accountId = $user->account_id;
        $children = $userMapper->query()->where('account_id',$accountId)->
        get(['first_name','last_name','profile_image_url','age','email','username'])->all();
        $results = array();
        $counter = 0;
        foreach($children as $child)
        {
            if($child->getRoles()->first()->name == 'child')
            {
                $results[$counter] = $child;
                $results[$counter]['XP'] = $child->getScores()['xp'];
                $results[$counter]['coins'] = $child->getScores()['coins'];

                $progress_count = 0;
                foreach($results[$counter]['progress'] as $progress)
                {
                    if(sizeof($progress['task']['missions'])>0)
                    {
                        $progress_count++;
                    }
                }
                $results[$counter]['child_progress'] = $progress_count;
                $counter++;
            }
        }

        return $results;
    }

    function validate_url($token_url)
    {
        return static::where('token_url',$token_url)->first();
    }

    function activate_user($token_url)
    {
        $user = static::where('token_url',$token_url)->first();
        if($user->is_activated == 1)
        {
            return;
        }
        else{
            $user->is_activated = 1;
            $user->save();
        }
    }
    //endregion

    public function getUserById($id)
    {

        $userMapper = Analogue::mapper(User::class);
        $user = $userMapper->query()->whereId($id)->first();

        return $user;
    }

    public function getUserByEmail($email)
    {
        $userMapper = Analogue::mapper(User::class);
        $user = $userMapper->query()->where('email',$email)->first();
        return $user;
    }

    public function getUserByUsername($username)
    {
        $userMapper = Analogue::mapper(User::class);
        $user = $userMapper->query()->where('username',$username)->first();
        return $user;
    }

    public function supervisorEmailExists($email)
    {
        $userMapper = Analogue::mapper(User::class);
        $user = $userMapper->query()->where('email',$email)->first();
        if($user != null)
            return true;
        return false;
    }

    public function saveProfileImage(User $user,$image_name,$image_data)
    {
        $user_id = $user->getId();

        $new_name = \Carbon\Carbon::now()->timestamp . "_" . $image_name;

        $s3 = APP::make('aws')->createClient('s3');
        $res = $s3->putObject(array(
            'Bucket' => 'robogarden-profile-image',
            'Key' => $new_name,
            'Body' => $image_data,
            'ACL' => 'public-read',
        ));

        return "https://robogarden-profile-image.s3.amazonaws.com/" . $new_name;
    }

    public function getFacebookProfileResponse($params)
    {
        $client = new \GuzzleHttp\Client();
        $accessTokenResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/oauth/access_token', [
            'query' => $params
        ]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);
        $fields = 'id,email,first_name,last_name,picture.type(large)';
        $profileResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/me', [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);
        $profile = json_decode($profileResponse->getBody(), true);
        $socialId = $profile['id'];
        $profile['picture'] = $profile['picture']['data']['url'];
        $profile['username'] = 'facebook'.strtolower($profile['first_name']).$profile['id'];
        $profile['password'] = $profile['id'];
        return $profile;
    }

    public function getGoogleProfileResponse($params)
    {
        $client = new \GuzzleHttp\Client();
        $accessTokenResponse = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', [
            'form_params' => $params
        ]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);


        // Step 2. Retrieve profile information about the current user.
        $profileResponse = $client->request('GET', 'https://www.googleapis.com/plus/v1/people/me/openIdConnect', [
            'headers' => array('Authorization' => 'Bearer ' . $accessToken['access_token'])
        ]);

        $profile = json_decode($profileResponse->getBody(), true);
        return $profile;
    }

    public function checkSocialUserRegistered($socialId,$provider)
    {
        $userMapper = Analogue::mapper(User::class);
        $user = $userMapper->query()->where('social_id',$socialId)->where('social_track',$provider)->first();
        if($user == null)return false;
        return true;
    }

    public function requestTwitterTokenOauth($consumerKey,$consumerSecret,$redirectUri)
    {
        $stack = \GuzzleHttp\HandlerStack::create();
        $requestTokenOauth = new Oauth1([
            'consumer_key' => $consumerKey,
            'consumer_secret' => $consumerSecret,
            'callback' => $redirectUri,
            'token' => '',
            'token_secret' => ''
        ]);
        $stack->push($requestTokenOauth);

        $client = new \GuzzleHttp\Client([
            'handler' => $stack
        ]);

        // Step 1. Obtain request token for the authorization popup.
        $requestTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/request_token', [
            'auth' => 'oauth'
        ]);
        $oauthToken = array();
        parse_str($requestTokenResponse->getBody(), $oauthToken);

        // Step 2. Send OAuth token back to open the authorization screen.
        return $oauthToken;
    }

    public function getTwitterAccessTokenOauth($consumerKey,$consumerSecret,$oauthToken,$oauthVerifier)
    {
        $stack = \GuzzleHttp\HandlerStack::create();
        $accessTokenOauth = new Oauth1([
            'consumer_key' => $consumerKey,
            'consumer_secret' => $consumerSecret,
            'token' => $oauthToken,
            'verifier' => $oauthVerifier,
            'token_secret' => ''
        ]);
        $stack->push($accessTokenOauth);

        $client = new \GuzzleHttp\Client([
            'handler' => $stack
        ]);
        // Step 3. Exchange oauth token and oauth verifier for access token.
        $accessTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/access_token', [
            'auth' => 'oauth'
        ]);

        $accessToken = array();
        parse_str($accessTokenResponse->getBody(), $accessToken);

        $profileOauth = new Oauth1([
            'consumer_key' => $consumerKey,
            'consumer_secret' => $consumerSecret,
            'oauth_token' => $accessToken['oauth_token'],
            'token_secret' => ''
        ]);
        $stack->push($profileOauth);
        $client = new \GuzzleHttp\Client([
            'handler' => $stack
        ]);

        // Step 4. Retrieve profile information about the current user.
        $profileResponse = $client->request('GET', 'https://api.twitter.com/1.1/users/show.json?screen_name=' . $accessToken['screen_name'], [
            'auth' => 'oauth'
        ]);
        $profile = json_decode($profileResponse->getBody(), true);

        return $profile;
    }

    public function getTokenfromUser($user_id)
    {
        $user = static::where('id',$user_id)->first();
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    public function storePosition(UserPosition $position)
    {
        $progressMapper= Analogue::mapper(UserPosition::class);
        DB::table('user_position')->insert(["created_at" => Carbon::now(),"updated_at"=>Carbon::now(),'user_id'=>$position->user->getId(),
            'journey_id'=> $position->getJourney()->getId(),'position_x' => $position->getPositionX() , 'position_y' => $position->getPositionY(), 'index' => $position->getIndex()]);
        //$progressMapper->store($position);
    }

    public function deletePosition(UserPosition $position)
    {
        $positionMapper= Analogue::mapper(UserPosition::class);
        $positionMapper->delete($position);
    }

    public function isRegisteredSupervisor($supervisorEmail){
        $userMapper= Analogue::mapper(User::class);
        $user = $userMapper->query()->where('email', $supervisorEmail)->first();
        if($user == null)
            return false;
        else{
            $userAccount = $user->getAccount();
            $userAccountType = $userAccount->getAccountType();
            if($userAccountType->name == "Family" || $userAccountType->name == "Individual")
                return $userAccount->name;
            else
                throw new BadRequestException("account type unrecognized");
        }
    }


    public function childConfirmEmailConfirmEmail(UserDto $childUserDto,$parentName,$parentAccountTypeName)
    {
        if($parentAccountTypeName == "Family")
            $subject = "Add Child";
        else if($parentAccountTypeName == "Individual")
            $subject = "Upgrade to family account and add child";
        else if($parentAccountTypeName == "Unregistered")
            $subject = "Register to robogarden and add child";

        // $payload = JWTFactory::sub(123)->aud('child')->child(['id' => $childUserDto->childId])->make();

        // $child_token = JWTAuth::encode($payload);

        $child_token = encrypt($childUserDto->childId);

        try{
            Mail::send('emails.childconfirm', [
                'username'=>$parentName,
                'childName'=>$childUserDto->fname.' '.$childUserDto->lname,
                'buttonText'=>$subject,
                'child_token'=>$child_token
            ], function ($m) use ($childUserDto,$subject) {
                $m->from('info@robogarden.ca','The RoboGarden Team');
                $m->to($childUserDto->supervisorEmail)->subject($subject);
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }

    public function sendPaymentFailedEmail(User $user,$trialNumber){
        try{
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            Mail::send('emails.paymentfailed', [
                'username' => $user_name,
                'trialNumber' => $trialNumber
            ], function ($m) use ($user) {
                $m->from('info@robogarden.ca','The RoboGarden Team');
                $m->to($user->getEmail())->subject(trans('locale.renewal_fail_subject'));
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }

    public function customerInfo($customerId){
        $paymentModule = new PaymentModule();
        return $paymentModule->getCustomerInfo($customerId);
    }

    public function customerInvoices($customerId){
        $paymentModule = new PaymentModule();
        return $paymentModule->retrieveCustomerInvoices($customerId);
    }

    public function retrieveInvoiceDetails($invoice_id){
        $paymentModule = new PaymentModule();
        return $paymentModule->retrieveInvoiceDetails($invoice_id);
    }

    public function requestParentEmail(User $user, $parent_email,$account_type, $parent_name)
    {
        //$payload =JWTFactory::sub(123)->aud('child')->child(['id' => $user->getId()])->make();

        //$child_token = JWTAuth::encode($payload);

        $child_token = encrypt($user->getId());
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());

        if($parent_name != '')
        {
            if($account_type == 'Family')
            {
                $subject = trans('locale.add_new_child_subject');
                $text = trans('locale.a_child')." ".($user_name).trans('locale.requested_to_be_added');
                $text2 = trans('locale.add_to_family_account');
                $buttonWidth = "200px";
            }
            else
            {
                $subject = trans('locale.add_new_child_subject');
                $text = trans('locale.a_child')." ".($user_name).trans('locale.requested_to_be_added');
                $text2 = trans('locale.upgrade_individual_to_family');
                $buttonWidth = "200px";
            }

        }
        else
        {
            $subject = trans('locale.subscribe_add_child');
            $text = trans('locale.a_child')." ".($user_name).trans('locale.requested_to_register');
            $text2 = trans('locale.start_subscription_process');
            $parent_name = ($user_name).'\'s '.trans('locale.parent');
            $buttonWidth = "300px";
        }

        try{
            Mail::send('emails.requestParent', [
                'parent_name'=>$parent_name,
                'buttonText'=>str_replace("RoboGarden: ","", $subject),
                'child_token'=>$child_token,
                'email_text' => $text,
                'email_text2' => $text2,
                'buttonWidth'=> $buttonWidth,
            ], function ($m) use ($parent_email,$subject) {
                $m->from('info@robogarden.ca','The RoboGarden Team');
                $m->to($parent_email)->subject($subject);
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }
    public function sendContactUsEmail(SupportEmailDto $supportEmailDto){
        //dd($supportEmailDto->message);
        //send to support the email
        try{
            Mail::send('emails.contactusemail', [
                'email'=>$supportEmailDto->email,
                'name'=>$supportEmailDto->name,
                'messagex'=>$supportEmailDto->message,
            ], function ($m) use ($supportEmailDto) {
                $m->from($supportEmailDto->email,$supportEmailDto->name);
                $m->to($supportEmailDto->supportEmail)->subject("Contact Us: ".($supportEmailDto->email));
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }

        //send auto reply
        try{
            Mail::send('emails.autocontactusreply', [
                'name'=>$supportEmailDto->name
            ], function ($m) use ($supportEmailDto) {
                $m->from($supportEmailDto->supportEmail,"The RoboGarden Team");
                $m->to($supportEmailDto->email)->subject(trans('locale.contact_us_confirm_subject'));
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }
    public function sendSupportEmail(User $user,SupportEmailDto $supportEmailDto){
        $emailUser=$supportEmailDto->email;
        $user_name= $supportEmailDto->name;

        $supportEmailDto->cc[0] = 'conted@ucalgary.ca';
        $supportEmailDto->cc[1] = 'lgsantam@ucalgary.ca';


        try{
             Mail::send('emails.supportemail', [
                'email' => $emailUser,
                'name' =>$user_name,
                'messagex' => $supportEmailDto->message,
                'info' => $supportEmailDto->info,
                'type' => $supportEmailDto->type,
                'courseNumber' => $supportEmailDto->courseNumber,
                'courseSection' => $supportEmailDto->courseSection
            ], function ($m) use ($supportEmailDto,$emailUser,$user_name) {
                $m->from($emailUser, $user_name);
                // $m->to($supportEmailDto->supportEmail)->subject("Support Requested: ".($emailUser));
                $m->to($supportEmailDto->supportEmail)->subject("UCALGARY requesting RoboGarden tech support");
                $m->cc($supportEmailDto->cc)->subject("UCALGARY requesting RoboGarden tech support");
               
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }

        try{
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            Mail::send('emails.autosupportreply', [
                'name' => $user_name
            ], function ($m) use ($supportEmailDto,$user,$emailUser) {
                $m->from($supportEmailDto->supportEmail,"The Professional RoboGarden Team");
                $m->to($emailUser)->subject(trans('locale.support_email_subject'));
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }

    public function getAllUsers(){
        $userMapper = Analogue::mapper(User::class);
        $user = $userMapper->query()->get();
        return $user;
    }

    public function createPasswordReset($email, $username){
        $passwordReset = new PasswordReset($email);
        $passwordResetMapper = Analogue::mapper(PasswordReset::class);
        $passwordResetMapper->store($passwordReset);
        $passwordResetDto = new PasswordResetDto();
        $passwordResetDto->email = $passwordReset->getEmail();
        $passwordResetDto->username = $username;
        $passwordResetDto->token = $passwordReset->getToken();
        return $passwordResetDto;
    }

    public function sendResetPasswordEmail(PasswordResetDto $passwordResetDto){
        try{
            Mail::send('emails.passwordreset', [
                "username" => $passwordResetDto->username,
                "email" => $passwordResetDto->email,
                "token" => $passwordResetDto->token
            ], function ($m) use ($passwordResetDto) {
                $m->from("info@robogarden.ca","The RoboGarden Team");
                $m->to($passwordResetDto->email)->subject(trans('locale.reset_password_subject'));
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }

    public function confirmResetPassword(PasswordResetDto $resetPasswordDto,$newPassword){
        $user = $this->getUserByEmail($resetPasswordDto->email);
        $passwordResetMapper = Analogue::mapper(PasswordReset::class);
        $passwordResetLatest = $passwordResetMapper->query()->where('email', $resetPasswordDto->email)->get()->last();
        $passwordResetWithToken = $passwordResetMapper->query()->where('email', $resetPasswordDto->email)->where('token',$resetPasswordDto->token)->first();
        if($passwordResetWithToken == null)
            throw new BadRequestException(trans('locale.invalid_token'));
        else if($passwordResetLatest->getToken() != $passwordResetWithToken->getToken())
            throw new BadRequestException(trans('locale.token_expired'));
        else if($passwordResetLatest->getUsed)
            throw new BadRequestException(trans('locale.token_used'));
        else
        {
            $hashedPassword = bcrypt($newPassword);
            $user->setPassword($hashedPassword);
            $this->storeUser($user);
        }
    }

    public function requestParentToSubscribe(User $user,User $parent,$name,$is_plan){
        try{
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $parent_name = (($parent->getFirstName() != null && !empty($parent->getFirstName())) ? $parent->getName() : $parent->getUsername());
            Mail::send('emails.remindparenttosubscribe', [
                "name" => $parent_name,
                "childName" => $user_name,
                "requestName"=>$name,
                "is_plan"=>$is_plan
            ], function ($m) use ($parent) {
                $m->from("info@robogarden.ca", "The RoboGarden Team");
                $m->to($parent->getEmail())->subject("RoboGarden Team: ". trans('locale.child_want_subscribe'));
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());

        }
    }

    public function resetChildsSupervisorEmail($supervisorEmail,$old_email){
        $userMapper= Analogue::mapper(User::class);
        $childs = $userMapper->query()->where('supervisor_email', $old_email)->get();
        foreach ($childs as $child) {
            $child->setSupervisorEmail($supervisorEmail);
            $userMapper->store($child);
        }
    }

    public function sendInvoiceEmail(InvoiceDetailsDto $invoiceDetails,User $user){
        $invoiceDetails->plan = '';
        foreach ($invoiceDetails->invoice_lines as $item) {
            if(strpos($item->plan_name, 'child') === false)
            {
                $invoiceDetails->plan = str_replace('_', ' ', $item->plan_name);
                break;
            }
        }

        try{
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            Mail::send('emails.invoice', [
                "name" => $user_name,
                "email" => $user->getEmail(),
                "invoiceDetails" => $invoiceDetails,
                "downloadLink" => url("user/customerinvoices/".$invoiceDetails->id."/download")
            ], function ($m) use ($user,$invoiceDetails) {
                $m->from("info@robogarden.ca", "The RoboGarden Team");
                $m->to($user->getEmail())->subject("RoboGarden Team: Invoice");
            });
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function requestCountryChange(User $user,$newCountry){
        $userEmail = $user->getEmail();
        if($userEmail == null)
            throw new BadRequestException("this user has no email");

        try{
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            Mail::send('emails.requestcountrychange', [
                "name" => $user_name,
                "email" => $user->getEmail(),
                "oldCountry" => $user->getAccount()->getCountry(),
                "newCountry" => $newCountry
            ], function ($m) use ($user,$user_name) {
                $m->from($user->getEmail(),$user_name);
                $m->to("info@robogarden.ca")->subject("Change Country Requested: ".$user->getEmail());
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        try{
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            Mail::send('emails.requestcountrychangeconfirm', [
                "name" => $user_name,
                "email" => $user->getEmail(),
                "oldCountry" => $user->getAccount()->getCountry(),
                "newCountry" => $newCountry
            ], function ($m) use ($user) {
                $m->from("info@robogarden.ca","The RoboGarden Team");
                $m->to($user->getEmail())->subject("RoboGarden: Change Country Request Confirmation");
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function beginDatabaseTransaction()
    {
        DB::beginTransaction();
    }

    public function commitDatabaseTransaction()
    {
        DB::commit();
    }

    public function rollBackDatabaseTransaction()
    {
        DB::rollBack();
    }

    public function getRbf($code)
    {
        $url = 'http://rbfconverter-3-env.us-west-2.elasticbeanstalk.com';
        $urlAsp = $url.'/rbf';
        $fields = array('smallBasicCode'=>$code);
        $response = HttpMethods::getWithParams($urlAsp,$fields);
        $status_code = $response['info']['http_code'];
        $body =  $response['body'];
        $data = json_decode($body , true);
        if($data['ErrorType'] == "NoError")
        {
            return array('Data'=>$data['Data'],"success" =>true);
        }
        else
        {
            return array('Data'=>$data,"success" =>false);
        }
    }

    public function isAuthorized($permission,User $user){
        $userRoles = $user->getRoles();
        foreach ($userRoles as $userRole) {
            $rolePermissions = $userRole->getPermissions();
            foreach($rolePermissions as $rolePermission){
                if($rolePermission->getName() == $permission)
                    return true;
            }
        }
        return false;
    }

    public function changeCountryForUser(User $user,$newCountry){
        $account = $user->getAccount();
        $account->setCountry($newCountry);
        return $account;

    }

    public function deleteUser(User $user){
        $userMapper= Analogue::mapper(User::class);
        $userMapper->delete($user);
    }

    public function getNumberOfUsersWithRole(Account $account,$roleName){
        $userMapper= Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id',$account->getId())->whereIn('id',function($queryx) use($roleName){
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')->whereIn('role_id',function($queryy) use($roleName){
                $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name',$roleName);
            });
        })->count();
        return $query;

    }

    public function checkDefaultEmails($email)
    {
        if(in_array($email,$this->default_emails))
            throw new BadRequestException(trans("locale.no_robogarden_emails"));
    }

    public function getLanguages()
    {
        $langMapper= Analogue::mapper(Language::class);
        return $langMapper->query()->where('is_active', 1)->get();
    }

    public function getLanguageById($language_id)
    {
        $langMapper= Analogue::mapper(Language::class);
        return $langMapper->query()->whereId($language_id)->where('is_active',1)->first();
    }

    public function reportBug($error){
        try{
            Mail::send('emails.reportbug', [
                "error"=>$error,
            ], function ($m) {
                $m->from("info@robogarden.ca","The RoboGarden Team");
                $m->to("robogarden.bugreport@gmail.com")->subject("RoboGarden: Bug Report");
            });

        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function UserSubscription($user_id){
        $userMapper = Analogue::mapper(User::class);
        return $userMapper->query()->whereRaw("md5(`id`) = '".$user_id."'")->first();
    }

    public function getUserModelAnswerUnlocked($user_id,$mission_id){
        $userModelAnswerUnlockedMapper = Analogue::mapper(ModelAnswerUnlocked::class);
        return $userModelAnswerUnlockedMapper->query()->where('user_id',$user_id)->where('mission_id',$mission_id)->first();
    }

    public function storeUserModelAnswerUnlocked(ModelAnswerUnlocked $modelAnswerUnlocked){
        $userModelAnswerUnlockedMapper = Analogue::mapper(ModelAnswerUnlocked::class);
        $userModelAnswerUnlockedMapper->store($modelAnswerUnlocked);
    }

    public function getUserModelAnswers($user_id,$mission_id){
        $userModelAnswerMapper = Analogue::mapper(UserModelAnswer::class);
        return $userModelAnswerMapper->query()->where('user_id',$user_id)->where('mission_id',$mission_id)->get();
    }

    public function storeUserModelAnswer(UserModelAnswer $userModelAnswer){
        $userModelAnswerMapper = Analogue::mapper(UserModelAnswer::class);
        $userModelAnswerMapper->store($userModelAnswer);
    }

    public function getExtrasNotInIds($extras_ids){
        $extraMapper = Analogue::mapper(Extra::class);
        return $extraMapper->query()->whereNotIn('id',$extras_ids)->get();
    }

    public function getUserScore($user_id){
        $userScoreMapper = Analogue::mapper(UserScore::class);
        return $userScoreMapper->query()->where('user_id', $user_id)->first();
    }

    public function storeUserScore(UserScore $userScore){
        $userScoreMapper = Analogue::mapper(UserScore::class);
        return $userScoreMapper->store($userScore);
    }

    public function customerCharges($customerId){
        $paymentModule = new PaymentModule();
        return $paymentModule->retrieveCustomerCharges($customerId);
    }

    public function retrieveChargeDetails($chargeId){
        $paymentModule = new PaymentModule();
        return $paymentModule->retrieveChargeDetails($chargeId);
    }

    public function getAccountUnlockable($account_id, $unlockable_id){
        $accountUnlockableMapper = Analogue::mapper(AccountUnlockable::class);
        return $accountUnlockableMapper->query()->where('account_id', $account_id)->where('unlockable_id', $unlockable_id)->first();

    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->id;
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
