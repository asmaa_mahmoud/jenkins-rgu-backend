<?php


namespace App\Infrastructure\Accounts;
use Analogue;
use App\DomainModelLayer\Accounts\Language;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\PushNotification;
use App\DomainModelLayer\Accounts\UserNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class NotificationRepository
{
    public function storeNotification(Notification $notification)
    {
    	$notificationMapper= Analogue::mapper(Notification::class);
        $notificationMapper->store($notification);
    }

    public function getNotificationById($id)
    {
    	$notificationMapper= Analogue::mapper(Notification::class);
    	return $notificationMapper->query()->whereId($id)->first();
    }

    public function getUserNotifications($user_id)
    {
        $notificationMapper= Analogue::mapper(Notification::class);
        return $notificationMapper->query()->where('user_id', $user_id)->where('starts_at', '<=', Carbon::now())
            ->where('ends_at', '>=', Carbon::now())->where('read', 0)->get();
    }

    public function getUserUnreadNotications($user_id){
        $notificationMapper= Analogue::mapper(Notification::class);
        return $notificationMapper->query()->where('starts_at','<',Carbon::now())->where('ends_at','>',Carbon::now())->where('read',0)->get();
    }

    public function translateNotification($title, $message){
        $translation = array();
        $real_lang = App::getLocale();
        $languageMapper = Analogue::mapper(Language::class);
        $languages = $languageMapper->query()->where('is_active', 1)->get();
        foreach ($languages as $language){
            App::setLocale($language->code);
            $translated_title = trans('locale.'.$title);
            if (is_array($message)) {
                $translated_message = "";
                foreach ($message as $msg) {
                    $translated_msg = trans('locale.'.$msg);
                    if ($translated_msg) {
                        $translated_message .= " " . $translated_msg;
                    } else {
                        $translated_message .= " " . $msg;
                    }
                }
            } else {
                $translated_message = trans('locale.'.$message);
            }
            array_push($translation, ['title' => $translated_title, 'message' => $translated_message, 'language' => $language->code]);
        }
        App::setLocale($real_lang);
        return $translation;
    }

    public function getUserNotificationById($notification_id){
        $notificationMapper= Analogue::mapper(UserNotification::class);
        $query = $notificationMapper->query()->whereId($notification_id)->first();
        return $query;
    }

    public function storeUserNotification(UserNotification $notification){
        $notificationMapper= Analogue::mapper(UserNotification::class);
        $notificationMapper->store($notification);
    }

    public function deleteUserNotification(UserNotification $notification){
        $notificationMapper= Analogue::mapper(UserNotification::class);
        $notificationMapper->delete($notification);
    }

    public function markNotificationAsRead(UserNotification $notification){
        $notificationMapper= Analogue::mapper(UserNotification::class);
        $notification->is_read = 1;
        $notificationMapper->store($notification);
    }

    public function storePushNotificationSub(PushNotification $notification){
        $notificationMapper= Analogue::mapper(PushNotification::class);
        $notificationMapper->store($notification);
    }

    public function deletePushNotificationSub(PushNotification $notification){
        $notificationMapper= Analogue::mapper(PushNotification::class);
        $notificationMapper->delete($notification);
    }

    public function getPushNotificationSub($user_id){
        $notificationMapper= Analogue::mapper(PushNotification::class);
        $query = $notificationMapper->query()->where('user_id',$user_id)->get();
        return $query;
    }

    public function getPushNotificationSubByEndpoint($endpoint){
        $notificationMapper = Analogue::mapper(PushNotification::class);
        $query = $notificationMapper->query()->where('endpoint', $endpoint)->first();
        return $query;
    }

    public function getNotifications($user_id, $type = null){
        $notificationMapper= Analogue::mapper(UserNotification::class);
        $query = $notificationMapper->query()->where('user_id', $user_id);

        if($type != null)
            $query = $query->where('type', $type);

        return $query->get();
    }
}