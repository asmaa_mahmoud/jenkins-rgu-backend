<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/24/2017
 * Time: 3:56 PM
 */

namespace App\Infrastructure\Accounts;


use App\DomainModelLayer\Accounts\AccountType;
use Analogue;

class AccountTypeRepository
{

    public function getAccountTypeById($id){
        $typeMapper = Analogue::mapper(AccountType::class);
        $type = $typeMapper->query()->whereId($id)->first();
        return $type;
    }

    public function getAccountTypeByName($name){
        $typeMapper = Analogue::mapper(AccountType::class);
        $type = $typeMapper->query()->whereName($name)->first();
        return $type;
    }

    public function getAllTypes(){
        $typeMapper = Analogue::mapper(AccountType::class);
        $types = $typeMapper->all();
        return $types;
    }
    public function getFamilyAccountType(){
        $typeMapper = Analogue::mapper(AccountType::class);
        $type = $typeMapper->query()->where('name',"Family")->first();
        return $type;
    }
    
}