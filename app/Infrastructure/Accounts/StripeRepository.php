<?php

namespace App\Infrastructure\Accounts;


use App\DomainModelLayer\Accounts\StripeEvent;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\DomainModelLayer\Accounts\Account;
use App\Framework\Exceptions\BadRequestException;
use Analogue;
use Mail;
use Carbon\Carbon;
use App\Helpers\PaymentModule;

class StripeRepository
{
	public function storeStripeEvent(StripeEvent $stripeEvent)
  {
    $stripeEventMapper= Analogue::mapper(StripeEvent::class);
    $stripeEventMapper->store($stripeEvent);
  }
}