<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 10:02 PM
 */

namespace App\Infrastructure\Repositories;


use Analogue;
use App\DomainModelLayer\UserJourneys\IUserJourneyRepository;
use App\DomainModelLayer\UserJourneys\UserJourney;

class UserJourneyRepository implements IUserJourneyRepository
{
    public function GetUserEnrolledJourneys($userId){
        $userJourneyMapper = Analogue::mapper(UserJourney::class);
        $userJourneys = $userJourneyMapper->query()->where('subscription_id',$userId)->get();
        return $userJourneys;
    }
}