<?php


namespace App\Infrastructure\Accounts;
use Analogue;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\PlanUnlockable;
use App\DomainModelLayer\Accounts\Status;
use App\DomainModelLayer\Accounts\UnlockableMethod;
use App\DomainModelLayer\Journeys\JourneyCategory;
use App\DomainModelLayer\Schools\Country;
use App\DomainModelLayer\Professional\ColorPalette;
use DB;
use App\Framework\Exceptions\BadRequestException;

class AccountRepository
{
    public function activate($token_url){
        $AccountMapper= Analogue::mapper(Account::class);
        $account = $AccountMapper->query()->where('token_url', $token_url)->first();

        $status = $this->getStatusByName('active');
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));

        if($account->getStatus() == 'active')
            throw new BadRequestException(trans('locale.account_already_activiated'));

        $account->setStatus($status);
        $AccountMapper->store($account);
        return $account;
    }

    public function validate_url($token_url)
    {
        $AccountMapper= Analogue::mapper(Account::class);
        return $AccountMapper->query()->where('token_url', $token_url)->first();
    }

    public function getAccountByUser($user)
    {
        $AccountMapper = Analogue::mapper(Account::class);
        return $AccountMapper->query()->where('id', $user->account_id)->first();
    }

    public function getAccountByAccountType($accountType){
        $AccountMapper = Analogue::mapper(Account::class);
        return $AccountMapper->query()->where('account_type_id', $accountType->id)->first();
    }

    public function getDefaultAccount(){
        $AccountMapper = Analogue::mapper(Account::class);
        $Account = $AccountMapper->query()->where('stripe_id','default')->first();
        return $Account;
    }

    public function updateToFamily(Account $account,$accountTypeId){
        $AccountMapper = Analogue::mapper(Account::class);
        $Account = $AccountMapper->query()->where('name','Family')->first();
        return $Account;
    }

    public function storeAccount(Account $account){
            $AccountMapper= Analogue::mapper(Account::class);
            $AccountMapper->store($account);
    }

    public function deleteAccount(Account $account)
    {
        $AccountMapper= Analogue::mapper(Account::class);
        $AccountMapper->delete($account);
    }

    public function getAccountByCustomerId($customerId){
        $AccountMapper = Analogue::mapper(Account::class);
        $Account = $AccountMapper->query()->where('stripe_id',$customerId)->first();
        return $Account;
    }
    public function getAccountById($accountId){
        $AccountMapper = Analogue::mapper(Account::class);
        $Account = $AccountMapper->query()->whereId($accountId)->first();
        return $Account;
    }

    public function getStatusByName($name)
    {
        $StatusMapper = Analogue::mapper(Status::class);
        $Status = $StatusMapper->query()->whereName($name)->first();
        return $Status;
    }

    public function getTax(Account $account){
        $AccountMapper = Analogue::mapper(Account::class);
        $Account = $AccountMapper->query()->where('id',$account->getId())->first();
        $country = $Account->getCountry();
        if($country == null)
            return 0;
        else if ("canada" == strtolower($country))
            return 0.05;
        else
            return 0;

    }

    public function getCountriesUsersCount(){
        $AllCountries = [];
        $AccountMapper = Analogue::mapper(Account::class);
        $countries = $AccountMapper->whereNotNull('country')->where('country', '!=', '')->distinct('country')->get();
        foreach ($countries as $country){
            $count = $AccountMapper->where('country', $country->country)->count();
            $AllCountries[strtolower($country->country)] = $count;
        }
//        $CountryMapper = Analogue::mapper(Country::class);
//        $codes = $CountryMapper->whereIn('country_name', $countries->pluck('country'))->get();
        return $AllCountries;
    }

    public function getAccountUnlockables($account_id){
        $AccountUnlockableMapper = Analogue::mapper(AccountUnlockable::class);
        return $AccountUnlockableMapper->where('account_id',$account_id)->get();
    }

    public function getUnlockableMethodByName($name){
        $unlockableMethodMapper = Analogue::mapper(UnlockableMethod::class);
        return $unlockableMethodMapper->where('name', $name)->first();
    }

    public function storeAccountUnlockable(AccountUnlockable $accountUnlockable){
        $accountUnlockableMapper= Analogue::mapper(AccountUnlockable::class);
        $accountUnlockableMapper->store($accountUnlockable);
    }

    public function getCategoryByName($categoryName){
        $categoryMapper = Analogue::mapper(JourneyCategory::class);
        return $categoryMapper->where('name',$categoryName)->first();
    }

    public function getPlanByCategoryAndAccountType($categoryId,$accountTypeId){
        $planHistoryMapper = Analogue::mapper(PlanHistory::class);
        return $planHistoryMapper->where('account_type_id',$accountTypeId)->whereIn('id',DB::table('plan_category')
            ->where('category_id',$categoryId)->pluck('plan_history_id')->toArray())->first();
    }

    public function getAllFreePlans(){
        $planHistoryMapper = Analogue::mapper(PlanHistory::class);
        return $planHistoryMapper->query()->where('new_name', 'Free')->get();
    }

    public function getPlanUnlockable($planHistoryId, $unlockableId){
        $planUnlockableMapper = Analogue::mapper(PlanUnlockable::class);
        return $planUnlockableMapper->where('plan_history_id', $planHistoryId)->where('unlockable_id', $unlockableId)->first();

    }

    public function getColorPalette($colorPaletteId){
        $colorPaletteMapper = Analogue::mapper(ColorPalette::class);
        return $colorPaletteMapper->where('id', $colorPaletteId)->first();

    }

    public function getSchoolAdmin($account_id){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id',$account_id)->whereIn('id',function ($q){
            $q->select('user_id')->from('user_role')->whereIn('role_id',function ($q2){
                $q2->select('id')->from('role')->where('name','school_admin');
            });
        });

        return $query->first();
    }


}