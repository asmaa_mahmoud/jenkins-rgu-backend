<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:11 AM
 */

namespace App\Infrastructure\HelpCenter;


use App\DomainModelLayer\HelpCenter\Article;
use App\DomainModelLayer\HelpCenter\Repositories\IHelpCenterMainRepository;
use App\DomainModelLayer\HelpCenter\Tag;
use App\DomainModelLayer\HelpCenter\Topic;

class HelpCenterMainRepository implements IHelpCenterMainRepository{

    private $topicRepository;
    private $articleRepository;
    private $tagRepository;

    public function __construct(){
        $this->topicRepository = new TopicRepository();
        $this->articleRepository = new ArticleRepository();
        $this->tagRepository = new TagRepository();
    }

//    =============== TOPICS ===============
    public function getAllTopics($limit,$search){
        return $this->topicRepository->getAllTopics($limit,$search);
    }

    public function getAllTopicsCount(){
        return $this->topicRepository->getAllTopicsCount();
    }

    public function getTopic($id){
        return $this->topicRepository->getTopic($id);
    }

    public function getTopicWithTitle($title){
        return $this->topicRepository->getTopicWithTitle($title);
    }

    public function getTopics($ids){
        return $this->topicRepository->getTopics($ids);
    }

    public function storeTopic(Topic $topic){
        return $this->topicRepository->storeTopic($topic);
    }

    public function deleteTopic(Topic $topic){
        return $this->topicRepository->deleteTopic($topic);
    }

    public function getTopicsCount($search){
        return $this->topicRepository->getTopicsCount($search);
    }

    public function uploadTopicIcon($imageData){
        return $this->topicRepository->uploadTopicIcon($imageData);
    }

    public function rearrangeTopics($topics){
        return $this->topicRepository->rearrangeTopics($topics);
    }
//  =============================================


//    =============== ARTICLES ===============
    public function getAllArticles($limit,$search,$count=false){
        return $this->articleRepository->getAllArticles($limit,$search,$count);
    }

    public function getArticle($id){
        return $this->articleRepository->getArticle($id);
    }

    public function getTopicArticle($topic_name){
        return $this->articleRepository->getTopicArticle($topic_name);
    }

    public function getArticles($ids, $limit){
        return $this->articleRepository->getArticles($ids, $limit);
    }

    public function storeArticle(Article $article){
        return $this->articleRepository->storeArticle($article);
    }

    public function deleteArticle(Article $article){
        return $this->articleRepository->deleteArticle($article);
    }

    public function getArticlesCount($search){
        return $this->articleRepository->getArticlesCount($search);
    }

    public function uploadArticleIcon($imageData){
        return $this->articleRepository->uploadArticleIcon($imageData);
    }
    public function getTopicArticles($topic_id){
        return $this->articleRepository->getTopicArticles($topic_id);

    }

    public function rearrangeArticles($articles,$topic_id){
        return $this->articleRepository->rearrangeArticles($articles,$topic_id);
    }

    public function setArticleOrder($article_id,$topic_id,$order){
        return $this->articleRepository->setArticleOrder($article_id,$topic_id,$order);
    }

//  =============================================


//    =============== TAGS ===============
    public function getAllTags($limit,$search){
        return $this->tagRepository->getAllTags($limit,$search);
    }

    public function getTag($id){
        return $this->tagRepository->getTag($id);
    }

    public function getTags($ids){
        return $this->tagRepository->getTags($ids);
    }

    public function storeTag(Tag $tag){
        return $this->tagRepository->storeTag($tag);
    }

    public function deleteTag(Tag $tag){
        return $this->tagRepository->deleteTag($tag);
    }

    public function getTagsCount($search){
        return $this->tagRepository->getTagsCount($search);
    }
//  =============================================
}