<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:04 AM
 */

namespace App\Infrastructure\HelpCenter;

use Analogue;
use App\DomainModelLayer\HelpCenter\Tag;

class TagRepository{
    public function getAllTags($limit=null,$search){
        $tagMapper = Analogue::mapper(Tag::class);
        $query = $tagMapper->query();

        if($search != null){
            $query = $query->where('name','like','%'.$search.'%');
        }

        if($limit !== null){
            $query = $query->paginate($limit);
        }else{
            $query = $query->get();
        }


        return $query;
    }

    public function getTag($id){
        $tagMapper = Analogue::mapper(Tag::class);
        $query = $tagMapper->query()->where('id', $id)->get();

        return $query;
    }

    public function getTags($ids){
        $tagMapper = Analogue::mapper(Tag::class);
        $query = $tagMapper->query()->whereIn('id', $ids)->get();

        return $query;
    }

    public function storeTag(Tag $tag){
        $tagMapper = Analogue::mapper(Tag::class);
        $tag = $tagMapper->store($tag);
        return $tag;
    }

    public function deleteTag(Tag $tag){
        $tagMapper = Analogue::mapper(Tag::class);
        return $tagMapper->delete($tag);
    }

    public function getTagsCount($search){
        $tagsMapper = Analogue::mapper(Tag::class);
        $query = $tagsMapper->query();

        if($search != null){
            $query = $query->where('name','like','%'.$search.'%');
        }

        $query = $query->count();

        return $query;
    }

}