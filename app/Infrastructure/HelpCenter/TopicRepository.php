<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:02 AM
 */

namespace App\Infrastructure\HelpCenter;

use Analogue;
use App;
use App\DomainModelLayer\HelpCenter\Topic;

class TopicRepository{
    public function getAllTopics($limit,$search){
        $topicMapper = Analogue::mapper(Topic::class);
        $query = $topicMapper->query()->orderBy('topic_order');

        if($search !=null){
            $query = $query->where('title','like','%'.$search.'%')->orderBy('topic_order');
        }

        if($limit !== null){
            $query = $query->paginate($limit);
        }else{
            $query = $query->get();
        }

        return $query;
    }

    public function getAllTopicsCount(){
        $topicMapper = Analogue::mapper(Topic::class);
        $query = $topicMapper->query()->get()->count();

        return $query;
    }

    public function getTopic($id){
        $topicMapper = Analogue::mapper(Topic::class);
        $query = $topicMapper->query()->where('id', $id)->get();

        return $query;
    }

    public function getTopicWithTitle($title){
        $topicMapper = Analogue::mapper(Topic::class);
        $query = $topicMapper->query()->where('title', $title)->first();

        return $query;
    }

    public function getTopics($ids){
        $topicMapper = Analogue::mapper(Topic::class);
        $query = $topicMapper->query()->whereIn('id', $ids)->get();

        return $query;
    }

    public function storeTopic(Topic $topic){
        $topicMapper = Analogue::mapper(Topic::class);
        $topic = $topicMapper->store($topic);
        return $topic;
    }

    /**
     * @param Topic $topic
     * @return mixed
     */
    public function deleteTopic(Topic $topic){
        $topicMapper = Analogue::mapper(Topic::class);
        return $topicMapper->delete($topic);
    }

    public function getTopicsCount($search){
        $topicMapper = Analogue::mapper(Topic::class);
        $query = $topicMapper->query();

        if($search !=null){
            $query = $query->where('title','like','%'.$search.'%');
        }

        $query = $query->count();

        return $query;
    }

    public function uploadTopicIcon($imageData){
        $image_name = 'topic.png';
        $new_name = \Carbon\Carbon::now()->timestamp . "_" . $image_name;

        $s3 = APP::make('aws')->createClient('s3');
        $res = $s3->putObject(array(
            'Bucket' => 'robogarden-professional',
            'Key' => 'help-center/topics/'.$new_name,
            'Body' => $imageData,
            'ACL' => 'public-read',
        ));

        return "https://robogarden-professional.s3.amazonaws.com/help-center/topics/" . $new_name;
    }

    public function rearrangeTopics($topics){
        $topicMapper = Analogue::mapper(Topic::class);
        foreach($topics as $order=>$topic){
            $topicInst = $topicMapper->query()->where('id',$topic)->first();
            
            if($topicInst->topic_order != $order+1){
                $topicInst->topic_order = $order+1;
                $topicMapper->store($topicInst);
            }
            
        }
        return 'Rearrange Success';
    }
}