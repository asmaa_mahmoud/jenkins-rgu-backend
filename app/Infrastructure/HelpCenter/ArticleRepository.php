<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:01 AM
 */

namespace App\Infrastructure\HelpCenter;

use Analogue;
use App;
use App\DomainModelLayer\HelpCenter\Article;
use App\DomainModelLayer\HelpCenter\ArticleTopic;

class ArticleRepository{
    private $articleMapper;

    public function __construct(){
        $this->articleMapper = Analogue::mapper(Article::class);
    }


    public function getAllArticles($limit,$search,$count=false){
//        $query = $this->articleMapper->query()->get();
        $query = $this->articleMapper->query();
        if($search != null){

            $query = $query->where('title', 'like', '%'.$search.'%')
           
                ->orWhereHas('topics', function($query) use ($search){
                    return $query->where('title', 'like', '%'.$search.'%');
                })
                ->orWhereHas('tags', function ($query) use($search){
                    return $query->where('name', $search);
                })
                ->orWhere(function($q) use ($search){
                    $q->whereRaw("(fnStripTags(summary)COLLATE utf8_general_ci) like ? ", '%' . $search . '%');
                });
        }

        if($limit !== null)
            $query = $query->paginate($limit);
        else
            $query = $query->get();

        if($count ==true)
         $query= $query->count();
        else
            return $query;
    }


    public function getTopicArticle($topic_name){
        $query = $this->articleMapper->query();

        $query = $query->WhereHas('topics', function($query) use ($topic_name){
                return $query->where('title', 'like', '%'.$topic_name.'%');
            });


        $query = $query->inRandomOrder()->first();

        return $query;
    }


     /* public function searchForCountArticle($search){
        $articlesMapper = Analogue::mapper(Article::class);
        $query = $articlesMapper->query();
        
            if($search != null){
            $query = $query->where('title', 'like', '%'.$search.'%')
                ->orWhereHas('topics', function($query) use ($search){
                    return $query->where('title', 'like', '%'.$search.'%');
                })
                ->orWhereHas('tags', function ($query) use($search){
                    return $query->where('name', $search);
                })
                ->orWhere(function($q) use ($search){
                    $q->whereRaw("(fnStripTags(summary)COLLATE utf8_general_ci) like ? ", '%' . $search . '%');
                });
        }

        $query = $query->count(); */

        //return $query;
   // }

    public function getArticle($id){
        $query = $this->articleMapper->query()->where('id', $id)->get();

        return $query;
    }


    public function getArticles($ids, $limit){
        $query = $this->articleMapper->query()->orderBy('article_order')->whereIn('id', $ids);

        if($limit !== null){
            $query = $query->paginate($limit);
        }else{
            $query = $query->get();
        }

        return $query;
    }

    public function storeArticle(Article $article){
        $articleMapper = Analogue::mapper(Article::class);
        $article = $articleMapper->store($article);
        return $article;
    }

    public function deleteArticle(Article $article){
        $topicMapper = Analogue::mapper(Article::class);
        return $topicMapper->delete($article);
    }

    public function getArticlesCount($search){
        $articlesMapper = Analogue::mapper(Article::class);
        $query = $articlesMapper->query();
        
        if($search != null){
            $query = $query->where('title','like','%'.$search.'%');
        }

        $query = $query->count();

        return $query;
    }

    public function uploadArticleIcon($imageData){
        $image_name = 'article.png';
        $new_name = \Carbon\Carbon::now()->timestamp . "_" . $image_name;

        $s3 = APP::make('aws')->createClient('s3');
        $res = $s3->putObject(array(
            'Bucket' => 'robogarden-professional',
            'Key' => 'help-center/articles/'.$new_name,
            'Body' => $imageData,
            'ACL' => 'public-read',
        ));

        return "https://robogarden-professional.s3.amazonaws.com/help-center/articles/" . $new_name;
    }

    public function getTopicArticles($topic_id){
        $articlesMapper = Analogue::mapper(Article::class);

            $query = $articlesMapper->query()
            ->join('help_center_article_topic','help_center_article_topic.article_id','=','help_center_article.id')
            ->orderBy('help_center_article_topic.article_order')
            ->where('topic_id',$topic_id)
            ->lists('help_center_article.title','help_center_article.id');
            
        return $query;
    }

    public function rearrangeArticles($articles,$topic_id){
        $articleTopicMapper = Analogue::mapper(ArticleTopic::class);
        
        foreach($articles as $order=>$article){
            $articleTopicInst = $articleTopicMapper->query()
            ->where('topic_id',$topic_id)
            ->where('article_id',$article)
            ->first();
            
            if($articleTopicInst->article_order != $order+1){
                $articleTopicInst->article_order = $order+1;
                $articleTopicMapper->store($articleTopicInst);
            }
            
        }
        return 'Rearrange Success';
    }

    public function setArticleOrder($article_id,$topic_id,$order){
        $articleTopicMapper = Analogue::mapper(ArticleTopic::class);
        $articleTopic = $articleTopicMapper->query()
        ->where('article_id',$article_id)
        ->where('topic_id',$topic_id)
        ->first();
        $articleTopic->article_order = $order;

        $articleTopicMapper->store($articleTopic);

    }
}