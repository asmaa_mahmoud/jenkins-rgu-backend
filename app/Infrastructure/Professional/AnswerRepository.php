<?php

namespace App\Infrastructure\Professional;
use Analogue;
use App\DomainModelLayer\Professional\AnalysisAnswer;

use DB;

class AnswerRepository{

	public function getAllAnswers(){
		//return "getAllAnswers from AnswerRepository";

        $answerMapper = Analogue::mapper(AnalysisAnswer::class);
        //dd($answerMapper);
        return $answerMapper->globalQuery()->whereNull('deleted_at')->get();

	}

	public function getAnswer($answer_id){

        $answerMapper = Analogue::mapper(AnalysisAnswer::class);
        return $answerMapper->globalQuery()->whereNull('deleted_at')->whereId($answer_id)->first();


	}



}

