<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/10/2018
 * Time: 11:27 AM
 */

namespace App\Infrastructure\Professional;


use Analogue;
use App\DomainModelLayer\Professional\AnalysisQA;
use App\DomainModelLayer\Professional\Submission;
use DB;

class SubmissionRepository
{




    public function submit(Submission $submission){
        $submissionMapper = Analogue::mapper(Submission::class);
        $submission=$submissionMapper->store($submission);
        return $submission;
    }

    public function updateSubmissionResult($submission_id,$result){
        $submissionMapper = Analogue::mapper(Submission::class);
        $submission=$submissionMapper->query()->whereId($submission_id)->first();
        $submission->setFinalResult($result);
        $submissionMapper->store($submission);
    }

    public function insertUserQA(array $analysisQAArray){
        $analysisQAMapper = Analogue::mapper(AnalysisQA::class);
        $analysisQAMapper->store($analysisQAArray);
    }

    public function getUserSubmission($user_id){
        $submissionMapper = Analogue::mapper(Submission::class);
        $query = $submissionMapper->query()->where('user_id', $user_id)->first();
        return $query;
    }

    public function getSectionSubmissions($user_id, $categories){
        $submissionMapper = Analogue::mapper(Submission::class);
        $query = $submissionMapper->query()->where('user_id', $user_id)->whereIn('category_code', $categories)->get();
        return $query;
    }

    public function getCategoryUserSubmission($user_id, $category_code){
        $submissionMapper = Analogue::mapper(Submission::class);
        $query = $submissionMapper->query()->where('user_id', $user_id)->where('category_code', $category_code)->first();
        return $query;
    }

    public function deleteUserSubmissions($user_id,$category_code){
         DB::table('submissions')->where('user_id', $user_id)->where('category_code',$category_code)->delete();
    }

    public function deleteCategoryPostSubmissions($user_id,$postCategories){

        return DB::table('submissions')->where('user_id', $user_id)->whereIn('category_code',$postCategories)->delete();

    }

}