<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/17/2018
 * Time: 2:09 PM
 */

namespace App\Infrastructure\Professional;


use Analogue;
use App\DomainModelLayer\Professional\AnalysisCategory;
use App\DomainModelLayer\Professional\AnalysisQuestion;
use App\DomainModelLayer\Professional\AnalysisUpCategory;
use DB;

class RoboscaleRepository
{

    public  function getCategoryQuestions($category_id){
        $categoryMapper = Analogue::mapper(AnalysisQuestion::class);
        //dd($answerMapper);
        return $categoryMapper->globalQuery()->whereNull('deleted_at')->where('analysis_category_id',$category_id)->get();
    }
    public  function getCategoryById($category_id){
        $categoryMapper = Analogue::mapper(AnalysisCategory::class);
        //dd($answerMapper);

        return $categoryMapper->query()->where('id',$category_id)->first();

    }
    public  function getAllCategories(){
        $upCategoryMapper = Analogue::mapper(AnalysisUpCategory::class);
        $query = $upCategoryMapper->get();
        return $query;
    }
    public  function getPostCategories(AnalysisCategory $category){
        $categoryMapper = Analogue::mapper(AnalysisCategory::class);
        $query = $categoryMapper->where(function ($q)use ($category){
            $q->where('up_category_id',$category->up_category_id)
            ->where('order_in_up_category','>=',$category->order_in_up_category);})
            ->orWhere(function ($qq)use($category){
                $qq->where('up_category_id','>',$category->up_category_id);
            })
            ->get();
        return $query;
    }



}