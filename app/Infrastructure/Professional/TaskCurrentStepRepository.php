<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 09-Apr-19
 * Time: 3:09 PM
 */

namespace App\Infrastructure\Professional;

use Analogue;
use App\DomainModelLayer\Professional\TaskCurrentStep;

class TaskCurrentStepRepository{

    public function getUserTaskCurrentStep($user_id, $task_id, $activity_id, $campus_id, $is_default = true){
        $currentStepMapped = Analogue::Mapper(TaskCurrentStep::class);
        $query = $currentStepMapped->query()->where('user_id', $user_id)
                                            ->where('task_id', $task_id)
                                            ->where('campus_id', $campus_id);

        if($is_default){
            $query = $query->where('default_activity_id', $activity_id);
        }else{
            $query = $query->where('activity_id', $activity_id);
        }

        $query = $query->first();

        return $query;
    }

    public function getUserActivityTasksCurrentSteps($user_id, $activity_id, $campus_id, $is_default = true){
        $currentStepMapped = Analogue::Mapper(TaskCurrentStep::class);
        $query = $currentStepMapped->query()->where('user_id', $user_id)
                                            ->where('campus_id', $campus_id);

        if($is_default){
            $query = $query->where('default_activity_id', $activity_id);
        }else{
            $query = $query->where('activity_id', $activity_id);
        }

        $query = $query->get();

        return $query;
    }

    public function storeUserTaskCurrentStep(TaskCurrentStep $currentStep){
        $currentStepMapped = Analogue::Mapper(TaskCurrentStep::class);
        $currentStep = $currentStepMapped->store($currentStep);

        return $currentStep;
    }



}