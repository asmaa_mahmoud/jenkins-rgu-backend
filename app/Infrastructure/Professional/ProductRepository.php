<?php

namespace App\Infrastructure\Professional;
use Analogue;
use App\DomainModelLayer\Professional\Product;

class ProductRepository{

	public function getProductAssets($product_url){

        $productMapper = Analogue::mapper(Product::class);
        return $productMapper->query()->where('url', $product_url)->first();
	}

}

