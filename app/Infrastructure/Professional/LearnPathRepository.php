<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/12/2018
 * Time: 11:00 AM
 */

namespace App\Infrastructure\Professional;


use ActivityProgress;
use Analogue;
use App\DomainModelLayer\Journeys\ActivityTask;
use App\DomainModelLayer\Journeys\Blockly;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Professional\LearningPathActivity;
use App\DomainModelLayer\Professional\LearningPathActivityProgress;
use App\DomainModelLayer\Professional\LearningPathUser;
use DB;

class LearnPathRepository
{
    public function getStudentLearningPath($student_id){

        $learnPathMapper = Analogue::mapper(LearningPath::class);
        $query = $learnPathMapper->query()->whereId(function ($qu) use ($student_id) {
            $qu->select('learning_path_id')->from('learning_path_user')->where('user_id', $student_id);
        })->first();

        return $query;
    }

    public function getStudentLearningPaths($student_id){

        $learnPathMapper = Analogue::mapper(LearningPath::class);
        $query = $learnPathMapper->query()->whereIn('id',function ($qu) use ($student_id) {
            $qu->select('learning_path_id')->from('learning_path_user')->where('user_id', $student_id);
        })->get();

        return $query;
    }

    public function getLearningPathsWithIds($ids){

        $learnPathMapper = Analogue::mapper(LearningPath::class);
        $query = $learnPathMapper->query()->whereIn('id',$ids)->get();

        return $query;
    }

    public function getLearnPathStages($learning_path_id, $is_default){
        if($is_default)
            $activity_id = 'default_activity_id';
        else
            $activity_id = 'activity_id';
        $query = DB::table('learning_path_activity')->select(DB::raw("Count(DISTINCT($activity_id)) as activity_count, stage_number, stage_name, stage_image_url"))
            ->where('learning_path_id', $learning_path_id)
            ->groupBy('stage_number','stage_name', 'stage_image_url')->get();

        return $query;
    }

    public function getStageActivities($user_id,$stage_number,$learning_path_id, $is_default){
        $query=DB::table('learning_path_activity_progress')->where('user_id',$user_id)->where('learning_path_activity.stage_number',$stage_number)
            ->where('learning_path_activity_progress.learning_path_id',$learning_path_id)->leftJoin('learning_path_activity','learning_path_activity_progress.default_activity_id','=','learning_path_activity.default_activity_id')
            ->select('learning_path_activity.default_activity_id' , 'learning_path_activity.order')->get();

        return $query;
    }

    public function getStageActivitiesIds($stage_number,$learning_path_id, $is_default){
        if($is_default)
            $activity_id = 'default_activity_id';
        else
            $activity_id = 'activity_id';

        $query=DB::table('learning_path_activity')->where('learning_path_id',$learning_path_id)->where('learning_path_activity.stage_number',$stage_number)
            ->select('learning_path_activity.default_activity_id')->get()->pluck($activity_id);

        return $query;
    }

    public function getLearningPathById($id){
        $learningPathMapper = Analogue::mapper(LearningPath::class);
        return $learningPathMapper->query()->whereId($id)->first();
    }

    public function getLearningPathByName($name){
        $learningPathMapper = Analogue::mapper(LearningPath::class);
        return $learningPathMapper->query()->where('name',$name)->first();
    }

    public function getLearningPathActivity($learning_path_id, $activity_id, $is_default){
        $learningPathActivityMapper = Analogue::mapper(LearningPathActivity::class);
        if($is_default)
            return $learningPathActivityMapper->query()->where('learning_path_id', $learning_path_id)->where('default_activity_id', $activity_id)->first();
        else
            return $learningPathActivityMapper->query()->where('learning_path_id', $learning_path_id)->where('activity_id', $activity_id)->first();
    }

    public function checkIfStudentInLearningPath($learning_path_id, $student_id){
        $learningPathUserMapper = Analogue::mapper(LearningPathUser::class);
        $learningPathUser = $learningPathUserMapper->query()->where('learning_path_id', $learning_path_id)->where('user_id', $student_id)->first();

        if($learningPathUser != null)
            return true;

        return false;
    }

    public function getActivityInStage($learning_path_id, $stage_number, $activity_id, $is_default){
        $learningPathActivityMapper = Analogue::mapper(LearningPathActivity::class);

        if($is_default)
            return $learningPathActivityMapper->query()->where('learning_path_id', $learning_path_id)->where('stage_number', $stage_number)->where('default_activity_id', $activity_id)->first();
        else
            return $learningPathActivityMapper->query()->where('learning_path_id', $learning_path_id)->where('stage_number', $stage_number)->where('activity_id', $activity_id)->first();
    }

    public function checkIfActivityInLearningPath($learning_path_id, $activity_id, $is_default=true){

        $learningPathActivityMapper = Analogue::mapper(LearningPathActivity::class);
        $learningPathActivity = $learningPathActivityMapper->query()->where('learning_path_id', $learning_path_id);
       // ->where('default_activity_id', $activity_id)->first();
        if($is_default)
            $learningPathActivity= $learningPathActivity->where('default_activity_id', $activity_id)->first();
        else
            $learningPathActivity=$learningPathActivity->where('activity_id', $activity_id)->first();



        if($learningPathActivity != null)
            return true;

        return false;
    }

    public function getUserTaskProgressActivity(Task $task, $user_id, $activity_id, $is_default = true){

        $query=DB::table('activity_progress')->where('user_id',$user_id)->where('task_id',$task->getId());

        if($is_default)
            $taskProgress = $query->where('default_activity_id', $activity_id)->first();
        else
            $taskProgress = $query->where('activity_id', $activity_id)->first();
        return $taskProgress;
    }

    public function getHtmlBlocklyCategories(){
        $blocklyMapper = Analogue::mapper(Blockly::class);
        $blockly = $blocklyMapper->query()->where('name', 'Web')->orWhere('name', 'Logic')
            ->orWhere('name', 'Loops')->orWhere('name', 'Math')->orWhere('name', 'Text')
            ->orWhere('name', 'List')->orWhere('name', 'Colour')->orWhere('name', 'Variables')
            ->orWhere('name', 'Functions')->orWhere('name', 'Values')->orWhere('name', 'Function')
            ->orderBy('order','asc')->get();
        return $blockly;
    }

    public function getUserTaskProgressActivityLearningPath(Task $task, $user_id, $learning_path_id, $activity_id, $is_default = true){
        $progressMapper = Analogue::mapper(LearningPathActivityProgress::class);
        $taskProgress = $progressMapper->query()->where('learning_path_id', $learning_path_id)->where('task_id', $task->getId())->where('user_id', $user_id);
        if($is_default)
            $taskProgress = $taskProgress->where('default_activity_id', $activity_id)->first();
        else
            $taskProgress = $taskProgress->where('activity_id', $activity_id)->first();
        return $taskProgress;
    }

    public function storeLearningPathActivityProgress(LearningPathActivityProgress $learningPathActivityProgress){
        $ActivityProgressMapper = Analogue::mapper(LearningPathActivityProgress::class);
        $ActivityProgressMapper->store($learningPathActivityProgress);
    }

    public function deleteLearningPathActivityProgress(LearningPathActivityProgress $learningPathActivityProgress){
        $ActivityProgressMapper = Analogue::mapper(LearningPathActivityProgress::class);
        $ActivityProgressMapper->delete($learningPathActivityProgress);
    }

    public function getUserTaskProgress($user_id, $learning_path_id, $task_id){
        $ActivityProgressMapper = Analogue::mapper(LearningPathActivityProgress::class);
        $taskProgress = $ActivityProgressMapper->query()->where('user_id', $user_id)->where('learning_path_id', $learning_path_id)
            ->where('task_id', $task_id)->first();
        return $taskProgress;
    }

    public function getSolvedTasksInActivity($user_id,$learning_path_id,$activity_id,$count = null,$is_default= true){
        $taskProgressActivityMapper = Analogue::mapper(LearningPathActivityProgress::class);
        if($is_default)
            $taskProgressActivity = $taskProgressActivityMapper->where('default_activity_id',$activity_id);
        else
            $taskProgressActivity = $taskProgressActivityMapper->where('activity_id',$activity_id);

        $taskProgressActivities = $taskProgressActivity->where('user_id',$user_id)->where('learning_path_id', $learning_path_id)
            ->whereNotNull('first_success')->get();
        $task_ids = [];
        foreach ($taskProgressActivities as $taskProgressActivityOne){
            array_push($task_ids,$taskProgressActivityOne->getTask()->getId());
        }
        //dd($task_ids);

        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        if($is_default)
            $activityTasks = $activityTaskMapper->where('default_activity_id',$activity_id);
        else
            $activityTasks = $activityTaskMapper->where('activity_id',$activity_id);

        $activityTasks = $activityTasks->whereIn('task_id',$task_ids)->orderBy('order','asc');
        //return($activityTasks);

        if($count != null)
            $activityTasks = $activityTasks->count();
            //$activityTasks =count( $activityTasks);

        else
            $activityTasks = $activityTasks->get();
        //dd($activityTasks);
        return $activityTasks;
    }

    public function getAllTasksInActivity($activity_id, $count = null, $is_default = true){
        $taskMapper = Analogue::mapper(Task::class);
        if($is_default)
            $activityTask = $taskMapper->query()->whereIn('id', function ($q) use ($activity_id){
                $q->select('task_id')->from('activity_task')->where('default_activity_id', $activity_id);
            });
        else
            $activityTask = $taskMapper->query()->whereIn('id', function ($q) use ($activity_id){
                $q->select('task_id')->from('activity_task')->where('activity_id', $activity_id);
            });

        if($count != null)
            $activityTask = $activityTask->count();

        else
            $activityTask = $activityTask->get();

        return $activityTask;
    }

    public function getAllTasksInStage($learning_path_id, $stage_number, $count=null, $is_default=true){
        $activityIds = $this->getStageActivitiesIds($stage_number, $learning_path_id, $is_default);

        $taskMapper = Analogue::mapper(Task::class);

        if($is_default)
            $stageTasks = $taskMapper->query()->whereIn('id', function ($q) use ($activityIds){
                $q->select('task_id')->from('activity_task')->whereIn('default_activity_id', $activityIds);
            });
        else
            $stageTasks = $taskMapper->query()->whereIn('id', function ($q) use ($activityIds){
                $q->select('task_id')->from('activity_task')->whereIn('activity_id', $activityIds);
            });

        if($count != null)
            $stageTasks = $stageTasks->count();
        else
            $stageTasks = $stageTasks->get();

        return $stageTasks;
    }

    public function storeLearningPathUser(LearningPathUser $learningPathUser){
        $learningPathUserMapper = Analogue::mapper(LearningPathUser::class);
        $learningPathUserMapper->store($learningPathUser);
    }

    public function deleteLearningPathUser($user_id){
        $learningPathUserMapper = Analogue::mapper(LearningPathUser::class);
        $pathUser = $learningPathUserMapper->query()->where('user_id', $user_id)->get();
        $learningPathUserMapper->delete($pathUser);
    }
}