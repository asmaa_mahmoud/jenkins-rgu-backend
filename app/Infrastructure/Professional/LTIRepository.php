<?php

namespace App\Infrastructure\Professional;
use Analogue;


use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\LTIConsumer;

class LTIRepository{


	public function getLTIConsumerById($consumer_id){
        $ltiConsumerMapper = Analogue::mapper(LTIConsumer::class);

        return $ltiConsumerMapper->query()->where('consumer_pk',$consumer_id)->first();

    }
    public function getLTIConsumerByKey($consumer_key){
        $ltiConsumerMapper = Analogue::mapper(LTIConsumer::class);

        return $ltiConsumerMapper->query()->where('consumer_key256',$consumer_key)->first();

    }

    public function getLTIConsumerByAccountId ($account_id){
        $ltiConsumerMapper = Analogue::mapper(LTIConsumer::class);

        return $ltiConsumerMapper->query()->where('account_id',$account_id)->first();

    }
    public function getUserByLtiId($user_lti_id){
	    $userMapper=Analogue::mapper(User::class);
	    return $userMapper->query()->where('user_lti_id',$user_lti_id)->first();
    }

    public function getRoundByLtiId($round_lti_id){
	    $userMapper=Analogue::mapper(CampusRound::class);
	    return $userMapper->query()->where('round_lti_id',$round_lti_id)->first();
    }

    public function checkIfSchoolAccountHasAdmin($account_id){
	    $userMapper=Analogue::mapper(User::class);
	    return $userMapper->query()->where('account_id',$account_id)->whereIn('id',function ($q){

	        $q->select('user_id')->from('user_role')->whereIn('role_id',function ($qq){
	            $qq->select('id')->from('role')->whereNull('deleted_at')->where('name', 'school_admin');
            });
        })->first();
    }


}

