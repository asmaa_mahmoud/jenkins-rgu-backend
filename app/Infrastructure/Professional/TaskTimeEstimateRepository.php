<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 09-Apr-19
 * Time: 11:40 AM
 */

namespace App\Infrastructure\Professional;

use Analogue;
use App\DomainModelLayer\Professional\TaskTimeEstimate;

class TaskTimeEstimateRepository{

    public function getTaskTypeTimeEstimate($type){
        $timeEstimateMapper = Analogue::Mapper(TaskTimeEstimate::class);
//        $query = $timeEstimateMapper->query()->where('name', $type)->first()->time;
        $query = $timeEstimateMapper->query()->where('name', $type)->first();
        if($query == null){
            $query = 0;
        }else{
            $query = $query->time;
        }

        return $query;
    }
}