<?php

namespace App\Infrastructure\Professional;
use Analogue;

use App\DomainModelLayer\Professional\AnalysisCategory;
use App\DomainModelLayer\Professional\AnalysisQuestion;

class QuestionRepository{

	public function getAllQuestions(){


		//return "getAllQuestions from QuestionRepository";
        $questionMapper = Analogue::mapper(AnalysisQuestion::class);
        //dd($answerMapper);

        return $questionMapper->globalQuery()->whereNull('deleted_at')->get();
	}

	public function getQuestion($question_id){
        $questionMapper = Analogue::mapper(AnalysisQuestion::class);
        //dd($answerMapper);

        //dd( $answerMapper->query()->where('id',$answer_id)->first());
        return $questionMapper->globalQuery()->whereNull('deleted_at')->whereId($question_id)->first();


    }


}

