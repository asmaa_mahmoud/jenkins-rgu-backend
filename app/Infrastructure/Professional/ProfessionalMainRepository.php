<?php


namespace App\Infrastructure\Professional;


use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\CampusUserStatus;
use App\DomainModelLayer\Journeys\MissionHtmlSteps;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Professional\AnalysisCategory;
use App\DomainModelLayer\Professional\CampusActivityTask;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusRoundActivityTask;
use App\DomainModelLayer\Professional\CampusRoundClass;
use App\DomainModelLayer\Professional\CampusUser;
use App\DomainModelLayer\Professional\LearningPathActivityProgress;
use App\DomainModelLayer\Professional\LearningPathUser;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\MissionAngularUserAnswer;
use App\DomainModelLayer\Professional\CampusActivityQuestion;
use App\DomainModelLayer\Professional\DragQuestionAnswer;
use App\DomainModelLayer\Professional\DropdownQuestionAnswer;
use App\DomainModelLayer\Professional\MatchQuestionAnswer;
use App\DomainModelLayer\Professional\McqQuestionAnswer;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\RoomUser;
use App\DomainModelLayer\Professional\SchoolCampus;
use App\DomainModelLayer\Professional\Submission;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\Infrastructure\Accounts\AccountRepository;
use App\Infrastructure\Accounts\UserRepository;
use App\Infrastructure\Journeys\MissionRepository;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Question;

use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\MatchValue;
use App\DomainModelLayer\Journeys\Choice;


use App\DomainModelLayer\Professional\Submodule;


class ProfessionalMainRepository implements IProfessionalMainRepository
{
    public function getProductAssets($product_url)
    {
        $productRepository = new ProductRepository;
        return $productRepository->getProductAssets($product_url);
    }

    public function getAllQuestions()
    {
        $questionRepository = new QuestionRepository;
        return $questionRepository->getAllQuestions();
    }

    public function getQuestion($question_id)
    {
        $questionRepository = new QuestionRepository;
        return $questionRepository->getQuestion($question_id);
    }

    public function getAllAnswers()
    {
        $answerRepository = new AnswerRepository;
        return $answerRepository->getAllAnswers();
    }

    public function getAnswer($answer_id)
    {
        $answerRepository = new AnswerRepository;
        return $answerRepository->getAnswer($answer_id);
    }

    public function submit(Submission $submission)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->submit($submission);
    }

    public function getUserSubmission($user_id)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->getUserSubmission($user_id);
    }

    // public function getCategorySubmission($user_id,$category_code){
    //     $submissionRepository = new SubmissionRepository();
    //     return $submissionRepository->getCategorySubmission($user_id,$category_code);
    // }

    public function getSectionSubmissions($user_id, $categories)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->getSectionSubmissions($user_id, $categories);
    }

    public function getCategoryUserSubmission($user_id, $category_code)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->getCategoryUserSubmission($user_id, $category_code);
    }

    public function deleteUserSubmissions($user_id, $category_code)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->deleteUserSubmissions($user_id, $category_code);
    }

    public function insertUserQA(array $analysisQAArray)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->insertUserQA($analysisQAArray);
    }

    public function updateSubmissionResult($submission_id, $result)
    {
        $submissionRepository = new SubmissionRepository();
        return $submissionRepository->updateSubmissionResult($submission_id, $result);
    }
    public function getCategoryById($category_id)
    {
        $roboscaleRepository = new RoboscaleRepository;
        return $roboscaleRepository->getCategoryById($category_id);
    }
    public function getPostCategories(AnalysisCategory $category)
    {
        $roboscaleRepository = new RoboscaleRepository;
        return $roboscaleRepository->getPostCategories($category);
    }
    public function deleteCategoryPostSubmissions($user_id, $postCategories)
    {
        $submissionRepository = new SubmissionRepository;
        return $submissionRepository->deleteCategoryPostSubmissions($user_id, $postCategories);
    }

    public function getCategoryQuestions($category_id)
    {
        $roboscaleRepository = new RoboscaleRepository;
        return $roboscaleRepository->getCategoryQuestions($category_id);
    }

    public function getAllCategories()
    {
        $roboscaleRepository = new RoboscaleRepository;
        return $roboscaleRepository->getAllCategories();
    }

    public function getStudentLearningPath($student_id)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getStudentLearningPath($student_id);
    }

    public function getStudentLearningPaths($student_id)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getStudentLearningPaths($student_id);
    }

    public function getLearningPathsWithIds($ids)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getLearningPathsWithIds($ids);
    }

    public  function getLearnPathStages($learning_path_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getLearnPathStages($learning_path_id, $is_default);
    }

    public function getStageActivities($user_id, $stage_number, $learning_path_id, $is_default = true)
    {

        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getStageActivities($user_id, $stage_number, $learning_path_id, $is_default);
    }

    public function getStageActivitiesIds($stage_number, $learning_path_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getStageActivitiesIds($stage_number, $learning_path_id, $is_default);
    }

    public function getLearningPathById($id)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getLearningPathById($id);
    }

    public function getLearningPathByName($name)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getLearningPathByName($name);
    }

    public function storeLearningPathUser(LearningPathUser $learningPathUser)
    {
        $learnPathRepository = new LearnPathRepository();
        $learnPathRepository->storeLearningPathUser($learningPathUser);
    }

    public function getLearningPathActivity($learning_path_id, $activity_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getLearningPathActivity($learning_path_id, $activity_id, $is_default);
    }

    public function checkIfStudentInLearningPath($learning_path_id, $student_id)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->checkIfStudentInLearningPath($learning_path_id, $student_id);
    }

    public function getActivityInStage($learning_path_id, $stage_number, $activity_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getActivityInStage($learning_path_id, $stage_number, $activity_id, $is_default);
    }

    public function checkIfActivityInLearningPath($learning_path_id, $activity_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->checkIfActivityInLearningPath($learning_path_id, $activity_id, $is_default = true);
    }

    public function getUserTaskProgressActivity($task, $user_id, $activity_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getUserTaskProgressActivity($task, $user_id, $activity_id, $is_default);
    }

    public function getHtmlBlocklyCategories()
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getHtmlBlocklyCategories();
    }

    public function getUserTaskProgressActivityLearningPath(Task $task, $user_id, $learning_path_id, $activity_id, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getUserTaskProgressActivityLearningPath($task, $user_id, $learning_path_id, $activity_id, $is_default);
    }

    public function storeLearningPathActivityProgress(LearningPathActivityProgress $learningPathActivityProgress)
    {
        $learnPathRepository = new LearnPathRepository();
        $learnPathRepository->storeLearningPathActivityProgress($learningPathActivityProgress);
    }

    public function deleteLearningPathUser($user_id)
    {
        $learnPathRepository = new LearnPathRepository();
        $learnPathRepository->deleteLearningPathUser($user_id);
    }

    public function deleteLearningPathActivityProgress(LearningPathActivityProgress $learningPathActivityProgress)
    {
        $learnPathRepository = new LearnPathRepository();
        $learnPathRepository->deleteLearningPathActivityProgress($learningPathActivityProgress);
    }

    public function getUserTaskProgress($user_id, $learning_path_id, $task_id)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getUserTaskProgress($user_id, $learning_path_id, $task_id);
    }

    public function getSolvedTasksInActivity($user_id, $learning_path_id, $activity_id, $count = null, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getSolvedTasksInActivity($user_id, $learning_path_id, $activity_id, $count, $is_default);
    }

    public function getAllTasksInActivity($activity_id, $count = null, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getAllTasksInActivity($activity_id, $count, $is_default);
    }

    public function getAllTasksInStage($learning_path_id, $stage_number, $count = null, $is_default = true)
    {
        $learnPathRepository = new LearnPathRepository();
        return $learnPathRepository->getAllTasksInStage($learning_path_id, $stage_number, $count, $is_default);
    }

    //campus

    public function getCampusById($campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusById($campus_id);
    }

    public function getCampusUserRound($campus_id, $user_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusUserRound($campus_id, $user_id);
    }

    public function getUserCampusRounds($user_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getUserCampusRounds($user_id);
    }

    public function getCampusRoundById($round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusRoundById($round_id);
    }

    public function getStudentRunningRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentRunningRounds($user_id, $gmt_difference, $count);
    }

    public function getStudentRoundClasses($user_id, $campus_id, $round_id, $class_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentRoundClasses($user_id, $campus_id, $round_id, $class_id, $is_default);
    }

    public function getCampusClassById($class_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusClassById($class_id);
    }

    public function getSubmoduleById($sub_module_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSubmoduleById($sub_module_id);
    }

    public function getRoundById($round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundById($round_id);
    }
    public function getCampusActivityTaskById($campusActivityTaskId)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusActivityTaskById($campusActivityTaskId);
    }

    public function checkStudentInRound($student_id, $round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkStudentInRound($student_id, $round_id);
    }
    public function checkUserInRound($user_id, $round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkUserInRound($user_id, $round_id);
    }
    public function checkStudentInCampus($student_id, $campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkStudentInCampus($student_id, $campus_id);
    }

    public function checkClassInRound($class_id, $round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkClassInRound($class_id, $round_id);
    }

    public function checkClassInCampus($class_id, $campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkClassInCampus($class_id, $campus_id);
    }

    public function checkTaskInActivity($campus_id, $activity_id, $task_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkTaskInActivity($campus_id, $activity_id, $task_id, $is_default);
    }

    public function getCampusActivityTasks($activity_id, $campus_id, $is_default = true,$limit=null, $search = null, $order_by = null, $order_by_type = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusActivityTasks($activity_id, $campus_id, $is_default,$limit, $search, $order_by, $order_by_type);
    }

    public function getUserRoundTaskProgressActivity(Task $task, $user_id, $campus_id, $activity_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id, $is_default);
    }
    public function getCampusActivityQuestion(CampusActivityProgress $campusActivityProgress, Question $question)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusActivityQuestion($campusActivityProgress, $question);
    }

    public function getDragQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, DragCell $dragCell)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getDragQuestionAnswer($campusActivityQuestion, $dragCell);
    }


    public function getDropdownQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, Dropdown $dropdown)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getDropdownQuestionAnswer($campusActivityQuestion, $dropdown);
    }

    public function getMatchQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, MatchKey $matchKey, MatchValue $matchValue)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMatchQuestionAnswer($campusActivityQuestion, $matchKey, $matchValue);
    }
    public function getMcqQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, Choice $choice)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMcqQuestionAnswer($campusActivityQuestion, $choice);
    }
    public function getOldMcqQuestionAnswer(CampusActivityQuestion $campusActivityQuestion)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getOldMcqQuestionAnswer($campusActivityQuestion);
    }
    public function removeOldMcqQuestionAnswer(McqQuestionAnswer $mcqQuestionAnswer)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->removeOldMcqQuestionAnswer($mcqQuestionAnswer);
    }

    public function getUserLastSeenTask($user_id, $campus_id, $activity_id, $task_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getUserLastSeenTask($user_id, $campus_id, $activity_id, $task_id, $is_default = true);
    }

    public function storeRoundActivityProgress(CampusActivityProgress $campusActivityProgress)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeRoundActivityProgress($campusActivityProgress);
    }

    public function storeMissionAngularUserAnswer(MissionAngularUserAnswer $missionAngularUserAnswer)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeMissionAngularUserAnswer($missionAngularUserAnswer);
    }

    public function storeUserLastSeenTask(TaskLastSeen $taskLastSeen)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeUserLastSeenTask($taskLastSeen);
    }


    public function storeUserTaskCurrentStep(TaskCurrentStep $taskCurrentStep)
    {
        //        $campusRepository = new CampusRepository();
        //        return $campusRepository->storeUserTaskCurrentStep($taskCurrentStep);
        $taskCurrentStepRepository = new TaskCurrentStepRepository();
        return $taskCurrentStepRepository->storeUserTaskCurrentStep($taskCurrentStep);
    }
    public function getCampusUserStatus($campus_id,$user_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusUserStatus($campus_id,$user_id);
        
    }

    public function getAllCampusUserStatus($user_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllCampusUserStatus($user_id);
        
    }

  public function getStudentSolvedTasksInCampus($student_id, $campus_id, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentSolvedTasksInCampus($student_id, $campus_id, $count);
    }
    public function getStudentSolvedTasksInClass($student, $campus_id, $activity_id, $campusRound,$is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentSolvedTasksInClass($student, $campus_id, $activity_id,$campusRound, $is_default, $count);
    }
    ////////////////////////////////////////
    public function getStudentSolvedMissionsInClass($student_id, $campus_id, $activity_id, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentSolvedMissionsInClass($student_id, $campus_id, $activity_id, $is_default, $count);
    }
    public function getStudentCompletedClassesInClass($student_id, $campus_id, $activity_id, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentCompletedClassesInClass($student_id, $campus_id, $activity_id, $is_default, $count);
    }
    public function getAllMissionsInClass($campus_id, $activity_id, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllMissionsInClass($campus_id, $activity_id, $is_default = true, $count = null);
    }
    public function getAllClassesInClass($campus_id, $activity_id, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllClassesInClass($campus_id, $activity_id, $is_default = true, $count = null);
    }
    /////////////////////////////////////////
    public function getAllTasksInCampus($campus_id, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTasksInCampus($campus_id, $count);
    }
    public function getAvailaibleTasksInCampus(User $user,CampusRound $campusRound, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAvailaibleTasksInCampus( $user,$campusRound, $count);
    }
    public function getAllTasksInClass($campus_id, $activity_id,$learner,$campusRound, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTasksInClass($campus_id, $activity_id, $learner,$campusRound,$is_default, $count);
    }

    public function getRooms($round_class_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRooms($round_class_id);
    }

    public function getMaterialByRoomId($room_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMaterialByRoomId($room_id);
    }


    public function getRoomById($room_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoomById($room_id);
    }


    public function storeRoomUser(RoomUser $roomUser)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeRoomUser($roomUser);
    }

    public function storeRoom(Room $room)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeRoom($room);
    }

    public function storeMaterial(Material $material)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeMaterial($material);
    }

    public function checkIfActivityInCampus($campus_id, $activity_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkIfActivityInCampus($campus_id, $activity_id, $is_default);
    }

    public function deleteRoundActivityProgress(CampusActivityProgress $campusActivityProgress)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteRoundActivityProgress($campusActivityProgress);
    }

    public function getCampusClassTasks($activity_id, $campus_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusClassTasks($activity_id, $campus_id, $is_default);
    }

    public function getRoundClasses($round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundClasses($round_id);
    }

    public function checkIfCampusInSchool($campus_id, $school_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkIfCampusInSchool($campus_id, $school_id);
    }

    public function checkIfStudentInCampusRound($campus_round_id, $unAssignedStudent)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkIfStudentInCampusRound($campus_round_id, $unAssignedStudent);
    }

    public function checkIfTeacherInCampusRound($campus_round_id, $unAssignedStudent)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkIfTeacherInCampusRound($campus_round_id, $unAssignedStudent);
    }

    public function getCampusRoundUser($campus_round_id, $unAssignedStudent)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusRoundUser($campus_round_id, $unAssignedStudent);
    }

    public function deleteCampusRoundUser(CampusUser $campusUser)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusRoundUser($campusUser);
    }
    public function deleteCampusRound(CampusRound $round)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusRound($round);
    }
    public function deleteCampusRoundClass(CampusRoundClass $roundClass)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusRoundClass($roundClass);
    }
    public function deleteRoom(Room $room)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteRoom($room);
    }
    public function deleteRoomUser(RoomUser $roomUser)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteRoomUser($roomUser);
    }

    public function deleteCampusRoundActivityTask(CampusRoundActivityTask $roundTask)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusRoundActivityTask($roundTask);
    }

    public function storeCampusRoundUser(CampusUser $campusUser)
    {

        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusRoundUser($campusUser);
    }

    public function storeCampusRound(CampusRound $campusRound)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusRound($campusRound);
    }

    public function checkRoundNameAvailability($campus_id,$round_name, User $user)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkRoundNameAvailability($campus_id,$round_name, $user);
    }


    public function storeSchoolCampus(SchoolCampus $schoolCampus)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeSchoolCampus($schoolCampus);
    }

    public  function storeCampusRoundClass(CampusRoundClass $campusRoundClass)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusRoundClass($campusRoundClass);
    }
    public  function storeCampusRoundActivityTask(CampusRoundActivityTask $campusRoundActivityTask)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusRoundActivityTask($campusRoundActivityTask);
    }

    public function saveWatchedVideoFlag($round_id, $user_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->saveWatchedVideoFlag($round_id, $user_id);
    }

    public function getWatchedVideoFlag($round_id, $user_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getWatchedVideoFlag($round_id, $user_id);
    }

    public function getSolvedTasksInRound($campusRound ,$user)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSolvedTasksInRound($campusRound ,$user);
    }

    public function getAllTasksInRound($campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTasksInRound($campus_id);
    }

    public function getIntroDetails($round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getIntroDetails($round_id);
    }

    public function getClassTasksAndRooms($class_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getClassTasksAndRooms($class_id);
    }

    public function getCampusClassTasksAndRooms($class_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusClassTasksAndRooms($class_id);
    }

    public function getCampusActivityTask($campus_id, $task_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusActivityTask($campus_id, $task_id);
    }

    public function getCampusClass($campus_id, $activity_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusClass($campus_id, $activity_id, $is_default);
    }

    public function getClassInCampusByOrder($order, $campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getClassInCampusByOrder($order, $campus_id);
    }

    public function getLastClassInCampus($campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getLastClassInCampus($campus_id);
    }

    public function getCampusRoundClass($round_id, $class_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusRoundClass($round_id, $class_id);
    }

    public function getCampusRoundClassByActivityId(CampusRound $round, $activity_id, $is_default)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusRoundClassByActivityId($round,  $activity_id, $is_default);
    }

    public function getCampusRoundTaskByActivityId(CampusRound $round, $activity_id, $task_id, $is_default)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusRoundTaskByActivityId($round,  $activity_id, $task_id, $is_default);
    }

    public function getCampusTaskByActivityId($campus_id, $activity_id, $task_id, $is_default)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusTaskByActivityId($campus_id,  $activity_id, $task_id, $is_default);
    }

    public function getClassSubmissions($campus_id, $round_id, $activity_id, $limit, $search, $order_by, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getClassSubmissions($campus_id, $round_id, $activity_id, $limit, $search, $order_by, $is_default, $count);
    }

    public function getRoundActivityTask($round_id, $campusActivityTask_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundActivityTask($round_id, $campusActivityTask_id);
    }

    public function getCampusActivityProgressById($campus_activity_progress_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusActivityProgressById($campus_activity_progress_id);
    }

    public function storeCampusActivityProgress(CampusActivityProgress $campusActivityProgress)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusActivityProgress($campusActivityProgress);
    }
    public function storeCampusActivityQuestion(CampusActivityQuestion $campusActivityQuestion)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusActivityQuestion($campusActivityQuestion);
    }
    public function storeDragQuestionAnswer(DragQuestionAnswer $dragQuestionAnswer)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeDragQuestionAnswer($dragQuestionAnswer);
    }

    public function storeDropdownQuestionAnswer(DropdownQuestionAnswer $dropdownQuestionAnswer)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeDropdownQuestionAnswer($dropdownQuestionAnswer);
    }
    public function storeMatchQuestionAnswer(MatchQuestionAnswer $matchQuestionAnswer)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeMatchQuestionAnswer($matchQuestionAnswer);
    }
    public function storeMcqQuestionAnswer(McqQuestionAnswer $mcqQuestionAnswer)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
    }
    public  function getAllStudentsInCampuses($user_id, $limit, $search, $order_by, $order_by_type, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllStudentsInCampuses($user_id, $limit, $search, $order_by, $order_by_type, $gmt_difference, $count);
    }
    public  function getAllStudentsInCampusesExceptInRunningRounds($user_id, $rounds_id, $round_id, $limit, $search, $order_by, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllStudentsInCampusesExceptInRunningRounds($user_id, $rounds_id, $round_id, $limit, $search, $order_by, $count);
    }

    public  function getAllStudentsNotInRounds($school_id, array $rounds_id, $limit, $search, $order_by, $order_by_type, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllStudentsNotInRounds($school_id, $rounds_id, $limit, $search, $order_by, $order_by_type, $count);
    }

    public  function getAllTeachersInCampuses($user_id, $limit, $search, $search_for, $order_by, $order_by_type, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTeachersInCampuses($user_id, $limit, $search, $search_for, $order_by, $order_by_type, $count);
    }

    public  function deleteCampusUsers(User $student, $is_student = false)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusUsers($student, $is_student);
    }
    public  function deleteCampusUser(CampusUser $student)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusUser($student);
    }

    public function UserNameAlreadyExist(UserDto $userDto)
    {
        $userRepository = new UserRepository;
        return $userRepository->UserNameAlreadyExist($userDto);
    }

    public function getTeacherRunningRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getTeacherRunningRounds($user_id, $gmt_difference, $count);
    }

    public function getTeacherUpcomingRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getTeacherUpcomingRounds($user_id, $gmt_difference, $count);
    }

    public function getTeacherEndedRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getTeacherEndedRounds($user_id, $gmt_difference, $count);
    }

    public function getAllTeacherRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTeacherRounds($user_id, $gmt_difference, $count);
    }

    public function getAdminRunningRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAdminRunningRounds($user_id, $gmt_difference, $count);
    }
    public function getAdminUpcomingRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAdminUpcomingRounds($user_id, $gmt_difference, $count);
    }
    public function getAdminEndedRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAdminEndedRounds($user_id, $gmt_difference, $count);
    }
    public function getAllAdminRounds($user_id, $gmt_difference, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllAdminRounds($user_id, $gmt_difference, $count);
    }

    public function getClassNotEvaluatedTasks($roundClassId, $account_id, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getClassNotEvaluatedTasks($roundClassId, $account_id, $is_default, $count);
    }

    public function getStuckStudentsPerClass($roundClassId, $account_id, $is_default = true, $count = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStuckStudentsPerClass($roundClassId, $account_id, $is_default, $count);
    }

    public function storeCampusClass(CampusClass $class)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusClass($class);
    }
    public function getTopStudentsInCampusRound($account, $campus_id, $round_id, $limit = 10)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getTopStudentsInCampusRound($account, $campus_id, $round_id, $limit);
    }
    public function storeCampusActivityTask(CampusActivityTask $campusTask)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusActivityTask($campusTask);
    }

    public function getAllCampuses($campus_ids = [])
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllCampuses($campus_ids);
    }

    public function getProductCoursesIds($product_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getProductCoursesIds($product_id);
    }

    public function getSupportCampuses($user ,$gmt_difference)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSupportCampuses($user,$gmt_difference);
    }
    public function getRoundsInCampuses($campus_ids = [])
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundsInCampuses($campus_ids);
    }

    public function getAllClasses()
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllClasses();
    }
    public function getAllTasks()
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTasks();
    }

    public function getCampusClasses($campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusClasses($campus_id);
    }

    public function getRoundSubmissions($campus_id, $round_id, $from_time, $to_time)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundSubmissions($campus_id, $round_id, $from_time, $to_time);
    }

    public function getRoundStudentsSubmissions($campus_id, $round_id, $from_time, $to_time)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundStudentsSubmissions($campus_id, $round_id, $from_time, $to_time);
    }

    public function getCampusRoundTeachers($round_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusRoundTeachers($round_id);
    }

    public function getStudentSolvedTaskInRound($round, $activity_id, $task_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentSolvedTaskInRound($round, $activity_id, $task_id);
    }

    public function getMissionAngularUserAnswerByName($campus_activity_progress_id, $name)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMissionAngularUserAnswerByName($campus_activity_progress_id, $name);
    }

    public function getMissionAngularUserAnswers($campus_activity_progress_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMissionAngularUserAnswers($campus_activity_progress_id);
    }

    public function getAllStudentsSolvingTaskInCampus($account_id,$campus_id, $activity_id, $task_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllStudentsSolvingTaskInCampus($account_id,$campus_id, $activity_id, $task_id);
    }

    public function checkStudentSolvedAllSubModuleTasks($student_id, $default_activity_id, $tasks_ids)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkStudentSolvedAllSubModuleTasks($student_id, $default_activity_id, $tasks_ids);
    }

    public function checkIfStudentSolveTask($student_id,$campus_id, $activity_id, $task_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkIfStudentSolveTask($student_id,$campus_id, $activity_id, $task_id);
    }
    
    public function getStudentSolvedTasksInRoundClass($round, $activity_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentSolvedTasksInRoundClass($round, $activity_id);
    }

    public function getRecentActivityRounds($user_id, $gmt_difference)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRecentActivityRounds($user_id, $gmt_difference);
    }

    public function getRecentActivityClasses($user_id, $gmt_difference)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRecentActivityClasses($user_id, $gmt_difference);
    }

    public function getRecentActivityTasks($user_id, $gmt_difference)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRecentActivityTasks($user_id, $gmt_difference);
    }
    public function getStudentsProgressInRound($user_id, $account_id, $round_id, $campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentsProgressInRound($user_id, $account_id, $round_id, $campus_id);
    }
    public function checkStudentInRunningCampuses($user_id, array $rounds_id, $round_id, $gmt_difference)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkStudentInRunningCampuses($user_id, $rounds_id, $round_id, $gmt_difference);
    }
    public function getMaterialsClass($round_class_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMaterialsClass($round_class_id);
    }

    public function checkRoomNameExists($roundClass_id, $name, $room_id = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkRoomNameExists($roundClass_id, $name, $room_id);
    }
    public function checkRoomTimeExists($roundClass_id, $name, $starts_at, $room_id = null)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkRoomTimeExists($roundClass_id, $name, $starts_at, $room_id);
    }

    public function getMaterialById($material_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMaterialById($material_id);
    }
    public function getRoomsByMaterialId($material_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoomsByMaterialId($material_id);
    }
    public function deleteMaterial(Material $material)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteMaterial($material);
    }
    public function getEditorMissionByQuiz($campus_id, $activity_id, $task_id, $is_default)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getEditorMissionByQuiz($campus_id, $activity_id, $task_id, $is_default);
    }

    public function getAllColorPalettes()
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllColorPalettes();
    }

    public function checkIfStudentInRunningRounds($user_id, $rounds_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->checkIfStudentInRunningRounds($user_id, $rounds_id);
    }
    public function getLTIConsumerById($consumer_id)
    {
        $ltiRepository = new LTIRepository();
        return $ltiRepository->getLTIConsumerById($consumer_id);
    }
    public function getLTIConsumerByKey($consumer_key)
    {
        $ltiRepository = new LTIRepository();
        return $ltiRepository->getLTIConsumerByKey($consumer_key);
    }
    public function getLTIConsumerByAccountId($account_id)
    {
        $ltiRepository = new LTIRepository();
        return $ltiRepository->getLTIConsumerByAccountId($account_id);
    }
    public function getUserByLtiId($user_lti_id)
    {
        $ltiRepository = new LTIRepository();
        return $ltiRepository->getUserByLtiId($user_lti_id);
    }
    public function getRoundByLtiId($round_lti_id)
    {
        $ltiRepository = new LTIRepository();
        return $ltiRepository->getRoundByLtiId($round_lti_id);
    }

    public function checkIfSchoolAccountHasAdmin($account_id)
    {
        $ltiRepository = new LTIRepository();
        return $ltiRepository->checkIfSchoolAccountHasAdmin($account_id);
    }

    public function getTaskTypeTimeEstimate($type)
    {
        $taskTimeEstimateRepository = new TaskTimeEstimateRepository();
        return $taskTimeEstimateRepository->getTaskTypeTimeEstimate($type);
    }


    public function getMissionStepsCount($mission_id)
    {
        $missionHtmlStepsRepository = new MissionRepository();
        return $missionHtmlStepsRepository->getMissionStepsCount($mission_id);
    }


    public function getMissionSectionsCount($mission_id, $count = true)
    {
        $missionHtmlStepsRepository = new MissionRepository();
        return $missionHtmlStepsRepository->getMissionSectionsCount($mission_id, $count);
    }

    public function getMissionSection($mission_id, $section_id)
    {
        $missionHtmlStepsRepository = new MissionRepository();
        return $missionHtmlStepsRepository->getMissionSection($mission_id, $section_id);
    }


    public function getSectionWithOrder($mission_id, $order)
    {
        $missionHtmlStepsRepository = new MissionRepository();
        return $missionHtmlStepsRepository->getSectionWithOrder($mission_id, $order);
    }

    public function getSectionById($id)
    {
        $missionHtmlStepsRepository = new MissionRepository();
        return $missionHtmlStepsRepository->getSectionById($id);
    }


    public function getTaskCurrentStep($user_id, $task_id, $activity_id, $campus_id, $is_default = true)
    {
        $taskCurrentStepRepository = new TaskCurrentStepRepository();
        return $taskCurrentStepRepository->getUserTaskCurrentStep($user_id, $task_id, $activity_id, $campus_id, $is_default = true);
    }


    public function getCampusClassId($campus_id, $activity_id, $is_default = true)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusClass($campus_id, $activity_id, $is_default);
    }

    public function getlessonsSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getlessonsSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent);
    }

    public function getProfessionalCampusesSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getProfessionalCampusesSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent);
    }

    public function searchForTasks($search, $limit, $user_id, $user_role, $gmt_difference, $count)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->searchForTasks($search, $limit, $user_id, $user_role, $gmt_difference, $count);
    }

    public function searchForCountTasks($search)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->searchForCountTasks($search);
    }

    public function getRoomUser($room, $user, $role)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoomUser($room, $user, $role);
    }

    public function getQuestionAnswerById($campus_activity_question_id, $question_type)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getQuestionAnswerById($campus_activity_question_id, $question_type);
    }

    public function deleteQuestionAnswer($campus_activity_question_id, $question_type)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteQuestionAnswer($campus_activity_question_id, $question_type);
    }

    public function deleteCampusActivityQuestion(CampusActivityQuestion $campusActivityQuestion)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteCampusActivityQuestion($campusActivityQuestion);
    }

    public function getCampusDictionaryWords($campus_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusDictionaryWords($campus_id);
    }

    public function getCampusDictionaryWordsInActivity($campus_id,$activity_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusDictionaryWordsInActivity($campus_id,$activity_id);
    }

    public function getRankLevelById($id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getRankLevelById($id);
    }

    public function getStepIdForQuestion($question_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStepIdForQuestion($question_id);
    }

    public function getStepById($step_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getStepById($step_id);
    }

    public function getDragChoiceId($campus_activity_question_id, $drag_cell_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getDragChoiceId($campus_activity_question_id, $drag_cell_id);
    }

    public function getDragChoiceById($drag_choice_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getDragChoiceById($drag_choice_id);
    }

    public function getTaskDifficulty($task_id,$activity_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getTaskDifficulty($task_id,$activity_id);
    }

    public function getActivityTask($task_id,$activity_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getActivityTask($task_id,$activity_id);
    }

    public function getMainTaskByBoosterTask($task_id,$activity_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getMainTaskByBoosterTask($task_id,$activity_id);
    }

    public function getBoosterTaskByMainTask($task_id,$activity_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getBoosterTaskByMainTask($task_id,$activity_id);
    }    

    public function resetStudentStreaks($user,$campus_id,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->resetStudentStreaks($user,$campus_id,$gmt_difference);    
    }

    public function checkStudentStreaksLastSeen($user,$campus_id,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->checkStudentStreaksLastSeen($user,$campus_id,$gmt_difference);  
    }

    public function checkStudentCourseSeen($user,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->checkStudentCourseSeen($user,$campus_id);  
    }


    public function getStreaksActiveStatus($user_id,$campus_id,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->getStreaksActiveStatus($user_id,$campus_id,$gmt_difference);    
    }
    public function getStreaksWeek($user_id,$campus_id,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->getStreaksWeek($user_id,$campus_id,$gmt_difference);
    }

    public function getStreaksCount($user_id,$campus_id,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->getStreaksCount($user_id,$campus_id,$gmt_difference);
    }

    public function getLatestStreakPeriod($userId, $campusId)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getLatestStreakPeriod($userId, $campusId);
    }

    public function getTodayStudentStreak($user_id,$xp_gained,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->getTodayStudentStreak($user_id,$xp_gained,$gmt_difference);
    }

    public function updateStudentStreak($userStreak,$xp_gained){
        $campusRepository = new CampusRepository();
        return $campusRepository->updateStudentStreak($userStreak,$xp_gained);
    }

    public function storeStudentStreak($user_id,$xp_gained,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->storeStudentStreak($user_id,$xp_gained,$campus_id);
    }

//    public function storeCampusUserStatus($user_id,$campus_id,$xp,$rank_level_id=1){
//        $campusRepository = new CampusRepository();
//        return $campusRepository->storeCampusUserStatus($user_id,$campus_id,$xp,$rank_level_id);
//    }

    public function getStudentTasksByStatus($campus_id, $user_id,$status){
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentTasksByStatus($campus_id, $user_id,$status);
    }

    public function getStudentTasksInClassByStatus($campus_id, $user_id,$status,$activity_id, $is_default = true){
        $campusRepository = new CampusRepository();
        return $campusRepository->getStudentTasksInClassByStatus($campus_id, $user_id,$status,$activity_id, $is_default);
    }

    public function getMissionPerModuleCount($activity_id,$count=true){
        $campusRepository = new CampusRepository();
        return $campusRepository->getMissionPerModuleCount($activity_id,$count);
    }


    public function getEnrolledStudentsPerCourseCount($account_id,$campus_id,$status,$limit=null, $search=null, $order_by=null, $order_by_type=null){
        $campusRepository = new CampusRepository();
        return $campusRepository->getEnrolledStudentsPerCourseCount($account_id,$campus_id,$status,$limit, $search, $order_by, $order_by_type);
    }

    public function getEnrolledStudentsPerSubmodule($account_id,$campus_id,$module_id,$submodule_id,$count = false,$limit = null,$search = null,$order_by = null,$order_by_type = null){
        $campusRepository = new CampusRepository();
        return $campusRepository->getEnrolledStudentsPerSubmodule($account_id,$campus_id,$module_id,$submodule_id,$count,$limit,$search,$order_by,$order_by_type);
    }

    public function getCompletedStudentsPerCourseCount($account_id,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getCompletedStudentsPerCourseCount($account_id,$campus_id);
    }

    public function getAllTasksPerModuleCount($campus_id,$default_activity_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllTasksPerModuleCount($campus_id,$default_activity_id);
    }

    public function getProgressCountforStudentInModule($student_id,$campus_id,$default_activity_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getProgressCountforStudentInModule($student_id,$campus_id,$default_activity_id);

    }

    public function getUserRankLevelInCourse($student_id,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getUserRankLevelInCourse($student_id,$campus_id);
    }

    public function getLearnerCampusUser($user_id,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getLearnerCampusUser($user_id,$campus_id);

    }

    public function getCampusModules($campus_id,$limit, $search,$order_by,$order_by_type,$count){
        $campusRepository = new CampusRepository();
        return $campusRepository->getCampusModules($campus_id,$limit, $search,$order_by,$order_by_type,$count);

    }

    public function getModuleSubmodules($module_id,$limit, $search,$order_by,$order_by_type,$count){
        $campusRepository = new CampusRepository();
        return $campusRepository->getModuleSubmodules($module_id,$limit, $search,$order_by,$order_by_type,$count);

    }

    public function getRoundLearners($round_id, $limit, $search,$order_by,$order_by_type,$count){
        $campusRepository = new CampusRepository();
        return $campusRepository->getRoundLearners($round_id, $limit, $search,$order_by,$order_by_type,$count);
    }

    public function toggleHideBlocks($learnerStatus){
        $campusRepository = new CampusRepository();
        return $campusRepository->toggleHideBlocks($learnerStatus);
    }
    
    public function storeCampusUserStatus(CampusUserStatus $campusUserStatus){
        $campusRepository = new CampusRepository();
        return $campusRepository->storeCampusUserStatus($campusUserStatus);
    }

    public function checkTodayStudentStreak($user_id,$xp_gained,$gmt_difference){
        $campusRepository = new CampusRepository();
        return $campusRepository->checkTodayStudentStreak($user_id,$xp_gained,$gmt_difference);
    }

    public function getAllScoringConstants()
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllScoringConstants();
    }
    public function getScoringConstant($constantName)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getScoringConstant($constantName);
    }
    public function getMultipleScoringConstants($constantsNames)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMultipleScoringConstants($constantsNames);
    }

    public function countRankLevels()
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->countRankLevels();
    }

    public function countLevelsByRankId($rank_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->countLevelsByRankId($rank_id);
    }

    public function getMaxRankLevel($rank_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getMaxRankLevel($rank_id);
    }

    public function getNextRank($rank_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getNextRank($rank_id);
    }
    
    public function getAngularMissionByQuiz($campus_id, $activity_id, $quiz_id, $is_default){
        $campusRepository = new CampusRepository();
        return $campusRepository->getAngularMissionByQuiz($campus_id, $activity_id, $quiz_id, $is_default);
    }
    public function getTaskById($task_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getTaskById($task_id);
    }

    public function getAllDropdownTranslations(){
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllDropdownTranslations();
    }
    public function getDropdownQuestionById($id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getDropdownQuestionById($id); 
    }
    public function deleteDropdown($dropdown){
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteDropdown($dropdown);
    }

    public function getAllProgress(){
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllProgress();
    }
    public function getAllLastSeen(){
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllLastSeen();
    }

    public function getEditorMissions(){
        $campusRepository = new CampusRepository();
        return $campusRepository->getEditorMissions();
    }

    public function getAllMissionCoding($type){
        $campusRepository = new CampusRepository();
        return $campusRepository->getAllMissionCoding($type);
    }

    public function storeMissionCoding($mission){
        $campusRepository = new CampusRepository();
        return $campusRepository->storeMissionCoding($mission);
    }

    public function getLearnerCurrentRoundinCampus($user,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getLearnerCurrentRoundinCampus($user,$campus_id);
    }

    public function getLastAddedStreak($user_id,$campus_id){
        $campusRepository = new CampusRepository();
        return $campusRepository->getLastAddedStreak($user_id,$campus_id);
    }

    public function storeSearchVault($searchVault){
        $campusRepository = new CampusRepository();
        return $campusRepository->storeSearchVault($searchVault);
    }

    public function getFirstSubmoduleInModule($module)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getFirstSubmoduleInModule($module);
    }

    public function getSubmoduleInModuleByOrder($module,$order)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSubmoduleInModuleByOrder($module,$order);
    }

    public function hasFinishedSubmoduleTasks($user,$campus_id,$submodule)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->hasFinishedSubmoduleTasks($user,$campus_id,$submodule);
    }
    public function getUserSolvedSubmoduleMainTasks($user,$round,$submodule,$count=false)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getUserSolvedSubmoduleMainTasks($user,$round,$submodule,$count);
    }
    public function getSubmoduleMainTasks($submodule,$count=false)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSubmoduleMainTasks($submodule,$count);
    }

    public function getLastMainTaskInSubmodule($submodule)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getLastMainTaskInSubmodule($submodule);
    }
    public function getSubmoduleTaskOrder($task_id,$submodule_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSubmoduleTaskOrder($task_id,$submodule_id);
    }

    public function getSubmoduleTaskOrderByOrder($order,$submodule_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->getSubmoduleTaskOrderByOrder($order,$submodule_id);
    }

    public function isSubmoduleHaveTask($submodule_id, $task_id)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->isSubmoduleHaveTask($submodule_id, $task_id);   
    }

    public function deleteAllModuleSubmodules($submodules)
    {
        $campusRepository = new CampusRepository();
        return $campusRepository->deleteAllModuleSubmodules($submodules);   
    }

}
