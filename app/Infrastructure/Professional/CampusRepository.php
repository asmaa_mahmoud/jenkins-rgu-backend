<?php

/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/12/2018
 * Time: 11:00 AM
 */

namespace App\Infrastructure\Professional;


use ActivityProgress;
use Analogue;
use Analogue\ORM\Analogue as AnalogueAnalogue;
use App\ApplicationLayer\Professional\Dtos\CampusUserDto;
use App\ApplicationLayer\Journeys\QuestionTask;
use App\DomainModelLayer\Journeys\MissionHtmlSteps;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\MissionEditor;
use App\DomainModelLayer\Journeys\MissionAngular;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\RankLevel;
use App\DomainModelLayer\Journeys\Rank;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Professional\CampusTranslation;
use App\DomainModelLayer\Journeys\ActivityTask;
use App\DomainModelLayer\Journeys\Difficulty;
use App\DomainModelLayer\Journeys\ActivityTranslation;
use App\DomainModelLayer\Journeys\MissionTranslation;
use App\DomainModelLayer\Journeys\MissionHtmlTranslation;
use App\DomainModelLayer\Journeys\McqQuestionTranslation;
use App\DomainModelLayer\Journeys\ChoiceTranslation;
use App\DomainModelLayer\Journeys\DragChoiceTranslation;
use App\DomainModelLayer\Journeys\MatchKeyTranslation;
use App\DomainModelLayer\Journeys\MatchValueTranslation;
use App\DomainModelLayer\Journeys\DropdownChoiceTranslation;
use App\DomainModelLayer\Journeys\DropdownQuestionTranslation;
use App\DomainModelLayer\Journeys\DropdownQuestion;
use App\DomainModelLayer\Journeys\MissionEditorTranslation;
use App\DomainModelLayer\Journeys\MissionCodingTranslation;
use App\DomainModelLayer\Journeys\MissionCoding;
use App\DomainModelLayer\Journeys\QuizTranslation;
use App\DomainModelLayer\Journeys\MissionHtmlStepsTranslation;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\MissionAngularUserAnswer;
use App\DomainModelLayer\Professional\CampusActivityQuestion;
use App\DomainModelLayer\Professional\DragQuestionAnswer;
use App\DomainModelLayer\Professional\MatchQuestionAnswer;
use App\DomainModelLayer\Professional\McqQuestionAnswer;
use App\DomainModelLayer\Professional\DropdownQuestionAnswer;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusWelcomeModules;
use App\DomainModelLayer\Professional\DictionaryWord;
use App\DomainModelLayer\Professional\SearchVault;
//use App\DomainModelLayer\Professional\CampusTranslation;
use App\DomainModelLayer\Professional\CampusRoundActivityTask;
use App\DomainModelLayer\Professional\CampusUser;
use App\DomainModelLayer\Professional\CampusProduct;
use App\DomainModelLayer\Professional\Product;
use App\DomainModelLayer\Professional\CampusRoundClass;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\CampusActivityTask;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\SchoolCampus;
use App\DomainModelLayer\Professional\RoomUser;
use App\DomainModelLayer\Professional\ColorPalette;
use App\DomainModelLayer\Professional\ScoringConstant;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\DomainModelLayer\Professional\Submodule;
use App\DomainModelLayer\Professional\SubmoduleTask;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\MatchValue;
use App\DomainModelLayer\Journeys\Choice;
use App\DomainModelLayer\Journeys\CampusUserStreaks;
use App\DomainModelLayer\Journeys\CampusUserStatus;
use DB;
use Carbon\Carbon;
use function foo\func;



class CampusRepository
{

    public function getCampusById($campus_id)
    {
        $campusMapper = Analogue::mapper(Campus::class);
        $query = $campusMapper->query()->whereId($campus_id)->first();
        return $query;
    }

    public function getCampusUserRound($campus_id, $user_id)
    {

        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->where('campus_id', $campus_id)->whereIn('id', function ($q) use ($campus_id, $user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
        });

        $query = $query->get();

        return $query;
    }

    public function getStudentRunningRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
        })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
            ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getUserCampusRounds($user_id)
    {
        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $query = $campusUserMapper->query()->where('user_id', $user_id);
        return $query->get();
    }
    public function getCampusRoundsPerUser($user_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusRound::class);
        $query = $campusRoundMapper->query()->where('user_id', $user_id);
        return $query->get();
    }


    public function getCampusRoundById($round_id)
    {

        $roundMapper = Analogue::mapper(CampusRound::class);
        $query = $roundMapper->query()->whereId($round_id)->first();
        return $query;
    }

    public function getIntroDetails($round_id)
    {

        $campusWelcomeModulesMapper = Analogue::mapper(CampusWelcomeModules::class);
        $campusWelcomeModule = $campusWelcomeModulesMapper->query()->whereIn('class_id', function ($q) use ($round_id) {
            $q->select('id')->from('class')->where("lessonType", '2')->whereIn('campus_id', function ($qq) use ($round_id) {
                $qq->select('campus_id')->from('campus_round')->whereId($round_id);
            });
        })->first();
        return $campusWelcomeModule;
    }

    public function getCampusClassById($class_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusClass::class);

        $query = $campusRoundMapper->query()->where('id', '=', $class_id)->first();
        return $query;
    }
    public function getSubmoduleById($sub_module_id)
    {
        $submoduleMapper = Analogue::mapper(Submodule::class);

        $query = $submoduleMapper->query()->where('id', '=', $sub_module_id)->first();
        return $query;
    }

    public function getRoundById($round_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusRound::class);

        $query = $campusRoundMapper->query()->where('id', '=', $round_id)->first();
        return $query;
    }
    public function getCampusActivityTaskById($campusActivityTaskId)
    {
        $campusRoundMapper = Analogue::mapper(CampusActivityTask::class);

        $query = $campusRoundMapper->query()->where('id', '=', $campusActivityTaskId)->first();
        return $query;
    }

    public function checkStudentInRound($student_id, $round_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusUser::class);

        $query = $campusRoundMapper->query()->where('campus_round_id', '=', $round_id)->where('user_id', '=', $student_id)->get();
        return $query;
    }

    public function checkUserInRound($user_id, $round_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusUser::class);

        $query = $campusRoundMapper->query()->where('campus_round_id', '=', $round_id)->where('user_id', '=', $user_id)->get();
        if ($query === null)
            return false;
        else
            return true;
    }
    public function checkStudentInCampus($student_id, $campus_id)
    {

        $campusUserMapper = Analogue::mapper(CampusUserDto::class);

        $query = $campusUserMapper->query()->where('user_id', '=', $student_id)
            ->whereIn('campus_round_id', function ($q) use ($campus_id) {
                $q->select('id')->from('campus_round')->where('campus_id', '=', $campus_id);
            })->first();
        if ($query == null)
            return false;
        else
            return true;
    }

    public function checkClassInRound($class_id, $round_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusRoundClass::class);

        $query = $campusRoundMapper->query()->where('campus_round_id', '=', $round_id)->where('class_id', '=', $class_id)->get();
        return $query;
    }
    public function checkClassInCampus($class_id, $campus_id)
    {
        $campusRoundMapper = Analogue::mapper(CampusClass::class);

        $query = $campusRoundMapper->query()->where('campus_id', '=', $campus_id)->whereId($class_id)->first();
        if ($query == null)
            return false;
        else
            return true;
    }

    public function getCampusActivityTasks($activity_id, $campus_id, $is_default = true,$limit=null, $search = null, $order_by = null, $order_by_type = null)
    {

         $campusRoundMapper = Analogue::mapper(CampusActivityTask::class);
        if ($is_default == true) {

            $query = $campusRoundMapper->query()->where('campus_id', '=', $campus_id)->where('default_activity_id', '=', $activity_id);
        } else {
            $query = $campusRoundMapper->query()->where('campus_id', '=', $campus_id)->where('activity_id', '=', $activity_id);
        }


        if ($search != null) {
            $query = $query->whereIn('task_id', function ($q1)use($search){
                $q1->select('id')->from('task')->whereIn('id', function ($q2)use($search){
                    $q2->select('task_id')->from('mission')->whereIn('id', function ($q3)use($search){
                        $q3->select('mission_id')->from('mission_translation')->where('title', 'like', '%'.$search.'%');  });
                })
                ->orwhereIn('id', function ($q2)use($search){
                    $q2->select('task_id')->from('mission_html')->where('is_mini_project', 1)->whereIn('id', function ($q3)use($search){
                        $q3->select('mission_html_id')->from('mission_html_translation')->where('title', 'like', '%'.$search.'%');
                    });
                })
                ->orwhereIn('id', function ($q2)use($search){
                    $q2->select('task_id')->from('quiz')->whereIn('id', function ($q3)use($search){
                        $q3->select('quiz_id')->from('quiz_translation')->where('title', 'like', '%'.$search.'%');  });

                })
                ->orwhereIn('id', function ($q2)use($search){
                    $q2->select('task_id')->from('mission_coding')->whereIn('id', function ($q3)use($search){
                        $q3->select('mission_coding_id')->from('mission_coding_translation')->where('title', 'like', '%'.$search.'%');  });
                })
                ->orwhereIn('id', function ($q2)use($search){
                    $q2->select('task_id')->from('mission_editor')->whereIn('id', function ($q3)use($search){
                        $q3->select('mission_editor_id')->from('mission_editor_translation')->where('title', 'like', '%'.$search.'%');  });
                });
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null){
            $query = $query->paginate($limit);
        }
        else{
            $query = $query->get();
        }


// dd($query);
        return $query;
    }

    public function getStudentSolvedTasksInCampus($student_id, $campus_id, $count = null)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $student_id)
            ->whereNotNull('first_success');

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
    public function getStudentSolvedTasksInClass($user, $campus_id, $activity_id,$campusRound ,$is_default = true, $count = null)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $user->id)->whereNull('deleted_at')
            ->whereNotNull('first_success');

        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        if(!$user->showBlocks($campus_id)&&$campusRound->showClasses ())
        {
           $query=$query->whereNotIn('task_id', function ($q) {
            $q->select('task_id')->from('mission');
        });
        }
        elseif($user->showBlocks($campus_id)&&!$campusRound->showClasses ())
        {
            $query=$query->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission_html')->where('is_mini_project', '0');
            });
        }
        elseif(!$user->showBlocks($campus_id)&&!$campusRound->showClasses ())
        {
            $query=$query->whereNotIn('task_id', function ($q) {
                    $q->select('task_id')->from('mission');
                })->whereNotIn('task_id', function ($q1) {
                    $q1->select('task_id')->from('mission_html')->where('is_mini_project', '0');
                });
        }
        
         //Exclude Boosters
        $query=$query->whereIn('task_id',function ($q){
            $q->select('task_id')->from('activity_task')->where('booster_type','!=',1)
              ->whereIn('default_activity_id',function($q2){
                $q2->select('default_activity_id')->from('activity_task');
            });
        });    
        
            
        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function getAllTasksInCampus($campus_id, $count = null)
    {
        $campusTasks = Analogue::mapper(CampusActivityTask::class);
        $query = $campusTasks->query()->where('campus_id', $campus_id);

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
    public function getCampusUserStatus($campus_id,$user_id)
    {
        $campusStatus = Analogue::mapper(CampusUserStatus::class);
        $query = $campusStatus->query()->where('campus_id', $campus_id)->where('user_id',$user_id);
        return $query->first();
       
    }

    public function getAllCampusUserStatus($user_id)
    {
        $campusStatus = Analogue::mapper(CampusUserStatus::class);
        $query = $campusStatus->query()->where('user_id',$user_id);
        return $query->get();
       
    }

    public function storeCampusUserStatus(CampusUserStatus $campusUserStatus)
    {
        $campusUserStatusMapper = Analogue::mapper(CampusUserStatus::class);
        return $campusUserStatusMapper->store($campusUserStatus);
    }

    public function getAvailaibleTasksInCampus(User $user,CampusRound $campusRound, $count = null)
    {   
        $campus_id=$campusRound->getCampus_id();
        $campusTasks = Analogue::mapper(CampusActivityTask::class);
        if($user->showBlocks($campus_id)&&$campusRound->showClasses ())
        {
            $query = $campusTasks->query()->where('campus_id', $campus_id);
        }
        elseif(!$user->showBlocks($campus_id)&&$campusRound->showClasses ())
        {
           $query= $campusTasks->query()->where('campus_id', $campus_id)->whereNotIn('task_id', function ($q) {
            $q->select('task_id')->from('mission');

        });
        }
        elseif($user->showBlocks($campus_id)&&!$campusRound->showClasses ())
        {
            $query= $campusTasks->query()->where('campus_id', $campus_id)->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission_html')->where('is_mini_project', '0');
            });
        }
        elseif(!$user->showBlocks($campus_id)&&!$campusRound->showClasses ())
        {
            $query= $campusTasks->query()->where('campus_id', $campus_id)->whereNotIn('task_id', function ($q) {
                    $q->select('task_id')->from('mission');
                })->whereNotIn('task_id', function ($q1) {
                    $q1->select('task_id')->from('mission_html')->where('is_mini_project', '0');
                });
        }
        //Exclude Boosters
        $query=$query->whereIn('task_id',function ($q){
            $q->select('task_id')->from('activity_task')->where('booster_type','!=',1)
              ->whereIn('default_activity_id',function($q2){
                $q2->select('default_activity_id')->from('activity_task');
            });
        });

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query; 
    }
    public function getAllTasksInClass($campus_id, $activity_id,$user,$campusRound, $is_default = true, $count = null)
    {
        $campusTasks = Analogue::mapper(CampusActivityTask::class);
        $query = $campusTasks->query()->where('campus_id', $campus_id)->whereNull('deleted_at');

        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

            if(!$user->showBlocks($campus_id)&&$campusRound->showClasses ())
            {
               $query=$query->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission');
            });
            }
            elseif($user->showBlocks($campus_id)&&!$campusRound->showClasses ())
            {
                $query=$query->whereNotIn('task_id', function ($q) {
                    $q->select('task_id')->from('mission_html')->where('is_mini_project', '0');
                });
            }
            elseif(!$user->showBlocks($campus_id)&&!$campusRound->showClasses ())
            {
                $query=$query->whereNotIn('task_id', function ($q) {
                        $q->select('task_id')->from('mission');
                    })->whereNotIn('task_id', function ($q1) {
                        $q1->select('task_id')->from('mission_html')->where('is_mini_project', '0');
                    });
            }
            
        //Exclude Boosters
        $query=$query->whereIn('task_id',function ($q){
            $q->select('task_id')->from('activity_task')->where('booster_type','!=',1)
              ->whereIn('default_activity_id',function($q2){
                $q2->select('default_activity_id')->from('activity_task');
            });
        });

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
    public function checkTaskInActivity($campus_id, $activity_id, $task_id, $is_default = true)
    {
        $campusRoundMapper = Analogue::mapper(CampusActivityTask::class);
        if ($is_default == true) {
            $query = $campusRoundMapper->query()->where('campus_id', '=', $campus_id)->where('default_activity_id', '=', $activity_id)->where('task_id', $task_id)->get();
        } else {
            $query = $campusRoundMapper->query()->where('campus_id', '=', $campus_id)->where('activity_id', '=', $activity_id)->where('task_id', $task_id)->get();
        }

        return $query;
    }

    

    public function getUserRoundTaskProgressActivity(Task $task, $user_id, $campus_id, $activity_id, $is_default = true)
    {
        $progressMapper = Analogue::mapper(CampusActivityProgress::class);
        $taskProgress = $progressMapper->query()->where('campus_id', $campus_id)->where('task_id', $task->getId())->where('user_id', $user_id);
        if ($is_default)
            $taskProgress = $taskProgress->where('default_activity_id', $activity_id)->first();
        else
            $taskProgress = $taskProgress->where('activity_id', $activity_id)->first();
        return $taskProgress;
    }

    public function getUserLastSeenTask($user_id, $campus_id, $activity_id, $task_id, $is_default = true)
    {
        $lastSeenMapper = Analogue::mapper(TaskLastSeen::class);
        $lastSeen = $lastSeenMapper->query()->where('campus_id', $campus_id)->where('task_id', $task_id)->where('user_id', $user_id);
        if ($is_default)
            $lastSeen = $lastSeen->where('default_activity_id', $activity_id)->first();
        else
            $lastSeen = $lastSeen->where('activity_id', $activity_id)->first();
        return $lastSeen;
    }


    public function storeRoundActivityProgress(CampusActivityProgress $campusActivityProgress)
    {
        $ActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $ActivityProgressMapper->store($campusActivityProgress);
        return $campusActivityProgress;
    }

    public function storeMissionAngularUserAnswer(MissionAngularUserAnswer $missionAngularUserAnswer)
    {
        $missionAngularUserAnswerMapper = Analogue::mapper(MissionAngularUserAnswer::class);
        $missionAngularUserAnswerMapper->store($missionAngularUserAnswer);
        return $missionAngularUserAnswer;
    }

    public function storeUserLastSeenTask(TaskLastSeen $taskLastSeen)
    {
        $taskLastSeenMapper = Analogue::mapper(TaskLastSeen::class);
        $taskLastSeenMapper->store($taskLastSeen);
    }


    public function storeUserTaskCurrentStep(TaskCurrentStep $taskCurrentStep)
    {
        $taskLastSeenMapper = Analogue::mapper(TaskCurrentStep::class);
        $taskLastSeenMapper->store($taskCurrentStep);
    }


    public function deleteRoundActivityProgress(CampusActivityProgress $campusActivityProgress)
    {
        $ActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $ActivityProgressMapper->delete($campusActivityProgress);
    }

    public function getCampusActivityTask($campus_id, $task_id)
    {
        $campusTaskMapper = Analogue::mapper(CampusActivityTask::class);
        $query = $campusTaskMapper->query()->where('campus_id', $campus_id)->where('task_id', $task_id)->first();

        return $query;
    }

    public function getCampusClass($campus_id, $activity_id, $is_default = true)
    {



        $campusTaskMapper = Analogue::mapper(CampusClass::class);
        $query = $campusTaskMapper->query()->where('campus_id', $campus_id);

        if ($is_default)

            $query = $query->where('default_activity_id', $activity_id)->first();
        else

            $query = $query->where('activity_id', $activity_id)->first();

        return $query;
    }

    public function getClassInCampusByOrder($order, $campus_id)
    {
        $campusTaskMapper = Analogue::mapper(CampusClass::class);
        $query = $campusTaskMapper->query()->where('campus_id', $campus_id)->where('order', $order);

        return $query->first();
    }

    public function getLastClassInCampus($campus_id)
    {
        $campusTaskMapper = Analogue::mapper(CampusClass::class);
        $query = $campusTaskMapper->query()->where('campus_id', $campus_id)->orderBy('order', 'desc')->first();

        return $query;
    }

    public function getCampusRoundClass($round_id, $class_id)
    {
        $roundClassMapper = Analogue::mapper(CampusRoundClass::class);
        $query = $roundClassMapper->query()->where('campus_round_id', $round_id)->where('class_id', $class_id)->first();

        return $query;
    }

    public function getCampusRoundClassByActivityId(CampusRound $round, $activity_id, $is_default)
    {
        $roundClassMapper = Analogue::mapper(CampusRoundClass::class);
        $query = $roundClassMapper->query()->where('campus_round_id', $round->getId())->where('class_id', function ($q) use ($round, $activity_id, $is_default) {
            $q->select('id')->from('class')->whereNull('deleted_at')->where('campus_id', $round->getCampus_id());
            if ($is_default)
                $q->where('default_activity_id', $activity_id);
            else
                $q->where('activity_id', $activity_id);
        })->first();

        return $query;
    }

    public function getCampusRoundTaskByActivityId(CampusRound $round, $activity_id, $task_id, $is_default)
    {
        $roundClassMapper = Analogue::mapper(CampusRoundActivityTask::class);
        $query = $roundClassMapper->query()->where('campus_round_id', $round->getId())->where('campus_activity_task_id', function ($q) use ($round, $activity_id, $task_id, $is_default) {
            $q->select('id')->from('campus_activity_task')->whereNull('deleted_at')->where('task_id', $task_id)->where('campus_id', $round->getCampus_id());
            if ($is_default)
                $q->where('default_activity_id', $activity_id);
            else
                $q->where('activity_id', $activity_id);
        })->first();

        return $query;
    }

    public function getCampusTaskByActivityId($campus_id, $activity_id, $task_id, $is_default)
    {
        $roundClassMapper = Analogue::mapper(CampusActivityTask::class);
        $query = $roundClassMapper->query()->where('campus_id', $campus_id)->whereNull('deleted_at')->where('task_id', $task_id);
        if ($is_default)
            $query->where('default_activity_id', $activity_id);
        else
            $query->where('activity_id', $activity_id);

        return $query->first();
    }

    public function getClassSubmissions($campus_id, $round_id, $activity_id, $limit = null, $search = null, $order_by = null, $is_default = true, $count = null)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)
            ->whereIn('user_id', function ($q) use ($round_id) {
                $q->select('user_id')->from('campus_user')->where('campus_round_id', $round_id)->whereIn('role_id', function ($queryy) {
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                });
            })->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission_html')->where('is_mini_project', '0')->whereNull('deleted_at');
            });

        if ($is_default == true)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        if ($order_by != null)
            $query = $query->orderBy($order_by);
        if ($limit != null)
            $query = $query->paginate($limit);


        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getRoundActivityTask($round_id, $campusActivityTask_id)
    {
        $taskProgressMapper = Analogue::mapper(CampusRoundActivityTask::class);
        $query = $taskProgressMapper->query()->where('campus_round_id', $round_id)
            ->where('campus_activity_task_id', $campusActivityTask_id)->first();
        return $query;
    }

    public function getCampusActivityProgressById($campus_activity_progress_id)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->whereId($campus_activity_progress_id)->first();
        return $query;
    }

    public function storeCampusActivityProgress(CampusActivityProgress $campusActivityProgress)
    {
        $ActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $ActivityProgressMapper->store($campusActivityProgress);
    }
    public function storeCampusActivityQuestion(CampusActivityQuestion $campusActivityQuestion)
    {
        $campusActivityQuestionMapper = Analogue::mapper(CampusActivityQuestion::class);
        $campusActivityQuestionMapper->store($campusActivityQuestion);
    }
    public function storeDragQuestionAnswer(DragQuestionAnswer $dragQuestionAnswer)
    {
        $dragQuestionAnswerMapper = Analogue::mapper(DragQuestionAnswer::class);
        $dragQuestionAnswerMapper->store($dragQuestionAnswer);
    }

    public function storeDropdownQuestionAnswer(DropdownQuestionAnswer $dropdownQuestionAnswer)
    {
        $dropdownQuestionAnswerMapper = Analogue::mapper(DropdownQuestionAnswer::class);
        $dropdownQuestionAnswerMapper->store($dropdownQuestionAnswer);
    }
    public function storeMatchQuestionAnswer(MatchQuestionAnswer $matchQuestionAnswer)
    {
        $matchQuestionAnswerMapper = Analogue::mapper(MatchQuestionAnswer::class);
        $matchQuestionAnswerMapper->store($matchQuestionAnswer);
    }
    public function storeMcqQuestionAnswer(McqQuestionAnswer $mcqQuestionAnswer)
    {
        $mcqQuestionAnswerMapper = Analogue::mapper(McqQuestionAnswer::class);
        $mcqQuestionAnswerMapper->store($mcqQuestionAnswer);
    }
    public function getRoundClasses($round_id)
    {
        $roundClassMapper = Analogue::mapper(CampusRoundClass::class);
        $query = $roundClassMapper->query()->where('campus_round_id', '=', $round_id);
        $query= $query->join('class','class.id','=','campus_round_class.class_id')
        ->orderBy('class.order')
        ->select('campus_round_class.*')
        ;
        return $query->get();
    }

    public function getRoundRooms($round_id)
    {
        $roundClassMapper = Analogue::mapper(CampusRoundClass::class);
        $query = $roundClassMapper->query()->where('campus_round_id', '=', $round_id)->get();
        return $query;
    }

    public function getCampusClassTasks($activity_id, $campus_id, $is_default)
    {
        $roundClassMapper = Analogue::mapper(CampusActivityTask::class);
        $query = $roundClassMapper->query()->where('campus_id', $campus_id);
        if ($is_default)
            $query->where('default_activity_id', '=', $activity_id);
        else
            $query->where('activity_id', '=', $activity_id);
        return $query->get();
    }

    public function getClassTasksAndRooms($class_id)
    {
        $roundClassMapper = Analogue::mapper(CampusRoundClass::class);
        $query = $roundClassMapper->query()->where('class_id', $class_id)->first();
        return $query;
    }
    public function getCampusClassTasksAndRooms($class_id)
    {
        $roundClassMapper = Analogue::mapper(CampusClass::class);
        $query = $roundClassMapper->query()->where('id', $class_id)->first();
        return $query;
    }

    public function  checkIfCampusInSchool($campus_id, $school_id)
    {
        $schoolCampusMapper = Analogue::mapper(SchoolCampus::class);
        $schoolCampus = $schoolCampusMapper->query()->where('campus_id', $campus_id)->where('school_id', $school_id)->first();
        if ($schoolCampus != null)
            return true;

        return false;
    }

    public function checkIfStudentInCampusRound($campus_round_id, $user_id)
    {
        $schoolCampusMapper = Analogue::mapper(CampusUser::class);
        $schoolCampusUser = $schoolCampusMapper->query()->where('campus_round_id', $campus_round_id)->where('user_id', $user_id)->whereIn('role_id', function ($q) {
            $q->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
        })->first();
        if ($schoolCampusUser != null)
            return true;

        return false;
    }

    public function checkIfTeacherInCampusRound($campus_round_id, $unAssignedStudent)
    {
        $schoolCampusMapper = Analogue::mapper(CampusUser::class);
        $schoolCampusUser = $schoolCampusMapper->query()->where('campus_round_id', $campus_round_id)->where('user_id', $unAssignedStudent)->whereIn('role_id', function ($q) {
            $q->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
        })->first();
        if ($schoolCampusUser != null)
            return true;

        return false;
    }

    public function deleteCampusRoundUser(CampusUser $campusUser)
    {
        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $campusUserMapper->delete($campusUser);
    }

    public function deleteCampusRound(CampusRound $round)
    {
        $campusRoundMapper = Analogue::mapper(CampusRound::class);
        $campusRoundMapper->delete($round);
    }

    public function deleteCampusRoundClass(CampusRoundClass $roundClass)
    {
        $campusRoundClassMapper = Analogue::mapper(CampusRoundClass::class);
        $campusRoundClassMapper->delete($roundClass);
    }
    public function deleteRoom(Room $room)
    {
        $campusRoundClassMapper = Analogue::mapper(Room::class);
        $campusRoundClassMapper->delete($room);
    }
    public function deleteRoomUser(RoomUser $roomUser)
    {
        $campusRoundClassMapper = Analogue::mapper(RoomUser::class);
        $campusRoundClassMapper->delete($roomUser);
    }

    public function deleteCampusRoundActivityTask(CampusRoundActivityTask $roundTask)
    {
        $campusRoundTaskMapper = Analogue::mapper(CampusRoundActivityTask::class);
        $campusRoundTaskMapper->delete($roundTask);
    }

    public function storeCampusRoundUser(CampusUser $campusUser)
    {
        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $campusUserMapper->store($campusUser);
    }

    public function storeCampusRound(CampusRound $campusRound)
    {
        $campusRoundMapper = Analogue::mapper(CampusRound::class);
        $campusRoundMapper->store($campusRound);
    }

    public function checkRoundNameAvailability($campus_id,$round_name, User $user)
    {
        $campusRoundMapper = Analogue::mapper(CampusRound::class);
        $query = $campusRoundMapper->query()->where('campus_id', $campus_id)->where('round_name', $round_name)
                ->whereIn('id', function($q) use($user){
                    $q->select("campus_round_id")->from("campus_user")->where("user_id", $user->getId());
                })->first();
        
        return isset($query);
    }


    public function storeSchoolCampus(SchoolCampus $schoolCampus)
    {
        $schoolCampusMapper = Analogue::mapper(SchoolCampus::class);
        $schoolCampusMapper->store($schoolCampus);
    }

    public function storeCampusRoundClass(CampusRoundClass $campusRoundClass)
    {
        $campusRoundMapper = Analogue::mapper(CampusRoundClass::class);
        $campusRoundMapper->store($campusRoundClass);
    }

    public function storeCampusRoundActivityTask(CampusRoundActivityTask $campusRoundActivityTask)
    {
        $campusRoundActivityTaskMapper = Analogue::mapper(CampusRoundActivityTask::class);
        $campusRoundActivityTaskMapper->store($campusRoundActivityTask);
    }

    public function saveWatchedVideoFlag($round_id, $user_id)
    {

        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $campus_user_record = $campusUserMapper->query()->where('campus_round_id', $round_id)->where('user_id', $user_id)->firstOrFail();
        $campus_user_record->hasSeen = 1;
        $campusUserMapper->store($campus_user_record);
        return 'ok';
    }

    public function getWatchedVideoFlag($round_id, $user_id)
    {

        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $campus_user_record = $campusUserMapper->query()->where('campus_round_id', $round_id)->where('user_id', $user_id)->firstOrFail();
        return $campus_user_record->hasSeen;
    }

    public function getSolvedTasksInRound(CampusRound $campusRound , User $user)
    {

        $campusActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $campus_id=$campusRound->getCampus_id();
        
        if($user->showBlocks($campus_id)&&$campusRound->showClasses ()){
            $query = $campusActivityProgressMapper->query()
            ->where('campus_id', $campus_id)
            ->where('user_id', $user->id)
            ->where('first_success', '!=', NULL);
        }elseif(!$user->showBlocks($campus_id)&&$campusRound->showClasses ()){
            $query = $campusActivityProgressMapper->query()
            ->where('campus_id', $campus_id)
            ->where('user_id', $user->id)
            ->where('first_success', '!=', NULL)
            ->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission');
    
            });
        }elseif($user->showBlocks($campus_id)&&!$campusRound->showClasses ()){
            $query = $campusActivityProgressMapper->query()
            ->where('campus_id', $campus_id)
            ->where('user_id', $user->id)
            ->where('first_success', '!=', NULL)->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission_html')->where('is_mini_project', '0');
            });

        }elseif(!$user->showBlocks($campus_id)&&!$campusRound->showClasses ()){
            $query = $campusActivityProgressMapper->query()
            ->where('campus_id', $campus_id)
            ->where('user_id', $user->id)
            ->where('first_success', '!=', NULL)->whereNotIn('task_id', function ($q) {
                $q->select('task_id')->from('mission');
            })->whereNotIn('task_id', function ($q1) {
                $q1->select('task_id')->from('mission_html')->where('is_mini_project', '0');
            });

        }
     
        //Exclude Boosters
        $query=$query->whereIn('task_id',function ($q){
            $q->select('task_id')->from('activity_task')->where('booster_type','!=',1)
              ->whereIn('default_activity_id',function($q2){
                $q2->select('default_activity_id')->from('activity_task');
            });
        });

        return count($query->get());
    }

    public function getAllTasksInRound($campus_id)
    {

        $campusActivityProgressMapper = Analogue::mapper(CampusActivityTask::class);
        $tasks_records = $campusActivityProgressMapper->query()->where('campus_id', $campus_id)->get();
        return count($tasks_records);
    }

    public function getCampusRoundUser($campus_round_id, $unAssignedStudent)
    {
        $schoolCampusMapper = Analogue::mapper(CampusUser::class);
        $schoolCampusUser = $schoolCampusMapper->query()->where('campus_round_id', $campus_round_id)->where('user_id', $unAssignedStudent)->first();

        return $schoolCampusUser;
    }

    public function getRooms($round_class_id)
    {
        $rooms = Analogue::mapper(Room::class);
        $query = $rooms->query()->where('campus_round_class_id', $round_class_id)->get();

        return $query;
    }

    public function getMaterialByRoomId($room_id)
    { }

    public function getAllStudentsInCampuses($school_id, $limit = null, $search = null, $order_by = null, $order_by_type = null, $gmt_difference, $count = null)
    {
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', function ($q) use ($school_id) {
            $q->select('account_id')->from('school')->where('id', $school_id);
        })->whereIn('id', function ($queryx) {
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id', function ($queryy) {
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                });
        });

        if ($search != null) {
            $query = $query->where(function ($queryzz) use ($search, $gmt_difference) {
                $queryzz->whereIn('id', function ($q) use ($search, $gmt_difference) {
                    $q->select('user_id')->from('campus_user')->whereNull('deleted_at')->whereIn('campus_round_id', function ($qq) use ($search, $gmt_difference) {
                        $qq->select('id')->from('campus_round')->whereNull('deleted_at')
                            ->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                            ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp)->whereIn('campus_id', function ($qqq) use ($search) {
                                $qqq->select('id')->from('campus')->whereNull('deleted_at')->whereIn('id', function ($qqqq) use ($search) {
                                    $qqqq->select('campus_id')->from('campus_translation')->whereNull('deleted_at')->where('name', 'like', '%' . $search . '%');
                                });
                            });
                    })->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
                })->orWhere('username', 'like', '%' . $search . '%');
            });
        }

        /*
         *  if($search != null){
            $query = $query->where('username', 'like', '%'.$search.'%')
                ->whereIn('id',function ($q)use($search){
                $q->select('user_id')->from('campus_user')->whereIn('campus_round_id',function($qq)use($search){
                    $qq->select('id')->from('campus_round')->whereIn('campus_id',function ($qqq)use($search){
                        $qqq->select('id')->from('campus')->whereIn('id',function($qqqq)use($search){
                            $qqqq->select('campus_id')->from('campus_translation')->where('name','like', '%'.$search.'%');
                        });
                    });
                });
            });
        }
         *
         *
         * */

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getAllStudentsInCampusesExceptInRunningRounds($school_id, $rounds_id, $round_id, $limit = null, $search = null, $order_by = null, $count = null)
    {
        //dd($rounds_id);
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', function ($q) use ($school_id) {
            $q->select('account_id')->from('school')->where('id', $school_id);
        })->whereIn('id', function ($queryx) {
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id', function ($queryy) {
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                });
        })->whereIn('id', function ($queryz) use ($rounds_id, $round_id) {
            $queryz->select('user_id')->from('campus_user')->whereNotIn('campus_round_id', $rounds_id);
        });
        /**
        ->whereIn('id',function($queryz) use ($rounds_id,$round_id) {
        $queryz->select('user_id')->from('campus_user')/*->where('id','!=',24)->where('campus_round_id','!=',23);*//*->whereNotIn('campus_round_id',$rounds_id);
    });
         */
        if ($search != null) {
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), 'ASC');

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }
    public function checkIfStudentInRunningRounds($user_id, $rounds_ids)
    {
        $userMapper = Analogue::mapper(CampusRound::class);
        $query = $userMapper->whereIn('id', $rounds_ids)->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->first();
        if ($query == null)
            return false;
        else
            return $query;
    }

    public function getAllStudentsNotInRounds($school_id, array $rounds_id, $limit = null, $search = null, $order_by = null, $order_by_type = null, $count = null)
    {
        // dd($rounds_id);
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', function ($q) use ($school_id) {
            $q->select('account_id')->from('school')->where('id', $school_id);
        })
            ->whereIn('id', function ($queryx) {
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->where(function ($queryz) use ($rounds_id) {
                $queryz->whereNotIn('id', function ($queryzz) use ($rounds_id) {
                    $queryzz->select('user_id')->from('campus_user')->whereNull('deleted_at')->whereIn('campus_round_id', $rounds_id);
                });
            });
        /*->orWhereNotIn('id',function ($queryzz) {
            $queryzz->select('user_id')->from('campus_user');
        });*/


        if ($search != null) {
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function getAllTeachersInCampuses($school_id, $limit = null, $search = null, $search_for = null, $order_by = null, $order_by_type = null, $count = null)
    {
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id', function ($q) use ($school_id) {
            $q->select('account_id')->from('school')->where('id', $school_id);
        })->whereIn('id', function ($queryx) {
            $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                ->whereIn('role_id', function ($queryy) {
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
                });
        });

        if ($search != null) {
            if ($search_for == null) {
                $query = $query->where(function ($innerQuery) use ($search) {
                    $innerQuery->where('username', 'like', '%' . $search . '%')
                        ->orWhere('email', 'like', '%' . $search . '%');
                });
            } else if ($search_for == "username") {
                $query = $query->where(function ($innerQuery) use ($search) {
                    $innerQuery->where('username', 'like', '%' . $search . '%');
                });
            }
        }
        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();

        return $query;
    }

    public function deleteCampusUser(CampusUser $campusUser)
    {
        //        dd($campusUser);
        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $campusUserMapper->delete($campusUser);

        //DB::delete('delete from campus_user where id=?',[$campusUser->getId()]);

    }

    public function deleteCampusUsers(User $user, $is_student = false)
    {
        $campusUsers  = $user->getAllCampusUsers();

        foreach ($campusUsers as $campusUser) {
            //            if($is_student) {
            //                $round = $campusUser->getCampusRound();
            //                $new_studentsNum=($round->getNoOfStudents())==0?0:$round->getNoOfStudents()-1;
            //                $round->setNoOfStudents($new_studentsNum);
            //                $this->storeCampusRound($round);
            //            }
            $this->deleteCampusUser($campusUser);
        }
    }

    public function getRoomById($room_id)
    {
        $room = Analogue::mapper(Room::class);
        $query = $room->query()->whereId($room_id)->first();
        return $query;
    }

    public function storeRoomUser(RoomUser $roomUser)
    {
        $roomUserMapper = Analogue::mapper(RoomUser::class);
        $roomUserMapper->store($roomUser);
    }

    public function storeRoom(Room $room)
    {
        $roomMapper = Analogue::mapper(Room::class);
        $roomMapper->store($room);
    }

    public function storeMaterial(Material $material)
    {
        $materialMapper = Analogue::mapper(Material::class);
        $materialMapper->store($material);
    }

    public function checkIfActivityInCampus($campus_id, $activity_id, $is_default = true)
    {

        $campusActivityMapper = Analogue::mapper(CampusActivityTask::class);
        $campusActivity = $campusActivityMapper->query()->where('campus_id', $campus_id);
        // ->where('default_activity_id', $activity_id)->first();
        if ($is_default)
            $campusActivity = $campusActivity->where('default_activity_id', $activity_id)->first();
        else
            $campusActivity = $campusActivity->where('activity_id', $activity_id)->first();

        if ($campusActivity != null)
            return true;

        return false;
    }

    public function getTeacherRunningRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getTeacherUpcomingRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('starts_at', '>', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '>', Carbon::today()->addHour(-($gmt_difference))->timestamp);


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getTeacherEndedRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('starts_at', '<', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '<', Carbon::today()->addHour(-($gmt_difference))->timestamp);


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getAllTeacherRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        });


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
   
    public function getAdminRunningRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getAdminUpcomingRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('starts_at', '>', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '>', Carbon::today()->addHour(-($gmt_difference))->timestamp);


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getAdminEndedRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('starts_at', '<', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '<', Carbon::today()->addHour(-($gmt_difference))->timestamp);


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();

        return $query;
    }

    public function getAllAdminRounds($user_id, $gmt_difference, $count = null)
    {
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        });


        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function getClassNotEvaluatedTasks($roundClassId, $account_id, $is_default = true, $count = null)
    {

        if ($is_default == true) {
            $query = DB::table('campus_round_class')->leftJoin('class', 'campus_round_class.class_id', '=', 'class.id')
                ->leftJoin('campus_activity_task', 'class.default_activity_id', '=', 'campus_activity_task.default_activity_id')
                ->leftJoin('campus_activity_progress', 'campus_activity_progress.task_id', '=', 'campus_activity_task.task_id')
                ->leftJoin('user', 'campus_activity_progress.user_id', '=', 'user.id')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNull('class.deleted_at')
                ->whereNull('campus_activity_task.deleted_at')
                //->whereNull('campus_activity_progress.deleted_at')
                ->whereNull('user.deleted_at')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNotIn('campus_activity_progress.task_id', function ($q) {
                    $q->select('task_id')->from('mission_html')->where('is_mini_project', '0')->whereNull('deleted_at');
                })
                ->where('user.account_id', '=', $account_id)
                ->where('campus_round_class.id', $roundClassId)
                ->where('campus_activity_progress.is_evaluated', '0')
                ->selectRaw('distinct campus_activity_progress.*');
        } else {
            $query = DB::table('campus_round_class')->leftJoin('class', 'campus_round_class.class_id', '=', 'class.id')
                ->leftJoin('campus_activity_task', 'class.activity_id', '=', 'campus_activity_task.activity_id')
                ->leftJoin('campus_activity_progress', 'campus_activity_progress.task_id', '=', 'campus_activity_task.task_id')
                ->leftJoin('user', 'campus_activity_progress.user_id', '=', 'user.id')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNull('class.deleted_at')
                ->whereNull('campus_activity_task.deleted_at')
                //->whereNull('campus_activity_progress.deleted_at')
                ->whereNull('user.deleted_at')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNotIn('campus_activity_progress.task_id', function ($q) {
                    $q->select('task_id')->from('mission_html')->where('is_mini_project', '0')->whereNull('deleted_at');
                })
                ->where('user.account_id', '=', $account_id)
                ->where('campus_round_class.id', $roundClassId)
                ->where('campus_activity_progress.is_evaluated', '0')
                ->selectRaw('distinct campus_activity_progress.*');
        }
        // dd($query->count());
        if ($count != null)
            return count($query->get()); //weired bug
        else
            return $query->get();
    }

    public function getStuckStudentsPerClass($roundClassId, $account_id, $is_default = true, $count = null)
    {

        if ($is_default == true) {
            $query = DB::table('campus_round_class')->leftJoin('class', 'campus_round_class.class_id', '=', 'class.id')
                ->leftJoin('campus_activity_task', 'class.default_activity_id', '=', 'campus_activity_task.default_activity_id')
                ->leftJoin('campus_activity_progress', 'campus_activity_progress.task_id', '=', 'campus_activity_task.task_id')
                ->leftJoin('user', 'campus_activity_progress.user_id', '=', 'user.id')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNull('class.deleted_at')
                ->whereNull('campus_activity_task.deleted_at')
                //->whereNull('campus_activity_progress.deleted_at')
                ->whereNull('user.deleted_at')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNotIn('campus_activity_progress.task_id', function ($q) {
                    $q->select('task_id')->from('mission_html')->whereNull('deleted_at');
                })
                ->where('user.account_id', '=', $account_id)
                ->where('campus_round_class.id', $roundClassId)
                ->where('campus_activity_progress.no_of_trials', '>=', '3')
                ->whereNull('campus_activity_progress.first_success')
                ->selectRaw('distinct user.*,campus_activity_progress.created_at,campus_activity_progress.task_id');
            //                ->selectRaw('campus_activity_progress.created_at')
            //                ->selectRaw('distinct campus_activity_progress.task_id');

        } else {
            $query = DB::table('campus_round_class')->leftJoin('class', 'campus_round_class.class_id', '=', 'class.id')
                ->leftJoin('campus_activity_task', 'class.activity_id', '=', 'campus_activity_task.activity_id')
                ->leftJoin('campus_activity_progress', 'campus_activity_progress.task_id', '=', 'campus_activity_task.task_id')
                ->leftJoin('user', 'campus_activity_progress.user_id', '=', 'user.id')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNull('class.deleted_at')
                ->whereNull('campus_activity_task.deleted_at')
                //->whereNull('campus_activity_progress.deleted_at')
                ->whereNull('user.deleted_at')
                ->whereNull('campus_round_class.deleted_at')
                ->whereNotIn('campus_activity_progress.task_id', function ($q) {
                    $q->select('task_id')->from('mission_html')->whereNull('deleted_at');
                })
                ->where('user.account_id', '=', $account_id)
                ->where('campus_round_class.id', $roundClassId)
                ->where('campus_activity_progress.no_of_trials', '>=', '3')
                ->whereNull('campus_activity_progress.first_success')
                ->selectRaw('distinct user.*,campus_activity_progress.created_at,campus_activity_progress.task_id');
            //                ->selectRaw('campus_activity_progress.created_at')
            //                ->selectRaw('distinct campus_activity_progress.task_id');
        }
        if ($count != null)
            return $query->count();
        else
            return $query->get();
    }

    public function getTopStudentsInCampusRound($account, $campus_id, $round_id, $limit)
    {
        $data = DB::table('user')->leftJoin('campus_activity_progress', 'user.id', '=', 'campus_activity_progress.user_id')
            ->leftJoin('campus_user', 'campus_activity_progress.user_id', '=', 'campus_user.user_id')
            ->select(DB::raw('user.id, user.first_name, user.last_name, COALESCE(count(campus_activity_progress.id),0) as tasksSolved'))
            ->where('campus_user.campus_round_id', $round_id)
            ->where('campus_activity_progress.campus_id', $campus_id)
            ->whereNotNull('campus_activity_progress.first_success')
            ->whereNull('user.deleted_at')
            //->whereNull('campus_activity_progress.deleted_at')
            ->where('account_id', $account->getId())
            ->whereIn('user.id', function ($queryx) {
                $queryx->select('user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->groupBy('user.id', 'user.first_name', 'user.last_name')->orderBy(DB::raw('tasksSolved'), 'DESC')->limit($limit)
            ->get();
        return $data;
    }

    public function storeCampusClass(CampusClass $class)
    {
        $classMapper = Analogue::mapper(CampusClass::class);
        $classMapper->store($class);
    }

    public function storeCampusActivityTask(CampusActivityTask $campusTask)
    {
        $taskMapper = Analogue::mapper(CampusActivityTask::class);
        $taskMapper->store($campusTask);
    }

    public function getAllCampuses($campus_ids = [])
    {
        $campusMapper = Analogue::mapper(Campus::class);
        if (count($campus_ids) > 0){
            if($campus_ids[0] != 'empty_school'){
                $query = $campusMapper->query()->whereIn('id', $campus_ids)->get();
                return $query;
            }else{
                return [];
            }
        }else{
            $query = $campusMapper->query()->get();
            return $query;
        }
    }

    public function getProductCoursesIds($product_id)
    {
        $campusProductMapper = Analogue::mapper(CampusProduct::class);
        $data = [];
        $campusProducts = $campusProductMapper->query()->where('product_id',$product_id)->get();
            foreach($campusProducts as $campusProduct){
                array_push($data,$campusProduct->campus_id);
            }
        return $data;
    }

    public function getSupportCampuses($user,$gmt_difference)
    { 
       // dd($gmt_difference);
        $campusMapper = Analogue::mapper(Campus::class);
       
       if ($user->isStudent()) {
          
            $query = $campusMapper->query()->whereIn('id', function ($q1) use ($user,$gmt_difference) {
                $q1->select('campus_id')->from('campus_round')->whereIn('id',function($q3)use($user){
                    $q3->select('campus_round_id')->from('campus_user')->where('user_id', $user->id)->whereNull('deleted_at');
                })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);
     
            });
            
        }
        elseif($user->isTeacher())
        { 
            $query = $campusMapper->query()->whereIn('id', function ($q1) use ($user,$gmt_difference) {
                $q1->select('campus_id')->from('campus_round')->whereIn('id',function($q3)use($user){
                    $q3->select('campus_round_id')->from('campus_user')->where('user_id', $user->id)->whereNull('deleted_at');
              
                });
            });   
       
            }
        elseif($user->isAdmin()) {
            $query = $campusMapper->query();
          
        }
        return $query->get();
    }

    public function getRoundsInCampuses($campus_ids = [])
    {
        $campusMapper = Analogue::mapper(CampusRound::class);
        if (count($campus_ids) > 0)
            $query = $campusMapper->query()->whereIn('campus_id', $campus_ids)->get();
        else
            $query = $campusMapper->query()->get();
        return $query;
    }



    public function searchForCountCampuses($search, $user_id, $gmt_difference)
    {
        $campusMapper = Analogue::mapper(Campus::class);
        $query = $campusMapper->query();

        if ($search != null) {
            // $query = $query->where('name', 'like', '%'.$search.'%')
            //     ->orWhere('description', 'like', '%'.$search.'%');

            $query = $campusMapper->query()->whereIn('id', function ($q1) use ($user_id, $gmt_difference) {
                $q1->select('campus_id')->from('campus_round')

                    ->whereIn('campus_id', function ($q) {
                        $q->select('campus_id')->from('school_campus')->where('locked', 0);
                    })->whereIn('id', function ($q) use ($user_id) {
                        $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
                    })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                    ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);
            })->whereIn('id', function ($q) use ($search) {

                $q->select('campus_id')->from('campus_translation')
                    ->where('name', 'like', '%' . $search . '%')
                    ->orWhere('description', 'like', '%' . $search . '%');;
            });
        }

        $query = $query->count();

        return $query;
    }

    public function getAllClasses()
    {
        $campusMapper = Analogue::mapper(CampusClass::class);
        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('id')->from('campus')->whereNull('deleted_at');
        })->get();
        return $query;
    }

    public function getAllTasks()
    {
        $campusMapper = Analogue::mapper(CampusActivityTask::class);
        $query = $campusMapper->query()->get();
        return $query;
    }

    public function getCampusClasses($campus_id)
    {
        $campusMapper = Analogue::mapper(CampusClass::class);
        $query = $campusMapper->query()->where('campus_id', $campus_id)->get();
        return $query;
    }




    public function getRoundSubmissions($campus_id, $round_id, $from_time, $to_time)
    {
        $studentProgressMapper = DB::table('campus_activity_progress')
            ->select(DB::raw('count(`campus_activity_progress`.`user_id`) as number, Date(updated_at) as time'))
            ->where('campus_id', $campus_id)
            ->whereIn('campus_activity_progress.user_id', function ($queryx) {
                $queryx->select('campus_activity_progress.user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->whereIn('campus_activity_progress.user_id', function ($queryx) use ($round_id) {
                $queryx->select('user_id')->from('campus_user')
                    ->where('campus_round_id', $round_id);
            })
            ->whereNotIn('campus_activity_progress.task_id', function ($queryz) {
                $queryz->select('task_id')->from('mission_html')->where('is_mini_project', '0')->whereNull('deleted_at');
            });

        return $studentProgressMapper->whereBetween(DB::raw('Date(updated_at)'), [$from_time, $to_time])->groupBy(DB::raw('Date(updated_at)'))->get();
    }

    public function getRoundStudentsSubmissions($campus_id, $round_id, $from_time, $to_time)
    {
        $studentProgressMapper = DB::table('campus_activity_progress')
            ->select(DB::raw('count(DISTINCT(`campus_activity_progress`.`user_id`)) as number, Date(updated_at) as time'))
            ->where('campus_id', $campus_id)
            ->whereIn('campus_activity_progress.user_id', function ($queryx) {
                $queryx->select('campus_activity_progress.user_id')->from('user_role')->whereNull('deleted_at')
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->whereIn('campus_activity_progress.user_id', function ($queryx) use ($round_id) {
                $queryx->select('user_id')->from('campus_user')
                    ->where('campus_round_id', $round_id);
            });

        return $studentProgressMapper->whereBetween(DB::raw('Date(updated_at)'), [$from_time, $to_time])->groupBy(DB::raw('Date(updated_at)'))->get();
    }


    public function getCampusRoundTeachers($round_id)
    {
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->whereIn('id', function ($q) use ($round_id) {
            $q->select('user_id')->from('campus_user')->where('campus_round_id', $round_id)
                ->whereIn('role_id', function ($queryy) {
                    $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'teacher');
                });
        })->get();

        return $query;
    }

    public function getStudentSolvedTaskInRound($round, $activity_id, $task_id)
    {
        $userMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $userMapper->query()->whereNull('deleted_at')->where('campus_id', $round->getCampus_id())->where('default_activity_id', $activity_id)->where('task_id', $task_id)
            ->whereIn('user_id', function ($q) use ($round) {
                $q->select('user_id')->from('campus_user')->where('campus_round_id', $round->getId())
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })->get();
            
        return $query;
    }

    public function getMissionAngularUserAnswerByName($campus_activity_progress_id, $name)
    {
        $userMapper = Analogue::mapper(MissionAngularUserAnswer::class);
        $query = $userMapper->query()->whereNull('deleted_at')->where('campus_activity_progress_id', $campus_activity_progress_id)->where('name', $name)
            ->first();
            
        return $query;
    }

    public function getMissionAngularUserAnswers($campus_activity_progress_id)
    {
        $userMapper = Analogue::mapper(MissionAngularUserAnswer::class);
        $query = $userMapper->query()->whereNull('deleted_at')->where('campus_activity_progress_id', $campus_activity_progress_id)
            ->get();
            
        return $query;
    }

    public function getAllStudentsSolvingTaskInCampus($account_id,$campus_id, $activity_id, $task_id)
    {
        $userMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $userMapper->query()->whereNull('deleted_at')->where('campus_id', $campus_id)->where('default_activity_id', $activity_id)->where('task_id', $task_id)->whereNotNull('first_success')
            ->whereIn('user_id', function ($q){
                $q->select('user_id')->from('campus_user')
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })
            ->whereIn('user_id', function ($qq)use($account_id){
                $qq->select('id')->from('user')->where('account_id',$account_id);
            })->get();

        return $query;
    }
    
    public function checkStudentSolvedAllSubModuleTasks($student_id, $default_activity_id, $tasks_ids)
    {
        $userMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $userMapper->query()->whereNull('deleted_at')
            ->whereIn('task_id', $tasks_ids)
            ->where('user_id', $student_id)
            ->where('default_activity_id', $default_activity_id)
            ->whereNull('first_success')
            ->whereNull('deleted_at')
            ->count();
        
        return $query == 0 ? 1 : 0;
    }

    public function checkIfStudentSolveTask($student_id,$campus_id, $activity_id, $task_id)
    {
        $userMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $userMapper->query()->whereNull('deleted_at')->where('campus_id', $campus_id)->where('default_activity_id', $activity_id)->where('task_id', $task_id)->where('user_id', $student_id)->whereNotNull('first_success')
            ->whereIn('user_id', function ($q){
                $q->select('user_id')->from('campus_user')
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })->first();

        return $query;
    }
    public function getStudentSolvedTasksInRoundClass($round, $activity_id)
    {
        $userMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $userMapper->query()->whereNull('deleted_at')->where('campus_id', $round->getCampus_id())->where('default_activity_id', $activity_id)
            ->whereIn('user_id', function ($q) use ($round) {
                $q->select('user_id')->from('campus_user')->where('campus_round_id', $round->getId())
                    ->whereIn('role_id', function ($queryy) {
                        $queryy->select('id')->from('role')->whereNull('deleted_at')->where('name', 'student');
                    });
            })->orderBy('user_id')->get();

        return $query;
    }

    public function getRecentActivityRounds($user_id, $gmt_difference)
    {
        $gmt_difference = (float) $gmt_difference;
        $date_limit = (Carbon::today()->addHour(-($gmt_difference))->addDays(4)->timestamp);
        $campusMapper = Analogue::mapper(CampusRound::class);

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
        })->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '<', $date_limit);
        return $query->get();
    }

    public function getRecentActivityClasses($user_id, $gmt_difference)
    {
        $gmt_difference = (float) $gmt_difference;
        $date_limit = (Carbon::today()->addHour(-($gmt_difference))->addDays(3)->timestamp);
        $campusMapper = Analogue::mapper(CampusRoundClass::class);
        $query = $campusMapper->query()->whereIn('campus_round_id', function ($q) use ($user_id) {
            $q->select('id')->from('campus_round')->whereIn('campus_id', function ($q) {
                $q->select('campus_id')->from('school_campus')->where('locked', 0);
            })->whereIn('id', function ($q) use ($user_id) {
                $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
            });
        })->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp)->where('ends_at', '<', $date_limit);

        return $query->get();
    }

    public function getRecentActivityTasks($user_id, $gmt_difference)
    {
        $gmt_difference = (float) $gmt_difference;
        $date_limit = (Carbon::today()->addHour(-($gmt_difference))->addDays(1)->timestamp);
        $campusMapper = Analogue::mapper(CampusRoundActivityTask::class);
        $query = $campusMapper->query()->whereIn('campus_round_id', function ($q) use ($user_id) {
            $q->select('id')->from('campus_round')->whereIn('campus_id', function ($q) {
                $q->select('campus_id')->from('school_campus')->where('locked', 0);
            })->whereIn('id', function ($q) use ($user_id) {
                $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id);
            });
        })->where('due_date', '>=', Carbon::today()->timestamp)->where('due_date', '<', $date_limit);
        return $query->get();
    }

    public function getStudentsProgressInRound($user_id, $account_id, $round_id, $campus_id)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)->whereIn('user_id', function ($q) use ($account_id, $round_id) {
            $q->select('id')->from('user')->where('account_id', $account_id)->whereNull('deleted_at')->whereIn('id', function ($qq) use ($round_id) {

                $qq->select('user_id')->from('campus_user')->whereIn('campus_round_id', function ($qqq) use ($round_id) {
                    $qqq->select('id')->from('campus_round')->whereNull('deleted_at')->whereId($round_id);
                });
            });
        });


        return $query->get();
    }

    public function checkStudentInRunningCampuses($user_id, $rounds_id, $round_id, $gmt_difference)
    {

        $campusMapper = Analogue::mapper(CampusUser::class);
        $query = $campusMapper->query()->where('user_id', $user_id)->whereIn('campus_round_id',  function ($q) use ($rounds_id, $round_id, $gmt_difference) {
            $q->select('id')->from('campus_round')->whereIn('id', $rounds_id)->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);
        })->get();
        //return $query;
        if ($query != null && count($query) > 0)
            return true;

        return false;
    }

    public function getMaterialsClass($round_class_id)
    {
        $materials = Analogue::mapper(Material::class);
        $query = $materials->query()->where('campus_round_class_id', $round_class_id)->get();

        return $query;
    }

    public function checkRoomNameExists($roundClass_id, $name, $room_id = null)
    {
        $roomMapper = Analogue::mapper(Room::class);
        $query = $roomMapper->query()->where('campus_round_class_id', $roundClass_id)->where('id', '!=', $room_id)->where('name', $name)->get();
        if (count($query) == 0)
            return false;
        else
            return true;
    }
    public function checkRoomTimeExists($roundClass_id, $name, $starts_at, $room_id = null)
    {
        $roomMapper = Analogue::mapper(Room::class);
        $query = $roomMapper->query()->where('campus_round_class_id', $roundClass_id)->where('id', '!=', $room_id)->where('name', '!=', $name)
            ->where('starts_at', $starts_at)->get();
        if (count($query) == 0)
            return false;
        else
            return true;
    }
    public function getMaterialById($materialId)
    {
        $materialMapper = Analogue::mapper(Material::class);
        return $materialMapper->query()->where('id', $materialId)->first();
    }
    public function deleteMaterial(Material $material)
    {
        $materialMapper = Analogue::mapper(Material::class);
        $materialMapper->delete($material);
    }
    public function getRoomsByMaterialId($materialId)
    {
        $roomMapper = Analogue::mapper(Room::class);
        $query = $roomMapper->query()->where('file_id', $materialId)->get();
        return $query;
    }

    public function getEditorMissionByQuiz($campus_id, $activity_id, $quiz_id, $is_default)
    {
        $editorMapper = Analogue::mapper(MissionEditor::class);
        $query = $editorMapper->query()->where('quiz_id', $quiz_id)->whereIn('task_id', function ($q) use ($campus_id, $is_default, $activity_id) {
            $q->select('task_id')->from('campus_activity_task')->where('campus_id', $campus_id);
            if ($is_default)
                $q->where('default_activity_id', $activity_id);
            else
                $q->where('activity_id', $activity_id);
        })->first();

        return $query;
    }

    public function getAngularMissionByQuiz($campus_id, $activity_id, $quiz_id, $is_default)
    {
        $editorMapper = Analogue::mapper(MissionAngular::class);
        $query = $editorMapper->query()->where('quiz_id', $quiz_id)->whereIn('task_id', function ($q) use ($campus_id, $is_default, $activity_id) {
            $q->select('task_id')->from('campus_activity_task')->where('campus_id', $campus_id);
            if ($is_default)
                $q->where('default_activity_id', $activity_id);
            else
                $q->where('activity_id', $activity_id);
        })->first();

        return $query;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    public function getStudentSolvedMissionsInClass($student_id, $campus_id, $activity_id, $is_default = true, $count = null)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $student_id)
            ->whereNotNull('first_success');
        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        $query = $query->whereNotIn('campus_activity_progress.task_id', function ($subQuery) {
            $subQuery->select('task_id')->from('mission_html')->whereNull('deleted_at')->where('is_mini_project', 0);
        });

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
    public function getStudentCompletedClassesInClass($student_id, $campus_id, $activity_id, $is_default = true, $count = null)
    {
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $student_id)
            ->whereNotNull('first_success');
        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        $query = $query->whereIn('campus_activity_progress.task_id', function ($subQuery) {
            $subQuery->select('task_id')->from('mission_html')->whereNull('deleted_at')->where('is_mini_project', 0);
        });

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
    public function getAllMissionsInClass($campus_id, $activity_id, $is_default = true, $count = null)
    {
        $campusTasks = Analogue::mapper(CampusActivityTask::class);
        $query = $campusTasks->query()->where('campus_id', $campus_id);

        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        $query = $query->whereNotIn('task_id', function ($subQuery) {
            $subQuery->select('task_id')->from('mission_html')->whereNull('deleted_at')->where('is_mini_project', 0);
        });

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }
    public function getAllClassesInClass($campus_id, $activity_id, $is_default = true, $count = null)
    {
        $campusTasks = Analogue::mapper(CampusActivityTask::class);
        $query = $campusTasks->query()->where('campus_id', $campus_id);

        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id);
        else
            $query = $query->where('activity_id', $activity_id);

        $query = $query->whereIn('task_id', function ($subQuery) {
            $subQuery->select('task_id')->from('mission_html')->whereNull('deleted_at')->where('is_mini_project', 0);
        });

        if ($count != null)
            $query = $query->count();
        else
            $query = $query->get();
        return $query;
    }

    public function getAllColorPalettes()
    {
        $colorPaletteMapper = Analogue::mapper(ColorPalette::class);
        $query = $colorPaletteMapper->get();
        return $query;
    }

    public function getCampusClassId($campus_id, $activity_id, $is_default)
    {

        $campusTaskMapper = Analogue::mapper(CampusClass::class);
        $query = $campusTaskMapper->query()->where('campus_id', $campus_id);

        if ($is_default)
            $query = $query->where('default_activity_id', $activity_id)->whereNull('deleted_at')->get();
        else
            $query = $query->where('activity_id', $activity_id)->whereNull('deleted_at')->get();

        return $query;
    }

    public function getlessonsSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent)
    {
        
        $vaultColumns = implode(',', SearchVault::searchable());
        
        $search = '\'"'.str_replace('+', ' ', $search).'"\''; 

            $query = DB::table('search_vault')
            ->where('type','module')
            ->Join('campus', 'campus.id' , '=', 'search_vault.campus_id')
            ->Join('campus_round', 'campus_round.campus_id' , '=', 'campus.id')
            ->Join('campus_user', 'campus_user.campus_round_id' , '=', 'campus_round.id')
            ->where('campus_user.user_id',$user_id)
            ->whereNull('campus_round.deleted_at')
            ->whereRaw("MATCH ({$vaultColumns}) AGAINST (? IN NATURAL LANGUAGE MODE) ", [$search]);

      
        

        if ($count == false) {
            if ($limit !== null)
                return $query->paginate($limit);
            else
                return $query->get();
        } elseif ($count == true) {
            $query = $query->count();
            return $query;
        }
    }
    

    public function _getlessonsSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent)
    {
        $columns = implode(',', ActivityTranslation::searchable());

        // $classMapper = Analogue::mapper(CampusClass::class);
        $classMapper = DB::table('class')->select('campus_round.campus_id','campus_round.id as round_id','class.default_activity_id');

        if ($isStudent == true) {
                $query = $classMapper->Join('campus_round', 'class.campus_id' , '=', 'campus_round.campus_id') 

                    ->whereIn('campus_round.campus_id', function ($q) {
                        $q->select('campus_id')->from('school_campus')->where('locked', 0);
                    })->whereIn('campus_round.id', function ($q) use ($user_id) {
                        $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
                    })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                    ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);
        } else {
                $query = $classMapper->Join('campus_round', 'class.campus_id' , '=', 'campus_round.campus_id') 
                 
                    ->whereIn('campus_round.campus_id', function ($q) {
                        $q->select('campus_id')->from('school_campus')->where('locked', 0);
                    })->whereIn('campus_round.id', function ($q) use ($user_id) {
                        $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
                    });
        }

        $query = $query->whereIn('class.default_activity_id', function ($q) use ($search, $columns) {
            $q->select('id')->from('default_activity')->whereIn('id', function ($qq) use ($search, $columns) {
                $qq->select('default_activity_id')->from('activity_translation')
                    ->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search)); //by default it orders by relevance score(the more it matches the searchable keyword the earlier it comes )
                // ->where('name', 'like', '%'.$search.'%')
                // ->orWhere('description', 'like', '%'.$search.'%');
            });
        });

        if ($count == false) {
            if ($limit !== null)
                return $query->paginate($limit);
            else
                return $query->get();
        } elseif ($count == true) {
            $query = $query->count();
            return $query;
        }
    }

    protected function fullTextWildcards($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['-',  '<', '>', '@', '(', ')', '~', '+', '*'];
        $term = str_replace($reservedSymbols, '', $term);

        $words = explode(' ', $term);

        foreach ($words as $key => $word) {

            if (strlen($word) >= 3) {
                $words[$key] = '+' . $word . '*';
            }
        }

        $searchTerm = implode(' ', $words);

        return $searchTerm;
    }

    public function getProfessionalCampusesSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent)
    {
        $vaultColumns = implode(',', SearchVault::searchable());
        
        $search = '\'"'.str_replace('+', ' ', $search).'"\''; 

        if ($isStudent == true) {
            $query = DB::table('search_vault')
            ->where('type','course')
            ->Join('campus', 'campus.id' , '=', 'search_vault.campus_id')
            ->Join('campus_round', 'campus_round.campus_id' , '=', 'campus.id')
            ->Join('campus_user', 'campus_user.campus_round_id' , '=', 'campus_round.id')
            ->where('campus_round.starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
            ->where('campus_round.ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
            ->where('campus_user.user_id',$user_id)
            ->whereNull('campus_round.deleted_at')
            ->whereRaw("MATCH ({$vaultColumns}) AGAINST (? IN NATURAL LANGUAGE MODE) ", [$search]);
        }else{
            $query = DB::table('search_vault')
            ->where('type','course')
            ->Join('campus', 'campus.id' , '=', 'search_vault.campus_id')
            ->Join('campus_round', 'campus_round.campus_id' , '=', 'campus.id')
            ->Join('campus_user', 'campus_user.campus_round_id' , '=', 'campus_round.id')
            ->where('campus_user.user_id',$user_id)
            ->whereNull('campus_round.deleted_at')
            ->whereRaw("MATCH ({$vaultColumns}) AGAINST (? IN NATURAL LANGUAGE MODE) ", [$search]);
        }

        // dd($query->get());
         //Return count when count is true
         if ($count == true)
            return $query->get()->count();
         if ($limit !== null)
            return $query->paginate($limit);
         else
            return $query->get();
    }

    public function _getProfessionalCampusesSearch($search, $limit, $user_id, $gmt_difference, $count, $isStudent)
    {

        $columns = implode(',', CampusTranslation::searchable());


        // $campusMapper = Analogue::mapper(Campus::class);
        $campusMapper = DB::table('campus');

        if ($isStudent == true) {
            $query = $campusMapper->Join('campus_round', 'campus.id' , '=', 'campus_round.campus_id')

                    ->whereIn('campus_round.campus_id', function ($q) {
                        $q->select('campus_id')->from('school_campus')->where('locked', 0);
                    })->whereIn('campus_round.id', function ($q) use ($user_id) {
                        $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
                    })->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                    ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);
        } else {
            $query = $campusMapper->Join('campus_round', 'campus.id' , '=', 'campus_round.campus_id')

                    ->whereIn('campus_round.campus_id', function ($q) {
                        $q->select('campus_id')->from('school_campus')->where('locked', 0);
                    })->whereIn('campus_round.id', function ($q) use ($user_id) {
                        $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
                    });
        }

        $query = $campusMapper->whereIn('campus.id', function ($q) use ($search, $columns) {

            $q->select('campus_id')->from('campus_translation')
                ->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search)); //by default it orders by relevance score(the more it matches the searchable keyword the earlier it comes )
            //->where('name', 'like', '%'.$search.'%')
            //->orWhere('description', 'like', '%'.$search.'%');;
        });


        if ($count == false) {
            if ($limit !== null)
                return $query->paginate($limit);
            else
                return $query->get();
        } elseif ($count == true) {
            $query = $query->count();
            return $query;
        }
    }

    public function searchForTasks($search, $limit, $user_id, $user_role, $gmt_difference, $count = false){
        $vaultColumns = implode(',', SearchVault::searchable());
        
        $search = '\'"'.str_replace('+', ' ', $search).'"\''; 

        $query = DB::table('search_vault')

        ->Join('campus', 'campus.id' , '=', 'search_vault.campus_id')
        ->Join('campus_round', 'campus_round.campus_id' , '=', 'campus.id')
        ->Join('campus_user', function($userInROundJoin) use($user_id){
            $userInROundJoin->on('campus_user.campus_round_id' , '=', 'campus_round.id')
            ->where('campus_user.user_id','=',$user_id);
            
        })
        ->where('campus_round.starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
        ->where('campus_round.ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
        ->where('campus_user.user_id',$user_id)
        ->whereNull('campus_round.deleted_at')
        ->whereRaw("MATCH ({$vaultColumns}) AGAINST (? IN NATURAL LANGUAGE MODE) ", [$search]);

        if($user_role->first()->name == 'student'){
            $query=$query->where(function($subQuery)use($user_id){
                $subQuery->whereExists(function ($campusActivityTaskProgressQuery)use($user_id) {
                    $campusActivityTaskProgressQuery->select('task_id')
                            ->from('campus_activity_progress')
                            
                            ->where('campus_activity_progress.task_id' , '=', 'search_vault.task_id')
                            ->where('campus_activity_progress.default_activity_id' , '=', 'search_vault.activity_id')
                            ->where('campus_activity_progress.campus_id' , '=', 'search_vault.campus_id')
                            ->where('campus_activity_progress.user_id','=',$user_id);

                

                })->orWhereExists(function($activityTasksQuery){
                    $activityTasksQuery->select('task_id')
                    ->from('activity_task')
                    ->where('activity_task.task_id','=','search_vault.task_id')
                    ->where('activity_task.default_activity_id','=','search_vault.activity_id')
                    ->where('activity_task.order','=',1);

                });
            });

            
        }

         //Return count when count is true
         if ($count == true)
            return $query->get()->count();
         if ($limit !== null)
            return $query->paginate($limit);
         else
            return $query->get();
        
    }

     public function _searchForTasks($search, $limit, $user_id, $user_role, $gmt_difference, $count = false)
    {
    
        $missionColumns = implode(',', MissionTranslation::searchable());
        $missionHtmlColumns = implode(',', MissionHtmlTranslation::searchable());
        $mcqQuestionColumns = implode(',', McqQuestionTranslation::searchable());
        $choiceQuestionColumns = implode(',', ChoiceTranslation::searchable());
        $dragChoiceColumns = implode(',', DragChoiceTranslation::searchable());
        $matchKeyColumns = implode(',', MatchKeyTranslation::searchable());
        $matchValueColumns = implode(',', MatchValueTranslation::searchable());
        $dropdownQuestionColumns = implode(',', DropdownQuestionTranslation::searchable());
        $dropdownChoiceColumns = implode(',', DropdownChoiceTranslation::searchable());
        $missionEditorColumns = implode(',', MissionEditorTranslation::searchable());
        $missionCodingColumns = implode(',', MissionCodingTranslation::searchable());
        $quizColumns = implode(',', QuizTranslation::searchable());
        $missionHtmlStepsColumns = implode(',', MissionHtmlStepsTranslation::searchable());


           $query = DB::table('campus_activity_task')
            ->Join('campus_round_activity_task', 'campus_round_activity_task.campus_activity_task_id' , '=', 'campus_activity_task.id')


            ->whereIn('campus_activity_task.task_id', function ($q1) 
                use ($search,$missionColumns,$missionHtmlColumns,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownQuestionColumns,$dropdownChoiceColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                 $q1->select('id')->from('task')->whereIn('id', function ($q2) 
                use ($search,$missionColumns,$missionHtmlColumns,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownQuestionColumns,$dropdownChoiceColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                    $q2->select('task_id')->from('mission')->whereIn('id', function ($q3) 
                        use ($search,$missionColumns,$missionHtmlColumns,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownQuestionColumns,$dropdownChoiceColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                            $q3->select('mission_id')->from('mission_translation')
                        // ->whereRaw("MATCH ({$missionColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search)) //by default it orders by relevance score(the more it matches the searchable keyword the earlier it comes )
                        //->whereRaw("MATCH ({$missionColumns}) AGAINST ('REGEXP \b(\b$search\b(?![^<>]*>))\b IN NATURAL LANGUAGE MODE')")    
                        //->orWhere('description', 'like', '%' . $search . '%');
                        ->whereRaw("(fnStripTags(description)COLLATE utf8_general_ci) like ? ", '%' . $search . '%')
                        ->orwhere('title', 'like', '%' . $search . '%');


                    });
                })
                ->orwhereIn('id', function ($q2)
                     use ($search,$missionHtmlColumns,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                    $q2->select('task_id')->from('mission_html')->whereIn('id', function ($q3) 
                        use ($search,$missionHtmlColumns,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                        $q3->select('mission_html_id')->from('mission_html_translation')
                        ->whereRaw("MATCH ({$missionHtmlColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search)) //by default it orders by relevance score(the more it matches the searchable keyword the earlier it comes )

                        ->orwhere('title', 'like', '%' . $search . '%')
                            ->orwhereIn('mission_html_id', function ($q4) 
                                use ($search,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                                $q4->select('mission_html_id')->from('mission_html_steps')->whereIn('id', function ($q5) 
                                    use ($search,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns,$missionHtmlStepsColumns) {
                                    $q5->select('mission_html_steps_id')->from('mission_html_steps_translation')

                                    //->whereRaw("MATCH ({$missionHtmlStepsColumns}) AGAINST ('REJEXP \b(\b$search\b(?![^<>]*>))\b IN NATURAL LANGUAGE MODE')")
                                    ->whereRaw("(fnStripTags(step_editor)COLLATE utf8_general_ci) like ? ", '%' . $search . '%')

                                    ->orwhere('title', 'like', '%' . $search . '%')
                                    ->orwhereIn('mission_html_steps_id', function ($q6) 
                                        use ($search,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                        $q6->select('html_step_id')->from('question_task')->whereIn('question_id', function ($qq) 
                                            use ($search,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                            $qq->select('question_id')->from('mcq_question')->whereIn('id', function ($qq2) 
                                                use ($search,$mcqQuestionColumns,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                 $qq2->select('question_id')->from('mcq_question_translation')
                                                 ->whereRaw("MATCH ({$mcqQuestionColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search)) //by default it orders by relevance score(the more it matches the searchable keyword the earlier it comes )
                                                 ->orwhere('body', 'like', '%' . $search . '%')
                                                ->orwhereIn('question_id', function ($qq3) 
                                                    use ($search,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                $qq3->select('mcq_question_id')->from('choice')->whereIn('id',function ($qq4) 
                                                    use ($search,$choiceQuestionColumns,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                    $qq4->select('choice_id')->from('choice_translation')
                                                    ->whereRaw("MATCH ({$choiceQuestionColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search)) //by default it orders by relevance score(the more it matches the searchable keyword the earlier it comes )
                                                    ->orwhere('body', 'like', '%' . $search . '%');
                                                    });
                                                });
                                            });
                                        })
                                        ->orwhereIn('question_id', function ($qq) use ($search,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                            $qq->select('question_id')->from('drag_question')->whereIn('id', function ($qq2) use ($search,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                $qq2->select('drag_question_id')->from('drag_choice')->whereIn('id', function ($qq3) use ($search,$dragChoiceColumns,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                    $qq3->select('drag_choice_id')->from('drag_choice_translation')
                                                    ->whereRaw("MATCH ({$dragChoiceColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))
                                                    ->orwhere('cell_answer', 'like', '%' . $search . '%');
                                                });
                                            });
                                        })
                                        ->orwhereIn('question_id', function ($qq) use ($search,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                            $qq->select('question_id')->from('match_question')->whereIn('id', function ($qq2) use ($search,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                $qq2->select('match_question_id')->from('match_key')->whereIn('id', function ($qq3) use ($search,$matchKeyColumns,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                    $qq3->select('match_key_id')->from('match_key_translation')
                                                    ->whereRaw("MATCH ({$matchKeyColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))

                                                    ->orwhere('key', 'like', '%' . $search . '%');
                                                })
                                                ->orwhereIn('match_question_id', function ($qq3) use ($search,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                    $qq3->select('match_question_id')->from('match_value')->whereIn('id', function ($qq4) use ($search,$matchValueColumns,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                        $qq4->select('match_value_id')->from('match_value_translation')
                                                        ->whereRaw("MATCH ({$matchValueColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))

                                                        ->orwhere('value', 'like', '%' . $search . '%');
                                                    });

                                                });  
                                            });
                                        })
                                        ->orwhereIn('question_id', function ($qq) use ($search,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                            $qq->select('question_id')->from('dropdown_question')->whereIn('id', function ($qq2) use ($search,$dropdownChoiceColumns,$dropdownQuestionColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                $qq2->select('dropdown_question_id')->from('dropdown_question_translation')
                                                ->whereRaw("MATCH ({$dropdownQuestionColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))
                                                ->orwhere('title_description', 'like', '%' . $search . '%')
                                                ->orwhereIn('dropdown_question_id', function ($qq3) use ($search,$dropdownChoiceColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                    $qq3->select('dropdown_question_id')->from('dropdown')->whereIn('id', function ($qq4) use ($search,$dropdownChoiceColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                        $qq4->select('dropdown_id')->from('dropdown_choice')->whereIn('id', function ($qq5) use ($search,$dropdownChoiceColumns,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                                                            $qq5->select('dropdown_choice_id')->from('dropdown_choice_translation')
                                                            ->whereRaw("MATCH ({$dropdownChoiceColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))
                                                           ->orwhere('choice', 'like', '%' . $search . '%');

                                                        });

                                                    });
                                                });
                                            });
                                        });

                                    }); 
                                }); 
                            });     
                    });
                })
                ->orwhereIn('id', function ($q2) use ($search,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                    $q2->select('task_id')->from('quiz')->whereIn('id', function ($q3) use ($search,$quizColumns,$missionCodingColumns,$missionEditorColumns) {
                        $q3->select('quiz_id')->from('quiz_translation')
                        ->whereRaw("MATCH ({$quizColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))
                      
                        ->orwhere('title', 'like', '%' . $search . '%');
                    });
                })
                ->orwhereIn('id', function ($q2) use ($search,$missionCodingColumns,$missionEditorColumns) {
                    $q2->select('task_id')->from('mission_coding')->whereIn('id', function ($q3) use ($search,$missionCodingColumns,$missionEditorColumns) {
                        $q3->select('mission_coding_id')->from('mission_coding_translation')
                        ->whereRaw("MATCH ({$missionCodingColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))
                        ->orwhere('title', 'like', '%' . $search . '%')
                            ->orWhere('description', 'like', '%' . $search . '%');
                    });
                })
                ->orwhereIn('id', function ($q2) use ($search,$missionEditorColumns) {
                    $q2->select('task_id')->from('mission_editor')->whereIn('id', function ($q3) use ($search,$missionEditorColumns) {
                        $q3->select('mission_editor_id')->from('mission_editor_translation')
                        ->whereRaw("MATCH ({$missionEditorColumns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($search))
                        ->orwhere('title', 'like', '%' . $search . '%')
                            ->orWhere('description', 'like', '%' . $search . '%');
                    });
                });
            })

            ->whereIn('campus_round_activity_task.campus_round_id', function ($q2) use ($user_id, $user_role) {

                        $q2->select('campus_round_id')->from('campus_user')
                            ->when($user_role->first()->name == 'student', function ($q) use ($user_id, $user_role) {
                                return $q->where('user_id', $user_id);
                            });
                }
            )
            ->whereIn('campus_round_activity_task.campus_round_id', function ($q2) use ($user_id, $user_role, $gmt_difference) {

                        $q2->select('id')->from('campus_round')
                            ->when($user_role->first()->name == 'student', function ($q) use ($gmt_difference) {
                                return $q->where('starts_at', '<=', Carbon::today()->addHour(-($gmt_difference))->timestamp)
                                    ->where('ends_at', '>=', Carbon::today()->addHour(-($gmt_difference))->timestamp);
                            });
                }
            );
            
  
        //Return count when count is true

        if ($count == true)
            return $query->get()->count();
        if ($limit !== null)
            return $query->paginate($limit);
        else
            return $query->get();
    }



    public function searchForCountTasks($search)
    {
        $tasksMapper = Analogue::mapper(CampusActivityTask::class);
        // $query = $tasksMapper->query();

        if ($search != null) {
            $query = $tasksMapper->query()
                ->whereIn('task_id', function ($q1) use ($search) {
                    $q1->select('id')->from('task')->whereIn('id', function ($q2) use ($search) {
                        $q2->select('task_id')->from('mission')->whereIn('id', function ($q3) use ($search) {
                            $q3->select('mission_id')->from('mission_translation')->where('title', 'like', '%' . $search . '%')
                                ->orWhere('description', 'like', '%' . $search . '%');
                        });
                    })
                        ->orwhereIn('id', function ($q2) use ($search) {
                            $q2->select('task_id')->from('mission_html')->where('is_mini_project', 1)->whereIn('id', function ($q3) use ($search) {
                                $q3->select('mission_html_id')->from('mission_html_translation')->where('title', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%');
                            });
                        })
                        ->orwhereIn('id', function ($q2) use ($search) {
                            $q2->select('task_id')->from('quiz')->whereIn('id', function ($q3) use ($search) {
                                $q3->select('quiz_id')->from('quiz_translation')->where('title', 'like', '%' . $search . '%');
                            });
                        })
                        ->orwhereIn('id', function ($q2) use ($search) {
                            $q2->select('task_id')->from('mission_coding')->whereIn('id', function ($q3) use ($search) {
                                $q3->select('mission_coding_id')->from('mission_coding_translation')->where('title', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%');
                            });
                        })
                        ->orwhereIn('id', function ($q2) use ($search) {
                            $q2->select('task_id')->from('mission_editor')->whereIn('id', function ($q3) use ($search) {
                                $q3->select('mission_editor_id')->from('mission_editor_translation')->where('title', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%');
                            });
                        });
                });
        }

        $query = $query->count();

        return $query;
    }

    public function getRoomUser($room, $user, $role)
    {
        $roomUserMapper = Analogue::mapper(RoomUser::class);
        $query = $roomUserMapper->query()->where('room_id', $room->id)->where('user_id', $user->id)->where('role_id', $role->id)->first();
        return $query;
    }
    public function getCampusActivityQuestion(CampusActivityProgress $campusActivityProgress, Question $question)
    {
        $campusActivityQuestionMapper = Analogue::mapper(CampusActivityQuestion::class);
        $query = $campusActivityQuestionMapper->query()->where('campus_activity_progress_id', $campusActivityProgress->id)->where('question_id', $question->id)->first();
        //dd($query);
        return $query;
    }
    public function getDragQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, DragCell $dragCell)
    {

        $dragQuestionAnswerMapper = Analogue::mapper(DragQuestionAnswer::class);
        $query = $dragQuestionAnswerMapper->query()->where('campus_activity_question_id', $campusActivityQuestion->id)->where('drag_cell_id', $dragCell->id)->first();
        return $query;
    }

    public function getDropdownQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, Dropdown $dropdown)
    {

        $dropdownQuestionAnswerMapper = Analogue::mapper(DropdownQuestionAnswer::class);
        $query = $dropdownQuestionAnswerMapper->query()->where('campus_activity_question_id', $campusActivityQuestion->id)->where('dropdown_id', $dropdown->id)->first();
        return $query;
    }

    public function getMatchQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, MatchKey $matchKey, MatchValue $matchValue)
    {

        $matchQuestionAnswerMapper = Analogue::mapper(MatchQuestionAnswer::class);
        $query = $matchQuestionAnswerMapper->query()->where('campus_activity_question_id', $campusActivityQuestion->id)->where('key_id', $matchKey->id)->first();
        return $query;
    }

    public function getMcqQuestionAnswer(CampusActivityQuestion $campusActivityQuestion, Choice $choice)
    {

        $mcqQuestionAnswerMapper = Analogue::mapper(McqQuestionAnswer::class);
        $query = $mcqQuestionAnswerMapper->query()->where('campus_activity_question_id', $campusActivityQuestion->id)->where('choice_id', $choice->id)->first();
        return $query;
    }
    public function getOldMcqQuestionAnswer(CampusActivityQuestion $campusActivityQuestion)
    {

        $mcqQuestionAnswerMapper = Analogue::mapper(McqQuestionAnswer::class);
        $query = $mcqQuestionAnswerMapper->query()->where('campus_activity_question_id', $campusActivityQuestion->id)->get();
        return $query;
    }
    public function removeOldMcqQuestionAnswer(McqQuestionAnswer $mcqQuestionAnswer)
    {

        $mcqQuestionAnswerMapper = Analogue::mapper(McqQuestionAnswer::class);
        $query = $mcqQuestionAnswerMapper->delete($mcqQuestionAnswer);
        return $query;
    }



    public function getQuestionAnswerById($campus_activity_question_id, $question_type)
    {
        switch ($question_type) {
            case 'match':
                $mapper = Analogue::mapper(MatchQuestionAnswer::class);
                break;
            case 'multiple_choice':
                $mapper = Analogue::mapper(McqQuestionAnswer::class);
                break;
            case 'sequence_match':
                $mapper = Analogue::mapper(DragQuestionAnswer::class);
                break;
            case 'dropdown':
                $mapper = Analogue::mapper(DropdownQuestionAnswer::class);
                break;
        }
        $query = $mapper->query()->where('campus_activity_question_id', $campus_activity_question_id)->get()->toArray();
        return $query;
    }

    public function deleteQuestionAnswer($campus_activity_question_id, $question_type)
    {
        // dd($question_type);
        switch ($question_type) {
            case 'match':
                $mapper = Analogue::mapper(MatchQuestionAnswer::class);
                break;
            case 'multiple_choice':
                $mapper = Analogue::mapper(McqQuestionAnswer::class);
                break;
            case 'sequence_match':
                $mapper = Analogue::mapper(DragQuestionAnswer::class);
                break;
            case 'dropdown':
                $mapper = Analogue::mapper(DropdownQuestionAnswer::class);
                break;
        }
        $questionAnswer = $mapper->query()->where('campus_activity_question_id', $campus_activity_question_id)->get();
        foreach ($questionAnswer as $answer) {
            $query = $mapper->delete($answer);
        }

        // return $query;
    }

    public function deleteCampusActivityQuestion(CampusActivityQuestion $campusActivityQuestion)
    {
        $campusActivityQuestionMapper = Analogue::mapper(CampusActivityQuestion::class);
        $campusActivityQuestionMapper->delete($campusActivityQuestion);
    }

    public function getCampusDictionaryWords($campus_id)
    {
        $campusDictionaryMapper = Analogue::mapper(DictionaryWord::class);

        $query = $campusDictionaryMapper->query()->where('campus_id', $campus_id)->get();

        return $query;
    }

    public function getCampusDictionaryWordsInActivity($campus_id,$activity_id)
    {
        $campusDictionaryMapper = Analogue::mapper(DictionaryWord::class);

        $query = $campusDictionaryMapper->query()->where('campus_id', $campus_id)->where('default_activity_id', $activity_id)->get();

        return $query;
    }

    public function getStepIdForQuestion($question_id)
    {
        $questionTaskMapper = Analogue::mapper(QuestionTask::class);
        $query = $questionTaskMapper->query()->where('question_id', $question_id)->first()->html_step_id;
        return $query;
    }

    public function getStepById($step_id)
    {
        $MissionHtmlStepsMapper = Analogue::mapper(MissionHtmlSteps::class);
        $query = $MissionHtmlStepsMapper->query()->where('id', $step_id)->first();
        return $query;
    }

    public function getDragChoiceId($campus_activity_question_id, $drag_cell_id)
    {
        $dragQuestionAnswerMapper = Analogue::mapper(DragQuestionAnswer::class);
        $query = $dragQuestionAnswerMapper->query()->where('campus_activity_question_id', $campus_activity_question_id)->where('drag_cell_id', $drag_cell_id)->first();
        if ($query == null) {
            return null;
        }
        return $query->drag_choice_id;
    }

    public function getDragChoiceById($drag_choice_id)
    {
        $dragChoiceMapper = Analogue::mapper(DragChoice::class);
        $query = $dragChoiceMapper->query()->where('id', $drag_choice_id)->first();
        return $query;
    }

    public function getTaskDifficulty($task_id,$activity_id){
        $difficultyMapper = Analogue::mapper(Difficulty::class);
        $difficulty = $difficultyMapper->query()->whereIn('id',function ($q) use ($task_id,$activity_id){
            $q->select('difficulty_id')->from('activity_task')->where('task_id',$task_id)->where('default_activity_id',$activity_id);
        })->first();
        return $difficulty;
    }

    public function getRankLevelById($id){
        $rankLevelMapper = Analogue::mapper(RankLevel::class);
        return $query = $rankLevelMapper->query()->find($id);
    }

    public function getActivityTask($task_id,$activity_id){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        $activityTask = $activityTaskMapper->query()->where('task_id',$task_id)->where('default_activity_id',$activity_id)->first();
        return $activityTask;
    }

    public function getMainTaskByBoosterTask($task_id,$activity_id){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        $activityTask = $activityTaskMapper->query()->where('booster_task_id',$task_id)->where('default_activity_id',$activity_id)->first();
        return $activityTask;
    }

    public function getBoosterTaskByMainTask($task_id,$activity_id){
        $activityTaskMapper = Analogue::mapper(ActivityTask::class);
        $activityTask = $activityTaskMapper->query()->where('default_activity_id',$activity_id)->whereIn('task_id',function($q)use($task_id,$activity_id){
            $q->select('booster_task_id')->from('activity_task')->where('task_id',$task_id)->where('default_activity_id',$activity_id);
        })->first();
        return $activityTask;
    }

    // ============== Student streaks ============== //

    public function resetStudentStreaks($user,$campus_id,$gmt_difference){
        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        $todayDate = Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp;
        
        $oldStreaks = $campusUserStreaksMapper->query()
        ->whereNull('deleted_at')
        ->where('user_id',$user->id)
        ->where('campus_id',$campus_id)
        ->where('date','<=',$todayDate)
        ->get();
        
        $campusUserStreaksMapper->delete($oldStreaks);

    }

    public function checkStudentStreaksLastSeen($user,$campus_id,$gmt_difference){
        $campusUserStatusMapper = Analogue::mapper(CampusUserStatus::class);
        $todayDate = Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp;

        $lastSeen = $campusUserStatusMapper->query()
        ->whereNull('deleted_at')
        ->where('user_id',$user->id)
        ->where('campus_id',$campus_id)
        ->first();

        //Check Last Seen status
        if((!$lastSeen->streaks_lastseen) || ($lastSeen->streaks_lastseen != $todayDate)){
            $lastSeen->streaks_lastseen = $todayDate;
            $campusUserStatusMapper->store($lastSeen);
            return true;
        }else{
            return false;
        }

    }

    public function checkStudentCourseSeen($user,$campus_id){
        $campusUserStatusMapper = Analogue::mapper(CampusUserStatus::class);

        $userStatus = $campusUserStatusMapper->query()
        ->whereNull('deleted_at')
        ->where('user_id',$user->id)
        ->where('campus_id',$campus_id)
        ->first();

        //Check Last Seen status
        if(!$userStatus->course_seen){
            return true;
        }else{
            return false;
        }

    }

    public function getStreaksActiveStatus($user_id,$campus_id,$gmt_difference){
        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        $todayDate = Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp;

        $campusUserStreaks = $campusUserStreaksMapper->query()
        // ->whereIn('id',function($q)use($gmt_difference,$campus_id){
        //     $q->select('id')->from('campus_user_streaks')
        //     ->where('campus_id',$campus_id);
            
            
        // })
        // ->whereIn('id',function($q)use($gmt_difference){
        //         $q->select('id')->from('campus_user_streaks')
        //         ->where('date','=',Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp)
        //         ->orWhere('date','=',Carbon::now()->addHours(-24)->addHours(-($gmt_difference))->startOfDay()->timestamp);
        //     })
        ->where('user_id',$user_id)
        ->where('xp_gained','>',0)->whereNull('deleted_at')
        ->where('campus_id',$campus_id)
        ->where('date','<=',$todayDate);
        $status = $campusUserStreaks->get()->count() > 0 ? true : false; 
        return $status;
    }

    public function getStreaksWeek($user_id,$campus_id,$gmt_difference){

        $week = ['Sat'=>false,'Sun'=>false,'Mon'=>false,'Tue'=>false,'Wed'=>false,'Thu'=>false,'Fri'=>false];
        $campusUserStreaks = Analogue::mapper(CampusUserStreaks::class);
        Carbon::setWeekStartsAt(Carbon::SATURDAY);
        Carbon::setWeekEndsAt(Carbon::FRIDAY);
        $firstDayOfWeek = Carbon::now()->addHours(($gmt_difference))->startOfWeek();
        $lastDayOfWeek = Carbon::now()->addHours(($gmt_difference))->endOfWeek();
        // dd('Today',Carbon::now()->addHours(($gmt_difference)),'First Day of week',$firstDayOfWeek,'Last day of week',$lastDayOfWeek,Carbon::now()->utc);
        $campusUserStreaks = $campusUserStreaks->query()
        ->where('date','>=',$firstDayOfWeek->timestamp)
        ->where('date','<',$lastDayOfWeek->timestamp)
        ->where('user_id',$user_id)
        ->where('campus_id',$campus_id)
        ->where('xp_gained','>',0)->get();
        foreach($campusUserStreaks as $day){
             $dayName = Carbon::createFromTimestamp($day->date)->format('D');
             $week[$dayName] = true;
        }
        // dd($campusUserStreaks);
        return $week;
    }

    private function getLatestCampusUserStreaks($user_id,$campus_id){
        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        $query = $campusUserStreaksMapper->query()
        ->where('user_id',$user_id)
        ->where('campus_id',$campus_id)
        ->whereNull('deleted_at')
        ->latest()->first();
        
        return $query;
    }

    public function getStreaksCount($user_id,$campus_id,$gmt_difference){

        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        //Getting Comparing day in case of user is away for more than 1 day.
        $latestStreak = $this->getLatestCampusUserStreaks($user_id,$campus_id);

        if(!$latestStreak){
            $comparingDay = Carbon::today();
        }else{
            $comparingDay = Carbon::createFromTimestamp($latestStreak->date)->startOfDay();
        }

        $counter = 0;
        $counterFlag = true;
        $query = $campusUserStreaksMapper->query()
        ->whereNull('deleted_at')
        ->where('user_id',$user_id)
        ->where('xp_gained','>',0)
        ->where('campus_id',$campus_id)
        ->orderBy('date','desc')
        ->get();
    
        // dd($query->count());
        if($counterFlag){
            foreach($query as $day){
                
                if($comparingDay->diffInDays(Carbon::createFromTimestamp($day->date)->startOfDay()) < 2){
                    
                     $comparingDay = Carbon::createFromTimestamp($day->date)->startOfDay();
                     $counter++;
                }else{
                    $counter++;
                    $counterFlag = false;
                }
            }
                    
        }    
        
        return $counter;
    }

    public function getLastAddedStreak($user_id,$campus_id){
        $campusUserStreakMapper = Analogue::mapper(CampusUserStreaks::class);
        $query = $campusUserStreakMapper->query()
        ->where('campus_id', $campus_id)
        ->where('user_id',$user_id)
        ->latest()->first();
        return $query;
    }

    public function getLatestStreakPeriod($userId, $campusId){
        $campusUserStreakMapper = Analogue::mapper(CampusUserStreaks::class);
        $query = $campusUserStreakMapper->query()->where('user_id', $userId)->where('campus_id', $campusId)
            ->where('xp_gained','>',0)
            ->where('date', '>=', function ($q1) use ($userId, $campusId){
                $q1->select('date')->from('campus_user_streaks')->whereNull('deleted_at')->where('id', function ($q2) use ($userId, $campusId){
                    $q2->select('latestStreakStartId')->from('campus_user_status')
                        ->whereNull('deleted_at')
                        ->where('user_id', $userId)
                        ->where('campus_id', $campusId)->get();
                })->latest();
            })
            ->orderBy('date', 'DESC')->get();
            
        return $query;
    }

    public function getTodayStudentStreak($user_id,$xp_gained,$gmt_difference){
      $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
      $todayDateTime = Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp;

       $query = $campusUserStreaksMapper->query()
       ->where('user_id',$user_id)
       ->where('xp_gained','>',0)
       ->where('date','>',$todayDateTime)->first();
        
       return $query; 
    }

    public function checkTodayStudentStreak($user_id,$xp_gained,$gmt_difference){
        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        $todayDateTime = Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp;
  
         $query = $campusUserStreaksMapper->query()
         ->where('user_id',$user_id)
         ->where('date','>=',$todayDateTime)->first();
          
         return $query; 
      }

    public function updateStudentStreak($userStreak,$score){
        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        $streak = $campusUserStreaksMapper->query()->find($userStreak->id);
        $streak->xp_gained = $userStreak->xp_gained + $score;
        return $campusUserStreaksMapper->store($streak);
    }

    public function storeStudentStreak($user_id,$score,$campus_id){
        $campusUserStreaksMapper = Analogue::mapper(CampusUserStreaks::class);
        $date = Carbon::now()->startOfDay()->timestamp;
        $streak = new CampusUserStreaks($user_id,$date,$score,$campus_id);
        return $campusUserStreaksMapper->store($streak);
        
    }


//    public function storeCampusUserStatus($user_id,$campus_id,$xp,$rank_level_id){
//        $campusUserStatusMapper = Analogue::mapper(CampusUserStatus::class);
//        $campusUserStatus = new CampusUserStatus($user_id,$campus_id,$xp,$rank_level_id);
//        $campusUserStatusMapper->store($campusUserStatus);
//
//    }


    public function getStudentTasksByStatus($campus_id, $user_id,$status)
    {
        //Status [Completed : true | inProgress : false]

        $campusActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);

        if($status){
            $progress_records = $campusActivityProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $user_id)->where('first_success', '!=', NULL)->get();
        }else{
            $progress_records = $campusActivityProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $user_id)->where('first_success', '=', NULL)->get();
        }
        return $progress_records;
    }

    public function getStudentTasksInClassByStatus($campus_id, $user_id,$status,$activity_id, $is_default = true)
    {
        $campusActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);

        if($status){
            $progress_records = $campusActivityProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $user_id)->where('first_success', '!=', NULL)->whereNull('deleted_at');
        }else{
            $progress_records = $campusActivityProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $user_id)->where('first_success', '=', NULL)->whereNull('deleted_at');
        }

        if ($is_default)
        $progress_records = $progress_records->where('default_activity_id', $activity_id);
        else
        $progress_records = $progress_records->where('activity_id', $activity_id);

        //Ordering
        // $progress_records = $progress_records
        // ->join('activity_task', function($join)
        //         {
        //             $join->on('campus_activity_progress.task_id', '=',  'activity_task.task_id');
        //             $join->on('campus_activity_progress.default_activity_id','=', 'activity_task.default_activity_id');
        //         })
        //         ->select('activity_task.order')
        //         ->lists('activity_task.order')
        //         ;
        // ->Join('activity_task', 'campus_activity_progress.task_id', '=', 'activity_task.task_id')
        // ->select('activity_task.order')
        // ->lists('activity_task.order')
        // ;

        $progress_records = $progress_records->whereIn('task_id',function($q) use($activity_id){
            $q->select('task_id')->from('activity_task')->where('default_activity_id',$activity_id)->orderBy('activity_task.order');
        });
    
        return $progress_records->get();
    }

    public function getMissionPerModuleCount($activity_id,$count=true){
        $campusActivityTaskMapper = Analogue::mapper(CampusActivityTask::class);
        $result = $campusActivityTaskMapper->query()->where('default_activity_id',$activity_id)->get();
        if($count == true)
            return count($result);
        if($count == false)
            return $result;
    }

 

    public function getEnrolledStudentsPerCourseCount($account_id,$campus_id,$status=null,$limit=null, $search = null, $order_by = null, $order_by_type = null){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id',$account_id)->whereIn('id',function($q) use ($campus_id){
            $q->select('user_id')->from('campus_user')->where('role_id',11)->whereIn('campus_round_id',function ($q2) use ($campus_id){
                $q2->select('id')->from('campus_round')->where('campus_id',$campus_id);
            });
        });

        if ($search != null) {
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($status == true)
            $query = count($query->get());

        else
            $query = $query->get();	

        return $query;
        
        // if($status){return count($query->get());}else{return $query;} 
    }

    public function getEnrolledStudentsPerSubmodule($account_id,$campus_id,$module_id,$submodule_id,$count = false,$limit = null,$search = null,$order_by = null,$order_by_type = null){
        $userMapper = Analogue::mapper(User::class);
        $query = $userMapper->query()->where('account_id',$account_id)->whereIn('id',function($q) use ($campus_id, $module_id, $submodule_id){
            $q->select('user_id')->from('campus_user')->where('role_id',11)->whereIn('campus_round_id',function ($q2) use ($campus_id, $module_id, $submodule_id){
                $q2->select('id')->from('campus_round')->where('campus_id',$campus_id)->whereIn('id', function($q3) use ($module_id, $submodule_id){
                    $q3->select('campus_round_id')->from('campus_round_class')->where('class_id', $module_id)->whereIn('class_id', function($q4) use ($submodule_id){
                        $q4->select('class_id')->from('sub_module')->where('id', $submodule_id);
                    });
                });
            });
        });
        if ($search != null) {
            $query = $query->where(function ($innerQuery) use ($search) {
                $innerQuery->where('username', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();	

        return $query;        
    }

    public function getCompletedStudentsPerCourseCount($account_id,$campus_id){
        $userMapper = Analogue::mapper(User::class);
        $result = $userMapper->query()->where('account_id',$account_id)->whereIn('id',function($q) use ($campus_id){
            $q->select('user_id')->from('campus_user')->whereIn('campus_round_id',function ($q2) use ($campus_id){
                $q2->select('id')->from('campus_round')->where('campus_id',$campus_id);
            });
        })->whereIn('id',function ($q3) use($campus_id){
            $q3->select('user_id')->from('campus_activity_progress')->where('campus_id',$campus_id)->where('first_success','!=',Null);
        })->get()->count();

        return $result;
    }

    public function getAllTasksPerModuleCount($campus_id,$default_activity_id){
        $campusActivityTaskMapper = Analogue::mapper(CampusActivityTask::class);
        $result = $campusActivityTaskMapper->query()->where('default_activity_id',$default_activity_id)->where('campus_id',$campus_id)->get();
        return count($result);
    }

    public function getProgressCountforStudentInModule($student_id,$campus_id,$default_activity_id){
        $campusActivityProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $result = $campusActivityProgressMapper->query()
        ->where('user_id',$student_id)
        ->where('campus_id',$campus_id)
        ->where('default_activity_id',$default_activity_id)
        ->get();

        return count($result);
    }

    public function getUserRankLevelInCourse($student_id,$campus_id) {
        $campusUserStatusMapper = Analogue::mapper(CampusUserStatus::class);
        $result = $campusUserStatusMapper->query()
        ->where('campus_id',$campus_id)
        ->where('user_id',$student_id)
        ->first();
        if($result){
            return $result->rank_level_id;
        }else{
            return null;
        }
    }
    
    public function getLearnerCampusUser($user_id,$campus_id){
        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $result = $campusUserMapper->query()->where('user_id',$user_id)->whereIn('campus_round_id',function ($q) use ($campus_id){
            $q->select('id')->from('campus_round')->whereNull('deleted_at');
        })->orderBy('id', 'desc')->first();

        return $result;
    }

    public function getCampusModules($campus_id,$limit = null, $search = null, $order_by = null, $order_by_type = null, $count = null)
    {
        
        $campusMapper = Analogue::mapper(CampusClass::class);
        $query = $campusMapper->query()->where('campus_id', $campus_id);

        if ($search != null) {
            $query = $query->whereIn('id',function ($innerQuery) use ($search) {
                $innerQuery->select('class_id')->from('class_translation')
                ->where('name', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();	
            
        return $query;
    }

    public function getModuleSubmodules($module_id,$limit = null, $search = null, $order_by = null, $order_by_type = null, $count = null)
    {
        
        $campusMapper = Analogue::mapper(Submodule::class);
        $query = $campusMapper->query()->where('class_id', $module_id);

        if ($search != null) {
            $query = $query->whereIn('id',function ($innerQuery) use ($search) {
                $innerQuery->select('sub_module_id')->from('sub_module_translation')
                ->where('name', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();	
            
        return $query;
    }

    public function getRoundLearners($round_id, $limit=null, $search=null,$order_by=null,$order_by_type=null,$count=null){
        $campusUserMapper = Analogue::mapper(CampusUser::class);
        $query = $campusUserMapper->query()->where('campus_round_id',$round_id)->whereIn('user_id',function($q){
            $q->select('user_id')->from('user_role')->whereIn('role_id',function($q2){
                $q2->select('id')->from('role')->where('name','student');
            });
        });
        
        if ($search != null) {
            $query = $query->whereIn('user_id',function ($innerQuery) use ($search) {
                $innerQuery->select('id')->from('user')
                ->where('username', 'like', '%' . $search . '%');
            });
        }

        if ($order_by != null)
            $query = $query->orderBy(DB::raw($order_by), $order_by_type);

        if ($limit != null)
            $query = $query->paginate($limit);

        else if ($count == true)
            $query = $query->count();

        else
            $query = $query->get();	
            
        return $query;
    }

    public function toggleHideBlocks($learnerStatus){
        $campusUserStatusMapper = Analogue::mapper(CampusUserStatus::class);
        if($learnerStatus->hide_blocks){
            $learnerStatus->hide_blocks = false;
        }else{
            $learnerStatus->hide_blocks = true;
        }
        $campusUserStatusMapper->store($learnerStatus);
    }

    public function getAllScoringConstants(){
        $scoringConstantsMapper = Analogue::mapper(ScoringConstant::class);
        return $scoringConstantsMapper->all();
    }

    public function getScoringConstant($constantName){
        $scoringConstantsMapper = Analogue::mapper(ScoringConstant::class);
        return $scoringConstantsMapper->query()->where('name', $constantName)->first();
    }

    public function getMultipleScoringConstants($constantsNames){
        $scoringConstantsMapper = Analogue::mapper(ScoringConstant::class);
        return $scoringConstantsMapper->query()->whereIn('name', $constantsNames)->get();
    }

    public function countRankLevels(){
        $rankLevelMapper = Analogue::mapper(RankLevel::class);
        return $rankLevelMapper->query()->count();
    }

    public function countLevelsByRankId($rank_id){
        $rankLevelMapper = Analogue::mapper(RankLevel::class);
        return $rankLevelMapper->query()->where('rank_id', $rank_id)->count();
    }

    public function getMaxRankLevel($rank_id){
        $rankLevelMapper = Analogue::mapper(RankLevel::class);
        // $order = Order::whereRaw('id = (select max(`id`) from orders)')->get();
        return $rankLevelMapper->query()->where('rank_id', $rank_id)->max('id');
    }

    public function getNextRank($rank_id){
        $rankMapper = Analogue::mapper(Rank::class);
        return $rankMapper->query()->where('id', '>', $rank_id)->orderBy('id')->first();
    }

    public function getTaskById($task_id){
        $taskMapper = Analogue::mapper(Task::class);
        return $taskMapper->query()->where('id',$task_id)->get();
    }

    public function getAllDropdownTranslations(){
        $dropdownTranslationMapper = Analogue::mapper(DropdownQuestionTranslation::class);
        return $dropdownTranslationMapper->all();
    }
    public function getDropdownQuestionById($id){
        $dropdownQuestionMapper = Analogue::mapper(DropdownQuestion::class);
        return $dropdownQuestionMapper->query()->where('id',$id)->first();
    }
    public function deleteDropdown(Dropdown $dropdown)
    {
        $dropdownMapper = Analogue::mapper(Dropdown::class);
        $dropdownMapper->delete($dropdown);
    }

    public function getAllProgress(){
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->whereNull('deleted_at')->get();
        return $query;
    }

    public function getAllLastSeen(){
        $lastSeenMapper = Analogue::mapper(TaskLastSeen::class);
        $query = $lastSeenMapper->query()->whereNull('deleted_at')->get();
        return $query;
    }
    public function getEditorMissions(){
        $missionEditorMapper = Analogue::mapper(MissionEditor::class);
        $query = $missionEditorMapper->query()->whereNull('deleted_at')->get();
        return $query;
    }

    public function getAllMissionCoding($type){
        $missionCodingMapper = Analogue::mapper(MissionCoding::class);
        $query = $missionCodingMapper->query()
        ->whereNull('deleted_at')
        ->whereIn('programming_language_type_id',function($q) use($type){
            $q->select('id')->from('programming_language_type')->where('name',$type);
        })
        ->get();

        return $query;

    }

    public function storeMissionCoding($mission){
        $missionCodingMapper = Analogue::mapper(MissionCoding::class);
        return $missionCodingMapper->store($mission);


    }

    public function getLearnerCurrentRoundinCampus($user,$campus_id){
        $campusMapper = Analogue::mapper(CampusRound::class);
        $user_id = $user->id;

        $query = $campusMapper->query()->whereIn('campus_id', function ($q) {
            $q->select('campus_id')->from('school_campus')->where('locked', 0);
        })->whereIn('id', function ($q) use ($user_id) {
            $q->select('campus_round_id')->from('campus_user')->where('user_id', $user_id)->whereNull('deleted_at');
        })->where('starts_at', '<=', Carbon::today()->timestamp)
            ->where('ends_at', '>=', Carbon::today()->timestamp)
            ->where('campus_id',$campus_id);

        return $query->first();
    }

    //Search vault section 
    
    public function storeSearchVault($searchVault){

        $searchVaultMapper = Analogue::mapper(SearchVault::class);
        return $searchVaultMapper->store($searchVault);
    }

    public function getFirstSubmoduleInModule($module){
        $moduleMapper = Analogue::mapper(Submodule::class);
        return $moduleMapper->query()->where('class_id',$module->getId())->where('order',1)->first();
    }
    public function getSubmoduleInModuleByOrder($module,$order){
        $moduleMapper = Analogue::mapper(Submodule::class);
        return $moduleMapper->query()->where('class_id',$module->getId())->where('order',$order)->first();
    }

    public function getSubmoduleMainTasks($submodule,$count=false){
        $default_activity_id=$submodule->getModule()->getDefaultActivity()->getId();
        $taskMapper = Analogue::mapper(Task::class);
        $query=$taskMapper->query()->whereIn('id',function($q ) use ($default_activity_id){
            $q->select('task_id')->from('activity_task')->where('default_activity_id',$default_activity_id)->whereNull('deleted_at')->where('booster_type','!=',1);
        })
        ->whereIn('id',function($q) use ($submodule){
            $q->select('task_id')->from('sub_module_task')->where('sub_module_id',$submodule->getId())->whereNull('deleted_at');
        });
        if($count===false)
            return $query->get();
        return $query->count();

    }

    public function hasFinishedSubmoduleTasks($user,$campus_id,$submodule){
        $default_activity_id=$submodule->getModule()->getDefaultActivity()->getId();
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $campus_id)->where('user_id', $user->getId())
        ->where('default_activity_id',$default_activity_id)
        ->whereNotNull('first_success')
        ->whereIn('task_id',function($q) use ($submodule){
            $q->select('task_id')->from('sub_module_task')->where('sub_module_id',$submodule->getId())->whereNull('deleted_at');

        })->whereIn('task_id',function($q) use ($default_activity_id){
            $q->select('task_id')->from('activity_task')->where('default_activity_id',$default_activity_id)->whereNull('deleted_at')->where('booster_type','!=',1);

        })->count();

        return $query===$this->getSubmoduleMainTasks($submodule,true);
    }

    public function getUserSolvedSubmoduleMainTasks($user,$round,$submodule,$count=false){
        $default_activity_id=$submodule->getModule()->getDefaultActivity()->getId();
        $taskProgressMapper = Analogue::mapper(CampusActivityProgress::class);
        $query = $taskProgressMapper->query()->where('campus_id', $round->getCampus_id())->where('user_id', $user->getId())
        ->where('default_activity_id',$default_activity_id)
        ->whereNotNull('first_success')
        ->whereIn('task_id',function($q) use ($submodule){
            $q->select('task_id')->from('sub_module_task')->where('sub_module_id',$submodule->getId())->whereNull('deleted_at');

        })->whereIn('task_id',function($q) use ($default_activity_id){
            $q->select('task_id')->from('activity_task')->where('default_activity_id',$default_activity_id)->whereNull('deleted_at')->where('booster_type','!=',1);

        });

        if($count===false)
            return $query->get();
        return $query->count();
    }


    public function getLastMainTaskInSubmodule($submodule){
        $default_activity_id=$submodule->getModule()->getDefaultActivity()->getId();
        $submoduleTaskMapper = Analogue::mapper(SubmoduleTask::class);
        $query=$submoduleTaskMapper->query()->whereIn('task_id',function($q ) use ($default_activity_id){
            $q->select('task_id')->from('activity_task')->where('default_activity_id',$default_activity_id)->whereNull('deleted_at')->where('booster_type','!=',1);
        })
        ->where('sub_module_id',$submodule->getId())->whereNull('deleted_at')->orderBy('order','desc')->first();

        
        return $query->getTask();

    }

    public function getSubmoduleTaskOrder($task_id,$submodule_id){
        $submoduleTaskMapper = Analogue::mapper(SubmoduleTask::class);
        $submoduleTask = $submoduleTaskMapper->where('task_id',$task_id)->where('sub_module_id',$submodule_id)->whereNull('deleted_at')->first();
        return $submoduleTask;
    }

    public function getSubmoduleTaskOrderByOrder($order,$submodule_id){
        $submoduleTaskMapper = Analogue::mapper(SubmoduleTask::class);
        $submoduleTask = $submoduleTaskMapper->where('order',$order)->where('sub_module_id',$submodule_id)->whereNull('deleted_at')->first();
        return $submoduleTask;
    }
    

    public function isSubmoduleHaveTask($submodule_id, $task_id){
        $submoduleTaskMapper = Analogue::mapper(SubmoduleTask::class);
        $query = $submoduleTaskMapper->query()->where('sub_module_id',$submodule_id)->where('task_id',$task_id);
        return $query->first() ? 1 : 0;
    }

    public function deleteAllModuleSubmodules($submodules){
        $submoduleTaskMapper = Analogue::mapper(Submodule::class);
        $submoduleTaskMapper->delete($submodules);    }

}
