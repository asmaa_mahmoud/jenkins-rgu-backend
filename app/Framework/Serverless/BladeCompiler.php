<?php

namespace App\Framework\Serverless;

use Illuminate\View\Compilers\BladeCompiler as DefaultBladeCompiler;

class BladeCompiler extends DefaultBladeCompiler
{
    /**
     * Get the path to the compiled version of a view.
     *
     * @param  string  $path
     * @return string
     */
    public function getCompiledPath($path)
    {
        return $this->cachePath.'/'.sha1(substr($path, strpos($path, 'resources'))).'.php';
    }

    /**
     * Determine if the view at the given path is expired.
     *
     * @param  string  $path
     * @return bool
     */
    public function isExpired($path)
    {
        return false;
    }
}
