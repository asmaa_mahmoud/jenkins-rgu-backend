<?php

namespace App\Framework\Providers;

use Illuminate\Support\Facades\Event;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\DomainModelLayer\Customers\CustomerCreated' => [
            'App\DomainModelLayer\Customers\CustomerCreatedHandle',
        ],
        'App\DomainModelLayer\Customers\CustomerChangedEmail' => [
            'App\DomainModelLayer\Customers\CustomerChangedEmailHandle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('tymon.jwt.absent', function () {
            $response = new ResponseObject();
            $response->errorMessage = 'User is not logged in';
            $response->status_code = 440;
            $response->errored = true;
            return Response::json($response,$response->status_code);
        });

        Event::listen('tymon.jwt.expired', function ($foo) {
            $response = new ResponseObject();
            $response->errorMessage = 'User session has expired';
            $response->status_code = 441;
            $response->errored = true;
            return Response::json($response,$response->status_code);
        });

        Event::listen('tymon.jwt.invalid', function ($foo) {
            $response = new ResponseObject();
            $response->errorMessage = 'User token is invalid';
            $response->status_code = 442;
            $response->errored = true;
            return Response::json($response,$response->status_code);
        });
    }
}
