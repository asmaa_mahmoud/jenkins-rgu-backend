<?php

namespace App\Framework\Providers;

use Illuminate\Support\ServiceProvider;
use Analogue;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Analogue::register(Category::class, CategoryMap::class);
        Analogue::registerPlugin('Analogue\ORM\Plugins\Timestamps\TimestampsPlugin');
        Analogue::registerPlugin('Analogue\ORM\Plugins\SoftDeletes\SoftDeletesPlugin');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //region Services binds
        $this->app->bind('App\ApplicationLayer\Accounts\Interfaces\IAccountMainService', 'App\ApplicationLayer\Accounts\AccountMainService');
        $this->app->bind('App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService', 'App\ApplicationLayer\Journeys\JourneyMainService');
        $this->app->bind('App\ApplicationLayer\Schools\Interfaces\ISchoolMainService', 'App\ApplicationLayer\Schools\SchoolMainService');
        $this->app->bind('App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService', 'App\ApplicationLayer\Professional\ProfessionalMainService');
        $this->app->bind('App\ApplicationLayer\HelpCenter\Interfaces\IHelpCenterMainService', 'App\ApplicationLayer\HelpCenter\HelpCenterMainService');
        //endregion


        //region Repositories binds
        $this->app->bind('App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository', 'App\Infrastructure\Accounts\AccountMainRepository');
        $this->app->bind('App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository', 'App\Infrastructure\Journeys\JourneyMainRepository');
        $this->app->bind('App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository', 'App\Infrastructure\Schools\SchoolMainRepository');
        $this->app->bind('App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository', 'App\Infrastructure\Professional\ProfessionalMainRepository');
        $this->app->bind('App\DomainModelLayer\HelpCenter\Repositories\IHelpCenterMainRepository', 'App\Infrastructure\HelpCenter\HelpCenterMainRepository');
        //endregion
    }
}