<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:04 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\School;

class SchoolCampusMap extends EntityMap
{
    protected $table = 'school_campus';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "school_campus.deleted_at";

    public function creator(SchoolCampus $schoolCampus){
        return $this->belongsTo($schoolCampus,  User::class , 'creator_id','id');
    }

    public function school(SchoolCampus $schoolCampus){
        return $this->belongsTo($schoolCampus, School::class , 'school_id','id' );
    }

    public function campus(SchoolCampus $schoolCampus){
        return $this->belongsTo($schoolCampus, Campus::class , 'campus_id','id' );
    }

//    public function distributorCamp(SchoolCampus $schoolCampus){
//        return $this->hasOne($schoolCampus, SchoolDistributorCampus::class, 'school_campus_id', 'id');
//    }
}