<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 1:53 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class LearningPathMap extends EntityMap
{
    protected $table = 'learning_path';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "learning_path.deleted_at";

    public function learningPathType(LearningPath $learningPath){
        return $this->belongsTo($learningPath, LearningPathType::class , 'learning_path_type_id', 'id');
    }

    public function activities(LearningPath $learningPath){
        return $this->belongsToMany($learningPath, Activity::class , 'learning_path_activity', 'learning_path_id', 'activity_id');
    }

    public function defaultActivities(LearningPath $learningPath){
        return $this->belongsToMany($learningPath, DefaultActivity::class , 'learning_path_activity','learning_path_id','default_activity_id' );
    }

    public function learningPathActivities(LearningPath $learningPath){
        return $this->hasMany($learningPath, LearningPathActivity::class, 'learning_path_id', 'id');
    }

    public function users(LearningPath $learningPath){
        return $this->belongsToMany($learningPath, User::class, 'learning_path_user', 'learning_path_id', 'user_id');
    }

    public function learningPathUsers(LearningPathUser $learningPath){
        return $this->hasMany($learningPath, LearningPathUser::class, 'learning_path_id', 'id');
    }
}