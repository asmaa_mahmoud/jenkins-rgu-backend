<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 12:59 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;


class AnalysisAnswerTranslationMap extends EntityMap
{
    protected $table = 'analysis_answers_translations';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "analysis_answers_translations.deleted_at";

    public function answer(AnalysisAnswerTranslation $analysisAnswerTranslation){
        return $this->belongsTo($analysisAnswerTranslation, AnalysisAnswer::class , 'answer_id', 'id');
    }

}