<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 04-Mar-19
 * Time: 10:42 AM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;

class AnalysisUpCategoryTranslationMap extends EntityMap{

    protected $table = 'analysis_up_category_translation';
    public $timestamps = true;

    public function upCategory(AnalysisUpCategoryTranslation $analysisUpCategoryTranslation){
        return $this->belongsTo($analysisUpCategoryTranslation, AnalysisUpCategory::class,'up_category_id','id');
    }
}