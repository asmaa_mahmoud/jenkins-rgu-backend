<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 1:43 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;

class LearningPathType extends Entity
{

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getNumberOfStages(){
        return $this->no_of_stages;
    }

    public function setNumberOfStages($no_of_stages){
        return $this->no_of_stages = $no_of_stages;
    }

    public function getLearningPaths(){
        return $this->learningPaths;
    }
}