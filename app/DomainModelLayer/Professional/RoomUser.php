<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/11/2018
 * Time: 3:54 PM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;

class RoomUser extends Entity
{
    public function __construct(Room $room, User $user, Role $role){
        $this->room = $room;
        $this->user = $user;
        $this->role = $role;
    }

    public function getId(){
        return $this->id;
    }

    public function getRoom(){
        return $this->room;
    }

    public function getUser(){
        return $this->user;
    }

    public function getRole(){
        return $this->role;
    }
}