<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 1:13 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;


class AnalysisQuestionMap extends EntityMap
{
    protected $table = 'analysis_questions';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "analysis_questions.deleted_at";

    public function answers(AnalysisQuestion $analysisQuestion){
        return $this->hasMany($analysisQuestion,AnalysisAnswer::class,'question_id','id');
    }

    public function  analysisCategory(AnalysisQuestion $analysisQuestion){
        return $this->belongsTo($analysisQuestion,AnalysisCategory::class,'analysis_category_id','id');
    }

    public function translations(AnalysisQuestion $analysisQuestion){
        return $this->hasMany($analysisQuestion, AnalysisQuestionTranslation::class , 'question_id', 'id');
    }

}