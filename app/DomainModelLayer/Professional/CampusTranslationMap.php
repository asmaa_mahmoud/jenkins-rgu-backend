<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;

class CampusTranslationMap extends EntityMap
{

    protected $table = 'campus_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_translation.deleted_at";


    public function campus(){
        return $this->belongsTo('App\Models\Professional\Campus', 'id');
    }

}