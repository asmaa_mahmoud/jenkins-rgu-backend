<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 3:37 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;

class Material extends Entity
{
    public function __construct(CampusRoundClass $roundClass){
        $this->roundClass = $roundClass;
    }

    public function getId(){
        return $this->id;
    }

    public function getRoundClass(){
        return $this->roundClass;
    }


    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getFileLink()
    {
        return $this->file_link;
    }

    public function setFileLink($link)
    {
        $this->file_link = $link;
    }

}