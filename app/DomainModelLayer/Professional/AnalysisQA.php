<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisQADto;

use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Professional\AnalysisQuestion;
use App\DomainModelLayer\Professional\AnalysisAnswer;
use App\DomainModelLayer\Professional\UserAssessment;

class AnalysisQA extends Entity
{

    public function __construct(AnalysisQADto $analysisQADto )
    {
        $this->question_id=$analysisQADto->question_id;
        $this->answer_id=$analysisQADto->answer_id;
        $this->submission_id=$analysisQADto->submission_id;

        
    }

   

    public function getQuestionId()
    {
        return $this->question_id;
    }

    public function getAnswerId()
    {
        return $this->answer_id;
    }

    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    public function setQuestionId(AnalysisQuestion $question)
    {
        $this->question_id=$question->id;
    }

    public function setAnswerId(AnalysisAnswer $answer)
    {
         $this->answer_id=$answer->id;
    }

    public function setSubmissionId(Submission $submission)
    {
         $this->user_assessment_id=$submission->id;
    }

       
  
}