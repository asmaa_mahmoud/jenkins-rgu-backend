<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 9:38 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;

class SubmoduleMap extends EntityMap
{
    protected $table = 'sub_module';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "sub_module.deleted_at";

    public function campusClass(Submodule $submodule){
        return $this->belongsTo($submodule, CampusClass::class, 'class_id', 'id');
    }
    public function translations(Submodule $submodule){
        return $this->hasMany($submodule, SubmoduleTranslation::class , 'sub_module_id', 'id');
    }
    public function tasks(Submodule $submodule){
        return $this->belongsToMany($submodule, Task::class , 'sub_module_task', 'sub_module_id', 'task_id')->orderBy('sub_module_task.order')->whereNull('sub_module_task.deleted_at');
    }
    

      
}