<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\CampusTranslationDto;

class CampusTranslation extends Entity
{

    public static function searchable(){
        $searchable = [
            'name',
            'description'
        ];
        return $searchable;

    }
    public function __construct(CampusTranslationDto $campusTranslationDto )
    {


    }


    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getDescription()
    {
        return $this->description;
    }
    public function getName()
    {
        return $this->name;
    }

    public function getCampusId()
    {
        return $this->campus_id;
    }


    public function setLanguageCode($language_code)
    {
        $this->language_code=$language_code;
    }

    public function setDescription($description)
    {
        $this->description=$description;
    }
    public function setName($name)
    {
        $this->name=$name;
    }

    public function setCampusId(Campus $campus)
    {
        $this->campus_id=$campus->id;
    }


}