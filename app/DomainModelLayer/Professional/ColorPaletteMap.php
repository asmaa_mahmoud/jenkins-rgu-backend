<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 11-Mar-19
 * Time: 2:00 PM
 */

namespace App\DomainModelLayer\Professional;

use App\DomainModelLayer\Schools\School;
use Analogue\ORM\EntityMap;

class ColorPaletteMap extends EntityMap{

    protected $table = 'color_palette';
    public $timestamps = true;

    public function schools(ColorPalette $colorPalette){
        return $this->hasMany($colorPalette, School::class,'color_palette_id','id');
    }
}
