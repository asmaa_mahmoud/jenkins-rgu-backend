<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 2:30 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class ProjectResourceMap extends EntityMap
{
    protected $table = 'project_resources';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "project_resources.deleted_at";
    
    public function project(ProjectResource $projectResource){
        return $this->belongsTo($projectResource, Project::class , 'project_id', 'id');
    }

}