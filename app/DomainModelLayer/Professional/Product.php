<?php


namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use Carbon\Carbon;
use LaravelLocalization;

class Product extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getUrl(){
        return $this->url;
    }

    public function getLogoImage(){
        return $this->logo_image;
    }

    public function getSideImage(){
        return $this->side_image;
    }

    public function getProduct_logo_image(){
        return $this->logo_image;
    }

    public function getProduct_side_image(){
        return $this->side_image;
    }

}