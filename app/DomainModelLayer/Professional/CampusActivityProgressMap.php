<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:34 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class CampusActivityProgressMap extends EntityMap
{
    protected $table = 'campus_activity_progress';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "campus_activity_progress.deleted_at";

    public function user(CampusActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, User::class , 'user_id', 'id');
    }

    public function task(CampusActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Task::class , 'task_id', 'id');
    }

    public function activity(CampusActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(CampusActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function campus(CampusActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Campus::class , 'campus_id', 'id');
    }
}