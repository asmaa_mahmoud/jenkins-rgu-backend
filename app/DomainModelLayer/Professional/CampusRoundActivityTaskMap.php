<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 05/11/2018
 * Time: 9:47 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class CampusRoundActivityTaskMap extends EntityMap
{
    protected $table = 'campus_round_activity_task';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_round_activity_task.deleted_at";

    public function campusRound(CampusRoundActivityTask $roundActivityTask)
    {
        return $this->belongsTo($roundActivityTask, CampusRound::class , 'campus_round_id', 'id');
    }

    public function campusActivityTask(CampusRoundActivityTask $roundActivityTask)
    {
        return $this->belongsTo($roundActivityTask, CampusActivityTask::class , 'campus_activity_task_id', 'id');
    }
}