<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 3:23 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class LearningPathActivityProgressMap extends EntityMap
{
    protected $table = 'learning_path_activity_progress';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "learning_path_activity_progress.deleted_at";

    public function user(LearningPathActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, User::class , 'user_id', 'id');
    }

    public function task(LearningPathActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Task::class , 'task_id', 'id');
    }

    public function activity(LearningPathActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(LearningPathActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function learningPath(LearningPathActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, LearningPath::class , 'learning_path_id', 'id');
    }
}