<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 3:09 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class LearningPathUserMap extends EntityMap
{
    protected $table = 'learning_path_user';
    public $timestamps = true;

//    public $softDeletes = true;
//    protected $deletedAtColumn = "learning_path_user.deleted_at";

    public function user(LearningPathUser $learningPathUser){
        return $this->belongsTo($learningPathUser, User::class , 'user_id', 'id');
    }

    public function learningPath(LearningPathUser $learningPathUser){
        return $this->belongsTo($learningPathUser, LearningPath::class , 'learning_path_id', 'id');
    }
}