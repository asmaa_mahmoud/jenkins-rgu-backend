<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;

class CourseObjectivesMap extends EntityMap
{

    protected $table = 'course_objectives';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "course_objectives.deleted_at";


    public function translations(CourseObjectives $courseObjectives){
        return $this->hasMany($courseObjectives, CourseObjectivesTranslation::class , 'course_objectives_id', 'id');
    }

}