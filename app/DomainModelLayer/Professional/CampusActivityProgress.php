<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:27 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class CampusActivityProgress extends Entity
{
    public function __construct(Task $task, User $user, Campus $campus, $score, $trials,$is_evaluated,$evaluation=null, $first_success = null, $timeTaken = null, $bestTimeTaken = null, $numberOfBlocks = null, $bestNumberOfBlocks = null, $userCode = null)
    {
        $this->task = $task;
        $this->user = $user;
        $this->score = $score;
        $this->campus = $campus;
        $this->no_of_trials = $trials;
        if($first_success != null)
            $this->first_success = $first_success;
        $this->task_duration = $timeTaken;
        $this->best_task_duration = $bestTimeTaken;
        $this->no_of_blocks = $numberOfBlocks;
        $this->best_blocks_number = $bestNumberOfBlocks;
        $this->user_code = $userCode;
        if($evaluation!==null)
            $this->evaluation=$evaluation;
       $this->is_evaluated=$is_evaluated;
        //dd($userCode);
    }

    public function getId(){
        return $this->id;
    }

    public function getBestTaskDuration(){
        return $this->best_task_duration;
    }

    public function setBestTaskDuration($bestTimeTaken){
        $this->best_task_duration = $bestTimeTaken;
    }

    public function getBestBlocksNumber(){
        return $this->best_blocks_number;
    }

    public function setBestBlocksNumber($bestNumberOfBlocks){
        $this->best_blocks_number = $bestNumberOfBlocks;
    }

    public function getNoBlocks(){
        return $this->no_of_blocks;
    }

    public function setNoBlocks($numberOfBlocks){
        $this->no_of_blocks = $numberOfBlocks;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;
    }
    public function getEvaluation()
    {
        return $this->evaluation;
    }

    public function setEvaluation($evaluation)
    {
        $this->evaluation = $evaluation;
    }

    public function getUnlockedByCoins()
    {
        return $this->unlocked_by_coins;
    }

    public function setUnlockedByCoins($unlockedByCoins)
    {
        $this->unlocked_by_coins = $unlockedByCoins;
    }

    public function getFirstSuccess()
    {
        return $this->first_success;
    }

    public function setFirstSuccess($first)
    {
        $this->first_success = $first;
    }

    public function getTaskDuration()
    {
        return $this->task_duration;
    }

    public function setTaskDuration($duration)
    {
        $this->task_duration = $duration;
    }

    public function getNoTrials()
    {
        return $this->no_of_trials;
    }

    public function setNoTrials($number)
    {
        $this->no_of_trials = $number;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
        $this->defaultActivity = $defaultActivity;
    }

    public function getCampus()
    {
        return $this->campus;
    }

    public function setCampus(Campus $campus)
    {
        $this->campus = $campus;
    }

    public function getUserCode()
    {
        return $this->user_code;
    }

    public function setUserCode($code)
    {
        $this->user_code = $code;
    }

    public function isSuccess(){
        if($this->getFirstSuccess() == null)
            return false;
        else if($this->getFirstSuccess() == 0)
            return false;
        else
            return true;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function getUpdatedAt(){
        return $this->updated_at;
    }

    public function setUpdatedAt($updatedAt){
        $this->updated_at = $updatedAt;
    }

    public function getIsEvaluated()
    {
        return $this->is_evaluated;
    }

    public function getFirstPass()
    {
        return intval($this->getFirstSuccess() == 1 && $this->getNoTrials() == 1);
    }

    public function setIsEvaluated($is_evaluated)
    {
        $this->is_evaluated = $is_evaluated;
    }
    public function setHasGivenUp($hasGivenUp)
    {
        $this->has_given_up = $hasGivenUp;
    }
}