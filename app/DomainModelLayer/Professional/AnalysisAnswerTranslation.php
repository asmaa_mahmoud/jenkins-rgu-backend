<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerTranslationDto;
use App\DomainModelLayer\Professional\AnalysisAnswer;

class AnalysisAnswerTranslation extends Entity
{

    public function __construct(AnalysisAnswerTranslationDto $analysisAnswerTranslationDto )
    {
 
        
    }

    
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getAnswerDescription()
    {
        return $this->answer_description;
    }

     public function getAnswerId()
    {
        return $this->answer_id;
    }


     public function setLanguageCode($language_code)
    {
         $this->language_code=$language_code;
    }

    public function setAnswerDescription($answer_description)
    {
         $this->answer_description=$answer_description;
    }

     public function setAnswerId(AnalysisAnswer $answer)
    {
         $this->answer_id=$answer->id;
    }

     
  
}