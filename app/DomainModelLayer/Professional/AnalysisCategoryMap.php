<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;

use App\DomainModelLayer\Professional\AnalysisQuestion;

class AnalysisCategoryMap extends EntityMap {

    protected $table = 'analysis_category';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "analysis_category.deleted_at";

    public  function questions(AnalysisCategory $analysisCategory){
        return $this->hasMany($analysisCategory,AnalysisQuestion::class,'analysis_category_id','id');

    }
    public function translations(AnalysisCategory $analysisCategory){
        return $this->hasMany($analysisCategory, AnalysisCategoryTranslation::class , 'analysis_category_id', 'id');
    }

    //TODO:Not sure will Ask for this relation

    public function startingQuestion(AnalysisCategory $analysisCategory){
        return $this->hasOne($analysisCategory,AnalysisQuestion::class,'id','starting_question_id');

    }
    public function upCategory(AnalysisCategory $analysisCategory){
        return $this->belongsTo($analysisCategory, AnalysisUpCategory::class, 'up_category_id');
    }
}