<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 3:47 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class RoomMap extends EntityMap
{
    protected $table = 'room';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "room.deleted_at";

    public function roundClass(Room $room){
        return $this->belongsTo($room,  CampusRoundClass::class , 'campus_round_class_id','id');
    }

    public function material(Room $room){
        return $this->belongsTo($room,  Material::class , 'file_id','id');
    }

    public function roomUsers(Room $room){
        return $this->hasMany($room, RoomUser::class, 'room_id', 'id');
    }

}