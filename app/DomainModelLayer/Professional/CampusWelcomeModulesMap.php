<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 9:38 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;


class CampusWelcomeModulesMap extends EntityMap
{
    protected $table = 'campus_welcome_modules';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_welcome_modules.deleted_at";


    public function translations(CampusWelcomeModules $campusWelcomeModules)
    {
        return $this->hasMany($campusWelcomeModules, CampusWelcomeModuleTranslation::class , 'campus_welcome_module_id', 'id');
    }
}