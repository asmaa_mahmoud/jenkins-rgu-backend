<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 2:30 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class ProjectTranslationMap extends EntityMap
{
    protected $table = 'project_translations';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "project_translations.deleted_at";

    public function project(ProjectTranslation $projectTranslation){
        return $this->belongsTo($projectTranslation, Project::class , 'project_id', 'id');
    }
}