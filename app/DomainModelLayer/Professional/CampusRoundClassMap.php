<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 2:34 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class CampusRoundClassMap extends EntityMap
{
    protected $table = 'campus_round_class';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_round_class.deleted_at";

    public function campusRound(CampusRoundClass $roundClass)
    {
        return $this->belongsTo($roundClass, CampusRound::class , 'campus_round_id', 'id');
    }

    public function campusClass(CampusRoundClass $roundClass)
    {
        return $this->belongsTo($roundClass, CampusClass::class , 'class_id', 'id');
    }

    public function rooms(CampusRoundClass $roundClass)
    {
        return $this->hasMany($roundClass, Room::class , 'campus_round_class_id', 'id');
    }

    public function materials(CampusRoundClass $roundClass)
    {
        return $this->hasMany($roundClass, Material::class , 'campus_round_class_id', 'id');
    }
}