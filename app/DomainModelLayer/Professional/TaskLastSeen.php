<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:20 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class TaskLastSeen extends Entity
{
    public function __construct(Task $task, User $user, Campus $campus,$lastSeen)
    {
        $this->task = $task;
        $this->user = $user;
        $this->campus = $campus;
        $this->last_seen = $lastSeen;

    }

    public function getId(){
        return $this->id;
    }



    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
        $this->defaultActivity = $defaultActivity;
    }

    public function getCampus()
    {
        return $this->campus;
    }

    public function setCampus(Campus $campus)
    {
        $this->campus = $campus;
    }

    public function getUserCode()
    {
        return $this->user_code;
    }

    public function setUserCode($code)
    {
        $this->user_code = $code;
    }

    public function getLastSeen()
    {
        return $this->last_seen;
    }

    public function setLastSeen($last_seen)
    {
        $this->last_seen = $last_seen;
    }



}