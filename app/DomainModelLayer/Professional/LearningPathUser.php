<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 3:07 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;

class LearningPathUser extends Entity
{
    public function __construct(LearningPath $learningPath, User $user){
        $this->learningPath = $learningPath;
        $this->user = $user;
    }

    public function getId(){
        return $this->id;
    }

    public function getLearningPath(){
        return $this->learningPath;
    }

    public function getUser(){
        return $this->user;
    }
}