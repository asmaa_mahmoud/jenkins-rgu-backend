<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 3:58 PM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use Carbon\Carbon;
use LaravelLocalization;


class CampusWelcomeModules extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getIntroductoryVideoUrl(){
        return $this->introductory_video_url;
    }

    public function getOrientationVideoUrl(){
        return $this->orientation_video_url;
    }

    public function getTranslations(){
        return $this->translations;
    }

    public function getText(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getText();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getText();
        }
        return $english_translation;
    }
}
