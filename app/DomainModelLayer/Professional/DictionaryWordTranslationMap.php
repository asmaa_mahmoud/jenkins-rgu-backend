<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;

class DictionaryWordTranslationMap extends EntityMap
{

    protected $table = 'dictionary_word_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dictionary_word_translation.deleted_at";


    public function dictionaryWord(){
        return $this->belongsTo('App\Models\Professional\DictionaryWord', 'id');
    }

}