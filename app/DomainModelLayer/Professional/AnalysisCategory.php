<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisCategoryDto;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;
class AnalysisCategory extends Entity
{

    public function __construct(AnalysisCategoryDto $analysisCategoryDto )
    {
        //id,starting_question_id
        //$this->starting_question_id=$analysisCategoryDto->startingQuestion->id;
 
        
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getId(){
        return $this->id;
    }
    public function getStartingQuestionId()
    {
        return $this->startingQuestion->id;
    }

    public function setStartingQuestionId($question_id)
    {
        $this->starting_question_id=$question_id;
    }

    public function getIsRandom(){
        return $this->is_random;
    }
    public function getUpCategory(){
        return $this->upCategory;
    }

    public function getOrder(){
        return $this->order_in_up_category;
    }

    public function getCategoryName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }
    

    public function getCategoryDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getIconUrl(){
        return $this->iconUrl;
    }
    
    public function getUpCategoryId(){
        return $this->up_category_id;
    }

    public function getCategoryCode(){
        return $this->category_code;
    }

    public function getQuestionsCount(){
        if($this->getCategoryCode() == "PSA\r")
            return 14;
        else    
            return count($this->questions);
    }
}