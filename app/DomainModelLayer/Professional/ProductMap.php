<?php

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;


class ProductMap extends EntityMap
{
    protected $table = 'product';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "product.deleted_at";


  
}