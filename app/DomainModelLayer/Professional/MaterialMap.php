<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 3:47 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class MaterialMap extends EntityMap
{
    protected $table = 'material';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "material.deleted_at";

    public function roundClass(Material $material){
        return $this->belongsTo($material,  CampusRoundClass::class , 'campus_round_class_id','id');
    }



}