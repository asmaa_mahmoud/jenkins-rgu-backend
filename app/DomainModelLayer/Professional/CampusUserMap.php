<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:10 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;

class CampusUserMap extends EntityMap
{
    protected $table = 'campus_user';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "campus_user.deleted_at";

    public function user(CampusUser $campusUser){
        return $this->belongsTo($campusUser, User::class , 'user_id', 'id');
    }

    public function role(CampusUser $campusUser){
        return $this->belongsTo($campusUser, Role::class , 'role_id', 'id');
    }

    public function campusRound(CampusUser $campusUser){
        return $this->belongsTo($campusUser, CampusRound::class , 'campus_round_id', 'id');
    }
}