<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:58 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class CampusActivityTaskMap extends EntityMap
{
    protected $table = 'campus_activity_task';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_activity_task.deleted_at";

    public function task(CampusActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, Task::class , 'task_id', 'id');
    }

    public function activity(CampusActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(CampusActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function campus(CampusActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, Campus::class , 'campus_id', 'id');
    }

     public function campusRoundActivityTasks(CampusActivityTask $activityTask)
    {
        return $this->hasMany($activityTask, CampusRoundActivityTask::class , 'campus_activity_task_id', 'id');
    }


}