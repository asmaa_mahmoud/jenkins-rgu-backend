<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\CampusTranslationDto;

class DictionaryWordTranslation extends Entity
{

   
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getDescription()
    {
        return $this->description;
    }
    public function getWord()
    {
        return $this->word;
    }

   

}