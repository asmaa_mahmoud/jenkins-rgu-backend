<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 2:10 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class LearningPathActivity extends Entity
{
    public function __construct(LearningPath $learningPath, $order){
        $this->learningPath = $learningPath;
        $this->order = $order;
    }

    public function getId(){
        return $this->id;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function setActivity(Activity $activity){
        $this->activity = $activity;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity){
        $this->defaultActivity = $defaultActivity;
    }

    public function getStageNumber(){
        return $this->stage_number;
    }

    public function setStageNumber($stage_number){
        return $this->stage_number = $stage_number;
    }

    public function getStageName(){
        return $this->stage_name;
    }

    public function setStageName($stage_name){
        return $this->stage_name = $stage_name;
    }

    public function getOrder(){
        return $this->order;
    }

    public function setOrder($order){
        return $this->order = $order;
    }

    public function getLearningPath(){
        return $this->learningPath;
    }

    public function setLearningPath(LearningPath $learningPath){
        $this->learningPath = $learningPath;
    }
}