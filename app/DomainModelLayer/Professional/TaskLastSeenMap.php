<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:34 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class TaskLastSeenMap extends EntityMap
{
    protected $table = 'campus_activity_last_seen';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_activity_last_seen.deleted_at";

    public function user(TaskLastSeen $taskLastSeen)
    {
        return $this->belongsTo($taskLastSeen, User::class , 'user_id', 'id');
    }

    public function task(TaskLastSeen $taskLastSeen)
    {
        return $this->belongsTo($taskLastSeen, Task::class , 'task_id', 'id');
    }

    public function activity(TaskLastSeen $taskLastSeen)
    {
        return $this->belongsTo($taskLastSeen, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(TaskLastSeen $taskLastSeen)
    {
        return $this->belongsTo($taskLastSeen, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function campus(TaskLastSeen $taskLastSeen)
    {
        return $this->belongsTo($taskLastSeen, Campus::class , 'campus_id', 'id');
    }
}