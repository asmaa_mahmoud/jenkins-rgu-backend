<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 11-Mar-19
 * Time: 2:00 PM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use LaravelLocalization;

class ColorPalette extends Entity{

    public function getId(){
        return $this->id;
    }

    public function getColors(){
        return $this->colors;
    }

}