<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 1:13 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;


class AnalysisAnswerMap extends EntityMap
{
    protected $table = 'analysis_answers';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "analysis_answers.deleted_at";

    public function question(AnalysisAnswer $analysisAnswer){
        return $this->belongsTo($analysisAnswer,AnalysisQuestion::class,'question_id','id');
    }

    public function nextQuestion(AnalysisAnswer $analysisAnswer){
        return $this->belongsTo($analysisAnswer,AnalysisQuestion::class,'next_question_id','id');
    }

    public function translations(AnalysisAnswer $analysisAnswer){
        return $this->hasMany($analysisAnswer, AnalysisAnswerTranslation::class , 'answer_id', 'id');
    }

}