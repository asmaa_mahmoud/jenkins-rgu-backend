<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:20 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use Carbon\Carbon;

class CampusRound extends Entity
{
    public function __construct(Campus $campus, $starts_at, $ends_at, $no_of_students=null, $round_settings='{"classes": true,"materials": true,"sessions": true}',$round_lti_id=null,$round_name){
        $this->campus = $campus;
        $this->starts_at = $starts_at;
        $this->ends_at = $ends_at;
        $this->no_of_students = $no_of_students;
        $this->round_settings = $round_settings;
        $this->round_lti_id=$round_lti_id;
        $this->round_name=$round_name;

        }

    public function getCampus(){
        return $this->campus;
    }

    public function getId(){
        return $this->id;
    }
    
    public function getRound_settings(){
        return $this->round_settings;
    }
    
    public function getCampusName(){
        return $this->campus->getName();
    }
    public function getCampusHasSubModules(){
        return $this->campus->hasSubModules();
    }

    public function getCampusIconUrl(){
        return $this->campus->getIconUrl();
    }

    public function getCampus_id(){
        return $this->campus->id;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }

    public function setStartsAt($starts_at){
        $this->starts_at = $starts_at;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getNoOfStudents(){
        return $this->no_of_students;
    }

    public function setNoOfStudents($noOfStudents){
        $this->no_of_students = $noOfStudents;
    }

    public function getDuration(){
        $startDate = Carbon::createFromTimestamp(intval($this->ends_at));
        $endDate = Carbon::createFromTimestamp(intval($this->starts_at));
       return $endDate->diffInDays($startDate);

    }
    public function getTimeEstimate(){
        return $this->getCampus()->getTimeEstimate();
    }

    public function getStatus($gmt_difference=null){
        $gmt_difference=($gmt_difference!==null)?(float)$gmt_difference:0;
        $campus = $this->getCampus();

        if($campus->getLocked() == 1){
            return 'locked';
        }
        else {
            if($this->getEndsAt() < Carbon::today()->addHour(-($gmt_difference))->timestamp)
                return 'ended';
            else if($this->getStartsAt() > Carbon::today()->addHour(-($gmt_difference))->timestamp)
                return 'upcoming';
            else
                return 'running';
        }
    }

    public function getUsers(){
        return $this->users;
    }

    public function getStudents(){
        $users = [];
        foreach ($this->getCampusUsers() as $campusUser) {
            //dd($user);
            $roles = $campusUser->getUser()->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "student")
                    array_push($users,  $campusUser->getUser());
            }
        }
        return $users;
    }


    public function getCountOfStudents(){
        $users = 0;
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "student")
                    $users++;
            }
        }
        return $users;
    }

    public function getTeachers(){
        $users = [];
        foreach ($this->getCampusUsers() as $campusUser) {
            $roles = $campusUser->getUser()->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "teacher")
                    array_push($users, $campusUser->getUser());
            }
        }
        return $users;
    }

    public function getCountOfTeachers(){
        $users = 0;
        foreach ($this->getCampusUsers() as $user) {
            $roles = $user->getUser()->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "teacher")
                    $users++;
            }
        }
        return $users;
    }

    public function addCampusUser(CampusUser $campusUser){
        $this->campusUsers->push($campusUser);
    }

    public function getCampusUsers(){
        return $this->campusUsers;
    }

    public function getCampusRoundClasses(){
        return $this->campusRoundClasses;
    }

    public function getCampusRoundActivityTasks(){
        return $this->campusRoundTasks;
    }
    public function getNoOfClasses(){
        return  $this->campusRoundClasses->count();
    }


    public function isMember($user_id){
        $users = $this->getUsers();
        foreach ($users as $user){
            if($user->getId() == $user_id)
                return true;
        }
        return false;
    }

    public function hasInstructor(){
        return $this->getCountOfTeachers()===0?0:1;
    }

    public function showClasses()
    {
        $settings = json_decode($this->round_settings, true);
        $showClasses = true;
        //dd($settings);
        foreach ($settings as $key => $val) {

            if ($key == "classes" && $val == false) {
                $showClasses = false;
                break;
            }


        }
        return $showClasses;
    }
    public function  getLTIResourceLinkId(){
        return $this->lti_resource_link_id;
    }
    public function  setLTIResourceLinkId($link_id){
        $this->lti_resource_link_id=$link_id;

    }

    public function getTasksUsersCurrentSteps(){
        return $this->tasksUsersCurrentSteps;
    }
    public function setTasksUsersCurrentSteps($currentSteps){
        $this->tasksUsersCurrentSteps = $currentSteps;
    }

    public function getRound_name(){
        return $this->round_name;
    }
    
    public function setRoundName($round_name){
         $this->round_name=$round_name;
    }

}