<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 09-Apr-19
 * Time: 1:57 PM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class TaskCurrentStepMap extends EntityMap{
    protected $table = "task_current_step";
    public $timestamps = true;
    public $softDeletes = true;

    public function user(TaskCurrentStep $taskCurrentStep){
        return $this->belongsTo($taskCurrentStep, User::class, 'user_id');
    }

    public function task(TaskCurrentStep $taskCurrentStep){
        return $this->belongsTo($taskCurrentStep, Task::class, 'task_id');
    }

    public function activity(TaskCurrentStep $taskCurrentStep){
        return $this->belongsTo($taskCurrentStep, Activity::class, 'activity_id');
    }

    public function default_activity(TaskCurrentStep $taskCurrentStep){
        return $this->belongsTo($taskCurrentStep, DefaultActivity::class, 'default_activity_id');
    }

    public function campus(TaskCurrentStep $taskCurrentStep){
        return $this->belongsTo($taskCurrentStep, Campus::class, 'campus_id');
    }
}