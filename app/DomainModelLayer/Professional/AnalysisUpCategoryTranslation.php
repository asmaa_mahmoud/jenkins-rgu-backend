<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 04-Mar-19
 * Time: 10:41 AM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;

class AnalysisUpCategoryTranslation extends Entity{

    public function getId(){
        return $this->id;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getDescription(){
        return $this->description;
    }

}