<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 12:59 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;


class AnalysisQuestionTranslationMap extends EntityMap
{
    protected $table = 'analysis_questions_translations';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "analysis_questions_translations.deleted_at";

    public function question(AnalysisQuestionTranslation $analysisQuestionTranslation){
        return $this->belongsTo($analysisQuestionTranslation, AnalysisQuestion::class , 'question_id', 'id');
    }

}