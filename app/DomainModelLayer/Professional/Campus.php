<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:56 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\CampusRequestDto;
use Carbon\Carbon;
use LaravelLocalization;

class Campus extends Entity
{
    public function __construct(CampusRequestDto $campusDto){
        $this->name = $campusDto->name;
        $this->description = $campusDto->description;
        $this->icon_url = $campusDto->iconUrl;
    }

    public function getId(){
        return $this->id;
    }

    public function hasSubModules(){
        return $this->has_sub_modules;
    }
    public function getTimeEstimate(){
        $estimateTime=0;
        foreach ($this->classes as $campusClass) {
            $estimateTime+=$campusClass->time_estimate;
        }
        
        return $estimateTime;
    }


    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

//    public function setName($name){
//        $this->name = $name;
//    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

//    public function setDescription($description){
//        return $this->description = $description;
//    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function setIconUrl($icon_url){
        $this->icon_url = $icon_url;
    }

    public function getSchoolCampus(){
        return $this->schoolCampus;
    }

    public function addSchoolCampus(SchoolCampus $schoolCampus){
        $this->school->push($schoolCampus);
    }

    public function getSchool(){
        return $this->getSchoolCampus()->getSchool()->getName();
    }

    public function getCampusActivityTasks(){
        return $this->campusActivityTasks;
    }

    public function getRounds(){
        return $this->rounds;
    }

    public function getClasses(){
        return $this->classes;
    }

    public function getLocked(){
        return $this->schoolCampus->getLocked();
    }

    public function getNoOfClasses(){
        return $this->no_of_classes;
    }

    public function getEstimateTime(){
        $estimate_time=0;
        foreach ( $this->classes as $class) {
            $class_estimateTime=$class->getTimeEstimate()!==null?$class->getTimeEstimate():0;
            $estimate_time+=$class_estimateTime;

        }
        return $estimate_time;
    }

    public function getHelpCenterArticles(){
        return $this->help_center_articles;
    }
    public function setHelpCenterArticles($articles){
        $this->help_center_articles = $articles;
    }

    public function getCampusArticleId(){
       $campusArticle= $this->help_center_articles->first();
       if($campusArticle){
            return $campusArticle->getId();
        }else{
            return null;
        }
    }

    public function getObjectives(){
        return $this->courseObjectives;
    }

    public function getXps(){
        return $this->xps;
    }
}