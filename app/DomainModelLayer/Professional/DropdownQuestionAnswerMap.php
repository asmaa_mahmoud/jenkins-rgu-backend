<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:34 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\DropdownChoice;

class DropdownQuestionAnswerMap extends EntityMap
{
    protected $table = 'dropdown_question_answers';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "drag_question_answers.deleted_at";

    public function campusActivityQuestion(DropdownQuestionAnswer $dropdownQuestionAnswer)
    {
        return $this->belongsTo($dropdownQuestionAnswer, CampusActivityQuestion::class , 'campus_activity_question_id', 'id');
    }

    public function dropdown(DropdownQuestionAnswer $dropdownQuestionAnswer)
    {
        return $this->belongsTo($dropdownQuestionAnswer, Dropdown::class , 'dropdown_id', 'id');
    }
    public function dropdownChoice(DropdownQuestionAnswer $dropdownQuestionAnswer)
    {
        return $this->belongsTo($dropdownQuestionAnswer, DropdownChoice::class , 'dropdown_choice_id', 'id');
    }

   
}