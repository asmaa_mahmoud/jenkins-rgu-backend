<?php

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;


class CampusProductMap extends EntityMap
{
    protected $table = 'campus_product';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_product.deleted_at";


  
}