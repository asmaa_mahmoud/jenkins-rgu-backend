<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 3:58 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use LaravelLocalization;
class CampusClass extends Entity
{
    public function __construct(Campus $campus, $order){
        $this->campus = $campus;
        $this->order = $order;
    }

    public function getId(){
        return $this->id;
    }

    public function getCampusWelcomeModules(){
        return $this->campusWelcomeModules;
    }

    public function getOrder(){
        return $this->order;
    }

    public function setOrder($order){
        return $this->order = $order;
    }

    public function getCampus(){
        return $this->campus;
    }

    public function setCampus(Campus $campus){
        $this->campus = $campus;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
        $this->defaultActivity = $defaultActivity;
    }
    public function getModuleCourseObjectives($is_default=true){
        if($is_default==true){
            return $this->getDefaultActivity()->getModuleCourseObjectives();
        }else{
            return $this->getActivity()->getModuleCourseObjectives();
        }

    }
    public function getModuleObjectives($is_default=true){
        if($is_default==true){
            return $this->getDefaultActivity()->getModuleObjectives();
        }else{
            return $this->getActivity()->getModuleObjectives();
        }

    }
    public function getName($is_default=true){
        if($is_default==true){
            return $this->getDefaultActivity()->getName();
        }else{
            return $this->getActivity()->getName();
        }

    }

    public function getDescription($is_default=true){
        if($is_default==true){
            return $this->getDefaultActivity()->getDescription();
        }else{
            return $this->getActivity()->getDescription();
        }

    }

    public function getIconUrl($is_default=true){
        if($is_default==true){
            return $this->getDefaultActivity()->getIconUrl();
        }else{
            return $this->getActivity()->getIconUrl();
        }

    }


    public function isProject()
    {
        return $this->isProject;
    }

    public function lessonType()
    {
        return $this->lessonType;
    }

    public function getTimeEstimate(){
        return $this->time_estimate;
    }
    public function setTimeEstimate($time){
        $this->time_estimate = $time;
    }

     public function getCampusArticleId(){

       $lessonArticle= $this->campus->help_center_articles->first();
       if($lessonArticle){
            return $lessonArticle->getId();
        }else{
            return null;
        }

    }

    public function getProject(){
        return $this->project;
    }

    public function getSubmodules(){
        return $this->submodules;
    }

    public function getTotalTimeEstimates(){
        return $this->getDefaultActivity()->getMainActivityTasks()->sum('time_estimate');
    }

    public function getTotalXps(){
        return $this->getDefaultActivity()->getMainActivityTasks()->sum('xps');
    }
    
}