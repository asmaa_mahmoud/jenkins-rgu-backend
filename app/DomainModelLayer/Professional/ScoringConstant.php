<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 3:37 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;

class ScoringConstant extends Entity
{
	public function getValue(){
		return $this->value;
	}

}