<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 12:59 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;


class AnalysisCategoryTranslationMap extends EntityMap
{
    protected $table = 'analysis_category_translations';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "analysis_category_translations.deleted_at";

    public function category(AnalysisCategoryTranslation $analysisCategoryTranslation){
        return $this->belongsTo($analysisCategoryTranslation, AnalysisCategory::class , 'analysis_category_id', 'id');
    }

}