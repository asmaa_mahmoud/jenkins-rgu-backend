<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 3:04 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\HelpCenter\Article;

class CampusMap extends EntityMap
{
    protected $table = 'campus';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus.deleted_at";

    public function classes(Campus $campus){
        return $this->hasMany($campus, CampusClass::class, 'campus_id', 'id')->orderBy('order');
    }

    public function schoolCampus(Campus $campus){
        return $this->hasOne($campus, SchoolCampus::class, 'campus_id', 'id');
    }
    public function article(Campus $campus){
        return $this->belongsTo($campus, Article::class, 'article_id', 'id');
    }

    public function rounds(Campus $campus){
        return $this->hasMany($campus, CampusRound::class, 'campus_id', 'id');
    }
    public function campusActivityTasks(Campus $campus){
        return $this->hasMany($campus, CampusRoundActivityTask::class, 'campus_id', 'id');
    }

    public function translations(Campus $campus){
        return $this->hasMany($campus, CampusTranslation::class , 'campus_id', 'id');
    }

    public function help_center_articles(Campus $campus){
        return $this->belongsToMany($campus, Article::class, 'help_center_article_campus', 'campus_id', 'article_id');
    }

    public function courseObjectives(Campus $campus){
        return $this->hasMany($campus, CourseObjectives::class , 'campus_id', 'id');
    }
}