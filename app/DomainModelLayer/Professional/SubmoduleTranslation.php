<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;

class SubmoduleTranslation extends Entity
{
    
    
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }

   
    public function getLanguageCode(){
        return $this->language_code;
    }

    

}