<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:34 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;


class MissionAngularUserAnswerMap extends EntityMap
{
    protected $table = 'mission_angular_user_answer';
    public $timestamps = true;
   public $softDeletes = true;
   protected $deletedAtColumn = "mission_angular_user_answer.deleted_at";

    public function campusActivityProgress(MissionAngularUserAnswer $missionAngularUserAnswer)
    {
        return $this->belongsTo($missionAngularUserAnswer, CampusActivityProgress::class , 'campus_activity_progress_id', 'id');
    }



   
}