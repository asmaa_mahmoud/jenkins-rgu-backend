<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 12:05 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;

class CampusRoundActivityTask extends Entity
{
    public function __construct(CampusActivityTask $campusTask, CampusRound $round,$weight=null,$weight_overdue=null,$due_date=null){
        $this->campusActivityTask = $campusTask;
        $this->campusRound = $round;
        if($weight!==null)
            $this->weight=$weight;
        //TODO: calculate CampusRoundActivityTask weight form CampusActivityTask table
        if($weight_overdue!==null)
            $this->weight_overdue=$weight_overdue;
        else
            $this->weight_overdue=$this->weight;

        if($due_date!=null)
            $this->due_date=$due_date;
        //TODO:calculate duedate from class table

    }

    public function getCampusActivityTask()
    {
        return $this->campusActivityTask;
    }

    public function getCampusRound()
    {
        return $this->campusRound;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        return $this->weight = $weight;
    }

    public function getWeightOverdue()
    {
        return $this->weight_overdue;
    }

    public function setWeightOverdue($weight)
    {
        return $this->weight_overdue = $weight;
    }

    public function getDueDate()
    {
        return $this->due_date;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setDueDate($date)
    {
        return $this->due_date = $date;
    }
}