<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 2:30 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\ProjectDto;
use Carbon\Carbon;
use LaravelLocalization;

class Project extends Entity
{
    public function __construct(ProjectDto $projectDto){

    }

    public function getProjectId(){
        return $this->id;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getVideoUrl(){
        return $this->video_url;
    }

    public function getProjectTitle(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getProjectDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getProjectResources(){

        
        $allProjectResources = $this->resources;
        /*$projectResources = [];
        foreach ($allProjectResources as $projectResource) {
            array_push($projectResources,  (object)array($projectResource->getTitle(),$projectResource->getResourceUrl()));  
        }*/

        return $allProjectResources;
    }

    public function getProjectTask(){
        return $this->task;
    }

}
