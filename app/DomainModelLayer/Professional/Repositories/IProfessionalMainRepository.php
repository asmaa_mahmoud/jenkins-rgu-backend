<?php

namespace App\DomainModelLayer\Professional\Repositories;


use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\CampusUserStatus;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Professional\AnalysisCategory;
use App\DomainModelLayer\Professional\CampusActivityTask;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusRoundActivityTask;
use App\DomainModelLayer\Professional\CampusRoundClass;
use App\DomainModelLayer\Professional\CampusUser;
use App\DomainModelLayer\Professional\LearningPathActivityProgress;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\MissionAngularUserAnswer;
use App\DomainModelLayer\Professional\CampusActivityQuestion;
use App\DomainModelLayer\Professional\DragQuestionAnswer;
use App\DomainModelLayer\Professional\DropdownQuestionAnswer;
use App\DomainModelLayer\Professional\LearningPathUser;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\RoomUser;
use App\DomainModelLayer\Professional\SchoolCampus;
use App\DomainModelLayer\Professional\Submission;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Professional\Submodule;

interface IProfessionalMainRepository
{
    public function getProductAssets($product_url);

    public function getAllQuestions();
    public function getAllAnswers();
    public function getQuestion($question_id);
    public function getAnswer($answer_id);
    public function submit(Submission $submission);
    public function getUserSubmission($user_id);
    // public function getCategorySubmission($user_id,$category_code);
    public function getSectionSubmissions($user_id, $categories);
    public function getCategoryUserSubmission($user_id, $category_code);
    public function deleteUserSubmissions($user_id,$category_code);
    public function getCategoryQuestions($category_id);
    public function getCategoryById($category_id);
    public function getPostCategories(AnalysisCategory  $category);
    public function deleteCategoryPostSubmissions($user_id,$postCategories);
    public function getAllCategories();
    public function updateSubmissionResult($submission_id, $result);
    public function insertUserQA(array $analysisQAArray);
    public function getStudentLearningPath($student_id);
    public function getStudentLearningPaths($student_id);
    public function getLearningPathsWithIds($ids);
    public function getLearningPathById($id);
    public function getLearningPathByName($name);
    public function storeLearningPathUser(LearningPathUser $learningPathUser);
    public function deleteLearningPathUser($user_id);
    public function getLearnPathStages($learning_path_id, $is_default=true);
    public function getStageActivities($user_id, $stage_number, $learning_path_id, $is_default=true);
    public function getLearningPathActivity($learning_path_id, $activity_id, $is_default=true);
    public function getStageActivitiesIds($stage_number, $learning_path_id, $is_default=true);
    public function checkIfStudentInLearningPath($learning_path_id, $student_id);
    public function checkIfActivityInLearningPath($learning_path_id, $activity_id, $is_default=true);
    public function getActivityInStage($learning_path_id, $stage_number, $activity_id, $is_default=true);
    public function getUserTaskProgressActivity($task, $user_id, $activity_id, $is_default = true);
    public function getHtmlBlocklyCategories();
    public function getUserTaskProgressActivityLearningPath(Task $task, $user_id, $learning_path_id, $activity_id, $is_default = true);
    public function storeLearningPathActivityProgress(LearningPathActivityProgress $learningPathActivityProgress);
    public function deleteLearningPathActivityProgress(LearningPathActivityProgress $learningPathActivityProgress);
    public function getUserTaskProgress($user_id, $learning_path_id, $task_id);
    public function getSolvedTasksInActivity($user_id,$learning_path_id,$activity_id,$count = null,$is_default= true);
    public function getAllTasksInActivity($activity_id, $count = null, $is_default = true);
    public function getAllTasksInStage($learning_path_id, $stage_number, $count = null, $is_default = true);

    //campus
    public function getAllCampuses($campus_ids=[]);
    public function getSupportCampuses($user,$gmt_difference );
    public function getRoundsInCampuses($campus_ids=[]);
    public function getAllClasses();
    public function getAllTasks();
    public function getCampusClasses($campus_id);
    public function getRoundSubmissions($campus_id, $round_id,$from_time,$to_time);
    public function getRoundStudentsSubmissions($campus_id, $round_id,$from_time,$to_time);
    public function getCampusById($campus_id);
    public function getCampusUserRound($campus_id,$user_id);
    public function getUserCampusRounds($user_id);
    public function getCampusRoundById($round_id);
    public function getStudentRunningRounds($user_id,$gmt_difference,$count=null);
    public function getCampusClassById($class_id);
    public function getSubmoduleById($sub_module_id);
    public function getRoundById($round_id);
    public function getCampusActivityTaskById($campusActivityTaskId);
    public function checkStudentInRound($student_id,$round_id);
    public function checkUserInRound($user_id,$round_id);
    public function checkStudentInCampus($student_id,$campus_id);
    public function getStudentRoundClasses($student_id,$campus_id,$round_id,$class_id,$is_default=true);
    public function checkClassInRound($class_id,$round_id);
    public function getCampusRoundTeachers($round_id);
    public function getStudentSolvedTaskInRound($round,$activity_id,$task_id);
    public function getMissionAngularUserAnswerByName($campus_activity_progress_id, $name);
    public function getMissionAngularUserAnswers($campus_activity_progress_id);
    public function getAllStudentsSolvingTaskInCampus($account_id,$campus_id, $activity_id, $task_id);
    public function checkStudentSolvedAllSubModuleTasks($student_id, $default_activity_id, $tasks_ids);
    public function checkIfStudentSolveTask($student_id,$campus_id, $activity_id, $task_id);
    public function getStudentSolvedTasksInRoundClass($round,$activity_id);

    public function checkClassInCampus($class_id,$campus_id);

    public function getCampusClassId($campus_id,$activity_id,$is_default=true);
    public function getRoundClasses($round_id);
    public function getCampusClassTasks($activity_id,$campus_id,$is_default=true);
    public function getCampusActivityTasks($activity_id,$campus_id,$is_default=true,$limit=null, $search = null, $order_by = null, $order_by_type = null);
    public function getStudentSolvedTasksInCampus($student_id, $campus_id, $count=null);
    public function getStudentSolvedTasksInClass($student, $campus_id,$activity_id,$campusRound,$is_default=true, $count=null);
    public function getAllTasksInCampus($campus_id, $count=null);
    public function getAllTasksInClass($campus_id,$activity_id,$learner,$campusRound,$is_default=true, $count=null);
    public function getCampusActivityTask($campus_id, $task_id);
    public function getRooms($round_class_id);
    public function getMaterialByRoomId($file_id);
    public function checkTaskInActivity($campus_id,$activity_id,$task_id,$is_default=true);
    public function getUserRoundTaskProgressActivity(Task $task, $user_id, $campus_id, $activity_id, $is_default = true);
    public function getCampusActivityQuestion(CampusActivityProgress $campusActivityProgress,Question $question);
    public function getDragQuestionAnswer(CampusActivityQuestion $campusActivityQuestion,DragCell $dragCell);
    public function getDropdownQuestionAnswer(CampusActivityQuestion $campusActivityQuestion,Dropdown $dropdown);
    public function getUserLastSeenTask($user_id,$campus_id,$activity_id,$task_id,$is_default=true);
    public function storeUserLastSeenTask(TaskLastSeen $taskLastSeen);

    public function storeUserTaskCurrentStep(TaskCurrentStep $taskCurrentStep);

    public function storeRoundActivityProgress(CampusActivityProgress $campusActivityProgress);
    public function storeMissionAngularUserAnswer(MissionAngularUserAnswer $missionAngularUserAnswer);
    public function deleteRoundActivityProgress(CampusActivityProgress $campusActivityProgress);
    public function checkIfCampusInSchool($campus_id, $school_id);
    public function getRoomById($room_id);
    public function storeRoomUser(RoomUser $roomUser);
    public function storeRoom(Room $room);
    public function checkIfActivityInCampus($campus_id, $activity_id, $is_default=true);
    public function getClassNotEvaluatedTasks($roundClassId,$account_id,$is_default=true,$count=null);
    public function getStuckStudentsPerClass($roundClassId,$account_id,$is_default=true,$count=null);
    public function getTopStudentsInCampusRound($account, $campus_id,$round_id, $limit=10);
    public function storeMaterial(Material $material);
    //admin
    public function getClassTasksAndRooms($class_id);
    public function getCampusClassTasksAndRooms($class_id);
    public function checkIfStudentInCampusRound($campus_round_id, $unAssignedStudent);
    public function checkIfTeacherInCampusRound($campus_round_id, $unAssignedStudent);
    public function getCampusRoundUser($campus_round_id, $unAssignedStudent);
    public function deleteCampusRoundUser(CampusUser $campusUser);
    public function deleteCampusRound(CampusRound $round);
    public function deleteCampusRoundClass(CampusRoundClass $roundClass);
    public function deleteRoom(Room $room);
    public function deleteRoomUser(RoomUser $roomUser);
    public function deleteCampusRoundActivityTask(CampusRoundActivityTask $roundTask);
    public function storeCampusRoundUser(CampusUser $campusUser);
    public function storeCampusRound(CampusRound $campusRound);
    public function checkRoundNameAvailability($campus_id,$round_name, User $user);
    public function storeSchoolCampus(SchoolCampus $schoolCampus);
    public function storeCampusRoundClass(CampusRoundClass $campusRoundClass);
    public function storeCampusRoundActivityTask(CampusRoundActivityTask $campusRoundActivityTask);
    public function storeCampusClass(CampusClass $class);
    public function storeCampusActivityTask(CampusActivityTask $campusTask);

    public function getCampusClass($campus_id, $activity_id, $is_default=true);
    public function getClassInCampusByOrder($order,$campus_id);
    public function getLastClassInCampus($campus_id);
    public function getCampusRoundClass($round_id, $class_id);
    public function getCampusRoundClassByActivityId(CampusRound $round, $activity_id,$is_default);
    public function getCampusRoundTaskByActivityId(CampusRound $round, $activity_id,$task_id,$is_default);
    public function getCampusTaskByActivityId($campus_id, $activity_id,$task_id,$is_default);
    public function getClassSubmissions($campus_id, $round_id, $activity_id,$limit, $search,$order_by, $is_default=true,$count=null);

    public function getRoundActivityTask($round_id, $campusActivityTask_id);
    public function getCampusActivityProgressById($campus_activity_progress_id);
    public function storeCampusActivityProgress(CampusActivityProgress $campusActivityProgress);
    public function storeCampusActivityQuestion(CampusActivityQuestion $campusActivityQuestion);
    public function storeDragQuestionAnswer(DragQuestionAnswer $dragQuestionAnswer);
    public function storeDropdownQuestionAnswer(DropdownQuestionAnswer $dropdownQuestionAnswer);
    public function getAllStudentsInCampuses($user_id , $limit, $search,$order_by,$order_by_type,$gmt_difference,$count=null);
    public function getAllStudentsInCampusesExceptInRunningRounds($user_id ,$rounds_id,$round_id, $limit, $search,$order_by,$count=null);
    public function getAllStudentsNotInRounds($school_id, array $rounds_id, $limit, $search,$order_by,$order_by_type,$count=null);
    public function getAllTeachersInCampuses($user_id , $limit, $search,$search_for,$order_by,$order_by_type,$count=null);
    public function deleteCampusUsers(User $student,$is_student=false);
    public function deleteCampusUser(CampusUser $student);
    public function UserNameAlreadyExist(UserDto $userDto);

    public function getTeacherRunningRounds($user_id,$gmt_difference,$count=null);
    public function getTeacherUpcomingRounds($user_id,$gmt_difference,$count=null);
    public function getTeacherEndedRounds($user_id,$gmt_difference,$count=null);
    public function getAllTeacherRounds($user_id,$gmt_difference,$count=null);

    public function getAdminRunningRounds($user_id,$gmt_difference,$count=null);
    public function getAdminUpcomingRounds($user_id,$gmt_difference,$count=null);
    public function getAdminEndedRounds($user_id,$gmt_difference,$count=null);
    public function getAllAdminRounds($user_id,$gmt_difference,$count=null);

    public function getRecentActivityRounds($user_id,$gmt_difference);
    public function getRecentActivityClasses($user_id,$gmt_difference);
    public function getRecentActivityTasks($user_id,$gmt_difference);
    public function getStudentsProgressInRound($user_id,$account_id,$round_id,$campus_id);
    public function checkStudentInRunningCampuses($user_id,array $rounds_id,$round_id,$gmt_difference);

    public function getMaterialsClass($round_class_id);

    public function checkRoomNameExists($roundClass_id,$name,$room_id=null);
    public function checkRoomTimeExists($roundClass_id,$name,$starts_at,$room_id=null);

    public function getMaterialById($material_id);
    public function getRoomsByMaterialId($material_id);
    public function deleteMaterial(Material $material);


    public function getEditorMissionByQuiz($campus_id,$activity_id,$task_id,$is_default);

    //LTI
    public function getLTIConsumerById($consumer_id);
    public function getLTIConsumerByKey($consumer_key);
    public function getLTIConsumerByAccountId($account_id);
    public function getUserByLtiId($user_lti_id);
    public function getRoundByLtiId($round_lti_id);
    public function checkIfSchoolAccountHasAdmin( $account_id);
    public function checkIfStudentInRunningRounds($user_id,$rounds_id);


    public function getTaskTypeTimeEstimate($type);
    public  function getMissionStepsCount($mission_id);
    public  function getMissionSectionsCount($mission_id,$count=true);
    public function getMissionSection($mission_id,$section_id);
    public function getSectionWithOrder($mission_id,$order);
    public function getSectionById($id);

    public function getTaskCurrentStep($user_id,$task_id,$activity_id,$campus_id,$is_default=true);


    public function getlessonsSearch($search,$limit,$user_id,$gmt_difference,$count,$isStudent);
    public function getProfessionalCampusesSearch($search,$limit,$user_id,$gmt_difference,$count,$isStudent);

    public function searchForTasks($search,$limit,$user_id,$user_role,$gmt_difference,$count);
    public function searchForCountTasks($search);

    public function getRoomUser($room,$user,$role);

    public function saveWatchedVideoFlag($round_id,$user_id);
    public function getWatchedVideoFlag($round_id,$user_id);
    public function getSolvedTasksInRound($campusRound ,$user);
    public function getAllTasksInRound($campus_id);
    public function getIntroDetails($round_id);
    public function getQuestionAnswerById($campus_activity_question_id,$question_type);

    public function getRankLevelById($id);
    public function getCampusDictionaryWords($campus_id);
    public function getCampusDictionaryWordsInActivity($campus_id,$activity_id);
    public function getStepIdForQuestion($question_id);
    public function getStepById($step_id);
    public function getDragChoiceId($campus_activity_question_id,$drag_cell_id);
    public function getDragChoiceById($drag_choice_id);


    public function getTaskDifficulty($task_id,$activity_id);
    public function getActivityTask($task_id,$activity_id);
    public function getMainTaskByBoosterTask($task_id,$activity_id);
    public function getBoosterTaskByMainTask($task_id,$activity_id);
    public function getStreaksActiveStatus($user_id,$campus_id,$gmt_difference);
    public function getStreaksWeek($user_id,$campus_id,$gmt_difference);
    public function getStreaksCount($user_id,$campus_id,$gmt_difference);
    public function getLatestStreakPeriod($userId, $campusId);
    public function getTodayStudentStreak($user_id,$xp_gained,$gmt_difference);
    public function updateStudentStreak($userStreak,$xp_gained);
    public function storeStudentStreak($user_id,$xp_gained,$campus_id);
//    public function storeCampusUserStatus($user_id,$campus_id,$xp,$rank_level_id);
    public function getStudentTasksByStatus($campus_id, $user_id,$status);
    public function getStudentTasksInClassByStatus($campus_id, $user_id,$status,$activity_id, $is_default = true);
    public function getMissionPerModuleCount($activity_id,$count=true);
    public function getCampusUserStatus($campus_id,$user_id);
    public function getAllCampusUserStatus($user_id);

    public function getEnrolledStudentsPerCourseCount($account_id,$campus_id,$status,$limit, $search, $order_by, $order_by_type);
    public function getEnrolledStudentsPerSubmodule($account_id,$campus_id,$module_id,$submodule_id,$count = false,$limit = null,$search = null,$order_by = null,$order_by_type = null);
    public function getCompletedStudentsPerCourseCount($account_id,$campus_id);
    public function getAllTasksPerModuleCount($campus_id,$default_activity_id);
    public function getProgressCountforStudentInModule($student_id,$campus_id,$default_activity_id);
    public function getUserRankLevelInCourse($student_id,$campus_id);
    public function getLearnerCampusUser($user_id,$campus_id);

    public function getAvailaibleTasksInCampus(User $user,CampusRound $campusRound, $count = null);
    public function getCampusModules($campus_id,$limit, $search,$order_by,$order_by_type,$count);
    public function getModuleSubmodules($module_id,$limit, $search,$order_by,$order_by_type,$count);
    public function getRoundLearners($round_id, $limit, $search,$order_by,$order_by_type,$count);
    public function toggleHideBlocks($learnerStatus);
    public function storeCampusUserStatus(CampusUserStatus $campusUserStatus);
    public function checkTodayStudentStreak($user_id,$xp_gained,$gmt_difference);
    public function resetStudentStreaks($user,$campus_id,$gmt_difference);
    public function checkStudentStreaksLastSeen($user,$campus_id,$gmt_difference);
    public function checkStudentCourseSeen($user,$campus_id);

    public function getAllScoringConstants();
    public function getScoringConstant($constantName);
    public function getMultipleScoringConstants($constantsNames);
    public function countRankLevels();
    public function countLevelsByRankId($rank_id);
    public function getMaxRankLevel($rank_id);
    public function getNextRank($rank_id);
    public function getAngularMissionByQuiz($campus_id, $activity_id, $quiz_id, $is_default);
    public function getTaskById($task_id);
    public function getAllDropdownTranslations();
    public function getDropdownQuestionById($id);
    public function deleteDropdown($dropdown);
    public function getAllProgress();
    public function getAllLastSeen();
    public function getEditorMissions();
    public function getAllMissionCoding($type);
    public function storeMissionCoding($mission);
    public function getLearnerCurrentRoundinCampus($user,$campus_id);
    public function getLastAddedStreak($user_id,$campus_id);
    public function storeSearchVault($searchVault);
    public function getProductCoursesIds($product_id);
    public function getFirstSubmoduleInModule($module);
    public function getSubmoduleInModuleByOrder($module,$order);
    public function hasFinishedSubmoduleTasks($user,$campus_id,$submodule);
    public function getUserSolvedSubmoduleMainTasks($user,$round,$submodule,$count=false);
    public function getSubmoduleMainTasks($submodule,$count=false);
    public function getLastMainTaskInSubmodule($submodule);
    public function getSubmoduleTaskOrder($task_id,$submodule_id);
    public function getSubmoduleTaskOrderByOrder($order,$submodule_id);
    public function isSubmoduleHaveTask($submodule_id, $task_id);
    public function deleteAllModuleSubmodules($submodules);
}