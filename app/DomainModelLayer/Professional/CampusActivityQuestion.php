<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:27 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Task;

class CampusActivityQuestion extends Entity
{
    public function __construct(CampusActivityProgress $campusActivityProgress,Question $question)
    {
        $this->campusActivityProgress = $campusActivityProgress;
        $this->question = $question;
        
    }

    public function getCampusActivityProgress(){
        return $this->campusActivityProgress;
    }
    public function getCampusActivityQuestion(){
        return $this->campusActivityQuestion;
    }


    
}