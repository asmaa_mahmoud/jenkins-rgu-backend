<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:51 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\Framework\Exceptions\BadRequestException;

class CampusActivityTask extends Entity
{
    public function __construct(Campus $campus, Task $task){
        $this->campus = $campus;
        $this->task = $task;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCampus()
    {
        return $this->campus;
    }
    public function getCampus_id()
    {
        return $this->campus->id;
    }

    public function getTask()
    {
        return $this->task;
    }
    public function getTask_id()
    {
        return $this->task->id;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
        $this->defaultActivity = $defaultActivity;
    }

    public function getCampusRoundActivityTasks(){
        return $this->campusRoundActivityTasks;
    }

    public function getTask_name(){
        $task=$this->task;
        if($task->getMissions()->first() != null) {
            return $task->getMissions()->first()->getName();

        }
        elseif($task->getMissionsHtml()->first() != null) {
            return $task->getMissionsHtml()->first()->getTitle();

        }
        elseif($task->getMissionCoding() != null) {
            return $task->getMissionCoding()->getTitle();

            //;
        }
        elseif($task->getMissionEditor() != null){
            return $task->getMissionEditor()->getTitle();

        }
        elseif($task->getQuizzes()->first()!=null){
            return $task->getQuizzes()->first()->getTitle();

        }else{
            throw new BadRequestException(trans("task with id ".$task->getId()."locale.has_unknown_type"));
        }

    }

    public function getTask_type(){
        $task=$this->task;
        if($task->getMissions()->first() != null) {

            return'mission';
        }
        elseif($task->getMissionsHtml()->first() != null) {

            return 'html_mission';
        }
        elseif($task->getMissionCoding() != null) {

            return 'coding_mission';
            //;
        }
        elseif($task->getMissionEditor() != null){
            return 'editor_mission';
        }
        elseif($task->getQuizzes()->first()!=null){
            return $task->getQuizzes()->first()->getType()->getName();
        }else{
            throw new BadRequestException(trans("task with id ".$task->getId()."locale.task_has_unknown_type"));
        }

    }

    public function getTaskFullMark($showLessons = true, $showBlocks = true){
        if($showLessons && $showBlocks)
            return $this->xp_lesson_1_blockly_1;

        if($showLessons && !$showBlocks)
            return $this->xp_lesson_1_blockly_0;

        if(!$showLessons && $showBlocks)
            return $this->xp_lesson_0_blockly_1;

        return $this->xp_lesson_0_blockly_0;
    }
}