<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 2:30 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;

class ProjectMap extends EntityMap
{
    protected $table = 'project';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "project.deleted_at";

    public function translations(Project $project){
        return $this->hasMany($project, ProjectTranslation::class , 'project_id', 'id');
    }

    public function resources(Project $project){
        return $this->hasMany($project, ProjectResource::class , 'project_id', 'id');
    }

    public function task(Project $project){
        return $this->belongsTo($project,Task::class,'project_id','id');
    }
}