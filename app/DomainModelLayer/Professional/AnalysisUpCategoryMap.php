<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 04-Mar-19
 * Time: 10:42 AM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;

class AnalysisUpCategoryMap extends EntityMap{

    protected $table = 'analysis_up_category';
    public $timestamps = true;

    public function categories(AnalysisUpCategory $analysisUpCategory){
        return $this->hasMany($analysisUpCategory, AnalysisCategory::class,'up_category_id','id');
    }

    public function translations(AnalysisUpCategory $analysisUpCategory){
        return $this->hasMany($analysisUpCategory, AnalysisUpCategoryTranslation::class,'up_category_id','id');
    }
}