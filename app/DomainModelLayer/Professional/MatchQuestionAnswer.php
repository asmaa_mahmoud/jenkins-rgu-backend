<?php

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\MatchValue;

class MatchQuestionAnswer extends Entity
{
    public function __construct(CampusActivityQuestion $campusActivityQuestion,MatchKey $matchKey,MatchValue $matchValue)
    {
        $this->campusActivityQuestion=$campusActivityQuestion;
        $this->matchKey=$matchKey;
        $this->matchValue=$matchValue;
    }

    public function getMatchKey(){
        return $this->matchKey;
    }
    public function getMatchValue(){
        return $this->matchValue;
    }
    public function getCampusActivityQuestion(){
        return $this->campusActivityQuestion;
    }

    public function setMatchKey($matchKey){
        $this->matchKey=$matchKey;
    }
    public function setMatchValue($matchValue){
        $this->matchValue=$matchValue;
    }
}