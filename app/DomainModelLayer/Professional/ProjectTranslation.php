<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 2:30 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
//use App\ApplicationLayer\Professional\Dtos\ProjectTranslationDto;
use Carbon\Carbon;
use LaravelLocalization;

class ProjectTranslation extends Entity
{
    public function __construct(/*ProjectTranslationDto $projectTranslationDto*/){

    }

    public function getId(){
        return $this->id;
    }


    public function getTitle(){
        return $this->title;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

}