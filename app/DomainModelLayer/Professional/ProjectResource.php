<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 2:30 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
//use App\ApplicationLayer\Professional\Dtos\ProjectResourceDto;
use Carbon\Carbon;
use LaravelLocalization;

class ProjectResource extends Entity
{

    public function __construct(/*ProjectResourceDto $projectResourceDto*/){

    }

    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getResourceUrl(){
        return $this->resource_url;
    }

}