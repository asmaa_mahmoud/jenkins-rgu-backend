<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 9:38 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Professional\Submodule;

class CampusClassMap extends EntityMap
{
    protected $table = 'class';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "class.deleted_at";

    public function campus(CampusClass $campusClass){
        return $this->belongsTo($campusClass, Campus::class, 'campus_id', 'id');
    }

    public function activity(CampusClass $campusClass){
        return $this->belongsTo($campusClass, Activity::class, 'activity_id', 'id');
    }

    public function defaultActivity(CampusClass $campusClass)
    {
        return $this->belongsTo($campusClass, DefaultActivity::class , 'default_activity_id', 'id');
    }


    public function courseModulesObjectives(CampusClass $campusClass)
    {
        return $this->hasMany($campusClass, CourseModulesObjectives::class , 'class_id', 'id');
    }

    public function submodules(CampusClass $campusClass)
    {
        return $this->hasMany($campusClass, Submodule::class , 'class_id', 'id')->orderBy('order');
    }

    public function campusWelcomeModules(CampusClass $campusClass)
    {
        return $this->hasone($campusClass, CampusWelcomeModules::class , 'class_id', 'id');
    }

    public function project(CampusClass $campusClass)
    {
        return $this->hasone($campusClass, Project::class , 'class_id', 'id');
    }
  
}