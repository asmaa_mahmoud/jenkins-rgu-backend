<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use  App\DomainModelLayer\Professional\AnalysisQuestion;
use LaravelLocalization;

class LTIConsumer extends Entity
{

    public function __construct()
    {

    }



    public function getAccount()
    {
        return $this->account;
    }
    public function getName()
    {
        return $this->name;
    }


}