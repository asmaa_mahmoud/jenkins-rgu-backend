<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:35 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class CampusRoundMap extends EntityMap
{
    protected $table = 'campus_round';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_round.deleted_at";

    public function campus(CampusRound $round){
        return $this->belongsTo($round, Campus::class , 'campus_id','id' );
    }

    public function users(CampusRound $round){
        return $this->belongsToMany($round, User::class, 'campus_user', 'campus_round_id', 'user_id');
    }

    public function campusUsers(CampusRound $round){
        return $this->hasMany($round, CampusUser::class, 'campus_round_id', 'id');
    }

    public function campusRoundClasses(CampusRound $round){
        return $this->hasMany($round, CampusRoundClass::class, 'campus_round_id', 'id')->orderBy('id','ASC');
    }
    public function campusRoundTasks(CampusRound $round){
        return $this->hasMany($round, CampusRoundActivityTask::class, 'campus_round_id', 'id');
    }

    public function tasksUsersCurrentSteps(CampusRound $round){
        return $this->hasMany($round, TaskCurrentStep::class, 'round_id');
    }
}