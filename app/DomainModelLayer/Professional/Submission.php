<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\SubmissionDto;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\User;

class Submission extends Entity
{

    public function __construct(SubmissionDto $submissionDto )
    {
        $this->user_id=$submissionDto->user_id;
        $this->final_result=$submissionDto->final_result;
        $this->category_code=$submissionDto->category_code;
    }

    
    public function getFinalResult()
    {
        return $this->final_result;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setFinalResult($finalResult)
    {
        $this->final_result=$finalResult;
    }
    public function setVisualProfile($visualProfile)
    {
        $this->visual_profile=$visualProfile;
    }

    public function setUserId(User $user)
    {
        $this->user_id=$user->id;
    }

    
  
}