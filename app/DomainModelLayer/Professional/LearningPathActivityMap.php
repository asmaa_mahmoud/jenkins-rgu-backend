<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 2:17 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class LearningPathActivityMap extends EntityMap
{
    protected $table = 'learning_path_activity';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "learning_path_activity.deleted_at";

    public function activity(LearningPathActivity $learningPathActivity){
        return $this->belongsTo($learningPathActivity, Activity::class ,'activity_id', 'id');
    }

    public function defaultActivity(LearningPathActivity $learningPathActivity){
        return $this->belongsTo($learningPathActivity, DefaultActivity::class ,'default_activity_id' , 'id');
    }

    public function learningPath(LearningPathActivity $learningPathActivity){
        return $this->belongsTo($learningPathActivity, LearningPath::class, 'learning_path_id', 'id');
    }
}