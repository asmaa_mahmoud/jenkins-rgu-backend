<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:20 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;

class TaskCurrentStep extends Entity
{
    public function __construct(Task $task, User $user, Campus $campus,$current_step,$current_section,$section_id)
    {
        $this->task = $task;
        $this->user = $user;
        $this->campus = $campus;
        $this->current_step= $current_step;
        $this->current_section= $current_section;
        $this->section_id= $section_id;
    }

    public function getId(){
        return $this->id;
    }



    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
        $this->default_activity = $defaultActivity;
    }

    public function getCampus()
    {
        return $this->campus;
    }

    public function setCampus(Campus $campus)
    {
        $this->campus = $campus;
    }


    public function getCurrentStep()
    {
        return $this->current_step;
    }

    public function setCurrentStep($current_step)
    {
        $this->current_step = $current_step;
    }


    public function getCurrentSection()
    {
        return $this->current_section;
    }

    public function setCurrentSection($current_section)
    {
        $this->current_section = $current_section;
    }


    public function getSectionId()
    {
        return $this->section_id;
    }

    public function setSectionId($section_id)
    {
        $this->section_id = $section_id;
    }
}