<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 1:14 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use Carbon\Carbon;

class CampusRoundClass extends Entity
{
    public function __construct(CampusClass $campusClass, CampusRound $round, $weight, $starts_at, $ends_at, $due_date = null){
        $this->campusClass = $campusClass;
        $this->campusRound = $round;
        $this->weight = $weight;
        $this->starts_at = $starts_at;
        $this->ends_at = $ends_at;
        $this->due_date = $due_date ?? $ends_at;
    }

    public function getCampusRound()
    {
        return $this->campusRound;
    }

    public function getCampus()
    {
        return $this->campusRound->campus;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getCampusClass()
    {
        return $this->campusClass;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        return $this->weight = $weight;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }

    public function setStartsAt($starts_at){
        $this->starts_at = $starts_at;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getDueDate(){
        return $this->due_date;
    }

    public function setDueDate($due_date){
        $this->due_date = $due_date;
    }

    public function getDuration(){
        $startDate = Carbon::createFromTimestamp(intval($this->ends_at));
        $endDate = Carbon::createFromTimestamp(intval($this->starts_at));
        return $endDate->diffInDays($startDate);
    }

    public function getRooms(){
        return $this->rooms;
    }
    public function getClass(){
        return $this->campusClass;
    }
    public function getMaterials(){
        return $this->materials;
    }

}