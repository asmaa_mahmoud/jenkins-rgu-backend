<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 9:38 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class CourseModulesObjectivesMap extends EntityMap
{
    protected $table = 'course_modules_objectives';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "course_modules_objectives.deleted_at";

    // public function campusClass(CourseModulesObjectives $courseModulesObjectives){
    //     return $this->belongsTo($courseModulesObjectives, CampusClass::class, 'class_id', 'id');
    // }


  
}