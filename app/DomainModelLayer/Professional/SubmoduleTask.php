<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Journeys\Task;

class SubmoduleTask extends Entity
{
        
    public function getId(){
        return $this->id;
    }

    public function getSubmodule(){
        return $this->submodule;
    }

    
    public function getTask(){
        return $this->task;
    }

    public function getOrder(){
        return $this->order;
    }

   

}