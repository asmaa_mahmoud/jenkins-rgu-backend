<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 11:35 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class SearchVaultMap extends EntityMap
{
    protected $table = 'search_vault';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "search_vault.deleted_at";

}