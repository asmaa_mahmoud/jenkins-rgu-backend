<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:27 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;

class DragQuestionAnswer extends Entity
{
    public function __construct(CampusActivityQuestion $campusActivityQuestion,DragCell $dragCell,DragChoice $dragChoice)
    {
        $this->campusActivityQuestion=$campusActivityQuestion;
        $this->dragCell=$dragCell;
        $this->dragChoice=$dragChoice;

    }

    public function getDragCell(){
        return $this->dragCell;
    }
    public function getDragChoice(){
        return $this->dragChoice;
    }
    public function setDragChoice($dragChoice){
        $this->dragChoice=$dragChoice;
    }
    public function getCampusActivityQuestion(){
        return $this->campusActivityQuestion;
    }


    
}