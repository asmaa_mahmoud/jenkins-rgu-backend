<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisCategoryTranslationDto;
use App\DomainModelLayer\Professional\AnalysisCategory;

class AnalysisCategoryTranslation extends Entity
{

    public function __construct(AnalysisCategoryTranslationDto $analysisCategoryTranslationDto )
    {
 
        
    }

   

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getAnalysisCategoryId()
    {
        return $this->analysis_category_id;
    }
     public function setLanguageCode($language_code)
    {
         $this->language_code=$language_code;
    }
    public function setName($name)
    {
         $this->name=$name;
    }

    public function setAnalysisCategoryId(AnalysisCategory $anlysisCategory)
    {
        $this->analysis_category_id=$anlysisCategory->id;
    }

    

    

    

    
    
}