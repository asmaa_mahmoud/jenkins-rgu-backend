<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:27 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Professional\CampusActivityProgress;

class MissionAngularUserAnswer extends Entity
{
    public function __construct(CampusActivityProgress $campusActivityProgress, $name, $data)
    {
        $this->campusActivityProgress = $campusActivityProgress;
        $this->name = $name;
        $this->data = $data;
        
    }

    public function getCampusActivityProgress(){
        return $this->campusActivityProgress;
    }

    public function setCampusActivityProgress($campusActivityProgress)
    {
        $this->campusActivityProgress = $campusActivityProgress;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }



    
}