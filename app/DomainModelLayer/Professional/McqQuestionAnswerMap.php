<?php

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Choice;

class McqQuestionAnswerMap extends EntityMap
{
    protected $table = 'mcq_question_answers';
    public $timestamps = true;


    public function campusActivityQuestion(McqQuestionAnswer $mcqQuestionAnswer)
    {
        return $this->belongsTo($mcqQuestionAnswer, CampusActivityQuestion::class , 'campus_activity_question_id', 'id');
    }

    public function choice(McqQuestionAnswer $mcqQuestionAnswer)
    {
        return $this->belongsTo($mcqQuestionAnswer, Choice::class , 'choice_id', 'id');
    }


   
}