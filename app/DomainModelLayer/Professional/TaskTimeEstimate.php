<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 09-Apr-19
 * Time: 11:24 AM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;

class TaskTimeEstimate extends Entity{

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getTime(){
        return $this->time;
    }
    public function setTime($time){
        $this->time = $time;
    }
}