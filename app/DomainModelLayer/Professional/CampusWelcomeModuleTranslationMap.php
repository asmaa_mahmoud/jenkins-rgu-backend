<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 9:38 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;


class CampusWelcomeModuleTranslationMap extends EntityMap
{
    protected $table = 'campus_welcome_module_translation';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "campus_welcome_module_translation.deleted_at";



}