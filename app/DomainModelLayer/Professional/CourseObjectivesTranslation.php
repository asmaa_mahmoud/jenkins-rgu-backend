<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\Entity;

class CourseObjectivesTranslation extends Entity
{


    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getObjective()
    {
        return $this->objective;
    }



}