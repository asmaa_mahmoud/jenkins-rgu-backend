<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;

class CourseObjectivesTranslationMap extends EntityMap
{

    protected $table = 'course_objectives_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "course_objectives_translation.deleted_at";


}