<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use  App\DomainModelLayer\Professional\AnalysisQuestion;
use LaravelLocalization;

class AnalysisAnswer extends Entity
{

    public function __construct(AnalysisAnswerDto $analysisAnswerDto)
    {
//        $this->factor=$analysisAnswerDto->factor;
//        $this->deduction=$analysisAnswerDto->deduction;
//        $this->question_id=$analysisAnswerDto->question->id;

        
    }

    public function setId($id){
        $this->id=$id;
    }
    public function getId(){
        return $this->id;
    }


    public function getFactor()
    {
        return $this->factor;
    }
    public function getDeduction()
    {
        return $this->deduction;
    }
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    public function getQuestion()
    {
        return $this->question;
    }

      public function getNextQuestion()
    {
        return $this->nextQuestion;
    }


    public function getQuestionId()
    {
        return $this->question->id;
    }

    public function getNextQuestionId()
        //$nextQuestionId
    {
       // return $this->nextQuestion->id;
        return $this->next_question_id;
    }




    public function setFactor($factor)
    {
         $this->factor=$factor;
    }

    public function setDeduction($deduction)
    {
        $this->deduction=$deduction;
    }

    public function setQuestionId($question_id)
    {
       $this->question_id=$question_id;
    }

    public function setNextQuestionId($question_id)
    {
        $this->next_question_id=$question_id;
    }
    public function getTranslations(){
        return $this->translations;
    }


    public function getAnswerDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getAnswerDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getAnswerDescription();
        }
        return $english_translation;
    }
}