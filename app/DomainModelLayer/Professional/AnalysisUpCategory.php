<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 04-Mar-19
 * Time: 10:41 AM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use LaravelLocalization;

class AnalysisUpCategory extends Entity{

    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getIconUrl(){
        return $this->iconUrl;
    }

    public function getCategories(){
        return $this->categories;
    }
}