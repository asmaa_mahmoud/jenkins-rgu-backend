<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 1:13 PM
 */

namespace App\DomainModelLayer\Professional;
use App\DomainModelLayer\Accounts\User;
use Analogue\ORM\EntityMap;


class SubmissionMap extends EntityMap
{
    protected $table = 'submissions';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "submissions.deleted_at";



    public function user(Submission $submission){
        return $this->belongsTo($submission,User::class,'user_id','id');
    }

    public function analysisQAs(Submission $submission)
    {
        return $this->hasMany($submission, AnalysisQA::class, 'submission_id', 'user_assessment_id');
    }



}