<?php

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Choice;


class McqQuestionAnswer extends Entity
{
    public function __construct(CampusActivityQuestion $campusActivityQuestion,Choice $choice ,$confidence)
    {
        $this->campusActivityQuestion=$campusActivityQuestion;
        $this->choice=$choice;
        $this->confidence=$confidence;
    }

    public function getChoices(){
        return $this->choice;
    }
    public function getCampusActivityQuestion(){
        return $this->campusActivityQuestion;
    }
    public function setChoice($choice){
        $this->choice=$choice;
    }

}