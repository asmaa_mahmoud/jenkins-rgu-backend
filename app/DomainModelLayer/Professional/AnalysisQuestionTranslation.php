<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionTranslationDto;
use App\DomainModelLayer\Professional\AnalysisQuestion;

class AnalysisQuestionTranslation extends Entity
{

    public function __construct(AnalysisQuestionTranslationDto $analysisQuestionTranslationDto )
    {
 
        
    }

    
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getQuestionDescription()
    {
        return $this->question_description;
    }
    public function getQuestion()
    {
        return $this->question;
    }

     public function getQuestionId()
    {
        return $this->question->id;
    }

     public function setLanguageCode($language_code)
    {
         $this->language_code=$language_code;
    }

    public function setQuestionDescription($question_description)
    {
         $this->question_description=$question_description;
    }

     public function setQuestionId(AnalysisQuestion $question)
    {
       $this->question_id=$question->id;
    }

     
  
}