<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionDto;
use LaravelLocalization;

class AnalysisQuestion extends Entity
{

    public function __construct(AnalysisQuestionDto $analysisQuestionDto )
    {
 
        
    }




    public function setId($id){
        $this->id=$id;
    }
    public function getId(){
        return $this->id;
    }

    public function getAnalysisCategory()
    {

        return $this->analysisCategory;
    }
    public function getIsMultiple(){
        return $this->is_multiple;
    }
    public function getVisualFormat(){
        return $this->visual_format;
    }

    public function getOrder(){
        return $this->order_in_category;
    }


    public function setAnalysisCategory( $anlysisCategory_id)
    {
        $this->analysis_category_id=$anlysisCategory_id;
    }

    //relations
    public function getAnswers( )
    {

       return $this->answers;
    }
    public function getTranslations( )
    {

        return $this->translations;
    }
    public function getQuestionDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getQuestionDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getQuestionDescription();
        }
        return $english_translation;
    }
  
}