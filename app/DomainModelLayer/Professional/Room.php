<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 3:37 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;

class Room extends Entity
{
    public function __construct(CampusRoundClass $roundClass){
        $this->roundClass = $roundClass;
    }

    public function getId(){
        return $this->id;
    }

    public function getRoundClass(){
        return $this->roundClass;
    }

    public function getBBBId(){
        return $this->bbb_id;
    }

    public function setBBBId($bbb_id){
        $this->bbb_id = $bbb_id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function isRunning(){
        return $this->is_running;
    }

    public function setIsRunning($is_running){
        $this->is_running = $is_running;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }

    public function setStartsAt($starts_at){
        return $this->starts_at = $starts_at;
    }

    public function getEndedAt(){
        return $this->ended_at;
    }

    public function setEndedAt($ended_at){
        return $this->ended_at = $ended_at;
    }

    public function getSessions(){
        return $this->sessions;
    }

    public function getFileLink()
    {
        if($this->material != null)
            return $this->material->getFileLink();
        else
            return null;
    }

    public function getMaterial(){
        return $this->material;
    }

//    public function setFileLink($link)
//    {
//        $this->file_link = $link;
//    }

    public function getRoomUsers()
    {
        return $this->roomUsers;
    }

    public function getMaterialId()
    {
        return $this->file_id;
    }

    public function setMaterialId($material_id) {
        $this->file_id = $material_id;
    }
}