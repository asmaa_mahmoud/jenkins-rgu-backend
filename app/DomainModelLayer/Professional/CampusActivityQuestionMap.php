<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:34 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;

class CampusActivityQuestionMap extends EntityMap
{
    protected $table = 'campus_activity_question';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "campus_activity_question.deleted_at";

    public function campusActivityProgress(CampusActivityQuestion $campusActivityQuestion)
    {
        return $this->belongsTo($campusActivityQuestion, CampusActivityProgress::class , 'campus_activity_progress_id', 'id');
    }

    public function question(CampusActivityQuestion $campusActivityQuestion)
    {
        return $this->belongsTo($campusActivityQuestion, Question::class , 'question_id', 'id');
    }

   
}