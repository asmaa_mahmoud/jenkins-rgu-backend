<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;

class DictionaryWordMap extends EntityMap
{

    protected $table = 'dictionary_word';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dictionary_word.deleted_at";


    public function campus(){
        return $this->belongsTo('App\Models\Professional\Campus', 'id');
    }

    public function translations(DictionaryWord $dictionaryWord){
        return $this->hasMany($dictionaryWord, DictionaryWordTranslation::class , 'dictionary_word_id', 'id');
    }

}