<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 3:47 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class ScoringConstantMap extends EntityMap
{
    protected $table = 'scoring_constants';
    public $timestamps = false;
    public $softDeletes = false;
}