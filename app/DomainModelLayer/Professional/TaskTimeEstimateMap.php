<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 09-Apr-19
 * Time: 11:25 AM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;

class TaskTimeEstimateMap extends EntityMap{

    protected $table = 'task_time_estimate';
    public $timestamps = true;
    public $softDeletes = true;

}