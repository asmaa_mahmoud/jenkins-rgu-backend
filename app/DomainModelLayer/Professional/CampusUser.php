<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:08 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;

class CampusUser extends Entity
{
    public function __construct(CampusRound $round, User $user, Role $role){
        $this->campusRound = $round;
        $this->user = $user;
        $this->role = $role;
    }

    public function getId(){
        return $this->id;
    }

    public function getCampus(){
        return $this->campusRound->campus;
    }

    public function getHasSeen(){
        return $this->hasSeen;
    }

    public function getCampusRound(){
        return $this->campusRound;
    }

    public function getUser(){
        return $this->user;
    }

    public function getRole(){
        return $this->role;
    }


//    public function setCampusRound(CampusRound $campusRound){
//         $this->campusRound=$campusRound;
//    }
//
//    public function setUser(User $user){
//         $this->user=$user;
//    }
//
//    public function setRole(Role $role){
//         $this->role=$role;
//    }
}