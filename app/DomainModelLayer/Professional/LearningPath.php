<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 1:49 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;

class LearningPath extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        return $this->description = $description;
    }

    public function getLearningPathType(){
        return $this->learningPathType;
    }

    public function setLearningPathType(LearningPathType $type){
        return $this->learningPathType = $type;
    }

    public function getActivities(){
        return $this->activities;
    }

    public function getDefaultActivities(){
        return $this->defaultActivities;
    }

    public function getLearningPathActivities(){
        return $this->learningPathActivities;
    }

    public function addLearningPathActivity(LearningPathActivity $learningPathActivity){
        $this->learningPathActivities->push($learningPathActivity);
    }

    public function getDefaultTasksCount(){
        $countTasks = 0;
        $activities = $this->getDefaultActivities();
        foreach ($activities as $activity){
            $countTasks += $activity->getTasksCount();
        }
        return $countTasks;
    }

    public function getTasksCount(){
        $countTasks = 0;
        $activities = $this->getActivities();
        foreach ($activities as $activity){
            $countTasks += $activity->getTasksCount();
        }
        return $countTasks;
    }

    public function getUsers(){
        return $this->users;
    }

    public function isMember($user_id){
        $users = $this->getUsers();
        foreach ($users as $user){
            if($user->getId() == $user_id)
                return true;
        }
        return false;
    }

    public function getLearningPathUsers(){
        return $this->learningPathUsers;
    }

    public function addCampUser(LearningPathUser $learningPathUser){
        $this->learningPathUsers->push($learningPathUser);
    }

}