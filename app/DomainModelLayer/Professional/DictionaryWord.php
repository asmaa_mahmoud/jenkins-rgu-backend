<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:09 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\Entity;
use App\ApplicationLayer\Professional\Dtos\CampusTranslationDto;
use LaravelLocalization;
class DictionaryWord extends Entity
{


    public function getCampusId()
    {
        return $this->campus_id;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getWord(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getWord();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getWord();
        }
        return $english_translation;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }


    

}