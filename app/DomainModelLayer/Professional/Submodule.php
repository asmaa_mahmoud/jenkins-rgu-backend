<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 3:58 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use LaravelLocalization;
class Submodule extends Entity
{
   

    public function getId(){
        return $this->id;
    }

    public function getOrder(){
        return $this->order;
    }
    public function getModule(){
        return $this->campusClass;
    }
    public function getTasks(){
        return $this->tasks;
    }

    
     public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    
}