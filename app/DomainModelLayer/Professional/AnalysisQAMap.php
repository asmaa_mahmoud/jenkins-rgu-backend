<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 1:13 PM
 */

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;


class AnalysisQAMap extends EntityMap
{
    protected $table = 'analysis_qa';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "user_assessments.deleted_at";

    public function question(AnalysisQA $analysisQA){
        return $this->belongsTo($analysisQA,AnalysisQuestion::class,'question_id','id');
    }

    public function answer(AnalysisQA $analysisQA){
        return $this->belongsTo($analysisQA,AnalysisAnswer::class,'answer_id','id');
    }
    public function submission(AnalysisQA $analysisQA){
        return $this->hasMany($analysisQA,Submission::class,'submission_id','id');
    }

}