<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/6/2018
 * Time: 1:13 PM
 */

namespace App\DomainModelLayer\Professional;
use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Account;


class LTIConsumerMap extends EntityMap
{
    protected $table = 'lti2_consumer';
    protected $primaryKey="consumer_pk";


    public function account(LTIConsumer $consumer){
        return $this->belongsTo($consumer,Account::class,'account_id','id');
    }



}