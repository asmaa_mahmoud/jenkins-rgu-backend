<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 9:52 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\School;

class SchoolCampus extends Entity
{
    public function __construct(School $school, Campus $campus, User $creator, $no_of_classes, $creator_type, $locked = 0){
        $this->school = $school;
        $this->campus = $campus;
        $this->creator = $creator;
        $this->no_of_classes = $no_of_classes;
        $this->creator_type = $creator_type;
        $this->locked = $locked;
    }

    public function getId(){
        return $this->id;
    }

    public function getNoOfClasses(){
        return $this->no_of_classes;
    }

    public function setNoOfClasses($noOfClasses){
        $this->no_of_classes = $noOfClasses;
    }

    public function getCreatorType(){
        return $this->creator_type;
    }

    public function setCreatorType($creatorType){
        $this->creator_type = $creatorType;
    }

    public function getCreator(){
        return $this->creator;
    }

    public function setCreator(User $creator){
        $this->creator = $creator;
    }

    public function getLocked(){
        return $this->locked;
    }

    public function setLocked($locked){
        $this->locked = $locked;
    }

    public function getSchool(){
        return $this->school;
    }

    public function getCampus(){
        return $this->campus;
    }

//    public function addDistributorCampus(SchoolDistributorCampus $distributorCampus){
//        $this->distributorCampus->push($distributorCampus);
//    }
//
//    public function getDistributorCampus(){
//        return $this->distributorCampus;
//    }
}