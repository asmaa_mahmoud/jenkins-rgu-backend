<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/11/2018
 * Time: 3:56 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;

class RoomUserMap extends EntityMap
{
    protected $table = 'room_user';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "room_user.deleted_at";

    public function user(RoomUser $roomUser){
        return $this->belongsTo($roomUser, User::class , 'user_id', 'id');
    }

    public function role(RoomUser $roomUser){
        return $this->belongsTo($roomUser, Role::class , 'role_id', 'id');
    }

    public function room(RoomUser $roomUser){
        return $this->belongsTo($roomUser, Room::class , 'room_id', 'id');
    }
}