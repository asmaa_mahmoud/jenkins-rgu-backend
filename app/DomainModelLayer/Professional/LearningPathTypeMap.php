<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 11/09/2018
 * Time: 1:46 PM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;

class LearningPathTypeMap extends EntityMap
{
    protected $table = 'learning_path_type';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "learning_path_type.deleted_at";

    public function learningPaths(LearningPathType $learningPathType){
        return $this->hasMany($learningPathType, LearningPath::class , 'learning_path_type_id', 'id');
    }
}