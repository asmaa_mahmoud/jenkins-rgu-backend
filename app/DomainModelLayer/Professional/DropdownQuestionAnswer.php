<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:27 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\DropdownChoice;

class DropdownQuestionAnswer extends Entity
{
    public function __construct(CampusActivityQuestion $campusActivityQuestion,Dropdown $dropdown,DropdownChoice $dropdownChoice)
    {
        $this->campusActivityQuestion=$campusActivityQuestion;
        $this->dropdown=$dropdown;
        $this->dropdownChoice=$dropdownChoice;

       
        
    }

    public function getDropdownChoice(){
        return $this->dropdownChoice;
    }
   
    public function setDropdownChoice($dropdownChoice){
        $this->dropdownChoice=$dropdownChoice;
    }
    public function getCampusActivityQuestion(){
        return $this->campusActivityQuestion;
    }


    
}