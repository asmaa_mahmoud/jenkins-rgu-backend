<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;

class SubmoduleTaskMap extends EntityMap
{
    protected $table = 'sub_module_task';
    public $timestamps = true;

    public function submodule(SubmoduleTask $submoduleTask)
    {
        return $this->belongsTo($submoduleTask, Submodule::class , 'sub_module_id', 'id');
    }
   

    public function task(SubmoduleTask $submoduleTask)
    {
        return $this->belongsTo($submoduleTask, Task::class , 'task_id', 'id');
    }

}