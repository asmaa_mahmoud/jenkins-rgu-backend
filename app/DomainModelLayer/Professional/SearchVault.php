<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;

class SearchVault extends Entity
{
    public function __construct($title,$type,$description,$campus_id,$activity_id,$task_id,$section_id){
        $this->title = $title;
        $this->type = $type;
        $this->description = $description;
        $this->campus_id = $campus_id;
        $this->activity_id = $activity_id;
        $this->task_id = $task_id;
        $this->section_id = $section_id;

       
    } 

    public static function searchable(){
        $searchable = [
            'title',
            'description'
        ];
        return $searchable;

    }

}