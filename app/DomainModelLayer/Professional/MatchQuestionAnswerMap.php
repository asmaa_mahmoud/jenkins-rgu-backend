<?php

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\MatchValue;

class MatchQuestionAnswerMap extends EntityMap
{
    protected $table = 'match_question_answers';
    public $timestamps = true;


    public function campusActivityQuestion(MatchQuestionAnswer $matchQuestionAnswer)
    {
        return $this->belongsTo($matchQuestionAnswer, CampusActivityQuestion::class , 'campus_activity_question_id', 'id');
    }

    public function matchKey(MatchQuestionAnswer $matchQuestionAnswer)
    {
        return $this->belongsTo($matchQuestionAnswer, MatchKey::class , 'key_id', 'id');
    }
    public function matchValue(MatchQuestionAnswer $matchQuestionAnswer)
    {
        return $this->belongsTo($matchQuestionAnswer, MatchValue::class , 'value_id', 'id');
    }

   
}