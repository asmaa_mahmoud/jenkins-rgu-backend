<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 04/11/2018
 * Time: 10:34 AM
 */

namespace App\DomainModelLayer\Professional;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;

class DragQuestionAnswerMap extends EntityMap
{
    protected $table = 'drag_question_answers';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "drag_question_answers.deleted_at";

    public function campusActivityQuestion(DragQuestionAnswer $dragQuestionAnswer)
    {
        return $this->belongsTo($dragQuestionAnswer, CampusActivityQuestion::class , 'campus_activity_question_id', 'id');
    }

    public function dragCell(DragQuestionAnswer $dragQuestionAnswer)
    {
        return $this->belongsTo($dragQuestionAnswer, DragCell::class , 'drag_cell_id', 'id');
    }
    public function dragChoice(DragQuestionAnswer $dragQuestionAnswer)
    {
        return $this->belongsTo($dragQuestionAnswer, DragChoice::class , 'drag_choice_id', 'id');
    }

   
}