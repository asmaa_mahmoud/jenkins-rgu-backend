<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:05 AM
 */

namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\Entity;

class Topic extends Entity{

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getTitle(){
        return $this->title;
    }
    public function setTitle($title){
        $this->title = $title;
    }

    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }

    public function getIconUrl(){
        return $this->iconUrl;
    }
    public function setIconUrl($iconUrl){
        $this->iconUrl = $iconUrl;
    }

    public function getArticles(){
        return $this->articles;
    }

    public function getArticlesCount(){
        return count($this->articles);
    }

    public function setArticles($articles){
        $this->articles = $articles;
    }
    public function getOrder(){
       return $this->topic_order;
    }
}