<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:06 AM
 */

namespace App\DomainModelLayer\HelpCenter\Repositories;


use App\DomainModelLayer\HelpCenter\Article;
use App\DomainModelLayer\HelpCenter\Tag;
use App\DomainModelLayer\HelpCenter\Topic;

interface IHelpCenterMainRepository{
    public function getAllTopics($limit,$search);
    public function getAllTopicsCount();
    public function getTopic($id);
    public function getTopicWithTitle($title);
    public function getTopics($ids);
    public function storeTopic(Topic $topic);
    public function deleteTopic(Topic $topic);
    public function getTopicsCount($search);
    public function uploadTopicIcon($imageData);
    public function rearrangeTopics($topics);

    public function getAllArticles($limit,$search,$count=false);
    public function getTopicArticle($topic_name);
    public function getArticle($id);
    public function getArticles($ids, $limit);
    public function storeArticle(Article $article);
    public function deleteArticle(Article $article);
    public function getArticlesCount($search);
    public function uploadArticleIcon($imageData);
    public function getTopicArticles($topic_id);
    public function rearrangeArticles($articles,$topic_id);
    public function setArticleOrder($article_id,$topic_id,$order);

    


    public function getAllTags($limit,$search);
    public function getTag($id);
    public function getTags($ids);
    public function storeTag(Tag $tag);
    public function deleteTag(Tag $tag);
    public function getTagsCount($search);

}