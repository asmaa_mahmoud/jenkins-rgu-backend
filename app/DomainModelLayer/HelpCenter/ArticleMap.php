<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:05 AM
 */

namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Professional\Campus;

class ArticleMap extends EntityMap{
    protected $table = "help_center_article";

    public function topics(Article $article){
        return $this->belongsToMany($article, Topic::class, 'help_center_article_topic');
    }

    public function tags(Article $article){
        return $this->belongsToMany($article, Tag::class, 'help_center_article_tag');
    }


    public function campuses(Article $article){
        return $this->belongsToMany($article, Campus::class, 'help_center_article_campus', 'article_id', 'campus_id');
    }
}