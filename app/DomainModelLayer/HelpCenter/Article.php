<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:05 AM
 */

namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\Entity;

class Article extends Entity{
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getTitle(){
        return $this->title;
    }
    public function setTitle($title){
        $this->title = $title;
    }

    public function getBody(){
        return $this->body;
    }
    public function setBody($body){
        $this->body = $body;
    }

    public function getSummary(){

        return $this->summary;
    }
    public function setSummary($summary){

        $this->summary = $summary;
    }

    public function getPartSummary()
   {
       $summary= $this->summary;
      if(preg_replace("/<img[^>]+\>/i", "", $summary)) {
        $partsummary= substr ( $summary ,0, 150 ) ;
        return $partsummary;
        }                                       
   }

    public function getIconUrl(){
        return $this->iconUrl;
    }
    public function setIconUrl($iconUrl){
        $this->iconUrl = $iconUrl;
    }

    public function getIsRecommended(){
        return $this->isRecommended;
    }
    public function setIsRecommended($isRecommended){
        $this->isRecommended = $isRecommended;
    }

    public function getNLikes(){
        return $this->nLikes;
    }
    public function setNLikes($nLikes){
        $this->nLikes = $nLikes;
    }

    public function getNDislikes(){
        return $this->nDislikes;
    }
    public function setNDislikes($nDislikes){
        $this->nDislikes = $nDislikes;
    }

    public function getTopics(){
        return $this->topics;
    }
    public function setTopics($topics){
        $this->topics = $topics;
    }

    public function getTags(){
        return $this->tags;
    }
    public function setTags($tags){
        $this->tags = $tags;
    }

    public function getCampuses(){
        return $this->campuses;
    }
    public function setCampuses($campuses){
        $this->campuses = $campuses;
    }
}