<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:05 AM
 */

namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\EntityMap;

class TopicMap extends EntityMap{
    protected $table = "help_center_topic";
    public $timestamps = false;

    public function articles(Topic $topic){
        return $this->belongsToMany($topic, Article::class, 'help_center_article_topic')->orderBy('article_order');
    }
}