<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:06 AM
 */

namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\Entity;

class Tag extends Entity
{
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getIconUrl(){
        return $this->iconUrl;
    }
    public function setIconUrl($iconUrl){
        $this->iconUrl = $iconUrl;
    }

    public function getArticles(){
        return $this->articles;
    }
    public function setArticles($articles){
        $this->articles = $articles;
    }
    
    public function getArticlesCount(){
        return count($this->articles);
    }
}