<?php


namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Professional\Campus;

class ArticleTopicMap extends EntityMap{
    protected $table = "help_center_article_topic";

}