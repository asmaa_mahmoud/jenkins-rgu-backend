<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 11:06 AM
 */

namespace App\DomainModelLayer\HelpCenter;

use Analogue\ORM\EntityMap;

class TagMap extends EntityMap {
    protected $table = "help_center_tag";
    public $timestamps = false;

    public function articles(Tag $tag){
        return $this->belongsToMany($tag, Article::class, "help_center_article_tag");
    }
}