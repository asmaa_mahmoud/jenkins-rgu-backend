<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\Group;

class GroupMemberMap extends EntityMap {

    protected $table = 'group_user';


    public function user(GroupMember $groupMember)
    {
        return $this->belongsTo($groupMember, User::class , 'user_id', 'id');
    }

    public function role(GroupMember $groupMember)
    {
        return $this->belongsTo($groupMember, Role::class , 'role_id', 'id');
    }

    public function group(GroupMember $groupMember){
        return $this->belongsTo($groupMember, Group::class, 'group_id','id');
    }


}