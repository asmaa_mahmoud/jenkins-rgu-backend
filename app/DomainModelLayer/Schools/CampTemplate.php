<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class CampTemplate extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getAgeFrom(){
        return $this->age_from;
    }

    public function getAgeTo(){
        return $this->age_to;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getActivities(){
        return $this->activities;
    }

}