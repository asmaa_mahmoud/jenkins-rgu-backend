<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Schools\Dtos\CampDto;
use App\ApplicationLayer\Schools\Dtos\CampRequestDto;
use Carbon\Carbon;

class Camp extends Entity
{
    public function __construct(CampRequestDto $campDto){
        $this->name = $campDto->name;
        $this->description = $campDto->description;
        $this->age_from = $campDto->agefrom;
        $this->age_to = $campDto->ageto;
        $this->icon_url = $campDto->iconUrl;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        return $this->description = $description;
    }

    public function getAgeFrom(){
        return $this->age_from;
    }

    public function setAgeFrom($age_from){
        $this->age_from = $age_from;
    }

    public function getAgeTo(){
        return $this->age_to;
    }

    public function setAgeTo($age_to){
        $this->age_to = $age_to;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function setIconUrl($icon_url){
        $this->icon_url = $icon_url;
    }

    public function getActivities(){
        return $this->activities;
    }

    public function getDefaultActivities(){
        return $this->defaultActivities;
    }

    public function getSchoolCamp(){
        return $this->schoolCamp;
    }

    public function getUsers(){
        return $this->users;
    }

    public function getStudents(){
        $users = [];
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "student")
                    array_push($users, $user);
            }
        }
        return $users;
    }

    public function getCountOfStudents(){
        $users = 0;
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "student")
                    $users++;
            }
        }
        return $users;
    }

    public function getTeachers(){
        $users = [];
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "teacher")
                    array_push($users, $user);
            }
        }
        return $users;
    }

    public function getCountOfTeachers(){
        $users = 0;
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "teacher")
                    $users++;
            }
        }
        return $users;
    }

    public function addCampActivity(CampActivity $campActivity){
        $this->campActivities->push($campActivity);
    }

    public function addCampUser(CampUser $campUser){
        $this->campUsers->push($campUser);
    }

    public function addSchoolCamp(SchoolCamp $schoolCamp){
        $this->school->push($schoolCamp);
    }

    public function isMember($user_id){
        $users = $this->getUsers();
        foreach ($users as $user){
            if($user->getId() == $user_id)
                return true;
        }
        return false;
    }

    public function getCampActivities(){
        return $this->campActivities;
    }

    public function getCampUsers(){
        return $this->campUsers;
    }

    public function getDefaultTasksCount(){
        $countTasks = 0;
        $activities = $this->getDefaultActivities();
        foreach ($activities as $activity){
            $countTasks += $activity->getTasksCount();
        }
        return $countTasks;
    }

    public function getTasksCount(){
        $countTasks = 0;
        $activities = $this->getActivities();
        foreach ($activities as $activity){
            $countTasks += $activity->getTasksCount();
        }
        return $countTasks;
    }

    public function getStatus(){
        $schoolCamp = $this->getSchoolCamp();
        if($schoolCamp->getLocked() == 0){
            return 'locked';
        }
        else {
            if($schoolCamp->getEndsAt() < Carbon::now()->timestamp)
                return 'ended';
            else if($schoolCamp->getStartsAt() > Carbon::now()->timestamp)
                return 'upcoming';
            else
                return 'running';
        }
    }

    public function getSchool(){
        return $this->getSchoolCamp()->getSchool()->getName();
    }

    public function getStartsAt(){
        return $this->getSchoolCamp()->getStartsAt();
    }

    public function getEndsAt(){
        return $this->getSchoolCamp()->getCampDetail()->getEndsAt();
    }



}