<?php

namespace App\DomainModelLayer\Schools;

use App\ApplicationLayer\Schools\Dtos\CourseRequestDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\Journey;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;
use App\Http\Controllers\Schools\GroupController;


class CourseAdventureDeadlines extends Entity
{
    //region Getters & Setters

    public function __construct(Course $course,Adventure $adventure,$startDate,$endDate)
    {

        $this->ends_at = $endDate;
        $this->starts_at = $startDate;

        $this->course = $course;
        $this->adventure = $adventure;

    }

    public function getId()
    {
        return $this->id;
    }

    public function getAdventure(){
        return $this->adventure;
    }

    public function setAdventure(Adventure $adventure){
        $this->adventure = $adventure;
    }

    public function getCourse(){
        return $this->course;
    }

    public function setCourse(Course $course){
        $this->course = $course;
    }


    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setStartsAt($starts_at){
        $this->starts_at = $starts_at;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }
}
