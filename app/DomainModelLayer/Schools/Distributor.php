<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;


class Distributor extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getDefaultCheck(){
        return $this->is_default;
    }

    public function getStripeCheck(){
        return $this->use_stripe;
    }

    public function getCountries(){
        return $this->countries;
    }

    public function getSubscriptions(){
        return $this->subscriptions;
    }

    public function getImage(){
        return $this->image;
    }
}

