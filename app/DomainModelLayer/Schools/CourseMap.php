<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\Assignment;
use App\DomainModelLayer\Schools\Group;


class CourseMap extends EntityMap {

    protected $table = 'course';

    public $timestamps = true;

    public function journey(Course $course)
    {
        return $this->belongsTo($course, Journey::class, 'journey_id','id');
    }

    public function groups(Course $course)
    {
        return $this->belongsToMany($course,Group::class,'course_group','course_id','group_id');
    }

    public function assignments(Course $course)
    {
        return $this->hasMany($course, Assignment::class, 'course_id','id');
    }

    public function creator(Course $course){
        return $this->belongsTo($course,User::class,'creator_id', 'id');
    }

    public function courseAdventureDeadlines(Course $course)
    {
        return $this->hasMany($course, CourseAdventureDeadlines::class, 'course_id','id');
    }
}