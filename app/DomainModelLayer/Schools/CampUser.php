<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;

class CampUser extends Entity
{
    public function __construct(Camp $camp, User $user, Role $role){
        $this->camp = $camp;
        $this->user = $user;
        $this->role = $role;
    }

    public function getId(){
        return $this->id;
    }

    public function getCamp(){
        return $this->camp;
    }

    public function getUser(){
        return $this->user;
    }

    public function getRole(){
        return $this->role;
    }
}