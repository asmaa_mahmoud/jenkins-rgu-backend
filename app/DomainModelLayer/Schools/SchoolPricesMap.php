<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolPricesMap extends EntityMap
{
    protected $table = 'school_prices';

}