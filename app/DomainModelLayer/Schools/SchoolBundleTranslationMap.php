<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolBundleTranslationMap extends EntityMap
{
    protected $table = 'school_bundle_translation';
    public $timestamps = true;

    public function schoolBundle(SchoolBundleTranslation $schoolBundleTranslation)
    {
        return $this->belongsTo($schoolBundleTranslation, SchoolBundle::class, 'school_bundle_id', 'id');
    }

}