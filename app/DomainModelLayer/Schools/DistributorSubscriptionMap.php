<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Schools\DistributorSubscription;
use App\DomainModelLayer\Schools\Distributor;

class DistributorSubscriptionMap extends EntityMap {

    protected $table = 'distributor_subscription';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "distributor_subscription.deleted_at";

    public function subscription(DistributorSubscription $distributorSubscription)
    {
        return $this->belongsTo($distributorSubscription, Subscription::class, 'subscription_id', 'id');
    }

    public function distributor(DistributorSubscription $distributorSubscription)
    {
        return $this->belongsTo($distributorSubscription, Distributor::class, 'distributor_id', 'id');
    }

    public function distributorCharge(DistributorSubscription $distributorSubscription)
    {
        return $this->hasOne($distributorSubscription, DistributorCharge::class, 'distributor_subscription_id', 'id');
    }

}