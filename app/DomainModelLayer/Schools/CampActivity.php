<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class CampActivity extends Entity
{
    public function __construct(Camp $camp, $order){
        $this->camp = $camp;
        $this->order = $order;
    }
    
    public function getId(){
        return $this->id;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function setActivity(Activity $activity){
        $this->activity = $activity;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity){
        $this->defaultActivity = $defaultActivity;
    }

    public function getOrder(){
        return $this->order;
    }

    public function setOrder($order){
        return $this->order = $order;
    }

    public function getCamp(){
        return $this->camp;
    }

    public function setCamp(Camp $camp){
        $this->camp = $camp;
    }


}