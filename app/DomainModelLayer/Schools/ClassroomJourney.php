<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Schools\Classroom;

class ClassroomJourney extends Entity
{

    public function __construct(Classroom $classroom,Journey $journey)
    {
        $this->classroom = $classroom;
        $this->journey = $journey;
    }

    public function getClassroom(){
        return $this->classroom;
    }

    public function getJourney(){
        return $this->journey;
    }

}