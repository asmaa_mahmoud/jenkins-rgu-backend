<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\SchoolCharge;

class SchoolChargeDetailsMap extends EntityMap
{
    protected $table = 'school_charge_details';
    public $timestamps = true;

    public function schoolCharge(SchoolChargeDetails $school_charge_details)
    {
        return $this->belongsTo($school_charge_details, SchoolCharge::class, 'school_charge_id', 'id');
    }

}