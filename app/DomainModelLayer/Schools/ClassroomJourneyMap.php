<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\ClassroomJourney;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Journeys\Journey;

class ClassroomJourneyMap extends EntityMap {

    protected $table = 'classroom_journey';


    public function journey(ClassroomJourney $classroomJourney)
    {
        return $this->belongsTo($classroomJourney, Journey::class , 'journey_id', 'id');
    }

    public function classroom(ClassroomJourney $classroomJourney){
        return $this->belongsTo($classroomJourney, Classroom::class,'classroom_id','id');
    }


}