<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;

class ClassroomMap extends EntityMap {

    protected $table = 'classroom';
    public $timestamps = true;



    public function classroomMembers(Classroom $classroom)
    {
        return $this->hasMany($classroom, ClassroomMember::class, 'classroom_id','id');
    }

    public function classroomJourneys(Classroom $classroom)
    {
        return $this->hasMany($classroom, ClassroomJourney::class, 'classroom_id','id');
    }

    public function grade(Classroom $classroom)
    {
        return $this->belongsTo($classroom, Grade::class, 'grade_id','id');
    }
    public function school(Classroom $classroom)
    {
        return $this->belongsTo($classroom, School::class, 'school_id','id');
    }

    public function groups(Classroom $classroom)
    {
        return $this->hasMany($classroom, Group::class, 'classroom_id','id');
    }

}