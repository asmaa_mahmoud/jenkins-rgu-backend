<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Schools\Dtos\StudentPositionDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Adventure;

class StudentPosition extends Entity
{

    public function __construct(StudentPositionDto $studentPositionDto, User $user, Course $course,Adventure $adventure)
    {
        $this->position_x = $studentPositionDto->PositionX;
        $this->position_y = $studentPositionDto->PositionY;
        $this->index = $studentPositionDto->Index;
        $this->course = $course;
        $this->adventure = $adventure;
        $this->user = $user;
    }

    public function getPositionX()
    {
        return $this->position_x;
    }

    public function getPositionY()
    {
        return $this->position_y;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getCourse()
    {
        return $this->course;
    }

}
