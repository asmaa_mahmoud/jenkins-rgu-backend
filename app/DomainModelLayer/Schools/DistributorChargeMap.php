<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class DistributorChargeMap extends EntityMap
{
    protected $table = 'distributor_charge';
    public $timestamps = true;

    public function subscription(DistributorCharge $distributorCharge)
    {
        return $this->belongsTo($distributorCharge, DistributorSubscription::class , 'distributor_subscription_id', 'id');
    }

    public function chargeUpgrades(DistributorCharge $distributorCharge)
    {
        return $this->hasMany($distributorCharge, DistributorChargeUpgrade::class , 'distributor_charge_id', 'id');
    }

    public function chargeDetail(DistributorCharge $distributorCharge)
    {
        return $this->hasOne($distributorCharge, DistributorChargeDetails::class , 'distributor_charge_id', 'id');
    }

    public function bundle(DistributorCharge $distributorCharge)
    {
        return $this->belongsTo($distributorCharge, SchoolBundle::class , 'bundle_id', 'id');
    }
    
}