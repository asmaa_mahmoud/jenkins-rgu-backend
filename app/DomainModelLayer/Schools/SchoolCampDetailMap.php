<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolCampDetailMap extends EntityMap
{
    protected $table = 'school_camp_details';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "school_camp_details.deleted_at";

    public function schoolCamp(SchoolCampDetail $SchoolCampDetail){
        return $this->belongsTo($SchoolCampDetail, SchoolCamp::class , 'school_camp_id', 'id');
    }

}