<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolDistributorCampMap extends EntityMap
{
    protected $table = 'school_distributor_camp';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "school_distributor_camp.deleted_at";

    public function schoolCamp(SchoolDistributorCamp $SchoolDistributorCamp){
        return $this->belongsTo($SchoolDistributorCamp, SchoolCamp::class , 'school_camp_id', 'id');
    }

    public function distributor(SchoolDistributorCamp $SchoolDistributorCamp){
        return $this->belongsTo($SchoolDistributorCamp, Distributor::class , 'distributor_id', 'id');
    }

}