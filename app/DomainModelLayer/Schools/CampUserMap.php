<?php

namespace App\DomainModelLayer\Schools;

use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;
use Analogue\ORM\EntityMap;

class CampUserMap extends EntityMap
{
    protected $table = 'camp_user';
    public $timestamps = true;

    public function user(CampUser $campUser){
        return $this->belongsTo($campUser, User::class , 'user_id', 'id');
    }

    public function role(CampUser $campUser){
        return $this->belongsTo($campUser, Role::class , 'role_id', 'id');
    }

    public function camp(CampUser $campUser){
        return $this->belongsTo($campUser, Camp::class , 'camp_id', 'id');
    }


}