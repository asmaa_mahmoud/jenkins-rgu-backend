<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class CampDiscountMap extends EntityMap
{
    protected $table = 'camp_discount';
    public $timestamps = true;

}