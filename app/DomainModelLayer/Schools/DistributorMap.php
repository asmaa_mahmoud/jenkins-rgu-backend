<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Distributor;
use App\DomainModelLayer\Schools\Country;
use App\DomainModelLayer\Schools\DistributorSubscription;

class DistributorMap extends EntityMap {

    protected $table = 'distributor';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "distributor.deleted_at";

    public function countries(Distributor $distributor)
    {
        return $this->belongsToMany($distributor, Country::class,'distributor_country', 'distributor_id', 'country_id');
    }

    public function subscriptions(Distributor $distributor)
    {
        return $this->hasMany($distributor, DistributorSubscription::class,'distributor_id','id');
    }

}