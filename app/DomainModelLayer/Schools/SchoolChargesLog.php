<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Schools\School;

class SchoolChargesLog extends Entity
{
    public function __construct(School $school, $cost, $description, $discount = 0, $from = null, $to= null, $tax = 0, $returned = 0)
    {
        $this->school = $school;
        $this->cost = $cost;
        $this->description = $description;
        $this->from = $from;
        $this->to = $to;
        $this->discount = $discount;
        $this->tax = $tax;
        $this->returned = $returned;
    }

    public function getId(){
        return $this->id;
    }

    public function getSchool(){
        return $this->school;
    }

    public function setSchool(School $school){
        $this->school = $school;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($number)
    {
        $this->discount = $number;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getFrom()
    {
        return explode(" ", $this->from)[0];
    }

    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function getTo()
    {
        return explode(" ", $this->to)[0];
    }

    public function setTo($to)
    {
        $this->to = $to;
    }

    public function getTax(){
        $this->tax;
    }

    public function setTax(){
        return $this->tax;
    }

    public function getReturned(){
        return $this->returned;
    }

    public function setReturned($returned){
        $this->returned = $returned;
    }


}