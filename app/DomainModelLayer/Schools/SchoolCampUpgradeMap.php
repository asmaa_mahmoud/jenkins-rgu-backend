<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolCampUpgradeMap extends EntityMap
{
    protected $table = 'school_distributor_camp_upgrade';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "school_distributor_camp_upgrade.deleted_at";

    public function schoolCamp(SchoolCampUpgrade $SchoolCampUpgrade){
        return $this->belongsTo($SchoolCampUpgrade, SchoolCamp::class , 'school_camp_id', 'id');
    }

}