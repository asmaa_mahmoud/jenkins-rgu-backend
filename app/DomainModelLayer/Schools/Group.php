<?php

namespace App\DomainModelLayer\Schools;

use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\UserRole;
use App\Helpers\UUID;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use App\ApplicationLayer\Schools\Dtos\GroupRequestDto;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;


class Group extends Entity
{
    //region Getters & Setters

    public function __construct(Classroom $classroom,GroupRequestDto $groupRequestDto,$is_default = true)
    {
        $this->classroom = $classroom;
        $this->name = $groupRequestDto->name;
        $this->is_default = $is_default;
        $this->groupMembers = new EntityCollection;
        $this->max_students = 1500;
        $this->courses = new EntityCollection;
    }

    public function getId(){
        return $this->id;
    }
    public function getClassroom(){
        return $this->classroom;
    }

    public function setClassroom(Classroom $classroom){
        $this->classroom = $classroom;
    }

    public function getGroupMembers(){
        return $this->groupMembers;
    }

    public function addGroupMember(GroupMember $groupMember){
        return $this->groupMembers->push($groupMember);
    }

    public function removeGroupMember(GroupMember $groupMember){
        return $this->groupMembers->remove($groupMember);
    }

    public function getCourses(){
        return $this->courses;
    }

    public function addCourse(Course $course){
        return $this->courses->push($course);
    }

    public function removeCourse(Course $course){
        return $this->courses->remove($course);
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function getIsDefault(){
        return $this->is_default;
    }

    public function setIsDefault($is_default){
        $this->is_default = $is_default;
    }


    public function getStudents()
    {
        $students = [];

        foreach ($this->groupMembers as $groupMember) {
            $user = $groupMember->getUser();
            foreach ($user->getRoles() as $role) {
                if($role->getName() == 'student')
                    $students[] = $user;
            }
        }

        return $students;
    }

    public function isMember($user_id)
    {
        $is_member = false;

        foreach ($this->groupMembers as $groupMember) {
            $user = $groupMember->getUser();
            if($user->getId() == $user_id)
                $is_member = true;
        }

        return $is_member;
    }

    public function getMembersWithRole($roleName){
        $groupMembers = $this->groupMembers;
        $members = array();
        foreach ($groupMembers as $groupMember){
            $role = $groupMember->getRole();
            if($role->getName() == $roleName)
            {
                array_push($members,$groupMember->getUser());
            }


        }
        return $members;
    }

    public function getMainTeacher(){
        $mainTeachers = $this->getMembersWithRole("main_teacher");
        if(sizeof($mainTeachers) == 0)
            return null;
        else
            return $mainTeachers[0];

    }
    public function getNumberOfStudents()
    {
        return count($this->getStudents());
    }

    public function getTotalScore()
    {
        $total_score = 0;
        $students = $this->getGroupMembers();
        foreach ($students as $student) {
            if($student->getRole()->getName() == "student")
            {
                $student_obj = $student->getUser();
                $score = $student_obj->getStudentScoresOld();
                $total_score += $score["mission_scores"] + $score["quiz_scores"];
            }
        }
        return $total_score;
    }
}

