<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class CampTemplateTranslationMap extends EntityMap
{
    protected $table = 'camp_template_translation';
    public $timestamps = true;

    public function campTemplate(CampTemplateTranslation $translation){
        return $this->belongsTo($translation, CampTemplate::class ,'camp_template_id','id');
    }


}