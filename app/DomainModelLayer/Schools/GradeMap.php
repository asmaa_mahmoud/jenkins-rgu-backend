<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Accounts\User;

class GradeMap extends EntityMap {

    protected $table = 'grade';

    public function classrooms(Grade $grade)
    {
        return $this->hasMany($grade, Classroom::class, 'grade_id', 'id');
    }

    public function school(Grade $grade)
    {
        return $this->belongsTo($grade, School::class, 'school_id','id');
    }

    public function students(Grade $grade)
    {
        return $this->belongsToMany($grade,User::class,'student_grade','grade_id','user_id');
    }


}