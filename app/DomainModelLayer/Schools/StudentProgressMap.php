<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Schools\StudentProgress;
use App\DomainModelLayer\Accounts\User;

class StudentProgressMap extends EntityMap {

    protected $table = 'student_progress';
    public $timestamps = true;

    public function task(StudentProgress $studentProgress)
    {
        return $this->belongsTo($studentProgress, Task::class , 'task_id', 'id');
    }

    public function student(StudentProgress $studentProgress)
    {
        return $this->belongsTo($studentProgress, User::class , 'student_id', 'id');
    }

    public function course(StudentProgress $studentProgress)
    {
        return $this->belongsTo($studentProgress, Course::class , 'course_id', 'id');
    }

    public function adventure(StudentProgress $studentProgress)
    {
        return $this->belongsTo($studentProgress, Adventure::class , 'adventure_id', 'id');
    }


}