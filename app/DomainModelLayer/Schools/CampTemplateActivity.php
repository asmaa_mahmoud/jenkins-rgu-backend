<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class CampTemplateActivity extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function getOrder(){
        return $this->order;
    }

    public function setOrder($order){
        return $this->order = $order;
    }

    public function getCamp(){
        return $this->camp;
    }
}