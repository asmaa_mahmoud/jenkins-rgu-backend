<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class CampActivityMap extends EntityMap
{
    protected $table = 'camp_activity';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "camp_activity.deleted_at";

    public function activity(CampActivity $campActivity){
        return $this->belongsTo($campActivity, Activity::class ,'activity_id', 'id');
    }

    public function defaultActivity(CampActivity $campActivity){
        return $this->belongsTo($campActivity, DefaultActivity::class ,'default_activity_id' , 'id');
    }

    public function camp(CampActivity $campActivity){
        return $this->belongsTo($campActivity, Camp::class, 'camp_id', 'id');
    }

}