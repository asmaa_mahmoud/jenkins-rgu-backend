<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\SchoolCharge;
use App\DomainModelLayer\Schools\SchoolChargeUpgrade;

class SchoolChargeUpgradeMap extends EntityMap
{
    protected $table = 'school_charge_upgrade';
    public $timestamps = true;

    public function schoolCharge(SchoolChargeUpgrade $schoolChargeUpgrade)
    {
        return $this->belongsTo($schoolChargeUpgrade, SchoolCharge::class , 'school_charge_id', 'id');
    }

    public function bundle(SchoolChargeUpgrade $schoolChargeUpgrade)
    {
        return $this->belongsTo($schoolChargeUpgrade, SchoolBundle::class , 'bundle_id', 'id');
    }

}