<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Accounts\UserRole;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;

class GroupMap extends EntityMap {

    protected $table = 'group';
    public $timestamps = true;

    public function courses(Group $group)
    {
        return $this->belongsToMany($group,Course::class,'course_group','group_id','course_id');
    }

    public function classroom(Group $group)
    {
        return $this->belongsTo($group, Classroom::class, 'classroom_id','id');
    }

    public function groupMembers(Group $group)
    {
        return $this->hasMany($group, GroupMember::class, 'group_id','id');
    }
}