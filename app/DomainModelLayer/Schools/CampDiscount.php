<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class CampDiscount extends Entity
{
    public function getNoOfStudents(){
        return $this->no_of_students;
    }

    public function getNoOfActivities(){
        return $this->no_of_activities;
    }

    public function getDiscount(){
        return $this->discount;
    }

}