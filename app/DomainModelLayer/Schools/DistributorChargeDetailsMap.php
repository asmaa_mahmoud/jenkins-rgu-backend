<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class DistributorChargeDetailsMap extends EntityMap
{
    protected $table = 'distributor_charge_details';
    public $timestamps = true;

    public function distributorCharge(DistributorChargeDetails $distributor_charge_details)
    {
        return $this->belongsTo($distributor_charge_details, DistributorCharge::class, 'distributor_charge_id', 'id');
    }
}