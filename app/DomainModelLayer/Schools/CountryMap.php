<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Distributor;
use App\DomainModelLayer\Schools\Country;

class CountryMap extends EntityMap {

    protected $table = 'country';

    public function distributors(Country $country)
    {
        return $this->belongsToMany($country, Distributor::class,'distributor_country', 'country_id', 'distributor_id');
    }

}