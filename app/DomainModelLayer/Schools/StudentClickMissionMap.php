<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\StudentClickMission;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;

class StudentClickMissionMap extends EntityMap {

    protected $table = 'student_click_mission';

    public function course(StudentClickMission $studentClickMission)
    {
        return $this->belongsTo($studentClickMission, Course::class, 'course_id','id');
    }
    public function user(StudentClickMission $studentClickMission)
    {
        return $this->belongsTo($studentClickMission, User::class, 'user_id','id');
    }
}