<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class CampTemplateMap extends EntityMap
{
    protected $table = 'camp_template';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "camp_template.deleted_at";

    public function activities(CampTemplate $campTemplate){
        return $this->belongsToMany($campTemplate, DefaultActivity::class , 'camp_template_activity','camp_template_id','default_activity_id');
    }

    public function translations(CampTemplate $campTemplate){
        return $this->hasMany($campTemplate, CampTemplateTranslation::class , 'camp_template_id', 'id');
    }


}