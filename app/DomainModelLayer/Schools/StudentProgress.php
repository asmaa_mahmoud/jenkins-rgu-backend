<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Accounts\User;

class StudentProgress extends Entity
{
	public function __construct(Task $task, User $student,Course $course, Adventure $adventure, $no_trials, $score,$first_success = null,$timeTaken = null,$bestTimeTaken = null,$numberOfBlocks = null,$bestNumberOfBlocks = null)
    {
        $this->task = $task;
        $this->student = $student;
        $this->course = $course;
        $this->adventure = $adventure;
        $this->no_trials = $no_trials;
        $this->score = $score;
        if($first_success != null)
            $this->first_success = $first_success;

        $this->task_duration = $timeTaken;
        $this->	best_task_duration	 = $bestTimeTaken;
        $this->blocks_number = $numberOfBlocks;
        $this->	best_blocks_number = $bestNumberOfBlocks;

    }

	public function getTask()
	{
		return $this->task;
	}

	public function getScore()
	{
		return $this->score;
	}

	public function setScore($score)
	{
		$this->score = $score;
	}

	public function getNo_of_trails()
	{
		return $this->no_trials;
	}

	public function setNo_of_trails($number)
	{
		$this->no_trials = $number;
	}

    public function getFirstSuccess()
    {
        return $this->first_success;
    }

    public function setFirstSuccess($number)
    {
        $this->first_success = $number;
    }

	public function getCourse()
	{
		return $this->course;
	}

	public function isSuccess(){
	    if($this->getFirstSuccess() == null)
	        return false;
	    else if($this->getFirstSuccess() == 0)
	        return false;
	    else
	        return true;
    }

    public function getAdventure()
    {
        return $this->adventure;
    }

    public function getDate()
    {
        return $this->updated_at;
    }


    public function getTaskDuration(){
        return $this->task_duration;

    }
    public function getBestTaskDuration(){
        return $this->	best_task_duration;

    }
    public function getBlocksNumber(){
        return $this->blocks_number;

    }
    public function getBestBlocksNumber(){
        return $this->	best_blocks_number;
    }
}
