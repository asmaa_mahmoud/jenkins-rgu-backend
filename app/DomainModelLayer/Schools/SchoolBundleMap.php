<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolBundleMap extends EntityMap
{
    protected $table = 'school_bundles';
    public $timestamps = true;

    public function translations(SchoolBundle $schoolBundle)
    {
        return $this->hasMany($schoolBundle, SchoolBundleTranslation::class , 'school_bundle_id', 'id');
    }

}