<?php


namespace App\DomainModelLayer\Schools;


use Analogue\ORM\Entity;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use Analogue\ORM\EntityCollection;
use DB;

class ClassroomMember extends Entity
{

    public function __construct(Classroom $classroom,User $user,Role $role)
    {
        $this->classroom = $classroom;
        $this->user = $user;
        $this->role = $role;
    }

    public function getClassroom(){
        return $this->classroom;
    }

    public function getRole(){
        return $this->role;
    }

    public function setRole(Role $role){
        $this->role = $role;
    }

    public function getUser(){
        return $this->user;
    }
}