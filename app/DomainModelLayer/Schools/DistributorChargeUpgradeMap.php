<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class DistributorChargeUpgradeMap extends EntityMap
{
    protected $table = 'distributor_charge_upgrade';
    public $timestamps = true;

    public function distributorCharge(DistributorChargeUpgrade $distributorChargeUpgrade)
    {
        return $this->belongsTo($distributorChargeUpgrade, DistributorCharge::class , 'distributor_charge_id', 'id');
    }

    public function bundle(DistributorChargeUpgrade $distributorChargeUpgrade)
    {
        return $this->belongsTo($distributorChargeUpgrade, SchoolBundle::class , 'bundle_id', 'id');
    }

}