<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Schools\SchoolChargeUpgrade;
use App\DomainModelLayer\Schools\SchoolBundle;
use App\DomainModelLayer\Schools\SchoolChargeDetails;

class SchoolChargeMap extends EntityMap
{
    protected $table = 'school_charge';
    public $timestamps = true;

    public function subscription(SchoolCharge $schoolCharge)
    {
        return $this->belongsTo($schoolCharge, Subscription::class , 'subscription_id', 'id');
    }

    public function chargeUpgrades(SchoolCharge $schoolCharge)
    {
        return $this->hasMany($schoolCharge, SchoolChargeUpgrade::class , 'school_charge_id', 'id');
    }

    public function chargeDetail(SchoolCharge $schoolCharge)
    {
        return $this->hasOne($schoolCharge, SchoolChargeDetails::class , 'school_charge_id', 'id');
    }

    public function bundle(SchoolCharge $schoolCharge)
    {
        return $this->belongsTo($schoolCharge, SchoolBundle::class , 'bundle_id', 'id');
    }

}