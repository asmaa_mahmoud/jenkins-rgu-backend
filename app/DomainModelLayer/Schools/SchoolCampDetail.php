<?php

namespace App\DomainModelLayer\Schools;
use Analogue\ORM\Entity;

class SchoolCampDetail extends Entity
{
    public function __construct(SchoolCamp $schoolCamp, $no_of_students, $no_of_activities, $ends_at){
        $this->schoolCamp = $schoolCamp;
        $this->no_of_students = $no_of_students;
        $this->no_of_activities = $no_of_activities;
        $this->ends_at = $ends_at;
    }

    public function getId(){
        return $this->id;
    }

    public function getSchoolCamp(){
        return $this->schoolCamp;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getNoOfStudents(){
        return $this->no_of_students;
    }

    public function setNoOfStudents($noOfStudents){
        $this->no_of_students = $noOfStudents;
    }

    public function getNoOfActivities(){
        return $this->no_of_activities;
    }

    public function setNoOfActivities($noOfActivities){
        $this->no_of_activities = $noOfActivities;
    }

}