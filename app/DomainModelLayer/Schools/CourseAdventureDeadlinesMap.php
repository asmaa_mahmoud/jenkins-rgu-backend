<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;

class CourseAdventureDeadlinesMap extends EntityMap {

    protected $table = 'course_adventure_deadlines';

    public function course(CourseAdventureDeadlines $courseAdventureDeadlines)
    {
        return $this->belongsTo($courseAdventureDeadlines, Course::class , 'course_id', 'id');
    }

    public function adventure(CourseAdventureDeadlines $courseAdventureDeadlines)
    {
        return $this->belongsTo($courseAdventureDeadlines, Adventure::class , 'adventure_id', 'id');
    }
}