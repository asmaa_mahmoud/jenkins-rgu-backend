<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Schools\SchoolBundle;

class SchoolCharge extends Entity
{
    public function __construct(Subscription $subscription, $charge_id, $cost, $no_of_students, $no_of_classroom, $description)
    {
        $this->subscription = $subscription;
        $this->charge_id = $charge_id;
        $this->cost = $cost;
        $this->no_of_students = $no_of_students;
        $this->no_of_classroom = $no_of_classroom;
        $this->description = $description;
        $this->chargeUpgrades = new EntityCollection;
    }

    public function getId(){
        return $this->id;
    }

    public function getChargeId(){
        return $this->charge_id;
    }

    public function setChargeId($charge){
        $this->charge_id = $charge;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getNoStudents()
    {
        return $this->no_of_students;
    }

    public function setNoStudents($number)
    {
        $this->no_of_students = $number;
    }

    public function getNoClassroom()
    {
        return $this->no_of_classroom;
    }

    public function setNoClassroom($number)
    {
        $this->no_of_classroom = $number;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

    public function setSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    public function getChargeUpgrades()
    {
        return $this->chargeUpgrades;
    }

    public function addChargeUpgrade(SchoolChargeUpgrade $upgrade)
    {
        return $this->chargeUpgrades->push($upgrade);
    }

    public function removeChargeUpgrade(SchoolChargeUpgrade $upgrade)
    {
        return $this->chargeUpgrades->remove($upgrade);
    }

    public function getChargeDetail()
    {
        return $this->chargeDetail;
    }

    public function setChargeDetail(SchoolChargeDetails $detail)
    {
        $this->chargeDetail = $detail;
    }

    public function setBundle(SchoolBundle $bundle){
        $this->bundle = $bundle;
    }

    public function getBundle(){
        return $this->bundle;
    }


}