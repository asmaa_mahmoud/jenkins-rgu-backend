<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolPrices extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getQuantity(){
        return $this->quantity;
    }

    public function getPrice(){
        return $this->price;
    }

    public function getType(){
        return $this->type;
    }

    public function getDiscount(){
        return $this->discount;
    }

}