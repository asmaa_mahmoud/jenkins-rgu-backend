<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\SchoolChargesLog;
use App\DomainModelLayer\Schools\School;

class SchoolChargesLogMap extends EntityMap
{
    protected $table = 'school_charges_log';
    public $timestamps = true;

    public function school(SchoolChargesLog $schoolChargesLog)
    {
        return $this->belongsTo($schoolChargesLog, School::class , 'school_id', 'id');
    }


}