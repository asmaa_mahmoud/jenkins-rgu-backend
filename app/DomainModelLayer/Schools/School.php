<?php
namespace App\DomainModelLayer\Schools;

use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\Country;
use App\DomainModelLayer\Professional\ColorPalette;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;


class School extends Entity
{
    //region Getters & Setters

    public function __construct(Account $account,ColorPalette $colorPalette,SchoolDto $schoolDto)
    {
        $this->account = $account;
        $this->name = $schoolDto->name;
        $this->email = $schoolDto->email;
        $this->address = $schoolDto->address;
        $this->website = $schoolDto->website;
        $this->logo_url = $schoolDto->schoolLogoUrl;
        $this->colorPalette = $colorPalette;
    }

    public function setAccount(Account $account){
        $this->account = $account;
    }

    public function getAccount(){
        return $this->account;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function setSchoolLogoUrl($logoUrl){
        $this->logo_url = $logoUrl;
    }

    public function getSchoolLogoUrl(){
        return $this->logo_url;
    }

    public function getId(){
        return $this->id;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setCountry(Country $country){
        $this->country = $country;
    }

    public function getCountry(){
        return $this->country;
    }

    public function setAddress($address){
        $this->address = $address;
    }

    public function getAddress(){
        return $this->address;
    }

    public function setWebsite($website){
        $this->website = $website;
    }

    public function getWebsite(){
        return $this->website;
    }

    public function getGrades(){
        return $this->grades;
    }

    public function getClassrooms()
    {
        return $this->classrooms;
    }

    public function getJourneysCount()
    {
        $classrooms = $this->getClassrooms();
        $total = 0;

        foreach ($classrooms as $classroom) {
            $total += count($classroom->getJourneys());
        }

        return $total;

    }

    public function getClassroomsInGrade($grade_id)
    {
        $classrooms = [];

        foreach ($this->getClassrooms() as $classroom) {
            if($classroom->getGrade()->getId() == $grade_id)
                $classrooms[] = $classroom;
        }

        return $classrooms;

    }

    public function getColorPalette()
    {
        return $this->colorPalette;
    }

    public function getColorPaletteId()
    {
        return $this->color_palette_id;
    }

    public function setColorPaletteId($colorPaletteId)
    {
        $this->color_palette_id = $colorPaletteId;
    }
}
