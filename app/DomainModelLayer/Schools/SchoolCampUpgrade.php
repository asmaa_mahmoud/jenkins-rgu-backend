<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolCampUpgrade extends Entity
{
    public function __construct(SchoolCamp $schoolCamp, $no_of_students, $no_of_activities, $description, $activation_code, $ends_at, $used){
        $this->schoolCamp = $schoolCamp;
        $this->no_of_students = $no_of_students;
        $this->no_of_activities = $no_of_activities;
        $this->ends_at = $ends_at;
        $this->activation_code = $activation_code;
        $this->used = $used;
        $this->description = $description;
    }

    public function getId(){
        return $this->id;
    }

    public function getSchoolCamp(){
        return $this->schoolCamp;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getNoOfStudents(){
        return $this->no_of_students;
    }

    public function setNoOfStudents($noOfStudents){
        $this->no_of_students = $noOfStudents;
    }

    public function getNoOfActivities(){
        return $this->no_of_activities;
    }

    public function setNoOfActivities($noOfActivities){
        $this->no_of_activities = $noOfActivities;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function getUsed(){
        return $this->used;
    }

    public function setUsed($used){
        $this->used = $used;
    }

    public function getActivationCode(){
        return $this->activation_code;
    }

    public function setActivationCode($activationCode){
        $this->activation_code = $activationCode;
    }



}