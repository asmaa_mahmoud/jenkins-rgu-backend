<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class SchoolCampMap extends EntityMap
{
    protected $table = 'school_camp';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "school_camp.deleted_at";

    public function creator(SchoolCamp $schoolCamp){
        return $this->belongsTo($schoolCamp,  User::class , 'creator_id','id');
    }

    public function school(SchoolCamp $schoolCamp){
        return $this->belongsTo($schoolCamp, School::class , 'school_id','id' );
    }

    public function camp(SchoolCamp $schoolCamp){
        return $this->belongsTo($schoolCamp, Camp::class , 'camp_id','id' );
    }

    public function upgrades(SchoolCamp $schoolCamp){
        return $this->hasMany($schoolCamp, SchoolCampUpgrade::class , 'school_camp_id', 'id');
    }

    public function details(SchoolCamp $schoolCamp){
        return $this->hasOne($schoolCamp, SchoolCampDetail::class , 'school_camp_id', 'id');
    }

    public function distributorCamp(SchoolCamp $schoolCamp){
        return $this->hasOne($schoolCamp, SchoolDistributorCamp::class, 'school_camp_id', 'id');
    }


}