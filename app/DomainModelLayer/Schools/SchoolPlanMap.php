<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolPlanMap extends EntityMap {

    protected $table = 'school_plan';

}