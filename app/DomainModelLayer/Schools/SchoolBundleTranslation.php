<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolBundleTranslation extends Entity
{
    public function __construct(SchoolBundle $schoolBundle, $description = null, $language_code)
    {
        $this->schoolBundle = $schoolBundle;
        $this->language_code = $language_code;
        $this->description = $description;
    }

    public function getId(){
        return $this->id;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function getLanguageCode($language_code){
        $this->language_code = $language_code;
    }

    public function setLanguageCode(){
        return $this->language_code;
    }

    public function getSchoolBundle(){
        return $this->schoolBundle;
    }

    public function setSchoolBundle(SchoolBundle $schoolBundle){
        $this->schoolBundle = $schoolBundle;
    }

}