<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Schools\StudentPosition;

class StudentPositionMap extends EntityMap {

    protected $table = 'student_position';
	public $timestamps = true;


    public function user(StudentPosition $studentPosition)
    {
        return $this->belongsTo($studentPosition, User::class , 'user_id', 'id');
    }

    public function course(StudentPosition $studentPosition)
    {
        return $this->belongsTo($studentPosition, Course::class , 'course_id', 'id');
    }

    public function adventure(StudentPosition $studentPosition)
    {
        return $this->belongsTo($studentPosition, Adventure::class , 'adventure_id', 'id');
    }


}