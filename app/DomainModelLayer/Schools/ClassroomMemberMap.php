<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;

class ClassroomMemberMap extends EntityMap {

    protected $table = 'classroom_user';


    public function user(ClassroomMember $classroomMember)
    {
        return $this->belongsTo($classroomMember, User::class , 'user_id', 'id');
    }

    public function role(ClassroomMember $classroomMember)
    {
        return $this->belongsTo($classroomMember, Role::class , 'role_id', 'id');
    }

    public function classroom(ClassroomMember $classroomMember){
        return $this->belongsTo($classroomMember, Classroom::class, 'classroom_id','id');
    }


}