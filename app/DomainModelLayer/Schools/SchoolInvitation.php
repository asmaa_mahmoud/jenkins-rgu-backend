<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolInvitation extends Entity
{

    public function setMaxNumberOfStudents($number){
        $this->max_students = $number;
    }

    public function getMaxNumberOfStudents(){
        return $this->max_students;
    }

    public function setMaxNumberOfCourses($number){
        $this->max_courses = $number;
    }

    public function getMaxNumberOfCourses(){
        return $this->max_courses;
    }
}

