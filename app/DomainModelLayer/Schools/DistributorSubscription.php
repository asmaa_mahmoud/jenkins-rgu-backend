<?php

namespace App\DomainModelLayer\Schools;

use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Schools\Distributor;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;

class DistributorSubscription extends Entity
{
    //region Getters & Setters

    public function __construct(Subscription $subscription,Distributor $distributor, $current_code, $upcoming_code)
    {
        $this->subscription = $subscription;
        $this->distributor = $distributor;
        $this->current_code = $current_code;
        $this->upcoming_code = $upcoming_code;
    }

    public function getSubscription(){
        return $this->subscription;
    }

    public function getDistributor(){
        return $this->distributor;
    }

    public function setDistributor(Distributor $distributor)
    {
        $this->distributor = $distributor;
    }

    public function getCurrentCode(){
        return $this->current_code;
    }

    public function getUpcomingCode(){
        return $this->upcoming_code;
    }

    public function setCurrentCode($code){
        $this->current_code = $code;
    }

    public function setUpcomingCode($code){
        $this->upcoming_code = $code;
    }

    public function getDistributorCharge(){
        return $this->distributorCharge;
    }
}

