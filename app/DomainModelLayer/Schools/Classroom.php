<?php


namespace App\DomainModelLayer\Classroom;
namespace App\DomainModelLayer\Schools;


use Analogue\ORM\Entity;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;
use Analogue\ORM\EntityCollection;
use DB;

class Classroom extends Entity
{

    public function __construct(ClassroomRequestDto $classroomDto = null,Grade $grade = null,School $school)
    {
        if($classroomDto != null && $grade != null)
        {
            $this->name = $classroomDto->name;
            $this->grade = $grade;
            if($classroomDto->max_students != null)
                $this->max_students = $classroomDto->max_students;
            $this->school = $school;
            //$this->classroomMembers = new EntityCollection;
            $this->classroomJourneys = new EntityCollection;
        }       
    }

//    public function addClassroomMembers(ClassroomMember $classroomMember)
//    {
//        $this->classroomMembers->push($classroomMember);
//    }
//
//    public function removeClassroomMembers(ClassroomMember $classroomMember)
//    {
//        $this->classroomMembers->remove($classroomMember);
//    }

    public function addClassroomJourneys(ClassroomJourney $classroomJourney)
    {
        $this->classroomJourneys->push($classroomJourney);
    }

    public function removeClassroomJourneys(ClassroomJourney $classroomJourney)
    {
        $this->classroomJourneys->remove($classroomJourney);
    }


    public function getJourneys(){
        $journeys = array();
        foreach ($this->classroomJourneys as $classroomJourney){
            array_push($journeys,$classroomJourney->getJourney());
        }

        return $journeys;
    }


    public function getMembers()
    {
        return $this->getDefaultGroup()->getGroupMembers();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->name = $Name;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    public function getGradeName()
    {
        return $this->grade->getName();
    }

    /**
     * @param mixed $Grade
     */
    public function setGrade(Grade $Grade)
    {
        $this->grade = $Grade;
    } 

    public function getMaxNumberOfStudents()
    {
        return $this->max_students;
    }

    public function getNumberOfStudents()
    {

        return count($this->getStudents());
    }

    public function getStudents()
    {
        return $this->getDefaultGroup()->getStudents();
    }

    public function isMember($user_id)
    {
        return $this->getDefaultGroup()->isMember($user_id);
    }

    public function getMembersWithRole($roleName){
        return $this->getDefaultGroup()->getMembersWithRole($roleName);
    }
    public function getSchool(){
        return $this->school;
    }

    public function getMainTeacher(){
        $mainTeachers = $this->getDefaultGroup()->getMembersWithRole("main_teacher");
        if(sizeof($mainTeachers) == 0)
            return null;
        else
            return $mainTeachers[0];

    }

    public function getPercentageOfStudentAssigned(){
        $max = $this->getMaxNumberOfStudents();
        $noOfStudents = $this->getNumberOfStudents();
        return ($noOfStudents/$max)*100;
    }

    public function getGroups(){

        return $this->groups;
    }

    public function getDefaultGroup(){
        $groups = $this->getGroups();
        foreach ($groups as $group){
            if($group->getIsDefault() == 1){
                return $group;
            }
        }
    }

    public function checkifThereisCourseWithJourneyInDefaultGroup(Journey $journey){
        $courses = $this->getDefaultGroup()->getCourses();
        $journeyExists = false;
        foreach ($courses as $course) {
            if($course->getJourney()->getId() == $journey->getId()){
                $journeyExists = true;
                break;
            }
        }
        return $journeyExists;
    }

    public function getTotalScore()
    {
        return $this->getDefaultGroup()->getTotalScore();
    }

    // public function getAllMembers()
    // {
    //     $result = [];

    //     foreach ($this->users as $user) {
    //         $result[] = $user->getClassroomInfo($this->creator_id);
    //     }
    //     return $result;
    //     //return array_map([$this,'getMemberInfo'], $this->users);
    // }

    // public function getMemberInfo(User $user)
    // {
    //     return $user->getClassroomInfo($this->creator_id);
    // }
    
}