<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class CampTemplateTranslation extends Entity
{
    public function getTitle(){
        return $this->title;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getSummary(){
        return $this->summary;
    }

    public function getConcepts(){
        return $this->concepts;
    }

    public function getVideoUrl(){
        return $this->video_url;
    }

    public function getTechnicalDetails(){
        return $this->technical_details;
    }

    public function getPrerequisites(){
        return $this->prerequisites;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function getCampTemplte(){
        return $this->campTemplate;
    }


}