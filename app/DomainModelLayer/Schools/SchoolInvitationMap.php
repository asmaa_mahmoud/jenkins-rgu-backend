<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;

class SchoolInvitationMap extends EntityMap {

    protected $table = 'school_invitation';

}