<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Schools\SchoolBundle;
use Analogue\ORM\EntityCollection;

class DistributorCharge extends Entity
{
    public function __construct(DistributorSubscription $subscription, $cost = null, $no_of_students, $no_of_classroom, $description = null)
    {
        $this->subscription = $subscription;
        $this->cost = $cost;
        $this->no_of_students = $no_of_students;
        $this->no_of_classroom = $no_of_classroom;
        $this->description = $description;
        $this->chargeUpgrades = new EntityCollection;
    }

    public function getId(){
        return $this->id;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getNoStudents()
    {
        return $this->no_of_students;
    }

    public function setNoStudents($number)
    {
        $this->no_of_students = $number;
    }

    public function getNoClassroom()
    {
        return $this->no_of_classroom;
    }

    public function setNoClassroom($number)
    {
        $this->no_of_classroom = $number;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDistributorSubscription()
    {
        return $this->subscription;
    }

    public function setDistributorSubscription(DistributorSubscription $subscription)
    {
        $this->subscription = $subscription;
    }

    public function getChargeUpgrades()
    {
        return $this->chargeUpgrades;
    }

    public function addChargeUpgrade(DistributorChargeUpgrade $upgrade)
    {
        return $this->chargeUpgrades->push($upgrade);
    }

    public function removeChargeUpgrade(DistributorChargeUpgrade $upgrade)
    {
        return $this->chargeUpgrades->remove($upgrade);
    }

    public function getChargeDetail()
    {
        return $this->chargeDetail;
    }

    public function setChargeDetail(DistributorChargeUpgrade $detail)
    {
        $this->chargeDetail = $detail;
    }

    public function setBundle(SchoolBundle $bundle){
        $this->bundle = $bundle;
    }

    public function getBundle(){
        return $this->bundle;
    }

}