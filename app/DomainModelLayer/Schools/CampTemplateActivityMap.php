<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class CampTemplateActivityMap extends EntityMap
{
    protected $table = 'camp_template_activity';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "camp_template_activity.deleted_at";

    public function activities(CampTemplateActivity $campActivity){
        return $this->belongsToMany($campActivity, DefaultActivity::class , 'camp_template_activity','camp_template_id','activity_id');
    }

    public function camp(CampTemplateActivity $campActivity){
        return $this->belongsTo($campActivity, CampTemplate::class, 'camp_template_id', 'id');
    }


}