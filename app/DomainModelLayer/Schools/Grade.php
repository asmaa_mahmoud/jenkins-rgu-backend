<?php


namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\Helpers\Mapper;


class Grade extends Entity
{

    public function __construct(School $school,GradeDto $gradeDto)
    {
        $this->school = $school;
        $this->name = $gradeDto->name;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->name = $Name;
    }

    public function getId(){
        return $this->id;
    }

    public function getSchool(){
        return $this->school;
    }
}