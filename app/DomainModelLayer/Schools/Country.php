<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;


class Country extends Entity
{

    public function getName(){
        return $this->country_name;
    }

    public function getCode(){
        return $this->country_code;
    }

    public function getDistributors()
    {
        return $this->distributors;
    }

    public function getDistributorsCount()
    {
        return count($this->getDistributors());
    }
}

