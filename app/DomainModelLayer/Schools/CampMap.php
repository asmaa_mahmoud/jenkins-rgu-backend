<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class CampMap extends EntityMap
{
    protected $table = 'camp';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "camp.deleted_at";

    public function activities(Camp $camp){
        return $this->belongsToMany($camp, Activity::class , 'camp_activity','camp_id','activity_id');
    }

    public function campActivities(Camp $camp){
        return $this->hasMany($camp, CampActivity::class, 'camp_id', 'id');
    }

    public function defaultActivities(Camp $camp){
        return $this->belongsToMany($camp, DefaultActivity::class , 'camp_activity','camp_id','default_activity_id' );
    }

    public function schoolCamp(Camp $camp){
        return $this->hasOne($camp, SchoolCamp::class, 'camp_id', 'id');
    }

    public function users(Camp $camp){
        return $this->belongsToMany($camp, User::class, 'camp_user', 'camp_id', 'user_id');
    }

    public function campUsers(Camp $camp){
        return $this->hasMany($camp, CampUser::class, 'camp_id', 'id');
    }


}