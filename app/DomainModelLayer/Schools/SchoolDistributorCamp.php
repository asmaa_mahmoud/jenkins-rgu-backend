<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolDistributorCamp extends Entity
{
    public function __construct(SchoolCamp $schoolCamp, Distributor $distributor, $activation_code){
        $this->schoolCamp = $schoolCamp;
        $this->distributor = $distributor;
        $this->activation_code = $activation_code;
    }

    public function getId(){
        return $this->id;
    }

    public function getActivationCode(){
        return $this->activation_code;
    }

    public function setActivationCode($activationCode){
        $this->activation_code = $activationCode;
    }

    public function getSchoolCamp(){
        return $this->schoolCamp;
    }

    public function getDistributor(){
        return $this->distributor;
    }

    public function setDistributor(Distributor $distributor){
        $this->distributor = $distributor;
    }

}