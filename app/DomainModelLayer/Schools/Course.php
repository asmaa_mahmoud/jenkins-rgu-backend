<?php

namespace App\DomainModelLayer\Schools;

use App\ApplicationLayer\Schools\Dtos\CourseRequestDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;
use App\Http\Controllers\Schools\GroupController;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;


class Course extends Entity
{
    //region Getters & Setters

    public function __construct(Journey $journey,User $creator,CourseRequestDto $courseRequestDto,$is_default = true)
    {
        $this->name = $courseRequestDto->name;
        $this->ends_at = $courseRequestDto->endsAt;
        $this->starts_at = $courseRequestDto->startsAt;

        $this->is_default = $is_default;
        $this->creator = $creator;
        $this->journey = $journey;

        $this->groups = new EntityCollection;
        $this->assignments = new EntityCollection;
        $this->courseAdventureDeadlines = new EntityCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getJourney(){
        return $this->journey;
    }

    public function setJourney(Journey $journey){
        $this->journey = $journey;
    }

    public function getCreator(){
        return $this->creator;
    }

    public function setCreator(User $creator){
        $this->creator = $creator;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setStartsAt($starts_at){
        $this->starts_at = $starts_at;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }


    public function getIsDefault(){
        return $this->is_default;
    }

    public function setIsDefault($is_default){
        $this->is_default = $is_default;
    }

    public function getGroups(){
        return $this->groups;
    }

    public function addGroup(Group $group){
        return $this->groups->push($group);
    }

    public function removeGroup(Group $group){
        return $this->groups->remove($group);
    }

    public function getClassroom(){
        $groups = $this->getGroups();
        foreach ($groups as $group){
            if($group->getIsDefault())
                return $group->getClassroom();
        }
    }

    public function getAssignments(){
        return $this->assignments;
    }

    public function getNoOfAssignments(){
        return count($this->getAssignments());
    }
     public function getTasksCount()
     {
         return $this->getJourney()->getTasksCount();
     }

    public function getTasks()
    {
        return $this->getJourney()->getTasks();
    }

    public function getQuizzes()
    {
        $quizzes = [];
        foreach ($this->getTasks() as $task) {
            if($task->getQuizzes()->first() != null)
                $quizzes[] = $task->getQuizzes()->first();
        }

        return $quizzes;
    }

     public function getAdventureDeadlines(){
        return $this->courseAdventureDeadlines;
     }

     public  function addAdventureDeadlines(CourseAdventureDeadlines $courseAdventureDeadlines){
         $this->courseAdventureDeadlines->push($courseAdventureDeadlines);
     }

     public function removeAdventureDeadlines(CourseAdventureDeadlines $courseAdventureDeadlines){
         $this->courseAdventureDeadlines->remove($courseAdventureDeadlines);
     }

     public function checkIfAnyBodyProgressed(){
         $students = $this->getClassroom()->getDefaultGroup()->getStudents();
         $anyBodyProgressed = false;
         foreach ($students as $student){
             $studentProgress = $student->getStudentProgress();
             if($studentProgress != null){
                 foreach ($studentProgress as $studentSingleProgress){
                     if($this->getId() == $studentSingleProgress->getCourse()->getId()){
                         $anyBodyProgressed = true;
                         break;
                     }
                 }
             }
         }
         return $anyBodyProgressed;
     }

     public function getCreatedAt()
     {
         return $this->created_at;
     }


}

