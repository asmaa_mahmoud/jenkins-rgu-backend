<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;

class SchoolCamp extends Entity
{
    public function __construct(School $school, Camp $camp, User $creator, $no_of_students, $no_of_activities, $creator_type, $starts_at, $ends_at, $locked = 0){
        $this->school = $school;
        $this->camp = $camp;
        $this->creator = $creator;
        $this->no_of_students = $no_of_students;
        $this->no_of_activities = $no_of_activities;
        $this->creator_type = $creator_type;
        $this->starts_at = $starts_at;
        $this->ends_at = $ends_at;
        $this->locked = $locked;
    }

    public function getId(){
        return $this->id;
    }

    public function getNoOfStudents(){
        return $this->no_of_students;
    }

    public function setNoOfStudents($noOfStudents){
        $this->no_of_students = $noOfStudents;
    }

    public function getNoOfActivities(){
        return $this->no_of_activities;
    }

    public function setNoOfActivities($noOfActivities){
        $this->no_of_activities = $noOfActivities;
    }

    public function getCreatorType(){
        return $this->creator_type;
    }

    public function setCreatorType($creatorType){
        $this->creator_type = $creatorType;
    }

    public function getCreator(){
        return $this->creator;
    }

    public function setCreator(User $creator){
        $this->creator = $creator;
    }

    public function getStartsAt(){
        return $this->starts_at;
    }

    public function setStartsAt($starts_at){
        $this->starts_at = $starts_at;
    }

    public function getEndsAt(){
        return $this->ends_at;
    }

    public function setEndsAt($ends_at){
        $this->ends_at = $ends_at;
    }

    public function getLocked(){
        return $this->locked;
    }

    public function setLocked($locked){
        $this->locked = $locked;
    }

    public function getSchool(){
        return $this->school;
    }

    public function getCamp(){
        return $this->camp;
    }

    public function getCampUpgrades(){
        return $this->upgrades;
    }

    public function getCampDetail(){
        return $this->details;
    }

    public function getDuration(){
        return ($this->ends_at - $this->starts_at);
    }

    public function addDistributorCamp(SchoolDistributorCamp $distributorCamp){
        $this->distributorCamp->push($distributorCamp);
    }

    public function getDistributorCamp(){
       return $this->distributorCamp;
    }

    public function addCampDetails(SchoolCampDetail $campDetail){
        $this->details->push($campDetail);
    }

}