<?php

namespace App\DomainModelLayer\Schools;

use App\ApplicationLayer\Schools\Dtos\CourseRequestDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Journey;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;
use App\Http\Controllers\Schools\GroupController;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;


class StudentClickMission extends Entity
{
    //region Getters & Setters

    public function __construct(User $user,Course $course)
    {
        $this->user = $user;
        $this->course = $course;
        $this->time = date("Y-m-d",time());
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(){
        return $this->user;
    }

    public function setUser(User $user){
        $this->user = $user;
    }

    public function getCourse(){
        return $this->course ;
    }

    public function setCourse(Course $course){
        $this->course = $course;
    }

    public function getTime(){
        return $this->time;
    }

    public function setTime($time){
        $this->time = $time;
    }




}

