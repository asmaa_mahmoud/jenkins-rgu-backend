<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolChargeUpgrade extends Entity
{
    public function __construct(SchoolCharge $schoolCharge, $charge_id, $cost, $no_of_students, $no_of_classroom, $description)
    {
        $this->schoolCharge = $schoolCharge;
        $this->charge_id = $charge_id;
        $this->cost = $cost;
        $this->no_of_students = $no_of_students;
        $this->no_of_classroom = $no_of_classroom;
        $this->description = $description;
    }

    public function getId(){
        return $this->id;
    }

    public function getChargeID(){
        return $this->charge_id;
    }

    public function setChargeId($charge){
        $this->charge_id = $charge;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getNoStudents()
    {
        return $this->no_of_students;
    }

    public function setNoStudents($number)
    {
        $this->no_of_students = $number;
    }

    public function getNoClassroom()
    {
        return $this->no_of_classroom;
    }

    public function setNoClassroom($number)
    {
        $this->no_of_classroom = $number;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getSchoolCharge()
    {
        return $this->schoolCharge;
    }

    public function setSchoolCharge(SchoolCharge $school_charge)
    {
        $this->schoolCharge = $school_charge;
    }

    public function setBundle(SchoolBundle $bundle){
        $this->bundle = $bundle;
    }

    public function getBundle(){
        return $this->bundle;
    }

    public function getStartDate(){
        return $this->created_at;
    }

}