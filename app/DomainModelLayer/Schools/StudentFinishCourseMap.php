<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\StudentFinishCourse;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\StudentClickMission;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;

class StudentFinishCourseMap extends EntityMap {

    protected $table = 'student_finish_course';

    public function course(StudentFinishCourse $studentFinishCourse)
    {
        return $this->belongsTo($studentFinishCourse, Course::class, 'course_id','id');
    }
    public function user(StudentFinishCourse $studentFinishCourse)
    {
        return $this->belongsTo($studentFinishCourse, User::class, 'user_id','id');
    }
}