<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Accounts\User;

class CampActivityProgress extends Entity
{
    public function __construct(Task $task, User $user, Camp $camp, $score, $trials, $first_success = null, $timeTaken = null, $bestTimeTaken = null, $numberOfBlocks = null, $bestNumberOfBlocks = null)
    {
        $this->task = $task;
        $this->user = $user;
        $this->score = $score;
        $this->camp = $camp;
        $this->no_of_trials = $trials;
        if($first_success != null)
            $this->first_success = $first_success;
        $this->task_duration = $timeTaken;
        $this->best_task_duration = $bestTimeTaken;
        $this->no_of_blocks = $numberOfBlocks;
        $this->best_blocks_number = $bestNumberOfBlocks;
    }

    public function getId(){
        return $this->id;
    }

    public function getBestTaskDuration(){
        return $this->best_task_duration;
    }

    public function setBestTaskDuration($bestTimeTaken){
        $this->best_task_duration = $bestTimeTaken;
    }

    public function getBestBlocksNumber(){
        return $this->best_blocks_number;
    }

    public function setBestBlocksNumber($bestNumberOfBlocks){
        $this->best_blocks_number = $bestNumberOfBlocks;
    }

    public function getNoBlocks(){
        return $this->no_of_blocks;
    }

    public function setNoBlocks($numberOfBlocks){
        $this->no_of_blocks = $numberOfBlocks;
    }

    public function getTaskDuration()
    {
        return $this->task_duration;
    }

    public function setTaskDuration($duration)
    {
        $this->task_duration = $duration;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;
    }

    public function getUnlockedByCoins()
    {
        return $this->unlocked_by_coins;
    }

    public function setUnlockedByCoins($unlockedByCoins)
    {
        $this->unlocked_by_coins = $unlockedByCoins;
    }

    public function getFirstSuccess()
    {
        return $this->first_success;
    }

    public function setFirstSuccess($first)
    {
        $this->first_success = $first;
    }

    public function getNoTrials()
    {
        return $this->no_of_trials;
    }

    public function setNoTrials($number)
    {
        $this->no_of_trials = $number;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
        $this->defaultActivity = $defaultActivity;
    }

    public function getCamp()
    {
        return $this->camp;
    }

    public function setCamp(Camp $camp)
    {
        $this->camp = $camp;
    }

    public function isSuccess(){
        if($this->getFirstSuccess() == null)
            return false;
        else if($this->getFirstSuccess() == 0)
            return false;
        else
            return true;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function getUpdatedAt(){
        return $this->updated_at;
    }

    public function setUpdatedAt($updatedAt){
        $this->updated_at = $updatedAt;
    }

    public function getNo_of_trails(){
        return $this->no_of_trials;
    }

}