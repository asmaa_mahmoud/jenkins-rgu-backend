<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;
use LaravelLocalization;

class SchoolBundle extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getNoStudents()
    {
        return $this->students;
    }

    public function getNoClassroom()
    {
        return $this->classrooms;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

}