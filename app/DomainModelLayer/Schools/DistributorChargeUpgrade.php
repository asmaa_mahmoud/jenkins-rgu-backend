<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class DistributorChargeUpgrade extends Entity
{
    public function __construct(DistributorCharge $distributorCharge, $cost = null, $no_of_students, $no_of_classroom, $description = null, $upgrade_code, $plan_name, $used = 0)
    {
        $this->distributorCharge = $distributorCharge;
        $this->cost = $cost;
        $this->no_of_students = $no_of_students;
        $this->no_of_classroom = $no_of_classroom;
        $this->description = $description;
        $this->upgrade_code = $upgrade_code;
        $this->plan_name = $plan_name;
        $this->used = $used;
    }

    public function getId(){
        return $this->id;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getNoStudents()
    {
        return $this->no_of_students;
    }

    public function setNoStudents($number)
    {
        $this->no_of_students = $number;
    }

    public function getNoClassroom()
    {
        return $this->no_of_classroom;
    }

    public function setNoClassroom($number)
    {
        $this->no_of_classroom = $number;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDistributorCharge()
    {
        return $this->distributorCharge;
    }

    public function setDistributorCharge(DistributorCharge $distributorCharge)
    {
        $this->distributorCharge = $distributorCharge;
    }

    public function setBundle(SchoolBundle $bundle){
        $this->bundle = $bundle;
    }

    public function getBundle(){
        return $this->bundle;
    }

    public function getUpgradeCode(){
        return $this->upgrade_code;
    }

    public function setUpgradeCode($upgradeCode){
        $this->upgrade_code = $upgradeCode;
    }

    public function getUsed(){
        return $this->used;
    }

    public function setUsed($used){
        $this->used = $used;
    }

    public function getPlanName(){
        return $this->plan_name;
    }

    public function setPlanName($planName){
        $this->plan_name = $planName;
    }

}