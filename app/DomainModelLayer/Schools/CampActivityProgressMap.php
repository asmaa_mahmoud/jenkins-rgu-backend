<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;

class CampActivityProgressMap extends EntityMap
{
    protected $table = 'camp_activity_progress';
    public $timestamps = true;

    public function user(CampActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, User::class , 'user_id', 'id');
    }

    public function task(CampActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Task::class , 'task_id', 'id');
    }

    public function activity(CampActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(CampActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function camp(CampActivityProgress $activityProgress)
    {
        return $this->belongsTo($activityProgress, Camp::class , 'camp_id', 'id');
    }
}