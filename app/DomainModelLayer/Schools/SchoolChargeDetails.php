<?php

namespace App\DomainModelLayer\Schools;

use Analogue\ORM\Entity;

class SchoolChargeDetails extends Entity
{
    public function __construct(SchoolCharge $schoolCharge, $no_of_students, $no_of_classroom,$planName = null)
    {
        $this->schoolCharge = $schoolCharge;
        $this->no_of_students = $no_of_students;
        $this->no_of_classroom = $no_of_classroom;
        $this->name = $planName;
    }

    public function getId(){
        return $this->id;
    }

    public function getNoStudents()
    {
        return $this->no_of_students;
    }

    public function setNoStudents($number)
    {
        $this->no_of_students = $number;
    }

    public function getNoClassroom()
    {
        return $this->no_of_classroom;
    }

    public function setNoClassroom($number)
    {
        $this->no_of_classroom = $number;
    }

    public function getSchoolCharge()
    {
        return $this->schoolCharge;
    }

    public function setSchoolCharge(SchoolCharge $school_charge)
    {
        $this->schoolCharge = $school_charge;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

}