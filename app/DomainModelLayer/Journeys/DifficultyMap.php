<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;

class DifficultyMap extends EntityMap
{
    protected $table = 'difficulty';
    public $timestamps = false;

    public function activities(Difficulty $difficulty)
    {
        return $this->hasMany($difficulty, Activity::class , 'difficulty_id', 'id');
    }

    public function translations(Difficulty $difficulty)
    {
        return $this->hasMany($difficulty, DifficultyTranslation::class , 'difficulty_id', 'id');
    }

}