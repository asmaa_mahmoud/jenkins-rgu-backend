<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:25 PM
 */

namespace App\DomainModelLayer\Journeys;
use Analogue\ORM\EntityMap;


class MissionEditorMap extends EntityMap
{
    protected $table = 'mission_editor';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_editor.deleted_at";

    public function task(MissionEditor $missionEditor)
    {
        return $this->belongsTo($missionEditor, Task::class , 'task_id', 'id');
    }

    public function translations(MissionEditor $missionEditor)
    {
        return $this->hasMany($missionEditor, MissionEditorTranslation::class , 'mission_editor_id', 'id');
    }

    public function missionScreens(MissionEditor $missionEditor)
    {
        return $this->hasMany($missionEditor, MissionEditorScreen::class , 'mission_id', 'id');
    }

    public function quiz(MissionEditor $missionEditor)
    {
        return $this->belongsTo($missionEditor, Quiz::class , 'quiz_id', 'id');
    }
}