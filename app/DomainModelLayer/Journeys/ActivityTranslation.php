<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Schools\Dtos\ActivityTranslationDto;

class ActivityTranslation extends Entity
{
    public function __construct(Activity $activity, DefaultActivity $defaultActivity, ActivityTranslationDto $activityTranslation){
        $this->activity = $activity;
        $this->defaultActivity = $defaultActivity;
        $this->name = $activityTranslation->name;
        $this->description = $activityTranslation->description;
        $this->language_code = $activityTranslation->language_code;
        $this->summary = $activityTranslation->summary;
        $this->concepts = $activityTranslation->concepts;
        $this->technical_details = $activityTranslation->technical_details;
        $this->prerequisites = $activityTranslation->prerequisites;
        $this->video_url = $activityTranslation->video_url;
    }

    public static function searchable(){
        $searchable = [
            'name',
            'description'
        ];
        return $searchable;

    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getSummary(){
        return $this->summary;
    }

    public function getConcepts(){
        return $this->concepts;
    }

    public function getModuleCourseObjectives(){
        return $this->course_module_objectives;
    }

    public function getModuleObjectives(){
        return $this->module_objectives;
    }

    public function getVideoUrl(){
        return $this->video_url;
    }

    public function getTechnicalDetails(){
        return $this->technical_details;
    }

    public function getPrerequisites(){
        return $this->prerequisites;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

}