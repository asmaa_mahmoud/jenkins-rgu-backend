<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\mcqQuestion;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\MatchValue;

class MatchQuestionMap extends EntityMap {

    protected $table = 'match_question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "match_question.deleted_at";



    public function question(MatchQuestion $matchQuestion)
    {
        return $this->belongsTo($matchQuestion,Question::class, 'question_id', 'id');
    }

    
    public function matchKeys(MatchQuestion $matchQuestion)
    {
        return $this->hasMany($matchQuestion,MatchKey::class, 'match_question_id', 'id')->orderBy('order');
    }

    public function matchValues(MatchQuestion $matchQuestion)
    {
        return $this->hasMany($matchQuestion,MatchValue::class, 'match_question_id', 'id')->orderBy('order');
    }
   

 
}