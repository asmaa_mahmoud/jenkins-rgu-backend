<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;

class MissionSavings extends Entity
{
    public function __construct(Mission $mission, User $user,$name,$xml,$journey_id,$adventure_id,$timeTaken)
    {
        $this->mission = $mission;
        $this->user = $user;
        $this->name = $name;
        $this->xml = $xml;
        $this->journey_id = $journey_id;
        $this->adventure_id = $adventure_id;
        $this->timetaken = $timeTaken;
    }

    public function setXml($xml)
    {
        $this->xml = $xml;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->xml;
    }

    public function getTimetaken(){
        return $this->timetaken;
    }

    public function setTimetaken($timeTaken){
        $this->timetaken = $timeTaken;
    }
}
