<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\AdventureTranslation;

class MissionMcqQuestionMap extends EntityMap {

    protected $table = 'mission_mcq_question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_mcq_question.deleted_at";

    // protected $with = ['choices','programmingLanguage'];

    public function choices(MissionMcqQuestion $question)
    {
        return $this->hasMany($question, MissionMcqChoose::class , 'question_id', 'id');
    }

    public function programmingLanguage(MissionMcqQuestion $question)
    {
        return $this->belongsTo($question, ProgrammingLanguageType::class , 'programming_language_type_id', 'id');
    }


}