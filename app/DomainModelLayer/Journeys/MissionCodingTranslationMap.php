<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:31 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionCodingTranslationMap extends EntityMap
{
    protected $table = 'mission_coding_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_coding_translation.deleted_at";
}