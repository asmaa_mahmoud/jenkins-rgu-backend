<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class JourneyTranslationMap extends EntityMap {

    protected $table = 'journey_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "journey_translation.deleted_at";


}