<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;

class MissionTypeMap extends EntityMap {

    protected $table = 'type';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "type.deleted_at";


}