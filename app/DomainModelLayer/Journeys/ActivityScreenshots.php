<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ActivityScreenshots extends Entity
{
    public function getScreenshotUrl(){
        return $this->screenshot_url;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

    public function getActivity(){
        return $this->activity;
    }
}