<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\DropdownChoice;
use App\DomainModelLayer\Journeys\DropdownChoiceTranslation;

class DropdownChoiceTranslationMap extends EntityMap {

    protected $table = 'dropdown_choice_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dropdown_choice_translation.deleted_at";



    public function dropdown_choice(DropdownChoiceTranslation $dropdownChoiceTranslation)
    {
        return $this->belongsTo($dropdownChoiceTranslation,DropdownChoice::class, 'dropdown_choice_id', 'id');
    }




}