<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ActivityTag extends Entity
{
    public function getActivity(){
        return $this->activity;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

    public function getTag(){
        return $this->tag;
    }
}