<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Quizzes\QuizTutorialsDto;
use LaravelLocalization;


class QuizTutorials extends Entity
{

    public function __construct(QuizTutorialsDto $quizTutorialsDto = null,Quiz $quiz)
    {
        if($quizDto != null && $quiz != null)
        {
            $this->image_url= $quizTutorialsDto->imageUrl;
            $this->video_url = $quizTutorialsDto->videoUrl;
            $this->description = $quizTutorialsDto->description;
            $this->quiz = $quiz;
        }
    }

    public function getImageUrl()
    {
        return $this->image_url;
    }

    public function getVideoUrl()
    {
        return $this->video_url;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

}
