<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Missions\MissionDto;
use LaravelLocalization;


class MissionHtml extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getLessonType()
    {
        return $this->lesson_type;
    }

    public function getWithSteps()
    {
        return $this->with_steps;
    }

    public function getOrder()
    {
        return $this->task->getOrder();
    }

    public function getPositionX()
    {
        return $this->task->getPositionX();
    }

    public function getPositionY()
    {
        return $this->task->getPositionY();
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getSteps(){
        return $this->steps;
    }

    public function getSections(){
        return $this->sections;
    }

    public function getTranslations(){
        return $this->translations;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getModelAnswerLink()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getModelAnswerLink();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getModelAnswerLink();
        }
        return $english_translation;
    }

    public function getPdfLink()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->pdfTranslations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getPdfLink();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getPdfLink();
        }
        return $english_translation;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getPracticeTask(){
        return $this->practiceTask;
    }

    public function getIsMiniProject(){
        return $this->is_mini_project;
    }

    public function getHtmlMissionType(){
        if($this->is_mini_project=='1')
            return 'mini_project';
        else
            return 'html_mission';
    }

    public function miniProjectType(){
        if($this->miniProjectType){
            return $this->miniProjectType->type;
        }else{
            return 'Not Specified';
        }
    }

    public function htmlMissionType(){
        if($this->htmlMissionType){
            return $this->htmlMissionType->name;
        }else{
            return 'Not Specified';
        }
    }

    public function getQuiz(){
        return $this->quiz;
    }
}