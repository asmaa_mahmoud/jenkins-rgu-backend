<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class FreeJourneyTask extends Entity
{

    public function getOrder()
    {
        return $this->order;
    }

    public function getTask()
    {
    	return $this->task;
    }

    public function getJourney(){
        return $this->journey;
    }

}
