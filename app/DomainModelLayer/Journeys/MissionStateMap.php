<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/11/17
 * Time: 1:25 PM
 */
namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\TaskOrder;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\Quiz;

class MissionStateMap extends EntityMap {

    protected $table = 'mission_state';
    public $timestamps = true;


    public function missions(Task $task)
    {
        return $this->hasMany($task, Mission::class , 'mission_state_id', 'id');
    }

}