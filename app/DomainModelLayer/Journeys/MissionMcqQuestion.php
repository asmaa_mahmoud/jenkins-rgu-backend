<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MissionMcqQuestion extends Entity
{
    public function getProgrammingLanguage()
    {
        if($this->programmingLanguage == null)
            return null;
        else
            return $this->programmingLanguage->getName();
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getChoices(){
        return $this->choices;
    }
}
