<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DropdownTranslation extends Entity
{
    

    public function getId()
    {
        return $this->id;
    }

    
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getRightExplanation()
    {
        return $this->right_explanation;
    }

    public function getWrongExplanation()
    {
        return $this->wrong_explanation;
    }


   

}
