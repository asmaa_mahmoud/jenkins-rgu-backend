<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class SceneMap extends EntityMap {

    protected $table = 'scenes';

}