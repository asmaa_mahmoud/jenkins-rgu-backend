<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 24/10/2018
 * Time: 10:21 AM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class TaskResourceMap extends EntityMap
{
    public function task(TaskResource $taskResource)
    {
        return $this->belongsTo($taskResource, Task::class , 'task_id', 'id');
    }

    public function resource(TaskResource $taskResource)
    {
        return $this->belongsTo($taskResource, Resource::class , 'resource_id', 'id');
    }
}