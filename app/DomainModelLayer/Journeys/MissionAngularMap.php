<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:25 PM
 */

namespace App\DomainModelLayer\Journeys;
use Analogue\ORM\EntityMap;


class MissionAngularMap extends EntityMap
{
    protected $table = 'mission_angular';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_angular.deleted_at";

    public function task(MissionAngular $missionAngular)
    {
        return $this->belongsTo($missionAngular, Task::class , 'task_id', 'id');
    }

    public function translations(MissionAngular $missionAngular)
    {
        return $this->hasMany($missionAngular, MissionAngularTranslation::class , 'mission_angular_id', 'id');
    }

    public function files(MissionAngular $missionAngular)
    {
        return $this->hasMany($missionAngular, MissionAngularFile::class , 'mission_angular_id', 'id');
    }

    public function dependencies(MissionAngular $missionAngular)
    {
        return $this->hasMany($missionAngular, MissionAngularDependency::class , 'mission_angular_id', 'id');
    }

    public function missionScreens(MissionAngular $missionAngular)
    {
        return $this->hasMany($missionAngular, MissionAngularScreen::class , 'mission_angular_id', 'id');
    }

    public function quiz(MissionAngular $missionAngular)
    {
        return $this->belongsTo($missionAngular, Quiz::class , 'quiz_id', 'id');
    }
}