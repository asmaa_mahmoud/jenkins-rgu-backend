<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class QuizTutorialsTranslationMap extends EntityMap {

    protected $table = 'quiz_tutorial_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "quiz_tutorial_translation.deleted_at";


}