<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class FreeJourney extends Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getType(){
        return ($this->type == 0 ? 'hour_of_code' : 'roboclub_event');
    }

    public function getTasks()
    {
        $tasks = [];
        foreach ($this->tasks as $task) {
            $tasks[] = $task;
        }
        return $tasks;
    }

    public function translations(){
        return $this->translations;
    }

    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

    public function getImageUrl(){
        return $this->image_url;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }


}
