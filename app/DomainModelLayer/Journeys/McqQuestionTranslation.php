<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class McqQuestionTranslation extends Entity
{

    public static function searchable(){
        $searchable = [
            'body',
        ];
        return $searchable;

    }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getBody()
    {
        return $this->body;
    }

}
