<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class MissionTranslationMap extends EntityMap {

    protected $table = 'mission_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_translation.deleted_at";


}