<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 24/10/2018
 * Time: 10:17 AM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;


class TaskResource extends Entity
{
    public function getTask(){
        return $this->task;
    }

    public function getResource(){
        return $this->resource;
    }
}