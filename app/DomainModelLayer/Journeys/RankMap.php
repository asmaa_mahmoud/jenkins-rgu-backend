<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;

class RankMap extends EntityMap
{
    protected $table = 'rank';
    public $timestamps = false;

    public function translations(Rank $rank)
    {
        return $this->hasMany($rank, RankTranslation::class , 'rank_id', 'id');
    }

}