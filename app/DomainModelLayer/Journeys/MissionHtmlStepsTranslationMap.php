<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionHtmlStepsTranslationMap extends EntityMap {

    protected $table = 'mission_html_steps_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_html_steps_translation.deleted_at";
}