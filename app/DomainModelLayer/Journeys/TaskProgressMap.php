<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Accounts\User;

class TaskProgressMap extends EntityMap {

    protected $table = 'task_progress';
    public $timestamps = true;
//    public $softDeletes = true;
//    protected $deletedAtColumn = "task_progress.deleted_at";


    public function task(TaskProgress $taskProgress)
    {
        return $this->belongsTo($taskProgress, Task::class , 'task_id', 'id');
    }

    public function user(TaskProgress $taskProgress)
    {
        return $this->belongsTo($taskProgress, User::class , 'user_id', 'id');
    }

    public function journey(TaskProgress $taskProgress)
    {
        return $this->belongsTo($taskProgress, Journey::class , 'journey_id', 'id');
    }

    public function adventure(TaskProgress $taskProgress)
    {
        return $this->belongsTo($taskProgress, Adventure::class , 'adventure_id', 'id');
    }


}