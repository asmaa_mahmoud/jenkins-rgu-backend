<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class CampusUserStreaksMap extends EntityMap
{
    protected $table = 'campus_user_streaks';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAt = "campus_user_streaks.deleted_at";

    public function user(CampusUserStreaks $campusUserStreaks)
    {
        return $this->belongsTo($campusUserStreaks, User::class , 'user_id', 'id');
    }

}