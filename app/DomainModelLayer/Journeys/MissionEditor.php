<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:22 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;


class MissionEditor extends Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getOrder()
    {
        return $this->task->getOrder();
    }

    public function getPositionX()
    {
        return $this->task->getPositionX();
    }

    public function getPositionY()
    {
        return $this->task->getPositionY();
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getTranslations(){
        return $this->translations;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getHtml(){
        return $this->html;
    }

    public function getCss(){
        return $this->css;
    }

    public function getJs(){
        return $this->js;
    }

    public function getType(){
        return $this->type;
    }
    public function getTests(){
        return $this->tests;
    }

    public function getScreenshots(){
        return $this->missionScreens;
    }

    public function getModelAnswer(){
        return $this->model_answer;
    }

    public function getModelAnswerDetailed(){
        $model_answer = json_decode($this->model_answer, true);
        $answerDto=[];
        $answerDto['html_model_answer']=$answerDto['css_model_answer'] =$answerDto['js_model_answer']='';
        if($model_answer!==null){
            foreach ($model_answer as $answer){
                if($answer['language'] == 'html')
                    $answerDto['html_model_answer'] = $answer['model_answer'];
                if($answer['language'] == 'css')
                    $answerDto['css_model_answer'] = $answer['model_answer'];
                if($answer['language'] == 'js')
                    $answerDto['js_model_answer'] = $answer['model_answer'];
            }

        }

        return $answerDto;
    }

    public function getModelAnswerStructuredAsUserCode(){
        $model_answer_detailed = $this->getModelAnswerDetailed();
        $answerDto=[];
        array_push($answerDto, ['language' => 'html', 'code' => $model_answer_detailed["html_model_answer"]]);
        array_push($answerDto, ['language' => 'css', 'code' => $model_answer_detailed["css_model_answer"]]);
        array_push($answerDto, ['language' => 'js', 'code' => $model_answer_detailed["js_model_answer"]]);

        return json_encode($answerDto, JSON_HEX_QUOT);
 
        
    }
    public function getQuiz(){
        return $this->quiz;
    }
    public function getQuiz_id(){
        if($this->quiz!==null)
            return $this->quiz->id;
        else
            return null;
    }
    public function getQuiz_type(){
        if($this->quiz!==null)
            return $this->getQuiz()->getType()->getName();
        else
            return null;
    }
}