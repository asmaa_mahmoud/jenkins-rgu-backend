<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MatchValueTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'value'
        ];
        return $searchable;

    }
    

    public function getId()
    {
        return $this->id;
    }

    
    public function getLanguageCode()
    {
        return $this->language_code;
    }


    public function getText()
    {
        return $this->value;
    }

    
    public function getExplanation()
    {
        return $this->explanation;
    }
 

}
