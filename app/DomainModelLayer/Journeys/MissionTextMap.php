<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\AdventureTranslation;

class MissionTextMap extends EntityMap {

    protected $table = 'mission_text';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_text.deleted_at";

    // protected $with = ['mission'];

    public function mission(MissionText $missionText)
    {
        return $this->belongsTo($missionText, Mission::class , 'mission_id', 'id');
    }

    public function questions(MissionText $missionText){
        return $this->hasMany($missionText, MissionTextQuestion::class , 'mission_text_id', 'id');
    }

}