<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\DomainModelLayer\Accounts\PlanHistory;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;

class JourneyData extends Entity
{
    public function getGrade()
    {
        return $this->grade;
    }
}