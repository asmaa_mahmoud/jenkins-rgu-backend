<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Accounts\User;

class TaskProgressActivityMap extends EntityMap {

    protected $table = 'activity_progress';
    public $timestamps = true;


    public function task(TaskProgressActivity $taskProgress)
    {
        return $this->belongsTo($taskProgress, Task::class , 'task_id', 'id');
    }

    public function user(TaskProgressActivity $taskProgress)
    {
        return $this->belongsTo($taskProgress, User::class , 'user_id', 'id');
    }

    public function activity(TaskProgressActivity $taskProgress)
    {
        return $this->belongsTo($taskProgress, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(TaskProgressActivity $taskProgress)
    {
        return $this->belongsTo($taskProgress, DefaultActivity::class , 'default_activity_id', 'id');
    }


}