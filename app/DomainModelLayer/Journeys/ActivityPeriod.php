<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class ActivityPeriod extends Entity
{
    public function __construct($period, $initial_period){
        $this->period = $period;
        $this->initial_period = $initial_period;
    }

    public function getId(){
        return $this->id;
    }

    public function getPeriod(){
        return $this->period;
    }

    public function setPeriod($period){
        $this->period = $period;
    }

    public function getInitialPrice(){
        return $this->initial_price;
    }

    public function setInitialPrice($initialPrice){
        $this->initial_price = $initialPrice;
    }

    public function getAccountType(){
        return $this->accountType;
    }

    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

}