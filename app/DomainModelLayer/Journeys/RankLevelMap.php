<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;

class RankLevelMap extends EntityMap
{
    protected $table = 'rank_level';
    public $timestamps = false;


    public function level(RankLevel $rankLevel)
    {
        return $this->hasOne($rankLevel, Level::class , 'id', 'level_id');
    }

    public function rank(RankLevel $rankLevel)
    {
        return $this->hasOne($rankLevel, Rank::class , 'id', 'rank_id');
    }

}