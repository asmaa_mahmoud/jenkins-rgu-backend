<?php

namespace App\DomainModelLayer\Journeys;

use App\ApplicationLayer\Schools\Dtos\ActivityDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Difficulty;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;

class Activity extends Entity
{
    public function __construct(Difficulty $difficulty, User $creator, ActivityDto $activityDto, $isB2B = true, $isB2C = false){
        $this->difficulty = $difficulty;
        $this->creator = $creator;
        $this->age_from = $activityDto->ageFrom;
        $this->age_to = $activityDto->ageTo;
        $this->icon_url = $activityDto->iconUrl;
        $this->image_url = $activityDto->imageUrl;
        $this->is_b2b = $isB2B;
        $this->is_b2c = $isB2C;
    }

    public function getId(){
        return $this->id;
    }

    public function getDifficulty(){
        return $this->difficulty;
    }

    public function setDifficulty(Difficulty $difficulty){
        $this->difficulty = $difficulty;
    }

    public function getCreator(){
        return $this->creator;
    }

    public function setCreator(User $creator){
        $this->creator = $creator;
    }

    public function getAgeFrom(){
        return $this->age_from;
    }

    public function setAgeFrom($age_from){
        $this->age_from = $age_from;
    }

    public function getAgeTo(){
        return $this->age_to;
    }

    public function setAgeTo($age_to){
        $this->age_to = $age_to;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function setIconUrl($icon_url){
        $this->icon_url = $icon_url;
    }

    public function getImageUrl(){
        return $this->image_url;
    }

    public function setImageUrl($image_url){
        $this->image_url = $image_url;
    }

    public function translate($column){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->$column;
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->$column;
        }
        return $english_translation;
    }

    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

    public function getEnglishName(){
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == 'en')
                return $translation->getName();
        }
        return $english_translation;
    }

    public function getDescription(){
        return $this->translate(getDescription());
    }

    public function getSummary(){
        return $this->translate(getSummary());
    }

    public function getConcepts(){
        return $this->translate(getConcepts());
    }

    public function getTechnicalDetails(){
        return $this->translate(getTechnicalDetails());
    }

    public function getPrerequisites(){
        return $this->translate(getPrerequisites());
    }

    public function getTasks(){
        return $this->tasks;
    }

    public function getTags(){
        return $this->tags;
    }

    public function getScreenshots(){
        return $this->screenshots;
    }

    public function getCamps(){
        return $this->camps;
    }

    public function getTasksCount(){
        return count($this->getTasks());
    }

    public function getActivityUnlockable(){
        return $this->activityUnlockable;
    }

    public function isB2B(){
        return $this->is_b2b;
    }

    public function isB2C(){
        return $this->is_b2c;
    }

    public function getLearningPaths(){
        return $this->learningPaths;
    }

    public function getTasksUsersCurrentSteps(){
        return $this->tasksUsersCurrentSteps;
    }
    public function setTasksUsersCurrentSteps($currentSteps){
        $this->tasksUsersCurrentSteps = $currentSteps;
    }

}