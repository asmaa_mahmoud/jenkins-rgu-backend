<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Unlockable;

class JourneyUnlockableMap extends EntityMap {

    protected $table = 'journey_unlockable';
    public $timestamps = true;

    public function journey(JourneyUnlockable $journey_unlockable)
    {
        return $this->belongsTo($journey_unlockable, Journey::class , 'journey_id', 'id');
    }

    public function unlockable(JourneyUnlockable $journey_unlockable)
    {
        return $this->belongsTo($journey_unlockable, Unlockable::class , 'unlockable_id', 'id');
    }

}