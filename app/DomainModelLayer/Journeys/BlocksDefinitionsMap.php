<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class BlocksDefinitionsMap extends EntityMap
{
    protected $table = 'blocks_definitions';
    public $timestamps = true;

    public function block(BlocksDefinitions $blocksDefinition)
    {
        return $this->belongsTo($blocksDefinition, Blocks::class , 'Blocks_Id', 'id');
    }
}