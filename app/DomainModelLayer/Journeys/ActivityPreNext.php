<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ActivityPreNext extends Entity
{
    public function __construct(DefaultActivity $defaultMainActivity, DefaultActivity $defaultActivity, $pre_or_next, $order){
        $this->defaultMainActivity = $defaultMainActivity;
        $this->defaultActivity = $defaultActivity;
        $this->pre_or_next = $pre_or_next;
        $this->order = $order;
    }

    public function getId(){
        return $this->id;
    }

    public function getMainActivity(){
        return $this->defaultMainActivity;
    }

    public function getActivity(){
        return $this->defaultActivity;
    }

    public function getPreNext(){
        return $this->pre_or_next;
    }

    public function setPreNext($pre_or_next){
        $this->pre_or_next = $pre_or_next;
    }

    public function getOrder(){
        return $this->order;
    }

    public function setOrder($order){
        $this->order = $order;
    }
}