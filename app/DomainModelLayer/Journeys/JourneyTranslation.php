<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class JourneyTranslation extends Entity
{

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getSummary()
    {
        return $this->summary;
    }

}
