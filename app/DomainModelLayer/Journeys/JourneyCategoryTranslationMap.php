<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class JourneyCategoryTranslationMap extends EntityMap {

    protected $table = 'category_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "category_translation.deleted_at";


}