<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\ExtraUnlockable;
use App\DomainModelLayer\Journeys\Unlockable;
use App\DomainModelLayer\Journeys\JourneyUnlockable;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use App\DomainModelLayer\Accounts\PlanUnlockable;

class UnlockableMap extends EntityMap {

    protected $table = 'unlockable';
    public $timestamps = true;

    public function journey_unlockable(Unlockable $unlockable)
    {
        return $this->hasOne($unlockable, JourneyUnlockable::class , 'unlockable_id', 'id');
    }

    public function activity_unlockable(Unlockable $unlockable)
    {
        return $this->hasOne($unlockable, ActivityUnlockable::class , 'unlockable_id', 'id');
    }

    public function unlockable_accounts(Unlockable $unlockable)
    {
        return $this->hasMany($unlockable, AccountUnlockable::class , 'unlockable_id', 'id');
    }

    public function unlockable_plans(Unlockable $unlockable)
    {
        return $this->hasMany($unlockable, PlanUnlockable::class , 'unlockable_id', 'id');
    }

    public function extra_unlockable(Unlockable $unlockable)
    {
        return $this->hasOne($unlockable, ExtraUnlockable::class , 'unlockable_id', 'id');
    }

    public function unlockable(Unlockable $unlockable)
    {
        return $this->belongsTo($unlockable, Unlockable::class , 'unlockable_id', 'id');
    }

}