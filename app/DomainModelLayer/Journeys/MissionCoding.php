<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:07 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;


class MissionCoding extends Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getOrder()
    {
        return $this->task->getOrder();
    }

    public function getPositionX()
    {
        return $this->task->getPositionX();
    }

    public function getPositionY()
    {
        return $this->task->getPositionY();
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getTranslations(){
        return $this->translations;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getDuration(){
        return $this->duration;
    }

    public function getVplId(){
        return $this->vpl_id;
    }

    public function getProgrammingLanguage(){
        return $this->programmingLanguage->name;
    }

    public function getCode(){
        return $this->code;
    }

    public function getScreenshots()
    {
        return $this->missionScreens;
    }

    public function getModelAnswer(){
        return $this->model_answer;
    }
}