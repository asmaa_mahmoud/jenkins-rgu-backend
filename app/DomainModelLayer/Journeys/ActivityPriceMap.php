<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityPriceMap extends EntityMap
{
    protected $table = 'activity_price';
    public $timestamps = false;

    public function activity(ActivityPrice $activityPrice){
        return $this->belongsTo($activityPrice, DefaultActivity::class, 'activity_id', 'id');
    }

    public function period(ActivityPrice $activityPrice){
        return $this->belongsTo($activityPrice, ActivityPeriod::class, 'period_id', 'id');
    }

}