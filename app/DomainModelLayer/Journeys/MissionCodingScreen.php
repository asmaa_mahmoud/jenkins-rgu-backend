<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 14/11/2018
 * Time: 3:57 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\Entity;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;

class MissionCodingScreen extends Entity
{
    public function __construct(MissionScreensDto $missionScreensDto = null, MissionCoding $mission)
    {
        if($missionScreensDto != null && $mission != null)
        {
            $this->screenshot_url = $missionScreensDto->url;
            $this->mission = $mission;
        }
    }

    public function getUrl()
    {
        return $this->screenshot_url;
    }
}