<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ConfidenceTranslation extends Entity
{


    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getName()
    {
        return $this->name;
    }

}
