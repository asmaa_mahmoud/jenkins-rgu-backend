<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;

class QuizTypeMap extends EntityMap {

    protected $table = 'quiz_type';

}