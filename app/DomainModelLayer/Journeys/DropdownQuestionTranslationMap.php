<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\DropdownQuestion;

class DropdownQuestionTranslationMap extends EntityMap {

    protected $table = 'dropdown_question_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dropdown_question_translation.deleted_at";



 
}