<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\AdventureTranslation;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;

class AdventureMap extends EntityMap {

    protected $table = 'default_adventure';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "default_adventure.deleted_at";


    public function journeys(Adventure $adventure)
    {
        return $this->belongsToMany($adventure, Journey::class , 'journey_adventure','adventure_id', 'journey_id');
    }

    public function tasks(Adventure $adventure)
    {
        return $this->belongsToMany($adventure, Task::class , 'adventure_task','adventure_id', 'task_id');
    }

    public function translations(Adventure $adventure)
    {
        return $this->hasMany($adventure, AdventureTranslation::class , 'default_adventure_id', 'id');
    }

    public function courseDeadlines(Adventure $adventure)
    {
        return $this->hasMany($adventure, CourseAdventureDeadlines::class , 'adventure_id', 'id');
    }

}