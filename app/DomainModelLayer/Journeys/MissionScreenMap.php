<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Mission;

class MissionScreenMap extends EntityMap {

    protected $table = 'mission_screenshot';
	public $timestamps = true;
	public $softDeletes = true;
    protected $deletedAtColumn = "mission_screenshot.deleted_at";


    public function mission(MissionScreen $missionScreen)
    {
        return $this->belongsTo($missionScreen, Mission::class , 'mission_id', 'id');
    }

}