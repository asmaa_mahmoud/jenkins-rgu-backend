<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class TaskOrder extends Entity
{

    public function getOrder()
    {
        return $this->order;
    }

    public function getPositionX()
    {
    	return $this->position_x;
    }

    public function getPositionY()
    {
    	return $this->position_y;
    }

    public function getTask()
    {
    	return $this->task;
    }

    public function getAdventure(){
        return $this->adventure;
    }

    public function getJourney(){
        return $this->journey;
    }

}
