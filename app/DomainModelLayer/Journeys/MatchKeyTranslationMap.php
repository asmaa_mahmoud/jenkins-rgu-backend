<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MatchKeyTranslationMap extends EntityMap {

    protected $table = 'match_key_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "match_key_translation.deleted_at";



    public function matchKey(MatchKeyTranslation $matchKeyTranslation)
    {
        return $this->belongsTo($matchKeyTranslation,MatchKey::class, 'match_key_id','id');
    }
 
}