<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\ConfidenceTranslation;

class ConfidenceMap extends EntityMap {

    protected $table = 'confidence';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "confidence.deleted_at";


    public function translations(Confidence $confidence)
    {
        return $this->hasMany($confidence, ConfidenceTranslation::class , 'confidence_id', 'id');
    }

}