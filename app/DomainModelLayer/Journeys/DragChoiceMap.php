<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Question;

class DragChoiceMap extends EntityMap {

    protected $table = 'drag_choice';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "drag_choice.deleted_at";


    public function dragQuestion(DragChoice $dragChoice)
    {
        return $this->belongsTo($dragChoice, DragQuestion::class , 'drag_question_id', 'id');
    }

    public function dragCell(DragChoice $dragChoice)
    {
        return $this->hasOne($dragChoice, DragCell::class ,'drag_choice_id','id');
    }

    public function translations(DragChoice $dragChoice)
    {
        return $this->hasMany($dragChoice, DragChoiceTranslation::class , 'drag_choice_id', 'id');
    }

}