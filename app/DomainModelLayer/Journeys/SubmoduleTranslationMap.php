<?php

namespace App\DomainModelLayer\Professional;

use Analogue\ORM\EntityMap;

class SubmoduleTranslationMap extends EntityMap
{
    protected $table = 'sub_module_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "sub_module_translation.deleted_at";

    public function submodule(SubmoduleTranslation $submoduleTranslation){
        return $this->belongsTo($submoduleTranslation, Submodule::class , 'sub_module_id', 'id');
    }

   

}