<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\QuizTutorials;
use App\DomainModelLayer\Journeys\QuizType;
use App\ApplicationLayer\Journeys\QuestionTask;

class QuizMap extends EntityMap {

    protected $table = 'quiz';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "quiz.deleted_at";



    public function task(Quiz $quiz)
    {
        return $this->belongsTo($quiz, Task::class , 'task_id', 'id');
    }

    public function type(Quiz $quiz)
    {
        return $this->belongsTo($quiz, QuizType::class , 'quiz_type_id', 'id');
    }

    public function questions(Quiz $quiz)
    {
        return $this->hasMany($quiz, Question::class , 'quiz_id', 'id');
    }

    public function quizTutorials(Quiz $quiz)
    {
    	return $this->hasMany($quiz, QuizTutorials::class , 'quiz_id', 'id');
    }

    public function translations(Quiz $quiz)
    {
        return $this->hasMany($quiz, QuizTranslation::class , 'Quiz_Id', 'id');
    }

    public function questionTasks(Quiz $quiz)
    {
        return $this->hasMany($quiz, QuestionTask::class , 'quiz_id', 'id');
    }
}