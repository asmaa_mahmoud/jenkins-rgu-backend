<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class QuizTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'title'
        ];
        return $searchable;

    }
    
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

}
