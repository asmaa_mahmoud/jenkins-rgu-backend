<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\FreeJourney;

class FreeJourneyTaskMap extends EntityMap {

    protected $table = 'free_journey_task';


    public function task(FreeJourneyTask $FreeJourneyTask)
    {
        return $this->belongsTo($FreeJourneyTask, Task::class , 'task_id', 'id');
    }

    public function journey(FreeJourneyTask $FreeJourneyTask)
    {
        return $this->belongsTo($FreeJourneyTask, FreeJourney::class , 'free_journey_id', 'id');
    }

}