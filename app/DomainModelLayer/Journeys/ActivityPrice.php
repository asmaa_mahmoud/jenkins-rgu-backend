<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ActivityPrice extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function getPeriod(){
        return $this->period;
    }

    public function getPrice(){
        return $this->price;
    }

    public function getDiscount(){
        return $this->discount;
    }

}