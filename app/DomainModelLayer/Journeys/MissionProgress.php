<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Customers\User;

class MissionProgress extends Entity
{
	public function __construct(Mission $mission, User $user,$score)
    {
        $this->mission = $mission;
        $this->user = $user;
        $this->MissionScore = $score;
    }

    public function getScore()
    {
    	return $this->MissionScore;
    }
}
