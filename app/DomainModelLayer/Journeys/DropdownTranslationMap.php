<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class DropdownTranslationMap extends EntityMap {

    protected $table = 'dropdown_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dropdown_translation.deleted_at";

}