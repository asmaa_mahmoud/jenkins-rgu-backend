<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class McqQuestion extends Entity
{

    public function getid()
    {
        return $this->id;
    }

    public function getSelectMany()
    {
        return $this->selectMany;
    }


    public function getTranslations()
    {
        return $this->translations;
    }
    public function getQuestion()
    {
        return $this->question;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getBody()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getBody();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getBody();
        }
        return $english_translation;
    }

    public function getMcqQuestionChoices()
    {
        return $this->choices;
    }

     public function getChoices()
    {
        return $this->choices;
    }
    public function getIsEditor(){
        return $this->is_editor;
    }

    public function getInitialImage(){
        return $this->initial_image;
    }

    public function getMainImage(){
        return $this->main_image;
    }
    public function getEditorCode(){
        return $this->editor_code;
    }
    public function getEmulatorType(){
        //return ($this->emulator_type->getName());//doesn't work as emulator_type is null
        return !is_null($this->emulatorType) ? $this->emulatorType->name : null;
        // return $this->emulatorType->name;
    }

}
