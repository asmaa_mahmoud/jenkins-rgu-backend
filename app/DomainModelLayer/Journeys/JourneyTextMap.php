<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;


class JourneyTextMap extends EntityMap {

    protected $table = 'journey_text';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "journey_text.deleted_at";


    public function journey(JourneyText $journeyText)
    {
        return $this->belongsTo($journeyText, Journey::class , 'journey_id', 'id');
    }

}