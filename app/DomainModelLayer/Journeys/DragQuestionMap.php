<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\DropdownQuestion;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\MissionHtmlStepsTranslation;
use App\DomainModelLayer\Journeys\Question;

class DragQuestionMap extends EntityMap {

    protected $table = 'drag_question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "drag_question.deleted_at";



    public function question(DragQuestion $dragQuestion){

        return $this->belongsTo($dragQuestion, Question::class , 'question_id', 'id');
    }

    public function dragCells(DragQuestion $dragQuestion){

        return $this->hasMany($dragQuestion, DragCell::class , 'drag_question_id', 'id')->orderBy('order');
    }

    public function dragChoices(DragQuestion $dragQuestion){

        return $this->hasMany($dragQuestion, DragChoice::class , 'drag_question_id', 'id')->orderBy('order');
    }
   

    
 
}