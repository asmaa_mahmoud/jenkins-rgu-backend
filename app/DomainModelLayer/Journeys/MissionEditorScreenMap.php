<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 15/11/2018
 * Time: 7:24 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\EntityMap;

class MissionEditorScreenMap extends EntityMap
{
    protected $table = 'mission_editor_screenshot';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_editor_screenshot.deleted_at";


    public function mission(MissionEditorScreen $missionScreen)
    {
        return $this->belongsTo($missionScreen, MissionEditor::class , 'mission_id', 'id');
    }
}