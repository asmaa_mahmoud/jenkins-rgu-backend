<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;
use LaravelLocalization;


class Choice extends Entity
{

    public function __construct(Choice $choiceDto = null)
    {
        if($choiceDto != null )
        {
            $this->id = $choiceDto->id;
            $this->Text= $choiceDto->text;
            $this->Question_Id = $choiceDto->question_id;
            $this->IsModelAnswer = $choiceDto->is_answer;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getText()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getBody();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getBody();
        }
        return $english_translation;
    }

    public function getExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getExplanation();
        }
        return $english_translation;
    }

    public function getWrongExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getWrongExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getWrongExplanation();
        }
        return $english_translation;
    }
    

    public function getQuestion_id(){
        return $this->question->getId();
    }

    public function getIs_answer()
    {
        return $this->is_model_answer;
    }
    public function getImage(){
        return $this->image;
    }

}
