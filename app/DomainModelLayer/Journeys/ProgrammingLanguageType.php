<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;


class ProgrammingLanguageType extends Entity
{
    public function getName(){
        return $this->name;
    }
}