<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 24/10/2018
 * Time: 9:55 AM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;


class Resource extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getPath(){
        return $this->path;
    }

    public function getIsUrl(){
        return $this->is_url;
    }

    public function getTasks(){
        return $this->tasks;
    }

    public function getTaskResources(){
        return $this->taskResources;
    }
}