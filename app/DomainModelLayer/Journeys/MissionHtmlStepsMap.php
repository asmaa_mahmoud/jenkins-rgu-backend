<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\MissionHtmlSteps;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\MissionHtmlStepsTranslation;
use App\ApplicationLayer\Journeys\QuestionTask;

class MissionHtmlStepsMap extends EntityMap {

    protected $table = 'mission_html_steps';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_html_steps.deleted_at";

    public function missionHtml(MissionHtmlSteps $missionHtmlSteps)
    {
        return $this->belongsTo($missionHtmlSteps, MissionHtml::class , 'mission_html_id', 'id');
    }

    public function translations(MissionHtmlSteps $missionHtmlSteps)
    {
        return $this->hasMany($missionHtmlSteps, MissionHtmlStepsTranslation::class , 'mission_html_steps_id', 'id');
    }

    public function question_task(MissionHtmlSteps $missionHtmlSteps){

        return $this->hasOne($missionHtmlSteps, QuestionTask::class, 'html_step_id', 'id');
    }

    // public function question(MissionHtmlSteps $missionHtmlSteps){
    //     return $this->belongsTo($missionHtmlSteps, Question::class , 'question_task', 'html_step_id', 'question_id');
    // }

  
}