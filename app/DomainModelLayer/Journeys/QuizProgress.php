<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;


class QuizProgress extends Entity
{

    public function __construct(QuizProgress $quizProgressDto = null)
    {
        if($quizProgressDto != null )
        {
            $this->id = $quizProgressDto->id;
            $this->QuizScore= $quizProgressDto->quiz_score;
            $this->NoOfTrials = $quizProgressDto->no_of_trails;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getQuiz_score()
    {
        return $this->QuizScore;
    }

    public function getNo_of_trails(){
        return $this->NoOfTrials;
    }

    public function setQuiz_score($quiz_score)
    {
        $this->QuizScore = $quiz_score;
    }

    public function setNo_of_trails($no_trails){
        $this->NoOfTrials = $no_trails;
    }

    public function setuser($user_id){
        $this->User_Id = $user_id;
    }
    public function setquiz($quiz_id){
        $this->Quiz_Id = $quiz_id;
    }
}
