<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MatchValue extends Entity
{

    public function getId()
    {
        return $this->id;
    }


    public function getOrder()
    {
        return $this->order;
    }


    public function getTranslations()
    {
        return $this->translations;
    }


    public function getText()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getText();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getText();
        }
        return $english_translation;
    }


    public function getExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getExplanation();
        }
        return $english_translation;
    }

 

}
