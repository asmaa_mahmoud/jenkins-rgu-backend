<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Question;

class DragCellMap extends EntityMap {

    protected $table = 'drag_cell';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "drag_cell.deleted_at";


    public function dragQuestion(DragCell $dragCell)
    {
        return $this->belongsTo($dragCell, DragQuestion::class , 'drag_question_id', 'id');
    }

    public function dragChoice(DragCell $dragCell)
    {
        return $this->hasOne($dragCell, DragChoice::class , 'id', 'drag_choice_id');
    }

}