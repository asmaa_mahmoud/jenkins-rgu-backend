<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class TagMap extends EntityMap
{
    protected $table = 'tags';
    public $timestamps = true;

    public function translations(Tag $tag){
        return $this->hasMany($tag, TagTranslation::class , 'tag_id', 'id');
    }

}