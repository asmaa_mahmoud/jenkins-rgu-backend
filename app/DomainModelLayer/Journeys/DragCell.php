<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;
use LaravelLocalization;


class DragCell extends Entity
{

    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    
    public function getQuestionId(){
        return $this->dragQuestion->getId();
    }
    public function getDragChoice(){
        return $this->dragChoice;
    }
    public function getShowAnswer(){
        return $this->show_answer;
    }
    public function getOrder(){
        return $this->order;
    }
    public function getCorrectChoiceId(){
        return $this->dragChoice->id;
    }
    public function getText(){
        return $this->dragChoice->getText();
    }

   

}
