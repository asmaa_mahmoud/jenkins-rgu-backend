<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MiniProjectTypesMap extends EntityMap {

    protected $table = 'miniproject_types';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "miniproject_types.deleted_at";



}