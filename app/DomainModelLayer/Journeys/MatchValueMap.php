<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\MatchValueTranslation;
use App\DomainModelLayer\Journeys\MatchKey;

class MatchValueMap extends EntityMap {

    protected $table = 'match_value';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "match_value.deleted_at";



    public function translations(MatchValue $matchValue)
    {
        return $this->hasMany($matchValue, MatchValueTranslation::class, 'match_value_id','id');
    }

    public function matchKey(MatchValue $matchValue)
    {
        return $this->hasOne($matchValue, MatchKey::class, 'match_value_id','id');
    }
 
}