<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class QuestionTranslationMap extends EntityMap {

    protected $table = 'question_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "question_translation.deleted_at";


}