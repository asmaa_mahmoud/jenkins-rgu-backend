<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DropdownQuestionTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'title_description'
        ];
        return $searchable;

    }
    

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitleDescription()
    {
        return $this->title_description;
    }

}
