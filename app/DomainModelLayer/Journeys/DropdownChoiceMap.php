<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\DropdownChoice;
use App\DomainModelLayer\Journeys\DropdownChoiceTranslation;

class DropdownChoiceMap extends EntityMap {

    protected $table = 'dropdown_choice';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dropdown_choice.deleted_at";



    public function dropdown(DropdownChoice $dropdownChoice)
    {
        return $this->belongsTo($dropdownChoice, dropdown::class, 'dropdown_id', 'id');
    }


    public function dropdown_choice_translation(DropdownChoice $dropdownChoice)
    {
        return $this->hasMany($dropdownChoice,DropdownChoiceTranslation::class, 'dropdown_choice_id', 'id');
    }


 
}