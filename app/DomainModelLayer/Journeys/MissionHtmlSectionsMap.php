<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\MissionHtmlSections;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\MissionHtmlSectionsTranslationMap;
use App\DomainModelLayer\Journeys\HtmlSectionSteps;

class MissionHtmlSectionsMap extends EntityMap {

    protected $table = 'html_section';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "html_section.deleted_at";

    public function missionHtml(MissionHtmlSections $missionHtmlSections)
    {
        return $this->belongsTo($missionHtmlSections, MissionHtml::class , 'mission_html_id', 'id');
    }

    public function translations(MissionHtmlSections $missionHtmlSections)
    {
        return $this->hasMany($missionHtmlSections, MissionHtmlSectionsTranslation::class , 'html_section_id', 'id');
    }

    public function steps(MissionHtmlSections $missionHtmlSections){
        return $this->belongsToMany($missionHtmlSections, MissionHtmlSteps::class , 'html_section_step', 'html_section_id', 'mission_html_step_id')->orderBy('html_section_step.order');
    }

}