<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class DropdownQuestion extends Entity
{

    public function getQuestion()
    {
        return $this->question;
    }

    public function getDropdown()
    {
        return $this->dropdown;
    }


    public function getDropdownQuestionTranslation()
    {
        return $this->dropdown_question_translation;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->dropdown_question_translation;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitleDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitleDescription();
        }
        return $english_translation;
    }

}
