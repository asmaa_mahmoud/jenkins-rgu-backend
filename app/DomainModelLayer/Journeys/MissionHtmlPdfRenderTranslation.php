<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;


class MissionHtmlPdfRenderTranslation extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getPdfLink()
    {
        return $this->pdf_link;
    }

}