<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;
use LaravelLocalization;


class Confidence extends Entity
{

    public function getValue()
    {
        return $this->value;
    }

    public function getName()
    {
      
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale){
                
                return $translation->name;
            }
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->name;
        }
        return $english_translation;
    }

   
}
