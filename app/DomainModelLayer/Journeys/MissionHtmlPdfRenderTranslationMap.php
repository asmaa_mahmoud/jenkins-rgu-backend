<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class MissionHtmlPdfRenderTranslationMap extends EntityMap {

    protected $table = 'mission_html_pdf_render_translations';

}