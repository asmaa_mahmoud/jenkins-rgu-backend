<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MissionHtmlSectionsTranslation extends Entity
{

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

}
