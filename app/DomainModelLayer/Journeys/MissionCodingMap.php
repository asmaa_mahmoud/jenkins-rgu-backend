<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:12 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionCodingMap extends EntityMap
{
    protected $table = 'mission_coding';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_coding.deleted_at";

    public function task(MissionCoding $missionCoding)
    {
        return $this->belongsTo($missionCoding, Task::class , 'task_id', 'id');
    }

    public function translations(MissionCoding $missionCoding)
    {
        return $this->hasMany($missionCoding, MissionCodingTranslation::class , 'mission_coding_id', 'id');
    }

    public function programmingLanguage(MissionCoding $missionCoding){
        return $this->belongsTo($missionCoding, ProgrammingLanguageType::class, 'programming_language_type_id', 'id');
    }

    public function missionScreens(MissionCoding $missionCoding)
    {
        return $this->hasMany($missionCoding, MissionCodingScreen::class , 'mission_id', 'id');
    }
}