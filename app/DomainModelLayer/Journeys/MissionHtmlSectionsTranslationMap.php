<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionHtmlSectionsTranslationMap extends EntityMap {

    protected $table = 'html_section_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "html_section_translation.deleted_at";
}