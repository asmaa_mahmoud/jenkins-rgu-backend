<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\AccountDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\AccountType;
use App\DomainModelLayer\Accounts\Subscription;
use Analogue\ORM\EntityCollection;

class JourneyText extends Entity
{

    
    /**
     * @return mixed
     */
    public function getJourneyId()
    {
        return $this->journey_id;
    }

    /**
     * @return mixed
     */
    public function getLanguageCode()
    {
        $this->language_code;
    }
    
    /**
     * @return mixed
     */
    public function getText()
    {
        $this->text;
    }

    /**
     * @param mixed $Grade
     */
    public function getOrder()
    {
        $this->order;
    }
    
}
