<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class Unlockable extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getXpToUnlock()
    {
        return $this->xp_to_unlock;
    }

    public function setXpToUnlock($xpToUnlock)
    {
        $this->xp_to_unlock = $xpToUnlock;
    }

    public function getCoinsToUnlock()
    {
        return $this->coins_to_unlock;
    }

    public function setCoinsToUnlock($coinsToUnlock)
    {
        $this->coins_to_unlock = $coinsToUnlock;
    }

    public function getCostToUnlock()
    {
        return $this->cost_to_unlock;
    }

    public function setCostToUnlock($costToUnlock)
    {
        $this->cost_toUnlock = $costToUnlock;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getIsPlan()
    {
        return $this->is_plan;
    }

    public function setIsPlan($isPlan)
    {
        $this->is_plan = $isPlan;
    }

    public function getJourneyUnlockable(){
        return $this->journey_unlockable;
    }

    public function setJourneyUnlockable(JourneyUnlockable $journeyUnlockable){
        $this->journey_unlockable = $journeyUnlockable;
    }

    public function getActivityUnlockable(){
        return $this->activity_unlockable;
    }

    public function getUnlockableAccounts(){
        return $this->unlockable_accounts;
    }

    public function getUnlockablePlans(){
        return $this->unlockable_plans;
    }

    public function getExtraUnlockable(){
        return $this->extra_unlockable;
    }

    public function isMobile(){
        return ($this->is_mobile == true ? 1 : 0);
    }


}