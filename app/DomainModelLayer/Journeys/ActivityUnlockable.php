<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class ActivityUnlockable extends Entity
{
    public function __construct(Activity $activity, Unlockable $unlockable)
    {
        $this->activity = $activity;
        $this->unlockable = $unlockable;
    }

    public function getUnlockable(){
        return $this->unlockable;
    }

    public function setUnlockable(Unlockable $unlockable){
        $this->unlockable = $unlockable;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

    public function setActivity(Activity $activity){
        $this->activity = $activity;
    }
}
