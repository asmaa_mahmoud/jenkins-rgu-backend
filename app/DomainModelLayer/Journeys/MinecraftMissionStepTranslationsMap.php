<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class MinecraftMissionStepTranslationsMap extends EntityMap
{
    protected $table = 'minecraft_mission_step_translations';
    public $timestamps = true;
    public $softDeletes = false;

}