<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityTagMap extends EntityMap
{
    protected $table = 'activity_tag';
    public $timestamps = true;

    public function activity(ActivityTag $activityTag){
        return $this->belongsTo($activityTag, Activity::class, 'activity_id', 'id');
    }

    public function defaultActivity(ActivityTag $activityTag){
        return $this->belongsTo($activityTag, DefaultActivity::class, 'default_activity_id', 'id');
    }

    public function tag(ActivityTag $activityTag){
        return $this->belongsTo($activityTag, Tag::class, 'tag_id', 'id');
    }

}