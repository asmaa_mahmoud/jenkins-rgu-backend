<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\MissionScreen;
use App\DomainModelLayer\Journeys\MissionTranslation;
use App\DomainModelLayer\Journeys\MiniProjectTypes;
use App\DomainModelLayer\Journeys\Blockly;
use App\DomainModelLayer\Journeys\ProgrammingLanguageType;

class MissionHtmlMap extends EntityMap {

    protected $table = 'mission_html';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_html.deleted_at";


    public function task(MissionHtml $missionHtml)
    {
        return $this->belongsTo($missionHtml, Task::class , 'task_id', 'id');
    }

    public function translations(MissionHtml $missionHtml)
    {
        return $this->hasMany($missionHtml, MissionHtmlTranslation::class , 'mission_html_id', 'id');
    }

    public function pdfTranslations(MissionHtml $missionHtml)
    {
        return $this->hasMany($missionHtml, MissionHtmlPdfRenderTranslation::class , 'mission_html_id', 'id');
    }

    public function steps(MissionHtml $missionHtml)
    {
        return $this->hasMany($missionHtml, MissionHtmlSteps::class , 'mission_html_id', 'id');
    }

    public function sections(MissionHtml $missionHtml)
    {
        return $this->hasMany($missionHtml, MissionHtmlSections::class , 'mission_html_id', 'id')->orderBy('order');
    }

    public function practiceTask(MissionHtml $missionHtml)
    {
        return $this->belongsTo($missionHtml, Task::class , 'practice_task_id', 'id');
    }

    public function miniProjectType(MissionHtml $missionHtml)
    {
        return $this->belongsTo($missionHtml, MiniProjectTypes::class , 'miniproject_type_id', 'id');
    }

    public function htmlMissionType(MissionHtml $missionHtml)
    {
        return $this->belongsTo($missionHtml, MissionHtmlType::class , 'mission_html_type_id', 'id');
    }

    public function quiz(MissionHtml $missionHtml)
    {
        return $this->belongsTo($missionHtml, Quiz::class , 'quiz_id', 'id');
    }
}