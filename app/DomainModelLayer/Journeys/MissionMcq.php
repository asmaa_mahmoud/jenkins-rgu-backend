<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MissionMcq extends Entity
{
    public function getId(){
        return $this->id;
    }
    public function getQuestions(){
        return $this->questions;
    }
}
