<?php

/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 12:39 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Journeys\Dtos\BlocklyDto;
use App\ApplicationLayer\Journeys\Dtos\BlocklyBlockDto;

class Blocks extends Entity
{
    public function __construct(BlocklyBlockDto $blockDto = null)
    {
        if($blockDto != null )
        {
            $this->name = $blockDto->Name;
        }
    }

    public function getType(){
        return $this->name;
    }
}