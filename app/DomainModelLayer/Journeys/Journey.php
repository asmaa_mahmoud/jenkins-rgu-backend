<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\Subscription;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;

class Journey extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getSummary()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getSummary();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getSummary();
        }
        return $english_translation;
    }

    public function getText(){
        $textsArray = [];
        $locale = LaravelLocalization::getCurrentLocale();
        $texts = $this->journeyText;
        foreach ($texts as $text) {
            if($text->getLanguageCode() == $locale && $text->getText() != null)
                $textsArray[] =  $text->getText();
        }
        return $textsArray;
    }

    public function getCardIconURL(){
        return $this->card_icon_url;
    }

    public function getCategories()
    {
        return $this->journeycategory;
    }

    public function getCreationDate()
    {
        return $this->created_at;
    }

    public function getDefaultJourneys()
    {
        return $this->defaultJourneys;
    }

    public function getWhatYouWillLearn()
    {
        $what_learn = [];
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->datatranslations;
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale && $translation->getWhatYouWillLearn() != null)
                $what_learn[] =  $translation->getWhatYouWillLearn();
        }
        return $what_learn;
    }

    public function getConcept()
    {
        $concepts = [];
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->datatranslations;
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale && $translation->getConcept() != null)
                $concepts[] =  $translation->getConcept();
        }
        return $concepts;
    }

    public function getVideoUrl()
    {
        $videos = [];
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->datatranslations;
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale && $translation->getVideoUrl() != null)
                $videos[] =  $translation->getVideoUrl();
        }
        return $videos;
    }

    public function getScene()
    {
        $scene = $this->scene;
        if($scene != null)
            return $scene->getName();
    }

    public function getStatus()
    {
        $status = $this->status;
        if($status != null)
            return $status->getName();
    }

    public function getAdventures()
    {
        return $this->adventures;
    }

    public function getTasksCount()
    {
        $adventures = $this->getAdventures();
        $tasks_count = 0;
        foreach ($adventures as $adventure) {
            $tasks_count += count($adventure->getTasks());
        }
        return $tasks_count;
    }

    public function getTasks()
    {
        $adventures = $this->getAdventures();
        $tasks = new EntityCollection;
        foreach ($adventures as $adventure) {
            $tasks = $tasks->merge($adventure->getTasks());
        }
        return $tasks;
    }

    public function getTasksIds()
    {
        $adventures = $this->getAdventures();
        $tasks = [];
        foreach ($adventures as $adventure) {
            foreach ($adventure->getTasks() as $task){
                $tasks[] = $task->id;
            }
        }
        return $tasks;
    }

    public function getData(){
        return $this->data->first();
    }

    public function getTutorialsIds(){
        $result = [];
        $tasks = $this->getTasks();
        foreach ($tasks as $task){
            if($task->getTaskType() == 'tutorial'){
                $result[] = $task->id;
            }
        }
        return $result;
    }

    public function getQuizzesIds(){
        $result = [];
        $tasks = $this->getTasks();
        foreach ($tasks as $task){
            if($task->getTaskType() == 'quiz'){
                $result[] = $task->id;
            }
        }
        return $result;
    }

    public function getGradeNumber(){
        return $this->getData()->getGrade();
    }

    public function getDefaultUnlockable(){
        return $this->defaultUnlockable;
    }
}