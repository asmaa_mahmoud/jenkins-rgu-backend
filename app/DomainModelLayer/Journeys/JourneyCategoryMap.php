<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\JourneyCategoryTranslation;
use App\DomainModelLayer\Journeys\JourneyCategory;
use App\DomainModelLayer\Accounts\PlanHistory;

class JourneyCategoryMap extends EntityMap {

    protected $table = 'category';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "category.deleted_at";


    public function journeys(JourneyCategory $journeyCategory)
    {
        return $this->belongsToMany($journeyCategory, Journey::class , 'journey_category','category_id', 'journey_id');
    }

    public function translations(JourneyCategory $journeyCategory)
    {
        return $this->hasMany($journeyCategory, JourneyCategoryTranslation::class , 'category_id', 'id');
    }

    public function plans(JourneyCategory $journeyCategory)
    {
        return $this->belongsToMany($journeyCategory,PlanHistory::class,'plan_category','category_id','plan_history_id');
    }

}