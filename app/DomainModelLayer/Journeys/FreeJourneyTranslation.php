<?php

namespace App\DomainModelLayer\Journeys;
use Analogue\ORM\Entity;

class FreeJourneyTranslation extends Entity
{
    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }


}