<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:34 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionAngularDependencyMap extends EntityMap
{
    protected $table = 'mission_angular_dependency';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_angular_dependency.deleted_at";
}