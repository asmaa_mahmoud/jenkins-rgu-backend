<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:34 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionEditorTranslationMap extends EntityMap
{
    protected $table = 'mission_editor_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_editor_translation.deleted_at";
}