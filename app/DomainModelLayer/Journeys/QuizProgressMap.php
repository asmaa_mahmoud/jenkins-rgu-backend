<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Customers\User;
use App\DomainModelLayer\Quizzes\Quiz;

class QuizProgressMap extends EntityMap {

    protected $table = 'quizprogress';
    public $timestamps = true;

    public function user(QuizProgress $quizProgress)
    {
        return $this->belongsTo($quizProgress, User::class , 'User_Id', 'id');
    }

    public function quiz(QuizProgress $quizProgress)
    {
        return $this->belongsTo($quizProgress, Quiz::class , 'Quiz_Id', 'id');
    }

}