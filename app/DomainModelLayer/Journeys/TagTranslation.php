<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class TagTranslation extends Entity
{
    public function getId(){
    return $this->id;
}

    public function getName(){
        return $this->name;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function getTag(){
        return $this->tag;
    }

}