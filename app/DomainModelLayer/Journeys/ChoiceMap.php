<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Question;

class ChoiceMap extends EntityMap {

    protected $table = 'choice';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "choice.deleted_at";


    public function question(Choice $choice)
    {
        return $this->belongsTo($choice, Question::class , 'question_id', 'id');
    }

    public function translations(Choice $choice)
    {
        return $this->hasMany($choice, ChoiceTranslation::class , 'choice_id', 'id');
    }

}