<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\AccountType;

class ActivityPeriodMap extends EntityMap
{
    protected $table = 'activity_period';
    public $timestamps = false;

    public function translations(ActivityPeriod $activityPeriod){
        return $this->hasMany($activityPeriod, ActivityPeriodTranslation::class , 'period_id', 'id');
    }

    public function accountType(ActivityPeriod $activityPeriod)
    {
        return $this->belongsTo($activityPeriod, AccountType::class, 'account_type_id', 'id');
    }

}