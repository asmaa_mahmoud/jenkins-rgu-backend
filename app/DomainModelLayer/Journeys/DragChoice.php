<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;
use LaravelLocalization;


class DragChoice extends Entity
{

    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    
    public function getQuestionId(){
        return $this->dragQuestion->getId();
    }

    public function getText()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";

        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getBody();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getBody();
        }
        return $english_translation;
    }
    public function getExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";

        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getExplanation();
        }
        return $english_translation;
    }
    public function getOrder(){
        return $this->order;
    }

    public function getCorrectCellOrder(){
        return $this->dragCell->getOrder();
    }

}
