<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MissionHtmlStepsTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'step_editor',
            'title'
        ];
        return $searchable;

    }
    

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getRightExplanation()
    {
        return $this->right_explanation;
    }

    public function getWrongExplanation()
    {
        return $this->wrong_explanation;
    }

    public function getContent()
    {
        return $this->step_editor;
    }

}
