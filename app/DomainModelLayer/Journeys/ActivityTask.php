<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Journeys\Task;

class ActivityTask extends Entity
{
    public function __construct(Activity $activity, DefaultActivity $defaultActivity, Task $task, $order){
        $this->activity = $activity;
        $this->defaultActivity = $defaultActivity;
        $this->task = $task;
        $this->order = $order;
    } 
    
    public function getId(){
        return $this->id;
    }

    public function getActivity(){
        return $this->activity;
    }

    public function getDefaultActivity(){
        return $this->defaultActivity;
    }

    public function getTask(){
        return $this->task;
    }

    public function getOrder(){
        return $this->order;
    }

    public function setOrder($order){
        $this->order = $order;
    }
    public function getDifficulty(){
        return $this->difficulty;
    }

    public function getXps(){
        return $this->xps;
    }

    public function getTimeEstimate(){
        return $this->time_estimate;
    }


}