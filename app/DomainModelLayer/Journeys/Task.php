<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class Task extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function addProgress(TaskProgress $task_progress)
    {
        $this->progress->push($task_progress);
    }

    public function removeProgress(TaskProgress $task_progress)
    {
        $this->progress->remove($task_progress);
    }

    public function getOrder()
    {
        //return $this->order;
        $task_order = $this->taskOrders->first();
        if($task_order != null)
            return $task_order->getOrder();
        else if($this->freetaskOrders->first() != null)
            return $this->freetaskOrders->first()->getOrder();
    }

    public function getPositionX()
    {
        return $this->position_x;
    }

    public function getPositionY()
    {
        return $this->position_y;
    }

    public function getMissions()
    {
        return $this->missions;
    }

    public function getMissionCoding()
    {
        return $this->missionCoding;
    }

    public function getMissionEditor()
    {
        return $this->missionEditor;
    }

    public function getMissionAngular()
    {
        return $this->missionAngular;
    }

    public function getQuizzes()
    {
        return $this->quizzes;
    }

    public function getMissionsHtml(){
        return $this->missonsHtml;
    }
    public function getMissionsCoding(){
        return $this->missonsCoding;
    }
    public function getMissionsEditor(){
        return $this->missonsEditor;
    }
    public function getProject(){
        return $this->projects;
    }
    public function getTaskOrder(){
        return $this->taskOrders->first();
    }

    public function getActivityTask(){
        return $this->activityTasks->first();
    }

    public function getActivityTasks(){
        return $this->activityTasks;
    }

    public function getTaskType(){
        if($this->getMissions()->first() != null)
            $type = 'mission';
        else if($this->getMissionsHtml()->first() != null)
            $type = 'mission';
        else
        {
            $type = $this->getQuizzes()->first()->getType()->getName();
        }


        return $type;
    }

    public function getActivityOrder($activity_id, $is_default = true){
        $activityTasks = $this->getActivityTasks();
        foreach ($activityTasks as $activityTask){
            if($is_default){
                if($activityTask->getDefaultActivity()->getId() == $activity_id){
                    return $activityTask->getOrder();
                }
            }
            else{
                if($activityTask->getActivity()->getId() == $activity_id){
                    return $activityTask->getOrder();
                }
            }
        }
        return 0;
    }

    public function getResources(){
        return $this->resources;
    }

    public function getTaskResources(){
        return $this->taskResources;
    }

    public function getTaskNameAndType(){
        $nameAndType=[];
        if($this->getMissions()->first() != null) {
            $mission=$this->getMissions()->first();
            $nameAndType['mission_id'] = $mission->getId();
            $nameAndType['task_name'] = $this->getMissions()->first()->getName();
            $nameAndType['task_type'] = 'mission';
        }
        elseif($this->getMissionsHtml()->first() != null) {
            $mission = $this->getMissionsHtml()->first();
            $nameAndType['mission_id'] = $mission->getId();
            $nameAndType['task_name'] = $this->getMissionsHtml()->first()->getTitle();
            $nameAndType['task_type'] = $this->getMissionsHtml()->first()->getHtmlMissionType();
        }
        elseif($this->getMissionCoding() != null) {
            $mission = $this->getMissionCoding();
            $nameAndType['mission_id'] = $mission->getId();
            $nameAndType['task_name'] = $this->getMissionCoding()->getTitle();
            $nameAndType['task_type'] = 'coding_mission';
            //;
        }
        elseif($this->getMissionEditor() != null){
            $mission = $this->getMissionEditor();
            $nameAndType['mission_id'] = $mission->getId();
            $nameAndType['task_name'] = $this->getMissionEditor()->getTitle();
            $nameAndType['task_type'] = 'editor_mission';
        }
        elseif($this->getMissionAngular() != null){
            $mission = $this->getMissionAngular();
            $nameAndType['mission_id'] = $mission->getId();
            $nameAndType['task_name'] = $this->getMissionAngular()->getTitle();
            $nameAndType['task_type'] = 'angular_mission';
        }
        elseif($this->getQuizzes()->first()!=null){
            $quiz = $this->getQuizzes()->first();
            $nameAndType['mission_id'] = $quiz->getId();
            $nameAndType['task_id'] = $quiz->getTask()->getId();
            $nameAndType['task_name'] = $this->getQuizzes()->first()->getTitle();
            $nameAndType['task_type'] = $this->getQuizzes()->first()->getType()->getName();
        }
        return $nameAndType;
    }

    public function getUsersCurrentSteps(){
        return $this->usersCurrentSteps;
    }
    public function setUsersCurrentSteps($currentSteps){
        $this->usersCurrentSteps = $currentSteps;
    }

}
