<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class MissionHtmlTypeMap extends EntityMap
{
    protected $table = 'mission_html_type';

}