<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MissionMcqChoose extends Entity
{
    public function isCorrect()
    {
        return $this->is_correct;
    }

    public function getCorrectionMsg()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getCorrectionMessage();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getCorrectionMessage();
        }
        return $english_translation;
    }


    public function getId()
    {
        return $this->id;
    }
}
