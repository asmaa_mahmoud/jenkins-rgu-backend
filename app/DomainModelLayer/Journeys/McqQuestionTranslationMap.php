<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class McqQuestionTranslationMap extends EntityMap {

    protected $table = 'mcq_question_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mcq_question_translation.deleted_at";



 
}