<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\AdventureTranslation;

class MissionMcqChooseMap extends EntityMap {

    protected $table = 'mission_mcq_choices';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_mcq_choices.deleted_at";

    public function translations(MissionMcqChoose $choice)
    {
        return $this->hasMany($choice, MissionMcqChooseTranslation::class , 'choice_id', 'id');
    }
}