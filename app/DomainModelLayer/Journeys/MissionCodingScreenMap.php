<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 14/11/2018
 * Time: 3:59 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\EntityMap;

class MissionCodingScreenMap extends EntityMap
{
    protected $table = 'mission_coding_screenshot';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_coding_screenshot.deleted_at";


    public function mission(MissionCodingScreen $missionScreen)
    {
        return $this->belongsTo($missionScreen, MissionCoding::class , 'mission_id', 'id');
    }
}