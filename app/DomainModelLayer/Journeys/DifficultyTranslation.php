<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DifficultyTranslation extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }
}