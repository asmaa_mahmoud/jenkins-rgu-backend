<?php

namespace App\DomainModelLayer\Journeys\Repositories;

use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\MissionMcq;
use App\DomainModelLayer\Journeys\MissionSavingActivity;
use App\DomainModelLayer\Journeys\MissionText;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\MissionSavings;
use App\DomainModelLayer\Journeys\TaskProgressActivity;
use App\DomainModelLayer\Journeys\Quiz;


interface IJourneyMainRepository
{
    public function GetJourneysCatalogue();
    public function getJourneyById($id);
    public function getAdventureById($id);
    public function getJourneyData(Journey $journey,$user_id);
    public function getTaskProgressByOrder($user_id,$order,$journey_id,$adventure_id);
    public function getMissionbyId($id);
    public function getCodingMissionById($id);
    public function getMissionEditorById($id);
    public function getMissionAngularById($id);
    public function getAngulaFileByIdAndName($id,$name);
    public function getUserSavedCode(Mission $mission,$user_id,$journey_id,$adventure_id);
    public function getUserSavedCodeByName(Mission $mission,$user_id,$name,$journey_id,$adventure_id);
    public function storeMission(Mission $mission);
    public function storeTask(Task $task);
    public function deleteMissionSavings(MissionSavings $missionsaving);
    public function getUserMissionProgrss(Mission $mission,$user_id,$journey_id,$adventure_id);
    public function deleteProgress(TaskProgress $task_progress);
    public function getquiz($id);
    public function getQuestionsforQuiz($quiz_id);
    public function getQuestionById($id);
    public function getQuestionMcqById($id);
    public function getChoiceById($id);
    public function getChoiceModelforQuestion($question_id);
    public function getChoiceModelforMcqQuestion($question_id);
    public function getprogressforquiz($quiz_id, $user_id,$journey_id,$adventure_id);
    public function getAllCategories();
    public function getTaskOrder(Task $task,$journey_id,$adventure_id);
    public function getTaskByOrder($order,$journey_id,$adventure_id);
    public function getAdventuresByOrder($journey_id);
    public function getBlocklyCategory($mission_id);

    public function getAdventureByOrder($order,$journey_id);

    public function getLastTaskInAdventure($journey_id,$adventure_id);
    public function getFirstTaskInAdventure($journey_id,$adventure_id);

    public function getFirstTaskInJourney($journey_id);

    public function getTasksByOrder($journey_id,$adventure_id);

    //
    public function getMissionMcqQuestions(MissionMcq $missionMcq);
    public function getMissionTextQuestions(MissionText $missionText);
    public function getMcqQuestionById($id);
    public function getMcqChoiceById($id);
    public function getMcqCorrectChooseChoice($question_id);
    public function getLastTaskInJourney(Journey $journey);

    public function getHtmlMissionbyId($html_mission_id);
    public function getUserTaskProgress(Task $task,$user_id,$journey_id,$adventure_id);
    public function getAllFreeJourneys();
    public function isFreeTask($task_id);
    public function getFreeJourneyTasks($free_journey_id);
    public function sendCertificateEmail($email,$pdf_file,$level);
    public function uploadCertificate($image_file);
    public function getLastPlayedTask($journey_id, $child_id);
    public function getLastSolvedTask($journey_id, $child_id);
    public function getTasksInActivity($activity_id, $count = null, $is_default = true);

    public function getModelAnswerUnlocked($mission_id,$user_id);
    public function removeTaskProgress(TaskProgress $taskProgress);
    public function storeTaskProgress(TaskProgress $taskProgress);
    public function getJourneyNotInIds($ids);
    public function getUserTaskProgressActivity(Task $task,$user_id,$activity_id,$is_default = true);
    public function getDefaultActivityById($id);
    public function getActivityById($id);
    public function storeTaskProgressActivity(TaskProgressActivity $taskProgressActivity);
    public function deleteTaskProgressActivity(TaskProgressActivity $taskProgressActivity);
    public function getFirstTaskInActivity($activity_id,$is_default = true);
    public function getActivityTaskOrder(Task $task,$activity_id,$is_default = true);
    public function getLastSolvedTaskinActivity($user_id,$activity_id,$is_default= true);
    public function getLastTaskInActivity($activity_id,$is_default = true);
    public function getLastMainTaskInActivity($activity_id,$is_default = true);
    public function getTaskInActivityByOrder($order,$activity_id,$is_default = true);
    public function getAllActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null);
    public function getAllMineCraftActivities($limit = null, $search = null, $count = null, $is_b2b = null, $is_b2c = null);
    public function getAllTasksInActivity($activity_id, $count = null, $is_default = true);
    public function getActivityPeriodsByAccountTypeId($account_type_id,$period = 0);
    public function getActivityPrice($account_type_id,$activity_id,$is_default = true);
    public function getSolvedTasksInActivity($user_id,$activity_id,$is_default= true);
    public function getSolvedActivityProgress($user_id,$activity_id,$is_default= true);
    public function getActivityProgressForTask($user_id,$task_id,$activity_id,$is_default= true);
    public function getLastPlayedProgressActivity($user_id,$activity_id,$is_default = true);
    public function getActivityTasksInOrder($activity_id,$is_default = true);
    public function getStuckProgressActivity($user_id,$activity_id,$is_default = true);
    public function getUserSavedCodeActivity(Mission $mission,$user_id,$activity_id,$is_default=true);
    public function getUserSavedCodeByNameActivity(Mission $mission,$user_id,$name,$activity_id,$is_default = true);
    public function removeMissionSaving(MissionSavingActivity $missionSavingActivity);
    public function getRoboClubFreeJourneys();
    public function getFreeJourneysTasksById($free_journey_id);
    public function getAllJourneys();
    public function getFirstTaskIdInActivity($activity_id);
    public function getDefaultActivityUnlockable($activity_id, $is_mobile = true);
    public function getTaskById($task_id);

    //lesson bricks
    public function checkQuestionInLesson($mission_id,$question_id);
    public function getDragCellById($cell_id);
    public function getDragChoiceById($choice_id);
    public function getDropdownById($dropdown_id);
    public function getDropdownChoiceById($choice_id);
    public function getDropdownCorrectAnswer($dropdown_id);

    public function getQuestionsByQuiz(Quiz $quiz);
    public function getConfidence();
    public function getConfidenceByValue($value);
    public function getChoicesforQuestionById($question_id);
    public function getChoicesforMcqQuestionById($question_id);
}