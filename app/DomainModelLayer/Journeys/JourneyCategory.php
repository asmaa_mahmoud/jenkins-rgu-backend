<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\DomainModelLayer\Accounts\PlanHistory;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;

class JourneyCategory extends Entity
{

    public function __construct(JourneyCategoryDto $journeyCategoryDtoDto = null)
    {
        if($journeyDto != null)
        {
            $this->name = $journeyCategoryDtoDto->name;
        }
        $this->journeys = new EntityCollection;       
    }

    public function addJourney(Journey $journey)
    {
        $this->journeys->push($journey);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getJourneys()
    {
        return $this->journeys;
    }

    public function getPlans()
    {
        return $this->plans;
    }

}