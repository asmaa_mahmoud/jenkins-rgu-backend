<?php

/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 12:39 PM
 */
namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Blocks;

class BlocklyMap extends EntityMap
{
    protected $table = 'block_category';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "block_category.deleted_at";


    public function blocks(Blockly $blockly)
    {
        return $this->belongsToMany($blockly, Blocks::class , 'block_category_block','block_category_id', 'block_id');
    }
}