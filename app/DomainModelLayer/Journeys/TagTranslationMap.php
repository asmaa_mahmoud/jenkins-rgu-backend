<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class TagTranslationMap extends EntityMap
{
    protected $table = 'tag_translation';
    public $timestamps = true;

    public function tag(TagTranslation $translation){
        return $this->belongsTo($translation, Tag::class, 'tag_id', 'id');
    }

}