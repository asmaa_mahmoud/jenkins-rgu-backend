<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ChoiceTranslationMap extends EntityMap {

    protected $table = 'choice_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "choice_translation.deleted_at";


}