<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MissionMcqChooseTranslation extends Entity {

    public function choice(){
        return $this->choice;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function getCorrectionMessage(){
        return $this->message;
    }

}