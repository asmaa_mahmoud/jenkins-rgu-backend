<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ActivityPeriodTranslation extends Entity
{
    public function __construct(ActivityPeriod $period, $name, $language_code){
        $this->period = $period;
        $this->name = $name;
        $this->language_code = $language_code;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function setLanguageCode($language_code){
        $this->language_code = $language_code;
    }

    public function getActivityPeriod(){
        return $this->period;
    }
}