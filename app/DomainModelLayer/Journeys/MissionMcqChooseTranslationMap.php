<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class MissionMcqChooseTranslationMap extends EntityMap {

    protected $table = 'mission_mcq_choice_translation';
    public $timestamps = true;

    public function choice(MissionMcqChooseTranslation $chooseTranslation)
    {
        return $this->belongsTo($chooseTranslation, MissionMcqChoose::class , 'choice_id', 'id');
    }
}