<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MatchKeyTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            '`key`'
        ];
        return $searchable;

    }
    

    public function getId()
    {
        return $this->id;
    }

    
    public function getLanguageCode()
    {
        return $this->language_code;
    }


    public function getText()
    {
        return $this->key;
    }

 

}
