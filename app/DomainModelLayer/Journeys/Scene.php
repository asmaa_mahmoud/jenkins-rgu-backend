<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class Scene extends Entity
{

    public function getName()
    {
        return $this->name;
    }

    public function getTitle()
    {
        return $this->title;
    }

}
