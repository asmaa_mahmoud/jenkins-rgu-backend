<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityTaskMap extends EntityMap
{
    protected $table = 'activity_task';
    public $timestamps = true;

    public function activity(ActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(ActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function task(ActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, Task::class , 'task_id', 'id');
    }
    public function difficulty(ActivityTask $activityTask)
    {
        return $this->belongsTo($activityTask, Difficulty::class , 'difficulty_id', 'id');
    }

}