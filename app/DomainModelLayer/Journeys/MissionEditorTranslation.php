<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:33 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\Entity;

class MissionEditorTranslation extends Entity
{


    public static function searchable(){
        $searchable = [
            'title',
            'description'
        ];
        return $searchable;

    }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDescriptionSpeech()
    {
        return $this->description_speech;
    }

    public function getVideoUrl()
    {
        return $this->video_url;
    }
}