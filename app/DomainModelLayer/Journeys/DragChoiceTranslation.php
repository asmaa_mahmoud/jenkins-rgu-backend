<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DragChoiceTranslation extends Entity
{

    public static function searchable(){
        $searchable = [
            'cell_answer',
        ];
        return $searchable;

    }
    

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getBody()
    {
        return $this->cell_answer;
    }
    public function getExplanation()
    {
        return $this->explanation;
    }

}
