<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\AdventureTranslation;

class MissionMcqMap extends EntityMap {

    protected $table = 'mission_mcq';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_mcq.deleted_at";

    // protected $with = ['mission'];

    public function mission(MissionMcq $missionMcq)
    {
        return $this->belongsTo($missionMcq, Mission::class , 'mission_id', 'id');
    }

    public function questions(MissionMcq $missionMcq){
        return $this->hasMany($missionMcq, MissionMcqQuestion::class , 'mission_mcq_id', 'id');
    }

}