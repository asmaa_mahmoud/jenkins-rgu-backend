<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class ChoiceTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'body',
        ];
        return $searchable;

    }

    // public function __construct(AdventureDto $adventureDto = null)
    // {
    //     if($adventureDto != null )
    //     {
    //         $this->name_english = $adventureDto->name_english;
    //         $this->id = $adventureDto->id;
    //         $this->desctiption_english = $adventureDto->desctiption_english;
    //         $this->roadmapImageURL = $adventureDto->link;
    //         $this->Journey_Id = $adventureDto->journey_id;
    //     }
    // }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getExplanation()
    {
        return $this->explanation;
    }

    public function getWrongExplanation()
    {
        return $this->wrong_explanation;
    }

}
