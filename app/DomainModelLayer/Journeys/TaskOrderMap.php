<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\Journey;

class TaskOrderMap extends EntityMap {

    protected $table = 'task_order';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "task_order.deleted_at";


    public function task(TaskOrder $taskOrder)
    {
        return $this->belongsTo($taskOrder, Task::class , 'task_id', 'id');
    }

    public function adventure(TaskOrder $taskOrder)
    {
        return $this->belongsTo($taskOrder, Adventure::class , 'adventure_id', 'id');
    }
    public function journey(TaskOrder $taskOrder)
    {
        return $this->belongsTo($taskOrder, Journey::class , 'journey_id', 'id');
    }

}