<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DragQuestion extends Entity
{

     public function getQuestionId()
    {
        return $this->question->getId();
    }

    public function getQuestion()
    {
        return $this->question;
    }
    public function getDragCells()
    {
        return $this->dragCells;
    }
    public function getExplanation()
    {
        return $this->getDragChoices()[0]->getExplanation();
    }
    public function getDragChoices()
    {
        $allQuestionChoices= $this->dragChoices;
        $viewedChoices=[];
        foreach ($allQuestionChoices as $questionChoice) {
            if($questionChoice->dragCell->getShowAnswer()=='0')
                $viewedChoices[]=$questionChoice;

        }
        return $viewedChoices;
    }
    public function getTitle()
    {
        return $this->question->question_task->step->getTitle();
    }
    public function getDescription()
    {
        return $this->question->question_task->step->getDescription();
    }
    public function getRightExplanation()
    {
        return $this->question->question_task->step->getRightExplanation();
    }
    public function getWrongExplanation()
    {
        return $this->question->question_task->step->getWrongExplanation();
    }
    public function getType()
    {
        return "sequence_match";
    }
    public function getId()
    {
        return $this->id;
    }

    

}
