<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Task;

class TaskProgressActivity extends Entity
{
    public function __construct(Task $task, User $user,$score,$trials,$first_success = null,$timeTaken = null,$bestTimeTaken = null,$numberOfBlocks = null,$bestNumberOfBlocks = null)
    {
        $this->task = $task;
        $this->user = $user;
        $this->score = $score;
        $this->no_of_trials = $trials;
        if($first_success != null)
            $this->first_success = $first_success;

        $this->task_duration = $timeTaken;
        $this->	best_task_duration	 = $bestTimeTaken;
        $this->no_of_blocks = $numberOfBlocks;
        $this->	best_blocks_number = $bestNumberOfBlocks;
    }


    public function getTask()
    {
        return $this->task;
    }

    public function getTaskId()
    {
        return $this->task_id;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;
    }

    public function getNo_of_trails()
    {
        return $this->no_of_trials;
    }

    public function setNo_of_trails($number)
    {
        $this->no_of_trials = $number;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity)
    {
         $this->defaultActivity = $defaultActivity;
    }

    public function setActivity(Activity $activity)
    {
        return $this->activity = $activity;
    }

    public function getFirstSuccess(){
        return $this->first_success;
    }
    public function getTaskDuration(){
        return $this->task_duration;

    }
    public function getBestTaskDuration(){
        return $this->	best_task_duration;

    }
    public function getBlocksNumber(){
        return $this->no_of_blocks;

    }
    public function getBestBlocksNumber(){
        return $this->	best_blocks_number;
    }


}
