<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/11/17
 * Time: 1:24 PM
 */
namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MissionState extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRatio()
    {
        return $this->ratio;
    }

    public function getMissions()
    {
        return $this->missions;
    }

}
