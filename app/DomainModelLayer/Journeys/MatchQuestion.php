<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MatchQuestion extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getMatchKeys()
    {
        return $this->matchKeys;
    }

    public function getMatchValues()
    {
        return $this->matchValues;
    }



}
