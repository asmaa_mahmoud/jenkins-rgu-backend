<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MissionHtmlSteps extends Entity
{

    public function getId(){
        return $this->id;
    }
    public function getBrickType(){

        return $this->step_type;
    }

    public function getQuestionTask(){
        if($this->step_type == 'quizbricks')
           return $this->question_task;
    }

    public function getQuestion(){
        if($this->step_type == 'quizbricks'){
            $question=$this->question_task->question;
           if($question->question_type == "dropdown"){
              return $question->getDropdownQuestion();
            }
            elseif($question->question_type == "question")
            {
              return $question->getMcqQuestion();
            }
            elseif($question->question_type == "match")
            {
              return $question->getMatchQuestion();
            }
            elseif($question->question_type == "sequence_match")
            {
              return $question->getDragQuestion();
            }
       }
    }
    public function getDragQuestion(){
        if($this->step_type == 'quizbricks'){
            $question=$this->question_task->question;
           if($question->question_type == "sequence_match"){
              return $question->getDragQuestion();
           }
       }
    }

    public function getStepQuestionType(){
        if($this->step_type=="quizbricks"){
            if($this->question_task->question->question_type == "question")
            {
              return "multiple_choice";
            }
            else{
               return $this->question_task->question->question_type;
            }

        }
   
    }

    public function getStepQuestionId(){
        if($this->step_type=="quizbricks"){

          return $this->question_task->question->id;
        }
   
    }

    public function getContent()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getContent();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getContent();
        }
        return $english_translation;
    }
    public function getMissionHtml()
    {
        return $this->missionHtml;
    }

    public function getVideoUrl()
    {
        return $this->video_url;
    }

    public function getImageUrl()
    {
        return $this->image_url;
    }

    public function getOrder(){
        return $this->order;
    }

    public function getTranslations(){
        return $this->translations;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getRightExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getRightExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getRightExplanation();
        }
        return $english_translation;
    }

    public function getWrongExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getWrongExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getWrongExplanation();
        }
        return $english_translation;
    }

    public function getEditorText(){
        return $this->editor_text;
    }




}
