<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MissionHtmlType extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

}
