<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Journeys\RankLevel;

class CampusUserStatusMap extends EntityMap
{
    protected $table = 'campus_user_status';
    public $timestamps = true;

    public function user(CampusUserStatus $campusUserStatus)
    {
        return $this->belongsTo($campusUserStatus, User::class , 'user_id', 'id');//TODO:FIX ERROR
    }

    public function campus(CampusUserStatus $campusUserStatus)
    {
        return $this->belongsTo($campusUserStatus, Campus::class , 'campus_id', 'id');
    }

    public function rank_level(CampusUserStatus $campusUserStatus)
    {
        return $this->belongsTo($campusUserStatus, RankLevel::class , 'user_id', 'id');//TODO:FIX ERROR
    }

}