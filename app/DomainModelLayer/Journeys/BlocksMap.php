<?php

/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 12:39 PM
 */
namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Blockly;

class BlocksMap extends EntityMap
{
    protected $table = 'block';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "block.deleted_at";

}