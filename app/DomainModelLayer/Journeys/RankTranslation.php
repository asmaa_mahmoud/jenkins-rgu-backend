<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class RankTranslation extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }
}