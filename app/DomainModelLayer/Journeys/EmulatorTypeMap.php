<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;

class EmulatorTypeMap extends EntityMap {

    protected $table = 'emulator_type';

}