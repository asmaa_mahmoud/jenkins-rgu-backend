<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 4/3/18
 * Time: 2:22 PM
 */

namespace App\DomainModelLayer\Journeys;


use App\DomainModelLayer\Accounts\User;
use Analogue\ORM\Entity;

class MissionSavingActivity extends Entity
{
    public function __construct(Mission $mission, User $user,$name,$xml,$timeTaken)
    {
        $this->mission = $mission;
        $this->user = $user;
        $this->name = $name;
        $this->xml = $xml;
        $this->timetaken = $timeTaken;
    }

    public function setDefaultActivity(DefaultActivity $defaultActivity){
        $this->defaultActivity = $defaultActivity;
    }

    public function setActivity(Activity $activity){
        $this->activity = $activity;
    }

    public function setXml($xml)
    {
        $this->xml = $xml;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->xml;
    }

    public function getTimetaken(){
        return $this->timetaken;
    }

    public function setTimetaken($timeTaken){
        $this->timetaken = $timeTaken;
    }
}