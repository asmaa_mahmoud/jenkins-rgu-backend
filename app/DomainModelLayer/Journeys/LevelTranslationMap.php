<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class LevelTranslationMap extends EntityMap
{
    protected $table = 'level_translation';
    public $timestamps = false;

    public function level(LevelTranslation $levelTranslation)
    {
        return $this->belongsTo($levelTranslation, Level::class , 'level_id', 'id');
    }
}