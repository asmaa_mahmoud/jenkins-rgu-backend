<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Schools\CampTemplate;

class DefaultActivityMap extends EntityMap
{
    protected $table = 'default_activity';
    public $timestamps = true;

    public function difficulty(DefaultActivity $activity){
        return $this->belongsTo($activity, Difficulty::class, 'difficulty_id', 'id');
    }

    public function translations(DefaultActivity $activity){
        return $this->hasMany($activity, ActivityTranslation::class , 'default_activity_id', 'id');
    }

    public function activityTasks(DefaultActivity $activity){
        return $this->hasMany($activity, ActivityTask::class , 'default_activity_id', 'id');
    }

    public function mainActivityTasks(DefaultActivity $activity){
        return $this->hasMany($activity, ActivityTask::class , 'default_activity_id', 'id')->orderBy('order')->whereNull('activity_task.deleted_at')->where('booster_type','!=','1');
    }

    public function tasks(DefaultActivity $activity){
        return $this->belongsToMany($activity, Task::class , 'activity_task', 'default_activity_id', 'task_id')->orderBy('order')->whereNull('activity_task.deleted_at');
    }
    public function mainTasks(DefaultActivity $activity){
        return $this->belongsToMany($activity, Task::class , 'activity_task', 'default_activity_id', 'task_id')->where('booster_type','!=','1')->orderBy('order');;
    }

    public function prices(DefaultActivity $activity){
        return $this->hasMany($activity, ActivityPrice::class , 'activity_id', 'id');
    }

    public function preNext(DefaultActivity $activity){
        return $this->hasMany($activity, ActivityPreNext::class , 'main_default_activity_id', 'id');
    }

    public function tags(DefaultActivity $activity){
        return $this->belongsToMany($activity, Tag::class , 'activity_tag', 'default_activity_id', 'tag_id');
    }

    public function screenshots(DefaultActivity $activity){
        return $this->hasMany($activity, ActivityScreenshots::class , 'default_activity_id', 'id');
    }

    public function camps(DefaultActivity $activity){
        return $this->belongsToMany($activity, Camp::class , 'camp_template','default_activity_id', 'id');
    }

    public function defaultCamps(DefaultActivity $activity){
        return $this->belongsToMany($activity, CampTemplate::class , 'camp_template_activity','default_activity_id', 'id');
    }

    public function defaultActivityUnlockable(DefaultActivity $activity)
    {
        return $this->hasOne($activity, ActivityUnlockable::class , 'default_activity_id', 'id');
    }

    public function tasksUsersCurrentSteps(DefaultActivity $activity){
        return $this->hasMany($activity, TaskCurrentStep::class, 'default_activity_id');
    }
}