<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\MissionScreen;
use App\DomainModelLayer\Journeys\MissionTranslation;
use App\DomainModelLayer\Journeys\Blockly;
use App\DomainModelLayer\Journeys\ProgrammingLanguageType;
use App\DomainModelLayer\Journeys\MissionState;

class MissionMap extends EntityMap {

    protected $table = 'mission';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission.deleted_at";


    public function task(Mission $mission)
    {
        return $this->belongsTo($mission, Task::class , 'task_id', 'id');
    }

    public function blocklyCategory(Mission $mission){
        return $this->belongsToMany($mission, Blockly::class , 'mission_category', 'mission_id', 'block_category_id');
    }

    public function missionSavings(Mission $mission)
    {
        return $this->hasMany($mission, MissionSavings::class , 'mission_id', 'id');
    }

    public function missionSavingActivity(Mission $mission){
        return $this->hasMany($mission, MissionSavingActivity::class , 'mission_id', 'id');
    }

    // public function missionProgress(Mission $mission)
    // {
    //     return $this->hasMany($mission, MissionProgress::class , 'Mission_Id', 'id');
    // }

    public function missionScreens(Mission $mission)
    {
        return $this->hasMany($mission, MissionScreen::class , 'mission_id', 'id');
    }

    public function translations(Mission $mission)
    {
        return $this->hasMany($mission, MissionTranslation::class , 'mission_id', 'id');
    }

    public function minecraftsteps(Mission $mission)
    {
        return $this->hasMany($mission, MinecraftMissionSteps::class , 'mission_id', 'id');
    }

    public function type(Mission $mission)
    {
        return $this->belongsTo($mission, MissionType::class , 'type_id', 'id');
    }

    public function programmingLanguageType(Mission $mission)
    {
        return $this->belongsTo($mission, ProgrammingLanguageType::class , 'programming_language_type_id', 'id');
    }

    public function MissionMcq(Mission $mission){
        return $this->hasOne($mission, MissionMcq::class , 'mission_id', 'id');
    }

    public function MissionText(Mission $mission){
        return $this->hasOne($mission, MissionText::class , 'mission_id', 'id');
    }

    public function missionState(Mission $mission)
    {
        return $this->belongsTo($mission, MissionState::class , 'mission_state_id', 'id');
    }

}