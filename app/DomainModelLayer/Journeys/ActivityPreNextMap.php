<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityPreNextMap extends EntityMap
{
    protected $table = 'activity_pre_next';
    public $timestamps = true;

    public function defaultMainActivity(ActivityPreNext $activityPreNext){
        return $this->belongsTo($activityPreNext, DefaultActivity::class, 'main_default_activity_id', 'id');
    }

    public function defaultActivity(ActivityPreNext $activityPreNext){
        return $this->belongsTo($activityPreNext, DefaultActivity::class, 'default_activity_id', 'id');
    }


}