<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityPeriodTranslationMap extends EntityMap
{
    protected $table = 'activity_period_translation';
    public $timestamps = false;

    public function period(ActivityPeriodTranslation $translation){
        return $this->belongsTo($translation, ActivityPeriod::class, 'period_id', 'id');
    }

}