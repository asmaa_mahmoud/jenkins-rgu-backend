<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class DifficultyTranslationMap extends EntityMap
{
    protected $table = 'difficulty_translation';
    public $timestamps = false;

    public function difficulty(DifficultyTranslation $difficultyTranslation)
    {
        return $this->belongsTo($difficultyTranslation, Difficulty::class , 'difficulty_id', 'id');
    }
}