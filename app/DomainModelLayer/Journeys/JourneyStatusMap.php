<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class JourneyStatusMap extends EntityMap {

    protected $table = 'journey_status';

}