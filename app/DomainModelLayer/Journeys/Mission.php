<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Missions\MissionDto;
use LaravelLocalization;


class Mission extends Entity
{

    public function addMissionSaving(MissionSavings $missionsaving)
    {
        $this->missionSavings->push($missionsaving);
    }

    public function removeMissionSaving(MissionSavings $missionsaving)
    {
        $this->missionSavings->remove($missionsaving);
    }

    public function addMissionSavingActivity(MissionSavingActivity $missionsaving)
    {
        $this->missionSavingActivity->push($missionsaving);
    }

    public function removeMissionSavingActivity(MissionSavingActivity $missionsaving)
    {
        $this->missionSavingActivity->remove($missionsaving);
    }

    public function getVplId()
    {
        return $this->vpl_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getType(){
        return $this->type->getName();
    }

    public function getBlocklyRules(){
        return $this->blockly_rules;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getVideoUrl()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getVideoUrl();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getVideoUrl();
        }
        return $english_translation;
    }

    public function getDescriptionSpeech()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescriptionSpeech();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescriptionSpeech();
        }
        return $english_translation;
    }

    public function getIconUrl()
    {
        return $this->icon_url;
    }
    
    public function getBlocklyCategory(){
        return $this->blocklyCategory;
    }

    public function getScreenshots()
    {
        return $this->missionScreens;
    }

    public function showModelAnswer()
    {
        return $this->model_answer;
    }

    public function getSceneName()
    {
        return $this->scene_name;
    }

    public function getOrder()
    {
        return $this->task->getOrder();
    }

    public function getPositionX()
    {
        return $this->task->getPositionX();
    }

    public function getPositionY()
    {
        return $this->task->getPositionY();
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getProgrammingLanguageType(){
        return $this->programmingLanguageType;
    }

    public function getHint(){
        return $this->hint;
    }

    public function MissionText(){
        return $this->MissionText;
    }

    public function getGoldenTime(){
        return $this->golden_time;
    }

    public function getModelAnswerNumberOfBlocks(){
        $modelAnswer = $this->model_answer;
        $splitted = preg_split('/type="(\w+)"/', $modelAnswer);
        return count($splitted)-1;

    }

    public function getMissionState(){
        return $this->missionState;
    }

    public function getMissionMcq(){
        return $this->MissionMcq;
    }

    public function getMinecraftSteps(){
        return $this->minecraftsteps;
    }

    public function getBlocksType(){
        return $this->blocks_type;
    }
}