<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\QuizTutorialsTranslation;
// use App\DomainModelLayer\Adventures\Adventure;
// use App\DomainModelLayer\Questions\Question;
// use App\DomainModelLayer\QuizProgress\QuizProgress;
// use App\DomainModelLayer\QuizTypes\QuizType;

class QuizTutorialsMap extends EntityMap {

    protected $table = 'quiz_tutorial';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "quiz_tutorial.deleted_at";


    // public function quiz(QuizTutorials $quizTutorials)
    // {
    //     return $this->belongsTo($quizTutorials, Quiz::class , 'Quiz_Id', 'id');
    // }

    public function translations(QuizTutorials $quizTutorials)
    {
        return $this->hasMany($quizTutorials, QuizTutorialsTranslation::class , 'quiz_tutorial_id', 'id');
    }

}