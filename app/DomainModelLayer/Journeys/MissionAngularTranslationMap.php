<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:34 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionAngularTranslationMap extends EntityMap
{
    protected $table = 'mission_angular_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_angular_translation.deleted_at";
}