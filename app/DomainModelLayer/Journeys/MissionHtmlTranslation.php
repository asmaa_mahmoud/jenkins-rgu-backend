<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MissionHtmlTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'title',
        ];
        return $searchable;

    }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getModelAnswerLink()
    {
        return $this->model_answer_link;
    }

}
