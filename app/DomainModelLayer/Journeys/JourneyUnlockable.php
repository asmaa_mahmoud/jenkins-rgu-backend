<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class JourneyUnlockable extends Entity
{
    public function __construct(Journey $journey, Unlockable $unlockable)
    {
        $this->journey = $journey;
        $this->unlockable = $unlockable;
    }

    public function getUnlockable(){
        return $this->unlockable;
    }

    public function setUnlockable(Unlockable $unlockable){
        $this->unlockable = $unlockable;
    }

    public function getJourney(){
        return $this->journey;
    }

    public function setJourney(Journey $journey){
        $this->journey = $journey;
    }
}
