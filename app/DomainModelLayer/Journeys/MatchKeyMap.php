<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\MatchKeyTranslation;
use App\DomainModelLayer\Journeys\MatchValue;

class MatchKeyMap extends EntityMap {

    protected $table = 'match_key';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "match_key.deleted_at";



    public function translations(MatchKey $matchKey)
    {
        return $this->hasMany($matchKey, MatchKeyTranslation::class, 'match_key_id', 'id');
    }

    public function matchValue(MatchKey $matchKey)
    {
        return $this->hasOne($matchKey, MatchValue::class,'id','match_value_id');
    }
 
}