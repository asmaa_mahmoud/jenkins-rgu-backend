<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class MissionTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'title',
            'description'
        ];
        return $searchable;

    }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getVideoUrl(){
        return $this->video_url;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDescriptionSpeech()
    {
        return $this->description_speech;
    }

}
