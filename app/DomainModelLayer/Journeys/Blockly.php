<?php

/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 12:39 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Journeys\Dtos\BlocklyCategoryDto;

class Blockly extends Entity
{
    public function __construct(BlocklyCategoryDto $blocklyDto = null)
    {
        if($blocklyDto != null )
        {
            $this->Name = $blocklyDto->Name;
            $this->blocks = new EntityCollection;
        }
    }

    public function getName(){
        return $this->name;
    }

    public function getBlocks(){
        return $this->blocks;
    }
}