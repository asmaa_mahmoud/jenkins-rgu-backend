<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\mcqQuestion;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\McqQuestionTranslation;
use App\DomainModelLayer\Journeys\Choice;

class McqQuestionMap extends EntityMap {

    protected $table = 'mcq_question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mcq_question.deleted_at";



     public function question(McqQuestion $mcqQuestion){

        return $this->belongsTo($mcqQuestion, Question::class , 'question_id', 'id');
    }

    public function emulatorType(McqQuestion $question)
    {
        return $this->belongsTo($question, EmulatorType::class , 'emulator_type_id', 'id');
    }

    public function translations(McqQuestion $mcqQuestion)
    {
        return $this->hasMany($mcqQuestion, McqQuestionTranslation::class, 'question_id', 'id');
    }

    public function choices(McqQuestion $mcqQuestion)
    {
        return $this->hasMany($mcqQuestion, Choice::class, 'mcq_question_id', 'id');
    }


 
}