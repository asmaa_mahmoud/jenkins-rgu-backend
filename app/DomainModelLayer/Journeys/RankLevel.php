<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class RankLevel extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getRank(){
        return $this->rank;
    }

    public function getLevel(){
        return $this->level;
    }

}
