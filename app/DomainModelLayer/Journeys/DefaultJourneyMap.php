<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\Adventure;

class DefaultJourneyMap extends EntityMap {

    protected $table = 'default_journey';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "default_journey.deleted_at";


    public function adventures(DefaultJourney $defaultJourney)
    {
        return $this->belongsToMany($defaultJourney, Adventure::class , 'journey_adventure','journey_id', 'adventure_id');
    }

    public function journey(DefaultJourney $defaultJourney)
    {
        return $this->belongsTo($defaultJourney, Journey::class , 'journey_id', 'id');
    }

}