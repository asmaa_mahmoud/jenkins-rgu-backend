<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DropdownChoiceTranslation extends Entity
{
    public static function searchable(){
        $searchable = [
            'choice'
        ];
        return $searchable;

    }
    

    public function getId()
    {
        return $this->id;
    }

    
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getChoice()
    {
        return $this->choice;
    }

    public function getExplanation()
    {
        return $this->explanation;
    }


   

}
