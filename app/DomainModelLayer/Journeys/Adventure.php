<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Adventures\AdventureDto;
use LaravelLocalization;

class Adventure extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
            
        }
        return $english_translation;
    }

    public function getRoadMapImage()
    {
        return $this->roadmap_image_url;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getTasks()
    {
        return $this->tasks;
    }

    public function getJourneys(){
        return $this->journeys;
    }

    public function getEndDateInCourse($course_id)
    {
        $deadlines = $this->courseDeadlines;

        foreach ($deadlines as $deadline) {
            if($deadline->getCourse()->getId() == $course_id)
                return $deadline->getEndsAt();
        }
    }

}
