<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionHtmlTranslationMap extends EntityMap {

    protected $table = 'mission_html_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_html_translation.deleted_at";




}