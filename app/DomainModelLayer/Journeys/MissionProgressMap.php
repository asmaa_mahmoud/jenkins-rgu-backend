<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Customers\User;

class MissionProgressMap extends EntityMap {

    protected $table = 'missionprogress';
	public $timestamps = true;

    public function mission(MissionProgress $missionProgress)
    {
        return $this->belongsTo($missionProgress, Mission::class , 'Mission_Id', 'id');
    }

    public function user(MissionProgress $missionProgress)
    {
        return $this->belongsTo($missionProgress, User::class , 'User_Id', 'id');
    }

}