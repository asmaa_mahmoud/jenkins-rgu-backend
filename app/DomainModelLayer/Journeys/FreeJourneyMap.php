<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\FreeJourneyTask;

class FreeJourneyMap extends EntityMap {

    protected $table = 'free_journey';

    public function tasks(FreeJourney $FreeJourney)
    {
        return $this->hasMany($FreeJourney, FreeJourneyTask::class , 'free_journey_id', 'id');
    }

    public function translations(FreeJourney $FreeJourney)
    {
        return $this->hasMany($FreeJourney, FreeJourneyTranslation::class , 'free_journey_id', 'id');
    }

}