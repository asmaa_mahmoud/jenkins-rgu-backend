<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\AdventureTranslation;

class MissionTextQuestionMap extends EntityMap {

    protected $table = 'mission_text_question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_text_question.deleted_at";


}