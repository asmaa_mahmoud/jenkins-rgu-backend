<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\TaskOrder;
use App\DomainModelLayer\Journeys\ActivityTask;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Professional\Project;
use App\DomainModelLayer\Professional\TaskCurrentStep;

class
TaskMap extends EntityMap {

    protected $table = 'task';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "task.deleted_at";


    public function missions(Task $task)
    {
        return $this->hasMany($task, Mission::class , 'task_id', 'id');
    }

    public function missionCoding(Task $task)
    {
        return $this->hasOne($task, MissionCoding::class , 'task_id', 'id');
    }

    public function missionEditor(Task $task)
    {
        return $this->hasOne($task, MissionEditor::class , 'task_id', 'id');
    }

    public function missionAngular(Task $task)
    {
        return $this->hasOne($task, MissionAngular::class , 'task_id', 'id');
    }

    public function missonsHtml(Task $task){
        return $this->hasMany($task,MissionHtml::class ,'task_id','id');
    }

    public function missonsCoding(Task $task){
        return $this->hasMany($task,MissionCoding::class ,'task_id','id');
    }
    public function missonsEditor(Task $task){
        return $this->hasMany($task,MissionEditor::class ,'task_id','id');
    }

    public function quizzes(Task $task)
    {
        return $this->hasMany($task, Quiz::class , 'task_id', 'id');
    }

    public function projects(Task $task)
    {
        return $this->hasMany($task,Project::class,'task_id','id');
    }

    public function progress(Task $task)
    {
        return $this->hasMany($task, TaskProgress::class , 'task_id', 'id');
    }

    public function activityTasks(Task $task)
    {
        return $this->hasMany($task,ActivityTask::class,'task_id','id');
    }

    public function taskOrders(Task $task)
    {
        return $this->hasMany($task, TaskOrder::class , 'task_id', 'id');
    }

    public function freetaskOrders(Task $task)
    {
        return $this->hasMany($task, FreeJourneyTask::class , 'task_id', 'id');
    }

    public function resources(Task $task)
    {
        return $this->belongsToMany($task, Resource::class, 'task_resource', 'task_id', 'resource_id');
    }

    public function TaskResources(Task $task)
    {
        return $this->hasMany($task,TaskResource::class,'task_id','id');
    }

    public function usersCurrentSteps(Task $task){
        return $this->hasMany($task, TaskCurrentStep::class, 'task_id');
    }

}