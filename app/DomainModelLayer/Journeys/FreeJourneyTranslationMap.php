<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class FreeJourneyTranslationMap extends EntityMap
{
    protected $table = 'free_journey_translations';

}