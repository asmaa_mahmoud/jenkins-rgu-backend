<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:33 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\Entity;

class MissionAngularFile extends Entity
{
  
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getModelAnswer()
    {
        return $this->mission->files->where('is_model',1)->where('name',$this->getName())->first()->model_answer;
    }

}