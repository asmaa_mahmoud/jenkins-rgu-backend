<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class DefaultJourney extends Entity
{
	public function getAdventures()
	{
		return $this->adventures;
	}

	public function getId()
	{
		return $this->id;
	}
}
