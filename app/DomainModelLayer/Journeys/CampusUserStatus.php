<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;

class CampusUserStatus extends Entity
{
    public function __construct($user_id , $campus_id , $xp, $rank_level_id){
        $this->user_id = $user_id;
        $this->campus_id = $campus_id;
        $this->rank_level_id = $rank_level_id;
        $this->xp = $xp;
       
    } 
    public function getId()
    {
        return $this->id;
    }

    public function getCampus()
    {
        return $this->campus;
    }

    public function getHideBlocks()
    {
        return $this->hide_blocks;
    }

    public function getXps()
    {
        return $this->xp;
    }
    public function setXps($xps)
    {
        $this->xp=$xps;
    }

    public function setRankLevelId($rankLevelId){
        $this->rank_level_id = $rankLevelId;
    }
}