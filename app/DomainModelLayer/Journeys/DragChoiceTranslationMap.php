<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class DragChoiceTranslationMap extends EntityMap {

    protected $table = 'drag_choice_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "drag_choice_translation.deleted_at";


}