<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
//use App\ApplicationLayer\Missions\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;


class MissionScreen extends Entity
{

    public function __construct(MissionScreensDto $missionScreensDto = null, Mission $mission)
    {
        if($missionScreensDto != null && $mission != null)
        {
            $this->screenshot_url = $missionScreensDto->url;
            $this->mission = $mission;
        }
    }    

    public function getUrl()
    {
        return $this->screenshot_url;
    }    
}