<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;
use LaravelLocalization;


class Quiz extends Entity
{

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function geticonURL(){
        return $this->icon_url;
    }

    public function getQuestions()
    {
        return $this->questions;
    }

    public function getTutorials()
    {
        return $this->quizTutorials;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getTaskId()
    {
        return $this->task->getId();
    }

    public function getType()
    {
        return $this->type;
    }

    public function getQuestionsWeight(){
        $weight = 0;
        $q_weight = 0;
        $key = 0;
        $questions = $this->getQuestions();
        if($questions != null){
            foreach ($questions as $question){
                if($key == 0){
                    $q_weight = $question->weight;
                }
                $weight += $question->weight;
                $key++;
            }
        }
        return ['weight' => $weight, 'q_weight' => $q_weight];
    }

    public function getQuestionTasks()
    {
        return $this->questionTasks;
    }
}
