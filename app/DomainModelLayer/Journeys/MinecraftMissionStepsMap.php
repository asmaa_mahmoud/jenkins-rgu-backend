<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class MinecraftMissionStepsMap extends EntityMap
{
    protected $table = 'minecraft_mission_steps';
    public $timestamps = true;
    public $softDeletes = false;

    public function mission(MinecraftMissionSteps $minecraftMissionSteps)
    {
        return $this->belongsTo($minecraftMissionSteps, Mission::class , 'mission_id', 'id');
    }

    public function translations(MinecraftMissionSteps $minecraftMissionSteps)
    {
        return $this->hasMany($minecraftMissionSteps, MinecraftMissionStepTranslations::class , 'mission_step_id', 'id');
    }
}