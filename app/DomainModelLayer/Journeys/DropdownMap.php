<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\DropdownQuestion;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\DropdownChoice;

class DropdownMap extends EntityMap {

    protected $table = 'dropdown';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dropdown.deleted_at";



    public function dropdown_question(Dropdown $dropdown)
    {
        return $this->belongsTo( $dropdown,DropdownQuestion::class, 'dropdown_question_id', 'id');
    }

    public function dropdown_choice(Dropdown $dropdown)
    {
        return $this->hasMany($dropdown,DropdownChoice::class, 'dropdown_id', 'id');
    }

    public function translations(Dropdown $dropdown)
    {
        return $this->hasMany($dropdown,DropdownTranslation::class, 'dropdown_id', 'id');
    }


 
}