<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use App\Helpers\Mapper;
use LaravelLocalization;


class Question extends Entity
{

    public function __construct(Question $questionDto = null)
    {
        if($questionDto != null )
        {
            $this->id = $questionDto->id;
            $this->body= $questionDto->body;
            $this->Quiz_Id = $questionDto->quiz_id;
            $this->Weight = $questionDto->weight;
        }
    }

    public function getBody()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getBody();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getBody();
        }
        return $english_translation;
    }

    public function getChoices()
    {
        return $this->choices;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getQuizId()
    {
        return $this->quiz->getId();
    }
    public function getIsEditor(){
        return $this->is_editor;
    }

    public function getInitialImage(){
        return $this->initial_image;
    }

    public function getMainImage(){
        return $this->main_image;
    }
    public function getEditorCode(){
        return $this->editor_code;
    }
    public function getEmulatorType(){
        //return ($this->emulator_type->getName());//doesn't work as emulator_type is null
        //dd($this->emulator_type->getName());
        return $this->emulator_type['name'];
    }

    public function getDropdownQuestion()
    {
        return $this->dropdown_question;
    }
    public function getDragQuestion()
    {
        return $this->dragQuestion;
    }

    public function getMcqQuestion()
    {
        return $this->mcq_question;
    }

    public function getSelectMany()
    {
        return $this->getMcqQuestion()->selectMany;
    }

    public function getMatchQuestion()
    {
        return $this->matchQuestion;
    }

}
