<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
// use App\ApplicationLayer\Blockly\BlocklyDto;
// use App\ApplicationLayer\BlocklyBlocks\BlocklyBlockDto;

class BlocksDefinitions extends Entity
{
    // public function __construct(BlocklyBlockDto $blockDto = null)
    // {
    //     if($blockDto != null )
    //     {
    //         $this->name = $blockDto->Name;
    //     }
    // }

    public function getBlocklyJson(){
        return $this->blockly_json;
    }

    public function getPythonJson(){
        return $this->python_json;
    }

    public function getJavascriptJson(){
        return $this->javascript_json;
    }

    public function getBlock(){
        return $this->block;
    }
}