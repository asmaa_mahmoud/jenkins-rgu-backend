<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Task;

class ProgrammingLanguageTypeMap extends EntityMap {

    protected $table = 'programming_language_type';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "programming_language_type.deleted_at";


}