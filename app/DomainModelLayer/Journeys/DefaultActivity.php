<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class DefaultActivity extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getDifficulty(){
        return $this->difficulty;
    }

    public function getAgeFrom(){
        return $this->age_from;
    }

    public function getAgeTo(){
        return $this->age_to;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getImageUrl(){
        return $this->image_url;
    }

    public function getActivityTasks(){
        return $this->activityTasks;
    }
    public function getMainActivityTasks(){
        return $this->mainActivityTasks;
    }

    public function translate($column){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->{$column};
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->{$column};
        }
        return $english_translation;
    }
    public function getModuleCourseObjectives(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getModuleCourseObjectives();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getModuleCourseObjectives();
        }
        return $english_translation;
    }

    public function getModuleObjectives(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getModuleObjectives();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getModuleObjectives();
        }
        return $english_translation;
    }

    public function getName(){
//        return $this->translate(getName());
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

    public function getEnglishName(){
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == 'en')
                return $translation->getName();
        }
        return $english_translation;
    }

    public function getDescription(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getSummary(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getConcepts(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getConcepts();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getConcepts();
        }
        return $english_translation;
    }

    public function getTechnicalDetails(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTechnicalDetails();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTechnicalDetails();
        }
        return $english_translation;
    }

    public function getPrerequisites(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getPrerequisites();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getPrerequisites();
        }
        return $english_translation;
    }

    public function getTasks(){
        return $this->tasks;
    }
    public function getMainTasks(){
        return $this->mainTasks;
    }

    public function getPrices(){
        return $this->prices;
    }

    public function getPreNext(){
        return $this->preNext;
    }

    public function getTags(){
        return $this->tags;
    }

    public function getScreenshots(){
        return $this->screenshots;
    }

    public function getCamps(){
        return $this->camps;
    }

    public function getDefaultCamps(){
        return $this->defaultCamps;
    }

    public function getDefaultActivityUnlockable(){
        return $this->defaultActivityUnlockable;
    }

    public function getType(){
        return $this->type == 1 ? 'normal' : 'minecraft';
    }

    public function getTasksCount(){
        return count($this->getTasks());
    }

    public function isB2B(){
        return $this->is_b2b;
    }

    public function isB2C(){
        return $this->is_b2c;
    }

    public function getTasksUsersCurrentSteps(){
        return $this->tasksUsersCurrentSteps;
    }
    public function setTasksUsersCurrentSteps($currentSteps){
        $this->tasksUsersCurrentSteps = $currentSteps;
    }

}