<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 15/11/2018
 * Time: 7:24 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\EntityMap;

class MissionAngularScreenMap extends EntityMap
{
    protected $table = 'mission_angular_screenshot';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_angular_screenshot.deleted_at";


    public function mission(MissionAngularScreen $missionScreen)
    {
        return $this->belongsTo($missionScreen, MissionAngular::class , 'mission_angular_id', 'id');
    }
}