<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class RankTranslationMap extends EntityMap
{
    protected $table = 'rank_translation';
    public $timestamps = false;

    public function rank(RankTranslation $rankTranslation)
    {
        return $this->belongsTo($rankTranslation, Rank::class , 'rank_id', 'id');
    }
}