<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class JourneyDataTranslationMap extends EntityMap {

    protected $table = 'journey_data_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "journey_data_translation.deleted_at";


}