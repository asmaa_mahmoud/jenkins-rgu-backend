<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 4/3/18
 * Time: 2:22 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

use App\DomainModelLayer\Accounts\User;

class MissionSavingActivityMap  extends EntityMap
{
    protected $table = 'mission_saving_activity';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_saving_activity.deleted_at";

    public function mission(MissionSavingActivity $missionSavings)
    {
        return $this->belongsTo($missionSavings, Mission::class , 'mission_id', 'id');
    }

    public function user(MissionSavingActivity $missionSavings)
    {
        return $this->belongsTo($missionSavings, User::class , 'user_id', 'id');
    }

    public function activity(MissionSavingActivity $missionSavings)
    {
        return $this->belongsTo($missionSavings, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(MissionSavingActivity $missionSavings)
    {
        return $this->belongsTo($missionSavings, DefaultActivity::class , 'default_activity_id', 'id');
    }

}