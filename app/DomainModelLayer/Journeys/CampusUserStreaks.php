<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;

class CampusUserStreaks extends Entity
{
    public function __construct($user_id , $date , $xp_gained,$campus_id){
        $this->user_id = $user_id;
        $this->date = $date;
        $this->xp_gained = $xp_gained;
        $this->campus_id = $campus_id;
       
    } 
    
    public function getId(){
        return $this->id;
    }


}