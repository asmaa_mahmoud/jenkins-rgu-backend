<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityTranslationMap extends EntityMap
{
    protected $table = 'activity_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "activity_translation.deleted_at";

    public function activity(ActivityTranslation $activityTranslation){
        return $this->belongsTo($activityTranslation, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(ActivityTranslation $activityTranslation){
        return $this->belongsTo($activityTranslation, DefaultActivity::class , 'default_activity_id', 'id');
    }

}