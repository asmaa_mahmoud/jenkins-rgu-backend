<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;


class ActivityUnlockableMap extends EntityMap {

    protected $table = 'activity_unlockable';
    public $timestamps = true;

    public function activity(ActivityUnlockable $activity_unlockable)
    {
        return $this->belongsTo($activity_unlockable, Activity::class , 'activity_id', 'id');
    }

    public function defaultActivity(ActivityUnlockable $activity_unlockable)
    {
        return $this->belongsTo($activity_unlockable, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function unlockable(ActivityUnlockable $activity_unlockable)
    {
        return $this->belongsTo($activity_unlockable, Unlockable::class , 'unlockable_id', 'id');
    }

}