<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Journeys\JourneyTranslation;
use App\DomainModelLayer\Journeys\JourneyDataTranslation;
use App\DomainModelLayer\Journeys\DefaultJourney;
use App\DomainModelLayer\Journeys\JourneyCategory;
use App\DomainModelLayer\Journeys\JourneyText;
use App\DomainModelLayer\Journeys\Scene;
use App\DomainModelLayer\Journeys\JourneyStatus;
use App\DomainModelLayer\Journeys\JourneyData;

class JourneyMap extends EntityMap {

    protected $table = 'journey';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "journey.deleted_at";


    public function journeycategory(Journey $journey)
    {
        return $this->belongsToMany($journey, JourneyCategory::class , 'journey_category','journey_id', 'category_id');
    }

    public function subscriptions(Journey $journey)
    {
        return $this->belongsToMany($journey, Subscription::class , 'subscription_journey','journey_id', 'subscription_id');
    }

    public function defaultJourneys(Journey $journey)
    {
        return $this->hasMany($journey, DefaultJourney::class , 'journey_id', 'id');
    }

    public function translations(Journey $journey)
    {
        return $this->hasMany($journey, JourneyTranslation::class , 'journey_id', 'id');
    }

    public function data(Journey $journey)
    {
        return $this->hasMany($journey, JourneyData::class , 'journey_id', 'id');
    }

    public function datatranslations(Journey $journey)
    {
        return $this->hasMany($journey, JourneyDataTranslation::class , 'journey_id', 'id');
    }
    public function journeyText(Journey $journey)
    {
        return $this->hasMany($journey, JourneyText::class , 'journey_id', 'id');
    }

    public function scene(Journey $journey)
    {
        return $this->belongsTo($journey, Scene::class , 'scene_id', 'id');
    }

    public function status(Journey $journey)
    {
        return $this->belongsTo($journey, JourneyStatus::class , 'journey_status_id', 'id');
    }

    public function adventures(Journey $journey)
    {
        return $this->belongsToMany($journey, Adventure::class , 'journey_adventure','journey_id', 'adventure_id');
    }

    public function defaultUnlockable(Journey $journey)
    {
        return $this->hasOne($journey, JourneyUnlockable::class , 'journey_id', 'id');
    }

}