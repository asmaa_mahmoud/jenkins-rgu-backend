<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:22 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;


class MissionAngular extends Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getTask()
    {
        return $this->task;
    }

     public function getOrder()
    {
        return $this->task->getOrder();
    }

    public function getTranslations(){
        return $this->translations;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getDescription();
        }
        return $english_translation;
    }

    public function getIconUrl(){
        return $this->icon_url;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getFiles(){
        return $this->files->where('is_model',0);
    }

    public function getQuiz(){
        return $this->quiz;
    }
    public function getQuiz_id(){
        if($this->quiz!==null)
            return $this->quiz->id;
        else
            return null;
    }
    public function getQuiz_type(){
        if($this->quiz!==null)
            return $this->getQuiz()->getType()->getName();
        else
            return null;
    }

    public function getScreenshots(){
        return $this->missionScreens;
    }

    public function getDependencies(){
        return $this->dependencies;
    }
}