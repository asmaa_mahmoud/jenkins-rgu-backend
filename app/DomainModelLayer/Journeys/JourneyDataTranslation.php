<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class JourneyDataTranslation extends Entity
{

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getWhatYouWillLearn()
    {
        return $this->what_learn;
    }

    public function getConcept()
    {
        return $this->concept;
    }

    public function getVideoUrl()
    {
        return $this->video_url;
    }

}
