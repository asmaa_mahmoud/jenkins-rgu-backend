<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class AdventureTranslationMap extends EntityMap {

    protected $table = 'default_adventure_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "default_adventure_translation.deleted_at";


}