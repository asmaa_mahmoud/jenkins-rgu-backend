<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:34 PM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MissionAngularFileMap extends EntityMap
{
    protected $table = 'mission_angular_file';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_angular_file.deleted_at";


    public function mission(MissionAngularFile $missionAngularFile)
    {
        return $this->belongsTo($missionAngularFile, MissionAngular::class , 'mission_angular_id', 'id');
    }
}