<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 24/10/2018
 * Time: 10:17 AM
 */

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class ResourceMap extends EntityMap
{
    protected $table = 'resource';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "resource.deleted_at";

    public function tasks(Resource $resource)
    {
        return $this->belongsToMany($resource, Task::class ,'task_resource', 'resource_id', 'task_id');
    }

    public function taskResources(Resource $resource)
    {
        return $this->hasMany($resource, TaskResource::class , 'resource_id', 'id');
    }
}