<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Schools\Camp;

class ActivityMap extends EntityMap
{
    protected $table = 'activity';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "activity.deleted_at";

    public function difficulty(Activity $activity){
        return $this->belongsTo($activity, Difficulty::class, 'difficulty_id', 'id');
    }

    public function creator(Activity $activity){
        return $this->belongsTo($activity, User::class, 'creator_id', 'id');
    }

    public function translations(Activity $activity){
        return $this->hasMany($activity, ActivityTranslation::class , 'activity_id', 'id');
    }

    public function tasks(Activity $activity){
        return $this->belongsToMany($activity, Task::class , 'activity_task', 'activity_id', 'task_id');
    }

    public function tags(Activity $activity){
        return $this->belongsToMany($activity, Tag::class , 'activity_tag', 'activity_id', 'tag_id');
    }

    public function screenshots(Activity $activity){
        return $this->hasMany($activity, ActivityScreenshots::class , 'activity_id', 'id');
    }

    public function camps(Activity $activity){
        return $this->belongsToMany($activity, Camp::class , 'camp_activity', 'activity_id', 'id');
    }

    public function activityUnlockable(Activity $activity){
        return $this->hasOne($activity, ActivityUnlockable::class , 'activity_id', 'id');
    }

    public function leaningPaths(Activity $activity){
        return $this->belongsToMany($activity, LearningPath::class , 'learning_path_activity', 'activity_id', 'id');
    }

    public function tasksUsersCurrentSteps(Activity $activity){
        return $this->hasMany($activity, TaskCurrentStep::class, 'activity_id');
    }
}