<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class MissionSavingsMap extends EntityMap {

    protected $table = 'mission_saving';
	public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "mission_saving.deleted_at";

    public function mission(MissionSavings $missionSavings)
    {
        return $this->belongsTo($missionSavings, Mission::class , 'Mission_Id', 'id');
    }

    public function user(MissionSavings $missionSavings)
    {
        return $this->belongsTo($missionSavings, User::class , 'User_Id', 'id');
    }

}