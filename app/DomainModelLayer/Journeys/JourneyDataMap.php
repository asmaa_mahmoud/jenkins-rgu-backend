<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class JourneyDataMap extends EntityMap {

    protected $table = 'journey_data';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "journey_data.deleted_at";


}