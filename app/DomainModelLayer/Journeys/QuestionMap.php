<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Choice;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Journeys\EmulatorType;
use App\DomainModelLayer\Journeys\QuestionTranslation;
use App\DomainModelLayer\Journeys\DropdownQuestion;
use App\ApplicationLayer\Journeys\QuestionTask;
use App\DomainModelLayer\Journeys\McqQuestion;
use App\DomainModelLayer\Journeys\MatchQuestion;

class QuestionMap extends EntityMap {

    protected $table = 'question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "question.deleted_at";


    public function quiz(Question $question)
    {
        return $this->belongsTo($question, Quiz::class , 'quiz_id', 'id');
    }
    public function emulator_type(Question $question)
    {
        return $this->belongsTo($question, EmulatorType::class , 'emulator_type_id', 'id');
    }

    public function choices(Question $question)
    {
        return $this->hasMany($question, Choice::class , 'question_id', 'id');
    }

    public function translations(Question $question)
    {
        return $this->hasMany($question, QuestionTranslation::class , 'question_id', 'id');
    }

    public function question_task(Question $question)
    {
        return $this->hasone($question, QuestionTask::class , 'question_id', 'id');
    }


    public function dropdown_question(Question $question)
    {
        return $this->hasone($question,DropdownQuestion::class, 'question_id' , 'id');
    }

    public function mcq_question(Question $question)
    {
        return $this->hasone($question,McqQuestion::class, 'question_id', 'id');

    }

    public function dragQuestion(Question $question)
    {
        return $this->hasone($question,DragQuestion::class, 'question_id' , 'id');
    }

    public function matchQuestion(Question $question)
    {
        return $this->hasOne($question,MatchQuestion::class, 'question_id', 'id');
    }


}