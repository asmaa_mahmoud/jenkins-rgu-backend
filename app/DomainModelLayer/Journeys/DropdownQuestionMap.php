<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\DropdownQuestion;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\Dropdown;
use App\DomainModelLayer\Journeys\DropdownQuestionTranslation;

class DropdownQuestionMap extends EntityMap {

    protected $table = 'dropdown_question';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "dropdown_question.deleted_at";



     public function question(DropdownQuestion $dropdownQuestion){

        return $this->belongsTo($dropdownQuestion, Question::class , 'question_id', 'id');
    }
   

    public function dropdown(DropdownQuestion $dropdownQuestion)
    {
        return $this->hasMany($dropdownQuestion,Dropdown::class, 'dropdown_question_id', 'id');
    }


    public function dropdown_question_translation(DropdownQuestion $dropdownQuestion)
    {
        return $this->hasMany($dropdownQuestion, DropdownQuestionTranslation::class, 'dropdown_question_id', 'id');
    }


 
}