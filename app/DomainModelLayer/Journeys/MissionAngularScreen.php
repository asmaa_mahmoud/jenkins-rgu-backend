<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 15/11/2018
 * Time: 7:23 PM
 */

namespace App\DomainModelLayer\Journeys;


use Analogue\ORM\Entity;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;

class MissionAngularScreen extends Entity
{
    public function __construct(MissionScreensDto $missionScreensDto = null, MissionAngular $mission)
    {
        if($missionScreensDto != null && $mission != null)
        {
            $this->screenshot_url = $missionScreensDto->url;
            $this->mission = $mission;
        }
    }

    public function getUrl()
    {
        return $this->screenshot_url;
    }
}