<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ConfidenceTranslationMap extends EntityMap {

    protected $table = 'confidence_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "confidence_translation.deleted_at";


}