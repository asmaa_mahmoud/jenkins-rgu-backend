<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;


class Dropdown extends Entity
{

	public function getId()
    {
        return $this->id;
    }

    public function getDropdownQuestion()
    {
        return $this->dropdown_question;
    }

    public function getDropdownChoice()
    {
        return $this->dropdown_choice;
    }

    public function getRightExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getRightExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getRightExplanation();
        }
        return $english_translation;
    }

    public function getWrongExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getWrongExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getWrongExplanation();
        }
        return $english_translation;
    }

    public function getChoices()
    {
        return $this->dropdown_choice;
    }


}
