<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class Level extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getTranslation(){
        return $this->translations;
    }

}
