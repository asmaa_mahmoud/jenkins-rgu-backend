<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class ActivityScreenshotsMap extends EntityMap
{
    protected $table = 'activity_screenshots';
    public $timestamps = true;

    public function defaultActivity(ActivityScreenshots $activityScreenshots){
        return $this->belongsTo($activityScreenshots, DefaultActivity::class, 'default_activity_id', 'id');
    }

    public function activity(ActivityScreenshots $activityScreenshots){
        return $this->belongsTo($activityScreenshots, Activity::class, 'activity_id', 'id');
    }

}