<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class DropdownChoice extends Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getTranslation()
    {
        return $this->dropdown_choice_translation;
    }



    public function getChoice()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->dropdown_choice_translation;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getChoice();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getChoice();
        }
        return $english_translation;
    }

    public function getExplanation()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->dropdown_choice_translation;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getExplanation();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getExplanation();
        }
        return $english_translation;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getIsModelAnswer()
    {
        return $this->is_correct;
    }

   
}
