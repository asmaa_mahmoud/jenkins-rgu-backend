<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;

class QuizTranslationMap extends EntityMap {

    protected $table = 'quiz_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "quiz_translation.deleted_at";


}