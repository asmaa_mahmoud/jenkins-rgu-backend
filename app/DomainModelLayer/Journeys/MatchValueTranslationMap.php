<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class MatchValueTranslationMap extends EntityMap {

    protected $table = 'match_value_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "match_value_translation.deleted_at";



    public function matchValue(MatchValueTranslation $matchValueTranslation)
    {
        return $this->belongsTo($matchValueTranslation, MatchValue::class, 'match_value_id','id');
    }
 
}