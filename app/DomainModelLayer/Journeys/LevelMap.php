<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;

class LevelMap extends EntityMap
{
    protected $table = 'level';
    public $timestamps = false;

    public function translations(Level $level)
    {
        return $this->hasMany($level, LevelTranslation::class , 'level_id', 'id');
    }

}