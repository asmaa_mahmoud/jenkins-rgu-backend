<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;

class MissionHtmlSections extends Entity
{

    public function getId(){
        return $this->id;
    }

    public function getOrder()
    {
       return $this->order;
    }

    public function getSteps()
    {
       return $this->steps;
    }

    
    public function getTranslations(){
        return $this->translations;
    }

    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    public function getType(){
        if($this->normal){
            return 'normal';
        }else{
            return 'guide';
        }
    }

}
