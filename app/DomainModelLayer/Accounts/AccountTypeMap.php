<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\Plan;

class AccountTypeMap extends EntityMap {

    protected $table = 'account_type';
    public $softDeletes = true;
    protected $deletedAtColumn = "account_type.deleted_at";

    public function accounts(AccountType $accounttype)
    {
        return $this->hasMany($accounttype, Account::class , 'account_type_id', 'id');
    }

    public function plans(AccountType $accounttype)
    {
        return $this->hasMany($accounttype, Plan::class, 'account_type_id','id');
    }

}