<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldParameterTranslation extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

}