<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\FreeSubscription;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\DomainModelLayer\Accounts\SubscriptionHistory;
use App\DomainModelLayer\Accounts\InvitationSubscription;
use App\DomainModelLayer\Schools\DistributorSubscription;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\DomainModelLayer\Schools\SchoolCharge;

class SubscriptionMap extends EntityMap {

    protected $table = 'subscription';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "subscription.deleted_at";


    public function plan(Subscription $subscription)
    {
        return $this->belongsTo($subscription, PlanHistory::class , 'plan_history_id', 'id');
    }

    public function journeys(Subscription $subscription)
    {
        return $this->belongsToMany($subscription, Journey::class , 'subscription_journey','subscription_id','journey_id');
    }

    public function account(Subscription $subscription)
    {
        return $this->belongsTo($subscription, Account::class, 'account_id','id');
    }

    public function freeSubscriptions(Subscription $subscription)
    {
        return $this->hasMany($subscription, FreeSubscription::class, 'subscription_id','id');
    }

    public function subscriptionHistory(Subscription $subscription)
    {
        return $this->hasMany($subscription, SubscriptionHistory::class, 'subscription_id','id');
    }

    public function stripeSubscriptions(Subscription $subscription)
    {
        return $this->hasMany($subscription, StripeSubscription::class, 'subscription_id','id');
    }

    public function invitationSubscriptions(Subscription $subscription)
    {
        return $this->hasMany($subscription, InvitationSubscription::class, 'subscription_id','id');
    }

    public function subscriptionItems(Subscription $subscription)
    {
        return $this->hasMany($subscription, SubscriptionItem::class, 'subscription_id','id');
    }

    public function distributorSubscriptions(Subscription $subscription)
    {
        return $this->hasMany($subscription, DistributorSubscription::class, 'subscription_id','id');
    }

    public function schoolCharge(Subscription $subscription)
    {
        return $this->hasOne($subscription, SchoolCharge::class , 'subscription_id', 'id');
    }

}