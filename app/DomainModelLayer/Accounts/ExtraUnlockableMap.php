<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Extra;
use App\DomainModelLayer\Accounts\ExtraUnlockable;
use App\DomainModelLayer\Journeys\Unlockable;

class ExtraUnlockableMap extends EntityMap {

    protected $table = 'extra_unlockable';
    public $timestamps = true;

    public function extra(ExtraUnlockable $extraUnlockable)
    {
        return $this->belongsTo($extraUnlockable, Extra::class , 'extra_id', 'id');
    }

    public function unlockable(ExtraUnlockable $extraUnlockable)
    {
        return $this->belongsTo($extraUnlockable, Unlockable::class , 'unlockable_id', 'id');
    }

}