<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 27/11/2018
 * Time: 4:12 PM
 */

namespace App\DomainModelLayer\Accounts;


use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;

class UserNotificationMap extends EntityMap
{
    protected $table = 'user_notifications';
    protected $casts = [
        'data' => 'array',
        'read_at' => 'datetime',
    ];
    public $timestamps = true;

    public function user(UserNotification $notification)
    {
        return $this->belongsTo($notification, User::class, 'user_id', 'id');
    }
}