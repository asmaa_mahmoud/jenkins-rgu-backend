<?php

namespace App\DomainModelLayer\Accounts;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Professional\DatabaseNotification;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Professional\LearningPathUser;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Schools\CampUser;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\RankLevel;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\UserRole;
use App\DomainModelLayer\Journeys\ CampusUserStatus;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Schools\GroupMember;
use App\Helpers\UUID;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Event;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;
use NotificationChannels\WebPush\HasPushSubscriptions;


class User extends Entity
{
    use Notifiable;
    use HasPushSubscriptions;
    //region Getters & Setters

    public function __construct($userDto = null, Account $account, $isSuperAdmin = false)
    {
        if($userDto!= null && $account != null)
        {
            $this->first_name = $userDto->fname;
            $this->last_name = $userDto->lname;
            $this->username = $userDto->username;
            $this->gender = 0;
            $this->age = $userDto->age;
            if(isset($userDto->email))
                $this->email = $userDto->email;
            if(isset($userDto->user_lti_id))
                $this->user_lti_id = $userDto->user_lti_id;
            if(isset($userDto->supervisorEmail))
                $this->supervisor_email = $userDto->supervisorEmail;
            $this->password = bcrypt($userDto->password);
            if($userDto->image_link != null)
            {
                $this->profile_image_url = $userDto->image_link;
            }else{
                $this->profile_image_url = 'https://s3-us-west-2.amazonaws.com/robogarden-professional/web+images/profile-image.png';
            }
            $this->account = $account;
            $this->roles = new EntityCollection;
            
            if(!$isSuperAdmin){
                $this->positions = new EntityCollection;
                $this->studentpositions = new EntityCollection;
                $this->userscore = new UserScore($this,0,0);
                $this->notifications = new EntityCollection;
                if(isset($userDto->social_id)){
                    if($userDto->social_id != null && $userDto->social_track != null)
                    {
                        $this->social_id = $userDto->social_id;
                        $this->social_track = $userDto->social_track;
                    }else{
                        $this->social_id = null;
                        $this->social_track = null;
                    }
                }
                $this->groupMembers = new EntityCollection;
                $this->grades = new EntityCollection;
                $this->modelAnswersUnlocked = new EntityCollection;
            }
        }
    }

    public function getKey()
    {
        return $this->id;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setAccountId($id)
    {
        $this->account_id = $id;
    }

    public function getSocialId()
    {
        return $this->social_id;
    }

    public function setSocialId($socialId)
    {
        $this->social_id = $socialId;
    }

    public function getSocialTrack()
    {
        return $this->social_track;
    }

    public function setSocialTrack($socialTrack)
    {
        $this->social_track = $socialTrack;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getFname()
    {
        return $this->getFirstName();
    }

    public function setFirstName($FirstName)
    {
        $this->first_name = $FirstName;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getLname()
    {
        return $this->getLastName();
    }

    public function setLastName($LastName)
    {
        $this->last_name = $LastName;
    }

    public function getName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($Email)
    {
        $this->email = $Email;
    }

    public function getSupervisorEmail()
    {
        return $this->supervisor_email;
    }

    public function setSupervisorEmail($supervisorEmail)
    {
         $this->supervisor_email = $supervisorEmail;
    }
    
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($Password)
    {
        $this->password = $Password;
    }

    public function getImage()
    {
        return $this->profile_image_url;
    }

    public function setImage($ImageLink)
    {
        $this->profile_image_url = $ImageLink;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($Gender)
    {
        $this->gender = $Gender;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($Username)
    {
        $this->username = $Username;
    }

    public function getTokenUrl()
    {
        return $this->token_url;
    }

    public function setTokenUrl($tokenurl)
    {
        $this->token_url = $tokenurl;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount(Account $account)
    {
        $this->account = $account;
    }

    public function getAccountType()
    {
        return $this->account->getAccountType()->getName();
    }

    public function getAccountTypeId()
    {
        return $this->account->getAccountType()->getId();
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function addRole(Role $role)
    {
        return $this->roles->push($role);
    }

    public function removeRole(Role $role)
    {
        return $this->roles->remove($role);
    }

    public function addNotification(Notification $notification)
    {
        return $this->notifications->push($notification);
    }

    public function addUserNotification(UserNotification $notification)
    {
        return $this->userNotifications->push($notification);
    }

    public function addPosition(UserPosition $userposition)
    {
        return $this->positions->push($userposition);
    }

    public function removePosition(UserPosition $userposition)
    {
        return $this->positions->remove($userposition);
    }

    public function getExperience(){
        return $this->userscore->getExperience();
    }

    public function getCoins(){
        return $this->userscore->getCoins();
    }

    public function getScores()
    {
        return ['xp'=>$this->getExperience(),'coins'=>$this->getCoins()];
    }

    public function getJourneyScore($journey){
        $tasks = $journey->getTasksIds();
        $progress = $this->progress;
        $totalScore = 0;
        if($progress != null) {
            foreach ($progress as $score) {
                $task = $score->getTask();
                if(array_search($task->id, $tasks) !== false){
                    $totalScore += $score->getScore();
                }
            }
        }
        return $totalScore;
    }

    public function getStudentScores()
    {
        return ['xp'=>$this->getExperience(),'coins'=>$this->getCoins(),'rank'=>$this->getUserRank(),'level'=>$this->getUserLevel()];
    }

    public function getStudentScoresOld(){
        $progress = $this->studentprogress;
        $mission_score = 0;
        $quiz_score = 0;
        if($progress != null)
        {
            foreach ($progress as $score) {
                $task = $score->getTask();
                if($task->getMissions()->count() > 0)
                {
                    $mission_score += $score->getScore();
                }
                else
                {
                    $quiz_score += $score->getScore();
                }
            }
        }
        return ['mission_scores'=>$mission_score,'quiz_scores'=>$quiz_score];
    }

    public function getStudentProgress(){
        return $this->studentprogress;
    }

    public function getNotifications()
    {
        return $this->notifications;
    }

    public function getUserNotifications()
    {
        return $this->userNotifications;
    }

    public function getPosition($journey)
    {
        $positions = [];

        foreach ($this->positions as $position) {
            if($position->getJourney()->getId() == $journey->getId())
                $positions[] = $position;
        }

        return $positions;
    }

    public function getTotalProgress(){
        return $this->progress;
    }

    public function getProgress(){
        $ids = [];
        $progress = $this->progress;
        if($progress != null) {
            foreach ($progress as $score) {
                $task = $score->getTask();
                $ids[] = $task->id;
            }
        }
        return $ids;
    }

    public function getJourneyProgress($journeyDto)
    {
        $missions = 0;
        $quizzes = 0;
        $mainQuizzes = 0;
        $tutorials = 0;
        $progress = $this->getTotalProgress();
        
        if($progress != null)
        {
            foreach ($progress as $score) {
                if($score->getJourney()->getId() == $journeyDto->Id)
                {
                    $task = $score->getTask();
                    if($score->getFirstSuccess() != null){
                        if(($task->getMissions()->count() > 0)) {
                            $missions++;
                        }
                        else if($task->getMissionsHtml()->count() > 0) {
                            $missions++;
                        }
                        else {
                            $quizzes++;
                            if($task->getQuizzes()->first()->getType()->getName() == "tutorial")
                                $tutorials++;
                            else if($task->getQuizzes()->first()->getType()->getName() == "quiz")
                                $mainQuizzes++;
                        }
                    }
                }
            }
        }

        return ["journey_name" => $journeyDto->Name, "journey_icon" => $journeyDto->CardIconURL, "missions" => $missions, "quizzes" => $quizzes, "tutorials" => $tutorials, "mainQuizzes" => $mainQuizzes];
    }

    public function getGroupMembers(){
        $groupMembersArray = array();
        foreach ($this->groupMembers as $groupMember){
            if($groupMember->getGroup()->getIsDefault())
                array_push($groupMembersArray,$groupMember);
        }
        return $groupMembersArray;
    }

    public function addGroupMember(GroupMember $groupMember){
        $this->groupMembers->push($groupMember);
    }

    public function getAllGroupMembers(){
        return $this->groupMembers;
    }
    public function getAllCampusUsers(){
        return $this->campusUsers;
    }

    public function getClassRooms(){
        $groupMembers = $this->groupMembers;
        $classrooms = array();
        foreach ($groupMembers as $groupMember){
            if($groupMember->getGroup()->getIsDefault())
                array_push($classrooms,$groupMember->getGroup()->getClassroom());
        }
        return $classrooms;
    }

    public function getClassRoomScores(){
        $classroom = $this->getBasicClassRoom();
        $currentStudentScore = 0;
        if($classroom != null){
            $defaultGroup = $classroom->getDefaultGroup();
            $courses = $defaultGroup->getCourses();
            foreach ($courses as $course){
                $data = $this->getCourseScores($course->getId());
                $currentStudentScore += $data['mission_scores'] + $data['quiz_scores'];
            }
        }
        return $currentStudentScore;
    }

    public function getBasicClassRoom(){
        $groupMember = $this->groupMembers->first();

        if ($groupMember != null){
            
            return $groupMember->getGroup()->getClassroom();
        }
    }

    public function getRolesInClassRoom($classroomId){
        $groupMembers = $this->groupMembers;
        $roles = array();
        foreach ($groupMembers as $groupMember){
            if($groupMember->getGroup()->getClassroom()->getId() == $classroomId && $groupMember->getGroup()->getIsDefault()){
                array_push($roles,$groupMember->getRole());
            }
        }
        return $roles;
    }

    public function hasRoleInClassRoom($classroomId,$roleName){
        $roles = $this->getRolesInClassRoom($classroomId);
        $hasRole = false;
        foreach ($roles as $role){
            if($role->getName() == $roleName)
                $hasRole = true;
        }
        return $hasRole;
    }

    public function addGrade(Grade $grade){
         $this->grades->push($grade);
    }

    public function getGrade(){
         return $this->grades->first();
    }

    public function removeGrade(Grade $grade){
        $this->grades->remove($grade);
    }

    public function is_student()
    {
        if($this->getAccount()->getAccountType()->getName() != "School")
            return false;

        if($this->getAccount()->getSchool() == null)
            return false;

        $roles = $this->getRoles();
        foreach ($roles as $role){
            if($role->getName() == "student")
                return true;
        }
        return false;
    }

    public function is_teacher()
    {
        if($this->getAccount()->getAccountType()->getName() != "School")
            return false;
        if($this->getAccount()->getSchool() == null)
            return false;
        $roles = $this->getRoles();
        foreach ($roles as $role){
            if($role->getName() == "teacher")
                return true;
        }
        return false;
    }

    public function is_schoolAdmin()
    {
        if($this->getAccount()->getAccountType()->getName() != "School")
            return false;
        if($this->getAccount()->getSchool() == null)
            return false;
        $roles = $this->getRoles();
        foreach ($roles as $role){
            if($role->getName() == "school_admin")
                return true;
        }
        return false;
    }

    public function hasCourse($course_id)
    {
        $group_members = $this->groupMembers;
        foreach ($group_members as $group_member) {
            $group = $group_member->getGroup();
            foreach ($group->getCourses() as $course) {
                if($course->getId() == $course_id)
                    return true;
            }
        }
        return false;
    }

    public function getCourses($search = null)
    {
        $courses = [];
        $group_members = $this->groupMembers;
        foreach ($group_members as $group_member) {
            $group = $group_member->getGroup();
            if($group->getIsDefault())
            {
                foreach ($group->getCourses() as $course) {
                    if($search == null || strpos(strtolower($course->getName()),strtolower($search)) !== false)
                        $courses[] = $course;
                }
            }

        }
        return $courses;
    }

    public function getProgressPercentage(){
        $courses = $this->getBasicClassRoom()->getDefaultGroup()->getCourses();
        $tasksDone = 0;
        $totalTasks = 0;
        foreach ($courses as $course){
            $tasksDone += $this->getCourseScores($course->getId())['task_count'];
            $totalTasks += $course->getTasksCount();
        }
        if ($totalTasks == 0)
            return 0;
        return ($tasksDone/$totalTasks)*100;
    }

    public function getJourneyProgressPercentage($journey){
        $progress = $this->getTotalProgress();
        $tasks_ids = $journey->getTasksIds();
        $totalTasks = count($tasks_ids);
        $tasksDone = 0;
        foreach($progress as $score){
            if($score->getFirstSuccess() != null) {
                if (in_array($score->getTaskId(), $tasks_ids)) {
                    $tasksDone++;
                }
            }
        }
        if ($totalTasks == 0 || count($progress) == 0)
            return 0;
        return ($tasksDone/$totalTasks)*100;
    }

    public function getCourseScores($course_id)
    {
        $progress = $this->studentprogress;
        $mission_score = 0;
        $quiz_score = 0;
        $task_count = 0;
        $missions_solved = 0;
        $missions_failed = 0;
        $percentage_progress = 0;
        if($progress == null)
            return ['percentage_progress' => $percentage_progress, 'mission_scores'=>$mission_score,'quiz_scores'=>$quiz_score,"task_count"=>$task_count,"missions_solved"=>$missions_solved,"missions_failed"=>$missions_failed];
        foreach ($progress as $score) {
            if($score->getCourse()->getId() == $course_id)
            {
                $task = $score->getTask();
                if($task->getMissions()->count() > 0) {
                    $mission_score += $score->getScore();
                }
                else {
                    $quiz_score += $score->getScore();
                }
                if($score->getFirstSuccess() == null)
                    $missions_failed++;

                else
                    $missions_solved++;

                $task_count++;
                $percentage_progress = ceil(($missions_solved/$score->getCourse()->getTasksCount())*100);
            }
        }
        return ['total_progress' => $percentage_progress, 'mission_scores'=>$mission_score,'quiz_scores'=>$quiz_score,"task_count"=>$task_count,"missions_solved"=>$missions_solved,"missions_failed"=>$missions_failed];
    }

    public function getCourseDataForTask(Course $course,Task $task)
    {
        $type = '';
        if($task->getMissions()->first() != null)
            $type = 'mission';
        else if($task->getMissionsHtml()->first() != null)
            $type = 'mission';
        else
            $type = $task->getQuizzes()->first()->getType()->getName();
        $progress = $this->studentprogress;
        if ($progress == null)
            return ['id' => $task->getId(),'type'=>$type, 'noOfTrials' => 0, "successTrialNumber" => null, "success" => false];

        else {
            $taskFound = false;
            foreach ($progress as $score) {
                if ($score->getCourse()->getId() == $course->getId()) {
                    if ($score->getTask()->getId() == $task->getId()) {
                        return ['id' => $task->getId(),'type'=>$type, 'noOfTrials' => $score->getNo_of_trails(), 'successTrialNumber' => $score->getFirstSuccess(), "success" => $score->isSuccess()];
                    }

                }

            }
            if (!$taskFound)
                return ['id' => $task->getId(),'type'=>$type, 'noOfTrials' => 0, "successTrialNumber" => null, "success" => false];
        }
    }

    public function getCoursePosition($course_id)
    {
        $positions = [];

        foreach ($this->studentpositions as $position) {
            if($position->getCourse()->getId() == $course_id)
                $positions[] = $position;
        }

        return $positions;
    }

    public function getRankInClassroom(Classroom $classroom){
        $defaultGroup = $classroom->getDefaultGroup();
        $courses = $defaultGroup->getCourses();
        $students = $defaultGroup->getStudents();

        $currentStudentScore = 0;
        foreach ($courses as $course){
            $data = $this->getCourseScores($course->getId());
            $currentStudentScore += $data['mission_scores']+$data['quiz_scores'];
        }

        $studentRank = 1;
        foreach ($students as $student) {
            $totalScore= 0;
            if($this->getId() != $student->getId()){
                foreach ($courses as $course){
                    $data = $student->getCourseScores($course->getId());
                    $totalScore += $data['mission_scores']+$data['quiz_scores'];
                }
                if($totalScore>$currentStudentScore)
                    $studentRank++;
            }

        }
        return $studentRank;
    }

    public function getLanguageCode()
    {
        if($this->language != null)
            return $this->language->getCode();
        else
            return 'en';
    }

    public function getLanguage(){
        return $this->language;
    }

    public function setLanguage(Language $language)
    {
        $this->language = $language;
    }

    public function setMailSubscription($action)
    {
        $this->mail_subscription = $action;
    }

    public function getModelAnswersUnlocked(){
        return $this->modelAnswersUnlocked;
    }

    public function setUserScore(UserScore $userScore){
        $this->userscore = $userScore;
    }

    public function getUserScore(){
        return $this->userscore;
    }

    public function getWorldDetails(){
        return $this->worldDetails;
    }

    public function getWorldScore(){
        return $this->worldScore;
    }

    public function getLastUserRole(){
        if($this->getAccount()->getAccountType()->getName() != "School")
            return false;
        $roles = $this->getRoles();
        foreach ($roles as $role){
            return $role->getName();
        }
        return false;
    }

    public function getCamps(){
        return $this->camps;
    }

    public function addCampUser(CampUser $campUser){
        $this->campUser->push($campUser);
    }

    public function getCampProgressPercentage(){
        $camps = $this->getCamps();
        $tasksDone = 0;
        $totalTasks = 0;
        foreach ($camps as $camp){
            $tasksDone += $this->getCampScores($camp->getId())['task_count'];
            $totalTasks += $camp->getDefaultTasksCount();
        }
        if ($totalTasks == 0)
            return 0;

        return ($tasksDone/$totalTasks)*100;
    }

    public function getCampProgress(){
        return $this->campProgress;
    }

    public function getCampusProgress(){
        return $this->campusProgress;
    }

    public function getCampScore(){
        $progress = $this->getCampProgress();
        $totalScore = 0;
        if($progress == null)
            return 0;

        foreach ($progress as $score) {
            $totalScore += intval($score->getScore() ? $score->getScore() : 0);
        }
        return $totalScore;
    }

    public function getCampScores($camp_id)
    {
        $progress = $this->getCampProgress();
        $mission_score = 0;
        $quiz_score = 0;
        $task_count = 0;
        $missions_solved = 0;
        $missions_failed = 0;
        $percentage_progress = 0;
        if($progress == null)
            return ['percentage_progress' => $percentage_progress, 'mission_scores' => $mission_score, 'quiz_scores' => $quiz_score,
                "task_count" => $task_count, "missions_solved" => $missions_solved, "missions_failed" => $missions_failed];

        foreach ($progress as $score) {
            if($score->getCamp()->getId() == $camp_id) {
                $task = $score->getTask();
                if($task->getMissions()->count() > 0) {
                    $mission_score += $score->getScore();
                }
                else {
                    $quiz_score += $score->getScore();
                }
                if($score->getFirstSuccess() == null)
                    $missions_failed++;

                else
                    $missions_solved++;

                $task_count++;
                $percentage_progress = ceil(($missions_solved/($score->getCamp()->getDefaultTasksCount() + $score->getCamp()->getTasksCount()))*100);
            }
        }
        return ['total_progress' => $percentage_progress, 'total_score' => ($mission_score + $quiz_score), 'mission_scores' => $mission_score, 'quiz_scores' => $quiz_score,
            "task_count" => $task_count, "missions_solved" => $missions_solved, "missions_failed" => $missions_failed];
    }

    public function getActivityCampDataForTask(Camp $camp, DefaultActivity $activity, Task $task)
    {
        $type = '';
        if($task->getMissions()->first() != null)
            $type = 'mission';
        else if($task->getMissionsHtml()->first() != null)
            $type = 'mission';
        else
            $type = $task->getQuizzes()->first()->getType()->getName();
        $progress = $this->getCampProgress();
        if ($progress == null)
            return ['id' => $task->getId(), 'type' => $type, 'noOfTrials' => 0, "successTrialNumber" => null, "success" => false];

        else {
            $taskFound = false;
            foreach ($progress as $score) {
                if ($score->getCamp()->getId() == $camp->getId()) {
                    if ($score->getDefaultActivity()->getId() == $activity->getId()) {
                        if($score->getTask()->getId() == $task->getId()){
                            return ['id' => $task->getId(), 'type' => $type, 'noOfTrials' => $score->getNoTrials(), 'successTrialNumber' => $score->getFirstSuccess(), "success" => $score->isSuccess()];
                        }
                    }
                }
            }
            if (!$taskFound)
                return ['id' => $task->getId(), 'type' => $type, 'noOfTrials' => 0, "successTrialNumber" => null, "success" => false];
        }
    }

    public function getRankInCamp(Camp $camp){
        $students = $camp->getStudents();
        $data = $this->getCampScores($camp->getId());
        $currentStudentScore = $data['mission_scores'] + $data['quiz_scores'];

        $studentRank = 1;
        foreach ($students as $student) {
            $totalScore = 0;
            if($this->getId() != $student->getId()){
                $data = $student->getCampScores($camp->getId());
                $totalScore += $data['mission_scores'] + $data['quiz_scores'];
                if($totalScore > $currentStudentScore)
                    $studentRank++;
            }
        }
        return $studentRank;
    }

    public function getLearningPaths(){
        return $this->learningPaths;
    }

    public function addLearningPathUser(LearningPathUser $learningPathUser){
        $this->learningPathUser->push($learningPathUser);
    }

    public function getLearningPathProgress(){
        return $this->learningPathProgress;
    }

    public function getLearningPathScore(){
        $progress = $this->getLearningPathProgress();
        $totalScore = 0;
        if($progress == null)
            return 0;

        foreach ($progress as $score) {
            $totalScore += intval($score->getScore() ? $score->getScore() : 0);
        }
        return $totalScore;
    }

    public function getLearningPathScores($learningPath_id)
    {
        $progress = $this->getLearningPathProgress();
        $mission_score = 0;
        $quiz_score = 0;
        $task_count = 0;
        $missions_solved = 0;
        $missions_failed = 0;
        $percentage_progress = 0;
        if($progress == null)
            return ['percentage_progress' => $percentage_progress, 'mission_scores' => $mission_score, 'quiz_scores' => $quiz_score,
                "task_count" => $task_count, "missions_solved" => $missions_solved, "missions_failed" => $missions_failed];

        foreach ($progress as $score) {
            if($score->getlearningPath()->getId() == $learningPath_id) {
                $task = $score->getTask();
                if($task->getMissions()->count() > 0) {
                    $mission_score += $score->getScore();
                }
                else {
                    $quiz_score += $score->getScore();
                }
                if($score->getFirstSuccess() == null)
                    $missions_failed++;

                else
                    $missions_solved++;

                $task_count++;
                $percentage_progress = ceil(($missions_solved/($score->getlearningPath()->getDefaultTasksCount() + $score->getlearningPath()->getTasksCount()))*100);
            }
        }
        return ['total_progress' => $percentage_progress, 'total_score' => ($mission_score + $quiz_score), 'mission_scores' => $mission_score, 'quiz_scores' => $quiz_score,
            "task_count" => $task_count, "missions_solved" => $missions_solved, "missions_failed" => $missions_failed];
    }

    public function getCampusRounds(){
        return $this->campusRounds;
    }

    public function getUserLTIId(){
        return $this->user_lti_id;
    }

    public function setUserLTIId($user_lti_id){
        $this->user_lti_id=$user_lti_id;
    }

    public function receivesBroadcastNotificationsOn()
    {
        return 'users.'.$this->id;
    }


    public function getTasksCurrentSteps(){
        return $this->tasksCurrentSteps;
    }
    public function setTasksCurrentSteps($currentSteps){
        $this->tasksCurrentSteps = $currentSteps;
    }
    public function isStudent(){
        $isStudent = false;
        foreach ($this->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        return $isStudent;
    }

    public function isTeacher(){
        $isTeacher = false;
        foreach ($this->getRoles() as $role) {
            if($role->getName() == 'teacher')
                $isTeacher = true;
        }
        return $isTeacher;
    }

    public function isAdmin(){
        $isAdmin = false;
        foreach ($this->getRoles() as $role) {
            if($role->getName() == 'school_admin')
                $isAdmin = true;
        }
        return $isAdmin;
    }

    public function isSuperAdmin(){
        $isSuperAdmin = false;
        foreach ($this->getRoles() as $role) {
            if($role->getName() == 'super_admin')
                $isSuperAdmin = true;
        }
        return $isSuperAdmin;
    }


    public function getShowDayTip()
    {
        return $this->showDayTip;
    }

    public function setShowDayTip($showDayTip)
    {
        $this->showDayTip = $showDayTip;
    }
    
    public function getUserRank(){
        $rank = $this->getUserScore()->rankLevel->rank->getName();
        return $rank;
    }

    public function getUserLevel(){
        $level = $this->getUserScore()->rankLevel->level->level;
        return $level;
    }

    public function showBlocks($campus_id)
    {
        $campusUserStatus = $this->campusUserStatus;
       foreach($campusUserStatus as $status)
        { 
              if($status->getCampus()->getId()==$campus_id)
            {
                return !$status->getHideBlocks();
            }
        }
        return true;
        
    }
    
    public function getUserLevelName(){
        $level = $this->getUserScore()->rankLevel->level->getName();
        return $level;
    }


}	
