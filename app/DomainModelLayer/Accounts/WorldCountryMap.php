<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldCountryMap extends EntityMap
{
    protected $table = 'world_countries';
    public $softDeletes = false;
    public $timestamps = false;

    public function translations(WorldCountry $worldCountry){
        return $this->hasMany($worldCountry, WorldCountryTranslation::class , 'country_id', 'id');
    }

}