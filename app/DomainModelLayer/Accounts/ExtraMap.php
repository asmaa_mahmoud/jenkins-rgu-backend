<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\ExtraTranslation;
use App\DomainModelLayer\Accounts\ExtraUnlockable;
use App\DomainModelLayer\Accounts\Extra;

class ExtraMap extends EntityMap
{
    protected $table = 'extra';
    public $timestamps = true;

    public function translations(Extra $extra)
    {
        return $this->hasMany($extra, ExtraTranslation::class , 'extra_id', 'id');
    }

    public function extra_unlockable(Extra $extra)
    {
        return $this->hasOne($extra, ExtraUnlockable::class , 'extra_id', 'id');
    }
}