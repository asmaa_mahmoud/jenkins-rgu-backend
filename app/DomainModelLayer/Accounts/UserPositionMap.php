<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\UserPosition;

class UserPositionMap extends EntityMap {

    protected $table = 'user_position';
	public $timestamps = true;
//	public $softDeletes = true;
//    protected $deletedAtColumn = "user_position.deleted_at";


    public function user(UserPosition $userposition)
    {
        return $this->belongsTo($userposition, User::class , 'user_id', 'id');
    }

    public function journey(UserPosition $userposition)
    {
        return $this->belongsTo($userposition, Journey::class , 'journey_id', 'id');
    }


}