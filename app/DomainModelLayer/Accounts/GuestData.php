<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class GuestData extends Entity
{
    public function __construct($ipAddress, $coins, $xps, $name = null){
        $this->name = $name;
        $this->ip_address = $ipAddress;
        $this->coins = $coins;
        $this->xp = $xps;
    }

    public function getId(){
        return $this->id;
    }

    public function getIpAddress(){
        return $this->ip_address;
    }

    public function setIpAddress($ip_address){
        $this->ip_address = $ip_address;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getCoins(){
        return $this->coins;
    }

    public function setCoins($coins){
        $this->coins = $coins;
    }

    public function getXp(){
        return $this->xp;
    }

    public function setXp($xp){
        $this->xp = $xp;
    }


}