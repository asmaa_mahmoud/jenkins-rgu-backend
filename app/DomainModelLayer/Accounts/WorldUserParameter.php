<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldUserParameter extends Entity
{
    public function __construct(User $user, WorldParameter $parameter, $weight){
        $this->user = $user;
        $this->parameter = $parameter;
        $this->weight = $weight;
    }

    public function getUser(){
        $this->user;
    }

    public function setUser($user){
        $this->user = $user;
    }

    public function getParameter(){
        return $this->parameter;
    }

    public function setParameter($parameter){
        $this->parameter = $parameter;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function setWeight($weight){
        $this->weight = $weight;
    }
}