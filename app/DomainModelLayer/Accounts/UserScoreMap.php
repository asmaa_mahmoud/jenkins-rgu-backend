<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/10/17
 * Time: 5:21 PM
 */

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Journeys\RankLevel;

class UserScoreMap extends EntityMap
{

    protected $table = 'user_score';
    public $timestamps = true;
//	public $softDeletes = true;
//    protected $deletedAtColumn = "user_position.deleted_at";


    public function user(UserScore $userScore)
    {
        return $this->belongsTo($userScore, User::class, 'user_id', 'id');
    }

    public function rankLevel(UserScore $userScore)
    {
        return $this->belongsTo($userScore, RankLevel::class, 'rank_level_id', 'id');
    }
    

}