<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldMatchMap extends EntityMap
{
    protected $table = 'world_match';
    public $softDeletes = false;
    public $timestamps = false;

    public function firstCountry(WorldMatch $worldMatch){
        return $this->belongsTo($worldMatch, WorldCountry::class , 'first_country_id', 'id');
    }

    public function secondCountry(WorldMatch $worldMatch){
        return $this->belongsTo($worldMatch, WorldCountry::class , 'second_country_id', 'id');
    }

}