<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;

class UserPosition extends Entity
{

    public function __construct(UserPositionDto $userpositiondto, User $user, Journey $journey)
    {
        $this->position_x = $userpositiondto->PositionX;
        $this->position_y = $userpositiondto->PositionY;
        $this->index = $userpositiondto->Index;
        $this->journey = $journey;
        $this->user = $user;
    }

    public function getPositionX()
    {
        return $this->position_x;
    }

    public function getPositionY()
    {
        return $this->position_y;
    } 

    public function getJourney()
    {
        return $this->journey;
    }

    public function getIndex()
    {
        return $this->index;
    }   
    
}
