<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\NotificationTranslation;

class PushNotificationMap extends EntityMap {

    protected $table = 'push_subscriptions';
	 public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "push_subscriptions.deleted_at";


    public function user(PushNotification $notification)
    {
        return $this->belongsTo($notification, User::class , 'user_id', 'id');
    }


}