<?php

namespace App\DomainModelLayer\Accounts;

use App\ApplicationLayer\Accounts\Dtos\StripeEventDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\UserRole;
use App\DomainModelLayer\Journeys\Journey;
use App\Helpers\UUID;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;


class StripeEvent extends Entity
{
	public function __construct(StripeEventDto $stripeEventDto = null)
    {
    	if($stripeEventDto!= null){

    		$this->title = $stripeEventDto->title;
            $this->body = $stripeEventDto->body;
            $this->type = $stripeEventDto->type;
    	}
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}