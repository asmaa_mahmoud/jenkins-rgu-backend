<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldCountryTranslationMap extends EntityMap
{
    protected $table = 'countries_translations';
    public $softDeletes = false;
    public $timestamps = false;


}