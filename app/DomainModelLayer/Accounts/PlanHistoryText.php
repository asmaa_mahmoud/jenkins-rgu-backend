<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\AccountDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\AccountType;
use App\DomainModelLayer\Accounts\Subscription;
use Analogue\ORM\EntityCollection;

class PlanHistoryText extends Entity
{

    
    /**
     * @return mixed
     */
    public function getPlanHistoryId()
    {
        return $this->plan_history_id;
    }

    /**
     * @return mixed
     */
    public function getLanguageCode()
    {
        $this->language_code;
    }
    
    /**
     * @return mixed
     */
    public function getText()
    {
        $this->text;
    }

    /**
     * @param mixed $Grade
     */
    public function getOrder()
    {
        $this->order;
    }
    
}
