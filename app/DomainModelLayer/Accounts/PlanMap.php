<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\AccountType;

class PlanMap extends EntityMap {

    protected $table = 'plan';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "plan.deleted_at";

    
    public function planhistory(Plan $plan)
    {
        return $this->hasMany($plan, PlanHistory::class , 'Plan_Id', 'Id');
    }

    public function accounttype(Plan $plan)
    {
        return $this->belongsTo($plan, AccountType::class , 'AccountType_Id', 'Id');
    }

    public function type(Plan $plan){
        return $this->belongsTo($plan, PlanType::class, 'type_id', 'id');
    }

}