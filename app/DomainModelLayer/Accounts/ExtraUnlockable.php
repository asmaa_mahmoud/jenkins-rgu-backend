<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Journeys\Unlockable;
use Analogue\ORM\Entity;
use LaravelLocalization;

class ExtraUnlockable extends Entity
{
    public function __construct(Extra $extra, Unlockable $unlockable)
    {
        $this->extra = $extra;
        $this->unlockable = $unlockable;
    }

    public function getUnlockable(){
        return $this->unlockable;
    }

    public function setUnlockable(Unlockable $unlockable){
        $this->unlockable = $unlockable;
    }

    public function getExtra(){
        return $this->extra;
    }

    public function setExtra(Extra $extra){
        $this->extra = $extra;
    }
}
