<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Permission\PermissionDto;
use App\Helpers\Mapper;


class Permission extends Entity
{

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->name = $Name;
    }
}