<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldUserScore extends Entity
{
    public function __construct(User $user, $scoreOne = null, $finalScore = null){
        $this->user = $user;
        $this->score_one = $scoreOne;
        $this->final_score = $finalScore;
    }

    public function getUser(){
        return $this->user;
    }

    public function setUser($user){
        $this->user = $user;
    }

    public function getScoreOne(){
        return $this->score_one;
    }

    public function setScoreOne($scoreOne){
        $this->score_one = $scoreOne;
    }

    public function getFinalScore(){
        return $this->final_score;
    }

    public function setFinalScore($finalScore){
        $this->final_score = $finalScore;
    }

}