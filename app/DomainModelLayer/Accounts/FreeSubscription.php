<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\Subscription;
use Analogue\ORM\Entity;

class FreeSubscription extends Entity
{

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    public function getCancelled()
    {
    	return $this->cancelled;
    }

    public function setCancelled($value)
    {
    	$this->cancelled = $value;
    }
    
}