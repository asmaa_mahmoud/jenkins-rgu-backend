<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\NotificationTranslation;

class NotificationMap extends EntityMap {

    protected $table = 'notifications';
	 public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "notifications.deleted_at";


    public function user(Notification $notification)
    {
        return $this->belongsTo($notification, User::class , 'user_id', 'id');
    }

    public function translations(Notification $notification)
    {
        return $this->hasMany($notification, NotificationTranslation::class , 'notification_id', 'id');
    }

}