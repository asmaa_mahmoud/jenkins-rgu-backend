<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Plan;
use App\DomainModelLayer\Accounts\PlanUnlockable;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Journeys\JourneyCategory;
use App\DomainModelLayer\Accounts\PlanHistoryText;
use App\DomainModelLayer\Schools\SchoolPlan;

class PlanHistoryMap extends EntityMap {

    protected $table = 'plan_history';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "plan_history.deleted_at";


    public function plan(PlanHistory $planHistory)
    {
        return $this->belongsTo($planHistory, Plan::class , 'Plan_Id', 'id');
    }

    public function subscriptions(PlanHistory $planHistory)
    {
        return $this->hasMany($planHistory, Subscription::class, 'PlansHistory_Id','id');
    }

    public function categories(PlanHistory $planHistory)
    {
        return $this->belongsToMany($planHistory,JourneyCategory::class,'plan_category','plan_history_id','category_id');
    }
    public function planHistoryText(PlanHistory $planHistory)
    {
        return $this->hasMany($planHistory, PlanHistoryText::class , 'plan_history_id', 'id');
    }
    public function schoolInfo(PlanHistory $planHistory)
    {
        return $this->hasOne($planHistory, SchoolPlan::class , 'plan_history_id', 'id');
    }

    public function plan_unlockables(PlanHistory $planHistory)
    {
        return $this->hasMany($planHistory, PlanUnlockable::class , 'plan_history_id', 'id');
    }

}