<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\PlanHistory;


class PlanHistoryTextMap extends EntityMap {

    protected $table = 'plan_history_text';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "plan_history_text.deleted_at";


    public function planHistory(PlanHistoryText $planHistoryText)
    {
        return $this->belongsTo($planHistoryText, PlanHistory::class , 'plan_history_id', 'id');
    }

}