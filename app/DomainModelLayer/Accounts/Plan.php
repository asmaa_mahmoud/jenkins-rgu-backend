<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\PlanDto;
use Analogue\ORM\EntityCollection;

class Plan extends Entity
{

    public function __construct(PlanDto $planDto = null)
    {
        if($planDto != null)
        {
            $this->name = $planDto->Name;
        }
        $this->planHistory = new EntityCollection;       
    }

    public function addHistory(PlanHistory $planHistory)
    {
        $this->planHistory->push($planHistory);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType(){
        return $this->type;
    }

    public function getPublicPermission()
    {
        return $this->public;
    }
    
}