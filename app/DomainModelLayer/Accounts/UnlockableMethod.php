<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Account;
use LaravelLocalization;

class UnlockableMethod extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getAccountUnlockables(){
        return $this->account_unlockables;
    }
}
