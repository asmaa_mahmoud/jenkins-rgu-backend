<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Invitation;
use Analogue\ORM\Entity;

class InvitationActivity extends Entity
{

    public function __construct(Activity $activity,Invitation $invitation)
    {
        $this->activity = $activity;
        $this->invitation = $invitation;
    }

    public function getInvitation()
    {
        return $this->invitation;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function getProgressLock()
    {
        return $this->progress_lock;
    }

}