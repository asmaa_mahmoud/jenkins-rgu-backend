<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class GuestDataMap extends EntityMap
{
    protected $table = 'guest_data';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "guest_data.deleted_at";

}