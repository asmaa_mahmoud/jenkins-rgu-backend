<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;

class PlanPeriod extends Entity
{

    public function __construct($name = null)
    {
 
        if($name != null)
        {
            $this->name = $name;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
}