<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldParameterTranslationMap extends EntityMap
{
    protected $table = 'parameter_translations';
    public $softDeletes = false;
    public $timestamps = false;


}