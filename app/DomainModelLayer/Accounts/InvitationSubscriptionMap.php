<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\InvitationSubscription;

class InvitationSubscriptionMap extends EntityMap {

    protected $table = 'invitation_subscription';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "invitation_subscription.deleted_at";


    public function subscription(InvitationSubscription $invitationSubscription)
    {
        return $this->belongsTo($invitationSubscription, Subscription::class , 'subscription_id', 'id');
    }

    public function invitation(InvitationSubscription $invitationSubscription)
    {
        return $this->belongsTo($invitationSubscription, Invitation::class , 'invitation_id', 'id');
    }

}