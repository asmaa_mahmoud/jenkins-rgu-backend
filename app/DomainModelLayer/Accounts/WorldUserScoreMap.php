<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldUserScoreMap extends EntityMap
{
    protected $table = 'world_user_score';
    public $softDeletes = false;
    public $timestamps = false;

    public function user(WorldUserScore $worldUserScore){
        return $this->belongsTo($worldUserScore, User::class , 'user_id', 'id');
    }

}