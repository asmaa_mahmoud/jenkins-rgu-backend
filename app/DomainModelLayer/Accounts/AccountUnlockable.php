<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Journeys\Unlockable;
use LaravelLocalization;

class AccountUnlockable extends Entity
{
    public function __construct(Account $account, Unlockable $unlockable, UnlockableMethod $method, $chargeId = null)
    {
        $this->account = $account;
        $this->unlockable = $unlockable;
        $this->method = $method;
        $this->charge_id = $chargeId;
    }

    public function getUnlockable(){
        return $this->unlockable;
    }

    public function setUnlockable(Unlockable $unlockable){
        $this->unlockable = $unlockable;
    }

    public function getAccount(){
        return $this->account;
    }

    public function setAccount(Account $account){
        $this->account = $account;
    }

    public function getUnlockableMethod(){
        return $this->method;
    }

    public function setUnlockableMethod(UnlockableMethod $method){
        $this->method = $method;
    }

    public function getChargeId(){
        return $this->charge_id;
    }

    public function setChargeId($chargeId){
        $this->charge_id = $chargeId;
    }

    public function getMethod(){
        return $this->method;
    }

}
