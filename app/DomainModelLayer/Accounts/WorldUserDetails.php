<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldUserDetails extends Entity
{
    public function __construct(User $user, $name = null, $address = null, $phone = null, $image = null, $predictor = null) {
        $this->user = $user;
        $this->address = $address;
        $this->phone = $phone;
        $this->name = $name;
        $this->image = $image;
        $this->predictor = $predictor;
    }

    public function getId(){
        return $this->id;
    }

    public function getAddress(){
        return $this->address;
    }

    public function setAddress($address){
        $this->address = $address;
    }

    public function getPhone(){
        return $this->phone;
    }

    public function setPhone($phone){
        $this->phone = $phone;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getImage(){
        return $this->image;
    }

    public function setImage($image){
        $this->image = $image;
    }

    public function getUser(){
        return $this->user;
    }

    public function setUser(User $user){
        $this->user = $user;
    }

    public function getPredictor(){
        return $this->predictor;
    }

    public function setPredictor($predictor){
        $this->predictor = $predictor;
    }
}