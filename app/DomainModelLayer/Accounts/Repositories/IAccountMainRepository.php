<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/23/2017
 * Time: 3:51 PM
 */

namespace App\DomainModelLayer\Accounts\Repositories;

use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use App\DomainModelLayer\Accounts\GuestData;
use App\DomainModelLayer\Accounts\PushNotification;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Plan;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\InvoiceDetailsDto;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Accounts\UserPosition;
use App\DomainModelLayer\Accounts\StripeEvent;
use App\DomainModelLayer\Accounts\UserScore;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;
use App\DomainModelLayer\Accounts\WorldUserDetails;
use App\DomainModelLayer\Accounts\WorldUserParameter;
use App\DomainModelLayer\Accounts\WorldUserPrediction;
use App\DomainModelLayer\Accounts\WorldUserScore;
use App\DomainModelLayer\Professional\ColorPalette;

interface IAccountMainRepository
{
    public function activate($token_url);
    public function validate_url($token_url);
    public function getAccountByUser($user);
    public function LoginUser($credentials,$gmt_difference);
    public function GetUser($username);
    public function getUserById($id);
    public function getLastSubscription(Account $account);
    public function getLastSubscriptionById($account_id);
    public function storeSubscription(Subscription $subscription);
    public function getAccountTypeByName($name);
    public function getAccountByAccountType($accountType);
//    public function AlreadyRegistered(UserDto $userDto);
    public function AlreadyRegistered($userDto);
   public function UserNameAlreadyExist(UserDto $userDto);
   // public function UserNameAlreadyExist($userDto);

  //  public function AlreadyRegistered(UserDto $userDto);
    public function isAlreadyRegistered($userEmail);
  //  public function UserNameAlreadyExist(UserDto $userDto);
    public function isUserNameAlreadyExist($username);

    public function getRoleByName($roleName);
    public function AddUser(User $user);
    public function getPlanByName($plan_name);
    public function getlastupdated(Plan $plan,$account_type_id);
    public function checkSubscription(PlanHistory $updated_plan, Account $account);
    public function emailSubscription($account_name,$plan_name,$amount,$currency,$interval,$quantity,$account_email,$trial = false);
    public function handlePaidSubscription($token = null , $plan_name,$plan_period,$account_type_name,$stripe_id = null,$trial,$country = null,$noOfChildren = 0,$coupon_id= null);
    public function storeUser(User $user);
    public function saveProfileImage(User $user,$image_name,$image_data);
    public function getAvailblePlanPeriods();
    public function getPlanPeriodByName($name);
    public function getPlansByAccountType($account_type_id);
    public function getAllTypes();
    public function getSubscriptionById($id);
    public function handleCancelPaidSubscription(StripeSubscription $stripeSubscription);
    public function handleUpgradeSubscription(StripeSubscription $stripeSubscription,$plan_name,$period,$account_type_name);
    public function getFacebookProfileResponse($params);
    public function getGoogleProfileResponse($params);
    public function checkSocialUserRegistered($socialId,$provider);
    public function requestTwitterTokenOauth($consumerKey,$consumerSecret,$redirectUri);
    public function getTwitterAccessTokenOauth($consumerKey,$consumerSecret,$oauthToken,$oauthVerifier);
    public function getActiveSubscriptions($account_id);
    public function AlreadyRegisteredAnotherUser($email, User $user);
    public function UserNameAlreadyExistAnotherUser($username, User $user);
    public function emailUnsubscription($account_name,$plan_name,$end_date,$account_email);
    public function emailUpgrade($account_name,$new_plan_name,$old_plan_name,$account_email);
    public function getTokenfromUser($user_id);
    public function getParentChildren($id);

    public function storeInvitation(Invitation $Invitation);
    public function getInvitationByCode($code);

    public function getUserNotifications($user_id);
    public function storeNotification(Notification $notification);
    public function getNotificationById($id);
    public function deletePosition(UserPosition $position);
    public function storePosition(UserPosition $position);
    public function supervisorEmailExists($email);
    public function createStripeCustomer($token = null);

    public function updateStripeCustomer($customerId,$token);

    public function getDefaultAccount();
    public function isRegisteredSupervisor($supervisorEmail);
    public function childConfirmEmail(UserDto $childUserDto,$parentName,$parentAccountTypeName);
    public function getUserByEmail($email);
    public function getFamilyAccountType();
    public function storeAccount(Account $account);
    public function deleteAccount(Account $account);
    public function storeStripeEvent(StripeEvent $stripeEvent);
    public function getAccountByCustomerId($customerId);
    public function getAccountById($accountId);
    public function sendPaymentFailedEmail(User $user,$trialNumber);
    public function getStripeSubsriptionByStripeId($stripeId);

    public function addSubscriptionItem(StripeSubscription $stripeSubscription,$plan_name,$period, $account_type_name = 'Individual',$end_trial = true,$quantity = 1);
    public function getChildsBySupervisorEmail($supervisorEmail);

    public function customerInfo($customerId);
    public function customerInvoices($customerId);
    public function retrieveInvoiceDetails($invoice_id);
    public function changeSubscriptionItemQuantity($stripe_id,$new_quantity = 1);
    public function sendContactUsEmail(SupportEmailDto $supportEmailDto);

    public function requestParentEmail(User $user, $parent_email,$account_type, $parent_name);
    public function sendSupportEmail(User $user,SupportEmailDto $supportEmailDto);
    public function getAllUsers();
    public function createPasswordReset($email, $username);
    public function sendResetPasswordEmail(PasswordResetDto $passwordResetDto);
    public function confirmResetPassword(PasswordResetDto $resetPasswordDto,$newPassword);
    public function requestParentToSubscribe(User $user,User $parent,$name,$is_plan);
    public function getUserUnreadNotications($user_id);
    public function resetChildsSupervisorEmail($supervisorEmail,$old_email);
    public function beginDatabaseTransaction();
    public function commitDatabaseTransaction();
    public function rollBackDatabaseTransaction();
    public function getNotCancelledSubscriptions($account_id);

    public function getTrialEnd(StripeSubscription $stripeSubscription);
    public function sendInvoiceEmail(InvoiceDetailsDto $invoiceDetails,User $user);
    public function requestCountryChange(User $user,$newCountry);
    public function changeCountryForUser(User $user,$newCountry);
    public function changeSubscriptionTax($subscriptionId,$newCountry);
    public function getRbf($code);
    public function isAuthorized($permission,User $user);
    public function getUserByUsername($username);
    public function getExtraPlans($account_type_id);
    public function getChildItem(Subscription $subscription);
    public function removeSubscriptionItem($item_id,$prorate = true);

    public function deleteSubscriptionItem(SubscriptionItem $subscriptionItem);

    public function retrieveSubscription($subscription_id);

    public function getLastRoboPalItem($subscription_id);

    public function getStatusByName($name);

    public function deleteUser(User $user);
    public function getNumberOfUsersWithRole(Account $account,$roleName);

    public function getLanguages();
    public function getLanguageById($language_id);
    public function reportBug($error);
    public function sendFeedbackMail($name, $feeling, $email, $msg);

    public function UserSubscription($user_id);
    public function translateNotification($title, $message);
    public function getChildItems($subscription);
    public function getCountriesUsersCount();

    public function addUserScore(UserScore $user);
    public function removeUserScore(User $userScore);

    public function getUserModelAnswerUnlocked($user_id,$mission_id);
    public function storeUserModelAnswerUnlocked(ModelAnswerUnlocked $modelAnswerUnlocked);

    public function getUserModelAnswers($user_id,$mission_id);
    public function storeUserModelAnswer(UserModelAnswer $userModelAnswer);

    public function getExtrasNotInIds($extras_ids);
    public function getActiveSubscriptionsWithoutFree($account_id);
    public function getActiveFreeSubscription($account_id);
    public function getTax(Account $account);

    public function getUserScore($user_id);
    public function storeUserScore(UserScore $userScore);
    public function getNotCancelledSubscriptionsWithoutFree($account_id);
    public function getCancelledSubscription($account_id);
    public function getAllPlanNames();
    public function getAccountUnlockables($account_id);
    public function charge($amount, $customer_id, $description);
    public function getUnlockableMethodByName($name);
    public function storeAccountUnlockable(AccountUnlockable $accountUnlockable);
    public function customerCharges($customerId);
    public function retrieveChargeDetails($chargeId);
    public function getCategoryByName($categoryName);
    public function getPlanByCategoryAndAccountType($categoryId,$accountTypeId);

    // world cup functions
    public function getMatchById($id);
    public function getParameterById($id);
    public function getMatchesByRoundNo($roundNo);
    public function getFeaturedMatchInDay($firstOfDay, $lastOfDay);
    public function getUsersPredictionInMatch($match_id, $winner, $count = false);
    public function getMatchesInDay($firstOfDay, $lastOfDay);
    public function getNoSubmission($match_id = null);
    public function getWorldUsersScore($roundNo, $user_id = null, $limit = null, $count = false);
    public function getUsersScore($roundNo);
    public function getNextMatchDate();
    public function getAllParameters();
    public function getNoWorldUsers();
    public function getLastPredictionOfUser($user_id);
    public function getUserPredictionInMatches($user_id, $match_ids);
    public function getWorldUserData($user_id);
    public function storeWorldUserDetails(WorldUserDetails $worldUserDetails);
    public function getUnScoredPredictions();
    public function storeWorldUserScore(WorldUserScore $worldUserScore);
    public function storeUserPrediction(WorldUserPrediction $worldUserPrediction);
    public function getUserParameters($user_id);
    public function getTomorrowMatches($dateTime);
    public function getMatchPrediction($user_id, $match_id);
    public function getLastMatchInDay($date);
    public function countUserPredictInRound($user_id, $roundNo);
    public function deleteUserParameters($user_id);
    public function storeUserParameter(WorldUserParameter $userParameter);
    public function getNextMatch();
    public function getUserParameterWithName($user_id, $parameterName);
    public function getMatchesByRoundNoFromDate($startDate, $roundNo);
    public function getAllFreePlans();
    public function getPlanUnlockable($planHistoryId, $unlockableId);
    public function getUserByIpAddress($ip_address);
    public function storeGuestData(GuestData $guestData);

    public function getAccountUnlockable($account_id, $unlockable_id);

    public function getUserNotificationById($notification_id);
    public function storeUserNotification(UserNotification $notification);
    public function deleteUserNotification(UserNotification $notification);
    public function storePushNotificationSub(PushNotification $notification);
    public function deletePushNotificationSub(PushNotification $notification);
    public function getPushNotificationSub($user_id);
    public function getPushNotificationSubByEndpoint($endpoint);
    public function getNotifications($user_id, $type=null);
    public function markNotificationAsRead(UserNotification $notification);
    public function getColorPalette($colorPaletteId); 
    public function getSchoolAdmin($account_id);
}