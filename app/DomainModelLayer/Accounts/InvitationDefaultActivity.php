<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Invitation;
use Analogue\ORM\Entity;

class InvitationDefaultActivity extends Entity
{

    public function __construct(DefaultActivity $activity,Invitation $invitation)
    {
        $this->defaultActivity = $activity;
        $this->invitation = $invitation;
    }

    public function getInvitation()
    {
        return $this->invitation;
    }

    public function getDefaultActivity()
    {
        return $this->defaultActivity;
    }

    public function getProgressLock()
    {
        return $this->progress_lock;
    }

}