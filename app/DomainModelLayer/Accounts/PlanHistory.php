<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\PlanHistoryDto;
use App\DomainModelLayer\Journeys\JourneyCategory;
use Analogue\ORM\EntityCollection;
use LaravelLocalization;

class PlanHistory extends Entity
{

    public function __construct(PlanHistoryDto $planHistoryDto = null,Plan $plan = null)
    {
        if($planHistoryDto != null && $plan != null)
        {
            $this->new_name = $planHistoryDto->Name;
            $this->plan = $plan;
        }
        $this->subscriptions = new EntityCollection;       
    }

    public function addSubscription(Subscription $subscription)
    {
        $this->subscriptions->push($subscription);
    }

    public function getCreditPerMonth()
    {
        return $this->credit_per_month;
    }

    public function getCreditPerYear()
    {
        return $this->credit_per_year;
    }

    public function getCreditPerQuarter()
    {
        return $this->credit_per_quarter;
    }

    public function getName()
    {
        return $this->new_name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMaximumNoJourneys()
    {
        return $this->number_of_journeys;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function getPlan(){
        return $this->plan;
    }

    public function getTrialPeriod()
    {
        return $this->trial_period;
    }

    public function getText(){
        $textsArray = [];
        $locale = LaravelLocalization::getCurrentLocale();
        $texts = $this->planHistoryText;
        foreach ($texts as $text) {
            if($text->getLanguageCode() == $locale && $text->getText() != null)
                $textsArray[] =  $text->getText();
        }
        return $textsArray;   
    }

    public function isExtra()
    {
        return $this->is_extra;
    }

    public function getSchoolInfo()
    {
        return $this->schoolInfo;
    }

    public function getPlanUnlockables(){
        return $this->plan_unlockables;
    }
    
}
