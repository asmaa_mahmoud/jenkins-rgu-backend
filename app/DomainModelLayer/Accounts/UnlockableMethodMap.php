<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\AccountUnlockable;

class UnlockableMethodMap extends EntityMap {

    protected $table = 'unlockable_method';

    public function account_unlockables(UnlockableMethod $unlockable_method)
    {
        return $this->hasMany($unlockable_method, AccountUnlockable::class , 'unlockable_id', 'id');
    }

}