<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\PlanHistory;
use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Subscription;

class SubscriptionItem extends Entity
{

    public function __construct(Subscription $subscription, PlanHistory $plan, $stripe_id,$quantity = 1)
    {    
        $this->plan = $plan;
        $this->subscription = $subscription; 
        $this->stripe_id =  $stripe_id;
        $this->quantity = $quantity;
    }

    public function getPlan()
    {
    	return $this->plan;
    }

    public function setPlan(PlanHistory $plan)
    {
        $this->plan = $plan;
    }

    public function getSubscription()
    {
    	return $this->subscription;
    }

    public function getStripeId()
    {
    	return $this->stripe_id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getEndDate()
    {
        return $this->ends_at;
    }

    public function setEndDate($end_date)
    {
        $this->ends_at = $end_date;
    }
    
}