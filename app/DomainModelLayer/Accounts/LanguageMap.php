<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class LanguageMap extends EntityMap {

    protected $table = 'languages';

}