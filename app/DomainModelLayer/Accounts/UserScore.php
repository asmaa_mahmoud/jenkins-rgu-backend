<?php
namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;

class UserScore extends Entity
{

    public function __construct(User $user,$xp,$coins)
    {
        $this->xp = $xp;
        $this->coins = $coins;
        $this->user = $user;
    }

    public function getExperience()
    {
        return $this->xp;
    }

    public function setExperience($xp){
        $this->xp = $xp;
    }

    public function getCoins()
    {
        return $this->coins;
    }

    public function setCoins($coins){
        $this->coins = $coins;
    }

    public function getRankLevel(){
        return $this->rankLevel;
    }

}