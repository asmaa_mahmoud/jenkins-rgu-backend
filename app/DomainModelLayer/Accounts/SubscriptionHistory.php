<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\Account;
use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Subscription;
use App\Helpers\Mapper;
use Analogue\ORM\EntityCollection;

class SubscriptionHistory extends Entity
{

    public function __construct(Subscription $subscription)
    {    
        $this->plan = $subscription->getPlan();
        $this->account = $subscription->getAccount();
        $this->subscription = $subscription;  
    }
    
}