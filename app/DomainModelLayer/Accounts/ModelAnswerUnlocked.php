<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/10/17
 * Time: 9:34 PM
 */

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Mission;

class ModelAnswerUnlocked extends Entity
{

    public function __construct(Mission $mission, User $user)
    {
        $this->mission = $mission;
        $this->user = $user;
    }

    public function getMission()
    {
        return $this->mission;
    }

    public function getUser()
    {
        return $this->user;
    }

}