<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;


class StripeEventMap extends EntityMap {

    protected $table = 'stripe_event';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "stripe_event.deleted_at";


}