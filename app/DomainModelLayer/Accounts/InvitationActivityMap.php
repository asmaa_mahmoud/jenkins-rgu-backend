<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\InvitationJourney;

class InvitationActivityMap extends EntityMap {

    protected $table = 'invitation_activity';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "invitation_activity.deleted_at";

    public function activity(InvitationActivity $invitationActivity)
    {
        return $this->belongsTo($invitationActivity, Activity::class , 'activity_id', 'id');
    }

    public function invitation(InvitationActivity $invitationActivity)
    {
        return $this->belongsTo($invitationActivity, Invitation::class , 'invitation_id', 'id');
    }

}