<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\InvitationJourney;

class InvitationJourneyMap extends EntityMap {

    protected $table = 'invitation_journey';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "invitation_journey.deleted_at";

    public function journey(InvitationJourney $invitationJourney)
    {
        return $this->belongsTo($invitationJourney, Journey::class , 'journey_id', 'id');
    }

    public function invitation(InvitationJourney $invitationJourney)
    {
        return $this->belongsTo($invitationJourney, Invitation::class , 'invitation_id', 'id');
    }

}