<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Unlockable;
use App\DomainModelLayer\Accounts\UnlockableMethod;
use App\DomainModelLayer\Accounts\Account;

class AccountUnlockableMap extends EntityMap {

    protected $table = 'account_unlockable';
    public $timestamps = true;

    public function account(AccountUnlockable $account_unlockable)
    {
        return $this->belongsTo($account_unlockable, Account::class , 'account_id', 'id');
    }

    public function unlockable(AccountUnlockable $account_unlockable)
    {
        return $this->belongsTo($account_unlockable, Unlockable::class , 'unlockable_id', 'id');
    }

    public function method(AccountUnlockable $account_unlockable)
    {
        return $this->belongsTo($account_unlockable, UnlockableMethod::class , 'method_id', 'id');
    }

}