<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldParameterMap extends EntityMap
{
    protected $table = 'parameters';
    public $softDeletes = false;
    public $timestamps = false;

    public function translations(WorldParameter $worldParameter){
        return $this->hasMany($worldParameter, WorldParameterTranslation::class , 'parameter_id', 'id');
    }

}