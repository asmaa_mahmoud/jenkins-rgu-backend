<?php
namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldUserParameterMap extends EntityMap
{
    protected $table = 'user_parameters';
    public $softDeletes = false;
    public $timestamps = false;

    public function parameter(WorldUserParameter $userParameter){
        return $this->belongsTo($userParameter, WorldParameter::class , 'parameter_id', 'id');
    }

    public function user(WorldUserParameter $userParameter){
        return $this->belongsTo($userParameter, User::class , 'user_id', 'id');
    }
}