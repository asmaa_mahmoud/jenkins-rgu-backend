<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Accounts\Dtos\PushNotificationDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\NotificationTranslation;

class PushNotification extends Entity
{

    public function __construct(PushNotificationDto $notificationDto, User $user)
    {
        $this->endpoint = $notificationDto->endpoint;
        $this->public_key = $notificationDto->public_key;
        $this->auth_token = $notificationDto->auth_token;

        $this->user = $user;
    }


    public  function getEndPoint() {
        return $this->endpoint;
    }

    public  function getPublicKey() {
        return $this->public_key;
    }

    public  function getAuthToken() {
        return $this->auth_token;
    }

    public function setEndPoint($endpoint){
        $this->endpoint = $endpoint;
    }

    public function setPublicKey($public_key){
        $this->public_key = $public_key;
    }

    public function setUserId($user_id){
        $this->user_id = $user_id;
    }

    public function setAuthToken($auth_token){
        $this->auth_token = $auth_token;
    }
}
