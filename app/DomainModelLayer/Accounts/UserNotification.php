<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 27/11/2018
 * Time: 4:10 PM
 */

namespace App\DomainModelLayer\Accounts;


use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\User;
use Carbon\Carbon;

class UserNotification extends Entity
{
    public function __construct(User $user,  $id, $data, $type){
        $this->user = $user;
        $this->id = $id;
        $this->data = $data;
        $this->type = $type;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function notifiable()
    {
        return $this->user;
    }

    public function markAsRead()
    {
        $this->read_at = Carbon::now()->timestamp;
    }

    public function read()
    {
        return $this->read_at !== null;
    }

    public function unread()
    {
        return $this->read_at === null;
    }

    public function getUser_id()
    {
        return $this->user->getId();
    }

    public function getData()
    {
        return $this->data;
    }

    public function  getIs_read()
    {
        if($this->is_read == 1)
           return true;
        else
           return false;

    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getUpdated_at()
    {
        return $this->updated_at;
    }
}