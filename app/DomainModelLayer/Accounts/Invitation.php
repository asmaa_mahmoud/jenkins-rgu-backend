<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\InvitationDto;
use App\DomainModelLayer\Accounts\AccountType;
use Analogue\ORM\EntityCollection;

class Invitation extends Entity
{

    public function __construct(InvitationDto $invitationDto = null,AccountType $accountType = null)
    {
        if($invitationDto != null && $accountType != null)
        {
            $this->code = $invitationDto->Code;
            $this->name = $invitationDto->Name;
            $this->accountType = $accountType;
            $this->ends_at = $invitationDto->EndDate;
            $this->number_of_users = $invitationDto->NumberOfUsers;
        }     
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAccountType()
    {
        return $this->accountType->getName();
    }

    public function getEndDate()
    {
        return $this->ends_at;
    }

    public function getNumberOfUsers()
    {
        return $this->number_of_users;
    }

    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    public function getJourneys()
    {
        return $this->journeys;
    }

    public function getDefaultActivities(){
        return $this->defaultActivities;
    }

    public function getActivities(){
        return $this->activities;
    }

    public function getPlans()
    {
        return $this->plans;
    }

    public function getSchoolInfo()
    {
        return $this->schoolInfo;
    }

    public function getFamilyInfo()
    {
        return $this->familyInfo;
    }

}