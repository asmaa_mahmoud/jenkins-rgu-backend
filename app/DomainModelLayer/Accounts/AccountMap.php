<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\AccountType;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\Status;
use App\DomainModelLayer\Accounts\Subscription;

class AccountMap extends EntityMap {

    protected $table = 'account';
    public $softDeletes = true;
    public $timestamps = true;
    protected $deletedAtColumn = "account.deleted_at";

    public function accountType(Account $account)
    {
        return $this->belongsTo($account, AccountType::class , 'account_type_id', 'id');
    }

    public function status(Account $account)
    {
        return $this->belongsTo($account, Status::class , 'status_id', 'id');
    }

    public function users(Account $account)
    {
        return $this->hasMany($account, User::class, 'account_id','id');
    }

	public function subscriptions(Account $account)
	{
	   return $this->hasMany($account, Subscription::class, 'account_id','id');
	}

    public function school(Account $account)
    {
        return $this->hasOne($account, School::class, 'account_id','id');
    }
    public function account_unlockables(Account $account)
    {
        return $this->hasMany($account, AccountUnlockable::class , 'account_id', 'id');
    }
//
//    public function roles(Account $account)
//    {
//        return $this->hasMany($account, Role::class, 'Account_Id','Id');
//    }

}