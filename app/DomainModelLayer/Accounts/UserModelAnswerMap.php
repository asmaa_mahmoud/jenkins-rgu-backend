<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/13/17
 * Time: 1:55 PM
 */
namespace App\DomainModelLayer\Accounts;
use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Accounts\UserPosition;

class UserModelAnswerMap extends EntityMap
{

    protected $table = 'user_model_answer';
    public $timestamps = true;
//	public $softDeletes = true;
//    protected $deletedAtColumn = "user_position.deleted_at";


    public function user(UserModelAnswer $userModelAnswer)
    {
        return $this->belongsTo($userModelAnswer, User::class, 'user_id', 'id');
    }

    public function mission(UserModelAnswer $userModelAnswer)
    {
        return $this->belongsTo($userModelAnswer, Mission::class, 'mission_id', 'id');
    }
}



