<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\UserRole;

class UserRoleMap extends EntityMap {

    protected $table = 'userrole';
	public $timestamps = true;
	public $softDeletes = true;
    protected $deletedAtColumn = "userrole.deleted_at";



    public function role(UserRole $userRole)
    {
        return $this->belongsTo($userRole, Role::class, 'Role_Id', 'id');
    }

    public function user(UserRole $userRole)
    {
        return $this->belongsTo($userRole, User::class, 'User_Id', 'id');
    }

}