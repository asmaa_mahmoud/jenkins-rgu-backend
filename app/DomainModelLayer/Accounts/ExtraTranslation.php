<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class ExtraTranslation extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }

    public function getExtra(){
        return $this->extra;
    }

}