<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Permission;
use App\DomainModelLayer\Accounts\Role;

class PermissionMap extends EntityMap {

    protected $table = 'permission';
    public $softDeletes = true;
    protected $deletedAtColumn = "permission.deleted_at";


    public function roles(Permission $permission)
    {
        return $this->belongsToMany($permission, Role::class, 'role_permission', 'permission_id', 'role_id');
    }

}