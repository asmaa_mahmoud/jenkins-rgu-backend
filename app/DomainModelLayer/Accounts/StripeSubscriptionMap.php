<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\PlanPeriod;
use App\DomainModelLayer\Accounts\StripeSubscription;

class StripeSubscriptionMap extends EntityMap {

    protected $table = 'stripe_subscription';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "stripe_subscription.deleted_at";


    public function subscription(StripeSubscription $stripeSubscription)
    {
        return $this->belongsTo($stripeSubscription, Subscription::class , 'subscription_id', 'id');
    }

    public function period(StripeSubscription $stripeSubscription)
    {
        return $this->belongsTo($stripeSubscription, PlanPeriod::class , 'period_id', 'id');
    }

}