<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Account;

class StatusMap extends EntityMap {

    protected $table = 'status';

}