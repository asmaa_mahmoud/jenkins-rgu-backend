<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Notification;

class NotificationTranslationMap extends EntityMap {

    protected $table = 'notifications_translation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "notifications_translation.deleted_at";


    public function notification(NotificationTranslation $notification_translation)
    {
        return $this->belongsTo($notification_translation, Notification::class , 'notification_id', 'id');
    }


}