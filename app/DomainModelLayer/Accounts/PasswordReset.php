<?php

namespace App\DomainModelLayer\Accounts;
use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;


class PasswordReset extends Entity
{
    //region Getters & Setters

    public function __construct($email)
    {
        $this->email = $email;
        $this->token = $this->random_str(30, 'abcdefghijklmnopqrstuvwxyz');        
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function setToken(){
        $this->token = $this->random_str(30, 'abcdefghijklmnopqrstuvwxyz');
    }

    public function getToken(){
        return $this->token;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function setUsed(){
        $this->used = 1;
    }
    public function getUsed(){
        return $this->used;
    }

    public function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

}