<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Extra;
use App\DomainModelLayer\Accounts\ExtraTranslation;

class ExtraTranslationMap extends EntityMap
{
    protected $table = 'extra_translation';
    public $timestamps = true;

    public function activity(ExtraTranslation $extraTranslation)
    {
        return $this->belongsTo($extraTranslation, Extra::class , 'extra_id', 'id');
    }

}