<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\UserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class UserCreatedHandle
{

    public function __construct()
    {
        $construct = 'test construct';
    }

    public function handle(UserCreated $event)
    {
        $user = $event->user;
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        if($user->getAge() <= 12)
            $this->sendEmailToParent($event->account, $event->user, $user_name);
        else
            $this->sendEmail($event->account, $event->user, $user_name);
    }

    public function sendEmail($account, $user, $user_name = null){
        Mail::send('emails.registration', ['token_url' => $account->getTokenUrl(), 'username' => $user_name], function($message) use($user, $user_name)
        {
            $message->from('info@robogarden.ca','The Professional RoboGarden Team');
            $message->to($user->getEmail(), $user_name)->subject(trans('locale.email_confirmation_subject'));

        });
    }

    public function sendEmailToParent($account, $user, $user_name = null)
    {
        Mail::send('emails.registration', ['token_url' => $account->getTokenUrl(), 'username' => $user_name], function($message) use($user, $user_name)
        {
            $message->from('info@robogarden.ca','The RoboGarden Team');
            $message->to($user->getEmail(), $user_name)->subject('RoboGarden: Email Confirmation');

        });
    }
}
