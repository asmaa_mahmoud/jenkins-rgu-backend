<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Invitation;
use Analogue\ORM\Entity;

class InvitationJourney extends Entity
{

    public function __construct(Journey $journey,Invitation $invitation)
    {
        $this->journey = $journey;
        $this->invitation = $invitation;
    }

    public function getInvitation()
    {
    	return $this->invitation;
    }

    public function getJourney()
    {
        return $this->journey;
    }

    public function getProgressLock()
    {
        return $this->progress_lock;
    }
    
}