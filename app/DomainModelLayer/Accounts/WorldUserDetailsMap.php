<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldUserDetailsMap extends EntityMap
{
    protected $table = 'world_user_details';
    public $softDeletes = false;
    public $timestamps = false;

    public function user(WorldUserDetails $worldUserDetails){
        return $this->belongsTo($worldUserDetails, User::class , 'user_id', 'id');
    }

}