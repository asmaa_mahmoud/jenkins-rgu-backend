<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionHistory;

class SubscriptionHistoryMap extends EntityMap {

    protected $table = 'subscription_history';
    public $timestamps = true;

    public function plan(SubscriptionHistory $subscriptionHistory)
    {
        return $this->belongsTo($subscriptionHistory, PlanHistory::class , 'plan_history_id', 'id');
    }

    public function subscription(SubscriptionHistory $subscriptionHistory)
    {
        return $this->belongsTo($subscriptionHistory, Subscription::class,'subscription_id','id');
    }

    public function account(SubscriptionHistory $subscriptionHistory)
    {
        return $this->belongsTo($subscriptionHistory, Account::class, 'account_id','id');
    }

}