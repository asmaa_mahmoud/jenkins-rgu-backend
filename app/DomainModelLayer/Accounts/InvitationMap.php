<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\AccountType;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\InvitationSubscription;
use App\DomainModelLayer\Accounts\InvitationJourney;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Schools\SchoolInvitation;

class InvitationMap extends EntityMap {

    protected $table = 'invitation';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "invitation.deleted_at";


    public function accountType(Invitation $invitation)
    {
        return $this->belongsTo($invitation, AccountType::class , 'account_type_id', 'id');
    }

    public function subscriptions(Invitation $invitation)
    {
    	return $this->hasMany($invitation, InvitationSubscription::class,'invitation_id','id');
    }

    public function journeys(Invitation $invitation)
    {
        return $this->hasMany($invitation, InvitationJourney::class,'invitation_id','id');
    }

    public function plans(Invitation $invitation)
    {
        return $this->belongsToMany($invitation,PlanHistory::class,'invitation_plan','invitation_id','plan_history_id');
    }

    public function schoolInfo(Invitation $invitation)
    {
        return $this->hasOne($invitation, SchoolInvitation::class , 'invitation_id', 'id');
    }

    public function familyInfo(Invitation $invitation)
    {
        return $this->hasOne($invitation, FamilyInvitation::class , 'invitation_id', 'id');
    }

    public function defaultActivities(Invitation $invitation)
    {
        return $this->hasMany($invitation, InvitationDefaultActivity::class,'invitation_id','id');
    }

    public function activities(Invitation $invitation)
    {
        return $this->hasMany($invitation, InvitationActivity::class,'invitation_id','id');
    }


}