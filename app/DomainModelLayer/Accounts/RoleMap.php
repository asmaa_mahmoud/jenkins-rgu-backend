<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\Permission;
//use App\DomainModelLayer\UserRole\UserRole;

class RoleMap extends EntityMap {

    protected $table = 'role';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "role.deleted_at";



    // public function account(Role $role)
    // {
    //     return $this->belongsTo($role, Account::class, 'Account_Id', 'Id');
    // }

   public function permissions(Role $role)
   {
       return $this->belongsToMany($role, Permission::class, 'role_permission', 'role_id', 'permission_id');
   }

    // public function user_roles(Role $role)
    // {
    //     return $this->hasMany($role, UserRole::class, 'Role_Id', 'id');
    // }

}