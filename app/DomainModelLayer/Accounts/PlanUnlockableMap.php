<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Journeys\Unlockable;
use App\DomainModelLayer\Accounts\PlanUnlockable;

class PlanUnlockableMap extends EntityMap {

    protected $table = 'plan_unlockable';
    public $timestamps = true;

    public function planHistory(PlanUnlockable $planUnlockable)
    {
        return $this->belongsTo($planUnlockable, PlanHistory::class , 'plan_history_id', 'id');
    }

    public function unlockable(PlanUnlockable $planUnlockable)
    {
        return $this->belongsTo($planUnlockable, Unlockable::class , 'unlockable_id', 'id');
    }

}