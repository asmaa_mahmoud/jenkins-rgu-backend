<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/10/17
 * Time: 9:31 PM
 */

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;

class ModelAnswerUnlockedMap extends EntityMap
{

    protected $table = 'model_answer_unlocked';
    public $timestamps = true;
//	public $softDeletes = true;
//    protected $deletedAtColumn = "user_position.deleted_at";


    public function user(ModelAnswerUnlocked $modelAnswerUnlocked)
    {
        return $this->belongsTo($modelAnswerUnlocked, User::class, 'user_id', 'id');
    }

    public function mission(ModelAnswerUnlocked $modelAnswerUnlocked)
    {
        return $this->belongsTo($modelAnswerUnlocked, Mission::class, 'mission_id', 'id');
    }

}