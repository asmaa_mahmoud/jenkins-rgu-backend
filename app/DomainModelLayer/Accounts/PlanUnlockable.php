<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Journeys\Unlockable;
use LaravelLocalization;

class PlanUnlockable extends Entity
{
    public function __construct(PlanHistory $planHistory, Unlockable $unlockable)
    {
        $this->planHistory = $planHistory;
        $this->unlockable = $unlockable;
    }

    public function getId(){
        return $this->id;
    }

    public function getUnlockable(){
        return $this->unlockable;
    }

    public function setUnlockable(Unlockable $unlockable){
        $this->unlockable = $unlockable;
    }

    public function getPlan(){
        return $this->plan;
    }

    public function setPlan(PlanHistory $planHistory){
        $this->planHistory = $planHistory;
    }


}
