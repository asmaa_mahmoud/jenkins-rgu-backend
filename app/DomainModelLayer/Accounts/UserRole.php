<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\UserRoleDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Role;
use App\Helpers\Mapper;


class UserRole extends Entity
{


    public function __construct(Role $role)
    {
        $this->Role_Id = $role->id;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->name = $Name;
    }
}