<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldCountryTranslation extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getLanguageCode(){
        return $this->language_code;
    }


}