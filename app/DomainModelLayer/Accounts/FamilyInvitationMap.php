<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class FamilyInvitationMap extends EntityMap {

    protected $table = 'family_invitation';

}