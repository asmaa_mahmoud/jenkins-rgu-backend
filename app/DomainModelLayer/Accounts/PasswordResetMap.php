<?php

namespace App\DomainModelLayer\Accounts;
use Analogue\ORM\EntityMap;



class PasswordResetMap extends EntityMap {

    protected $table = 'password_resets';

    public $timestamps = true;

}