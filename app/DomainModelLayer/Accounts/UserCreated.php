<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\Account;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserCreated
{
    use InteractsWithSockets, SerializesModels;

    public $user;
    public $account;

    public function __construct(Account $account, User $user)
    {
        $this->user = $user;
        $this->account = $account;
    }
}
