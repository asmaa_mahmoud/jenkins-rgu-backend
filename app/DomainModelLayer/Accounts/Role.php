<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\RoleDto;
use App\Helpers\Mapper;


class Role extends Entity
{
    
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->name = $Name;
    }

    public function getPermissions()
    {
        return $this->permissions;
    }

    public function getId(){
        return $this->id;
    }
}