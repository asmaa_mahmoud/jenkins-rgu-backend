<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;



class Language extends Entity
{
    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function isActive(){
        return $this->is_active;
    }
}