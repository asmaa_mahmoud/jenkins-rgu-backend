<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldMatch extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getFirstCountry(){
        return $this->firstCountry;
    }

    public function getSecondCountry(){
        return $this->secondCountry;
    }

    public function getMatchTime(){
        return $this->match_time;
    }

    public function getRoundNo(){
        return $this->round_no;
    }

    public function getWinner(){
        return $this->winner;
    }

    public function getFeatured(){
        return $this->featured;
    }

    public function getWinCount(){
        return $this->win;
    }

    public function getLoseCount(){
        return $this->lose;
    }

    public function getDrawCount(){
        return $this->draw;
    }

    public function getFirstImage(){
        return $this->first_win_image;
    }

    public function getSecondImage(){
        return $this->second_win_image;
    }

    public function getDrawImage(){
        return $this->draw_image;
    }

    public function getDefaultImage(){
        return $this->default_image;
    }

}