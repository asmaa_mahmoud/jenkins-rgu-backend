<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionItem;

class SubscriptionItemMap extends EntityMap {

    protected $table = 'subscription_item';
    public $timestamps = true;

    public function plan(SubscriptionItem $subscriptionItem)
    {
        return $this->belongsTo($subscriptionItem, PlanHistory::class , 'plan_history_id', 'id');
    }

    public function subscription(SubscriptionItem $subscriptionItem)
    {
        return $this->belongsTo($subscriptionItem, Subscription::class,'subscription_id','id');
    }

}