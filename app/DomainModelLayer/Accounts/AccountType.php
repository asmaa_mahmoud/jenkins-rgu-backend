<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\AccountTypeDto;
use Analogue\ORM\EntityCollection;

class AccountType extends Entity
{

    public function __construct(AccountTypeDto $accountTypeDto = null)
    {
 
        if($accountTypeDto != null)
        {
            $this->name = $accountTypeDto->Name;
        }
        $this->accounts = new EntityCollection;
        $this->plans = new EntityCollection;
        $this->invitations = new EntityCollection;
    }

    public function addAccount(Account $account)
    {
        $this->accounts->push($account);
    }

    public function addPlan(Plan $plan)
    {
        $this->plans->push($plan);
    }

    public function addInvitation(Plan $plan)
    {
        $this->plans->push($plan);
    }

    public function removeAccount(Account $account)
    {
        $this->accounts->remove($account);
    }

    public function removePlan(Plan $plan)
    {
        $this->plans->remove($plan);
    }

    public function removeInvitation(Invitation $invitation)
    {
        $this->invitations->remove($invitation);
    }

    public function getAccounts()
    {
        return $this->accounts;
    }

    public function getPlans()
    {
        return $this->plans;
    }

    public function getInvitations()
    {
        return $this->invitations;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
}