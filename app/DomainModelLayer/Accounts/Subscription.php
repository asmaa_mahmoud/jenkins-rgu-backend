<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\PlanHistory;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Account;
use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\SubscriptionDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\FreeSubscription;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\DomainModelLayer\Accounts\InvitationSubscription;
use App\DomainModelLayer\Accounts\SubscriptionHistory;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\DomainModelLayer\Schools\DistributorSubscription;
use Analogue\ORM\EntityCollection;

class Subscription extends Entity
{
    public function __construct(SubscriptionDto $subscriptionDto = null,PlanHistory $planHistory = null,Account $account)
    {
        if($subscriptionDto != null && $planHistory != null && $account != null)
        {
            $this->created_at = $subscriptionDto->StartDate;
            $this->ends_at = $subscriptionDto->EndDate;
            $this->plan = $planHistory;
            $this->account = $account;
            // $this->Subscription_StripeId = $subscriptionDto->StripeId;
            // $this->Period = $subscriptionDto->Period;
            $this->journeys = new EntityCollection;
            $this->freeSubscriptions = new EntityCollection;
            $this->stripeSubscriptions = new EntityCollection;
            $this->subscriptionHistory = new EntityCollection;
            $this->invitationSubscriptions = new EntityCollection;
            $this->subscriptionItems = new EntityCollection;
            $this->distributorSubscriptions = new EntityCollection;
        }       
    }

    public function addJourney(Journey $journey)
    {
        $this->journeys->push($journey);
    }

    public function addHistory(SubscriptionHistory $subscriptionHistory)
    {
        $this->subscriptionHistory->push($subscriptionHistory);
    }

    public function addFreeSubscription(FreeSubscription $freeSubscription)
    {
        $this->freeSubscriptions->push($freeSubscription);
    }

    public function removeFreeSubscription(FreeSubscription $freeSubscription)
    {
        $this->freeSubscriptions->remove($freeSubscription);
    }

    public function addStripeSubscription(StripeSubscription $stripeSubscription)
    {
        $this->stripeSubscriptions->push($stripeSubscription);
    }

    public function addInvitationSubscription(InvitationSubscription $invitationSubscription)
    {
        $this->invitationSubscriptions->push($invitationSubscription);
    }

    public function addDistributorSubscription(DistributorSubscription $distributorSubscription)
    {
        $this->distributorSubscriptions->push($distributorSubscription);
    }

    public function addSubscriptionItem(SubscriptionItem $subscriptionItem)
    {
        $this->subscriptionItems->push($subscriptionItem);
    }

    public function removeSubscriptionItem(SubscriptionItem $subscriptionItem)
    {
        $this->subscriptionItems->remove($subscriptionItem);
    }

    public function getJourneys()
    {
       return  $this->journeys;
    }

    public function getSortedJourneys()
    {
        $journeys = $this->journeys;
        $date = array();
        foreach ($journeys as $key => $row)
        {
            $date[$key] = $row['created_at'];
        }
        array_multisort($date, SORT_ASC, $journeys);
        return $journeys;
    }

    public function isEnrolledJourney(Journey $newjourney)
    {
        $enrolled = false;
        foreach ($this->journeys as $journey) {
            if($journey->getId() == $newjourney->getId())
                $enrolled = true;
        }
        return $enrolled;
    }

    public function getPlan()
    {
        return $this->plan;
    }

    public function setPlan(PlanHistory $plan)
    {
        $this->plan = $plan;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount(Account $account)
    {
        $this->account = $account;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStartDate()
    {
        return $this->created_at;
    }

    public function getEndDate()
    {
        return $this->ends_at;
    }

    public function setEndDate($end_date)
    {
        $this->ends_at = $end_date;
    }

    public function removeEndsAt()
    {
        $this->ends_at = null;
    }

    public function getStripeId()
    {
        $stripeSubscription  = $this->getStripeSubscriptions()->first();
        if($stripeSubscription != null)
            return $stripeSubscription->getStripeId();
    }

    public function getPeriod()
    {
        $stripeSubscription  = $this->getStripeSubscriptions()->first();
        if($stripeSubscription != null)
            return $stripeSubscription->getPeriod();
    }

    public function getStripeSubscriptions()
    {
        return $this->stripeSubscriptions;
    }

    public function getFreeSubscriptions()
    {
        return $this->freeSubscriptions;
    }

    public function getPlanName()
    {
        return $this->plan->getName();
    }

    public function getInvitationSubscription()
    {
        return $this->invitationSubscriptions;
    }

    public function getDistributorSubscriptions()
    {
        return $this->distributorSubscriptions;
    }

    public function getItems()
    {
        return $this->subscriptionItems;
    }

    public function schoolCharge(){
        return $this->schoolCharge;
    }

    public function getSchoolCharge(){
        return $this->schoolCharge;
    }

    
}