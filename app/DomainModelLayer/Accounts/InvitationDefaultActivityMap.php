<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\InvitationJourney;

class InvitationDefaultActivityMap extends EntityMap {

    protected $table = 'invitation_default_activity';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "invitation_default_activity.deleted_at";

    public function defaultActivity(InvitationDefaultActivity $invitationActivity)
    {
        return $this->belongsTo($invitationActivity, DefaultActivity::class , 'default_activity_id', 'id');
    }

    public function invitation(InvitationDefaultActivity $invitationActivity)
    {
        return $this->belongsTo($invitationActivity, InvitationDefaultActivity::class , 'invitation_id', 'id');
    }

}