<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\ApplicationLayer\Accounts\Dtos\AccountDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\AccountType;
use App\DomainModelLayer\Accounts\Status;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\User;
use Analogue\ORM\EntityCollection;

class Account extends Entity
{

    public function __construct(UserDto $accountDto = null, AccountType $accountType, Status $status )
    {
        if($accountDto != null)
        {
            $this->accountType = $accountType;            
            $this->token_url = str_random(30);
        }

        $this->subscriptions = new EntityCollection;
        $this->country = $accountDto->country;
        $this->status = $status;
    }

    public function addSubscription(Subscription $subscription)
    {
        $this->subscriptions->push($subscription);
    }

    public function addUser(User $user){
        $this->users->push($user);
    }
    public function removeUser(User $user){
        $this->users->remove($user);
    }
    public function removeSubscription(Subscription $subscription)
    {
        $this->subscriptions->remove($subscription);
    }

    /**
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param mixed $Grade
     */
    public function setAccountType(AccountType $accounttype)
    {
        $this->accountType = $accounttype;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return ($this->getStatus() != 'active' ? false : true);
    }


    /**
     * @return mixed
     */
    public function getStripeId()
    {
        return $this->stripe_id;
    }

    /**
     * @param mixed $Grade
     */
    public function setStripeId($stripeid)
    {
        $this->stripe_id = $stripeid;
    }

    /**
     * @param mixed $Grade
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTokenUrl()
    {
        return $this->token_url;
    }

    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getChildren()
    {
        $users = [];
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "child")
                    $users[] = $user;
            }
        }
        return $users;
    }

    public function getStudents(){
        $users = [];
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "student")
                    $users[] = $user;
            }
        }
        return $users;
    }

    public function getCountOfStudents(){
        $users = 0;
        foreach ($this->getUsers() as $user) {
            $roles = $user->getRoles();
            foreach ($roles as $role){
                if($role->getName() == "student")
                    $users++;
            }
        }
        return $users;
    }

    public function setCountry($country){
        $this->country = $country;
    }

    public function getCountry(){
        return $this->country;
    }

    public function getSchool(){
        return $this->school;
    }

    public function getStatus()
    {
        return $this->status->getName();
    }

    public function setStatus(Status $status)
    {
        $this->status = $status;
    }

    public function isStripeAccount()
    {
        if($this->getStripeId() == null)
            return false;
        $subscriptions = $this->getSubscriptions();
        foreach ($subscriptions as $subscription) {
            if($subscription->getStripeSubscriptions()->first() != null)
                return true;
        }
        return false;
    }

    public function getAccountUnlockables(){
        return $this->account_unlockables;
    }
    
}
