<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Accounts\Dtos\NotificationDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\NotificationTranslation;
use LaravelLocalization;

class Notification extends Entity
{

    public function __construct(NotificationDto $notificationDto, User $user)
    {
        $this->starts_at = $notificationDto->StartDate;
        $this->type = $notificationDto->Type;
        $this->read = 0;
        $this->ends_at = $notificationDto->EndDate;

        $this->user = $user;
        $this->translations = new EntityCollection; 
    }

    public function addTranslation(NotificationTranslation $translation)
    {
        $this->translations->push($translation);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getTitle();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getMessage();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getMessage();
        }
        return $english_translation;
    }


    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->starts_at;
    }

    public function setStartDate($start_date)
    {
        $this->starts_at = $start_date;
    }

    /**
     * @param mixed $Grade
     */
    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function confirmed()
    {
        $this->read = 1;
    }

    public function getEndDate()
    {
        return $this->ends_at;
    }

    public function  getTranslations()
    {
        return $this->translations;
    }
    
}
