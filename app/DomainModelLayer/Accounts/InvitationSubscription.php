<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\Invitation;
use Analogue\ORM\Entity;

class InvitationSubscription extends Entity
{

    public function __construct(Subscription $subscription,Invitation $invitation)
    {
        $this->subscription = $subscription;
        $this->invitation = $invitation;
    }

    public function getInvitation()
    {
    	return $this->invitation;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }
    
}