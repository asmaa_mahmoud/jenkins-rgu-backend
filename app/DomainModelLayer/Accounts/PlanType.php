<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class PlanType extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getPlans(){
        return $this->plans;
    }

}