<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use LaravelLocalization;

class WorldCountry extends Entity
{
    public function getId(){
        return $this->id;
    }

    public function getFlag(){
        return $this->flag_image;
    }

    public function translations(){
        return $this->translations;
    }

    public function getName(){
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getName();

            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getName();
        }
        return $english_translation;
    }

    public function getGamesWon(){
        return $this->games_won;
    }

    public function getRank(){
        return $this->rank;
    }

    public function getPoints(){
        return $this->points;
    }

    public function getMatchesPlayed(){
        return $this->matches_played;
    }

    public function getParticipation(){
        return $this->participation;
    }

    public function getGoalsScore(){
        return $this->goals_score;
    }

    public function GetGID(){
        return $this->gid;
    }


}