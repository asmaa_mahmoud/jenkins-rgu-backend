<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use LaravelLocalization;

class Extra extends Entity
{
    public function getId(){
        return $this->id;
    }



    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $translations = $this->translations;
        $english_translation = "";
        foreach ($translations as $translation) {
            if($translation->getLanguageCode() == $locale)
                return $translation->getDescription();
            elseif($translation->getLanguageCode() == 'en')
                $english_translation = $translation->getTitle();
        }
        return $english_translation;
    }


    public function getExtraUnlockable(){
        return $this->extra_unlockable;
    }
    public function getXpToUnlock(){
        return $this->getExtraUnlockable()->getUnlockable()->getXpToUnlock();
    }

    public function getCoinsToUnlock(){
        return $this->getExtraUnlockable()->getUnlockable()->getCoinsToUnlock();
    }

    public function getCostToUnlock(){
        return $this->getExtraUnlockable()->getUnlockable()->getCostToUnlock();
    }

}