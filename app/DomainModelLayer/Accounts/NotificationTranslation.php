<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use App\DomainModelLayer\Accounts\Notification;
use App\ApplicationLayer\Accounts\Dtos\NotificationDto;

class NotificationTranslation extends Entity
{

    public function __construct(NotificationDto $notificationDto,$language_code, Notification $notification)
    {
        $this->title = $notificationDto->Title;
        $this->message = $notificationDto->Message;
        $this->language_code = $language_code;
        $this->notification = $notification;
    }

    public function getLanguageCode()
    {
        return $this->language_code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getMessage()
    {
        return $this->message;
    }

}
