<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;

class WorldUserPrediction extends Entity
{
    public function __construct(User $user, WorldMatch $match, $winner){
        $this->user = $user;
        $this->match = $match;
        $this->winner = $winner;
    }

    public function getUser(){
        return $this->user;
    }

    public function setUser($user){
        $this->user = $user;
    }

    public function getMatch(){
        return $this->match;
    }

    public function setMatch($match){
        $this->match = $match;
    }

    public function getWinner(){
        return $this->winner;
    }

    public function setWinner($winner){
        $this->winner = $winner;
    }

    public function getCounter(){
        return $this->prediction_counter;
    }

    public function setCounter($predictionCounter){
        $this->prediction_counter = $predictionCounter;
    }

    public function getScored(){
        return $this->scored;
    }

    public function setScored($scored){
        $this->scored = $scored;
    }


}