<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class PlanTypeMap extends EntityMap
{
    protected $table = 'plan_type';
    public $timestamps = false;

    public function plans(PlanType $planType)
    {
        return $this->hasMany($planType, Plan::class, 'type_id', 'id');
    }

}