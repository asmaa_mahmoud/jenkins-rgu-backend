<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/13/17
 * Time: 1:45 PM
 */
namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\Entity;
use Analogue\ORM\EntityCollection;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\Journey;

class UserModelAnswer extends Entity
{

    public function __construct(User $user,Mission $mission,$modelAnswer)
    {
        $this->user = $user;
        $this->mission = $mission;
        $this->model_answer = $modelAnswer;
    }

    public function getUser(){
        return $this->user;
    }

    public function getMission(){
        return $this->mission;
    }

    public function getModelAnswer(){
        return $this->model_answer;
    }



}