<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class WorldUserPredictionMap extends EntityMap
{
    protected $table = 'user_predictions';
    public $softDeletes = false;
    public $timestamps = true;

    public function user(WorldUserPrediction $userPrediction){
        return $this->belongsTo($userPrediction, User::class , 'user_id', 'id');
    }

    public function match(WorldUserPrediction $userPrediction){
        return $this->belongsTo($userPrediction, WorldMatch::class , 'match_id', 'id');
    }


}