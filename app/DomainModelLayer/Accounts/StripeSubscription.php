<?php

namespace App\DomainModelLayer\Accounts;

use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\PlanPeriod;
use Analogue\ORM\Entity;

class StripeSubscription extends Entity
{

    public function __construct(Subscription $subscription,PlanPeriod $period,$stripe_id)
    {
        $this->subscription = $subscription;
        $this->period = $period;
        $this->stripe_id = $stripe_id;
    }

    public function getStripeId()
    {
    	return $this->stripe_id;
    }

    public function getPeriod()
    {
    	return $this->period->getName();
    }

    public function getSubscription(){
        return $this->subscription;
    }
    
}