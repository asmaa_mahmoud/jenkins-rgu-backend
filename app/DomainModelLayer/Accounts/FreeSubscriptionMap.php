<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\FreeSubscription;

class FreeSubscriptionMap extends EntityMap {

    protected $table = 'free_subscription';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "free_subscription.deleted_at";

    public function subscription(FreeSubscription $freeSubscription)
    {
        return $this->belongsTo($freeSubscription, Subscription::class , 'subscription_id', 'id');
    }

}