<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;

class PlanPeriodMap extends EntityMap {

    protected $table = 'plan_period';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "plan_period.deleted_at";


}