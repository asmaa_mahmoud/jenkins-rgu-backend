<?php

namespace App\DomainModelLayer\Accounts;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusUser;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Professional\LearningPathUser;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Schools\CampActivityProgress;
use App\DomainModelLayer\Schools\CampUser;
use App\DomainModelLayer\Schools\Classroom;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Schools\Grade;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\ CampusUserStatus;
use App\DomainModelLayer\Journeys\RankLevel;
use App\DomainModelLayer\Journeys\Rank;
use App\DomainModelLayer\Schools\StudentProgress;
use App\DomainModelLayer\Schools\StudentPosition;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Accounts\Role;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserRole;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\UserPosition;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Accounts\UserModelAnswer;

class UserMap extends EntityMap {

    protected $table = 'user';

    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "user.deleted_at";

   public function account(User $user)
   {
       return $this->belongsTo($user, Account::class, 'account_id', 'id');
   }

    public function language(User $user)
    {
        return $this->belongsTo($user, Language::class, 'language_id', 'id');
    }

    public function roles(User $user)
    {
        return $this->belongsToMany($user,Role::class,'user_role','user_id','role_id');
    }

    public function grades(User $user)
    {
        return $this->belongsToMany($user,Grade::class,'student_grade','user_id','grade_id');
    }

    public function classroomMembers(User $user)
    {
        return $this->hasMany($user, ClassroomMember::class, 'user_id','id');
    }

    public function groupMembers(User $user)
    {
        return $this->hasMany($user, GroupMember::class, 'user_id','id');
    }

    public function progress(User $user)
    {
        return $this->hasMany($user, TaskProgress::class, 'user_id', 'id');
    }

    public function studentprogress(User $user)
    {
        return $this->hasMany($user, StudentProgress::class, 'student_id', 'id');
    }

    public function notifications(User $user)
    {
        return $this->hasMany($user, Notification::class, 'user_id', 'id');
    }

    public function userNotifications(User $user)
    {
        return $this->hasMany($user, UserNotification::class, 'user_id', 'id')->whereNull('read_at')->orderBy('created_at', 'desc');
    }

    public function positions(User $user)
    {
        return $this->hasMany($user, UserPosition::class, 'user_id', 'id');
    }

    public function userscore(User $user)
    {
        return $this->hasOne($user, UserScore::class, 'user_id', 'id');
    }

    public function studentpositions(User $user)
    {
        return $this->hasMany($user, StudentPosition::class, 'user_id', 'id');
    }

    public function modelAnswersUnlocked(User $user)
    {
        return $this->hasMany($user, ModelAnswerUnlocked::class, 'user_id', 'id');
    }

    public function userModelAnswers(User $user)
    {
        return $this->hasMany($user, UserModelAnswer::class, 'user_id', 'id');
    }

    public function campProgress(User $user){
        return $this->hasMany($user, CampActivityProgress::class, 'user_id', 'id');
    }

    public function campusProgress(User $user){
        return $this->hasMany($user, CampusActivityProgress::class, 'user_id', 'id');
    }

    public function worldDetails(User $user){
        return $this->hasOne($user, WorldUserDetails::class, 'user_id', 'id');
    }

    public function worldScore(User $user){
        return $this->hasOne($user, WorldUserScore::class, 'user_id', 'id');
    }

    public function camps(User $user){
        return $this->belongsToMany($user,Camp::class,'camp_user','user_id','camp_id');
    }

    public function campUser(User $user){
        return $this->hasMany($user, CampUser::class, 'user_id', 'id');
    }

    public function learningPaths(User $user){
        return $this->belongsToMany($user,LearningPath::class,'learning_path_user','user_id','learning_path_id');
    }

    public function learningPathUser(User $user){
        return $this->hasMany($user, LearningPathUser::class, 'user_id', 'id');
    }

    public function campusRounds(User $user){
        return $this->belongsToMany($user,CampusRound::class,'campus_user','user_id','campus_round_id');
    }

    public function campusUsers(User $user){
        return $this->hasMany($user, CampusUser::class, 'user_id', 'id');
    }

    public function tasksCurrentSteps(User $user){
        return $this->hasMany($user, TaskCurrentStep::class, 'user_id');
    }

    public function campusUserStatus(User $user){
        return $this->hasMany($user,CampusUserStatus::class, 'user_id');
    }


}
