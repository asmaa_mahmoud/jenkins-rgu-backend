<?php

namespace App\Http\Controllers\Gees;

use App\Gees\ClassRoomJourneys;
use App\Gees\ClassRoomMembers;
use App\Gees\MissionProgress;
use App\Gees\Classroom;
use App\Gees\Journey;
use App\Gees\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ClassRoomController extends Controller
{
    public function getClassRoomsByTeacherId(Request $request)
    {
        $classroomIds = ClassRoomMembers::where('user_id',$request->id)->get();
        $results = array();
        $counter = 0;
        foreach($classroomIds as $classroomId)
        {
            $results[$counter]['data'] = Classroom::findOrFail($classroomId->classroom_id);
            $results[$counter]['progress'] = self::getMissionProgressForStudent($classroomId->classroom_id,$request->id);
            $results[$counter]['students'] = $this->getNumberofStudentsForClassRoom($classroomId->classroom_id);
            $results[$counter]['teachers'] = $this->getTeachersForClassRoom($classroomId->classroom_id);
            $results[$counter]['students_data'] = $this->getStudentsForClassRoom($classroomId->classroom_id);
            $results[$counter]['journeys'] = $this->getJourneysForClassRoom($classroomId->classroom_id);
            $results[$counter]['grade'] = $this->getGradeByClassroomId($classroomId->classroom_id);

            $counter++;
        }
        return $results;
    }

    public function getClassRoomsByAdminId(Request $request){
        $accountId = User::where('id',$request->id)->first()->Account_Id;
        $users = User::where('Account_Id',$accountId)->get();
        $results = array();
        $check = array();
        $counter = 0;
        foreach($users as $user)
        {
            if($user->Role()->first()->Name == 'Teacher' || $user->Role()->first()->Name == 'SchoolAdmin' )
            {
                $classroomIds = ClassRoomMembers::where('user_id',$user->id)->get();
                foreach($classroomIds as $classroomId)
                {
                    if(!in_array($classroomId->classroom_id,$check))
                    {
                        $results[$counter]['teachers'] = $this->getTeachersForClassRoom($classroomId->classroom_id);
                        $results[$counter]['data'] = Classroom::findOrFail($classroomId->classroom_id);
                        $results[$counter]['students'] = $this->getNumberofStudentsForClassRoom($classroomId->classroom_id);
                        $results[$counter]['students_data'] = $this->getStudentsForClassRoom($classroomId->classroom_id);
                        $results[$counter]['journeys'] = $this->getJourneysForClassRoom($classroomId->classroom_id);
                        $results[$counter]['grade'] = $this->getGradeByClassroomId($classroomId->classroom_id);
                        array_push($check,$classroomId->classroom_id);
                        $counter++;
                    }
                }
            }
        }
        return $results;
    }

    public function getJourneysForClassRoom($id)
    {
        $classroom_journeys = ClassRoomJourneys::where('classroom_id',$id)->get()->all();
        $results = array();

        foreach($classroom_journeys as $classroom_journey)
        {
            array_push($results,Journey::where('id',$classroom_journey->journey_id)->first());
        }
        return $results;
    }

    public function getNumberofStudentsForClassRoom($id)
    {
        $classrooms = ClassRoomMembers::where('classroom_id',$id)->get();
        $counter = 0;
        $check = array();
        foreach($classrooms as $classroom)
        {
            $user = User::where('id',$classroom->user_id)->first();
                if($user->Role()->first()->id == 2)
                {
                    if(!in_array($user,$check))
                    {
                        $counter++;
                        array_push($check,$user);
                    }
                }
        }
        return $counter;
    }

    public function getTeachersForClassRoom($id)
    {
        $classrooms = ClassRoomMembers::where('classroom_id',$id)->get();
        $results = array();
        foreach($classrooms as $classroom)
        {
            $user = User::where('id',$classroom->user_id)->first();
            if($user->Role()->first()->id == 3) array_push($results,$user);
        }
        return $results;
    }

    public function getStudentsForClassRoom($id)
    {
        $classrooms = ClassRoomMembers::where('classroom_id',$id)->get();
        $results = array();
        foreach($classrooms as $classroom)
        {
            $user = User::where('id',$classroom->user_id)->first();
            if($user->Role()->first()->id == 2) array_push($results,$user);
        }
        return $results;
    }

    public function createClassRoom(Request $request)
    {
        $classroom = new Classroom();
        $classroom->name = $request['name'];
        $classroom->grade = $request['grade'];
        if($classroom->save())
        {
            foreach($request['members'] as $member)
            {
                $classroom->Members()->attach($member);
            }

            foreach($request['journeys'] as $journey)
            {
                $classroom->Journeys()->attach($journey);
            }

            return trans('locale.success');
        }
        return trans('locale.failed');

    }

    public function getGradeByClassroomId($id)
    {
        return Classroom::findOrFail($id)->Grade()->first();
    }

    public static function getMissionProgressForStudent($id,$studentId)
    {
        $score = 0;
        $missionsInClassRoom = MissionsController::getMissionsByClassroom($id);
        if (sizeof($missionsInClassRoom) != 0) {
            for ($i = 0; $i < sizeof($missionsInClassRoom); $i++) {
                $missionsWithUser = MissionProgress::where('Mission_Id', '=', $missionsInClassRoom[$i])
                    ->where('User_Id', '=', $studentId)->get()->toArray();
                if (!empty($missionsWithUser)){
                    $score += 1;
                }
            }
            $progressValue = intval($score/sizeof($missionsInClassRoom) * 100);
            return $progressValue;
        }
          return 0;
        }
    }

