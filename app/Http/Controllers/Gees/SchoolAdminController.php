<?php

namespace App\Http\Controllers\Gees;

use App\Gees\Account;
use App\Gees\Classroom;
use App\Gees\ClassRoomJourneys;
use App\Gees\ClassRoomMembers;
use App\Gees\EnrolledJourneys;
use App\Gees\Grade;
use App\Gees\Journey;
use App\Gees\School;
use App\Gees\Distributor;
use App\Gees\Country;
use App\Gees\Subscription;
use App\Gees\SchoolPlanSpecial;
use App\Http\Controllers\AccountTypeController;
use App\Http\Controllers\Controller;
use App\Gees\User;
use App\Gees\Plan;
use App\Gees\Code;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Gees\JourneyTranslation;
use Mail;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class SchoolAdminController extends Controller
{
    public function createSchoolAccount(Request $request)
    {
        $v = Validator::make($request->all(), [
            'PrimaryContact_FirstName' => 'required',
            'PrimaryContact_LastName' => 'required',
            'PrimaryContact_Email' => 'required|email',
            'password' => 'required|alpha_dash|min:8',
            'Name' => 'required',
            'plan_id' => 'required',
            'school_name' => 'required',
            'country' => 'required',
            'school_email' => 'required|email',
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        DB::transaction(function () use ($request){
            $plan = Plan::where('id',$request['plan_id'])->first();
            if($plan == null)
                throw new BadRequestException("Plan doesn't exist");
//            $code = Code::where('given',0)->first();
//            if($code == null)
//                throw new BadRequestException("No code found");
            $account = new Account();

            $account->Name = $request['Name'];
            $account->AccountType_Id = 8;
            $account->PrimaryContact_FirstName = $request['PrimaryContact_FirstName'];
            $account->PrimaryContact_LastName = $request['PrimaryContact_LastName'];
            $account->PrimaryContact_Email = $request['PrimaryContact_Email'];
            $account->isActive = 0;

            if($account->save())
            {
                $subscription = new Subscription();
                $subscription->StartDate = Carbon::now();
                $subscription->PlansHistory_Id = $request['plan_id'];
                $subscription->Account_Id = $account->id;
                $subscription->Period = 'monthly';
                $subscription->save();

                $journeys = Journey::all()->all();
                foreach($journeys as $journey)
                {
                    $enroll = new EnrolledJourneys();
                    $enroll->subscription_Id = $subscription->id;
                    $enroll->journey_Id = $journey['id'];
                    $enroll->save();
                }



                $admin = new User();
                $admin->FirstName = $request['PrimaryContact_FirstName'];
                $admin->LastName = $request['PrimaryContact_LastName'];
                $admin->CreatedBy = $request['Name'];
                $admin->Account_id = $account->id;
                $admin->password =  Hash::make($request['password']);
                $admin->username = $request['Name'];

                if($admin->save()){
                    $admin->Role()->attach(4);

                    $school = new School();
                    $school->admin_Id = $admin->id;
                    $school->name = $request['school_name'];
                    $school->country = $request['country'];
                    $school->logo = $request['logo'];
                    $school->address = $request['address'];
                    $school->website = $request['website'];
                    $school->email = $request['school_email'];

                    if($school->save())
                    {
//                        $code->given = 1;
//                        $code->save();
                        return $this->handleResponse($account);
                    }
                    else{
                        throw new BadRequestException(trans('locale.school_creation_failed'));
                    }
                    
                }
                else{
                        throw new BadRequestException(trans('locale.admin_creation_failed'));
                    }
                
            }
            else{
                    throw new BadRequestException(trans('locale.account_creation_failed'));
                }
            
        });

    }

    public function createTeacher(Request $request)
    {

        $admin = User::findOrFail($request->id);

        $user = User::where('username',$request['username'])->first();
        if($user != null)return trans('locale.failed');

        $teacher = new User();
        $teacher->FirstName = $request['FirstName'];
        $teacher->LastName = $request['LastName'];
        $teacher->CreatedBy = $admin->Name;
        $teacher->Account_id = $admin->Account_Id;
        $teacher->password =  Hash::make($request['password']);
        $teacher->username = $request['username'];
        $teacher->Email = $request['Email'];
        $teacher->gender = $request['gender'];
        if($request['gender'] == 0)
        $teacher->ProfileImageUrl = 'https://robogarden.s3.amazonaws.com/female.png';
        else $teacher->ProfileImageUrl = 'https://robogarden.s3.amazonaws.com/male.png';
        if($teacher->save()) {
            $teacher->Role()->attach(3);
            if($request->has('grades'))
            {
                foreach($request['grades'] as $grade)
                {
                    $teacher->Grade()->attach($grade->id);
                }
            }
            return trans('locale.success');
        }
        return trans('locale.failed');
    }

    public function getTeachersByAdminId(Request $request)
    {
        $admin = User::findOrFail($request->id);
        $users = User::where('Account_Id',$admin->Account_Id)->get();
        $results = array();
        $counter = 0;
        foreach($users as $user)
        {
            if($user->Role()->first()->Name == 'Teacher')
            {
                $results[$counter]['grades'] = $user->Grade()->get();
                $results[$counter]['data'] = $user;
                $classrooms = ClassRoomMembers::where('user_id',$user->id)->get();
                $results[$counter]['classrooms'] = array();
                $results[$counter]['classrooms_id'] = array();
                foreach($classrooms as $classroom)
                {
                    array_push($results[$counter]['classrooms'],Classroom::where('id',$classroom->classroom_id)->first()->name);
                    array_push($results[$counter]['classrooms_id'],Classroom::where('id',$classroom->classroom_id)->first()->id);
                }
                $counter++;
            }
        }
        return $results;
    }

    public function createClassroom(Request $request)
    {
        $admin = User::where('id',$request->id)->first();
        $classroom = new Classroom();
        $classroom->name = $request['name'];
        $classroom->grade = null;

        if($classroom->save())
        {
            $classroom->Members()->attach($request->id);

            if($request->has('journeys'))
            {
                $account = Account::where('id',$admin->Account_Id)->first();
                $subscription = Subscription::where('Account_Id',$account->id)->first();
                $plan = Plan::where('id',$subscription->PlansHistory_id)->first();
                $max_journeys = 1000000;
                if($plan->NewName == 'Primary')
                    $max_journeys = $this->primary_courses;
                else if($plan->NewName == 'Advanced')
                    $max_journeys = $this->advanced_courses;
                else if($plan->NewName == 'Junior')
                    $max_journeys = $this->junior_courses;
                else if($plan->NewName == 'Combined')
                    $max_journeys = $this->combined_courses;
                $other_journeys = ClassRoomJourneys::whereIn('classroom_id',ClassRoomMembers::where('user_id',$admin->id)->pluck('classroom_id')->toArray())->get();
                if(count($other_journeys) >= $max_journeys)
                    return trans('locale.failure');
                else
                   $max_journeys = $max_journeys - count($other_journeys);
                foreach ($request['journeys'] as $journey)
                {
                    if(count($classroom->Journeys) > $max_journeys)
                        return trans('locale.failure');
                    $classroom->Journeys()->attach($journey['id']);
                }
            }

            if($request->has('grades'))
            {
                foreach($request['grades'] as $grade)
                {
                    $classroom->Grade()->attach($grade['id']);
                }
            }

            return trans('locale.success');
        }
        return trans('locale.failed');
    }

    public function getJourneysByAdminId(Request $request)
    {
        $user = User::findOrFail($request->id);
        $accountId = $user->Account_Id;
        $subscription = Subscription::where('Account_Id',$accountId)->first()->Journey()->get();
        $journeysArray = array();
        for ($i = 0; $i < sizeof($subscription); $i++) {
            $journeyTitle = JourneyTranslation::where('Journey_Id', '=', $subscription[$i]->id)
                ->where('language_code', '=', 'en')->get(['title']);
            $journeyInfo = (object)[
                'id' => $subscription[$i]->id ,
                'cardIconURL' => $subscription[$i]->cardIconURL,
                'journeycategory_id' => $subscription[$i]->journeycategory_id,
                'title' => $journeyTitle[0]->title,
                'pivot' => $subscription[$i]->pivot,
            ];
            array_push($journeysArray,$journeyInfo);
        }
        return $journeysArray;
    }

    public function createStudent(Request $request)
    {

        $admin = User::findOrFail($request->id);

        $teacher = new User();
        $teacher->FirstName = $request['FirstName'];
        $teacher->LastName = $request['LastName'];
        $teacher->CreatedBy = $admin->Name;
        $teacher->Account_id = $admin->Account_Id;
        $teacher->password =  Hash::make($request['password']);
        $teacher->username = $request['username'];
        if($teacher->save()) {
            $teacher->Role()->attach(2);
            return trans('locale.success');
        }
        return trans('locale.failed');
    }

    public function getStudentsByAdminId(Request $request)
    {
        $admin = User::findOrFail($request->id);
        $users = User::where('Account_Id',$admin->Account_Id)->get();
        $results = array();
        $counter = 0;
        foreach($users as $user)
        {
            if($user->Role()->first()->Name == 'Student')
            {
                $results[$counter]['data'] = $user;
                $results[$counter]['data']['grade'] = $user->Grade()->first()['name'];
                $classrooms = ClassRoomMembers::where('user_id',$user->id)->get();
                $results[$counter]['classrooms'] = array();
                $results[$counter]['classrooms_id'] = array();
                foreach($classrooms as $classroom)
                {
                    array_push($results[$counter]['classrooms'],Classroom::where('id',$classroom->classroom_id)->first()->name);
                    array_push($results[$counter]['classrooms_id'],Classroom::where('id',$classroom->classroom_id)->first()->id);
                }
                $counter++;
            }
        }
        return $results;
    }

    public function uploadStudents(Request $request)
    {
        $errors = array();
        $admin = User::findOrFail($request->id);
          $data = json_encode(Excel::load($request->file,function($reader){
             $results = $reader->get();
             return $results;
         }));

          $data = json_decode($data,true);
          $data = $data['parsed'];

          foreach($data as $row)
          {
              $user = User::where('username',$row['username'])->first();
              if($user != null)
              {
                  array_push($errors,$user);
                  continue;
              }

              $teacher = new User();
              $teacher->FirstName = $row['firstname'];
              $teacher->LastName = $row['lastname'];
              $teacher->CreatedBy = $admin->Name;
              $teacher->Account_id = $admin->Account_Id;
              $teacher->password =  Hash::make($row['password']);
              $teacher->username = $row['username'];
              $teacher->ProfileImageUrl = $row['image'];
              if($teacher->save()) {
                  $teacher->Role()->attach(2);
                  if(isset($row['grade']) && $row['grade'] > 0 && $row['grade'] < 10)
                  {
                      $teacher->Grade()->attach($row['grade']);
                  }
              }
          }

          Excel::create('Errors',function($excel) use($errors){
                $excel->sheet('Errors',function($sheet) use($errors){
                    $sheet->fromArray($errors);
                });
          })->export('xls');
          return trans('locale.success');
    }

    public function deleteStudent(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->Role()->detach();
        $user->Classroom()->detach();
        $user->Grade()->detach();
        if($user->delete())return trans('locale.success');
        return trans('locale.failed');
    }

    public function deleteClassroom(Request $request)
    {
        $classroom = Classroom::findOrFail($request->id);
        $classroom->Grade()->detach();
        $classroom->Journeys()->detach();
        $classroom->Members()->detach();
        if($classroom->delete())return trans('locale.success');
        return trans('locale.failed');
    }

    public function getAllGrades(Request $request)
    {
        $admin = User::findOrFail($request->id);
        $users = array();
        $response = array();
        $results = Grade::all();
        $counter = 0;
        foreach($results as $result)
        {
            $response[$counter]['grade'] = $result;
            $temps = $result->User()->get();
            foreach($temps as $temp)
            {
                if($temp->Account_Id == $admin->Account_Id)
                    array_push($users,$temp);

            }
            $response[$counter]['users'] = $users;
            $users = array();
            $response[$counter]['classrooms'] = $result->Classroom()->get();
            $counter++;
        }
        return $response;
    }

    public function assignTeacher(Request $request)
    {
        $classroom = Classroom::where('id',$request->classroom_id)->first();
        foreach ($request['teachers'] as $teacher)
        {
            $classroom->Members()->attach($teacher['id']);
        }
        return trans('locale.success');
    }

    public function enrollStudent(Request $request)
    {
        $user = User::findOrFail($request->id);
        $account = Account::where('id',$user->Account_Id)->first();
        $subscription = Subscription::where('Account_Id',$account->id)->first();
        $plan = Plan::where('id',$subscription->PlansHistory_id)->first();
        $max_students = 1000000;
        if($plan->NewName == 'Primary')
            $max_students = $this->primary_students;
        else if($plan->NewName == 'Advanced')
            $max_students = $this->advanced_students;
        else if($plan->NewName == 'Junior')
            $max_students = $this->junior_students;
        else if($plan->NewName == 'Combined')
            $max_students = $this->combined_students;

        $classroom = Classroom::where('id',$request->classroom_id)->first();
        foreach ($request['students'] as $student)
        {
            if(count($classroom->Members) > $max_students)
                return trans('locale.failure');
            $classroom->Members()->attach($student['id']);
        }
        return trans('locale.success');
    }

    public function getCountries()
    {
        return Country::all()->all();
    }

    public function getDistributors($country_name = null)
    {
        if($country_name == null)
            return Distributor::all()->all();
        else
        {
            $country = Country::where('country_name',$country_name)->first();
            if($country == null)
                throw new BadRequestException("Country Not Found");
            return Distributor::where('country_id',$country->id)->orWhere('default_distributor',1)->get();
        }
    }

    public function getSchoolPlansSpecial()
    {
        return SchoolPlanSpecial::with('plan')->get();
    }

    public function isActivatedAccount($username)
    {
        $user = User::where('username',$username)->first();
        if($user == null)
           throw new BadRequestException(trans('locale.user_not_found'));
       $account = Account::where('id',$user->Account_Id)->first();
        if($account == null)
           throw new BadRequestException(trans('locale.account_not_found'));

       return Response::json(($account->isActive != null)? $account->isActive:false,200);
           
    }

    public function activateAccount(Request $request)
    {
         $v = Validator::make($request->all(), [
            'username' => 'required',
            'code' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        DB::transaction(function () use ($request){

            $user = User::where('username',$request['username'])->first();
            if($user == null)
               throw new BadRequestException(trans('locale.user_not_found'));
           $account = Account::where('id',$user->Account_Id)->first();
            if($account == null)
               throw new BadRequestException(trans('locale.account_not_found'));

           $code = Code::where('given',1)->where('used',0)->where('code',$request['code'])->first();
           if($code == null)
               throw new BadRequestException(trans('locale.code_invalid'));
           $account->isActive = 1;
           $account->save();
           $code->used = 1;
           $code->save();
           return Response::json($account,200);

       });
    }

    public function getJourneyName( $request)
    {
//        $v = Validator::make($request->all(), [
//            'journeyId' => 'required|integer|exists:journey,id',
//        ]);
//        if ($v->fails()) {
//            return $v->errors()->all();
//        } else {
//            $id = $request['journeyId'];
            $journeyTitle = JourneyTranslation::where('Journey_Id', '=', $request)
                ->where('language_code', '=', 'en')->get(['title']);
            return $journeyTitle[0];
//        }
    }

    public function getPlanByAdminId($request)
    {
        $admin = User::findOrFail($request);
        $account = Account::where('id',$admin->Account_Id)->first();
        $subscription = Subscription::where('Account_Id',$account->id)->first();
        return Plan::where('id',$subscription->PlansHistory_id)->first();

    }

}
