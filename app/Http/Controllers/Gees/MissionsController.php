<?php

namespace App\Http\Controllers\Gees;

use App\Gees\Quiz;
use App\Gees\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gees\Classroom;
use App\Gees\Mission;
use App\Gees\MissionProgress;
use App\Gees\JourneyTranslation;
use App\Gees\AdventureTranslations;
use Illuminate\Support\Facades\Validator;
use Response;

class MissionsController extends Controller
{

    public function getAdventuresForJourneys(Request $request)
    {
        $v = Validator::make(['id' => $request['classroom_id']], [
            'id' => 'required|integer|exists:classroom,id',
        ]);
        if ($v->fails())
        {
            return $v->errors()->all();
        }else {
            $id = $request['classroom_id'];
            $journeysArray = array();
            $adventures = Classroom::with('Journeys.Adventures')->where('id', '=', $id)->findOrFail($id);

                for ($i = 0; $i < sizeof($adventures->Journeys); $i++) {
                    $adventuresArray = array();
                    for ($index = 0; $index < sizeof($adventures->Journeys[$i]->Adventures); $index++) {
                        $adventureTitle = AdventureTranslations::where('Adventure_Id', '=', $adventures->Journeys[$i]->Adventures[$index]->id)
                            ->where('language_code', '=', 'en')->get(['title']);

                        $journeyInfo = (object)[
                            'id' => $adventures->Journeys[$i]->Adventures[$index]->id ,
                            'cardIconURL' => $adventures->Journeys[$i]->Adventures[$index]->roadmapImageURL,
                            'Journey_Id' => $adventures->Journeys[$i]->Adventures[$index]->Journey_Id,
                            'order' => $adventures->Journeys[$i]->Adventures[$index]->order,
                            'title' => $adventureTitle[0]->title,
                        ];
                        array_push($adventuresArray,$journeyInfo);
                    }
                    $journeyTitle = JourneyTranslation::where('Journey_Id', '=', $adventures->Journeys[$i]->id)
                        ->where('language_code', '=', 'en')->get(['title']);
                    $journeyInfo = (object)[
                        'id' => $adventures->Journeys[$i]->id ,
                        'cardIconURL' => $adventures->Journeys[$i]->cardIconURL,
                        'journeycategory_id' => $adventures->Journeys[$i]->journeycategory_id,
                        'title' => $journeyTitle[0]->title,
                        'adventures' => $adventuresArray
                    ];
                    array_push($journeysArray,$journeyInfo);
                }
            $adventureObject = (object)[
                'id' => $adventures->id ,
                'name' => $adventures->name,
                'grade' => $adventures->grade,
                'journeys' => $journeysArray,
            ];
            return Response::json($adventureObject);
        }
    }

    public function getNotPlayedMissions(Request $request)
    {
        $v = Validator::make(['id' => $request['classroom_id']], [
            'id' => 'required|integer|exists:classroom,id',
        ]);
        if ($v->fails())
        {
            return $v->errors()->all();
        }else {
            $id = $request['classroom_id'];

            //Students in this Classroom
            $studentIds = self::getStudentInClass($id);

            //Missions in this class
            $missionsInClassroomArray = self::getMissionsByClassroom($id);

            //Played Missions in this Class
            $playedMission = MissionProgress::whereIn('Mission_Id', $missionsInClassroomArray)
                ->whereIn('User_Id',$studentIds)->pluck("Mission_Id")->toArray();

            //Not Played Missions in this Class
            $notPlayedMissions = Mission::whereIn('id', $missionsInClassroomArray)->whereNotIn('id',$playedMission)->get();

            return $notPlayedMissions;
        }
    }

    public function getPlayedMissionsByAllStudents(Request $request)
    {
        $v = Validator::make(['id' => $request['classroom_id']], [
            'id' => 'required|integer|exists:classroom,id',
        ]);
        if ($v->fails()) {
            return $v->errors()->all();
        } else {
            $completedMissionsByAllStudents = array();
            $id = $request['classroom_id'];

            //Students in this Classroom
            $studentIds = self::getStudentInClass($id);

            $playedMissionIDs = MissionProgress::pluck("Mission_Id");
            $missionsPlayedInClass = Mission::with(['Adventures.Journeys.Classroom' => function ($query) use ($id){
                $query->where('classroom.id', '=', $id);
            }])->whereHas('Adventures.Journeys.Classroom')->whereIn('id', $playedMissionIDs)->pluck("id");

            for ($i = 0; $i < sizeof($missionsPlayedInClass); $i++){
                for ($index = 0; $index < sizeof($studentIds); $index++){
                    $missionsWithUser = MissionProgress::where('Mission_Id', '=', $missionsPlayedInClass[$i])
                        ->where('User_Id', '=', $studentIds[$index])->get()->toArray();
                    if (empty($missionsWithUser)){
                        break;
                    }
                }
                if ($index == sizeof($studentIds) ){
                    $missionInfo = Mission::findOrFail($missionsPlayedInClass[$i]);
                    array_push($completedMissionsByAllStudents,$missionInfo);
                }
            }
            return $completedMissionsByAllStudents;
        }
    }

    public function getStudentWithMissionsAndQuizzes(Request $request)
    {
        $v = Validator::make($request->all(), [
            'classroom_id' => 'required|integer|exists:classroom,id',
            'Adventure_id' => 'required|integer|exists:mission,Adventure_id',
        ]);
        if ($v->fails()) {
            return $v->errors()->all();
        } else {
            $missionsInClassroomArray = array();
            $quizzesInClassroomArray = array();
            $studentMissionsQuizzesArray = array();
            $id = $request['classroom_id'];
            $adventureId = $request['Adventure_id'];
            //Students in this Classroom
            $studentIds = self::getStudentInClass($id);
            //Missions in this class
            $missionsInClassroom = Mission::with(['Adventures.Journeys.Classroom' => function ($query) use ($id){
                $query->where('classroom.id', '=', $id);
            }])->whereHas('Adventures.Journeys.Classroom')->where('Adventure_id', '=',$adventureId)
                ->orderBy('Order')->get();

            for ($index = 0; $index < sizeof($missionsInClassroom); $index++) {
                if (!empty($missionsInClassroom[$index]->Adventures->Journeys->Classroom[0])) {
                    array_push($missionsInClassroomArray, $missionsInClassroom[$index]->id);
                }
            }
            //Quizzes in this Classroom
            $quizzesInClassroom = Quiz::with(['Adventures.Journeys.Classroom' => function ($query) use ($id){
                $query->where('classroom.id', '=', $id);
            }])->whereHas('Adventures.Journeys.Classroom')->where('Adventure_id', '=',$adventureId)->get();

            for ($index = 0; $index < sizeof($quizzesInClassroom); $index++) {
                if (!empty($quizzesInClassroom[$index]->Adventures->Journeys->Classroom[0])) {
                    array_push($quizzesInClassroomArray, $quizzesInClassroom[$index]->id);
                }
            }
            //Final Array
            for ($index = 0; $index < sizeof($studentIds); $index++) {
                $studentMissionArray = array();
                $studentQuizArray = array();
                //Missions in this classroom for every student with his score.
                for ($i = 0; $i < sizeof($missionsInClassroomArray); $i++) {
                    $missionInfo = Mission::with(['Played' => function ($query) use ($studentIds,$index) {
                        $query->where('User_Id', '=', $studentIds[$index]);
                    }])->findOrFail($missionsInClassroomArray[$i]);
                    $score = null;
                    if (sizeof($missionInfo->Played) == 0) {
                        $score = 0;
                    } else {
                        $score = $missionInfo->Played[0]->MissionScore;
                    }
                    $missionObject = (object)[
                        'id' => $missionInfo->id ,
                        'adventureId' => $missionInfo->Adventure_id,
                        'order' => $missionInfo->Order,
                        'modelAnswer' => $missionInfo->model_answer,
                        'iconURL' => $missionInfo->iconURL,
                        'score' => $score,
                    ];
                    array_push($studentMissionArray,$missionObject);
                }
                //Quizzes
                for ($j = 0; $j < sizeof($quizzesInClassroomArray); $j++) {
                    $quizInfo = Quiz::with(['QuizDetails' => function ($query) use ($studentIds,$index) {
                        $query->where('User_Id', '=', $studentIds[$index]);
                    }])->findOrFail($quizzesInClassroomArray[$j]);
                    $score = null ;
                    $noOfTrials = null;
                    if (sizeof($quizInfo->QuizDetails) == 0) {
                        $score = 0;
                        $numberOfTrials = 0;
                    } else {
                        $score = $quizInfo->QuizDetails[0]->QuizScore;
                        $numberOfTrials = $quizInfo->QuizDetails[0]->NoOfTrials;
                    }
                    $type = null;
                    if ($quizInfo->QuizType_Id == 1) {
                        $type = 'Quiz';
                    } elseif($quizInfo->QuizType_Id == 2){
                        $type = 'Tutorial';
                    }
                    $quizObject = (object)[
                        'id' => $quizInfo->id,
                        'adventureId' => $quizInfo->Adventure_id,
                        'quizType' => $type,
                        'iconURL' => $quizInfo->iconURL,
                        'score' => $score,
                        'numberOfTrials' => $numberOfTrials,
                    ];
                    array_push($studentQuizArray,$quizObject);
                }
                //Student Info with his missions and quizzes
                $userInfo = User::findOrFail($studentIds[$index]);
                $studentObject = (object)[
                    'id' => $userInfo->id ,
                    'studentName' => $userInfo->FirstName . " " . $userInfo->LastName,
                    'email' => $userInfo->Email,
                    'username' => $userInfo->username,
                    'missions' => $studentMissionArray,
                    'quizzes' => $studentQuizArray,
                ];
                array_push($studentMissionsQuizzesArray,$studentObject);
            }
            return $studentMissionsQuizzesArray;
        }
    }

    public static function getStudentIdsInClass(Request $request)
    {
        $v = Validator::make(['id' => $request['classroom_id']], [
            'id' => 'required|integer|exists:classroom,id',
        ]);
        if ($v->fails()) {
            return $v->errors()->all();
        } else {
            $id = $request['classroom_id'];
            $studentsIds = self::getStudentInClass($id);
            return $studentsIds;
        }
    }
    public static function getStudentInClass($id)
    {
        //Students in this Classroom
        $studentIds = array();
        $userIds = User::with(['Classroom' => function ($query) use ($id) {
            $query->where('classroom_id', '=', $id);
        }])->with(['Role' => function ($query) use ($id) {
            $query->where('Role_Id', '=', 2);
        }])->get();
        for ($index = 0; $index < sizeof($userIds); $index++){
            if (!$userIds[$index]->Classroom->isEmpty() && !$userIds[$index]->Role->isEmpty()) {
                array_push($studentIds,$userIds[$index]->id);
            }
        }
        return $studentIds;
    }

    public static function getMissionsByClassroom($id)
    {
        $missionsInClassroomArray = array();
        $missionsInClassroom = Mission::with(['Adventures.Journeys.Classroom' => function ($query) use ($id){
            $query->where('classroom.id', '=', $id);
        }])->whereHas('Adventures.Journeys.Classroom')->get();

        for ($index = 0; $index < sizeof($missionsInClassroom); $index++) {
            if (!empty($missionsInClassroom[$index]->Adventures->Journeys->Classroom[0])) {
                array_push($missionsInClassroomArray, $missionsInClassroom[$index]->id);
            }
        }
        return $missionsInClassroomArray;
    }

    public static function getQuizzesByClassroom($id)
    {
        $quizzesInClassroomArray = array();
        $quizzesInClassroom = Quiz::with(['Adventures.Journeys.Classroom' => function ($query) use ($id){
            $query->where('classroom.id', '=', $id);
        }])->whereHas('Adventures.Journeys.Classroom')->get();

        for ($index = 0; $index < sizeof($quizzesInClassroom); $index++) {
            if (!empty($quizzesInClassroom[$index]->Adventures->Journeys->Classroom[0])) {
                array_push($quizzesInClassroomArray, $quizzesInClassroom[$index]->id);
            }
        }
        return $quizzesInClassroomArray;
    }

    public static function getNumberOfStudentsForQuizInClassroom($id)
    {
        $quizzesInClassroom = self::getQuizzesByClassroom($id);
        $studentsInClassroom =  self::getStudentInClass($id);
        $quizzesStudentsArray = array();
        for($i=0;$i<sizeof($quizzesInClassroom);$i++)
        {
            $quiz = Quiz::where('id',$quizzesInClassroom[$i])->first();
            $counter = 0;
            for($j=0;$j<sizeof($studentsInClassroom);$j++)
            {

                if($quiz->User_Id == $studentsInClassroom[$j])
                {
                    $counter++;
                }
            }
            $quizzesStudentsArray[$i]['Quiz_id'] = $quiz->id;
            $quizzesStudentsArray[$i]['noOfStudents'] = $counter;
        }
        return $quizzesStudentsArray;

    }
}
