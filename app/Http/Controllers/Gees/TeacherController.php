<?php

namespace App\Http\Controllers\Gees;

use App\Gees\School;
use App\Gees\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Gees\ClassRoomMembers;
use App\Gees\Role;
use Illuminate\Support\Facades\Hash;
use DatabaseSeeder;

class TeacherController extends Controller
{
    public function test(Request $request)
    {
        //$schoolAdmin = new MissionsController();
//        return MissionsController::getNumberOfStudentsForQuizInClassroom($request->id);

        $parameters = array();
        $parameters['adminId'] = $request['adminId'];
        $parameters['schoolName'] = School::where('admin_Id',$request['adminId'])->first()->name;
        $parameters['accountId'] = User::where('id',$request['adminId'])->first()->Account_Id;
        $class = new DatabaseSeeder();
        $class->Classrooms($parameters);
        return trans('locale.done');
        //return $class->Missions($request['accountId'],$request['schoolName'],$request['adminId']);

    }

    public function getUserData(Request $request)
    {
        $user = User::findOrFail($request->id);
        return $user->Role()->first()->Name;
    }

    public function getPlayedMissions($classroom_id)
    {
    	// $students = User::whereIn('id',(ClassRoomMembers::where('classroom_id',$classroom_id)->whereIn()))
    	
    }

    public function getUnplayedMissions($classroom_id)
    {

    }

    public function getClassroomJourneys($classroom_id)
    {

    }

    public function getClassroomMissions($adventure_id)
    {

    }
}
