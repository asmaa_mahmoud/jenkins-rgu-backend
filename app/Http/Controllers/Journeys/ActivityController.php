<?php

namespace App\Http\Controllers\Journeys;

use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use App\ApplicationLayer\Schools\CustomMappers\SchoolDtoMapper;
use App\ApplicationLayer\Schools\Dtos\CampRequestDto;
use App\ApplicationLayer\Schools\Dtos\CustomPlanDto;
use App\ApplicationLayer\Schools\Dtos\DistributorSubscriptionDto;
use App\Helpers\Mapper;
use Illuminate\Http\Request;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    private $journeyService;

    public function __construct(IJourneyMainService $journeyService){
        $this->middleware('jwt.auth', ['except' =>['getFreeActivitiesAndJourneys', 'getAllActivitiesForB2CNonUser']]);
        $this->middleware('check-subscription', ['only' =>[]]);
        $this->middleware('allow-origin');
        $this->journeyService = $journeyService;
    }

    public function index(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $camp_id = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['camp_id'])){
            $camp_id = $_GET['camp_id'];
        }
        $response = $this->journeyService->getAllActivities($user->id, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getAllActivities(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $is_mobile = true;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['is_mobile'])){
            $is_mobile = $_GET['is_mobile'];
        }
        $response = $this->journeyService->getAllActivitiesForB2C($user->id, $start_from, $limit, $search, $is_mobile);
        return $this->handleResponse($response);
    }

    public function getFreeActivitiesAndJourneys(){
        $response = $this->journeyService->getFreeActivitiesAndJourneys();
        return $this->handleResponse($response);
    }

    public function getAllActivitiesForB2CNonUser(){
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $response = $this->journeyService->getAllActivitiesForB2CNonUser($start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getActivity(Request $request, $activity_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->getActivity($user->id, $activity_id, $request->is_default, $request->is_mobile ? false : true);
        return $this->handleResponse($response);
    }

    public function getActivityTasks(Request $request, $activity_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->getActivityTasks($user->id, $activity_id,$request->is_default,$request->is_mobile ? false : true);
        return $this->handleResponse($response);
    }

    public function countAllActivities(){
        $user = JWTAuth::toUser();
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->journeyService->countActivitiesForB2C($user->id,$search);
        return $this->handleResponse($response);
    }

    public function countAllMinecraftActivities(){
        $user = JWTAuth::toUser();
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->journeyService->countAllMinecraftActivities($user->id,$search);
        return $this->handleResponse($response);
    }

    public function getAllMinecraftActivities(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $is_mobile = true;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['is_mobile'])){
            $is_mobile = $_GET['is_mobile'];
        }

        $response = $this->journeyService->getAllMinecraftActivities($user->id, $start_from, $limit, $search, $is_mobile);
        return $this->handleResponse($response);
    }

    public function unlockActivityWithCoins(Request $request, $activity_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
            'coins' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->unlockActivityWithCoins($user->id, $activity_id, $request->is_default, $request->coins, $request->is_mobile ? false : true);
        return $this->handleResponse($response);
    }

}