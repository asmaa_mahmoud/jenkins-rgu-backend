<?php

namespace App\Http\Controllers;

use App\ApplicationLayer\Missions\IMissionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BlocklyController extends Controller
{
    private $missionService;

    public function __construct(IMissionService $missionService){
        $this->missionService = $missionService;
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    // public function getMissionToolbox($missionID){        
    //     $response = $this->blocklySerivce->getMissionToolbox($missionID);
    //     return $this->handleResponse($response);
    // }

    public function getBlocksDefinitions()
    {
        $response = $this->missionService->getBlocksDefinitions();
        return $this->handleResponse($response);
    }
}
