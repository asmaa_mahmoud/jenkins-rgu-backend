<?php

namespace App\Http\Controllers\Journeys;

use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Controllers\Controller;

class JourneysController extends Controller
{

    private $journeyService;

    public function __construct(IJourneyMainService $journeyMainService){
        $this->middleware('jwt.auth', ['except' =>[ 'index','show']]);
        $this->middleware('check-subscription',['only' =>[ 'getJourneyData','getJourneyData2']]);
        $this->journeyService = $journeyMainService;
    }

    public function index()
    {        
        $response = $this->journeyService->GetAllJourneys();
        return $this->handleResponse($response);
    }

    public function getJourneysSummery()
    {
        $response = $this->journeyService->getJourneysSummery();
        return $this->handleResponse($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $response = $this->journeyService->GetJourneyById($id);
        return $this->handleResponse($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getJourneyData($id)
    {
        $user = JWTAuth::toUser();        
        $response = $this->journeyService->getJourneyData($id,$user->id);
        return $this->handleResponse($response);
    }

    public function getJourneyData2($id,$user_id)
    {
        //$user = JWTAuth::toUser();        
        $response = $this->journeyService->getJourneyData($id,$user_id);
        return $this->handleResponse($response);
    }

    public function getJourneyCategories()
    {
        $response = $this->journeyService->getJourneyCategories();
        return $this->handleResponse($response);
    }

    public function getJourneyAdventures($journey_id){
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getJourneyAdventures($user->id,$journey_id);
        return $this->handleResponse($response);
    }
}
