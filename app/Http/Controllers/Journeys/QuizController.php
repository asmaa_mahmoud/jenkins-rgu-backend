<?php

namespace App\Http\Controllers\Journeys;

use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\ResponseObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class QuizController extends Controller
{
    private $journeyService;

    public function __construct(IJourneyMainService $journeyService){
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
        $this->journeyService = $journeyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,request $request)
    {        
        $user = JWTAuth::toUser();
        
        $response = $this->journeyService->GetQuiz($id,$request->activity_id,$request->round_id,$request->type,$user);
        return $this->handleResponse($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function submit(Request $request, $id,$journey_id,$adventure_id)
    {
        $user = JWTAuth::toUser();
        $response = $this->journeyService->submitQuiz($request->data, $id, $user->id,$journey_id,$adventure_id);
        return $this->handleResponse($response);
    }

    public function submitActivityQuiz(Request $request, $id,$activity_id)
    {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->submitActivityQuiz($request->data, $id, $user->id,$activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function submitActivityCampQuiz(Request $request, $id, $camp_id, $activity_id) {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->submitActivityCampQuiz($request->data, $id, $user->id, $camp_id, $activity_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getTutorials($id)
    {
        $response = $this->journeyService->getTutorials($id);
        return $this->handleResponse($response);
    }

    public function checkQuizAuthorization(Request $request)
    {
        $v = Validator::make($request->all(), [
            'quiz_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser(); 
        $response = $this->journeyService->checkQuizAuthorization($user->id,$request->quiz_id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public function checkQuizAuthorizationActivity(Request $request)
    {
        $v = Validator::make($request->all(), [
            'quiz_id' => 'required',
            'activity_id'=>'required',
            'is_default'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->checkQuizAuthorizationActivity($user->id,$request->quiz_id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function checkQuizAuthorizationActivityCamp(Request $request){
        $v = Validator::make($request->all(), [
            'quiz_id' => 'required',
            'activity_id' => 'required',
            'camp_id' => 'required',
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->journeyService->checkQuizAuthorizationActivityCamp($user->id,$request->quiz_id,$request->camp_id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getConfidence(){
        $response = $this->journeyService->getConfidence();
        return $this->handleResponse($response);
    }
}
