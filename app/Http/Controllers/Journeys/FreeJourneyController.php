<?php

namespace App\Http\Controllers\Journeys;

use JWTAuth;
use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FreeJourneyController extends Controller
{
    private $journeyService;

    public function __construct(IJourneyMainService $journeyMainService){
        $this->middleware('jwt.auth', ['except' => ['getFreeRoboClubMissions'] ]);
        $this->middleware('check-subscription', ['only' => [] ]);
        $this->journeyService = $journeyMainService;
    }

    public function getFreeRoboClubMissions(){
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $response = $this->journeyService->getFreeRoboClubMissions($start_from, $limit, $search);
        return $this->handleResponse($response);
    }

}