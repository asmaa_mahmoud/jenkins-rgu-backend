<?php

namespace App\Http\Controllers\Journeys;

use Illuminate\Http\Request;
use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class JourneyCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $journeyService;

    public function __construct(IJourneyMainService $journeyService){
        //$this->middleware('allow-origin');
        $this->journeyService = $journeyService;
        //$this->middleware('jwt.auth');
    }

    public function index()
    {        
        $response = $this->journeyService->getAllCategories();
        return $this->handleResponse($response);
    }

    public function getCategoryJournies($id)
    {        
        $response = $this->journeyCategoryService->getCategoryJournies($id);
        return $this->handleResponse($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
