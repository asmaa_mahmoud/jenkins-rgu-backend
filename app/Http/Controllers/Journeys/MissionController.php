<?php

namespace App\Http\Controllers\Journeys;

use Illuminate\Http\Request;
use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\Framework\Exceptions\BadRequestException;
use App\DomainModelLayer\Accounts\User;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Http\Controllers\Controller;


class MissionController extends Controller
{

    private $journeyService;

    public function __construct(IJourneyMainService $journeyService){
        //$this->middleware('allow-origin');
        $this->journeyService = $journeyService;
        $this->middleware('jwt.auth', ['except' =>['getMission','getMissionCategories','getFreeMissions','getFreeMissionById','encryptProgress','decryptProgress','sendCertificateEmail','uploadCertificate']]);
        $this->middleware('check-subscription', ['only' =>['show','loadSavedCode','updateSavedCode','updateSavedCode2','deleteSavedCode','deleteSavedCode2','updateMissionProgress','updateMissionProgress2','updateLastMission']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getMissionbyId($id,$user->id);
        return $this->handleResponse($response);
    }

    public function getMission($id,Request $request)
    {
        //$user = JWTAuth::toUser();
        $response = $this->journeyService->getMissionbyId($id,$request->user_id);
        return $this->handleResponse($response);
    }

    public function getMissionActivity($id,Request $request)
    {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        //$user = JWTAuth::toUser();
        $response = $this->journeyService->getActivityMissionbyId($id,$request->user_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getMissionActivityData($id,Request $request)
    {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getActivityMissionbyId($id,$user->id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getMissionActivityCampData($id, Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required',
            'is_default' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getCampActivityMissionbyId($id, $request->camp_id, $user->id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getMissionHtmlActivityCampData($id, Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required',
            'is_default' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getCampActivityMissionHtmlbyId($id, $request->camp_id, $user->id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getMissionData(Request $request){
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getMissionbyId($request->mission_id,$user->id,$request->course_id);
        return $this->handleResponse($response);
    }

    public function getHtmlMission($html_mission_id){
        $user = JWTAuth::toUser();
        $response = $this->journeyService->getHtmlMissionbyId($user->id,$html_mission_id);
        return $this->handleResponse($response);
    }

    public function getMissionMcqQuestions($id)
    {
        //$user = JWTAuth::toUser();
        $response = $this->journeyService->getMissionMcqQuestions($id);
        return $this->handleResponse($response);
    }

    public function getMissionTextQuestions($id)
    {
        //$user = JWTAuth::toUser();
        $response = $this->journeyService->getMissionTextQuestions($id);
        return $this->handleResponse($response);
    }

    public function submitMcqMission(Request $request, $id,$journey_id,$adventure_id)
    {
        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->submitMcqMission($request->data,$request->selected_ids,$request->use_model_answer,$id,$user->id,$journey_id,$adventure_id,$request->noOfBlocks,$request->timeTaken);
        return $this->handleResponse($response);
    }

    public function submitActivityMcqMission(Request $request, $id, $activity_id)
    {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);
        }

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->journeyService->submitActivityMcqMission($request->data, $request->selected_ids, $request->use_model_answer, $id, $user->id, $activity_id,
            $request->noOfBlocks, $request->timeTaken, $request->is_default, $request->is_mobile ? false : true);
        return $this->handleResponse($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadSavedCode($mission_id,$journey_id,$adventure_id){
        $user = JWTAuth::toUser();
        $response = $this->journeyService->loadMissionSavedCode($mission_id,$user->id,$journey_id,$adventure_id);
        return $this->handleResponse($response);
    }

    public function loadSavedCodeActivity(Request $request,$mission_id,$activity_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->journeyService->loadMissionSavedCodeActivity($mission_id,$user->id,$activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function updateSavedCode(Request $request, $mission_id) {
        $v = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
            'xml' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        try
        {
            simplexml_load_string($request->xml);
        }
        catch(Exception $exception)
        {
            return $this->handleValidationError([trans('locale.xml_input_format')]);
        }        

        $user = JWTAuth::toUser();
        $response = $this->journeyService->updateMissionSavedCode($mission_id,$user->id,$request->name,$request->xml);
        return $this->handleResponse($response);
    }

    public function updateSavedCode2(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
//            'xml' => 'required',
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required',
            'timetaken'=>'required|integer|min:0'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

//        try
//        {
//            simplexml_load_string($request->xml);
//        }
//        catch(Exception $exception)
//        {
//            return $this->handleValidationError(['xml input should be in XML format']);
//        }        

        $user = JWTAuth::toUser();
        $response = $this->journeyService->updateMissionSavedCode($request->mission_id,$user->id,$request->name,$request->xml,$request->journey_id,$request->adventure_id,$request->timetaken);
        return $this->handleResponse($response);
    }

    public function updateSavedCode2Activity(Request $request)
    {
        $v = Validator::make($request->all(), [
                'name' => 'required|alpha_num',
           'xml' => 'required',
            'mission_id' => 'required',
            'activity_id'=>'required',
            'is_default'=>'required',
            'timetaken'=>'required|integer|min:0'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

//        try
//        {
//            simplexml_load_string($request->xml);
//        }
//        catch(Exception $exception)
//        {
//            return $this->handleValidationError(['xml input should be in XML format']);
//        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->updateMissionSavedCodeActivity($request->mission_id,$user->id,$request->name,$request->xml,$request->activity_id,$request->timetaken,$request->is_default);
        return $this->handleResponse($response);
    }

    public function deleteSavedCode($mission_id,$name)
    {
        $user = JWTAuth::toUser();
        $response = $this->journeyService->deleteMissionSavedCode($mission_id,$user->id,$name);
        return $this->handleResponse($response);
    }

    public function deleteSavedCode2(Request $request)
    {

        $v = Validator::make($request->all(), [
            'name' => 'required',
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->deleteMissionSavedCode($request->mission_id,$user->id,$request->name,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public function deleteSavedCode2Activity(Request $request)
    {

        $v = Validator::make($request->all(), [
            'name' => 'required',
            'mission_id' => 'required',
            'activity_id'=>'required',
            'is_default'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->deleteMissionSavedCodeActivity($request->mission_id,$user->id,$request->name,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function updateMissionProgress(Request $request, $mission_id) {
        $user = JWTAuth::toUser();        
        $response = $this->journeyService->updateMissionProgress($mission_id,$user->id);
        return $this->handleResponse($response);
    }

    public function updateMissionProgress2(Request $request) {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required',
            'success' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $user = JWTAuth::toUser();

        $response = $this->journeyService->updateMissionProgress($request->mission_id,$user->id,$request->journey_id,$request->adventure_id,$request->success,$request->noOfBlocks,$request->timeTaken,$request->xmlCode);
        return $this->handleResponse($response);
    }

    public function updateActivityMissionProgress(Request $request) {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'activity_id'=>'required',
            'is_default' => 'required',
            'success' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $user = JWTAuth::toUser();

        $response = $this->journeyService->updateActivityMissionProgress($request->mission_id,$user->id,$request->activity_id,$request->success,$request->noOfBlocks,$request->timeTaken,$request->xmlCode,$request->is_default);
        return $this->handleResponse($response);
    }

    public function updateHTMLMissionProgress(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->updateHTMLMissionProgress($request->mission_id,$user->id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public function updateActivityHTMLMissionProgress(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'activity_id'=>'required',
            'is_default'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->updateActivityHTMLMissionProgress($request->mission_id,$user->id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getMissionCategories($missionId){
        $response = $this->journeyService->getMissionCategories($missionId);
        return $this->handleResponse($response);
    }

    public function updateLastMission(Request $request) 
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();        
        $response = $this->journeyService->updateLastMission($request->mission_id,$user->id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public  function getNextMission(Request $request)
    {
       //dd("lll");
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->journeyService->getNextMission($request->mission_id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public  function getNextTaskActivity(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'activity_id'=>'required',
            'is_default'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->journeyService->getNextTaskActivity($request->mission_id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public  function getNextMissionHtml(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->journeyService->getNextMissionHtml($request->mission_id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public  function getNextQuiz(Request $request)
    {
        $v = Validator::make($request->all(), [
            'quiz_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->journeyService->getNextQuiz($request->quiz_id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public function checkMissionAuthorization(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'journey_id'=>'required',
            'adventure_id'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser(); 
        $response = $this->journeyService->checkMissionAuthorization($user->id,$request->mission_id,$request->journey_id,$request->adventure_id);
        return $this->handleResponse($response);
    }

    public function checkMissionAuthorizationActivity(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'activity_id'=>'required',
            'is_default'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->checkMissionAuthorizationActivity($user->id,$request->mission_id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getFreeMissions()
    {
        $response = $this->journeyService->getFreeMissions();
        return $this->handleResponse($response);
    }

    public function getFreeMissionById($id)
    {
        $response = $this->journeyService->getFreeMissionById($id);
        return $this->handleResponse($response);
    }

    public function encryptProgress(Request $request)
    {
        $v = Validator::make($request->all(), [
            'progress' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        return $this->handleResponse(encrypt($request->progress));
    }

    public function decryptProgress(Request $request)
    {
        $v = Validator::make($request->all(), [
            'progress' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        return $this->handleResponse(decrypt($request->progress));
    }

    public function sendCertificateEmail(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required',
            'pdf_file' => 'required',
            'level' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->journeyService->sendCertificateEmail($request->email,$request->pdf_file,$request->level);
        return $this->handleResponse($response);
    }

    public function uploadCertificate(Request $request)
    {
        $v = Validator::make($request->all(), [
            'image_file' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->journeyService->uploadCertificate($request->image_file);
        return $this->handleResponse($response);
    }

    public function updateActivityCampMissionProgress(Request $request) {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'camp_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'success' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $user = JWTAuth::toUser();

        $response = $this->journeyService->updateActivityCampMissionProgress($request->mission_id, $user->id, $request->camp_id, $request->activity_id, $request->success, $request->noOfBlocks, $request->timeTaken, $request->xmlCode, $request->is_default);
        return $this->handleResponse($response);
    }

    public function updateActivityCampHTMLMissionProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'camp_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'success'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->updateActivityCampHTMLMissionProgress($request->mission_id,$user->id,$request->camp_id,$request->activity_id,$request->is_default,$request->success);
        return $this->handleResponse($response);
    }

    public function submitActivityCampMcqMission(Request $request, $id, $camp_id, $activity_id)
    {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->submitActivityCampMcqMission($request->data,$request->selected_ids,$request->use_model_answer,$id,$user->id,$camp_id,$activity_id,$request->noOfBlocks,$request->timeTaken,$request->is_default);
        return $this->handleResponse($response);
    }

    public function checkMissionAuthorizationActivityCamp(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'camp_id' => 'required|integer|min:0',
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->journeyService->checkMissionAuthorizationActivityCamp($user->id,$request->mission_id,$request->camp_id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function  getNextTaskHtmlActivityCamp(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'activity_id' => 'required',
            'camp_id' => 'required',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->journeyService->getNextTaskHtmlActivityCamp($request->mission_id, $request->camp_id, $request->activity_id, $request->is_default);
        return $this->handleResponse($response);

    }

    public function getNextTaskActivityCamp(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required',
            'activity_id' => 'required',
            'camp_id' => 'required',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->journeyService->getNextTaskActivityCamp($request->mission_id, $request->camp_id, $request->activity_id, $request->is_default);
        return $this->handleResponse($response);
    }





}
