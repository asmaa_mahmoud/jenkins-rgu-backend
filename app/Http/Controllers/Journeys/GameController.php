<?php

namespace App\Http\Controllers;

use App\ApplicationLayer\Game\IGameService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class GameController extends Controller
{

    private $gameService;

    public function __construct(IGameService $gameService){
        $this->middleware('allow-origin');
        $this->gameService = $gameService;
    }

    //region CRUD REST APIs
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    //endregion

    //region JOURNEY APIs
    public function GetJourneysCatalogue(){   
        
        $response = $this->gameService->GetJourneysCatalogue();
        return $this->handleResponse($response);
    }
    //endregion

    //region ADVENTURE APIs
    //endregion

    //region MISSION APIs
    //endregion

    //region QUIZ APIs
    //endregion

    //region PROGRESS APIs
    //endregion

}
