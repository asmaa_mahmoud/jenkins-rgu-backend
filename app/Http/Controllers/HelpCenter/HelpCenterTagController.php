<?php

namespace App\Http\Controllers\HelpCenter;

use App\ApplicationLayer\HelpCenter\Interfaces\IHelpCenterMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpCenterTagController extends Controller
{
    //
    //
    /**
     * @var IHelpCenterMainService
     */
    private $helpCenterService;

    /**
     * HelpCenterTagController constructor.
     * @param IHelpCenterMainService $helpCenterMainService
     */
    public function __construct(IHelpCenterMainService $helpCenterMainService){
        $this->helpCenterService = $helpCenterMainService;

        $this->middleware('allow-origin');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        $start_from = 0;
        $limit = null;
        $search = null;
        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        if(isset($request->search)){
            $search = $request->search;
        }

        $response = $this->helpCenterService->getAllTagsAdmin($start_from, $limit,$search);

        return $this->handleResponse($response);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request){
        $response = $this->helpCenterService->storeTag($request);
        return $this->handleResponse($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id){
        $response = $this->helpCenterService->getTag($id);
        return $this->handleResponse($response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id){
        $response = $this->helpCenterService->updateTag($request, $id);
        return $this->handleResponse($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id){
        $response = $this->helpCenterService->deleteTag($id);
        return $this->handleResponse($response);
    }

    //    =============== Non - RESTfull APIs Actions ===============

    public function count(Request $request){
        $search = null;

        if(isset($request->search)){
            $search = $request->search;
        }

        $response = $this->helpCenterService->getTagsCount($search);
        return $this->handleResponse($response);
    }
}
