<?php

namespace App\Http\Controllers\HelpCenter;

use App\ApplicationLayer\HelpCenter\Interfaces\IHelpCenterMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Framework\Exceptions\BadRequestException;

class HelpCenterController extends Controller
{
    private $helpCenterService;

    public function __construct(IHelpCenterMainService $helpCenterMainService){
        $this->helpCenterService = $helpCenterMainService;

        $this->middleware('allow-origin');
    }

//    =============== TOPICS ===============
    public function getAllTopics(Request $request){
        $start_from = 0;
        $limit = null;

        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        $response = $this->helpCenterService->getAllTopics(false, $start_from, $limit,$search=null);
        return $this->handleResponse($response);
    }

    public function getAllTopicsCount(){

        $response = $this->helpCenterService->getAllTopicsCount();
        return $this->handleResponse($response);
    }

    public function getAllTopicsDetails(){
        $start_from = 0;
        $limit = null;

        $response = $this->helpCenterService->getAllTopics(true, $start_from, $limit);
        return $this->handleResponse($response);
    }

    public function getTopic(Request $request, $id){
        if(!isset($id)){
            throw new BadRequestException("Topic ID is missing");
        }

        $start_from = 0;
        $limit = null;

        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        $response = $this->helpCenterService->getTopic($id, $start_from, $limit);
        return $this->handleResponse($response);
    }
    
    
    public function getTopicArticlesCount($id){
        if(!isset($id)){
            throw new BadRequestException("Topic ID is missing");
        }

        $response = $this->helpCenterService->getTopicArticlesCount($id);
        return $this->handleResponse($response);
    }
    
//  =============================================


//    =============== ARTICLES ===============
    public function getArticle($id){
        if(!isset($id)){
            throw new BadRequestException("Article ID is missing");
        }

        $response = $this->helpCenterService->getArticle($id);
        return $this->handleResponse($response);
    }


    public function getTopicArticle(){
        $topic_name='Articles';
        $response = $this->helpCenterService->getTopicArticle($topic_name);
        return $this->handleResponse($response);
    }

    public function searchForArticle(Request $request){
        $start_from = 0;
        $limit = null;
        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        if(!isset($request->keyword)){
            throw new BadRequestException("Search keyword is missing");
        }

        $response = $this->helpCenterService->searchForArticle($request->keyword, $start_from, $limit);
        return $this->handleResponse($response);
    }


    public function searchForCountArticle(Request $request){
        
        if(!isset($request->keyword)){
            throw new BadRequestException("Search keyword is missing");
        }

        $response = $this->helpCenterService->searchForCountArticle($request->keyword);
        return $this->handleResponse($response);
    }
//  =============================================


//    =============== TAGS ===============
    public function getAllTags(Request $request){
        $start_from = 0;
        $limit = null;
        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        $response = $this->helpCenterService->getAllTags($start_from, $limit);
        return $this->handleResponse($response);
    }
   
    public function getTagArticlesCount($id){
           if(!isset($id)){
            throw new BadRequestException("Tag ID is missing");
        }

        $response = $this->helpCenterService->getTagArticlesCount($id);
        return $this->handleResponse($response);
   
    }

    public function getTag(Request $request, $id){
        if(!isset($id)){
            throw new BadRequestException("Tag ID is missing");
        }

        $start_from = 0;
        $limit = null;

        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }
        $response = $this->helpCenterService->getTag($id, $start_from, $limit);
        return $this->handleResponse($response);
    }
//  =============================================

}
