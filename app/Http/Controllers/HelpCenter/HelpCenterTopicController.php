<?php

namespace App\Http\Controllers\HelpCenter;

use App\ApplicationLayer\HelpCenter\Interfaces\IHelpCenterMainService;
use App\DomainModelLayer\HelpCenter\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\Framework\Exceptions\BadRequestException;


/**
 * Class HelpCenterTopicController
 * @package App\Http\Controllers\HelpCenter
 */
class HelpCenterTopicController extends Controller
{
    //
    /**
     * @var IHelpCenterMainService
     */
    private $helpCenterService;


    /**
     * HelpCenterTopicController constructor.
     * @param IHelpCenterMainService $helpCenterMainService
     */
    public function __construct(IHelpCenterMainService $helpCenterMainService){
        $this->helpCenterService = $helpCenterMainService;
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
//        dd($request->method());
//        dd($request->getPathInfo());
//        dd(Str::random(32));

        $start_from = 0;
        $limit = null;
        $search = null;
        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        if(isset($request->search)){
            $search = $request->search;
        }

        $response = $this->helpCenterService->getAllTopicsAdmin($start_from, $limit,$search);

        return $this->handleResponse($response);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request){
        $response = $this->helpCenterService->storeTopic($request);
        return $this->handleResponse($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show(Request $request, $id){
        $start_from = 0;
        $limit = null;

        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        $response = $this->helpCenterService->getTopic($id, $start_from, $limit);
        return $this->handleResponse($response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id){
        $response = $this->helpCenterService->updateTopic($request, $id);
        return $this->handleResponse($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id){
        $response = $this->helpCenterService->deleteTopic($id);
        return $this->handleResponse($response);
    }

    //    =============== Non - RESTfull APIs Actions ===============

    /**
     * @param
     * @return mixed
     */
    public function count(Request $request){
        $search = null;

        if(isset($request->search)){
            $search = $request->search;
        }

        $response = $this->helpCenterService->getTopicsCount($search);
        return $this->handleResponse($response);
    }

    public function uploadIcon(Request $request){
        $response = $this->helpCenterService->uploadTopicIcon($request->getContent());
        return $this->handleResponse($response);
    }

    public function rearrange(Request $request){
        $user = JWTAuth::toUser(); 
        $response = $this->helpCenterService->rearrangeTopics($request->topics,$user,true);
        return $this->handleResponse($response);
    }

    public function rearrangeList(Request $request){
        $user = JWTAuth::toUser(); 
        $response = $this->helpCenterService->rearrangeTopics($request->topics,$user,false);
        return $this->handleResponse($response);
    }

}
