<?php

namespace App\Http\Controllers\HelpCenter;

use App\ApplicationLayer\HelpCenter\Interfaces\IHelpCenterMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\Framework\Exceptions\BadRequestException;


/**
 * Class HelpCenterArticleController
 * @package App\Http\Controllers\HelpCenter
 */
class HelpCenterArticleController extends Controller
{
    //
    /**
     * @var IHelpCenterMainService
     */
    private $helpCenterService;

    /**
     * HelpCenterArticleController constructor.
     * @param IHelpCenterMainService $helpCenterMainService
     */
    public function __construct(IHelpCenterMainService $helpCenterMainService){
        $this->helpCenterService = $helpCenterMainService;
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        $start_from = 0;
        $limit = null;
        $search = null;
        if(isset($request->start_from)){
            $start_from = $request->start_from;
        }
        if(isset($request->limit)){
            $limit = $request->limit;
        }
        if(isset($request->search)){
            $search = $request->search;
        }

        $response = $this->helpCenterService->getAllArticlesAdmin($start_from, $limit,$search);

        return $this->handleResponse($response);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request){
        $response = $this->helpCenterService->storeArticle($request);
        return $this->handleResponse($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id){
        $response = $this->helpCenterService->getArticle($id);
        return $this->handleResponse($response);
    }



    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id){
    
        $response = $this->helpCenterService->updateArticle($request, $id);
        return $this->handleResponse($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id){
        $response = $this->helpCenterService->deleteArticle($id);
        return $this->handleResponse($response);
    }


//    =============== Non - RESTfull APIs Actions ===============

    public function getArticleTags($id){
        $response = $this->helpCenterService->getArticleTags($id);
        return $this->handleResponse($response);
    }

    public function count(Request $request){
        $search = null;

        if(isset($request->search)){
            $search = $request->search;
        }

        $response = $this->helpCenterService->getArticlesCount($search);
        return $this->handleResponse($response);
    }

    public function uploadIcon(Request $request){
        $response = $this->helpCenterService->uploadArticleIcon($request->getContent());
        return $this->handleResponse($response);
    }

    // ============ Rearrange Section ===================//
    public function articlesRearrange(Request $request){
        $user = JWTAuth::toUser(); 
        $response = $this->helpCenterService->rearrangeArticles($request->topic_id,$user,$request->articles,true);
        return $this->handleResponse($response);
    }

    public function articlesRearrangeList(Request $request){
        $user = JWTAuth::toUser(); 
        $response = $this->helpCenterService->rearrangeArticles($request->topic_id,$user,$request->articles,false);
        return $this->handleResponse($response);
    }   
}
