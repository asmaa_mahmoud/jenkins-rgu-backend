<?php

namespace App\Http\Controllers;

//use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Helpers\ResponseObject;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\InternalErrorException;
use App\Framework\Exceptions\UnauthorizedException;
use Illuminate\Support\Facades\Response;
use App\Helpers\HttpMethods;
use DB;
use LaravelLocalization;
use Excel;
use Carbon\Carbon;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $SUPER_ADMIN = 1;
    public $EDITOR = 2;
    public $SUPER_PREMIUM = 3;
    public $PREMIUM = 4;
    public $FREE = 5;

    public $ACTIVE = 1;
    public $EXPIRED = 2;
    public $INACTIVE = 3;

    public $PAGE_LIMIT = 5;

    public $XP_DIV = 1;

    public $EASY_LEVEL = 0;
    public $MEDIUM_LEVEL = 1;
    public $HARD_LEVEL = 2;

    public $primary_students = 350;
    public $primary_courses = 10;
    public $junior_students = 700;
    public $junior_courses = 20;
    public $combined_students = 1050;
    public $combined_courses = 30;
    public $advanced_students = 700;
    public $advanced_courses = 20;

    public $journey_roadmap = ['J6'=>'Journey_two','J28'=>'Journey_one','J29'=>'Journey_two'];

    public $journey_scene = ['J6'=>'birthday','J28'=>'birthday','J29'=>'winter'];

    public $default_emails = ['info@robogarden.ca','support@robogarden.ca'];

    public function getAllWorlds()
    {
        $themes =  DB::table('theme')->get();
        $locale = LaravelLocalization::getCurrentLocale();
        foreach ($themes as $theme) {

            $theme_translation = DB::table('theme_translation')->where('theme_id',$theme->id)->where('language_code',$locale)->first();
            if($theme_translation != null)
                $theme->description = $theme_translation->body;

            $theme_journeys = DB::table('theme_journey')->where('theme_id',$theme->id)->whereNull('deleted_at')->get();

            $theme->journeys = [];
            $counter = 0;
            foreach ($theme_journeys as $theme_journey) {
                $journey = DB::table('journey')->where('id',$theme_journey->journey_id)->first();
                $journey_details = DB::table('journey_data')->where('journey_id',$theme_journey->journey_id)->where('language_code',$locale)->first();
                if($journey_details == null)
                {
                    $journey_details = DB::table('journey_data')->where('journey_id',$theme_journey->journey_id)->where('language_code','en')->first();
                    if($journey_details == null)
                        continue;
                }
                $theme->journeys[$counter]['shorten_title'] = $journey_details->shorten_title;
                $theme->journeys[$counter]['card_icon_url'] = $journey->card_icon_url;
                $theme->journeys[$counter]['id'] = $journey->id;
                $counter++;
            }
        }
        return $themes;
    }

    public function getWorldById($id)
    {
        $theme = DB::table('theme')->where('id',$id)->first();
        if($theme == null)
            return [];
        $locale = LaravelLocalization::getCurrentLocale();
        $theme_translation = DB::table('theme_translation')->where('theme_id',$id)->where('language_code',$locale)->first();
        if($theme_translation != null)
            $theme->description = $theme_translation->body;

        $theme_journeys = DB::table('theme_journey')->where('theme_id',$id)->get();

        $theme->journeys = [];
        $counter = 0;
        foreach ($theme_journeys as $theme_journey) {
            $journey = DB::table('journey')->where('id',$theme_journey->journey_id)->first();
            $journey_details = DB::table('journey_data')->where('journey_id',$theme_journey->journey_id)->where('language_code',$locale)->first();
            if($journey_details == null)
            {
                $journey_details = DB::table('journey_data')->where('journey_id',$theme_journey->journey_id)->where('language_code','en')->first();
                if($journey_details == null)
                    continue;
            }
            $theme->journeys[$counter]['shorten_title'] = $journey_details->shorten_title;
            $theme->journeys[$counter]['card_icon_url'] = $journey->card_icon_url;
            $theme->journeys[$counter]['id'] = $journey->id;
            $counter++;
        }

        return [$theme];
    }

    public function getJourneysByGradeNumber($GradeNumber){
        $journeyIds = DB::table('journey_data')->where("grade",$GradeNumber)->pluck('journey_id')->toArray();
        if(sizeof($journeyIds) == 0){
            throw new BadRequestException("There is no journeys with this grade number");
        }
        else{
            $journeys = DB::table('journey')->whereIn('id',$journeyIds)->get();
            foreach ($journeys as $journey) {
                $locale = LaravelLocalization::getCurrentLocale();
                $journey_details = DB::table('journey_data')->where('journey_id',$journey->id)->where('language_code',$locale)->first();

                if($journey_details != null)
                {
                    $journey->meaning = $journey_details->meaning;
                    $journey->shorten_title = $journey_details->shorten_title;
                    $journey->description = $journey_details->description;
                    $journey->technical_details = $journey_details->technical_details;
                    //$journey->concepts = explode("qqqqq", $journey_details->concepts);
                    $journey->concepts = $journey_details->concepts;
//                    $prequisites = [];
//                    $prequisites['journey'] = explode("aaaaa",explode(":",explode("qqqqq", $journey_details->prequisites)[0])[1]);
//                    $prequisites['knowledge'] = explode("aaaaa",explode(":",explode("qqqqq", $journey_details->prequisites)[1])[1]);

                    $journey->prequisites = $journey_details->prequisites;
                }

                $journey_theme = DB::table('theme_journey')->where('journey_id',$journey->id)->first();

                if($journey_theme != null)
                {
                    $journey->theme = DB::table('theme')->where('id',$journey_theme->theme_id)->first()->name;
                }
            }
            return $journeys;
        }
    }

    public function getJourneysByGrades(){
        $grades = DB::table('journey_data')->where("grade","<>","No")->groupBy("grade")->get(['grade']);
        
        $gradesJourneys = array();
        foreach ($grades as $key => $grade) {
            $journeyIds = DB::table('journey_data')->where("grade",$grade->grade)->pluck('journey_id')->toArray();

            $journeys = DB::table('journey')->whereIn('id',$journeyIds)->get();
           
            foreach ($journeys as $journey) {
                $locale = LaravelLocalization::getCurrentLocale();
                $journey_details = DB::table('journey_data')->where('journey_id',$journey->id)->where('language_code',$locale)->first();
                if($journey_details == null)
                    $journey_details = DB::table('journey_data')->where('journey_id',$journey->id)->where('language_code','en')->first();

                if($journey_details != null)
                {
                    $journey->meaning = $journey_details->meaning;
                    $journey->shorten_title = $journey_details->shorten_title;
                    $journey->description = $journey_details->description;
                    $journey->technical_details = $journey_details->technical_details;
                    $journey->concepts = $journey_details->concepts;
//                    $prequisites = [];
//                    $prequisites['journey'] = explode("aaaaa",explode(":",explode("qqqqq", $journey_details->prequisites)[0])[1]);
//                    $prequisites['knowledge'] = explode("aaaaa",explode(":",explode("qqqqq", $journey_details->prequisites)[1])[1]);

                    $journey->prequisites = $journey_details->prequisites;
                }

                $journey_theme = DB::table('theme_journey')->where('journey_id',$journey->id)->first();

                if($journey_theme != null)
                {
                    $journey->theme = DB::table('theme')->where('id',$journey_theme->theme_id)->first()->name;
                    $icon_description = DB::table('theme_translation')->where('theme_id',$journey_theme->theme_id)
                        ->where('language_code', $locale)->first()->icon_description ?? DB::table('theme_translation')->where('theme_id',$journey_theme->theme_id)
                            ->where('language_code', 'en')->first()->icon_description;
                    $journey->icon_description = $icon_description;
                }
            }
            $gradeName = trans('locale.grade') . " " . str_replace("G", "", $grade->grade);
            $data = ["gradeNumber" => $grade->grade, "gradeName" => $gradeName, "journeys" => $journeys];
            array_push($gradesJourneys, $data);
        }
        return $gradesJourneys;
    }

    public function getJourneyDetailsById($id)
    {
        $journey = DB::table('journey')->where('id',$id)->first();
        $locale = LaravelLocalization::getCurrentLocale();

        $journey_translation = DB::table('journey_translation')->where('journey_id', $id)->where('language_code', $locale)->whereNull('deleted_at')->first();
        if($journey_translation == null)
            $journey_translation = DB::table('journey_translation')->where('journey_id', $id)->where('language_code', 'en')->whereNull('deleted_at')->first();

        if($journey_translation != null){
            $journey->title = $journey_translation->title;
            $journey->description2 = $journey_translation->description;
        }

        $journey_details = DB::table('journey_data')->where('journey_id',$id)->where('language_code',$locale)->whereNull('deleted_at')->first();
        if($journey_details == null)
            $journey_details = DB::table('journey_data')->where('journey_id',$id)->where('language_code','en')->whereNull('deleted_at')->first();

        if($journey_details != null)
        {
            $journey->meaning = $journey_details->meaning;
            $journey->shorten_title = $journey_details->shorten_title;
            $journey->description = $journey_details->description;
            $journey->technical_details = $journey_details->technical_details;
            $journey->concepts = $journey_details->concepts;

            $journey->prequisites = $journey_details->prequisites;
        }

        $journey_theme = DB::table('theme_journey')->where('journey_id',$id)->first();

        if($journey_theme != null)
        {
            $journey->theme = DB::table('theme')->where('id',$journey_theme->theme_id)->first()->name;
        }


        /*$adventures_translations = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code',$locale)->
            whereIn('default_adventure_id', function($q) use($id){
                $q->select('id')->from('default_adventure')->whereNull('deleted_at')->
                whereIn('id', function ($q2) use($id){
                        $q2->select('adventure_id')->from('journey_adventure')->where('journey_id',$id);
                }); 
            })->get();*/

        /*$adventures = array();
        if($adventures_translations != null){
            foreach($adventures_translations as $adventure_translation){
                $adventure_roadmap = DB::table('default_adventure')->whereId($adventure_translation->default_adventure_id)->pluck('roadmap_image_url');
                $adventures[] = array(  'id' => $adventure_translation->default_adventure_id,
                                        'title' => $adventure_translation->title,
                                        'description' => $adventure_translation->description,
                                        'roadmap_image_url' => $adventure_roadmap);
            }
        }*/

        $adventures_records = DB::table('default_adventure')->whereNull('deleted_at')->
            whereIn('id',function ($q2) use($id){
                        $q2->select('adventure_id')->from('journey_adventure')->where('journey_id',$id);
                })->orderBy('order')->get();

        $adventures = array();
        if($adventures_records != null){
            foreach($adventures_records as $adventure_record){
                $adventure_roadmap = $adventure_record->roadmap_image_url;
                $adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code',$locale)->where('default_adventure_id',$adventure_record->id)->first();
                if($adventure_translation == null){
                    //throw new BadRequestException("No translations reached for this adventure.");
                    //$adventure_translation->title = trans("locale.adventure")." #".$adventure_record->id;
                    //$adventure_translation->description = null;
                    $adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code','en')->where('default_adventure_id',$adventure_record->id)->first();

                    if($adventure_translation == null){
                        $adventure_translation = (object)[
                                        'title' => trans("locale.adventure")." #".$adventure_record->id,
                                        'description' => null
                                    ];
                    }
                }

                $adventures[] = array(  'id' => $adventure_record->id,
                                        'title' => $adventure_translation->title,
                                        'description' => $adventure_translation->description,
                                        'roadmap_image_url' => $adventure_roadmap);
            }
        }
        $journey->adventures = $adventures;

        return [$journey];
    }

    public function getAdventureDetailsById($adventure_id){
        $locale = LaravelLocalization::getCurrentLocale();

        $find_adventure = DB::table('default_adventure')->whereNull('deleted_at')->whereId($adventure_id)->first();
        if($find_adventure == null)
            throw new BadRequestException("Adventure not found");

        $adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code',$locale)->where('default_adventure_id',$adventure_id)->first();
        if($adventure_translation == null){
            //throw new BadRequestException("No translations reached for this adventure or this adventure doesn't exist");
            //$adventure_translation->title = trans("locale.adventure")." #".$adventure_id;
            //$adventure_translation->description = null;
            $adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code','en')->where('default_adventure_id',$adventure_id)->first();

            if($adventure_translation == null){
                $adventure_translation = (object)[
                                        'title' => trans("locale.adventure")." #".$adventure_id,
                                        'description' => null
                                    ];
            } 
        }
        $adventure_roadmap = DB::table('default_adventure')->whereId($adventure_id)->pluck('roadmap_image_url');

        //Journey details section
        $journey = DB::table('journey')->whereNull('deleted_at')->
            whereIn('id', function ($q) use ($adventure_id){
                $q->select('journey_id')->from('journey_adventure')->where('adventure_id',$adventure_id);
            })->first();

        if($journey == null)
            throw new BadRequestException("Can't find jouney of this adventure");

        $journey_translation = DB::table('journey_translation')->whereNull('deleted_at')->where('language_code',$locale)->where('journey_id',$journey->id)->first();

        if($journey_translation == null){
            //throw new BadRequestException("Can't find translations of the journey");
            //$journey_translation->title = trans("locale.journey")." #".$journey->id;
            //$journey_translation->description = null;
            $journey_translation = DB::table('journey_translation')->whereNull('deleted_at')->where('language_code','en')->where('journey_id',$journey->id)->first();

            if($journey_translation == null){
                $journey_translation = (object)[
                                        'title' => trans("locale.journey")." #".$journey->id,
                                        'description' => null
                                    ];
            } 
        }

        $journey_details[] = (object)[
                            'id' => $journey->id,
                            'title' => $journey_translation->title,
                            'description' => $journey_translation->description,
                            'icon' => $journey->card_icon_url
                        ];


        //Tutorial videos and quiz section
        $tutorial_quiz = DB::table('quiz')->whereNull('deleted_at')->where('quiz_type_id',1)->
                            whereIn('task_id', function($q) use ($adventure_id){
                                $q->select('task_id')->from('task_order')->whereNull('deleted_at')->where('order',1)->where('adventure_id',$adventure_id);
                            })->first();
        if($tutorial_quiz == null)
            throw new BadRequestException("No tutorial quiz reached for this adventure");
        $tutorial_quiz_title = DB::table('quiz_translation')->whereNull('deleted_at')->where('quiz_id',$tutorial_quiz->id)->where('language_code', $locale)->pluck('title')->first();
        $tutorial_videos = DB::table('quiz_tutorial')->whereNull('deleted_at')->where('quiz_id',$tutorial_quiz->id)->pluck('video_url');
        $tutorial_quiz_object = (object)[
                                    'id' => $tutorial_quiz->id,
                                    'title' => $tutorial_quiz_title
                                ];


        //adventure quiz section
        $adventure_quiz = DB::table('quiz')->whereNull('deleted_at')->where('quiz_type_id',2)->
                            whereIn('task_id', function($q) use ($adventure_id){
                                $q->select('task_id')->from('task_order')->whereNull('deleted_at')->where('order',7)->where('adventure_id',$adventure_id);
                            })->first();
        if($adventure_quiz == null){
            throw new BadRequestException("No adventure quiz reached for this adventure");
        }
        $adventure_quiz_title = DB::table('quiz_translation')->whereNull('deleted_at')->where('quiz_id',$adventure_quiz->id)->where('language_code', $locale)->pluck('title')->first();
        $adventure_quiz_object = (object)[
                                    'id' => $adventure_quiz->id,
                                    'title' => $adventure_quiz_title
                                ];

        //missions section
        $missions = $this->getMissionsSummaryForAdventure($adventure_id, $locale);


        $adventure[] = (object)[
                            'id' => $adventure_id,
                            'title' => $adventure_translation->title,
                            'description' => $adventure_translation->description,
                            'roadmap_image_url' => $adventure_roadmap,
                            'journey_details' => $journey_details,
                            'tutorial_videos' => $tutorial_videos,
                            'tutorial_quiz' => $tutorial_quiz_object,
                            'missions' => $missions,
                            'adventure_quiz' => $adventure_quiz_object
                        ];
        return $adventure;
    }

    private function getMissionsSummaryForAdventure($adventure_id, $locale = 'en'){
        $is_html_missions = false;
        $missions_records = DB::table('mission')->whereNull('deleted_at')
                        ->whereIn('task_id', function($q) use($adventure_id){
                                $q->select('task_id')->from('task_order')->whereNull('deleted_at')->where('adventure_id',$adventure_id);
                            })->get();

        if($missions_records == null || count($missions_records) == 0){
            $missions_records = DB::table('mission_html')->whereNull('deleted_at')
                                    ->whereIn('task_id', function($q) use($adventure_id){
                                        $q->select('task_id')->from('task_order')->whereNull('deleted_at')->where('adventure_id',$adventure_id);
                                    })->get();
            if($missions_records == null || count($missions_records) == 0)
                throw new BadRequestException("Missions not found");
                
            $is_html_missions = true;
        }


        foreach($missions_records as $mission_record){
            if($is_html_missions){
                $screenshots = null;
                $mission_translation = DB::table('mission_html_translation')->whereNull('deleted_at')->where('language_code', $locale)->where('mission_html_id',$mission_record->id)->first();
            }
            else{
                $screenshots = DB::table('mission_screenshot')->whereNull('deleted_at')->where('mission_id',$mission_record->id)->pluck('screenshot_url');
                $mission_translation = DB::table('mission_translation')->whereNull('deleted_at')->where('language_code', $locale)->where('mission_id',$mission_record->id)->first();
            }

            if($mission_translation == null){
                //throw new BadRequestException("Mission translation not found");
                //$mission_translation->title = trans("locale.mission")." #".$mission_record->id;
                //$mission_translation->description = null;

                if($is_html_missions){
                    $mission_translation = DB::table('mission_html_translation')->whereNull('deleted_at')->where('language_code', 'en')->where('mission_html_id',$mission_record->id)->first();
                }
                else{
                    $mission_translation = DB::table('mission_translation')->whereNull('deleted_at')->where('language_code', 'en')->where('mission_id',$mission_record->id)->first();
                }

                if($mission_translation == null){
                    $mission_translation = (object)[
                                        'title' => trans("locale.mission")." #".$mission_record->id,
                                        'description' => null
                                    ];
                }
            }
            
            $missions[] = (object)[
                            //'id' => $mission_record->id,
                            'task_id' => $mission_record->task_id,
                            'title' => $mission_translation->title,
                            'description' => $mission_translation->description,
                            'screenshots' => $screenshots,
                            'is_html_missions' => $is_html_missions
                        ];
        }

        return $missions;
    }

    public function getMissionDetailsByTaskId($task_id){
        $mission = DB::table('mission')->whereNull('deleted_at')->where('task_id',$task_id)->first();

        if($mission == null){
            $html_mission = DB::table('mission_html')->whereNull('deleted_at')->where('task_id',$task_id)->first();
            if($html_mission == null)
                throw new BadRequestException(trans("locale.not_a_mission"));
            else
                return $this->getHtmlMissionDetailsById($html_mission->id);
        }
        else
            return $this->getMissionDetailsById($mission->id);
    }

    public function getMissionDetailsById($mission_id){
        $locale = LaravelLocalization::getCurrentLocale();

        $mission = DB::table('mission')->whereNull('deleted_at')->whereId($mission_id)->first();

        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist")); 

        $screenshots = DB::table('mission_screenshot')->whereNull('deleted_at')->where('mission_id',$mission_id)->pluck('screenshot_url');
        $state = DB::table('mission_state')->whereId($mission->mission_state_id)->pluck('name')->first();
        $type = DB::table('type')->whereNull('deleted_at')->whereId($mission->type_id)->pluck('name')->first();
        $mission_translation = DB::table('mission_translation')->whereNull('deleted_at')->where('language_code', $locale)->where('mission_id',$mission_id)->first();

        if($mission_translation == null){
            $mission_translation = DB::table('mission_translation')->whereNull('deleted_at')->where('language_code', 'en')->where('mission_id',$mission_id)->first();

            if($mission_translation == null){
                $mission_translation = (object)[
                                    'title' => trans("locale.mission")." #".$mission_id,
                                    'description' => null,
                                    'speech' => null
                                ];
            }
        }

        $categories = DB::table('block_category')->whereIn('id', function($q) use ($mission_id){
                        $q->select('block_category_id')->from('mission_category')->where('mission_id', $mission_id);
                     })->pluck('name');

        $task_order = DB::table('task_order')->whereNull('deleted_at')->where('task_id', $mission->task_id)->first();
        $adventure_summary = ($task_order != null)? $this->getAdventureSummary($task_order->adventure_id, $locale) : null;
        $journey_summary = ($task_order != null)? $this->getJourneySummary($task_order->journey_id, $locale) : null;

        $mission_object[] = (object)[
                            //'id' => $mission_id,
                            'task_id' => $mission->task_id,
                            'adventure_summary' => $adventure_summary,
                            'journey_summary' => $journey_summary,
                            'icon_url' => $mission->icon_url,
                            'title' => $mission_translation->title,
                            'description' => $mission_translation->description,
                            'speech' => $mission_translation->description_speech,
                            'screenshots' => $screenshots,
                            'type' => $this->formatMissionType($type),
                            'state' => $state,
                            'categories' => $categories,
                            'is_html_mission' => false
                        ];
        return $mission_object;
    }

    public function getHtmlMissionDetailsById($html_mission_id){
        $locale = LaravelLocalization::getCurrentLocale();

        $html_mission = DB::table('mission_html')->whereNull('deleted_at')->whereId($html_mission_id)->first();

        if($html_mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist")); 

        $html_mission_translation = DB::table('mission_html_translation')->whereNull('deleted_at')->where('language_code', $locale)->where('mission_html_id',$html_mission_id)->first();

        if($html_mission_translation == null){
            $html_mission_translation = DB::table('mission_html_translation')->whereNull('deleted_at')->where('language_code', 'en')->where('mission_html_id',$html_mission_id)->first();

            if($html_mission_translation == null){
                $html_mission_translation = (object)[
                                    'title' => trans("locale.mission")." #".$html_mission_id,
                                    'description' => null
                                ];
            }
        }

        $task_order = DB::table('task_order')->whereNull('deleted_at')->where('task_id', $html_mission->task_id)->first();
        $adventure_summary = ($task_order != null)? $this->getAdventureSummary($task_order->adventure_id, $locale) : null;
        $journey_summary = ($task_order != null)? $this->getJourneySummary($task_order->journey_id, $locale) : null;

        $html_mission_object[] = (object)[
                            //'id' => $html_mission_id,
                            'task_id' => $html_mission->task_id,
                            'adventure_summary' => $adventure_summary,
                            'journey_summary' => $journey_summary,
                            'icon_url' => $html_mission->icon_url,
                            'title' => $html_mission_translation->title,
                            'description' => $html_mission_translation->description,
                            'is_html_mission' => true
                        ];
        return $html_mission_object;
    }

    private function getAdventureSummary($adventure_id, $locale = 'en'){
        $adventure = DB::table('default_adventure')->whereNull('deleted_at')->whereId($adventure_id)->first();
        if($adventure == null)
            return null;

        $adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code',$locale)->where('default_adventure_id',$adventure_id)->first();
        if($adventure_translation == null){
            $adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code','en')->where('default_adventure_id',$adventure_id)->first();

            if($adventure_translation == null){
                $adventure_translation = (object)[
                                        'title' => trans("locale.adventure")." #".$adventure_id
                                    ];
            } 
        }
        
        $adventure_summary = (object)[
                                'id' => $adventure_id,
                                'name' => $adventure_translation->title
                            ];
        return $adventure_summary;
    }

    private function formatMissionType($type){
        if($type == "normal")
            return "Blockly";
        else if($type == "mcq")
            return "MCQ";
        else if($type == "text")
            return "Textual";
        else
            return $type;
    }

    private function getJourneySummary($journey_id, $locale = 'en'){
        $journey = DB::table('journey')->whereNull('deleted_at')->whereId($journey_id)->first();
        if($journey == null)
            return null;

        $journey_translation = DB::table('journey_translation')->whereNull('deleted_at')->where('language_code',$locale)->where('journey_id',$journey_id)->first();

        if($journey_translation == null){
            $journey_translation = DB::table('journey_translation')->whereNull('deleted_at')->where('language_code','en')->where('journey_id',$journey_id)->first();

            if($journey_translation == null){
                $journey_translation = (object)[
                                        'title' => trans("locale.journey")." #".$journey_id
                                    ];
            } 
        }

        $journey_summary = (object)[
                            'id' => $journey_id,
                            'name' => $journey_translation->title
                        ];
        return $journey_summary;
    }

    public function handleResponse($data)
    {
        $response = new ResponseObject();
        $response->Object = $data;
        $response->status_code = 200;
        //$headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type";
        return Response::json($response,$response->status_code);
    }

    public function handleCreated($message)
    {
        $response = new ResponseObject();
        $response->Object = $message;
        $response->status_code = \Illuminate\Http\Response::HTTP_CREATED;
        //$headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type";
        return Response::json($response,$response->status_code);
    }

    public function handleValidationError($data)
    {
        $response = new ResponseObject();
        $response->errorMessage = $data;
        $response->status_code = \Illuminate\Http\Response::HTTP_BAD_REQUEST;
        //$headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type";
        return Response::json($response,$response->status_code);
    }

    public function mapJourney($course_id)
    {
        DB::beginTransaction();
        try{
            $course = $this->getCourse($course_id);

            $journey_id = $this->addJourney($course);

            $lessons = $this->getLessons($course['id']);
            foreach($lessons as $lesson){
                $lessonRoadMap = $this->getRoadMap($lesson['id']);
                $adventure_id = $this->addAdventure($lesson,$lessonRoadMap,$journey_id);

                $examples = $this->getExamples($lesson['id']);
                foreach($examples as $example){
                    $mission_id = $this->addMission($example,$lessonRoadMap,$adventure_id,$course_id,$lesson['order'],$journey_id);
                }

                $quiz = $this->getQuiz($lesson['id']);
                $quiz_id = $this->addQuiz($quiz,$lessonRoadMap,$adventure_id,$lesson['order'],$journey_id);

               $questions = $this->getQuestions($quiz['id']);
               foreach($questions as $question) {
                   $this->addQuestion($question,$quiz_id);
               }

                $tutorialTest = $this->getTutorialTest($lesson['id']);
                $tutorialTest_id = $this->addTutorialTest($tutorialTest,$lessonRoadMap,$adventure_id,$lesson['order'],$journey_id);

               $generalQuestions = $this->getGeneralQuestions($tutorialTest['id']);
               foreach($generalQuestions as $question){
                   $this->addQuestionFromGeneralQuestion($question,$tutorialTest_id);
               }

               $blockQuestions = $this->getBlockQuestions($tutorialTest['id']);
               foreach($blockQuestions as $question){
                   $this->addQuestionFromBlockQuestion($question,$tutorialTest_id);
               }

               $blocks = $this->getTutorialTestBlocks($tutorialTest['id']);
               foreach($blocks as $block){
                   $this->addQuizTutorial($block,$tutorialTest_id);
               }
            }

            DB::commit();
            return "Done";
        }catch (\Exception $e){
            DB::rollBack();
            return "Error: " . $e->getMessage();
        }
    }


    function getCourse($course_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/course/'.$course_id);
        return json_decode($response['body'],true);
    }
    function getLessons($course_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/course/'.$course_id.'/lesson?limit=1000');
        return json_decode($response['body'],true)['data'];
    }
    function getRoadMap($lesson_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/lesson/'.$lesson_id.'/roadmap?limit=1000');
        return json_decode($response['body'],true)['data'][0];
    }
    function getExamples($lesson_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/lesson/'.$lesson_id.'/examples?limit=1000');
        return json_decode($response['body'],true)['data'];
    }
    function getQuiz($lesson_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/lesson/'.$lesson_id.'/quizzes?limit=1000');
        return json_decode($response['body'],true)['data'][0];
    }
    function getTutorialTest($lesson_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/lesson/'.$lesson_id.'/tutorialtests?limit=1000');
        return json_decode($response['body'],true)['data'][0];
    }
    function getTutorialTestBlocks($tutorialTest_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/test_lesson_block/'.$tutorialTest_id.'?limit=1000');
        return json_decode($response['body'],true);
    }
    function getGeneralQuestions($tutorialTest_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/tutorialTest/'.$tutorialTest_id.'/general_question?limit=1000');
        return json_decode($response['body'],true)['data'];
    }
    function getBlockQuestions($tutorialTest_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/tutorialTest/'.$tutorialTest_id . '/block_question?limit=1000');
        return json_decode($response['body'],true)['data'];
    }
    function getQuestions($quiz_id){
        $response = HttpMethods::get('http://dev.robogarden.ca/dev/HD/individual/backend/refactor/public/quiz/'.$quiz_id . '/questions?limit=1000');
        return json_decode($response['body'],true)['data'];
    }

    function addJourney($course){
        $journey_id = DB::table('journey')->insertGetId([
                'card_icon_url' => $course['thumbnail_image_link'],
                'journey_category_id' => $course['category']['id']
            ]
        );

        DB::table('journey_translation')->insert([
                'journey_id' => $journey_id,
                'language_code' => 'en',
                'title' => $course['title'],
                'description' => $course['desc'],
                'summary' => $course['summary']
            ]
        );

        $default_journey_id = DB::table('default_journey')->insertGetId([
                'journey_id' => $journey_id
            ]
        );

        return $default_journey_id;
    }
    function addAdventure($lesson,$lessonRoadMap,$journey_id){
        $adventure_id = DB::table('default_adventure')->insertGetId([
                'order' => $lesson['order'],
                'roadmap_image_url' => $lessonRoadMap['img_link']
            ]
        );

        DB::table('default_adventure_translation')->insert([
                'default_adventure_id' => $adventure_id,
                'language_code' => 'en',
                'title' => $lesson['title'],
                'description' => $lesson['body']
            ]
        );

        DB::table('journey_adventure')->insert([
                'adventure_id' => $adventure_id,
                'journey_id' => $journey_id
            ]
        );

        return $adventure_id;
    }
    function addMission($example,$lessonRoadMap,$adventure_id,$course_id,$lesson_order,$journey_id){
        $X = 0;
        $Y = 0;
        switch($example['order']){
            case 1:{
                $X = $lessonRoadMap['x2'];
                $Y = $lessonRoadMap['y2'];
                break;
            }
            case 2:{
                $X = $lessonRoadMap['x3'];
                $Y = $lessonRoadMap['y3'];
                break;
            }
            case 3:{
                $X = $lessonRoadMap['x4'];
                $Y = $lessonRoadMap['y4'];
                break;
            }
            case 4:{
                $X = $lessonRoadMap['x5'];
                $Y = $lessonRoadMap['y5'];
                break;
            }
            case 5:{
                $X = $lessonRoadMap['x6'];
                $Y = $lessonRoadMap['y6'];
                break;
            }
        }
        $task_id = DB::table('task')->insertGetId([
                'position_x' => $X.'px',
                'position_y' => $Y.'px'
            ]
        );

        DB::table('task_order')->insert([
                'adventure_id' => $adventure_id,
                'task_id' => $task_id,
                'journey_id'=>$journey_id,
                'order' => ((intval($lesson_order) - 1) * 7) + 1 + intval($example['order'])
            ]
        );

        DB::table('adventure_task')->insert([
                'adventure_id' => $adventure_id,
                'task_id' => $task_id
            ]
        );

        $mission_id = DB::table('mission')->insertGetId([
                'weight' => 20,
                'icon_url' => (array_key_exists('J'.$course_id,$this->journey_roadmap)? 'https://s3-us-west-2.amazonaws.com/robogarden/Learn+Page/Images/Road+Map/Missions\'+icons/'.$this->journey_roadmap['J'.$course_id].'/'.$example['order'].'.png' : 'https://s3-us-west-2.amazonaws.com/robogarden/Learn+Page/Images/Road+Map/Missions\'+icons/Journey_one/1.png'),
                'model_answer' => $example['answer_workspace'],
                'vpl_id' => $example['vpl_id'],
                'scene_name' => (array_key_exists('J'.$course_id,$this->journey_scene)? $this->journey_scene['J'.$course_id] : 'villa'),
                'blocks_maximum_number'=>$example['max_blocks'],
                'task_id' => $task_id
            ]
        );

        DB::table('mission_translation')->insert([
                'mission_id' => $mission_id,
                'language_code' => 'en',
                'title' => $example['title'],
                'description' => $example['body']
            ]
        );

        DB::table('mission_screenshot')->insert([
                    'mission_id' => $mission_id,
                    'screenshot_url' => $example['thumbnail_image_link']
                ]
            );

        foreach($example['screens'] as $screen){
            DB::table('mission_screenshot')->insert([
                    'mission_id' => $mission_id,
                    'screenshot_url' => $screen['image']
                ]
            );
        }

        foreach($example['cats'] as $cat){
            DB::table('mission_category')->insert([
                    'mission_id' => $mission_id,
                    'block_category_id' => $cat['id']
                ]
            );
        }

        $main_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - christmas main data.csv');

        foreach ($main_data as $journey) {
            if(intval($journey[6]) == $example['id'])
            {
                DB::table('mission_translation')->insert([
                        'mission_id' => $mission_id,
                        'language_code' => 'ar',
                        'title' => $journey[8],
                        'description' => $journey[10]
                    ]
                );
                if($journey[1] != null)
                {
                    DB::table('journey_translation')->insert([
                            'journey_id' => $journey_id,
                            'language_code' => 'ar',
                            'title' => $journey[2],
                            'description' => "",
                            'summary' => ""
                        ]
                    );

                    DB::table('default_adventure_translation')->insert([
                            'default_adventure_id' => $adventure_id,
                            'language_code' => 'ar',
                            'title' => $journey[5],
                            'description' => ""
                        ]
                    );
                    
                }
                elseif($journey[4] != null)
                {
                    DB::table('default_adventure_translation')->insert([
                            'default_adventure_id' => $adventure_id,
                            'language_code' => 'ar',
                            'title' => $journey[5],
                            'description' => ""
                        ]
                    );
                }
                break;
            }
        }

        return $mission_id;
    }
    function addQuiz($quiz, $lessonRoadMap, $adventure_id,$lesson_order,$journey_id){

        $task_id = DB::table('task')->insertGetId([
                'position_x' => $lessonRoadMap['x7'].'px',
                'position_y' => $lessonRoadMap['y7'].'px'
            ]
        );

        DB::table('task_order')->insert([
                'adventure_id' => $adventure_id,
                'task_id' => $task_id,
                'journey_id'=>$journey_id,
                'order' => (intval($lesson_order) * 7)
            ]
        );


        DB::table('adventure_task')->insert([
                'adventure_id' => $adventure_id,
                'task_id' => $task_id
            ]
        );

        $quiz_id = DB::table('quiz')->insertGetId([
                'task_id' => $task_id,
                'icon_url' => 'https://s3-us-west-2.amazonaws.com/robogarden/Learn+Page/Images/Road+Map/Missions%27+icons/quiz.png'
            ]
        );

        DB::table('quiz_translation')->insert([
                'quiz_id' => $quiz_id,
                'language_code' => 'en',
                'title' => $quiz['name'],
            ]
        );

        return $quiz_id;
    }
    function addTutorialTest($tutorialTest, $lessonRoadMap, $adventure_id,$lesson_order,$journey_id){

        $task_id = DB::table('task')->insertGetId([
                'position_x' => $lessonRoadMap['x1'].'px',
                'position_y' => $lessonRoadMap['y1'].'px'
            ]
        );

        DB::table('task_order')->insert([
                'adventure_id' => $adventure_id,
                'task_id' => $task_id,
                'journey_id'=>$journey_id,
                'order' => ((intval($lesson_order) - 1) * 7) + 1
            ]
        );

        DB::table('adventure_task')->insert([
                'adventure_id' => $adventure_id,
                'task_id' => $task_id
            ]
        );

        $tutorialTest_id = DB::table('quiz')->insertGetId([
                'task_id' => $task_id,
                'icon_url' => 'https://s3-us-west-2.amazonaws.com/robogarden/Learn+Page/Images/Road+Map/Missions%27+icons/tutorial.png'
            ]
        );

        DB::table('quiz_translation')->insert([
                'quiz_id' => $tutorialTest_id,
                'language_code' => 'en',
                'title' => $tutorialTest['name'],
            ]
        );

        return $tutorialTest_id;
    }
    function addQuizTutorial($block,$quiz_id){
        $tutorialQuiz_id = DB::table('quiz_tutorial')->insertGetId([
                'quiz_id' => $quiz_id,
                'image_url' => $block['thumbnail_image_link'],
                'video_url' => $block['thumbnail_video_link'],
            ]
        );

        DB::table('quiz_tutorial_translation')->insert([
                'quiz_tutorial_id' => $tutorialQuiz_id,
                'language_code' => 'en',
                'description' => $block['description'],
            ]
        );

        return $tutorialQuiz_id;
    }
    function addQuestionFromGeneralQuestion($question,$quiz_id){
        $question_id = DB::table('question')->insertGetId([
                'quiz_id' => $quiz_id,
                'weight' => 20,
            ]
        );

        DB::table('question_translation')->insert([
                'question_id' => $question_id,
                'language_code' => 'en',
                'body' => $question['body'],
            ]
        );

        foreach($question['choices'] as $choice){
            $choice_id = DB::table('choice')->insertGetId([
                    'question_id' => $question_id,
                    'is_model_answer' => intval($choice['is_correct']),
                ]
            );

            DB::table('choice_translation')->insert([
                    'choice_id' => $choice_id,
                    'language_code' => 'en',
                    'body' => $choice['body'],
                ]
            );

            $quiz_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - Tutorial Quiz.csv');
            foreach ($quiz_data as $quiz) {
                if(intval($quiz[8]) == $choice['id'])
                {
                    $choice_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[10])), $quiz[11], str_replace("_x000D_", "",$quiz[9]));

                    DB::table('choice_translation')->insert([
                            'choice_id' => $choice_id,
                            'language_code' => 'ar',
                            'body' => $choice_body,
                        ]
                    );
                    if($quiz[1] != "")
                    {
                        DB::table('quiz_translation')->insert([
                                'quiz_id' => $quiz_id,
                                'language_code' => 'ar',
                                'title' => $quiz[3],
                            ]
                        );
                        $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                        DB::table('question_translation')->insert([
                                'question_id' => $question_id,
                                'language_code' => 'ar',
                                'body' => $question_body,
                            ]
                        );
                    }
                    elseif($quiz[4] != "")
                    {
                        $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                        DB::table('question_translation')->insert([
                                'question_id' => $question_id,
                                'language_code' => 'ar',
                                'body' => $question_body,
                            ]
                        );
                    }
                    break;
                }
            }
        }

        return $question_id;
    }
    function addQuestionFromBlockQuestion($question,$quiz_id){
        $question_id = DB::table('question')->insertGetId([
                'quiz_id' => $quiz_id,
                'weight' => 20,
            ]
        );

        DB::table('question_translation')->insert([
                'question_id' => $question_id,
                'language_code' => 'en',
                'body' => $question['body'],
            ]
        );

        foreach($question['choices'] as $choice){
            $choice_id = DB::table('choice')->insertGetId([
                    'question_id' => $question_id,
                    'is_model_answer' => intval($choice['is_correct']),
                ]
            );

            DB::table('choice_translation')->insert([
                    'choice_id' => $choice_id,
                    'language_code' => 'en',
                    'body' => $choice['body'],
                ]
            );

            $quiz_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - Tutorial Quiz.csv');
            foreach ($quiz_data as $quiz) {
                if(intval($quiz[8]) == $choice['id'])
                {
                    $choice_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[10])), $quiz[11], str_replace("_x000D_", "",$quiz[9]));

                    DB::table('choice_translation')->insert([
                            'choice_id' => $choice_id,
                            'language_code' => 'ar',
                            'body' => $choice_body,
                        ]
                    );
                    if($quiz[1] != "")
                    {
                        DB::table('quiz_translation')->insert([
                                'quiz_id' => $quiz_id,
                                'language_code' => 'ar',
                                'title' => $quiz[3],
                            ]
                        );
                        $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                        DB::table('question_translation')->insert([
                                'question_id' => $question_id,
                                'language_code' => 'ar',
                                'body' => $question_body,
                            ]
                        );
                    }
                    elseif($quiz[4] != "")
                    {
                        $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                        DB::table('question_translation')->insert([
                                'question_id' => $question_id,
                                'language_code' => 'ar',
                                'body' => $question_body,
                            ]
                        );
                    }
                    break;
                }
            }
        }
        return $question_id;
    }
    function addQuestion($question,$quiz_id){
        $question_id = DB::table('question')->insertGetId([
                'quiz_id' => $quiz_id,
                'weight' => 20,
            ]
        );

        DB::table('question_translation')->insert([
                'question_id' => $question_id,
                'language_code' => 'en',
                'body' => $question['body'],
            ]
        );

        foreach($question['choices'] as $choice){
            $choice_id = DB::table('choice')->insertGetId([
                    'is_model_answer' => intval($choice['is_correct']),
                    'question_id' => $question_id,
                ]
            );

            DB::table('choice_translation')->insert([
                    'choice_id' => $choice_id,
                    'language_code' => 'en',
                    'body' => $choice['body'],
                ]
            );

            $quiz_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - Adventure quiz.csv');
            foreach ($quiz_data as $quiz) {
                if(intval($quiz[8]) == $choice['id'])
                {
                    $choice_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[10])), $quiz[11], str_replace("_x000D_", "",$quiz[9]));

                    DB::table('choice_translation')->insert([
                            'choice_id' => $choice_id,
                            'language_code' => 'ar',
                            'body' => $choice_body,
                        ]
                    );
                    if($quiz[1] != "")
                    {
                        DB::table('quiz_translation')->insert([
                                'quiz_id' => $quiz_id,
                                'language_code' => 'ar',
                                'title' => $quiz[3],
                            ]
                        );
                        $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                        DB::table('question_translation')->insert([
                                'question_id' => $question_id,
                                'language_code' => 'ar',
                                'body' => $question_body,
                            ]
                        );
                    }
                    elseif($quiz[4] != "")
                    {
                        $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                        DB::table('question_translation')->insert([
                                'question_id' => $question_id,
                                'language_code' => 'ar',
                                'body' => $question_body,
                            ]
                        );
                    }
                    break;
                }
            }

        }
        return $question_id;
    }

    public function mapJourneyCSV($course_id)
    {
        // $main_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - christmas main data.csv');
        // //return $main_data;
        // foreach ($main_data as $key => $journey) {
        //     if(intval($journey[6]) == $course_id)
        //     {
        //         if($journey[1] != null)
        //             return $journey[2].' ==== '.$journey[5].' ==== '.$journey[8].' ==== '.$journey[10];
        //         elseif($journey[4] != null)
        //             return 'NO Journey ==== '.$journey[5].' ==== '.$journey[8].' ==== '.$journey[10];
        //     }
        // }

        // $quiz_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - Adventure quiz.csv');
        // return $quiz_data;
        // // $choice_body = "";
        // // $question_body = "";
        // // $quiz_body = "";
        // foreach ($quiz_data as $quiz) {
        //     if(intval($quiz[8]) == $course_id)
        //     {
        //         return $quiz;
        //         return str_replace("_x000D_", "", str_replace("\n", "", $quiz[6]));
        //         $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
        //             $choice_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[10])), $quiz[11], str_replace("_x000D_", "",$quiz[9]));
        //         if($quiz[1] != "")
        //         {                    
        //             return $quiz[3].' ==== '.$question_body.' ==== '.$choice_body;
        //         }
        //         elseif($quiz[4] != "")
        //         {
        //             return 'NO QUIZ ==== '.$question_body.' ==== '.$choice_body;
        //         }
        //         else
        //         {
        //             return 'NO QUIZ OR QUESTION ==== '.$choice_body;
        //         }
        //     }
        // }

        $tutorial_data = $this->loadCSVFileFromStorage('Winter Journey_Adv_Missions_Questions.xlsx - Tutorial Quiz.csv');
        //return $tutorial_data;
        foreach ($tutorial_data as $quiz) {
            if(intval($quiz[8]) == $course_id)
            {
                if($quiz[1] != "")
                {
                    $question_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[6])), $quiz[7], str_replace("_x000D_", "",$quiz[5]));
                    $choice_body = str_replace(str_replace("_x000D_", "", str_replace("\n", "", $quiz[10])), $quiz[11], str_replace("_x000D_", "",$quiz[9]));
                    return $quiz[3].' ==== '.$question_body.' ==== '.$choice_body;
                }
                elseif($quiz[4] != "")
                {
                    return 'NO QUIZ ==== '.$quiz[7].' ==== '.$quiz[11];
                }
                else
                {
                    return 'NO QUIZ OR QUESTION ==== '.$quiz[11];
                }
            }
        }

        // $choices = DB::table('choice')->get();
        // $adventures = DB::table('default_adventure')->get();
        // $journeys = DB::table('journey')->get();
        // $missions = DB::table('mission')->get();
        // $journeyscategories = DB::table('journey_category')->get();
        // $quizzes = DB::table('quiz')->get();
        // $questions = DB::table('question')->get();
        // $quizzestutorials = DB::table('quiz_tutorial')->get();

        // DB::beginTransaction();

        // try{

        // foreach ($choices as $choice) {
        //     DB::table('choice_translation')->insert([
        //             'choice_id' => $choice->id,
        //             'language_code' => 'ar',
        //             'body' => "اجابة",
        //         ]
        //     );
        // }

        // foreach ($adventures as $adventure) {
        //     DB::table('default_adventure_translation')->insert([
        //         'default_adventure_id' => $adventure->id,
        //         'language_code' => 'ar',
        //         'title' => "عنوان المغامرة",
        //         'description' => "وصف المغامرة"
        //     ]
        // );
        // }

        // foreach ($journeys as $journey) {
        //     DB::table('journey_translation')->insert([
        //         'journey_id' => $journey->id,
        //         'language_code' => 'ar',
        //         'title' => "عنوان الرحلة",
        //         'description' => "وصف الرحلة",
        //         'summary' => "ملخص الرحلة"
        //     ]
        // );            
        // }

        // foreach ($missions as $mission) {
        //     DB::table('mission_translation')->insert([
        //         'mission_id' => $mission->id,
        //         'language_code' => 'ar',
        //         'title' => "عنوان المهمة",
        //         'description' => "وصف المهمة"
        //     ]
        // );
        // }

        // foreach ($journeyscategories as $journeyscategory) {
            
        // }

        // foreach ($quizzes as $quiz) {
        //     DB::table('quiz_translation')->insert([
        //         'quiz_id' => $quiz->id,
        //         'language_code' => 'ar',
        //         'title' => "عنوان الاختبار",
        //     ]
        // );
        // }

        // foreach ($questions as $question) {
        //     DB::table('question_translation')->insert([
        //         'question_id' => $question->id,
        //         'language_code' => 'ar',
        //         'body' => "سؤال",
        //     ]
        // );
        // }

        // foreach ($quizzestutorials as $quizzestutorial) {
        //     DB::table('quiz_tutorial_translation')->insert([
        //         'quiz_tutorial_id' => $quizzestutorial->id,
        //         'language_code' => 'ar',
        //         'description' => "وصف الدرس",
        //     ]
        // );
        // }

        //     DB::commit();
        //     return "Done";
        // }catch (\Exception $e){
        //     DB::rollBack();
        //     return "Error: " . $e->getMessage();
        // }

        
    }

    public function loadXLSXFile(Request $request){
        //dd($request->file);
        $originLanguageCode = "en";
        $targetLanguageCode = $request->targetLanguage;
        DB::beginTransaction();
        $data = json_encode(Excel::load($request->file,function($reader){
            $results = $reader->get();
            return $results;
        }));


        $data = json_decode($data,true);
        $data = $data['parsed'];

        $keys = array_keys($data[0]);
//        dd($keys);

        if($keys[0] != 'title' &&  $keys[2] != 'description' && $keys[4] != 'adventure'
            && $keys[6] != 'adventure_description' && $keys[8] != 'missionsquizzes'
            && $keys[10] != 'type' && $keys[11] != 'mission_descriptions' && $keys[13]  != 'quiz_questions'
            && $keys[15] != 'quiz_choices')
            return "file doesn't contain the apporprite english titles";

        $journeytitleKeyOrigin = $keys[0];
        $journeytitleKeyTarget= $keys[1];

        $journeyDescriptionKeyOrigin= $keys[2];
        $journeyDescriptionKeyTarget= $keys[3];

        $adventureTitleKeyOrigin= $keys[4];
        $adventureTitleKeyTarget = $keys[5];

        $adventureDescriptionKeyOrigin= $keys[6];
        $adventureDescriptionKeyTarget= $keys[7];

        $missionQuizTitleKeyOrigin= $keys[8];
        $missionQuizTitleKeyTarget= $keys[9];

        $missionDescriptionKeyOrigin= $keys[10];
        $missionDescriptionKeyTarget= $keys[11];

        $quizQuestionKeyOrigin= $keys[13];
        $quizQuestionKeyTarget= $keys[14];

        $questionChoicesKeyOrigin= $keys[15];
        $questionChoicesKeyTarget= $keys[16];

        for ($i=1;$i<sizeof($data); $i++){
            foreach ($data[$i] as $key => $value) {
                if($data[$i][$key]== null)
                    $data[$i][$key] = $data[$i-1][$key];
            }
        }
        $error = false;
        //return $data;
        $log= "";
        $log = $log.$this->getReportForEntryTranslation($originLanguageCode,$targetLanguageCode);
        //dd($log);
        $missionsCounter ="";

        foreach ($data as $row){

            if(DB::table('journey_translation')->where('title',$row[$journeytitleKeyTarget])->whereNull('deleted_at')->where('language_code',$targetLanguageCode)->first() == null){
                $journey_translation = DB::table('journey_translation')->whereNull('deleted_at')
                    ->where('title',$row[$journeytitleKeyOrigin])
                    ->where('language_code',$originLanguageCode)->first();
                if($journey_translation != null){
                    $journey_id = $journey_translation->journey_id;
                    DB::table('journey_translation')->insert(['journey_id'=>$journey_id,
                        'language_code'=>$targetLanguageCode,'title' => $row[$journeytitleKeyTarget],'description'=>$row[$journeyDescriptionKeyTarget]]);
                    //$log = $log."journey translation inserted ".$row['title']."\n";
                }
                else{
                    $log = $log."journey not found ".$row[$journeytitleKeyOrigin]."\n";
                    $error = true;
                }
            }

            $journey_adventure_ids = DB::table('adventure_task')->whereIn('task_id',DB::table('task_order')->where('journey_id',DB::table('journey_translation')->where('title',$row[$journeytitleKeyOrigin])->first()->journey_id)->pluck('task_id')->toArray())->pluck('adventure_id')->toArray();
            if(DB::table('default_adventure_translation')->whereNull('deleted_at')->where('title',$row[$adventureTitleKeyTarget])->where('language_code',$targetLanguageCode)->whereIn('default_adventure_id',$journey_adventure_ids)->first() == null){
                $default_adventure_translation = DB::table('default_adventure_translation')->whereNull('deleted_at')
                    ->where('title',$row[$adventureTitleKeyOrigin])
                    ->where('language_code',$originLanguageCode)->whereIn('default_adventure_id',$journey_adventure_ids)->first();

                if($default_adventure_translation != null){
                    $default_adventure_id = $default_adventure_translation->default_adventure_id;

                    DB::table('default_adventure_translation')->insert([
                        'language_code'=>$targetLanguageCode,
                        'default_adventure_id'=>$default_adventure_id,
                        'title'=>$row[$adventureTitleKeyTarget],
                        'description'=>$row[$adventureDescriptionKeyTarget]

                    ]);

                    //$log = $log."adventure translation inserted ".$row['adventure']."\n";
                }
                else{
                    $log = $log."adventure not found ".$row[$adventureTitleKeyOrigin]."\n";
                    $error = true;
                }

            }

            if($row['type'] == 'quiz'){
                $journey_quiz_ids  = DB::table('quiz')->whereNull('deleted_at')->whereIn('task_id',DB::table('task_order')->where('journey_id',DB::table('journey_translation')->where('title',$row[$journeytitleKeyOrigin])->first()->journey_id)->pluck('task_id')->toArray())->pluck('id')->toArray();
                if(DB::table('quiz_translation')->whereNull('deleted_at')->where('title','like','%'.rtrim($row[$missionQuizTitleKeyTarget]).'%')->where('language_code',$targetLanguageCode)->whereIn('quiz_id',$journey_quiz_ids)->first() == null)
                {
                    $quiz_translation = DB::table('quiz_translation')->whereNull('deleted_at')->where('title','like','%'.rtrim($row[$missionQuizTitleKeyOrigin]).'%')
                        ->where('language_code',$originLanguageCode)->whereIn('quiz_id',$journey_quiz_ids)->first();
                    if($quiz_translation != null){
                        $quiz_id = $quiz_translation->quiz_id;
                        DB::table('quiz_translation')->insert([
                            'language_code'=>$targetLanguageCode,
                            'quiz_id'=>$quiz_id,
                            'title'=>$row[$missionQuizTitleKeyTarget]
                        ]);
                        //$log = $log."quiz inserted ".$row['missionsquizzes']."\n";
                    }
                    else{
                        $log = $log."quiz not found ".$row[$missionQuizTitleKeyTarget]."\n";
                        $error = true;
                    }

                }

                $journey_question_ids  = DB::table('question')->whereNull('deleted_at')->whereIn('quiz_id',DB::table('quiz')->whereIn('task_id',DB::table('task_order')->where('journey_id',DB::table('journey_translation')->where('title',$row[$journeytitleKeyOrigin])->first()->journey_id)->pluck('task_id')->toArray())->pluck('id')->toArray())->pluck('id')->toArray();
                if(DB::table('question_translation')->whereNull('deleted_at')->where('body','like','%'.rtrim(html_entity_decode(str_replace('&nbsp;', ' ',htmlentities(str_replace('"','&quot;',rtrim(($row[$quizQuestionKeyTarget]))),null,'utf-8')))).'%')->whereIn('question_id',$journey_question_ids)->where('language_code',$targetLanguageCode)->first() == null)
                {

                    $question_translations = DB::table('question_translation')->whereNull('deleted_at')->where('body','like','%'.rtrim(html_entity_decode(str_replace('&nbsp;', ' ',htmlentities(str_replace('"','&quot;',rtrim(($row[$quizQuestionKeyOrigin]))),null,'utf-8')))).'%')
                        ->where('language_code',$originLanguageCode)->whereIn('question_id',$journey_question_ids)->get();
                    //dd($quizQuestionKeyOrigin);
                    if(sizeof($question_translations) >0){
                        foreach ($question_translations as $question_translation) {
                            $question_id = $question_translation->question_id;
                            DB::table('question_translation')->insert([
                                'language_code' => $targetLanguageCode,
                                'question_id' => $question_id,
                                'body' => rtrim(html_entity_decode(str_replace('&nbsp;', ' ',str_replace('"','&quot;',htmlentities(str_replace(rtrim(($row[$quizQuestionKeyOrigin])),rtrim(($row[$quizQuestionKeyTarget])),$question_translation->body),null,'utf-8')))))
                            ]);
                            //$log = $log . "question inserted " . $row['quiz_questions'] . "\n";
                        }
                    }
                    else{

                        $log = $log."question not found ".rtrim(html_entity_decode(str_replace('&nbsp;', ' ',htmlentities(str_replace('"','&quot;',rtrim(($row[$quizQuestionKeyOrigin]))),null,'utf-8'))))."\n";
                        $error = true;
                    }

                }

                $journey_choice_ids  = DB::table('choice')->whereNull('deleted_at')->whereIn('question_id',DB::table('question')->whereNull('deleted_at')->whereIn('quiz_id',DB::table('quiz')->whereIn('task_id',DB::table('task_order')->where('journey_id',DB::table('journey_translation')->where('title',$row[$journeytitleKeyOrigin])->first()->journey_id)->pluck('task_id')->toArray())->pluck('id')->toArray())->pluck('id')->toArray())->pluck('id')->toArray();

                if(DB::table('choice_translation')->whereNull('deleted_at')->where('body','like','<p>'.rtrim($row[$questionChoicesKeyTarget]).'%')->whereIn('choice_id',$journey_choice_ids)->where('language_code',$targetLanguageCode)->first() == null)
                {
                    $choice_translations = DB::table('choice_translation')->whereNull('deleted_at')->where('body','like','<p>'.rtrim($row[$questionChoicesKeyOrigin]).'%')
                        ->where('language_code',$originLanguageCode)->whereIn('choice_id',$journey_choice_ids)->get();
                    if(sizeof($choice_translations)>0){
                        foreach ($choice_translations as $choice_translation){
                            $choice_id = $choice_translation->choice_id;
                            DB::table('choice_translation')->insert([
                                'language_code'=>$targetLanguageCode,
                                'choice_id'=>$choice_id,
                                'body'=>'<p>'.rtrim($row[$questionChoicesKeyTarget]).'</p>'
                            ]);
                            //$log = $log."choice inserted ".$row[$questionChoicesKeyTarget]."\n";
                        }
                    }
                    else{
                        $log = $log."choice not found ".$row[$questionChoicesKeyOrigin]."\n";
                        $error = true;
                    }

                }




            }


            else if($row['type'] == 'mission'){
                $journey_mission_ids  = DB::table('mission')->whereNull('deleted_at')->whereIn('task_id',DB::table('task_order')->where('journey_id',DB::table('journey_translation')->where('title',$row[$journeytitleKeyOrigin])->first()->journey_id)->pluck('task_id')->toArray())->pluck('id')->toArray();
                //$missionsCounter = $missionsCounter.str_replace('`','"',$row['misionespruebas'])."\n";
                if(DB::table('mission_translation')->whereNull('deleted_at')->where('title','like','%'.str_replace('`','"',$row[$missionQuizTitleKeyTarget]).'%')->whereIn('mission_id',$journey_mission_ids)->where('language_code',$targetLanguageCode)->first() == null)
                {
                    //$missionsCounter = $missionsCounter.str_replace('`','"',$row['misionespruebas'])."\n";
                    $mission_translations = DB::table('mission_translation')->whereNull('deleted_at')->where('title','like','%'.str_replace('`','"',$row[$missionQuizTitleKeyOrigin]).'%')
                        ->where('language_code',$originLanguageCode)->whereIn('mission_id',$journey_mission_ids)->get();
                    if(sizeof($mission_translations)>0){

                        foreach($mission_translations as $mission_translation){
                            $missionsCounter = $missionsCounter.str_replace('`','"',$row[$missionQuizTitleKeyOrigin])."\n";
                            $mission_id = $mission_translation->mission_id;
                            DB::table('mission_translation')->insert([
                                'language_code'=>$targetLanguageCode,
                                'mission_id'=>$mission_id,
                                'title'=>str_replace('`','"',$row[$missionQuizTitleKeyTarget]),
                                'description'=>'<p>'.str_replace('`','"',$row[$missionDescriptionKeyTarget]).'</p>',
                            ]);
                        }

                        //$log = $log."mission inserted ".$row['missionsquizzes']."\n";
                    }
                    else{
                        $log = $log."mission not found ".$row[$missionQuizTitleKeyOrigin]."\n";
                        //$log = $log."mission not found ".$row[$missionQuizTitleKeyTarget]."\n";
                        $error = true;
                    }

                }
            }
            else if($row['type'] == 'html_mission'){
                $journey_mission_ids  = DB::table('mission_html')->whereNull('deleted_at')->whereIn('task_id',DB::table('task_order')->where('journey_id',DB::table('journey_translation')->where('title',$row[$journeytitleKeyOrigin])->first()->journey_id)->pluck('task_id')->toArray())->pluck('id')->toArray();
                //$missionsCounter = $missionsCounter.str_replace('`','"',$row['misionespruebas'])."\n";
                if(DB::table('mission_html_translation')->whereNull('deleted_at')->where('title','like','%'.str_replace('`','"',$row[$missionQuizTitleKeyTarget]).'%')->whereIn('mission_html_id',$journey_mission_ids)->where('language_code',$targetLanguageCode)->first() == null)
                {
                    //$missionsCounter = $missionsCounter.str_replace('`','"',$row['misionespruebas'])."\n";
                    $mission_translations = DB::table('mission_html_translation')->whereNull('deleted_at')->where('title','like','%'.str_replace('`','"',$row[$missionQuizTitleKeyOrigin]).'%')
                        ->where('language_code',$originLanguageCode)->whereIn('mission_html_id',$journey_mission_ids)->get();
                    if(sizeof($mission_translations)>0){

                        foreach($mission_translations as $mission_translation){
                            $missionsCounter = $missionsCounter.str_replace('`','"',$row[$missionQuizTitleKeyOrigin])."\n";
                            $mission_id = $mission_translation->mission_html_id;
                            DB::table('mission_html_translation')->insert([
                                'language_code'=>$targetLanguageCode,
                                'mission_html_id'=>$mission_id,
                                'title'=>str_replace('`','"',$row[$missionQuizTitleKeyTarget]),
                                'description'=>'<p>'.str_replace('`','"',$row[$missionDescriptionKeyTarget]).'</p>',
                            ]);
                        }

                        //$log = $log."mission inserted ".$row['missionsquizzes']."\n";
                    }
                    else{
                        $log = $log."html mission not found ".$row[$missionQuizTitleKeyOrigin]."\n";
                        //$log = $log."mission not found ".$row[$missionQuizTitleKeyTarget]."\n";
                        $error = true;
                    }

                }
            }
            else{
                $log = $log."unknown type".$row['type'];
            }


        }
        //return $missionsCounter;

        $log = $log.$this->getReportForEntryTranslation($originLanguageCode,$targetLanguageCode);
        //dd('hi');

        // if(!$error)
            DB::commit();

        return $log;
    }

    public function getReportForEntryTranslation($originLanguage,$targetLanguage){
        $log = 'journey_translation '.$originLanguage.' '
            .DB::table('journey_translation')->whereNull('deleted_at')->where('language_code',$originLanguage)->count().' '.$targetLanguage.' '
            .DB::table('journey_translation')->whereNull('deleted_at')->where('language_code',$targetLanguage)->count()."\n";

        $log = $log.'default_adventure_translation '.$originLanguage.' '
            .DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code',$originLanguage)->count().' '.$targetLanguage.' '
            .DB::table('default_adventure_translation')->whereNull('deleted_at')->where('language_code',$targetLanguage)->count()."\n";

        $log = $log.'quiz_translation '.$originLanguage.' '
            .DB::table('quiz_translation')->whereNull('deleted_at')->where('language_code',$originLanguage)->count().' '.$targetLanguage.' '
            .DB::table('quiz_translation')->whereNull('deleted_at')->where('language_code',$targetLanguage)->count()."\n";

        $log = $log.'question_translation '.$originLanguage.' '
            .DB::table('question_translation')->whereNull('deleted_at')->where('language_code',$originLanguage)->count().' '.$targetLanguage.' '
            .DB::table('question_translation')->whereNull('deleted_at')->where('language_code',$targetLanguage)->count()."\n";

        $log = $log.'choice_translation '.$originLanguage.' '
            .DB::table('choice_translation')->whereNull('deleted_at')->where('language_code',$originLanguage)->count().' '.$targetLanguage.' '
            .DB::table('choice_translation')->whereNull('deleted_at')->where('language_code',$targetLanguage)->count()."\n";

        $log = $log.'mission_translation '.$originLanguage.' '
            .DB::table('mission_translation')->whereNull('deleted_at')->where('language_code',$originLanguage)->count().' '.$targetLanguage.' '
            .DB::table('mission_translation')->whereNull('deleted_at')->where('language_code',$targetLanguage)->count()."\n";

        return $log;
    }
    public function loadCSVFileFromStorage($file_name)
    {
        $path = storage_path() . '/' . $file_name;
        $file = fopen($path, "r");
        $data = [];
        while (!feof($file)) {
            $data[] = fgetcsv($file);
        }
        fclose($file);
        return $data;
    }

    public function insertHtmlJourney(Request $request){

        $data = json_encode(Excel::load($request->file,function($reader){
            $results = $reader->get();
            return $results;
        }));


        $data = json_decode($data,true);
        $data = $data['parsed'];
        $jouneysData = array();
        $currentJourneyIndex = -1;
        $currentAdventureIndex = -1;
        $currentTaskIndex = -1;
        $currentQuestionIndex = -1;
        foreach($data as $row){
            if($row['journey_name'] != null){
                array_push($jouneysData,['journey_name'=>$row['journey_name']
                    ,'journey_description'=>$row['journey_description'],'adventures'=>array()]);
                $currentJourneyIndex++;
                $currentAdventureIndex = -1;
            }
            if($row['adventure_name'] != null){
               array_push($jouneysData[$currentJourneyIndex]['adventures']
                   ,['adventure_name'=>$row['adventure_name'],'adventure_description'=>$row['adventure_description'],'tasks'=>array()]);
               $currentAdventureIndex++;
               $currentTaskIndex = -1;
            }
            if($row['type'] != null){
                if($row['type'] == 'quiz'){
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks']
                        ,['title'=>$row['html_mission_quizzes_title'],'description'=>$row['html_mission_description'],
                            'tutorial_quiz_description'=>$row['tutorial_quiz_description']
                            ,'tutorial_quiz_image_url'=>$row['tutorial_quiz_image_url']
                            ,'tutorial_quiz_video_url'=>$row['tutorial_quiz_video_url']
                            ,'type'=>'quiz','questions'=>array()]);
                    $currentTaskIndex++;
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['questions']
                        ,['title'=>$row['quiz_question'],'choices'=>array()]);
                    $currentQuestionIndex = 0;
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['questions'][$currentQuestionIndex]['choices'],
                        ['title'=>$row['quiz_choices'],'is_answer'=>$row['quiz_choice_is_answer']]);
                }
                else if($row['type'] == 'html_mission'){
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks']
                        ,['title'=>$row['html_mission_quizzes_title'],'description'=>$row['html_mission_description'],'type'=>'html_mission','steps'=>array()]);
                    $currentTaskIndex++;
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['steps']
                        ,['title'=>$row['html_mission_steps_title'],'description'=>$row['html_mission_steps_description'],'video_url'=>$row['html_mission_steps_video_url'],'image_url'=>$row['html_mission_image_url']]);

                }
            }
            else{
                if($row['quiz_question']!= null){
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['questions']
                        ,['title'=>$row['quiz_question'],'choices'=>array()]);
                    $currentQuestionIndex ++;
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['questions'][$currentQuestionIndex]['choices'],
                        ['title'=>$row['quiz_choices'],'is_answer'=>$row['quiz_choice_is_answer']]);
                }
                else if($row['quiz_choices'] != null){
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['questions'][$currentQuestionIndex]['choices'],
                        ['title'=>$row['quiz_choices'],'is_answer'=>$row['quiz_choice_is_answer']]);
                }
                else if($row['html_mission_steps_title'] != null){
                    array_push($jouneysData[$currentJourneyIndex]['adventures'][$currentAdventureIndex]['tasks'][$currentTaskIndex]['steps']
                        ,['title'=>$row['html_mission_steps_title'],'description'=>$row['html_mission_steps_description'],'video_url'=>$row['html_mission_steps_video_url'],'image_url'=>$row['html_mission_image_url']]);
                }
            }

        }
        //return $jouneysData;
        DB::beginTransaction();
        $journeysInserted = 0;
        $adventuresInserted = 0;
        $tasksInserted = 0;
        $defaultJourney = DB::table('journey')->where('id',1)->first();
        $tutorialTypeId = DB::table('quiz_type')->where('name','tutorial')->first()->id;
        $quizTypeId = DB::table('quiz_type')->where('name','quiz')->first()->id;
        $quizIconUrl = DB::table('quiz')->where('quiz_type_id',$quizTypeId)->first()->icon_url;
        $tutorialIconUrl = DB::table('quiz')->where('quiz_type_id',$tutorialTypeId)->first()->icon_url;
        foreach ($jouneysData as $journey){
            $journey_id = 19;
//            DB::table('journey_translation')->insert([
//                'journey_id'=>$journey_id,'language_code'=>'en',
//                'title'=>$journey['journey_name'],'description'=>$journey['journey_description']
//            ]);
            $adventureOrder = 0;
            $defaultAdventures = DB::table('default_adventure')->whereIn('id',DB::table('journey_adventure')->where('journey_id',1)->pluck('adventure_id')->toArray())
                ->get();
            foreach ($journey['adventures'] as $adventure){

                $adventure_id = DB::table('default_adventure')->insertGetId([
                    'order'=>$adventureOrder+1,'roadmap_image_url'=>$defaultAdventures[$adventureOrder]->roadmap_image_url
                ]);
                DB::table('journey_adventure')->insert([
                    'journey_id'=>$journey_id,
                    'adventure_id'=>$adventure_id
                ]);
                DB::table('default_adventure_translation')->insert([
                   'default_adventure_id'=>$adventure_id,'language_code'=>'en',
                    'title'=>$adventure['adventure_name'],'description'=>$adventure['adventure_description']
                ]);

                $taskOrder = 0;
                foreach ($adventure['tasks'] as $task){
                    $default_task_order = DB::table('task_order')->where('order',$taskOrder+1)->where('journey_id',1)
                        ->where('adventure_id',$defaultAdventures[$adventureOrder]->id)->first();
                    $positionX = 0;
                    $positionY = 0;
                    if($default_task_order != null){
                        $positionX = $default_task_order->position_x;
                        $positionY = $default_task_order->position_y;
                    }

                    $task_id = DB::table('task')->insertGetId([]);

                    DB::table('task_order')->insert([
                       'task_id'=>$task_id,'order'=>$taskOrder+1,'journey_id'=>$journey_id,
                        'adventure_id'=>$adventure_id,'position_x'=>$positionX,'position_y'=>$positionY
                    ]);

                    if($task['type'] == 'quiz'){
                        $typeId = $quizTypeId;
                        $iconUrl = $quizIconUrl;
                        if($taskOrder == 0){
                            $typeId = $tutorialTypeId;
                            $iconUrl = $tutorialIconUrl;
                        }

                        $quiz_id = DB::table('quiz')->insertGetId([
                            'task_id'=>$task_id,'quiz_type_id'=>$typeId,'icon_url'=>$iconUrl
                        ]);

                        DB::table('quiz_translation')->insert(['quiz_id'=>$quiz_id,'title'=>$task['title'],'language_code'=>'en']);
                        if($taskOrder == 0){
                            $quiz_tutorial_id = DB::table('quiz_tutorial')->insertGetId([
                                'quiz_id'=>$quiz_id,'image_url'=>$task['tutorial_quiz_image_url'],
                                'video_url'=>$task['tutorial_quiz_video_url']
                            ]);
                            DB::table('quiz_tutorial_translation')->insert([
                                'quiz_tutorial_id'=>$quiz_tutorial_id,
                                'description'=>$task['tutorial_quiz_description'],
                                'language_code'=>'en'
                            ]);
                        }

                        foreach ($task['questions'] as $question){
                            $question_id = DB::table('question')->insertGetId(['quiz_id'=>$quiz_id,'weight'=>20]);
                            DB::table('question_translation')->insert(['question_id'=>$question_id,'body'=>$question['title'],'language_code'=>'en']);
                            foreach($question['choices'] as $choice){
                                $choice_id  = DB::table('choice')->insertGetId([
                                    'question_id'=>$question_id,'is_model_answer'=>$choice['is_answer']
                                ]);
                                DB::table('choice_translation')->insert([
                                    'choice_id'=>$choice_id,'body'=>$choice['title'],'language_code'=>'en'
                                ]);
                            }
                        }
                    }
                    else if($task['type'] == 'html_mission'){
                        $mission_html_id = DB::table('mission_html')->insertGetId(['task_id'=>$task_id]);
                        DB::table('mission_html_translation')->insert([
                            'mission_html_id'=>$mission_html_id,'title'=>$task['title'],
                            'description'=>$task['description'],
                            'language_code'=>'en'
                        ]);
                        $stepsOrder = 0;
                        foreach ($task['steps']  as $step){
                            $step_id = DB::table('mission_html_steps')->insertGetId([
                                'mission_html_id'=>$mission_html_id,'video_url'=>$step['video_url'],
                                'image_url'=>$step['image_url'],'order'=>$stepsOrder+1
                            ]);
                            DB::table('mission_html_steps_translation')->insert([
                               'mission_html_steps_id'=>$step_id,'title'=>$step['title'],
                                'description'=>$step['description'],
                                'language_code'=>'en'
                            ]);
                            $stepsOrder++;
                        }
                    }

                    $taskOrder++;
                }

                $adventureOrder++;
            }
        }
        DB::commit();
        return "data inserted";
    }

    public function mapUsers()
    {
        // camps: 62349
        // beta: 62352
        $new_invitations =  DB::connection('mysql')->table('invitation')->where('id','>',62352)->pluck('code')->toArray();
        $invitations =  DB::connection('mysql2')->table('invitation')->where('id','>',62352)->whereNotIn('code',$new_invitations)->get();
        DB::connection('mysql')->beginTransaction();
        foreach ($invitations as $invitation) {
            $invitation_id = DB::connection('mysql')->table('invitation')->insertGetId([
                'code' => $invitation->code,
                'deleted_at' => $invitation->deleted_at,
                'account_type_id' => $invitation->account_type_id,
                'ends_at' => $invitation->ends_at,
                'number_of_users' => $invitation->number_of_users
            ]);

            $details = DB::connection('mysql2')->table('invitation_details')->where('invitation_id',$invitation->id)->first();
            if($details != null)
            {
                DB::connection('mysql')->table('invitation_details')->insert([
                    'invitation_id' => $invitation_id,
                    'invitation_receiver_name' => $details->invitation_receiver_name,
                    'invitation_receiver_email' => $details->invitation_receiver_email,
                    'invitation_comment' => $details->invitation_comment,
                    'invitation_receiver_phone_number' => $details->invitation_receiver_phone_number,
                    'invitation_receiver_company' => $details->invitation_receiver_company,
                    'invitation_receiver_position' => $details->invitation_receiver_position
                ]);
            }

            $journeys = DB::connection('mysql2')->table('invitation_journey')->where('invitation_id',$invitation->id)->get();
            foreach ($journeys as $journey) {
                DB::connection('mysql')->table('invitation_journey')->insert([
                    'invitation_id' => $invitation_id,
                    'deleted_at' => $journey->deleted_at,
                    'progress_lock' => $journey->progress_lock,
                    'journey_id' => $journey->journey_id
                ]);
            }

            $invitation_subscriptions = DB::connection('mysql2')->table('invitation_subscription')->where('invitation_id',$invitation->id)->get();
            foreach ($invitation_subscriptions as $invitation_subscription) {
                $subscription = DB::connection('mysql2')->table('subscription')->where('id',$invitation_subscription->subscription_id)->first();
                $account = DB::connection('mysql2')->table('account')->where('id',$subscription->account_id)->first();

                $account_id = DB::connection('mysql')->table('account')->insertGetId([
                    'account_type_id' => $account->account_type_id,
                    'deleted_at' => $account->deleted_at,
                    'stripe_id' => $account->stripe_id,
                    'token_url' => $account->token_url,
                    'old' => $account->old,
                    'country' => $account->country,
                    'status_id' => $account->status_id
                ]);

                $subscription_id = DB::connection('mysql')->table('subscription')->insertGetId([
                    'deleted_at' =>  $subscription->deleted_at,
                    'ends_at' => $subscription->ends_at,
                    'plan_history_id' => $subscription->plan_history_id,
                    'account_id' => $account_id
                ]);
                $items = DB::connection('mysql2')->table('subscription_item')->where('subscription_id',$subscription->id)->get();
                foreach ($items as $item) {
                    DB::connection('mysql')->table('subscription_item')->insert([
                        'plan_history_id' =>  $item->plan_history_id,
                        'subscription_id' => $subscription_id,
                        'stripe_id' => $item->stripe_id,
                        'quantity' => $item->quantity,
                        'ends_at' => $item->ends_at
                    ]);
                }

                DB::connection('mysql')->table('invitation_subscription')->insert([
                    'subscription_id' =>  $subscription_id,
                    'deleted_at' => $invitation_subscription->deleted_at,
                    'invitation_id' => $invitation_id
                ]);

                $users = DB::connection('mysql2')->table('user')->where('account_id',$account->id)->get();
                foreach ($users as $user) {
                    $user_id = DB::connection('mysql')->table('user')->insertGetId([
                        'deleted_at' =>  $user->deleted_at,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->email,
                        'username' => $user->username,
                        'password' => $user->password,
                        'remember_token' => $user->remember_token,
                        'profile_image_url' => $user->profile_image_url,
                        'gender' => $user->gender,
                        'social_id' => $user->social_id,
                        'social_track' => $user->social_track,
                        'supervisor_email' => $user->supervisor_email,
                        'age' => $user->age,
                        'account_id' => $account_id
                    ]);

                    $roles = DB::connection('mysql2')->table('user_role')->where('user_id',$user->id)->get();
                    foreach ($roles as $role) {
                        DB::connection('mysql')->table('user_role')->insert([
                            'user_id' =>  $user_id,
                            'deleted_at' => $role->deleted_at,
                            'role_id' => $role->role_id
                        ]);
                    }
                    $tasks = DB::connection('mysql2')->table('task_progress')->where('user_id',$user->id)->get();
                    foreach ($tasks as $task) {
                        DB::connection('mysql')->table('task_progress')->insert([
                            'user_id' =>  $user_id,
                            'deleted_at' => $task->deleted_at,
                            'task_id' => $task->task_id,
                            'blocks_number' => $task->blocks_number,
                            'task_duration' => $task->task_duration,
                            'score' => $task->score,
                            'number_of_trials' => $task->number_of_trials,
                            'journey_id' => $task->journey_id,
                            'adventure_id' => $task->adventure_id
                        ]);
                    }
                    $savings = DB::connection('mysql2')->table('mission_saving')->where('user_id',$user->id)->get();
                    foreach ($savings as $saving) {
                        DB::connection('mysql')->table('mission_saving')->insert([
                            'user_id' =>  $user_id,
                            'deleted_at' => $saving->deleted_at,
                            'name' => $saving->name,
                            'mission_id' => $saving->mission_id,
                            'xml' => $saving->xml,
                            'journey_id' => $saving->journey_id,
                            'adventure_id' => $saving->adventure_id
                        ]);
                    }
                    $notifications = DB::connection('mysql2')->table('notifications')->where('user_id',$user->id)->get();
                    foreach ($notifications as $notification) {
                        $notification_id  = DB::connection('mysql')->table('notifications')->insertGetId([
                            'user_id' =>  $user_id,
                            'deleted_at' => $notification->deleted_at,
                            'starts_at' => $notification->starts_at,
                            'type' => $notification->type,
                            'read' => $notification->read,
                            'ends_at' => $notification->ends_at
                        ]);
                        $translations = DB::connection('mysql2')->table('notifications_translation')->where('notification_id',$notification->id)->get();
                        foreach ($translations as $translation) {
                            DB::connection('mysql')->table('notifications_translation')->insert([
                                'notification_id' => $notification_id,
                                'deleted_at' => $translation->deleted_at,
                                'language_code' => $translation->language_code,
                                'title' => $translation->title,
                                'message' => $translation->message
                            ]);
                        }

                    }

                }
            }
        }
        DB::connection('mysql')->commit();
        return "Users migrated successfully";
    }

    public function mapUsersProduction()
    {
        // prod : 1227
        $users =  DB::connection('mysql2')->table('user')->where('id','>',1320)->get();
        DB::connection('mysql')->beginTransaction();
        foreach ($users as $user) {
            $account = DB::connection('mysql2')->table('account')->where('id',$user->account_id)->first();
            $account_id = DB::connection('mysql')->table('account')->insertGetId([
                'account_type_id' => $account->account_type_id,
                'deleted_at' => $account->deleted_at,
                'stripe_id' => $account->stripe_id,
                'token_url' => $account->token_url,
                'old' => $account->old,
                'country' => $account->country,
                'status_id' => ($account->is_active == 1? 1:2)
            ]);

            $user_id = DB::connection('mysql')->table('user')->insertGetId([
                'deleted_at' =>  $user->deleted_at,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'username' => $user->username,
                'password' => $user->password,
                'remember_token' => $user->remember_token,
                'profile_image_url' => $user->profile_image_url,
                'gender' => $user->gender,
                'social_id' => $user->social_id,
                'social_track' => $user->social_track,
                'supervisor_email' => $user->supervisor_email,
                'age' => $user->age,
                'account_id' => $account_id
            ]);

            $subscriptions = DB::connection('mysql2')->table('subscription')->where('account_id',$account->id)->get();
            foreach ($subscriptions as $subscription) {
                $subscription_id = DB::connection('mysql')->table('subscription')->insertGetId([
                    'deleted_at' =>  $subscription->deleted_at,
                    'ends_at' => $subscription->ends_at,
                    'plan_history_id' => $subscription->plan_history_id,
                    'account_id' => $account_id
                ]);

                $invitation_subscriptions = DB::connection('mysql2')->table('invitation_subscription')->where('subscription_id',$subscription->id)->get();
                foreach ($invitation_subscriptions as $invitation_subscription) {
                    $invitation = DB::connection('mysql2')->table('invitation')->where('id',$invitation_subscription->invitation_id)->first();
                    $new_invitation = DB::connection('mysql')->table('invitation')->where('id',$invitation_subscription->invitation_id)->first();
                    if($new_invitation == null)
                    {
                        $invitation_id = DB::connection('mysql')->table('invitation')->insertGetId([
                            'code' => $invitation->code,
                            'deleted_at' => $invitation->deleted_at,
                            'account_type_id' => $invitation->account_type_id,
                            'ends_at' => $invitation->ends_at,
                            'number_of_users' => $invitation->number_of_users
                        ]);

                        $details = DB::connection('mysql2')->table('invitation_details')->where('invitation_id',$invitation->id)->first();
                        if($details != null)
                        {
                            DB::connection('mysql')->table('invitation_details')->insert([
                                'invitation_id' => $invitation_id,
                                'invitation_receiver_name' => $details->invitation_receiver_name,
                                'invitation_receiver_email' => $details->invitation_receiver_email,
                                'invitation_comment' => $details->invitation_comment,
                                'invitation_receiver_phone_number' => $details->invitation_receiver_phone_number,
                                'invitation_receiver_company' => $details->invitation_receiver_company,
                                'invitation_receiver_position' => $details->invitation_receiver_position
                            ]);
                        }

                        $journeys = DB::connection('mysql2')->table('invitation_journey')->where('invitation_id',$invitation->id)->get();
                        foreach ($journeys as $journey) {
                            DB::connection('mysql')->table('invitation_journey')->insert([
                                'invitation_id' => $invitation_id,
                                'deleted_at' => $journey->deleted_at,
                                'progress_lock' => $journey->progress_lock,
                                'journey_id' => $journey->journey_id
                            ]);
                        }
                    }
                    else
                    {
                        $invitation_id = $new_invitation->id;
                    }
                    DB::connection('mysql')->table('invitation_subscription')->insert([
                        'subscription_id' =>  $subscription_id,
                        'deleted_at' => $invitation_subscription->deleted_at,
                        'invitation_id' => $invitation_id
                    ]);

                }

                $items = DB::connection('mysql2')->table('subscription_item')->where('subscription_id',$subscription->id)->get();
                foreach ($items as $item) {
                    DB::connection('mysql')->table('subscription_item')->insert([
                        'plan_history_id' =>  $item->plan_history_id,
                        'subscription_id' => $subscription_id,
                        'stripe_id' => $item->stripe_id,
                        'quantity' => $item->quantity,
                        'ends_at' => $item->ends_at
                    ]);
                }

                $stripe_subscriptions = DB::connection('mysql2')->table('stripe_subscription')->where('subscription_id',$subscription->id)->get();
                foreach ($stripe_subscriptions as $stripe_subscription) {
                    DB::connection('mysql')->table('stripe_subscription')->insert([
                        'deleted_at' =>  $stripe_subscription->deleted_at,
                        'subscription_id' => $subscription_id,
                        'stripe_id' => $stripe_subscription->stripe_id,
                        'period_id' => $stripe_subscription->period_id
                    ]);
                }

                $historys = DB::connection('mysql2')->table('subscription_history')->where('subscription_id',$subscription->id)->get();
                foreach ($historys as $history) {
                    DB::connection('mysql')->table('subscription_history')->insert([
                        'plan_history_id' =>  $history->plan_history_id,
                        'subscription_id' => $subscription_id,
                        'account_id' => $account_id
                    ]);
                }
            }

            $roles = DB::connection('mysql2')->table('user_role')->where('user_id',$user->id)->get();
            foreach ($roles as $role) {
                DB::connection('mysql')->table('user_role')->insert([
                    'user_id' =>  $user_id,
                    'deleted_at' => $role->deleted_at,
                    'role_id' => $role->role_id
                ]);
            }
            $tasks = DB::connection('mysql2')->table('task_progress')->where('user_id',$user->id)->get();
            foreach ($tasks as $task) {
                DB::connection('mysql')->table('task_progress')->insert([
                    'user_id' =>  $user_id,
                    'deleted_at' => $task->deleted_at,
                    'task_id' => $task->task_id,
                    'blocks_number' => $task->blocks_number,
                    'task_duration' => $task->task_duration,
                    'score' => $task->score,
                    'number_of_trials' => $task->number_of_trials,
                    'journey_id' => $task->journey_id,
                    'adventure_id' => $task->adventure_id
                ]);
            }
            $savings = DB::connection('mysql2')->table('mission_saving')->where('user_id',$user->id)->get();
            foreach ($savings as $saving) {
                DB::connection('mysql')->table('mission_saving')->insert([
                    'user_id' =>  $user_id,
                    'deleted_at' => $saving->deleted_at,
                    'name' => $saving->name,
                    'mission_id' => $saving->mission_id,
                    'xml' => $saving->xml,
                    'journey_id' => $saving->journey_id,
                    'adventure_id' => $saving->adventure_id
                ]);
            }
        }
        DB::connection('mysql')->commit();
        return "Users migrated successfully";
    }


    public function updateGoldenTimeForAllMission(){
        $missions = DB::table('mission')->whereNull('deleted_at')->get();
        foreach ($missions as $mission){
            $modelAnswer = $mission->model_answer;
            if($modelAnswer != null){
                $splitted = preg_split('/type="(\w+)"/', $modelAnswer);
                $noOfBlocks =  count($splitted)-1;
                $factor = 20;
                $time = $noOfBlocks*$factor;
                //$mission->golden_time = $time;
                DB::table('mission')->where('id',$mission->id)->update(['golden_time'=>$time]);
            }

        }
        return "finished";
    }

    public function AddFreePlanToUsers()
    {
        // add free plan to all account types who don't have free plan.
        $account_types = [7, 10];
        Db::beginTransaction();
        $plan_history_id = [23, 24, 25];
        foreach ($account_types as $key => $account_type_id) {
            $accounts_ids = DB::table('account')->where('account_type_id', $account_type_id)->whereNotIn('id', function ($q) use ($plan_history_id, $key) {
                $q->select('account_id')->from('subscription')->where('plan_history_id', $plan_history_id[$key])->whereNull('deleted_at');
            })->whereNull('deleted_at')->pluck('id');
            foreach ($accounts_ids as $account_id) {
                DB::insert('insert into subscription (ends_at, plan_history_id, account_id) values (?, ?, ?)', [NULL, $plan_history_id[$key], $account_id]);
            }
        }
        DB::commit();
        return 'Free plan added to all accounts';
    }

    public function removeRoboPalItems(){
        $robopalPlanHistoryIds = [17,18,19];
        DB::beginTransaction();
        DB::table('subscription_item')->whereIn('plan_history_id',$robopalPlanHistoryIds)->delete();
        DB::table('plan_history')->whereIn('id',$robopalPlanHistoryIds)->delete();
        $robopalPlanId = 14;
        DB::table('plan')->where('id',$robopalPlanId)->delete();
        $robopalUnlockableId = 2;
        $freeplanids = [23,24,25];
        foreach ($freeplanids as $freeplanid){
            DB::table('plan_unlockable')->insert([
                'unlockable_id'=>$robopalUnlockableId,
                'plan_history_id'=>$freeplanid
            ]);
        }

        DB::commit();
        return "robopalSubscriptionItems removed";
    }

    public function schoolMigration(){
        DB::beginTransaction();

        $accounts = DB::table('account')->whereNull('deleted_at')->where('account_type_id',
            DB::table('account_type')->where('name','school')->first()->id)->get();

        $lockedStatus = DB::table('status')->where('name','locked')->first();
        $activeStatus = DB::table('status')->where('name','active')->first();
        $waitSubscriptionApprovalStatus = DB::table('status')->where('name','wait_subscription_approval')->first();

        $freePlan = DB::table('plan_history')->whereNull('deleted_at')
            ->where('new_name','Free')->where('account_type_id',
            DB::table('account_type')->where('name','school')->first()->id)->first();

        $customPlan = DB::table('plan_history')->whereNull('deleted_at')
            ->where('new_name','Custom')->where('account_type_id',
                DB::table('account_type')->where('name','school')->first()->id)->first();

        $log = "no of school accounts ".count($accounts)."\r\n";
        foreach ($accounts as $account){
            $latestSubscription = DB::table('subscription')->whereNull('deleted_at')
                ->where('account_id',$account->id)->orderBy('created_at','desc')->first();

            if($latestSubscription == null){
                $log .= 'account with id '.$account->id." didn't have a subscription\r\n";

                //create a subscription on free
                DB::table('subscription')->insert([
                    'ends_at'=>null,
                    'plan_history_id'=>$freePlan->id,
                    'account_id'=>$account->id
                ]);

                //change account type to active
                DB::table('account')->where('id',$account->id)->update([
                   'status_id'=>$activeStatus->id
                ]);

            }

            else{
                $invitaionSubscription = DB::table('invitation_subscription')->whereNull('deleted_at')
                    ->where('subscription_id',$latestSubscription->id)->first();

                $schoolCharge = DB::table('school_charge')->whereNull('deleted_at')
                    ->where('subscription_id',$latestSubscription->id)->first();

                $distributorSubscription = DB::table('distributor_subscription')->whereNull('deleted_at')
                    ->where('subscription_id',$latestSubscription->id)->first();

                if($invitaionSubscription != null){
                    $log .= 'account with id '.$account->id.' is invited skipping\r\n';
                }
                else if($schoolCharge != null){
                    $log .= 'account with id '.$account->id.' has a school charge skipping\r\n';
                }

                else if($distributorSubscription != null){
                    $distributorCharge = DB::table('distributor_charge')->whereNull('deleted_at')
                        ->where('distributor_subscription_id',$distributorSubscription->id)->first();
                    if($distributorCharge != null){
                        $log .= 'account with id '.$account->id.' has a distributor charge skipping\r\n';
                    }
                    else{
                        if($latestSubscription->ends_at == null && $account->status_id  == $lockedStatus->id){
                            $log .= 'account with id '.$account->id.' has a  locked distributor subscription attaching free subscription changing account type to wait_subscription_approval and setting ends at for subscription \r\n ';

                            //change account status to wait_subscription_approval
                            DB::table('account')->where('id',$account->id)->update([
                                'status_id'=>$waitSubscriptionApprovalStatus->id
                            ]);

                            //change subscription end date to it's created at
                            DB::table('subscription')->where('id',$latestSubscription->id)->update([
                                'ends_at'=>$latestSubscription->created_at
                            ]);

                            $schoolPlan = DB::table('school_plan')->where('plan_history_id',$latestSubscription->plan_history_id)->first();
                            $planHistory = DB::table('plan_history')->where('id',$latestSubscription->plan_history_id)->first();
                            $bundle = DB::table('school_bundles')->where('name',$planHistory->new_name)->first();
                            //create distributor charge
                            $distributorChargeId =DB::table('distributor_charge')->insertGetId([
                                'cost'=>null,
                                'no_of_students'=>$schoolPlan->max_students,
                                'no_of_classroom'=>$schoolPlan->max_courses,
                                'description'=>null,
                                'distributor_subscription_id'=>$distributorSubscription->id,
                                'bundle_id'=>$bundle->id
                            ]);

                            //create distributor charge details
                            DB::table('distributor_charge_details')->insertGetId([
                                'no_of_students'=>$schoolPlan->max_students,
                                'no_of_classroom'=>$schoolPlan->max_courses,
                                'distributor_charge_id'=>$distributorChargeId,
                                'name'=>$bundle->name
                            ]);

                            //change subscriptionto be on custom plan
                            DB::table('subscription')->where('id',$latestSubscription->id)->update([
                                'plan_history_id'=>$customPlan->id
                            ]);

                            //create a free subscription
                            DB::table('subscription')->insert([
                                'ends_at'=>null,
                                'plan_history_id'=>$freePlan->id,
                                'account_id'=>$account->id
                            ]);

                        }
                        else if ($latestSubscription->ends_at == null && $account->status_id  != $lockedStatus->id){
                            $log .= 'account with id '.$account->id.' has a   distributor subscription with null ends at but not locked dont know what to do \r\n';
                        }

                        else{
                            $log .= 'account with id '.$account->id.' has an active distributor subscription creating distributor charge and distrubutor charge details';

                            $schoolPlan = DB::table('school_plan')->where('plan_history_id',$latestSubscription->plan_history_id)->first();
                            $planHistory = DB::table('plan_history')->where('id',$latestSubscription->plan_history_id)->first();
                            $bundle = DB::table('school_bundles')->where('name',$planHistory->new_name)->first();
                            //create distributor charge
                            $distributorChargeId =DB::table('distributor_charge')->insertGetId([
                                'cost'=>null,
                                'no_of_students'=>$schoolPlan->max_students,
                                'no_of_classroom'=>$schoolPlan->max_courses,
                                'description'=>null,
                                'distributor_subscription_id'=>$distributorSubscription->id,
                                'bundle_id'=>$bundle->id
                            ]);

                            //create distributor charge details
                            DB::table('distributor_charge_details')->insertGetId([
                                'no_of_students'=>$schoolPlan->max_students,
                                'no_of_classroom'=>$schoolPlan->max_courses,
                                'distributor_charge_id'=>$distributorChargeId,
                                'name'=>$bundle->name
                            ]);

                            //change subscriptionto be on custom plan
                            DB::table('subscription')->where('id',$latestSubscription->id)->update([
                                'plan_history_id'=>$customPlan->id
                            ]);

                    }

                    }

                }

                else{
                    if($latestSubscription->plan_history_id == $freePlan->id){
                        $log .= 'account with id '.$account->id.' had a free subscription skipping\r\n';
                    }
                    else{
                        $log .= 'account with id '.$account->id.' had a subscription with no attachtement dont know what to do\r\n';
                    }

                }


            }


        }



        DB::commit();
        return $log;
    }

    //=================== LOAD ALL CSVs IN EVERY ADD QUESTIONS =================


    //Functions related to subscription to mail list
    public function subscribe_to_mailing_list(){
        // dd(request()->all());
        $mail = request('email');

        //  if(in_array($mail,$this->default_emails)) {
        //   $response = trans("locale.no_robogarden_emails");
        //   return $this->handleValidationError($response);
        // }

        if(in_array($mail,$this->default_emails))
            throw new BadRequestException(trans("locale.no_robogarden_emails"));

        //attempt to find this mail in the mailing list table
        $mail_in_DB = DB::table('subscribed_to_mailing_list')->where('mail',$mail)->first();
        //print_r($mail_in_DB);
        if(is_null($mail_in_DB)){
            //Insert into the table new row with the mail and is_subscribed is true.
            DB::table('subscribed_to_mailing_list')
                    ->insert(
                                ['mail' => $mail, 'is_subscribed' => 1]
                            );

            $response = trans("locale.successfully_subscribed_to_mailing_list");
            return $this->handleResponse($response);
        }
        else {
            //We need to know this mail's subscription status to the mailing list.
            if($mail_in_DB -> is_subscribed == 1){
                //No updates in the database
                $response = trans("locale.already_subscribed_to_mailing_list");
                return $this->handleValidationError($response);
            }
            else{
                //Update 'is_subscribed' to be true.
                DB::table('subscribed_to_mailing_list')
                        -> where ('mail',$mail)
                        -> update (
                                ['is_subscribed' => 1]
                            );

                $response = trans("locale.successfully_subscribed_to_mailing_list");
                return $this->handleResponse($response);
            }
        }
    }

    public function unsubscribe_from_mailing_list($mail){
        //attempt to find this mail in the mailing list table
        $mail_in_DB = DB::table('subscribed_to_mailing_list')->where('mail',$mail)->first();
        if(is_null($mail_in_DB)){
            //This is not an expected case because the person can't unsubscribe unless he has received the mail.
            $response = trans("locale.not_subscribed_before_to_mailing_list");
            return $this->handleValidationError($response);
        }
        else {
            //We need to know this mail's subscription status to the mailing list.
            if($mail_in_DB -> is_subscribed == 0){
                //No updates in the database
                $response = trans("locale.already_unsubscribed_from_mailing_list");
                return $this->handleValidationError($response);
            }
            else{
                //Update 'is_subscribed' to be false.
                //print_r($mail_in_DB);
                DB::table('subscribed_to_mailing_list')
                        -> where ('mail',$mail)
                        -> update (
                                ['is_subscribed' => 0]
                            );

                $response = trans("locale.successfully_unsubscribed_from_mailing_list");
                return $this->handleResponse($response);
            }
        }
    }

    public function get_mailing_list_subscribers_mails(){
        //return mails of all rows where is_subscribed is true.
        $mails = DB::table('subscribed_to_mailing_list')
                        -> select('mail')
                        -> where('is_subscribed',1)
                        -> distinct()
                        -> get();
        //return $mails;
        return $this->handleResponse($mails);
    }

    public function pythonScript($predictor, $gidTeam1, $gidTeam2,
                                 $fifaRank1, $fifaRank2, $fifaRankWeight,
                                 $goal1, $goal2, $goalWeight,
                                 $history, $historyWeight,
                                 $matchesPlayed1, $matchesPlayed2, $matchesPlayedWeight,
                                 $participation1, $participation2, $participationWeight,
                                 $rationWinning1, $rationWinning2, $ratioWinningWeight,
                                 $win1, $win2, $winWeight){
        $process = new Process("python business.py ".$predictor." ".$gidTeam1." ".$gidTeam2." ".$fifaRank1." ".$fifaRank2." ".$fifaRankWeight." ".$goal1
        ." ".$goal2." ".$goalWeight." ".$history." ".$historyWeight." ".$matchesPlayed1." ".$matchesPlayed2." ".$matchesPlayedWeight." ".$participation1." "
        .$participation2." ".$participationWeight." ".$rationWinning1." ".$rationWinning2." ".$ratioWinningWeight." ".$win1." ".$win2." ".$winWeight);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }
//              =======================================================================

    //functions related to subscription to world cup
    public function subscribe_to_world_cup(){
        //dd(request()->all());
        $mail = request('email');
        //attempt to find this mail in the mailing list table
        $mail_in_DB = DB::table('subscribe_world_cup')->where('mail',$mail)->first();
        //print_r($mail_in_DB);
        if(is_null($mail_in_DB)){
            //Insert into the table new row with the mail and is_subscribed is true.
            DB::table('subscribe_world_cup')
                    ->insert(
                                [   'mail' => $mail, 
                                    'is_subscribed' => 1, 
                                    'created_at' => Carbon::now(), 
                                    'updated_at' => Carbon::now()
                                ]
                            );

            $response = trans("locale.successfully_subscribed_to_mailing_list");
            return $this->handleResponse($response);
        }
        else {
            //We need to know this mail's subscription status to the mailing list.
            if($mail_in_DB ->is_subscribed == 1){
                //No updates in the database
                $response = trans("locale.already_subscribed_to_mailing_list");
                return $this->handleValidationError($response);
            }
            else{
                //Update 'is_subscribed' to be true.
                DB::table('subscribe_world_cup')
                        -> where ('mail',$mail)
                        -> update (
                                [   'is_subscribed' => 1,
                                    'updated_at' => Carbon::now()
                                ]
                            );

                $response = trans("locale.successfully_subscribed_to_mailing_list");
                return $this->handleResponse($response);
            }
        }
    }

    public function unsubscribe_from_world_cup($mail){
        //attempt to find this mail in the mailing list table
        $mail_in_DB = DB::table('subscribe_world_cup')->where('mail',$mail)->first();
        if(is_null($mail_in_DB)){
            //This is not an expected case because the person can't unsubscribe unless he has received the mail.
            $response = trans("locale.not_subscribed_before_to_mailing_list");
            return $this->handleValidationError($response);
        }
        else {
            //We need to know this mail's subscription status to the mailing list.
            if($mail_in_DB ->is_subscribed == 0){
                //No updates in the database
                $response = trans("locale.already_unsubscribed_from_mailing_list");
                return $this->handleValidationError($response);
            }
            else{
                //Update 'is_subscribed' to be false.
                //print_r($mail_in_DB);
                DB::table('subscribe_world_cup')
                        -> where ('mail',$mail)
                        -> update (
                                [   'is_subscribed' => 0,
                                    'updated_at' => Carbon::now()
                                ]
                            );

                $response = trans("locale.successfully_unsubscribed_from_mailing_list");
                return $this->handleResponse($response);
            }
        }
    }

    public function get_world_cup_subscribers_mails(){
        //return mails of all rows where is_subscribed is true.
        $mails = DB::table('subscribe_world_cup')
                        -> select('mail')
                        -> where('is_subscribed',1)
                        -> distinct()
                        -> get();
        //return $mails;
        return $this->handleResponse($mails);
    }



}
