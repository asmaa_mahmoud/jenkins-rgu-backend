<?php

namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Schools\Dtos\CourseRequestDto;

use Illuminate\Http\Request;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;

use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\ApplicationLayer\Schools\CustomMappers\ClassroomDtoMapper;

use App\Http\Controllers\Controller;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;

use JWTAuth;

class CourseController extends Controller
{

    private $schoolService;

    public function __construct(ISchoolMainService $schoolService)
    {
        //$this->middleware('allow-origin');
        $this->schoolService = $schoolService;
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
    }

    public function store(Request $request){
        $v = Validator::make($request->all(), [
            'name' => 'required|string',
            'journey_id' => 'required|integer',
            'startsAt'=>'required',
            'classroom_id'=>'required|integer',
            'adventures_deadlines'=>'required|array',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $courseRequestDto = Mapper::MapRequest(CourseRequestDto::class,$request->all());
        $courseRequestDto->startsAt = date("Y-m-d h:i:s",strtotime(implode(" ",array_slice(explode(" ",$request->startsAt),0,5))));

        for($i = 0;$i<sizeof($courseRequestDto->adventures_deadlines);$i++){
            $courseRequestDto->adventures_deadlines[$i]['endsAt'] = date("Y-m-d h:i:s",strtotime(implode(" ",array_slice(explode(" ",$courseRequestDto->adventures_deadlines[$i]['endsAt']),0,5))));
        }
        $response = $this->schoolService->createCourse($user->id,$courseRequestDto);
        return $this->handleResponse($response);
    }

    public function update($courseId,Request $request){
        $v = Validator::make($request->all(), [
            'startsAt'=>'required|date|after:yesterday',
            'name'=>'required|string'
        ]);

        $user = JWTAuth::toUser();
        $courseRequestDto = Mapper::MapRequest(CourseRequestDto::class,$request->all());
        $courseRequestDto->startsAt = date("Y-m-d h:i:s",strtotime(implode(" ",array_slice(explode(" ",$request->startsAt),0,5))));
        for($i = 0;$i<sizeof($courseRequestDto->adventures_deadlines);$i++){
            $courseRequestDto->adventures_deadlines[$i]['endsAt'] = date("Y-m-d h:i:s",strtotime(implode(" ",array_slice(explode(" ",$courseRequestDto->adventures_deadlines[$i]['endsAt']),0,5))));
        }
        $response = $this->schoolService->editCourse($user->id,$courseId,$courseRequestDto);
        return $this->handleResponse($response);
    }

    public function show($course_id){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getCourseMainData($user->id,$course_id);
        return $this->handleResponse($response);
    }

    public function destroy($course_id){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->deleteCourse($user->id,$course_id);
        return $this->handleResponse($response);
    }

    public function getCourseStatistics($course_id)
    {
        $response = $this->schoolService->getCourseStatistics($course_id);
        return $this->handleResponse($response);
    }
}