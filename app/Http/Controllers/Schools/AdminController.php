<?php

namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Schools\CustomMappers\SchoolDtoMapper;
use App\ApplicationLayer\Schools\Dtos\CustomPlanDto;
use App\ApplicationLayer\Schools\Dtos\DistributorSubscriptionDto;
use App\Helpers\Mapper;
use Illuminate\Http\Request;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    private $schoolService;

    public function __construct(ISchoolMainService $schoolService){


        $this->middleware('jwt.auth', ['except' =>['getSchoolPrices','getSchoolPlansBundle','schoolRegister','store','getSchoolPlans','getAllDistributors','getDistributorsByCountryCode','getAllCountries','activateSchoolAccount2','registerInvitedSchool']]);
        $this->middleware('check-subscription', ['only' =>['generalStatisticsAdmin','getAdminClassroomStatistics','getTopStudents','getAdminMissionsChart','getAdminActivityEvents','getClassroomsForAdmin']]);
        $this->middleware('allow-origin');
        $this->schoolService = $schoolService;
    }

    public function store(Request $request){

        $v = Validator::make($request->all(), [

            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',
            'schoolName'=>'required|string',
            'schoolCountry'=>'required|string',
            'schoolEmail' => 'email',
            'schoolAddress' => 'min:2',
            'schoolWebsite' => 'url',
            'distributor_id' => 'required',
            'plan' => 'required',

        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $schoolDto = SchoolDtoMapper::RequestMapper($request);
        $response = $this->schoolService->registerAdmin($userDto,$schoolDto);
        return $this->handleResponse($response);

    }

    public function getSchoolInfo(){

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getSchoolInfo($user->id);
        return $this->handleResponse($response);

    }
    public function updateSchoolInfo(Request $request){
        $v = Validator::make($request->all(), [

            'schoolName'=>'required|string',
            'schoolCountry'=>'nullable|string',
            'schoolEmail' => 'nullable|email',
            'schoolAddress' => 'nullable|min:2',
            'schoolWebsite' => 'nullable|url',
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $schoolDto = SchoolDtoMapper::RequestMapper($request);
        $response = $this->schoolService->updateSchoolInfo($user->id,$schoolDto);
        return $this->handleResponse($response);
    }

    public function update(Request $request){
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $userDto = UserDtoMapper::RequestMapper($request);
        $response = $this->schoolService->updateAdminInfo($user->id,$userDto);
        return $this->handleResponse($response);
    }
    public function getSchoolPlans()
    {
        $response = $this->schoolService->getSchoolPlans();
        return $this->handleResponse($response);
    }

    public function getAllDistributors()
    {
        $response = $this->schoolService->getAllDistributors();
        return $this->handleResponse($response);
    }

    public function getDistributorsByCountryCode($country_code)
    {
        $response = $this->schoolService->getDistributorsByCountryCode($country_code);
        return $this->handleResponse($response);
    }

    public function getAllCountries()
    {
        $response = $this->schoolService->getAllCountries();
        return $this->handleResponse($response);
    }

    public function activateSchoolAccount(Request $request)
    {
        $v = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->activateSchoolAccount($user->id,$request->code);
        return $this->handleResponse($response);
    }

    public function activateSchoolAccount2(Request $request)
    {
        $v = Validator::make($request->all(), [

            'code' => 'required',

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->schoolService->activateSchoolAccount($request->code);
        return $this->handleResponse($response);
    }

    public function renewSubscription(Request $request)
    {
        $v = Validator::make($request->all(), [

            'code' => 'required',
            'subscription_id' => 'required',

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->renewSubscription($user->id,$request->code,$request->subscription_id);
        return $this->handleResponse($response);
    }

    public function upgradeSubscription(Request $request)
    {
        $v = Validator::make($request->all(), [

            'code' => 'required',
            'subscription_id' => 'required',
            'plan' => 'required',

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->upgradeSubscription($user->id,$request->code,$request->subscription_id,$request->plan);
        return $this->handleResponse($response);
    }

    public function unsubscribe(Request $request)
    {
        $v = Validator::make($request->all(), [

            'subscription_id' => 'required',

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->unsubscribe($user->id,$request->subscription_id);
        return $this->handleResponse($response);
    }

    public function changeDistributor(Request $request)
    {
        $v = Validator::make($request->all(), [

            'distributor_id' => 'required',
            'subscription_id' => 'required',

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->changeDistributor($user->id,$request->distributor_id,$request->subscription_id);
        return $this->handleResponse($response);
    }

    public function generalStatisticsAdmin(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->generalStatisticsAdmin($user->id);
        return $this->handleResponse($response);
    }

    public function getAdminClassroomStatistics($grade_id)
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAdminClassroomStatistics($user->id,$grade_id);
        return $this->handleResponse($response);
    }
    public function getTopStudents(Request $request){

        $v = Validator::make($request->all(), [
            'grade_id' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTopStudents($user->id,$request->grade_id);
        return $this->handleResponse($response);
    }

    public function getAdminMissionsChart(Request $request)
    {
        $v = Validator::make($request->all(), [
            'from_time' => 'required|date',
            'to_time' => 'required|date'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAdminMissionsChart($user->id,$request['from_time'],$request['to_time']);
        return $this->handleResponse($response);
    }

    public function getAdminActivityEvents(Request $request){
        $v = Validator::make($request->all(), [
            'from_time' => 'date',
            'to_time' => 'date'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAdminActivityEvents($user->id,$request['from_time'],$request['to_time']);
        return $this->handleResponse($response);
    }

    public function getClassroomsForAdmin(Request $request){
        $v = Validator::make($request->all(), [
            'grade_id' => 'integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getClassroomsForAdmin($user->id,$request['grade_id']);
        return $this->handleResponse($response);
    }

    public function registerInvitedSchool(Request $request)
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',
            'invitation_code' => 'required',
            'schoolName'=>'required|string',
            'schoolCountry'=>'required|string',
            'schoolEmail' => 'email',
            'schoolAddress' => 'min:2',
            'schoolWebsite' => 'url',
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $schoolDto = SchoolDtoMapper::RequestMapper($request);

        $response = $this->schoolService->registerInvitedSchool($userDto,$schoolDto,$request->invitation_code);
        return $this->handleResponse($response);
    }

    public function schoolRegister(Request $request){
        $v = Validator::make($request->all(), [
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|max:24',
            'school_name' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $userDto->accounttype = 'School';
        $userDto->plan = 'free_school';
        $userDto->role = 'school_admin';

        $response = $this->schoolService->AddUser($userDto);
        $response->isPlan = false;
        if(isset($userDto->planname) && !empty($userDto->planname)){
            $response->isPlan = true;
            $response->planname = $userDto->planname;
        }
        return $this->handleResponse($response);
    }

    public function subscribeDistributor(Request $request){
        $v = Validator::make($request->all(), [
            'students' => 'integer|min:0',
            'classrooms' => 'integer|min:0',
            'bundle_id' => 'integer|min:0',
            'distributor_id' => 'integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $distributorDto = Mapper::MapRequest(DistributorSubscriptionDto::class, $request->all());
        $distributorDto->user = JWTAuth::toUser();
        $response = $this->schoolService->subscribeDistributor($distributorDto);
        return $this->handleResponse($response);
    }

    public function subscribeCustom(Request $request){
        $v = Validator::make($request->all(), [
            'students' => 'integer|min:0',
            'classrooms' => 'integer|min:0',
            'bundle_id' => 'integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $customDto = Mapper::MapRequest(CustomPlanDto::class, $request->all());
        $customDto->user = JWTAuth::toUser();
        $response = $this->schoolService->subscribeCustom($customDto);
        return $this->handleResponse($response);
    }

    public function upgradeCustom(Request $request){
        $v = Validator::make($request->all(), [
            'students' => 'integer|min:0',
            'classrooms' => 'integer|min:0',
            'bundle_id' => 'integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $customDto = Mapper::MapRequest(CustomPlanDto::class, $request->all());
        $customDto->user = JWTAuth::toUser();
        $response = $this->schoolService->upgradeCustom($customDto);
        return $this->handleResponse($response);
    }

    public function buildUpgradeCustomReceipt(Request $request){
        $v = Validator::make($request->all(), [
            'students' => 'integer|min:0',
            'classrooms' => 'integer|min:0',
            'bundle_id' => 'integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $customDto = Mapper::MapRequest(CustomPlanDto::class, $request->all());
        $customDto->user = JWTAuth::toUser();
        $response = $this->schoolService->buildUpgradeCustomReceipt($customDto);
        return $this->handleResponse($response);
    }

    public function getSchoolInvoices()
    {
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        if (isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (isset($_GET['start_from'])) {
            $start_from = $_GET['start_from'];
        }
        $response = $this->schoolService->getSchoolInvoices($user->id, $limit, $start_from);
        return $this->handleResponse($response);
    }

    public function getSchoolPlansBundle(){
        $response =  $this->schoolService->getSchoolPlansBundle();
        return $this->handleResponse($response);
    }

    public function getSchoolPrices(){
        $response = $this->schoolService->getSchoolPrices();
        return $this->handleResponse($response);
    }

    public function getSubscriptionDetails(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getSubscriptionDetails($user->id);
        return $this->handleResponse($response);
    }

    public function upgradeDistributorSubscription(Request $request){
        $v = Validator::make($request->all(), [
            'students' => 'integer|min:0',
            'classrooms' => 'integer|min:0',
            'bundle_id' => 'integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $customDto = Mapper::MapRequest(CustomPlanDto::class, $request->all());
        $customDto->user = JWTAuth::toUser();
        $response = $this->schoolService->upgradeDistributorSubscription($customDto);
        return $this->handleResponse($response);
    }

    public function activateDistributorUpgrade(Request $request){
        $v = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->activateDistributorUpgrade($user->id, $request->code);
        return $this->handleResponse($response);
    }

    public function checkSchoolStatus(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->checkSchoolStatus($user->id);
        return $this->handleResponse($response);
    }

    public function getAllPlanNames(){
        $response = $this->schoolService->getAllPlanNames();
        return $this->handleResponse($response);
    }

    public function getLastEndedSubscription(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getLastEndedSubscription($user->id);
        return $this->handleResponse($response);
    }

    public function getInvitedJourneys(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getInvitedJourneys($user->id);
        return $this->handleResponse($response);
    }


}