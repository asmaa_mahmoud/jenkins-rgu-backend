<?php

namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Http\Request;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;
use App\ApplicationLayer\Accounts\Dtos\UserDto;

use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;

use App\Http\Controllers\Controller;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;


class DummyController extends Controller
{
    public $token = "robogarden";
    private $schoolService;
    public  $dummyPassword = "robogarden_faker";
    public $hashedDummyPassword ="";
    public function __construct(ISchoolMainService $schoolService){
        $this->schoolService = $schoolService;
    }
    public function addTeachers(Request $request){

        $v = Validator::make($request->all(), [
            'token' => 'required',
            'number_of_teachers' => 'required',
            'user_id' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if(decrypt($request->token) != $this->token)
            throw new BadRequestException(trans('locale.token_incorrect'));

        $faker = \Faker\Factory::create();
        for($i = 0;$i<$request->number_of_teachers;$i++){
            $teacher = new UserDto();
            $teacher->fname = $faker->name;
            $teacher->lname = $faker->name;
            $teacher->username = $faker->userName;
            $teacher->email = $faker->email;
            $teacher->password = $this->dummyPassword;
            $response = $this->schoolService->addTeacher($request->user_id,$teacher);
        }

        return trans('locale.teachers_added');
    }
}