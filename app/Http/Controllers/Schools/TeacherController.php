<?php
namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use Illuminate\Http\Request;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    private $schoolService;

    public function __construct(ISchoolMainService $schoolService){
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
        //$this->middleware('allow-origin');
        $this->schoolService = $schoolService;
    }

    public function count(){
        $user = JWTAuth::toUser();
         $search = null;
        $grade_name = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $response = $this->schoolService->countTeachers($user->id,$grade_name,$search);
        return $this->handleResponse($response);

    }

    public function index()
    {   
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $grade_name = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }
          
        $response = $this->schoolService->getAllTeachers($user->id,$start_from,$limit,$grade_name,$search);
        return $this->handleResponse($response);
    }

    public function show($id)
    {  
        $user = JWTAuth::toUser();      
        $response = $this->schoolService->getTeacherById($user->id,$id);
        return $this->handleResponse($response);
    }

    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'min:6|max:24',
            'classrooms' => 'array',

        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $userDto->id = $id;
        $user = JWTAuth::toUser();
        $response = $this->schoolService->updateTeacher($user->id,$userDto,$request["classrooms"]);
        return $this->handleResponse($response);
    }

    public function deleteTeachers(Request $request)
    {
        $v = Validator::make($request->all(), [
            'teachers' => 'required|array',
            'force'=>'boolean'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();


        $response = $this->schoolService->deleteTeachers($user->id,$request->teachers,$request->force);
        return $this->handleResponse($response);
    }

    public function store(Request $request){

        $v = Validator::make($request->all(), [

            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',

        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $user = JWTAuth::toUser();
        $response = $this->schoolService->addTeacher($user->id,$userDto);
        return $this->handleResponse($response);

    }

    public function addTeachersCSV(Request $request){

        $v = Validator::make(
            ['file'=> $request->file],['file' => 'required|max:20480']
        );

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $v = Validator::make(
            ['extension' => strtolower($request->file->getClientOriginalExtension())], ['extension' => 'required|in:csv']
        );

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->addTeachersCSV($user->id,$request->file);
        return $this->handleResponse($response);


    }

    public function addTeachersToClassroom($classroom_id, Request $request){
        $v = Validator::make($request->all(), [

            'teachers' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->assignTeachersToClassRoom($user->id,$classroom_id,$request->teachers);
        return $this->handleResponse($response);
    }

    public function UnassignTeachersFromClassroom($classroom_id,Request $request)
    {
        $v = Validator::make($request->all(), [

            'teachers' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->UnassignTeachersFromClassroom($user->id,$classroom_id,$request->teachers);
        return $this->handleResponse($response);
    }

    public function addMainRoleToTeacher($classroom_id,Request $request)
    {
        $v = Validator::make($request->all(), [

            'teacher' => 'required',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->addMainRoleToTeacher($user->id,$classroom_id,$request->teacher);
        return $this->handleResponse($response);
    }

    public function removeMainRoleFromTeacher($classroom_id,Request $request)
    {
        $v = Validator::make($request->all(), [

            'teacher' => 'required',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->removeMainRoleFromTeacher($user->id,$classroom_id,$request->teacher);
        return $this->handleResponse($response);
    }

    public function getTeacherClassrooms(){

        $limit = 100;
        $start_from = 0;
        $search = null;
        $grade_name = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTeacherClassrooms($user->id,$start_from,$limit,$grade_name,$search);
        return $this->handleResponse($response);
    }

    public function countTeacherClassrooms(){

        $search = null;
        $grade_name = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->countTeacherClassrooms($user->id,$grade_name,$search);
        return $this->handleResponse($response);
    }

    public function getTeacherCourses(){
        $limit = 100;
        $start_from = 0;
        $search = null;

        $classroom_id = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['classroom_id'])){
            $classroom_id = $_GET['classroom_id'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTeacherCourses($user->id,$start_from,$limit,$classroom_id,$search);
        return $this->handleResponse($response);
    }

    public function countTeacherCourses(){
        $search = null;
        $classroom_id = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['classroom_id'])){
            $classroom_id = $_GET['classroom_id'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->countTeacherCourses($user->id,$classroom_id,$search);
        return $this->handleResponse($response);
    }

    public function getStudentsDataByCourseId(Request $request){
        $v = Validator::make($request->all(), [
            'course_id' => 'required|integer',
            'adventure_id'=>'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $limit = 100;
        $start_from = 0;
        $search = null;


        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentsDataByCourseId($user->id,$request['course_id'],$request['adventure_id'],$start_from,$limit,$search);
        return $this->handleResponse($response);

    }

    public function getStudentsDataByCourseIdCount(Request $request){
        $v = Validator::make($request->all(), [
            'course_id' => 'required|integer',
            'adventure_id'=>'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }


        $search = null;

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentsDataByCourseIdCount($user->id,$request['course_id'],$request['adventure_id'],$search);
        return $this->handleResponse($response);
    }

    public function getStudentsDataByClassroomId(Request $request){
        $v = Validator::make($request->all(), [
            'classroom_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;


        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentDataByClassroomId($user->id,$request['classroom_id'],$start_from,$limit,$search);
        return $this->handleResponse($response);
    }

    public function getStudentDataByClassroomIdCount(Request $request){
        $v = Validator::make($request->all(), [
            'classroom_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $search = null;

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentDataByClassroomIdCount($user->id,$request['classroom_id'],$search);
        return $this->handleResponse($response);
    }

    public function getStudentInfoById(Request $request){
        $v = Validator::make($request->all(), [
            'student_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentInfoById($user->id,$request['student_id']);
        return $this->handleResponse($response);
    }

    public function getTopStudentsInClassroom(Request $request){
        $v = Validator::make($request->all(), [
            'classroom_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTopStudentsInClassroom($user->id,$request['classroom_id']);
        return $this->handleResponse($response);
    }

    public function getClassroomScoreRange(Request $request){
        $v = Validator::make($request->all(), [
            'classroom_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getClassroomScoreRange($user->id,$request['classroom_id']);
        return $this->handleResponse($response);
    }

    public function generalStatisticsTeacher(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->generalStatisticsTeacher($user->id);
        return $this->handleResponse($response);
    }

    public function getTeacherMissionsChart(Request $request){

        $v = Validator::make($request->all(), [
            'from_time'=>'required|date',
            'to_time'=>'required|date',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTeacherMissionsChart($user->id,$request['from_time'],$request['to_time'],$request['classroom_id']);
        return $this->handleResponse($response);
    }

    public function getTeacherActivityEvents(Request $request){
        $v = Validator::make($request->all(), [
            'from_time'=>'date',
            'to_time'=>'date',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTeacherActivityEvents($user->id,$request['from_time'],$request['to_time']);
        return $this->handleResponse($response);
    }

    public function getStudentBlockingTasks($student_id)
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentBlockingTasks($user->id,$student_id);
        return $this->handleResponse($response);
    }

    public function getStudentLastPlayedTask($student_id)
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentLastPlayedTask($user->id,$student_id);
        return $this->handleResponse($response);
    }

    public function getStudentFeeds($student_id)
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentFeeds($user->id,$student_id);
        return $this->handleResponse($response);
    }

    // camps part
    public function generalTeacherStatisticsInCamp(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->generalTeacherStatisticsInCamp($user->id);
        return $this->handleResponse($response);
    }

    public function getTopStudentsInCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTopTenStudentsInCamp($user->id, $request->camp_id);
        return $this->handleResponse($response);
    }

    public function getCampTasksChart(Request $request){
        $v = Validator::make($request->all(), [
            'from_time' => 'required|date',
            'to_time' => 'required|date'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTeacherCampTasksChart($user->id, $request['from_time'], $request['to_time']);
        return $this->handleResponse($response);
    }

    public function getAllTeacherCamps(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $status = 'running';
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['type'])){
            $status = $_GET['type'];
        }
        $response = $this->schoolService->getAllTeacherCampsByStatus($user->id, $status, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getAllTeacherCampsCount(){
        $user = JWTAuth::toUser();
        $search = null;
        $status = 'running';
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['type'])){
            $status = $_GET['type'];
        }
        $response = $this->schoolService->countTeacherCamps($user->id, $status, $search);
        return $this->handleResponse($response);
    }

    public function getAllActiveTeacherCamps(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAllActiveTeacherCamps($user->id);
        return $this->handleResponse($response);
    }

    public function getAllActivitiesInTeacherCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAllActivitiesInTeacherCamp($user->id, $request['camp_id'], $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function countActivitiesInTeacherCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->countActivitiesInTeacherCamp($user->id, $request['camp_id'], $search);
        return $this->handleResponse($response);
    }

    public function getAllStudentsInTeacherCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAllStudentsInTeacherCamp($user->id, $request['camp_id'], $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function CountStudentsInTeacherCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->CountStudentsInTeacherCamp($user->id, $request['camp_id'], $search);
        return $this->handleResponse($response);
    }

    public function getActivityStudentsInTeacherCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1',
            'activity_id' => 'required|numeric|min:1',
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getActivityStudentsInTeacherCamp($user->id, $request['camp_id'], $request['activity_id'], $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function countActivityStudentsInTeacherCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1',
            'activity_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->countActivityStudentsInTeacherCamp($user->id, $request['camp_id'], $request['activity_id'], $search);
        return $this->handleResponse($response);
    }

    public function getStudentTasksInActivity(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1',
            'activity_id' => 'required|numeric|min:1',
            'student_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentTasksInActivity($user->id, $request['camp_id'], $request['activity_id'], $request['student_id']);
        return $this->handleResponse($response);
    }

    public function getStudentInfoInCamp(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1',
            'student_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentInfoInCamp($user->id, $request['camp_id'], $request['student_id']);
        return $this->handleResponse($response);
    }

    public function getStudentFeedsInCamp($student_id, Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentFeedsInCamp($user->id, $request['camp_id'], $student_id);
        return $this->handleResponse($response);
    }

    public function getRecentActivitiesInCamp(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getRecentActivitiesInCamp($user->id);
        return $this->handleResponse($response);
    }

    public function getRunningTeacherCamps(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getRunningTeacherCamps($user->id);
        return $this->handleResponse($response);
    }

    public function getActivityCampStatistics($activity_id, Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|numeric|min:1',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getActivityCampStatistics($user->id, $request->camp_id, $activity_id);
        return $this->handleResponse($response);
    }


}