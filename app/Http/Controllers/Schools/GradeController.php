<?php
namespace App\Http\Controllers\Schools;


use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Http\Request;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;
use App\ApplicationLayer\Accounts\Dtos\UserDto;

use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;

use App\Http\Controllers\Controller;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ISchoolMainService $schoolService){
        $this->middleware('jwt.auth', ['except' =>[]]);
        $this->schoolService = $schoolService;
    }
    public function index()
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAllGrades($user->id);
        return $this->handleResponse($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
