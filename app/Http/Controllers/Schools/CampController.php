<?php

namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\UserRequestDto;
use App\ApplicationLayer\Schools\CustomMappers\SchoolDtoMapper;
use App\ApplicationLayer\Schools\Dtos\CampRequestDto;
use App\ApplicationLayer\Schools\Dtos\CustomPlanDto;
use App\ApplicationLayer\Schools\Dtos\DistributorSubscriptionDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\ApplicationLayer\Schools\Dtos\TeacherDto;
use App\Helpers\Mapper;
use Illuminate\Http\Request;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class CampController extends Controller
{
    private $schoolService;

    public function __construct(ISchoolMainService $schoolService){
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
        $this->middleware('allow-origin');
        $this->schoolService = $schoolService;
    }

    public function index(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $status = 'running';
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['type'])){
            $status = $_GET['type'];
        }

        $response = $this->schoolService->getAllCampsByStatus($user->id, $status, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function show($id){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getCamp($user->id, $id);
        return $this->handleResponse($response);
    }

    public function destroy($id){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->deleteCamp($user->id, $id);
        return $this->handleResponse($response);
    }

    public function countCamps(){
        $user = JWTAuth::toUser();
        $search = null;
        $status = 'running';
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['type'])){
            $status = $_GET['type'];
        }
        $response = $this->schoolService->countCampsByStatus($user->id, $status, $search);
        return $this->handleResponse($response);
    }

    public function generalStatisticsCamp(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->generalStatisticsCamp($user->id);
        return $this->handleResponse($response);
    }

    public function getTopStudents(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTopStudentsInCamp($user->id, $request->camp_id);
        return $this->handleResponse($response);
    }

    public function getCampTasksChart(Request $request){
        $v = Validator::make($request->all(), [
            'from_time' => 'required|date',
            'to_time' => 'required|date'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getCampTasksChart($user->id, $request['from_time'], $request['to_time']);
        return $this->handleResponse($response);
    }

    public function getCampActivityEvents(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getCampActivityEvents($user->id);
        return $this->handleResponse($response);
    }

    public function getStudentsInCamp($camp_id){
        $user = JWTAuth::toUser();
        $limit = 200;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->schoolService->getStudentsInCamp($user->id, $camp_id, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getTeachersInCamp($camp_id){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $response = $this->schoolService->getTeachersInCamp($user->id, $camp_id, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function store(Request $request){
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'startdate' => 'required|integer',
            'enddate' => 'required|integer',
            'agefrom' => 'required|integer|min:1',
            'numberofstudents' => 'required|integer|min:1',
            'ageto' => 'integer|min:1',
            'students' => 'array',
            'teachers' => 'array',
            'activities' => 'required|array',
            'distributor_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $campDto = Mapper::MapRequest(CampRequestDto::class, $request->all());
        $response = $this->schoolService->createCamp($campDto, $user->id);
        return $this->handleResponse($response);
    }

    public function getAllStudentsInCamps(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $grade_name = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }
        $response = $this->schoolService->getAllStudentsInCamps($user->id, $grade_name, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getAllTeachersInCamps(){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->schoolService->getAllTeachersInCamps($user->id, $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getCountStudentsInCamps(){
        $user = JWTAuth::toUser();
        $search = null;
        $grade_name = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }
        $response = $this->schoolService->getCountStudentsInCamps($user->id, $grade_name, $search);
        return $this->handleResponse($response);
    }

    public function getCountTeachersInCamps(){
        $user = JWTAuth::toUser();
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->schoolService->getCountTeachersInCamps($user->id, $search);
        return $this->handleResponse($response);
    }

    public function addTeacher(Request $request){
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'min:6|max:24',
            'email' => 'required|email',
            'camp' => 'numeric|min:1',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = Mapper::MapRequest(UserDto::class, $request->all());
        $userDto->camp_id = $request['camp'];

        $user = JWTAuth::toUser();
        $response = $this->schoolService->addNewTeacherToCamp($user->id, $userDto);
        return $this->handleResponse($response);
    }

    public function addStudent(Request $request){
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'min:6|max:24',


        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $studentDto = Mapper::MapRequest(UserDto::class, $request->all());

        $user = JWTAuth::toUser();
        $response = $this->schoolService->addNewStudentToCamp($user->id, $studentDto);
        return $this->handleResponse($response);
    }

    public function update($campId, Request $request){
        $errors = [
            'change' => 'required|boolean',
            'name' => 'required|string|max:255',
            'startdate' => 'required|integer',
            'enddate' => 'required|integer',
            'agefrom' => 'required|integer|min:1',
            'ageto' => 'integer|min:1',
            'numberofstudents' => 'required|integer|min:1',
            'students' => 'array',
            'teachers' => 'array',
        ];
        if(isset($request->change) && $request->change != 0){
            array_push($errors, [
                'activities' => 'required|array',
            ]);
        }
        $v = Validator::make($request->all(), $errors);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $campDto = Mapper::MapRequest(CampRequestDto::class, $request->all());
        $response = $this->schoolService->updateCamp($user->id, $campDto, $campId);
        return $this->handleResponse($response);
    }

    public function setAssignedAndUnassignedStudentsInCamp($camp_id, Request $request){
        $v = Validator::make($request->all(), [
            'assignedStudents' => 'array',
            'unAssignedStudents' => 'array',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->setAssignedAndUnassignedStudentsInCamp($user->id, $camp_id, $request->unAssignedStudents, $request->assignedStudents);
        return $this->handleResponse($response);
    }

    public function setAssignedAndUnassignedTeachersInCamp($camp_id, Request $request){
        $v = Validator::make($request->all(), [
            'assignedTeachers' => 'array',
            'unAssignedTeachers' => 'array',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->setAssignedAndUnassignedTeachersInCamp($user->id, $camp_id, $request->unAssignedTeachers, $request->assignedTeachers);
        return $this->handleResponse($response);
    }

    public function getAllInActiveCamps(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAllInActiveCampsNames($user->id);
        return $this->handleResponse($response);
    }

    public function getCampsStatus(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getCampsStatus($user->id);
        return $this->handleResponse($response);
    }

    public function getAllActiveCamps(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAllActiveCampsNames($user->id);
        return $this->handleResponse($response);
    }

    public function getRunningCamps(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getRunningCamps($user->id);
        return $this->handleResponse($response);
    }

    public function deleteCamps(Request $request){
        $v = Validator::make($request->all(), [
            'camps' => 'required|array',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->deleteCamps($user->id, $request->camps);
        return $this->handleResponse($response);
    }

    public function getActivityInCamp($camp_id, $activity_id, Request $request){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getActivityInCamp($user->id, $camp_id, $activity_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function activateCamp($camp_id, Request $request){
        $v = Validator::make($request->all(), [
            'code' => 'required|string|max:255',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->activateCamp($user->id, $camp_id, $request->code);
        return $this->handleResponse($response);
    }

    public function getActivityTasksInCamp($camp_id, $activity_id, Request $request){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getActivityTasksInCamp($user->id, $camp_id, $activity_id, $request->is_default);
        return $this->handleResponse($response);
    }


}