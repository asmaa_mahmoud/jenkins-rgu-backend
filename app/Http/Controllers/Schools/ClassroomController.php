<?php

namespace App\Http\Controllers\Schools;

use Illuminate\Http\Request;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;

use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\ApplicationLayer\Schools\CustomMappers\ClassroomDtoMapper;

use App\Http\Controllers\Controller;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;

class ClassroomController extends Controller
{

    private $schoolService;

    public function __construct(ISchoolMainService $schoolService){
        //$this->middleware('allow-origin');
        $this->schoolService = $schoolService;
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
    }

    public function count(){
        $user = JWTAuth::toUser();
        $search = null;
        $grade_name = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $response = $this->schoolService->countClassrooms($user->id,$grade_name,$search);
        return $this->handleResponse($response);

    }

    public function index()
    {
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $grade_name = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $response = $this->schoolService->getAllClassrooms($user->id,$start_from,$limit,$grade_name,$search);
        return $this->handleResponse($response);
    }


    public function indexClassRoomStudents($classroom_id){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $response = $this->schoolService->getStudentsInClassroom($user->id,$classroom_id,$start_from,$limit,$search);
        return $this->handleResponse($response);
    }

    public function indexClassRoomTeachers($classroom_id){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $response = $this->schoolService->getTeachersInClassroom($user->id,$classroom_id,$start_from,$limit,$search);
        return $this->handleResponse($response);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|string',
            'grade' => 'required',
            'max_students'=>'integer|min:1',
            'journeys' => 'array',
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $classroomDto = Mapper::MapRequest(ClassroomRequestDto::class,$request->all());
        $response = $this->schoolService->createClassroom($classroomDto,$user->id);
        return $this->handleResponse($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->GetClassroom($user->id,$id);
        return $this->handleResponse($response);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|string',
            'grade' => 'required',
            'journeys'=>'array'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $classroomDto = Mapper::MapRequest(ClassroomRequestDto::class,$request->all());
        $force = false;
        if($request->force != null){
            $force = $request->force;
        }
        //$classroomDto = Mapper::MapRequest(ClassroomDto::class,$request->all());
        $response = $this->schoolService->updateClassroom($user->id,$classroomDto,$id,$force);
        return $this->handleResponse($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = JWTAuth::toUser();
        $response = $this->schoolService->deleteClassroom($user->id,$id);
        return $this->handleResponse($response);
    }

    public function getGrades()
    {
        $response = $this->schoolService->GetGrades();
        return $this->handleResponse($response);
    }

    public function assignJourneysToClassroom($classroom_id,Request $request)
    {
        $v = Validator::make($request->all(), [

            'journeys' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->assignJourneysToClassroom($user->id,$classroom_id,$request->journeys);
        return $this->handleResponse($response);
    }

    public function unassignJourneysFromClassroom($classroom_id,Request $request)
    {
        $v = Validator::make($request->all(), [

            'journeys' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->unassignJourneysFromClassroom($user->id,$classroom_id,$request->journeys);
        return $this->handleResponse($response);
    }


    public function getUnEnrolledStudents($classroom_id){
        $user = JWTAuth::toUser();
        $start_from = 0;
        $search = null;
        $limit = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->schoolService->getUnEnrolledStudents($user->id,$classroom_id,$start_from,$limit,$search);
        return $this->handleResponse($response);
    }

    public function getUnAssignedTeachers($classroom_id){
        $user = JWTAuth::toUser();
        $start_from = 0;
        $search = null;
        $limit = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->schoolService->getUnAssignedTeachers($user->id,$classroom_id,$start_from,$limit,$search);
        return $this->handleResponse($response);
    }

    public function deleteClassrooms(Request $request)
    {
        $v = Validator::make($request->all(), [

            'classrooms' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->deleteClasrooms($user->id,$request->classrooms);
        return $this->handleResponse($response);
    }

    public function getAssignedAndUnassignedStudentsInClassroom($classroom_id){
        $start_from = 0;
        $search = null;
        $limit = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAssignedAndUnassignedStudentsInClassroom($user->id,$classroom_id,$start_from,$limit,$search);
        return $this->handleResponse($response);
    }

    public function getAssignedAndUnassignedTeachersInClassroom($classroom_id){
        $start_from = 0;
        $search = null;
        $limit = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getAssignedAndUnassignedTeachersInClassroom($user->id,$classroom_id,$start_from,$limit,$search);
        return $this->handleResponse($response);
    }

    public function setAssignedAndUnassignedStudentsInClassroom(Request $request ,$classroom_id){
        $v = Validator::make($request->all(), [
            'assignedStudents' => 'array',
            'unAssignedStudents' => 'array',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->setAssignedAndUnassignedStudentsInClassroom($user->id,$classroom_id,$request->unAssignedStudents,$request->assignedStudents);
        return $this->handleResponse($response);
    }

    public function setAssignedAndUnassignedTeachersInClassroom(Request $request ,$classroom_id){

        $v = Validator::make($request->all(), [
            'assignedTeachers' => 'array',
            'unAssignedTeachers'=> 'array',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->setAssignedAndUnassignedTeachersInClassroom($user->id,$classroom_id,$request->unAssignedTeachers,$request->assignedTeachers);
        return $this->handleResponse($response);
    }
}