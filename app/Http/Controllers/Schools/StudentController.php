<?php
namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use Illuminate\Http\Request;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    private $schoolService;

    public function __construct(ISchoolMainService $schoolService){
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
        //$this->middleware('allow-origin');
        $this->schoolService = $schoolService;
    }

    public function count(){
        $user = JWTAuth::toUser();
        $search = null;
        $grade_name = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $response = $this->schoolService->countStudents($user->id,$grade_name,$search);
        return $this->handleResponse($response);

    }

    public function index()
    {   
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $grade_name = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }

        if(isset($_GET['grade_name'])){
            $grade_name = $_GET['grade_name'];
        }

        $response = $this->schoolService->getAllStudents($user->id,$start_from,$limit,$grade_name,$search);
        return $this->handleResponse($response);
    }

    public function show($id)
    {  
        $user = JWTAuth::toUser();      
        $response = $this->schoolService->getStudentById($user->id,$id);
        return $this->handleResponse($response);
    }

    public function update(Request $request, $id) {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'min:6|max:24',
            'classroom' => 'min:1',
            'camp' => 'min:1',
            'force' => 'boolean',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $studentDto = Mapper::MapRequest(StudentDto::class, $request->all());
        $studentDto->id = $id;
        $studentDto->password = $request['password'];
        $studentDto->classroom = $request['classroom'];
        $studentDto->camp = $request['camp'];
        $studentDto->force = $request['force'];
        $user = JWTAuth::toUser();
        $response = $this->schoolService->updateStudent($user->id,$studentDto);
        return $this->handleResponse($response);
    }

    public function deleteStudents(Request $request)
    {

        $v = Validator::make($request->all(), [

            'students' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->deleteStudents($user->id,$request->students);
        return $this->handleResponse($response);
    }

    public function store(Request $request){

        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'required|min:6|max:24',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $studentDto = Mapper::MapRequest(StudentDto::class,$request->all());
        $studentDto->password = $request->password;
        if($request->classroom != null)
            $studentDto->classroom_id = $request->classroom;
        $user = JWTAuth::toUser();
        $response = $this->schoolService->addStudent($user->id,$studentDto);
        return $this->handleResponse($response);
    }

    public function addStudentsCSV(Request $request){
    
        $v = Validator::make(
            ['file'=> $request->file],['file' => 'required|max:20480']
        );

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $v = Validator::make(
            ['extension' => strtolower($request->file->getClientOriginalExtension())], ['extension' => 'required|in:csv']
        );

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();

        $response = $this->schoolService->addStudentsCSV($user->id,$request->file);
        return $this->handleResponse($response);

    }

    public function addStudentsToClassroom($classroom_id, Request $request){
        $v = Validator::make($request->all(), [

            'students' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->assignStudentsToClassRoom($user->id,$classroom_id,$request->students);
        return $this->handleResponse($response);
    }

    public function UnassignStudentsFromClassroom($classroom_id, Request $request)
    {
        $v = Validator::make($request->all(), [

            'students' => 'required|array',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->UnassignStudentsFromClassroom($user->id,$classroom_id,$request->students);
        return $this->handleResponse($response);
    }

    public function updateMissionProgress(Request $request)
    {
        $v = Validator::make($request->all(), [

            'mission_id' => 'required',
            'adventure_id' => 'required',
            'course_id' => 'required',
            'success' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->updateMissionProgress($user->id,$request->mission_id,$request->adventure_id,$request->course_id,$request->success,$request->noOfBlocks,$request->timeTaken,$request->xmlCode);
        return $this->handleResponse($response);
    }

    public function updateHTMLMissionProgress(Request $request)
    {
        $v = Validator::make($request->all(), [

            'mission_id' => 'required',
            'adventure_id' => 'required',
            'course_id' => 'required',
            'success' => 'required',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->updateHTMLMissionProgress($user->id,$request->mission_id,$request->adventure_id,$request->course_id,$request->success);
        return $this->handleResponse($response);
    }

    public function updateQuizProgress(Request $request)
    {
        $v = Validator::make($request->all(), [

            'quiz_id' => 'required',
            'adventure_id' => 'required',
            'course_id' => 'required',
            'quiz_data' => 'required',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->schoolService->updateQuizProgress($user->id,$request->quiz_id,$request->adventure_id,$request->course_id,$request->success,$request->quiz_data);
        return $this->handleResponse($response);
    }

    public function getTotalScore()
    {
        $user = JWTAuth::toUser();

        $response = $this->schoolService->getTotalScore($user->id);
        return $this->handleResponse($response);
    }

    public function getCourses()
    {
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $response = $this->schoolService->getCourses($user->id,$limit,$start_from,$search);
        return $this->handleResponse($response);
    }

    public function getAdventures()
    {
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $status = "";
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        if(isset($_GET['status'])){
            $status = $_GET['status'];
        }
        $response = $this->schoolService->getAdventures($user->id,$limit,$start_from,$search,$status);
        return $this->handleResponse($response);
    }

     public function getCourseData($course_id)
     {
         $user = JWTAuth::toUser();
         $response = $this->schoolService->getCourseData($user->id,$course_id);
         return $this->handleResponse($response);
     }

     public function getStudentPosition($course_id)
     {
         $user = JWTAuth::toUser();
         $response = $this->schoolService->getStudentPosition($user->id,$course_id);
         return $this->handleResponse($response);
     }

     public function updateStudentPosition($course_id,Request $request)
     {
         $v = Validator::make($request->all(), [
             'adventure_id' => 'required',
             'id' => 'required',
             'type' => 'required|in:mission,quiz,html_mission',
             'index' => 'required|integer|min:0|max:7'
         ]);

         if ($v->fails()) {
             return $this->handleValidationError($v->errors()->all());
         }

         $user = JWTAuth::toUser();
         $response = $this->schoolService->updateStudentPosition($user->id,$course_id,$request->adventure_id,$request->id,$request->type,$request->index);
         return $this->handleResponse($response);
     }

    public function getNextTask(Request $request)
    {
        $v = Validator::make($request->all(), [
            'course_id' => 'required',
            'adventure_id' => 'required',
            'id' => 'required',
            'type' => 'required|in:mission,quiz',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->schoolService->getNextTask($request->course_id,$request->adventure_id,$request->id,$request->type);
        return $this->handleResponse($response);
    }

    public function getNextTaskHtml(Request $request)
    {
        $v = Validator::make($request->all(), [
            'course_id' => 'required',
            'adventure_id' => 'required',
            'id' => 'required',
            'type' => 'required|in:mission,quiz',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->schoolService->getNextTaskHtml($request->course_id,$request->adventure_id,$request->id,$request->type);
        return $this->handleResponse($response);
    }

    public function getTaskAuthorization(Request $request)
    {
        $v = Validator::make($request->all(), [
            'course_id' => 'required',
            'adventure_id' => 'required',
            'id' => 'required',
            'type' => 'required|in:mission,quiz',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->getTaskAuthorization($user->id,$request->course_id,$request->adventure_id,$request->id,$request->type);
        return $this->handleResponse($response);
    }

    public function storeStudentMissionClick(Request $request){

        $v = Validator::make($request->all(), [
            'course_id' => 'required|integer',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->schoolService->storeStudentMissionClick($user->id,$request['course_id']);
        return $this->handleResponse($response);
    }

    public function updateMCQMissionProgress(Request $request)
    {
        $v = Validator::make($request->all(), [

            'mission_id' => 'required',
            'adventure_id' => 'required',
            'course_id' => 'required',
            'success' => 'required',
            'quiz_data' => 'required',
            'selected_ids' => 'array',
            'use_model_answer' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }

        $response = $this->schoolService->updateMCQMissionProgress($user->id,$request->mission_id,$request->adventure_id,$request->course_id,$request->success,$request->quiz_data,$request->selected_ids,$request->use_model_answer,$request->noOfBlocks,$request->timeTaken);
        return $this->handleResponse($response);
    }

    public function getStudentCamps(){
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentCamps($user->id);
        return $this->handleResponse($response);
    }

    public function getStudentActivities(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|integer|min:1',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if(isset($_GET['start_from'])){
            $start_from = $_GET['start_from'];
        }
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentActivities($user->id, $request['camp_id'], $start_from, $limit, $search);
        return $this->handleResponse($response);
    }

    public function getStudentActivitiesCount(Request $request){
        $v = Validator::make($request->all(), [
            'camp_id' => 'required|integer|min:1',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $search = null;
        if(isset($_GET['search'])){
            $search = $_GET['search'];
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->getStudentActivitiesCount($user->id, $request['camp_id'], $search);
        return $this->handleResponse($response);
    }

    public function deleteStudentsCamp(Request $request){
        $v = Validator::make($request->all(), [
            'students' => 'required|array',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->schoolService->deleteStudentsCamp($user->id, $request->students);
        return $this->handleResponse($response);
    }

}