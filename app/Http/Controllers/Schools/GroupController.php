<?php

namespace App\Http\Controllers\Schools;

use Illuminate\Http\Request;
use App\Helpers\Mapper;
use Illuminate\Support\Facades\Validator;

use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\ApplicationLayer\Schools\CustomMappers\ClassroomDtoMapper;

use App\Http\Controllers\Controller;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use JWTAuth;

class GroupController extends Controller
{

    private $schoolService;

    public function __construct(ISchoolMainService $schoolService)
    {
        //$this->middleware('allow-origin');
        $this->schoolService = $schoolService;
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
    }
}