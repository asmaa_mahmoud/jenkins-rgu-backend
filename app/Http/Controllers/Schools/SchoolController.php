<?php

namespace App\Http\Controllers\Schools;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\CustomMappers\SupportEmailDtoMapper;
use App\ApplicationLayer\Schools\CustomMappers\SchoolDtoMapper;
use App\DomainModelLayer\Accounts\User;
use Illuminate\Http\Request;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\Framework\Exceptions\BadRequestException;
use JWTAuth;
use App\Helpers\Mapper;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\ApplicationLayer\Accounts\Dtos\UserJourneyRequestDto;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use Analogue;
use Config;
use Stripe;
use App\ApplicationLayer\Accounts\Dtos\UserBasicInfoDto;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class SchoolController extends Controller
{
    private $schoolService;

    public function __construct(ISchoolMainService $schoolService){
        $this->middleware('jwt.auth', ['except' =>[]]);
        $this->middleware('check-subscription', ['only' =>[]]);
        $this->middleware('allow-origin');
        $this->schoolService = $schoolService;
    }

}