<?php

namespace App\Http\Controllers\Accounts;

use App\ApplicationLayer\Accounts\CustomMappers\GuestMapper;
use App\ApplicationLayer\Customers\IUserService;
use Illuminate\Http\Request;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\ApplicationLayer\Customers\UserDto;
use App\Http\Controllers\Controller;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    private $familyService;

    public function __construct(IAccountMainService $familyService){
        $this->familyService = $familyService;
    }

    public function index(Request $request){
        $v = Validator::make($request->all(), [
            'email' => 'email|max:255',
            'message' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->familyService->sendFeedbackMail($request->input('name'), $request->input('feeling'),
                                                           $request->input('email'), $request->input('message'));
        return $this->handleResponse($response);
    }

    public function getGuestData(){
        $ipAddress = null;
        if(isset($_GET['ip_address'])) {
            $ipAddress = $_GET['ip_address'];
        }
        $response = $this->familyService->getGuestData($ipAddress);
        return $this->handleResponse($response);
    }

    public function saveGuestData(Request $request){
        $v = Validator::make($request->all(), [
            'ip_address' => 'required',
            'coins' => 'required|numeric',
            'xp' => 'required|numeric',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $guestDto = GuestMapper::RequestMapper($request);
        $response = $this->familyService->saveGuestData($guestDto);
        return $this->handleResponse($response);

    }



}