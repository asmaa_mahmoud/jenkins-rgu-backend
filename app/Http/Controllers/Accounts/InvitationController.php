<?php

namespace App\Http\Controllers\Accounts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use Carbon\Carbon;
use App\ApplicationLayer\Accounts\Dtos\InvitationRequestDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\Helpers\Mapper;

class InvitationController extends Controller
{

    private $accountService;

    public function __construct(IAccountMainService $accountService){
        $this->accountService = $accountService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
           'invitation_name' => 'required',
           'invitation_code' => 'required',
           'account_type' => 'required',
           'end_date' => 'required|date|after:'.Carbon::now(),
           'number_of_users' => 'required|integer|min:0'
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

       $invitation_dto = Mapper::MapRequest(InvitationRequestDto::class,$request->all());

       $response = $this->accountService->storeInvitation($invitation_dto);
       return $this->handleResponse($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validateCode($code) 
    {
       $response = $this->accountService->validateCode($code);
       return $this->handleResponse($response);
    }

    public function register(Request $request) 
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24|regex:/^[a-zA-Z0-9_\\$@\\-]+$/',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'invitation_code' => 'required'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user_dto = Mapper::MapRequest(UserDto::class,$request->all());

        $response = $this->accountService->registerInvitation($user_dto,$request->invitation_code);
        return $this->handleResponse($response);
    }

    public function registerIndividual(Request $request) 
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24|regex:/^[a-zA-Z0-9_\\$@\\-]+$/',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'invitation_code' => 'required'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user_dto = Mapper::MapRequest(UserDto::class,$request->all());

        $response = $this->accountService->registerIndividual($user_dto,$request->invitation_code);
        return $this->handleResponse($response);
    }

    public function registerStudent(Request $request) 
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'required|min:6|max:24|regex:/^[a-zA-Z0-9_\\$@\\-]+$/',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'invitation_code' => 'required'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user_dto = Mapper::MapRequest(UserDto::class,$request->all());

        $response = $this->accountService->registerStudent($user_dto,$request->invitation_code);
        return $this->handleResponse($response);
    }

}
