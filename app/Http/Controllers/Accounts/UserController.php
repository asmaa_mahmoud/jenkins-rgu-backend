<?php

namespace App\Http\Controllers\Accounts;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\CustomMappers\SupportEmailDtoMapper;
use App\DomainModelLayer\Accounts\User;
use Illuminate\Http\Request;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\Framework\Exceptions\BadRequestException;
use JWTAuth;
use App\Helpers\Mapper;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\ApplicationLayer\Accounts\Dtos\UserJourneyRequestDto;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use Analogue;
use Config;
use Stripe;
use App\ApplicationLayer\Accounts\Dtos\UserBasicInfoDto;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    private $accountService;

    public function __construct(IAccountMainService $accountService){
        $this->middleware('jwt.auth', ['except' =>['loginRoboGardenFacebook', 'addUserCoupon','learnerRegister','homeSchoolerRegister','superAdminRegister', 'getCountriesUsersCount', 'UserSubscription', 'loginFacebook', 'loginTwitter', 'loginGoogle','store','getAvailablePlans','getAllPlanPeriods','getUserType','activateParent','sendContactUsEmail','validateUsername','validateUserEmail','resetPassword','confirmResetPassword','downloadInvoicePDF','getExtraPlans','getLanguages','reportBug','resendActivationMail']]);
        $this->middleware('check-subscription', ['only' =>['addUserJourney','getUserJourneys','updateUserPosition']]);
        $this->middleware('allow-origin');
        $this->accountService = $accountService;
    }

    public function loginFacebook(Request $request)
    {
        $v = Validator::make($request->all(), [
            'code' => 'required',
            'clientId' => 'required',
            'redirectUri' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->accountService->loginFacebook($request->input('code'),$request->input('clientId'),$request->input('redirectUri'),'3651a21e161e3321dff84906f38bc520');
        return $this->handleResponse($response);

    }

    public function loginRoboGardenFacebook(Request $request){
        $v = Validator::make($request->all(), [
            'social_id' => 'required',
            'social_track' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $userDto = UserDtoMapper::RequestMapper($request);
        $response = $this->accountService->loginRoboGardenFacebook($userDto);
        return $this->handleResponse($response);

    }

    public function loginTwitter(Request $request)
    {

        $response = $this->accountService->loginTwitter($request->input('oauth_token'),$request->input('oauth_verifier'),$request->input('redirectUri'),'XiQhy9UtM7lvaBSZ20W797nBt','wxR0P47lIltHjfypWOV3KwPqT8XtykqBQWUsI7DHNemMJGqDAj');
        if($request->has('oauth_verifier'))
        {
            return $this->handleResponse($response);
        }
        return $response;
    }

    public function loginGoogle(Request $request)
    {
        $v = Validator::make($request->all(), [
            'code' => 'required',
            'clientId' => 'required',
            'redirectUri' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->accountService->loginGoogle($request->input('code'),$request->input('clientId'),$request->input('redirectUri'),'KDfri5vy5ZAhIKj71Oytx12L');
        return $this->handleResponse($response);
    }

    public function addChildManually(Request $request){
         $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'required|min:6|max:24',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $childDto = UserDtoMapper::RequestMapper($request);
        $childDto->accounttype = "Family";
        $childDto->role = "child";

        $parent = JWTAuth::toUser();
        $childDto->supervisorEmail = $parent->email;
        $response = $this->accountService->addChildManually($parent->id, $childDto);
        return $this->handleResponse($response);
    }

    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',
            'accounttype'=>'required',
            'age' => 'numeric|max:99|min:6'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        // if($request['role'] == 'Individual')
        // {
        //     if($request['age'] == null)
        //         throw new BadRequestException("Age is required");
        // }

        // if($request['role'] == 'Child' || $request['role'] == 'Parent')
        // {
        //     $request['accounttype'] = 'Family';
        // }else{
        //     $request['accounttype'] = 'Individual';
        // }
        if($request->accounttype != "Individual" && $request->accounttype != "Family")
            throw new BadRequestException(trans('locale.account_type_must_individual_family'));
            
        $userDto = UserDtoMapper::RequestMapper($request);

        if($userDto->accounttype == 'Individual')
        {
            if($userDto->age != null && $userDto->age < 13)
            {
                $userDto->accounttype = 'Family';
                $userDto->role = 'child';
                $userDto->supervisorEmail = $userDto->email;
                $userDto->email = null;
            }
            else
            {
                $userDto->role = 'user';
            }
        }
        else if($userDto->accounttype == "Family"){
            $userDto->role = "parent";
        }

        if($request->child_token != null)
        {
            //JWTAuth::parseToken('bearer','authorization','child_token');
            //$userDto->child_id = JWTAuth::decode(JWTAuth::getToken())->toArray()['child']['id'];
            $userDto->child_id = decrypt($request->child_token);
        }
        else
           $userDto->child_id = null; 
        // {
        //     $userDto->plan = 'Family_Free';
        // }else{
        //     $userDto->plan = 'Free';    
        // }
        
        $response = $this->accountService->Add($userDto);
        return $this->handleResponse($response);
    }

    public function getAvailablePlans($accountType_id)
    {
        $response = $this->accountService->getAvailablePlans($accountType_id);
        return $this->handleResponse($response);
    }

    public function getOrderedAvailablePlans($accountType_id)
    {
        $response = $this->accountService->getOrderedAvailablePlans($accountType_id);
        return $this->handleResponse($response);
    }

    public function getAvailablePlansByAccountName($accountType_Name)
    {
        $response = $this->accountService->getAvailablePlansByAccountName($accountType_Name);
        return $this->handleResponse($response);
    }

    public function addUserJourney(Request $request)
    {
       $v = Validator::make($request->all(), [
           'journeys' => 'required|array',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

        $userJourneyRequestDto = Mapper::MapRequest(UserJourneyRequestDto::class,$request->all());
        $userJourneyRequestDto->user = JWTAuth::toUser();
        $response = $this->accountService->addJourney($userJourneyRequestDto);
        return $this->handleResponse($response);
    }

    public function getUserJourneys()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserJourneys($user->id);
        return $this->handleResponse($response);
    }

    public function getUserJourneysAll(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserJourneysIncludingLocked($user->id);
        return $this->handleResponse($response);
    }

    public function updateBasicInfo(Request $request)
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'email',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            // 'fname' => 'required|regex:/^[a-zA-Z0-9\u0620-\u064A_\\$@\\-]+$/|min:2|max:15',
            // 'lname' => 'required|regex:/^[a-zA-Z0-9\u0620-\u064A_\\$@\\-]+$/|min:2|max:15',
            // 'username' => 'required|regex:/^[a-zA-Z0-9\u0620-\u064A_\\$@\\-]+$/|min:2|max:75',
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $userBasicInfoDto = Mapper::MapRequest(UserBasicInfoDto::class,$request->all());
        $response = $this->accountService->updateBasicInfo($user->id,$userBasicInfoDto);
        return $this->handleResponse($response);
    }

    public function updatePassword(Request $request)
    {
        $v = Validator::make($request->all(), [
            'old' => 'required|min:6|max:24',
            'new' => 'required|min:6|max:24',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->accountService->updatePassword($user->id,$request->old,$request->new);
        return $this->handleResponse($response);
    }

    public function updateProfileImage(Request $request)
    {
        //return $request->getContent();
        // $v = Validator::make($request->all(), [
        //     'image_name' => 'required',
        //     'image_data' => 'required',
        // ]);

        // if ($v->fails()) {
        //     return $this->handleValidationError($v->errors()->all());
        // }
        $user = JWTAuth::toUser();
        $response = $this->accountService->updateProfileImage($user->id,'user_'.$user->id.'.png',$request->getContent());
        return $this->handleResponse($response);
    }

    public function updateSchoolLogo(Request $request)
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->updateSchoolLogo($user->id,'user_'.$user->id.'.png',$request->getContent());
        return $this->handleResponse($response);
    }

    public function getBasicInfo()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->getBasicInfo($user->id);
        return $this->handleResponse($response);
    }

    public function getUserScores()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserScores($user->id);
        return $this->handleResponse($response);
    }

    public function getAllPlanPeriods()
    {
       $response = $this->accountService->getAllPlanPeriods();
        return $this->handleResponse($response); 
    }

    public function subscribe(Request $request)
    {
       $v = Validator::make($request->all(), [
           'plan' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

        $userSubscriptionDto = Mapper::MapRequest(UserSubscriptionDto::class, $request->all());
        $userSubscriptionDto->user = JWTAuth::toUser();
        $userSubscriptionDto->period = 'monthly';
        $response = $this->accountService->subscribe($userSubscriptionDto,false);
        return $this->handleResponse($response);
    }

    public function getUserSubscriptions()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserSubscriptions($user->id);
        return $this->handleResponse($response);
    }

    public function unsubscribe(Request $request)
    {
       $v = Validator::make($request->all(), [
           'subscription_id' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

        $user = JWTAuth::toUser();
        $response = $this->accountService->unsubscribe($user->id,$request->subscription_id);
        return $this->handleResponse($response); 
    }

    public function upgradePlan(Request $request)
    {
        $v = Validator::make($request->all(), [
           'subscription_id' => 'required',
           'plan_name' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }
        $user = JWTAuth::toUser();
        $response =$this->accountService->upgradePlan($user->id, $request->subscription_id, $request->plan_name,'monthly', $request->stripe_token);
        return $this->handleResponse($response); 
    }

    public function getPlanByName($name)
    {
        $response =$this->accountService->getPlanByName($name);
        return $this->handleResponse($response);
    }

    public function getInvitationByUser()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->getInvitationByUser($user->id);
        return $this->handleResponse($response);
    }

    public function getNotifications()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->getNotifications($user->id);
        return $this->handleResponse($response);
    }

    public function confirmNotification($id)
    {
        $response = $this->accountService->confirmNotification($id);
        return $this->handleResponse($response);
    }

    public function delayNotification($id)
    {
        $response = $this->accountService->delayNotification($id);
        return $this->handleResponse($response);
    }

    public function updateUserPosition(Request $request)
    {
        $v = Validator::make($request->all(), [
           'journey_id' => 'required',
           'adventure_id' => 'required',
           'id' => 'required',
           'type' => 'required|in:mission,quiz,html_mission',
           'index' => 'required|integer|min:0|max:7'
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

        $user = JWTAuth::toUser();
        $response = $this->accountService->updateUserPosition($user->id,$request->journey_id,$request->adventure_id,$request->id,$request->type,$request->index);
        return $this->handleResponse($response); 
    }

    public function getUserType(Request $request)
    {

        $v = Validator::make($request->all(), [
           'child_token' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }
       // JWTAuth::parseToken('bearer','authorization','child_token');
       //  $child_id = JWTAuth::decode(JWTAuth::getToken())->toArray()['child']['id'];
       $child_id = decrypt($request->child_token);
        $response = $this->accountService->getUserType($child_id);
        return $this->handleResponse($response); 
    }

    public function addUserChild(Request $request)
    {
        $v = Validator::make($request->all(), [
           'child_token' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }
       $user = JWTAuth::toUser();
       // JWTAuth::parseToken('bearer','authorization','child_token');
       //  $child_id = JWTAuth::decode(JWTAuth::getToken())->toArray()['child']['id'];
       $child_id = decrypt($request->child_token);
       $response = $this->accountService->addUserChild($user->id,$child_id,$request->stripe_token);
        return $this->handleResponse($response); 
    }

    public function convertToFamily(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->convertToFamily($user->id);
        return $this->handleResponse($response);
    }

    public function activateParent(Request $request)
    {
        $v = Validator::make($request->all(), [
           'child_token' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }
        // JWTAuth::parseToken('bearer','authorization','child_token');
        // $child_id = JWTAuth::decode(JWTAuth::getToken())->toArray()['child']['id'];
        $child_id = decrypt($request->child_token);
        $response = $this->accountService->activateParent($child_id);
        return $this->handleResponse($response);
    }

    public function customerInfo(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->customerInfo($user->id);
        return $this->handleResponse($response);
    }

    public function customerInvoices(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->customerInvoices($user->id);
        return $this->handleResponse($response);
    }

    public function getCustomerInvoiceById($id){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getCustomerInvoiceById($user->id,$id);
        return $this->handleResponse($response);
    }

    public function requestFromParent(Request $request)
    {
        $v = Validator::make($request->all(), [
            'journey_category' => 'required',
        ]);
        $user = JWTAuth::toUser();
        $response = $this->accountService->requestFromParent($user->id,$request->journey_category);
        return $this->handleResponse($response);
    }

    public function requestFromParentActivity(Request $request)
    {
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
            'activity_id'=>'required'
        ]);
        $user = JWTAuth::toUser();
        $response = $this->accountService->requestFromParentActivity($user->id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function sendContactUsEmail(Request $request){

       $v = Validator::make($request->all(), [
           'name' => 'required',
           'email' => 'required|email',
           'message' => 'required'
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

       $supportEmailDto = SupportEmailDtoMapper::RequestMapperContactUs($request);
       $response = $this->accountService->sendContactUsEmail($supportEmailDto);
       return $this->handleResponse($response);

    }

    public function sendSupportEmail(Request $request){
       $v = Validator::make($request->all(), [
           'type' => 'required',
           'message' => 'required'
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }
       $user = JWTAuth::toUser();
       $supportEmailDto = SupportEmailDtoMapper::RequestMapperSupport($request);
       //dd($supportEmailDto);
       $response = $this->accountService->sendSupportEmail($user->id,$supportEmailDto);
       return $this->handleResponse($response);
    }

    public function updateUserCard(Request $request)
    {
        $v = Validator::make($request->all(), [
           'stripe_token' => 'required',
       ]);

        if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

       $user = JWTAuth::toUser();
       $response = $this->accountService->updateCreditCard($user->id,$request->stripe_token);
       return $this->handleResponse($response);
   }

    public function validateUserEmail(Request $request)
    {
        $v = Validator::make($request->all(), [
           'email' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

       $response = $this->accountService->validateUserEmail($request->email);
       return $this->handleResponse($response);
    }

    public function validateUsername(Request $request)
    {
        $v = Validator::make($request->all(), [
           'username' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

       $response = $this->accountService->validateUsername($request->username);
       return $this->handleResponse($response);
    }

    public function resetPassword(Request $request){
        $v = Validator::make($request->all(), [
            'email'=>'required|email',
            ]);

        if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->accountService->resetPassword($request->email);
        return $this->handleResponse($response);
    }

    public function confirmResetPassword(Request $request){
        $v = Validator::make($request->all(),[
            'email'=>'required|email',
            'token'=>'required',
            'password'=>'required']);
        if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
        }
        $passwordResetDto = Mapper::MapRequest(PasswordResetDto::class,$request->all());
        $response = $this->accountService->confirmResetPassword($passwordResetDto,$request['password']);
        return $this->handleResponse($response);
    }

    public function downloadInvoicePDF($id)
    {
        $response = $this->accountService->downloadInvoicePDF($id);
        return $response;
    }

    public function requestCountryChange(Request $request){
        $v = Validator::make($request->all(),[
            'newCountry'=>'required',
          ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());

        }
        $user = JWTAuth::toUser();
        $response = $this->accountService->requestCountryChange($user->id, $request->newCountry);
        return $this->handleResponse($response);
    }

    public function getRbf(Request $request)
    {
        $v = Validator::make($request->all(),[
            'code'=>'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());

        }

        $user = JWTAuth::toUser();
        $response = $this->accountService->getRbf($user->id, $request->code);
        return $this->handleResponse($response);
    }

    public function addExtraPlan(Request $request)
    {
        $v = Validator::make($request->all(), [
           'plan' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

        $user = JWTAuth::toUser();
        $response = $this->accountService->addExtraPlan($user->id, $request->plan);
        return $this->handleResponse($response);
    }

    public function removeExtraPlan(Request $request)
    {
        $v = Validator::make($request->all(), [
           'plan' => 'required',
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }

        $user = JWTAuth::toUser();
        $response = $this->accountService->removeExtraPlan($user->id, $request->plan);
        return $this->handleResponse($response);
    }

    public function authenticateRobopal()
    {
        $user = JWTAuth::toUser();
        $response = $this->accountService->authenticateRobopal($user->id);
        return $this->handleResponse($response);
    }

    public function getExtraPlans($accounttypename)
    {
        $response = $this->accountService->getExtraPlans($accounttypename);
        return $this->handleResponse($response);
    }

    public function getLanguages()
    {
        $response = $this->accountService->getLanguages();
        return $this->handleResponse($response);
    }

    public function updateUserLanguages(Request $request)
    {
        $v = Validator::make($request->all(), [
            'language_id' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->accountService->updateUserLanguages($user->id, $request->language_id);
        return $this->handleResponse($response);
    }

    public function reportBug(Request $request){
        $v = Validator::make($request->all(), [
            'error' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->accountService->reportBug($request->error);
        return $this->handleResponse($response);
    }

    public function resendActivationMail(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->accountService->resendActivationMail($request->email);
        return $this->handleResponse($response);
    }

    public function UserSubscription($action, $user_id){
        $v = Validator::make(['action' => $action , 'user_id' => $user_id], [
            'action' => 'required|numeric|min:0|max:1',
            'user_id' => 'required|max:100'
        ]);
        if ($v->fails()) {
            return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'?subscription=no');
        }
        $this->accountService->UserSubscription($action, $user_id);
        if($action == 1){
            return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'?subscription=success');
        }
        return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'?subscription=failed');
    }

    public function getMaxChildren(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getMaxChildren($user->id);
        return $this->handleResponse($response);
    }

    public function getCountriesUsersCount(){
        $response = $this->accountService->getCountriesUsersCount();
        return $this->handleResponse($response);
    }

    public function unlockModelAnswer(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->accountService->unlockModelAnswer($user->id,$request->mission_id);
        return $this->handleResponse($response);
    }
    //endregion

    // new payment + new registration
    public function learnerRegister(Request $request){
        if($request->social_id == null){
            $rules = [
                'email' => 'required|email|max:255',
                'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'password' => 'required|min:6|max:24',
            ];
        }
        else {
            $rules = [
                'username' => 'required|min:2|max:255',
                'password' => 'required|min:6|max:24',
                'email' => 'email|max:255',
            ];
        }

        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $userDto->accounttype = 'Individual';
        $userDto->plan = 'free_learner';
        $userDto->role = 'user';

        $response = $this->accountService->AddUser($userDto);
        $response->isPlan = false;
        if(isset($userDto->planname) && !empty($userDto->planname)){
            $response->isPlan = true;
            $response->planname = $userDto->planname;
        }
        return $this->handleResponse($response);
    }

    public function homeSchoolerRegister(Request $request){
        if($request->social_id == null){
            $rules = [
                'email' => 'required|email|max:255',
                'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'password' => 'required|min:6|max:24',
            ];
        }
        else {
            $rules = [
                'username' => 'required|min:2|max:255',
                'password' => 'required|min:6|max:24',
                'email' => 'email|max:255',
            ];
        }

        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $userDto->accounttype = 'Family';
        $userDto->plan = 'free_homeschooler';
        $userDto->role = 'parent';

        $response = $this->accountService->AddUser($userDto);
        $response->isPlan = false;
        if(isset($userDto->planname) && !empty($userDto->planname)){
            $response->isPlan = true;
            $response->planname = $userDto->planname;
        }
        return $this->handleResponse($response);
    }

    public function getCurrentExtras(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getCurrentExtras($user->id);
        return $this->handleResponse($response);
    }

    public function checkIfHaveCreditCard(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->checkIfHaveCreditCard($user->id);
        return $this->handleResponse($response);
    }

    public function getNumberOfChildren(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getNumberOfChildren($user->id);
        return $this->handleResponse($response);
    }

    public function updateUserEmail(Request $request){
        $v = Validator::make($request->all(), [
            'email' => 'required|max:255|email'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->accountService->updateUserEmail($user->id, $request->email);
        return $this->handleResponse($response);
    }

    public function checkStripeSubscription(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->checkStripeSubscription($user->id);
        return $this->handleResponse($response);
    }

    public function getChildrenCount(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getChildrenCount($user->id);
        return $this->handleResponse($response);
    }

    public function purchaseActivity(Request $request){
        $user = JWTAuth::toUser();
        $v = Validator::make($request->all(), [
            'activity_id' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->accountService->purchaseActivity($user->id,$request->activity_id,$request->stripe_token, $request->is_mobile ? false : true);
        return $this->handleResponse($response);
    }

    public function checkUserActivation(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->checkUserActivation($user->id);
        return $this->handleResponse($response);
    }

    public function addUserCoupon(Request $request){
        $v = Validator::make($request->all(), [
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',
            'coupon_id'=>'required',
            'stripe_token'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $userDto = UserDtoMapper::RequestMapper($request);
        $response = $this->accountService->addUserCoupon($userDto,$request->coupon_id);
        return $this->handleResponse($response);
    }

    public function getUserData(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserData($user->id);
        return $this->handleResponse($response);
    }

    public function saveGuestRegisteredScore(Request $request){
        $user = JWTAuth::toUser();
        $v = Validator::make($request->all(), [
            'coins' => 'required|numeric|min:0',
            'xp' => 'required|numeric|min:0'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->accountService->saveGuestRegisteredScore($user->id, $request->coins, $request->xp);
        return $this->handleResponse($response);
    }

    public function registerMobileUser(Request $request){
        $user = JWTAuth::toUser();
        if($request->social_id == null){
            $rules = [
                'email' => 'required|email|max:255',
                'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'password' => 'required|min:6|max:24',
            ];
        }
        else {
            $rules = [
                'username' => 'required|min:2|max:255',
                'password' => 'required|min:6|max:24',
                'email' => 'email|max:255',
            ];
        }

        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $response = $this->accountService->updateMobileUser($user->id, $userDto);
        return $this->handleResponse($response);
    }

    public function getUserNotifications(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserNotifications($user->id);
        return $this->handleResponse($response);
    }

    public function deleteUserNotification($id){
        $user = JWTAuth::toUser();
        $response = $this->accountService->deleteUserNotification($id);
        return $this->handleResponse($response);
    }



//    ===============     Superadmin      ===============

    public function superAdminRegister(Request $request){
        $rules = [
            'email' => 'required|email|max:255',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'required|min:6|max:24',
        ];

        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        //$superAdminUserDto = UserDtoMapper::RegisterSuperAdminMapper($request);
        $superAdminUserDto = UserDtoMapper::RequestMapper($request);
        $superAdminUserDto->plan = "free_learner";
        $superAdminUserDto->role = "super_admin";
        $superAdminUserDto->accounttype = "Super Admin";
        $superAdminUserDto->image_link = null;
        $superAdminUserDto->is_admin = 1;

        $response = $this->accountService->addSuperAdminUser($superAdminUserDto);
        return $this->handleResponse($response);
    }

    public function markNotificationAsRead(request $request){
        $user = JWTAuth::toUser();
        $response = $this->accountService->markNotificationAsRead($user->id,$request->id);
        return $this->handleResponse($response);
    }
}
