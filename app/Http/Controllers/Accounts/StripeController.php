<?php

namespace App\Http\Controllers\Accounts;

use Illuminate\Http\Request;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\ApplicationLayer\Accounts\Dtos\StripeEventDto;
use App\ApplicationLayer\Accounts\CustomMappers\StripeEventDtoMapper;
use App\DomainModelLayer\Accounts\User;
use DB;

class StripeController extends Controller
{

    private $accountService;

    public function __construct(IAccountMainService $accountService){
        $this->accountService = $accountService;
    }

    public function handleStripeTestEvents(Request $request)
    {

        $stripeEventDto = StripeEventDtoMapper::RequestMapper($request);
        $response = $this->accountService->handleWebhookEvent($stripeEventDto);
        return $this->handleResponse($response);
    }

    public function handleStripeLiveEvents(Request $request)
    {
        $stripeEventDto = StripeEventDtoMapper::RequestMapper($request);
        $response = $this->accountService->handleWebhookEvent($stripeEventDto);
        return $this->handleResponse($response);
    }

}
