<?php

namespace App\Http\Controllers\Accounts;

use App\ApplicationLayer\Customers\IUserService;
use Illuminate\Http\Request;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\ApplicationLayer\Customers\UserDto;
use App\Http\Controllers\Controller;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;




class FamilyController extends Controller
{

    private $familyService;

    public function __construct(IAccountMainService $familyService){
        $this->familyService = $familyService;
        $this->middleware('jwt.auth');
        $this->middleware('check-subscription');
        $this->middleware('allow-origin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = JWTAuth::toUser();
        $response = $this->familyService->getParentChildren($user->id);
        return $this->handleResponse($response);
    }

    public function addChild(Request $request)
    {
        $v = Validator::make($request->all(),[
            'fname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'required|min:6|max:24',
            ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $request['accounttype'] = 'Family';
        $request['role'] = 'Child';

        $userDto = UserDtoMapper::RequestMapper($request);
        if($request['accounttype'] == 'Family')
        {
            // $userDto->plan = 'Family_Free';
        }else{
            $userDto->plan = 'Free';    
        }

        $parent = JWTAuth::toUser();
        
        $response = $this->familyService->AddChild($parent,$userDto);
        return $this->handleResponse($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getChildJourneyAndActivities($child_id){
        $user = JWTAuth::toUser();
        $is_mobile = true;
        if(isset($_GET['is_mobile'])){
            $is_mobile = $_GET['is_mobile'];
        }
        $response = $this->familyService->getChildJourneyAndActivities($user->id, $child_id, $is_mobile);
        return $this->handleResponse($response);
    }

    public function getChildProgress($journey_id, $child_id){
        $user = JWTAuth::toUser();
        $response = $this->familyService->getChildProgress($user->id, $journey_id, $child_id);
        return $this->handleResponse($response);
    }

    public function getChildProgressActivity($activity_id,$child_id){
        $user = JWTAuth::toUser();
        $response = $this->familyService->getChildProgressActivity($user->id,$activity_id,$child_id);
        return $this->handleResponse($response);
    }

    public function getChildStatistics($journey_id, $child_id){
        $user = JWTAuth::toUser();
        $response = $this->familyService->getChildStatistics($user->id, $journey_id, $child_id);
        return $this->handleResponse($response);
    }

    public function getChildStatisticsActivity($activity_id,$child_id){
        $user = JWTAuth::toUser();
        $response = $this->familyService->getChildStatisticsActivity($user->id, $activity_id, $child_id);
        return $this->handleResponse($response);
    }

    public function getChildFeeds($journey_id, $child_id){
        $user = JWTAuth::toUser();
        $response = $this->familyService->getChildFeeds($user->id, $journey_id, $child_id);
        return $this->handleResponse($response);
    }
    public function getChildFeedsActivity($activity_id, $child_id){
        $user = JWTAuth::toUser();
        $response = $this->familyService->getChildFeedsActivity($user->id, $activity_id, $child_id);
        return $this->handleResponse($response);
    }

}
