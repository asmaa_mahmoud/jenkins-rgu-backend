<?php

namespace App\Http\Controllers\Accounts;

use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use App\ApplicationLayer\Accounts\Dtos\UserLoginDto;
use App\Helpers\Mapper;
use Illuminate\Http\Request;
use App\ApplicationLayer\Accounts\CustomMappers\UserLoginDtoMapper;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{

    private $accountService;

    public function __construct(IAccountMainService $accountService){
        $this->middleware('allow-origin');
        $this->accountService = $accountService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'gmt_difference' => 'required'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userloginDto = Mapper::MapRequest(UserLoginDto::class, $request->all());
        $response = $this->accountService->Login($userloginDto,$request->gmt_difference);

        $responseobject = new ResponseObject();
        $responseobject->Object = $response;
        $responseobject->status_code = $response->status_code;
        //$headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type";
        return Response::json($responseobject,$responseobject->status_code);
        //return $this->handleResponse($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userActivation($token_url){
        $response = $this->accountService->activate($token_url);
        return $this->handleResponse($response);
    }
}
