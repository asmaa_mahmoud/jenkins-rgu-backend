<?php

namespace App\Http\Controllers\Accounts;

use App\ApplicationLayer\Accounts\Dtos\WorldUserDto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use JWTAuth;
use App\Helpers\Mapper;
use Validator;

class WorldCupController extends Controller
{
    private $accountService;

    public function __construct(IAccountMainService $accountService){
        $this->middleware('jwt.auth', ['except' => ['predictWorldCupAutomatic','getSharedPrediction', 'updateUsersScore', 'getFeaturedMatch', 'getMatchesPerDay', 'getNoSubmissionInMatch', 'getLeaderBoardData']]);
        $this->middleware('allow-origin');
        $this->accountService = $accountService;
    }

    public function predictWorldCupAutomatic(Request $request){
        $response = $this->accountService->predictWorldCupAutomatic($request->size,$request->from);
        return $this->handleResponse($response);
    }
    public function getFeaturedMatch(){
        $response = $this->accountService->getFeaturedMatch();
        return $this->handleResponse($response);
    }

    public function getMatchesPerDay(){
        $response = $this->accountService->getMatchesPerDay();
        return $this->handleResponse($response);
    }

    public function getNoSubmissionInMatch(){
        $response = $this->accountService->getTotalNoSubmission();
        return $this->handleResponse($response);
    }

    public function getLeaderBoardData(){
        $limit = 100;
        if(isset($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        $response = $this->accountService->getLeaderBoardData($limit);
        return $this->handleResponse($response);
    }

    public function countLeaderBoardData(){
        $response = $this->accountService->countLeaderBoardData();
        return $this->handleResponse($response);
    }

    public function getUserInLeaderBoard(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserInLeaderBoard($user->id);
        return $this->handleResponse($response);
    }

    public function getAllParameters(){
        $response = $this->accountService->getAllParameters();
        return $this->handleResponse($response);
    }

    public function getUserPrediction(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getUserPrediction($user->id);
        return $this->handleResponse($response);
    }

    public function getWorldUserData(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getWorldUserData($user->id);
        return $this->handleResponse($response);
    }

    public function updateWorldUserData(Request $request){
        $v = Validator::make($request->all(), [
            'name' => 'required|string',
            'image' => 'required|string',
            'predictor' => 'required|string',
            'address' => 'string',
            'phone' => 'numeric',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $worldUserDto = Mapper::MapRequest(WorldUserDto::class, $request->all());
        $response = $this->accountService->updateWorldUserData($user->id, $worldUserDto);
        return $this->handleResponse($response);
    }

    public function updateUsersScore(){
        $response = $this->accountService->updateUsersScore();
        return $this->handleResponse($response);
    }

    public function predictWorldUser(Request $request){
        $v = Validator::make($request->all(), [
            'parameters' => 'required|array',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->accountService->predictWorldUser($user->id, $request->parameters);
        return $this->handleResponse($response);
    }

    public function getSharedPrediction(){
        $response = $this->accountService->getSharedPredictionData();
        return $this->handleResponse($response);
    }

    public function getUserSharedPredictionData(){
        $user = JWTAuth::toUser();
        $response = $this->accountService->getSharedPredictionData($user->id);
        return $this->handleResponse($response);
    }


}
