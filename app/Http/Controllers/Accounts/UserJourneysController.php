<?php

namespace App\Http\Controllers;

use App\ApplicationLayer\UserJourneys\IUserJourneyService;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\ResponseObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UserJourneysController extends Controller
{
    private $userJourneyService;

    public function __construct(IUserJourneyService $userJourneyService){
        $this->userJourneyService = $userJourneyService;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function getUserJourneys($userId){
        $response = $this->userJourneyService->GetUserJourneys($userId);
        return $this->handleResponse($response);
    }
}
