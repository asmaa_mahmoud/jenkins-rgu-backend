<?php

namespace App\Http\Controllers;

use App\ApplicationLayer\Customers\IUserService;
use Illuminate\Http\Request;
use App\Helpers\ResponseObject;
use Illuminate\Support\Facades\Response;
use App\ApplicationLayer\Customers\UserDto;
use App\ApplicationLayer\Customers\UserDtoMapper;

class StudentController extends Controller
{

    private $studentService;

    public function __construct(IUserService $studentService){
        $this->middleware('allow-origin');
        $this->studentService = $studentService;
        //$this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $studentDto = UserDtoMapper::RequestMapper($request);
        $response = $this->studentService->addStudent($studentDto);
        return $this->handleResponse($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
