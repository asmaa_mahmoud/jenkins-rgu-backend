<?php

namespace App\Http\Controllers\Professional;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use App\Helpers\RobogardenToolProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ColorPaletteController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }


    public function getAllColorPalettes(){
        $response = $this->professionalService->getAllColorPalettes();
        return $this->handleResponse($response);
    }


}