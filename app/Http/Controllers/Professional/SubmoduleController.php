<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use BigBlueButton\BigBlueButton;

class SubmoduleController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }
    public function getNextTaskInfo(Request $request){
        $v = Validator::make($request->all(), [
            'round_id'=>'required|integer|min:0',
            'activity_id'=>'required' ,
            'task_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getNextTaskInfo($request->activity_id ,$request->sub_module_id, $request->task_id,$request->round_id, $user->id,$request->is_default);
        return $this->handleResponse($response);

    }
}    