<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use Storage;
use Config;
use Carbon\Carbon;

class BBBController extends Controller
{
    private $professionalService;

    const Attendee_Password = '12345';
    const Moderator_Password = '54321';

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }


    public function testBBB(Request $request){

        $bbb                 = new BigBlueButton();
        $createMeetingParams = new \BigBlueButton\Parameters\CreateMeetingParameters('bbb-meeting-uid-65', 'BigBlueButton API Meeting');
        $createMeetingParams->addPresentation('https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf');
        $createMeetingParams->setAttendeePassword($this::Attendee_Password);
        $createMeetingParams->setModeratorPassword($this::Moderator_Password);
        $response            = $bbb->createMeeting($createMeetingParams);
        $name = 'demo';
        $joinMeetingParams = new JoinMeetingParameters('bbb-meeting-uid-65', $name, $this::Moderator_Password);
        $joinMeetingParams->setRedirect(true);
        $url = $bbb->getJoinMeetingURL($joinMeetingParams);

        $createMeetingParams2 = new \BigBlueButton\Parameters\CreateMeetingParameters('bbb-meeting-uid-66', 'BigBlueButton API Meeting2');
        $createMeetingParams2->addPresentation('https://en.unesco.org/inclusivepolicylab/sites/default/files/dummy-pdf_2.pdf');
        $createMeetingParams2->setAttendeePassword($this::Attendee_Password);
        $createMeetingParams2->setModeratorPassword($this::Moderator_Password);
        $response2            = $bbb->createMeeting($createMeetingParams2);
        $name = 'demo';
        $joinMeetingParams2 = new JoinMeetingParameters('bbb-meeting-uid-66', $name, $this::Moderator_Password);
        $joinMeetingParams2->setRedirect(true);
        $url2 = $bbb->getJoinMeetingURL($joinMeetingParams2);


        return [$url,$url2];

    }

    public function create(Request $request) {
        $bbb = new BigBlueButton();

        $createMeetingParams = new CreateMeetingParameters($request->meetingID, $request->meetingName);
        $createMeetingParams->setAttendeePassword($this::Attendee_Password);
        $createMeetingParams->setModeratorPassword($this::Moderator_Password);
        // $createMeetingParams->setDuration($duration);
        // $createMeetingParams->setLogoutUrl($urlLogout);
        $isRecordingTrue = true;
         if ($isRecordingTrue) {
             $createMeetingParams->setRecord(true);
             $createMeetingParams->setAllowStartStopRecording(true);
             $createMeetingParams->setAutoStartRecording(true);
         }

        $response = $bbb->createMeeting($createMeetingParams);

        if ($response->getReturnCode() == 'FAILED') {
            return $this->handleResponse('Can\'t create room! please contact our administrator.');
        } else {
            // process after room created
            $res = "Created Meeting with ID: " . $response->getInternalMeetingId();
            return $this->handleResponse($res);
        }
    }

    public function getRooms()
    {
        $bbb = new BigBlueButton();
        $response = $bbb->getMeetings();
        $meetings = [];
        dd($response);
        if ($response->getReturnCode() == 'SUCCESS') {
            foreach ($response->getRawXml()->meetings->meeting as $meeting) {
                // process all meeting
                $meetings []= $meeting;
            }
        }
        return $this->handleResponse($meetings);
    }

    public function join($room_id) {
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->join($user_id, $room_id);
        return $this->handleResponse($response);
    }

    public function start(Request $request ,$room_id) {

        $ThereAreFile = false;
        $file_s3_url = '';
//        $v = Validator::make(
//            ['file'=> $request->file],['file' => 'max:10480']
//        );
//
//        if ($v->fails()) {
//            return $this->handleValidationError($v->errors()->all());
//        }else {
//            $ThereAreNoFile = true;
//        }

        $v = Validator::make($request->all(), [
            'file' => 'max:10480',
            'file_id' => 'integer',
            'file_uploaded' => 'required|boolean',
            'file_from_list' => 'required|boolean'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $file_uploading_flag = $request->file_uploaded;
        $file_from_list_flag = $request->file_from_list;
        $file_s3_url = '';
        $file_id = null;
        $file_name_ = '';
        if($request->file_id) {
            $file_id = $request->file_id;
        }

        if($request->file) {
            $file = $request->file;
            $file_name_ = $file->getClientOriginalName();
            $file_name =  Carbon::now()->timestamp . '_' . $file->getClientOriginalName();
            Config::set('filesystems.disks.s3.bucket', 'robogarden-professional');
            Config::set('filesystems.disks.s3.region', 'us-west-2');
            Storage::disk('s3')->put('presentations'.'/'.$file_name, file_get_contents($file), 'public');
            $file_s3_url = "https://s3-us-west-2.amazonaws.com/robogarden-professional/presentations/" .urlencode($file_name);
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->start($user_id, $room_id, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name_,true);
        $this->professionalService->end($user_id, $room_id);
        $response = $this->professionalService->start($user_id, $room_id, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name_,false);
        return $this->handleResponse($response);
    }

    public function end($room_id) {
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->end($user_id, $room_id);
        return $this->handleResponse($response);

    }

    public function getRecords($room_id) {
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $recordingParams = new GetRecordingsParameters();
        $recordingParams->setMeetingId($room_id);

        $bbb = new BigBlueButton();
        $response = $bbb->getRecordings($recordingParams);
        $records = [];
        if ($response->getReturnCode() == 'SUCCESS') {
            foreach ($response->getRecords() as $record){
                $records []= $record->getPlaybackUrl();
            }
//            foreach ($response->getRawXml()->recordings->recording as $recording) {
//                    // process all recording
//                    $records []= $recording->playback->format->url;
//                }
        }
       return $this->handleResponse($records);
    }



    public function joined($room_id) {
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->joined($user_id, $room_id);
        return $this->handleResponse($response);
    }
}
