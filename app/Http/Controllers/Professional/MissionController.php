<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class MissionController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth',['except'=>['getPracticeMission']]);
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }

    public function getHtmlBlocklyCategories(){
        $response = $this->professionalService->getHtmlBlocklyCategories();
        return $this->handleResponse($response);
    }

    public function updateActivityMissionProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'learning_path_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'success' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $user = JWTAuth::toUser();

        $response = $this->professionalService->updateActivityMissionProgress($request->mission_id, $user->id, $request->learning_path_id, $request->activity_id, $request->success, $request->noOfBlocks, $request->timeTaken, $request->xmlCode, $request->is_default);
        return $this->handleResponse($response);
    }

    public function updateActivityHTMLMissionProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'learning_path_id' => 'required|integer|min:0',
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->updateActivityHTMLMissionProgress($request->mission_id,$user->id,$request->learning_path_id,$request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function submitActivityMcqMission(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'learning_path_id' => 'required|integer|min:0',
            'is_default' => 'required'
        ]);

        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitActivityMcqMission($request->data,$request->selected_ids,$request->use_model_answer,$request->mission_id,$user->id,$request->learning_path_id,$request->activity_id,$request->noOfBlocks,$request->timeTaken,$request->is_default);
        return $this->handleResponse($response);
    }

    public function submitActivityQuiz(Request $request)
    {
        $v = Validator::make($request->all(), [
            'quiz_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'learning_path_id' => 'required|integer|min:0',
            'is_default' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitActivityQuiz($request->data, $request->quiz_id, $user->id, $request->learning_path_id, $request->activity_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function updateActivityMissionCodingProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'learning_path_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'success' => 'required'
        ]);

        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $user = JWTAuth::toUser();

        $response = $this->professionalService->updateActivityMissionCodingProgress($request->mission_id, $user->id, $request->learning_path_id, $request->activity_id, $request->success, $request->timeTaken, $request->is_default);
        return $this->handleResponse($response);
    }

    public function updateActivityMissionEditorProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'learning_path_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'success' => 'required'
        ]);

        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->professionalService->updateActivityMissionEditorProgress($request->mission_id, $user->id, $request->learning_path_id, $request->activity_id, $request->success, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getPracticeMission(Request $request){

        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'type' => 'required',
            
        ]);
          if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

         $response = $this->professionalService->getPracticeMission($request->mission_id, $request->type);
        return $this->handleResponse($response);

    }
}
