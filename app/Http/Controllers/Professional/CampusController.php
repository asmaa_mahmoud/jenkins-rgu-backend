<?php 

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Accounts\CustomMappers\SupportEmailDtoMapper;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Storage;
use Config;
use Carbon\Carbon;
use App\Framework\Exceptions\BadRequestException;

class CampusController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){

        $this->middleware('jwt.auth',['except'=>['createSubModules']]);
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;

    }

    public function unlockStudentRound($round_id,Request $request){
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->unlockStudentRound($user_id,$round_id);
        return $this->handleResponse($response);

    }

    public function setFirstLoginTime(Request $request){
        $v = Validator::make($request->all(), [
           'gmt_difference' => 'required'
       ]);

        if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->setFirstLoginTime($user_id,$request->gmt_difference);
        return $this->handleResponse($response);

    }

    public function getStudentRunningRounds(Request $request){
       $v = Validator::make($request->all(), [
           'gmt_difference' => 'required'
       ]);

       if ($v->fails()) {
           return $this->handleValidationError($v->errors()->all());
       }
      $user = JWTAuth::toUser();
        $user_id=$user->id;
        //dd($request->count);
        $count=isset($request->count)?$request->count:null;
        //dd($count);

        $response = $this->professionalService->getStudentRunningRounds($user_id,$request->gmt_difference,$count);
        return $this->handleResponse($response);
   }

   public function getStudentRoundClasses(Request $request){
     $v = Validator::make($request->all(), [

            'round_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

     $user = JWTAuth::toUser();
        $user_id=$user->id;

         $response = $this->professionalService->getStudentRoundClasses($user_id,$request->round_id,$request->is_default);
        return $this->handleResponse($response);

   }

   public function getStudentRoundClassesData(Request $request,$round_id){
     $v = Validator::make($request->all(), [
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

     $user = JWTAuth::toUser();
        $user_id=$user->id;

         $response = $this->professionalService->getStudentRoundClassesData($user_id,$request->round_id,$request->is_default);
        return $this->handleResponse($response);

   }
    public function getStudentClassTasks(Request $request,$round_id, $class_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required',
            'gmt_difference'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getStudentClassTasks($user_id,$round_id, $class_id, $request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getClassTasks(Request $request,$round_id, $class_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getClassTasks($user_id,$round_id, $class_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getWelcomeTasks(Request $request,$round_id, $class_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getWelcomeTasks($user_id,$round_id, $class_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function updateRoundActivityMissionProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'success' => 'required',
            'gmt_difference'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $user = JWTAuth::toUser();

        $response = $this->professionalService->updateRoundActivityMissionProgress($request->mission_id, $user->id, $request->round_id, $request->activity_id, $request->success, $request->noOfBlocks, $request->timeTaken, $request->xmlCode, $request->userCode,$request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateRoundActivityHTMLMissionProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->updateRoundActivityHTMLMissionProgress($request->mission_id,$user->id,$request->round_id,$request->activity_id,$request->user_code,$request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateRoundActivityMissionHandoutProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->updateRoundActivityMissionHandoutProgress($request->mission_id,$user->id,$request->round_id,$request->activity_id,$request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateRoundActivityMiniProjectProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'project_url' => 'required',
            'gmt_difference'=>'required',
        ]);
        $duration = null;
        if(isset($request->duration)){
            $duration = $request->duration;
        }

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->updateRoundActivityMiniProjectProgress($request->mission_id,$user->id,$request->round_id,$request->activity_id,$request->project_url,$request->is_default,$duration,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function submitRoundActivityMcqMission(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference'=>'required',
        ]);

        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        if($request->noOfBlocks != null){
            $v = Validator::make($request->all(), [
                'noOfBlocks' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        if($request->timeTaken != null){
            $v = Validator::make($request->all(), [
                'timeTaken' => 'integer|min:0'
            ]);

            if ($v->fails()) {
                return $this->handleValidationError($v->errors()->all());
            }
        }
        $practice=false;
        $activity_id=null;
        $round_id=null;
        if($request->practice != null){
            $practice=$request->practice;
            if($practice==false||trim($practice)=="false"){
                $practice=false;
                $v = Validator::make($request->all(), [

                    'activity_id' => 'required|integer|min:0',
                    'round_id' => 'required|integer|min:0',
                        ]);

                if($v->fails()) {
                    return $this->handleValidationError($v->errors()->all());
                }
                $activity_id=$request->activity_id;
                $round_id=$request->round_id;
            }
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitRoundActivityMcqMission($request->data,$request->selected_ids,$request->use_model_answer,$request->mission_id,$user->id,$round_id,$activity_id,$request->noOfBlocks,$request->timeTaken,$practice,$request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function submitRoundActivityQuiz(Request $request)
    {
        $v = Validator::make($request->all(), [
            'quiz_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'duration' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference' => 'required|integer',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitRoundActivityQuiz($request->data, $request->quiz_id, $user->id, $request->round_id, $request->activity_id,$request->duration,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);
    }

    public function submitRoundActivityMiniProjectQuiz(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'duration' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference' => 'required|integer',
            'questionnaire' => 'required|array'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitRoundActivityMiniProjectQuiz($request->questionnaire, $request->mission_id, $user->id, $request->round_id, $request->activity_id,$request->duration,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);
    }    

    public function updateRoundActivityMissionCodingProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'userCode' => 'required',
            'success' => 'required',
            'gmt_difference' => 'required|integer',
            'timeTaken' => 'required|integer|min:0'
        ]);

        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
                                                                                                                    //($mission_id,$user_id,$round_id,$activity_id,$timeTa)
        $response = $this->professionalService->updateRoundActivityMissionCodingProgress($request->mission_id, $user->id, $request->round_id, $request->activity_id, $request->success, $request->timeTaken,$request->userCode, $request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateRoundActivityMissionEditorProgress(Request $request){
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'userCode' => 'required',
//            'duration' => 'required',
            'gmt_difference' => 'required|integer',
        ]);

        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->professionalService->updateRoundActivityMissionEditorProgress($request->mission_id, $user->id, $request->round_id, $request->activity_id,$request->duration,$request->userCode,$request->data, $request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateRoundActivityMissionAngularProgress(Request $request){

        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'userCode' => 'required',
            'gmt_difference' => 'required|integer',
        ]);


        if($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        

        $response = $this->professionalService->updateRoundActivityMissionAngularProgress($request->mission_id, $user->id, $request->round_id, $request->activity_id,$request->duration,$request->userCode,$request->data, $request->is_default,$request->gmt_difference);
        return $this->handleResponse($response);
    }


    public function getAllCampuses(){
        $user = JWTAuth::toUser();

        $response = $this->professionalService->getAllCampuses($user->id);
        return $this->handleResponse($response);
    }

    public function getSupportCampuses(Request $request){
       // dd("yous");
        $user=JWTAuth::toUser();

        $response = $this->professionalService->getSupportCampuses($user->id,$request->gmt_difference);
        return $this->handleResponse($response);

    }


     public function searchForCountCampuses(Request $request){

        $v = Validator::make($request->all(), [
            'search' => 'required',
            'gmt_difference' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $start_from =null;
        $limit =null;

        $response = $this->professionalService->getProfessionalSearch($request->search,$start_from,$limit,$user_id,$request->gmt_difference,$count=true);

        return $this->handleResponse($response);
    }


    public function searchForCountClasses(Request $request){
        $v = Validator::make($request->all(), [
            'search' => 'required',
            'gmt_difference' => 'required'

        ]);
  
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $start_from =null;
        $limit =null;

        $response = $this->professionalService->getlessonsSearch($request->search,$start_from,$limit,$user_id,$request->gmt_difference,$count=true);

        return $this->handleResponse($response);
    }

    public function getAllClasses(){
        $user = JWTAuth::toUser();

        $response = $this->professionalService->getAllClasses($user->id);
        return $this->handleResponse($response);
    }
    public function getAllTasks(){
        $user = JWTAuth::toUser();

        $response = $this->professionalService->getAllTasks($user->id);
        return $this->handleResponse($response);
    }

    public function getCampusClasses($campus_id){
        $user = JWTAuth::toUser();

        $response = $this->professionalService->getCampusClasses($user->id, $campus_id);
        return $this->handleResponse($response);
    }
    public function sendProfessionalSupportEmail(Request $request){
       
        $v = Validator::make($request->all(), [
            'message' => 'required',
            //'name'=>'required',
            'email'=>'required',
            
        ]);
        

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $supportEmailDto = SupportEmailDtoMapper::RequestMapperProfessionalSupport($request);
        $response = $this->professionalService->sendProfessionalSupportEmail($user->id,$supportEmailDto);
        return $this->handleResponse($response);
    }
    public function getCampusClassesData($campus_id){

        $user = JWTAuth::toUser();
        $response = $this->professionalService->getCampusClassesData($user->id,$campus_id);
        return $this->handleResponse($response);

    }
    public function getCampusClassTasks(Request $request,$class_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->getCampusClassTasks($user->id,$class_id,$request->is_default);
        return $this->handleResponse($response);

    }

    public function getRoundClassTasks(Request $request,$round_id,$class_id){

        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->getRoundClassTasks($user->id,$round_id,$class_id,$request->is_default);
        return $this->handleResponse($response);

    }

    public function getCampusClassId(Request $request,$campus_id,$activity_id){

        $v = Validator::make($request->all(), [
            'is_default' => 'required',
            'gmt_difference'=>'required'

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->getCampusClassId($user->id,$campus_id,$activity_id,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);

    }







    public  function updateRoundClassTasks(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
            'tasks' => 'required',
            'gmt_difference'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->updateRoundClassTasks($user_id, $request->round_id,$request->class_id,$request->tasks,$request->gmt_difference);
        return $this->handleResponse($response);

    }

    public function createRoom(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' =>'required|integer|min:0',
            'starts_at' => 'required|integer|min:0',
            'name' => 'required|string',
            'file' => 'max:10480',
            'file_id' => 'integer',
            'gmt_difference'=>'required',
            'file_uploaded' => 'required|boolean',
            'file_from_list' => 'required|boolean'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $file_uploading_flag = $request->file_uploaded;
        $file_from_list_flag = $request->file_from_list;
        $file_s3_url = '';
        $file_name = '';
        $file_name_ = '';
        $file_id = null;
        if($request->file_id) {
            $file_id = $request->file_id;
        }
        if($request->file) {
            $file = $request->file;
//            if(preg_match_all("/[^a-z0-9\_\-\s\.]/i",$file->getClientOriginalName())){
//                throw new BadRequestException(trans('locale.file_name_should_not_has_special_chars'));
//            }
            $file_name_ = $file->getClientOriginalName();
            $file_name =  Carbon::now()->timestamp . '_' . $file->getClientOriginalName();
            Config::set('filesystems.disks.s3.bucket', 'robogarden-professional');
            Config::set('filesystems.disks.s3.region', 'us-west-2');
            Storage::disk('s3')->put('presentations'.'/'.$file_name, file_get_contents($file), 'public');
            $file_s3_url = "https://s3-us-west-2.amazonaws.com/robogarden-professional/presentations/" .urlencode($file_name);
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->createRoom($user_id, $request->round_id,$request->class_id, $request->starts_at, $request->name,$request->gmt_difference, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name_);
        return $this->handleResponse($response);
    }

    public function updateRoom(Request $request){
        $v = Validator::make($request->all(), [
            'room_id' => 'required|integer|min:0',
            'starts_at' => 'integer|min:0',
            'name' => 'string',
            'file' => 'max:10480',
            'file_id' => 'integer',
            'file_uploaded' => 'required|boolean',
            'file_from_list' => 'required|boolean'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $file_uploading_flag = $request->file_uploaded;
        $file_from_list_flag = $request->file_from_list;
        $file_s3_url = '';
        $file_id = null;
        $file_name = '';
        $file_name_ = '';
        if($request->file_id) {
            $file_id = $request->file_id;
        }
        if($request->file) {
            $file = $request->file;
//            if(preg_match_all("/[^a-z0-9\_\-\s\.]/i",$file->getClientOriginalName())){
//                throw new BadRequestException(trans('locale.file_name_should_not_has_special_chars'));
//            }
            $file_name_ = $file->getClientOriginalName();
            $file_name =  Carbon::now()->timestamp . '_' . $file->getClientOriginalName();
            Config::set('filesystems.disks.s3.bucket', 'robogarden-professional');
            Config::set('filesystems.disks.s3.region', 'us-west-2');
            Storage::disk('s3')->put('presentations'.'/'.$file_name, file_get_contents($file), 'public');
            $file_s3_url = "https://s3-us-west-2.amazonaws.com/robogarden-professional/presentations/" .urlencode($file_name);
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->updateRoom($user_id, $request->room_id,$request->starts_at, $request->name, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name_);
        return $this->handleResponse($response);
    }
    public function getMaterialsClass(Request $request) {
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->professionalService->getMaterialsClass($request->round_id,$request->class_id);
        return $this->handleResponse($response);
    }

    public function getStudentProgress(Request $request,$round_id){
        $v = Validator::make($request->all(), [
            'task_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
            'is_default' => 'required',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->getStudentProgress($user_id,$round_id,$request->class_id,$request->task_id,$request->is_default);
        return $this->handleResponse($response);

    }

    public function getCampusInfo($round_id){
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->getCampusInfo($user_id,$round_id);
        return $this->handleResponse($response);

    }

    public function getCampusObjectives($round_id){
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->getCampusObjectives($user_id,$round_id);
        return $this->handleResponse($response);

    }

    public function updateDayTipStatus(Request $request){
        $v = Validator::make($request->all(), [
            'show'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->updateDayTipStatus($user_id,$request->show);
        return $this->handleResponse($response);

    }

    public function updateStudentcurrentStep($round_id,Request $request){
        $v = Validator::make($request->all(), [
            'task_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference'=>'required',
            'current_step' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->updateStudentCurrentStep($user_id,$round_id,$request->activity_id,$request->task_id,$request->current_step,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);

    }


    public function updateStudentCurrentSection($round_id,Request $request){
        $v = Validator::make($request->all(), [
            'task_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference'=>'required',
            'current_section' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->updateStudentCurrentSection($user_id,$round_id,$request->activity_id,$request->task_id,$request->current_section,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);

    }


    public function updateStudentLastSeen($round_id,Request $request){
        $v = Validator::make($request->all(), [
            'task_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'is_default' => 'required',
            'gmt_difference'=>'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->updateStudentLastSeen($user_id,$round_id,$request->activity_id,$request->task_id,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);

    }

    public function calculateCourseLessonsTime(Request $request){
        $campus_ids=$request->campus_ids;
        $this->professionalService->calculateCourseLessonsTime($campus_ids);
        return "Course Lessons times have been estimated";
    }

    public function calculateScore(Request $request){
        $campus_ids=$request->campus_ids;
        $this->professionalService->calculateScore($campus_ids);
        return "Scores have been calculated";
    }


    public function getlessonsSearch(Request $request){

        $v = Validator::make($request->all(), [
            'search' => 'required',
            'gmt_difference' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getlessonsSearch($request->search,$start_from,$limit,$user_id,$request->gmt_difference,$count=false);
        return $this->handleResponse($response);


    }



    public function getProfessionalSearch(Request $request){

        $v = Validator::make($request->all(), [
            'search' => 'required',
            'gmt_difference' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getProfessionalSearch($request->search,$start_from,$limit,$user_id,$request->gmt_difference,$count=false);
        return $this->handleResponse($response);


    }


    public function searchForTasks(Request $request){

        $v = Validator::make($request->all(), [
            'search' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        
        //Get User
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        //dd($user);
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        $response = $this->professionalService->searchForTasks($request->search,$start_from,$limit,$user_id,$request->gmt_difference,false);
        return $this->handleResponse($response);

    }


    public function searchForCountTasks(Request $request){

        $v = Validator::make($request->all(), [
            'keyword' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        //Get User
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->searchForTasks($request->keyword,$start_from,$limit,$user_id,$request->gmt_difference,true);
        return $this->handleResponse($response);
    }


    public function searchForAll(Request $request){

        $v = Validator::make($request->all(), [
            'search' => 'required',
            'gmt_difference' => 'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $count=$request->count==null?false:filter_var($request->count, FILTER_VALIDATE_BOOLEAN);
       
        $response = $this->professionalService->searchForAll($request->search,$start_from,$limit,$user_id,$request->gmt_difference,$count);
        return $this->handleResponse($response);
    }
    
    
    public function saveWatchedVideoFlag($round_id){
        
        $user = JWTAuth::toUser();
        $user_id=$user->id;
    
        $response = $this->professionalService->saveWatchedVideoFlag($round_id,$user_id);
        return $this->handleResponse($response);
    }

    public function getIntroDetails($round_id){
        
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->getIntroDetails($round_id);
        return $this->handleResponse($response);
    }

    public function getCampusDictionary($campus_id){
        $response = $this->professionalService->getCampusDictionary($campus_id);
        return $this->handleResponse($response);
    }

    public function getStudentStreaks(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required',
            'round_id' => 'required',
        ]);
 
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->getStudentStreaks($user,$request->round_id,$request->gmt_difference);
        return $this->handleResponse($response);

    }

    public function getLearnerProgress(Request $request){
        $user = JWTAuth::toUser();
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $response = $this->professionalService->getLearnerProgress($user,$request->student_id,$request->round_id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getCourseDetails(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getCourseDetails($user,$request->campus_id,$start_from, $limit, $search,$order_by,$order_by_type);
        return $this->handleResponse($response);
    }

    public function getCourseDetailsCount(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getCourseDetails($user,$request->campus_id,$start_from, $limit, $search,$order_by,$order_by_type,true);
        return $this->handleResponse($response);
    }

    public function getCourseLearners(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getCourseLearners($user,$request->campus_id,$request->module_id,$start_from, $limit, $search,$order_by,$order_by_type);
        return $this->handleResponse($response);
    }

    public function getCourseLearnersCount(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getCourseLearners($user,$request->campus_id,$request->module_id,$start_from, $limit, $search,$order_by,$order_by_type,true);
        return $this->handleResponse($response);
    }

    public function getCourseModuleDetails(Request $request){
       $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getCourseModuleDetails($user,$request->campus_id,$request->module_id,$start_from, $limit, $search,$order_by,$order_by_type);
        return $this->handleResponse($response);
    }

    public function getCourseModuleDetailsCount(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getCourseModuleDetails($user,$request->campus_id,$request->module_id,$start_from, $limit, $search,$order_by,$order_by_type,true);
        return $this->handleResponse($response);
    }


    public function getModuleMissionLearners(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getModuleMissionLearners($user,$request->campus_id,$request->module_id,$request->task_id,$start_from, $limit, $search,$order_by,$order_by_type);
        return $this->handleResponse($response);
    }

    public function getModuleMissionLearnersCount(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getModuleMissionLearners($user,$request->campus_id,$request->module_id,$request->task_id,$start_from, $limit, $search,$order_by,$order_by_type,true);
        return $this->handleResponse($response);
    }

    public function getRoundLearnersProgress(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getRoundLearnersProgress($user,$request->round_id,$start_from, $limit, $search,$order_by,$order_by_type,false);
        return $this->handleResponse($response);
    }

    
    public function getRoundLearnersProgressCount(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }
        $response = $this->professionalService->getRoundLearnersProgress($user,$request->round_id,$start_from, $limit, $search,$order_by,$order_by_type,true);
        return $this->handleResponse($response);
    }

    public function updateHideBlocks(request $request){
        $v = Validator::make($request->all(), [
            'learner_id' => 'required',
            'round_id' => 'required'
        ]);
 
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->updateHideBlocks($user,$request->learner_id,$request->round_id);
        return $this->handleResponse($response);
    }

    public function bulkUpdateHideBlocks(request $request){
        $v = Validator::make($request->all(), [
            'learners' => 'required',
            'round_id' => 'required'
        ]);
 
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->bulkUpdateHideBlocks($user,$request->learners,$request->round_id);
        return $this->handleResponse($response);
    } 

    public function getStudentCampusStatusData(request $request){
        
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->getStudentCampusStatusData($user,$request->round_id);
        return $this->handleResponse($response);
    }

    public function getStudentCampusScoreStatistics(request $request){

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->getStudentCampusScoreStatistics($user,$request->round_id);
        return $this->handleResponse($response);
    }

    public function getAllStudentCampusStatusData(request $request){
        
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        
        $response = $this->professionalService->getAllStudentCampusStatusData($user);
        return $this->handleResponse($response);
    }

    public function cleanDropdown(request $request){
        $user = JWTAuth::toUser();

                
        $response = $this->professionalService->cleanDropdown($user,$request->delete);
        return $this->handleResponse($response);
    }

    public function progressFix(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->progressFix($user,$request->fix);
        return $this->handleResponse($response);
    }

    public function lastSeenFix(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->lastSeenFix($user,$request->fix);
        return $this->handleResponse($response);
    }

    public function pythonFix(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->pythonFix($user,$request->fix);
        return $this->handleResponse($response);
    }

    public function checkEditorMissionBug(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->checkEditorMissionBug($user,$request->fix);
        return $this->handleResponse($response);
    }

    public function evalFix(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->evalFix($user,$request->fix);
        return $this->handleResponse($response);
    }

    public function createSearchVault(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->createSearchVault($request->course_id);
        return $this->handleResponse($response); 
    }

    public function createSearchCoursesVault(request $request){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->createSearchCoursesVault();
        return $this->handleResponse($response); 
    }
    public function getModuleSubModules($round_id,$module_id){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getModuleSubModules($user->id,$round_id,$module_id);
        return $this->handleResponse($response); 

    }
    public function getSubModuleTasks($round_id,$module_id,$sub_module_id){
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getSubModuleTasks($user->id,$round_id,$module_id,$sub_module_id);
        return $this->handleResponse($response);
        
    }
    public function studentGiveUpMission(Request $request,$round_id,$module_id,$sub_module_id){
        $v = Validator::make($request->all(), [
            'mission_type' => 'required|in:blockly,quiz,html_mission,mini_project,handout,coding_mission,editor_mission,angular_mission',
            'task_id'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->studentGiveUpMission($user->id,$round_id,$module_id,$sub_module_id,$request->mission_type,$request->task_id);
        return $this->handleResponse($response);
        
    }

    public function studentGiveUpMissionGeneral(Request $request,$round_id,$module_id){
        $v = Validator::make($request->all(), [
            'mission_type' => 'required|in:blockly,quiz,html_mission,mini_project,handout,coding_mission,editor_mission,angular_mission',
            'task_id'=>'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->studentGiveUpMissionGeneral($user->id,$round_id,$module_id,$request->mission_type,$request->task_id);
        return $this->handleResponse($response);
        
    }

    public function createSubModules($campus_id){
        $response = $this->professionalService->createSubModules($campus_id);
        return $this->handleResponse($response);
    }
}
