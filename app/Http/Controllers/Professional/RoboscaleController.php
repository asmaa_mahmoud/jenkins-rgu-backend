<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\DomainModelLayer\Professional\AnalysisAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;


use App\Helpers\Mapper;
use JWTAuth;


class RoboscaleController extends Controller
{

	private $professionalService;
    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth',['except' =>['getAllQuestions','getCategoryQuestions','getAllCategories','getQuestion','getAnswer']]);
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }

    public function getQuestion(Request $request,$question_id){
        //$user = JWTAuth::toUser();
        //$user_id=$user->id;
        //$response = $this->professionalService->getQuestion($user_id,$question_id);
        $response = $this->professionalService->getQuestion($question_id);
        return $this->handleResponse($response);
    }

    public function getAllQuestions(){
        //$user = JWTAuth::toUser();
        //$user_id=$user->id;
        //$response = $this->professionalService->getAllQuestions($user_id);
        $response = $this->professionalService->getAllQuestions();
        return $this->handleResponse($response);
    }

    public function getAnswer(Request $request,$answer_id){
        //$user = JWTAuth::toUser();
        //$user_id=$user->id;
        //$response = $this->professionalService->getAnswer($user_id,$answer_id);
        $response = $this->professionalService->getAnswer($answer_id);
        //return $response;

        return $this->handleResponse($response);

    }

    public function getAllAnswers(){
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $response = $this->professionalService->getAllAnswers($user_id);
        return $this->handleResponse($response);
    	
    }

    public function submit(Request $request, $categoryID){
        $user = JWTAuth::toUser();

        $userAnswers = json_decode($request->answers);
        //dd($request->answers,$userAnswers);
        if(isset($request->category_code)){
            $categoryCode = $request->category_code;
        }else{
            $categoryCode = $categoryID;
        }
        $visualProfile = json_decode($request->visual_profile);

        if($user->id == 96677)
            return $this->handleResponse(['result' => '', 'path' => '']);

//        dd($user->id);

        $result=$this->professionalService->submit($user->id,$userAnswers, $categoryCode, $visualProfile, $categoryID);

        return $this->handleResponse($result);
    }

    public function deleteCategoryPostSubmissions(Request $request, $category_id){
        $user = JWTAuth::toUser();



        $result=$this->professionalService->deleteCategoryPostSubmissions($user->id,$category_id);

        return $this->handleResponse($result);
    }

    public  function getCategoryQuestions($category_id){
        //$user = JWTAuth::toUser();
        //$user_id=$user->id;
        //$response = $this->professionalService->getCategoryQuestions($user_id,$category_id);
        $response = $this->professionalService->getCategoryQuestions($category_id);
        return $this->handleResponse($response);

    }

    public function getAllCategories(){
       // $user = JWTAuth::toUser();
       // $user_id=$user->id;
       // $response = $this->professionalService->getAllCategories($user_id);
        $response = $this->professionalService->getAllCategories();
        return $this->handleResponse($response);
    }

    public function getUserResult(){
        $user = JWTAuth::toUser();

        $response=$this->professionalService->getUserSubmission($user->id);
        return $this->handleResponse($response);
    }

    public function getCategorySubmission($category_code){
        $user = JWTAuth::toUser();

        $response=$this->professionalService->getCategorySubmission($user->id,$category_code);
        return $this->handleResponse($response);
    }

    public function getSectionSubmissions($category_id){
        $user = JWTAuth::toUser();

        $response=$this->professionalService->getSectionSubmissions($user->id,$category_id);
        return $this->handleResponse($response);
    }

}
