<?php

namespace App\Http\Controllers\Professional;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use App\Helpers\LTI;
use App\Helpers\LTIMapper;
use App\Helpers\ResponseObject;
use App\Helpers\RobogardenToolProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use IMSGlobal\LTI\ToolProvider\DataConnector;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Config;
use GuzzleHttp;
use App\Helpers\HttpMethods;

class LTIController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService)
    {
        //$this->middleware('jwt.auth', ['except' => ['routeLtiCourse']]);
        $this->middleware('allow-origin');
        $this->middleware(\App\Http\Middleware\LTIVerifyCsrfToken::class);
        $this->professionalService = $professionalService;
    }


    public function routeLtiCourse(Request $request){
        //return ($request->all());
        //return ($_POST['lti_message_type']);
        // return ($request['lti_message_type']);
        $v = Validator::make($request->all(), [

            'custom_round_lti_id'=>'required'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        //dd("hhhhh");
        $lti=new LTI();
        //return "hii";
        //dd("hhh");
        $tool=new RobogardenToolProvider($lti->getDataConnector(),$this->professionalService,$request->custom_round_lti_id,$request->all());
        $tool->setParameterConstraint('oauth_consumer_key', TRUE, 50, array('basic-lti-launch-request', 'ContentItemSelectionRequest', 'DashboardRequest'));
        $tool->setParameterConstraint('resource_link_id', TRUE, 50, array('basic-lti-launch-request'));
        $tool->setParameterConstraint('user_id', TRUE, 50, array('basic-lti-launch-request'));
        $tool->setParameterConstraint('roles', TRUE, NULL, array('basic-lti-launch-request'));
        $x=$tool->handleRequest();

        if($tool->ok==true) {

            return $x;
        }else{
            $response = new ResponseObject();
            $response->error=true;
            $response->message=$tool->message;
            $response->reason=$tool->reason;
            $response->status_code=400;
            $responseobject = new ResponseObject();
            $responseobject->Object = $response;
            $responseobject->status_code = $response->status_code;

            return Response::json($responseobject,$responseobject->status_code);
        }
        //dd("return",$x);
    }
    public function routeLtiCourse2(Request $request){
        if(isset($request->relaunch_url)){
            $lti=new LTI();
            $tool=new RobogardenToolProvider($lti->getDataConnector(),$this->professionalService,$request->custom_round_lti_id,$request->all(),'test2');
            $tool->handleRequest();

            if($tool->ok==true) {
                $relaunchUrl = $request->relaunch_url;

                echo '<form id="LtiRequestForm" name="LtiRequestForm" action="'. $relaunchUrl.'" method="post">';
                echo '<input value="'.$request->platform_state.'" name="platform_state" >';
                echo '<input value="'.csrf_token().'" name="tool_state" >';
                echo '<input type="submit" value="Submit">';
                echo '</form>';
                
                echo "<script language='JavaScript'>
                        document.getElementById('LtiRequestForm').style.display = 'none';
                        
                        document.LtiRequestForm.submit();
                    </script> ";
            }else{
                return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'d2l?error='."true&message=".$tool->message.$tool->reason);

            }

        }else{
            return $this->routeLtiCourse3($request);
        }

    }
    public function routeLtiCourse3(Request $request){
        $v = Validator::make($request->all(), [

            'ext_d2l_username'=>'required'

        ]);
        if ($v->fails()) {
            return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'d2l?error='."true&message=`ext_d2l_username` field is required");
        }
        $lti=new LTI();
        $tool=new RobogardenToolProvider($lti->getDataConnector(),$this->professionalService,$request->custom_round_lti_id,$request->all(),'test2');
        $tool->handleRequest();

        if($tool->ok==true) {
            $userDto = LTIMapper::mapLTIUser($tool->user);
            $username = isset($request['ext_d2l_username']) ? $request['ext_d2l_username'] : '';
            if ($output=preg_match_all('/[\'^£%&*()}{#~?><>.,|=+¬]/', $username,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    $username = substr_replace($username, "_", $match[1]+$temp, 1);
                    $temp++;
                }
            }

            $userDto->username=$username;
            
            $x=$this->professionalService->routeConsumerUsers2( $userDto, $request->custom_round_lti_id,$request->all());

            return $x;
            
        }else{
            return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'d2l?error='."true&message=".$tool->message.$tool->reason);
        }

    }
    public function authenticateLTIUser(Request $request){
        // dd($request->all(),$_POST);

        $v = Validator::make($request->all(), [

            'custom_round_lti_id'=>'required',
            'account_id'=>'required',
            'user_id'=>'required',
            'lis_person_name_given'=>'required',
            'lis_person_name_family'=>'required',
            'roles'=>'required',
            'ext_d2l_username'=>'required',
            'lis_person_contact_email_primary'=>'required',

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = LTIMapper::mapLTIUser($request);
        $username = isset($request->ext_d2l_username) ? $request->ext_d2l_username : '';

        if ($output=preg_match_all('/[\'^£%&*()}{#~?><>.,|=+¬]/', $username,$matches,PREG_OFFSET_CAPTURE)) {
            $temp=0;

            foreach ($matches[0] as $match){
                //dd($match[1]);
                $username = substr_replace($username, "_", $match[1]+$temp, 1);
                $temp++;
            }
        }
        $userDto->username=$username;
        $response=$this->professionalService->authenticateLTIUser($request->account_id,$userDto,$request->custom_round_lti_id);
        return $response;

    }

    public function routeD2lLti(Request $request){

        $lti=new LTI();
        $tool=new RobogardenToolProvider($lti->getDataConnector(),$this->professionalService,null,$request->all(),"test");

        $x=$tool->handleRequest();
        return $x;

    }
    public function  ltiSynchBack(){
        $response=$this->professionalService->ltiSynchBack();
        return $response;
    }


}