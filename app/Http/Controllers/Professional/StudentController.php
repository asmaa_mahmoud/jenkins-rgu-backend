<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use BigBlueButton\BigBlueButton;

class StudentController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }

    public function getHtmlMissionData(Request $request){

        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id' => 'integer|min:0',
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;
       
        $response = $this->professionalService->getHtmlMissionData($request->learning_path_id, $request->campus_id, $request->activity_id, $request->mission_id, $user_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getHtmlMissionNext(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getHtmlMissionNext($request->learning_path_id, $request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id,$request->round_id, $user_id,$request->is_default);
        return $this->handleResponse($response);

    }

    public function getProjectData(Request $request){

        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id' => 'integer|min:0',
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;
       
        $response = $this->professionalService->getProjectData($request->learning_path_id, $request->campus_id, $request->activity_id, $request->mission_id, $user_id,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getMissionData(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'mission_id'=>'required',
            'activity_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getMissionData($request->learning_path_id, $request->campus_id,$request->activity_id, $request->mission_id, $user_id,$request->is_default);
        return $this->handleResponse($response);

    }

    public function getMissionNext(Request $request){

        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getMissionNext($request->learning_path_id, $request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id,$request->round_id,$user_id, $request->is_default);
        return $this->handleResponse($response);

    }
    public function getQuizNext(Request $request){

        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getQuizNext($request->learning_path_id, $request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id,$request->round_id,$user_id, $request->is_default);
        return $this->handleResponse($response);

    }

    public function checkMissionAuthorization(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'required',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->checkMissionAuthorization($request->learning_path_id, $request->activity_id , $request->mission_id,$user_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getCodingMissionData(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id' => 'integer|min:0',
            'mission_id'=>'required',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getCodingMissionData($request->learning_path_id, $request->campus_id, $request->activity_id, $request->mission_id, $user_id);
        return $this->handleResponse($response);
    }

    public function getCodingMissionNext(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getCodingMissionNext($request->learning_path_id, $request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id,$request->round_id,$user_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getEditorMissionData(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id' => 'integer|min:0',
            'mission_id'=>'required',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getMissionEditorData($request->learning_path_id, $request->campus_id, $request->activity_id, $request->mission_id, $user_id);
        return $this->handleResponse($response);
    }

    public function getEditorMissionNext(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getMissionEditorNext($request->learning_path_id, $request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id,$request->round_id,$user_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getAngularMissionData(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id' => 'integer|min:0',
            'mission_id'=>'required',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getAngularMissionData($request->learning_path_id, $request->campus_id, $request->activity_id, $request->mission_id, $user_id);
        return $this->handleResponse($response);
    }

    public function getAngularMissionNext(Request $request){
        $v = Validator::make($request->all(), [
            'learning_path_id'=>'integer|min:0',
            'campus_id'=>'integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getAngularMissionNext($request->learning_path_id, $request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id,$request->round_id,$user_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getNextClass(Request $request){
        $v = Validator::make($request->all(), [
            'round_id'=>'required|integer|min:0',
            'activity_id'=>'required' ,
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getNextClass($request->round_id, $request->activity_id , $request->is_default);
        return $this->handleResponse($response);

    }

    public function checkCampusMissionAuthorization (Request $request){
        $v = Validator::make($request->all(), [
            'round_id'=>'required|integer|min:0',
            'activity_id'=>'required|integer|min:0' ,
            'mission_id'=>'required|integer|min:0',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->checkCampusMissionAuthorization($request->round_id, $request->activity_id, $request->mission_id, $user_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getHandoutMissionData(Request $request){
        $v = Validator::make($request->all(), [
            'campus_id'=>'integer|min:0',
            'activity_id' => 'integer|min:0',
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;
       
        $response = $this->professionalService->getHandoutMissionData($request->campus_id, $request->activity_id, $request->mission_id, $user_id,$request->is_default);
        return $this->handleResponse($response);   
    }

    public function getHandoutMissionNext(Request $request){
        $v = Validator::make($request->all(), [
            'campus_id'=>'required|integer|min:0',
            'activity_id'=>'required' ,
            'mission_id'=>'required',
            'is_default'=>'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getHandoutMissionNext($request->campus_id, $request->activity_id ,$request->submodule_id, $request->mission_id, $request->round_id, $user_id, $request->is_default);
        return $this->handleResponse($response);   
    }
}
