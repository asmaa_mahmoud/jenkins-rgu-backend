<?php 

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Accounts\CustomMappers\SupportEmailDtoMapper;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Storage;
use Config;
use Carbon\Carbon;
use App\Framework\Exceptions\BadRequestException;

class LessonController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){

        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
   
    }

    public function submitLessonDragQuestion(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'question_id' => 'required|integer|min:0',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitLessonDragQuestion($request->data, $request->mission_id,$request->question_id, $user->id, $request->round_id, $request->activity_id);
        return $this->handleResponse($response);
    }


    public function submitLessonDropdownQuestion(Request $request)
    {
        
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'question_id' => 'required|integer|min:0',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitLessonDropdownQuestion($request->data, $request->mission_id,$request->question_id, $user->id, $request->round_id, $request->activity_id);
        return $this->handleResponse($response);
    }

    public function submitLessonMcqQuestion(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'question_id' => 'required|integer|min:0',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitLessonMcqQuestion($request->choices, $request->mission_id,$request->question_id, $user->id, $request->round_id, $request->activity_id);
        return $this->handleResponse($response);

    }

    public function submitLessonMatchQuestion(Request $request)
    {
        $v = Validator::make($request->all(), [
            'mission_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'round_id' => 'required|integer|min:0',
            'question_id' => 'required|integer|min:0',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->submitLessonMatchQuestion($request->data, $request->mission_id,$request->question_id, $user->id, $request->round_id, $request->activity_id);
        return $this->handleResponse($response);

    }

   
}
