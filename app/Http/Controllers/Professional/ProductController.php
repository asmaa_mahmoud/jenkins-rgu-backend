<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class ProductController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService)
    {
        // $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }

    public function getProductAssets(Request $request)
    {
        $v = Validator::make($request->all(), [
            'product_url' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $response = $this->professionalService->getProductAssets($request->product_url);
        return $this->handleResponse($response);
    }
}
