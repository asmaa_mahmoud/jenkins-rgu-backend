<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundRequestDto;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Helpers\Mapper;


class AdminController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }


    public function getCampusRounds(Request $request,$campus_id){
        $v = Validator::make($request->all(), [

            'is_default'=>'required',
            'gmt_difference' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getAdminCampusRounds($user_id, $campus_id,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);
    }

    public function editCampusRound(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'round_name'=>'required',
            'starts_at' => 'required|integer|min:0',
            'ends_at' => 'required|integer|min:0',
            'classes' =>'required',
            'gmt_difference' => 'required',
            ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $campusRoundDto = Mapper::MapRequest(CampusRoundRequestDto::class, $request->all());

        $response = $this->professionalService->editAdminCampusRound( $campusRoundDto, $user_id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getClassTasksAndRooms(Request $request,$class_id){
        $v = Validator::make($request->all(), [
            'is_default'=>'required',
            'round_id'=>'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getClassTasksAndRooms($user_id,$class_id,$request->round_id,$request->is_default);
        return $this->handleResponse($response);

    }

    public function setAssignedAndUnassignedStudentsInCampusRound( Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|numeric',
            'assignedStudents' => 'array',
            'unAssignedStudents' => 'array',
            'gmt_difference' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->setAssignedAndUnassignedStudentsInCampusRound($user->id,$request->round_id, $request->unAssignedStudents, $request->assignedStudents,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function setAssignedAndUnassignedTeachersInCampusRound( Request $request){
        $v = Validator::make($request->all(), [
            'round_id'=>'required|numeric',
            'assignedTeachers' => 'array',
            'unAssignedTeachers' => 'array',
            'gmt_difference' => 'required',
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->setAssignedAndUnassignedTeachersInCampusRound($user->id,$request->round_id, $request->unAssignedTeachers, $request->assignedTeachers,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function addCampusRound(Request $request){
        $v = Validator::make($request->all(), [
            'campus_id' => 'required|numeric',
            'starts_at' => 'required|integer',
            'ends_at' => 'required|integer',
            'is_default'=>'required',
            'round_name' =>'required'
            /*'students' => 'array',
            'teachers' => 'array',
            'classes_ids'=>'array',
            'classes_start_dates'=>'array',
            'classes_end_dates'=>'array',
            'classes_due_dates'=>'array',
            'classes_weights'=>'array',
            'classes_tasks_ids'=>'array',
            'classes_tasks_weights'=>'array',
            'classes_tasks_weights_overdue'=>'array',
            'classes_tasks_due_dates'=>'array',*/

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $campusRoundDto = Mapper::MapRequest(CampusRoundRequestDto::class, $request->all());
        $response = $this->professionalService->createCampusRound($campusRoundDto,$user->id,$request->is_default);
        return $this->handleResponse($response);
    }
    public function getRoundData(Request $request,$round_id){
        $v = Validator::make($request->all(), [
            'is_default'=>'required',
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getRoundData($user->id,$round_id,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);

    }
    public function getRoundDataWithoutHtml(Request $request,$round_id){
        //dd("hh");
        $v = Validator::make($request->all(), [
            'is_default'=>'required',
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getRoundDataWithoutHtml($user->id,$round_id,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);

    }

    public  function getAdminStudents(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required',
            
            

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }
        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }


        $response = $this->professionalService->getAdminStudents($user->id, $start_from, $limit, $search,$order_by,$order_by_type,$request->gmt_difference);

        return $this->handleResponse($response);
    }

    public  function getAdminTeachers(Request $request){
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by=null;


        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }
        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }

        $response = $this->professionalService->getAdminTeachers($user->id, $start_from, $limit, $search,$order_by,$order_by_type);

        return $this->handleResponse($response);
    }

    public  function getAdminStudentsCount(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();

        $search = null;

        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }



        $response = $this->professionalService->getAdminStudentsCount($user->id,  $search,$request->gmt_difference);

        return $this->handleResponse($response);
    }

    public  function getAdminTeachersCount(Request $request){
        $user = JWTAuth::toUser();

        $search = null;

        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }

        $response = $this->professionalService->getAdminTeachersCount($user->id,$search);

        return $this->handleResponse($response);
    }

    public function updateCampusStudent(Request $request,$student_id){
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'min:6|max:24',
            'force' => 'boolean',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $studentDto = Mapper::MapRequest(UserDto::class, $request->all());
        $studentDto->password = $request['password'];
        $studentDto->id = $student_id;
        $studentDto->force = $request['force'];
        $user = JWTAuth::toUser();
        $response = $this->professionalService->updateCampusStudent($user->id,  $studentDto);
        return $this->handleResponse($response);

    }

    public function updateCampusTeacher(Request $request,$teacher_id){
        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'min:6|max:24',
            'email' => 'email',
            'force' => 'boolean',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $teacherDto = Mapper::MapRequest(UserDto::class, $request->all());
        $teacherDto->password = $request['password'];
        $teacherDto->id = $teacher_id;
        $teacherDto->force = $request['force'];
        $user = JWTAuth::toUser();
        $response = $this->professionalService->updateCampusTeacher($user->id,  $teacherDto);
        return $this->handleResponse($response);

    }

    public function deleteStudents(Request $request)
    {

        $v = Validator::make($request->all(), [

            'students' => 'required|array',
            //'force'=>'boolean'

        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->professionalService->deleteStudents($user->id,$request->students,$request->force);
        return $this->handleResponse($response);
    }

    public function deleteTeachers(Request $request)
    {
        $v = Validator::make($request->all(), [
            'teachers' => 'required|array',
            'force'=>'boolean'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();


        $response = $this->professionalService->deleteTeachers($user->id,$request->teachers,$request->force);
        return $this->handleResponse($response);
    }

    public function updateCampusRound(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'starts_at' => 'integer|min:0',
            'ends_at' => 'integer|min:0',
            //'gmt_difference' => 'required'
            /*'classes' => 'array',
            'tasks' => 'array',
            'rooms' => 'array'*/
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $campusRoundDto = Mapper::MapRequest(CampusRoundRequestDto::class, $request->all());

        $response = $this->professionalService->updateCampusRound($user_id, $campusRoundDto->round_id, $campusRoundDto->starts_at, $campusRoundDto->ends_at, $campusRoundDto->classes, $campusRoundDto->tasks, $campusRoundDto->sessions,$campusRoundDto->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getAdminRoundsByStatus(Request $request){
        $v = Validator::make($request->all(), [
            'status' => 'required',
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getAdminRoundsByStatus($user_id, $request->status,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function addTeacher(Request $request){

        $v = Validator::make($request->all(), [

            'fname' => 'required|min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'email' => 'required|email',
            'password' => 'required|min:6|max:24',

        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $userDto = UserDtoMapper::RequestMapper($request);
        $user = JWTAuth::toUser();
        $response = $this->professionalService->addTeacher($user->id,$userDto);
        return $this->handleResponse($response);

    }

    public function addStudent(Request $request){

        $v = Validator::make($request->all(), [
            'fname' => 'required|min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'lname' => 'min:2|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
            'password' => 'required|min:6|max:24',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $studentDto = Mapper::MapRequest(UserDto::class,$request->all());
        $studentDto->password = $request->password;

        $user = JWTAuth::toUser();
        $response = $this->professionalService->addStudent($user->id,$studentDto);
        return $this->handleResponse($response);
    }

    public function addStudentsCSV(Request $request){

        $v = Validator::make(
            ['file'=> $request->file],['file' => 'required|max:20480']
        );

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $v = Validator::make(
            ['extension' => strtolower($request->file->getClientOriginalExtension())], ['extension' => 'required|in:csv']
        );

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();

        $response = $this->professionalService->addStudentsCSV($user->id,$request->file);
        return $this->handleResponse($response);

    }

    public function generalAdminStatisticsInCampuses(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->generalAdminStatisticsInCampuses($user->id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getAdminSubmissionsChart(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'from_time' => 'required|date',
            'to_time' => 'required|date'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->getSubmissionsChart($user->id,$request->round_id,$request['from_time'],$request['to_time']);
        return $this->handleResponse($response);
    }

    public function getAdminRecentActivity(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getAdminRecentActivity($user->id,$request->gmt_difference);
        return $this->handleResponse($response);

    }

    public function getStudentsInRoundToEnroll($round_id,Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $search=null;

        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getStudentsInRoundToEnroll($user->id,$round_id,$search,$request->gmt_difference);
        return $this->handleResponse($response);

    }

    public function getTeachersInRoundToEnroll($round_id,Request $request){
        $user = JWTAuth::toUser();
        $search=null;

        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        $response = $this->professionalService->getTeachersInRoundToEnroll($user->id,$round_id,$search);
        return $this->handleResponse($response);

    }


    public function deleteCampusRound(Request $request){
       // dd($request->force);
        $v = Validator::make($request->all(), [
            'round_id'=>'required|numeric',
            'force' => 'in:true,false',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->deleteCampusRound($user->id,$request->round_id,$request->force);
        return $this->handleResponse($response);

    }


    public function getCampusAdminStudents($campus_id, Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }

        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }


        $response = $this->professionalService->getCampusAdminStudents($user->id, $campus_id, $start_from, $limit, $search,$order_by,$order_by_type,$request->gmt_difference);

        return $this->handleResponse($response);
    }
    public function getCampusAdminStudentsCount($campus_id, Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;

        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
            if ($output=preg_match_all('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $search,$matches,PREG_OFFSET_CAPTURE)) {
                $temp=0;

                foreach ($matches[0] as $match){
                    //dd($match[1]);
                    $search = substr_replace($search, "\\", $match[1]+$temp, 0);
                    $temp++;
                }
            }
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }
        if(!empty($request['order_by_type'])){
            $order_by_type = $request['order_by_type'];
        }else{
            $order_by_type="ASC";
        }


        $response = $this->professionalService->getCampusAdminStudents($user->id, $campus_id, $start_from, $limit, $search,$order_by,$order_by_type,$request->gmt_difference,true);

        return $this->handleResponse($response);
    }
    public function getCalenderData(Request $request){
        $v = Validator::make($request->all(), [

            'gmt_difference' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getCalenderData($user_id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateCalenderData(Request $request){
        $v = Validator::make($request->all(), [
            'round_id'=>'required',
            'ends_at'=>'required',
            'gmt_difference' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService-> updateCalenderData($user_id,$request->round_id,$request->ends_at,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateCalendarDataInModule(Request $request){
        $v = Validator::make($request->all(), [
            'round_id'=>'required',
            'class_id'=>'required',
            'starts_at'=>'required',
            'ends_at'=>'required',
            'gmt_difference' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService-> updateCalendarDataInModule($user_id,$request->round_id,$request->class_id,$request->starts_at,$request->ends_at,$request->gmt_difference);
        return $this->handleResponse($response);
    }


}
