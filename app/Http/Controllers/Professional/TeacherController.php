<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Dtos\CampusRoundRequestDto;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Storage;
use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class TeacherController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth', ['except' => ['roomWebHook'] ]);
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }


    public function getCampusRound($round_id,Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherCampusRound($user_id, $round_id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getCampuses(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherCampuses($user_id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function updateCampusRound(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'starts_at' => 'integer|min:0',
            'ends_at' => 'integer|min:0',
            //'gmt_difference' => 'required'
           /* 'classes' => 'array',
            'tasks' => 'array',
            'rooms' => 'array'*/
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $campusRoundDto = Mapper::MapRequest(CampusRoundRequestDto::class, $request->all());

        $response = $this->professionalService->updateCampusRound($user_id, $campusRoundDto->round_id, $campusRoundDto->starts_at, $campusRoundDto->ends_at, $campusRoundDto->classes, $campusRoundDto->tasks, $campusRoundDto->sessions,$campusRoundDto->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getStudentsInRound($round_id,Request $request){
        $user = JWTAuth::toUser();
        $user_id=$user->id;
        $search = $request['search'];
        $response = $this->professionalService->getStudentsInRound($user_id, $round_id,$search);
        return $this->handleResponse($response);
    }

    public function getRoundClasses($round_id){
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherRoundClasses($user_id, $round_id);
        return $this->handleResponse($response);
    }

    public function getRoundClass(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
            'gmt_difference'=>'required',
            'is_default'=>'required'

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherRoundClass($user_id, $request->round_id, $request->class_id,$request->gmt_difference,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getRoundClassSubmissions(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'is_default' => 'required'
        ]);
        $limit = 100;
        $start_from = 0;
        $search = null;
        $order_by = null;
        if(!empty($request['limit'])) {
            $limit = $request['limit'];
        }
        if(!empty($request['start_from'])){
            $start_from = $request['start_from'];
        }
        if(!empty($request['search'])){
            $search = $request['search'];
        }
        if(!empty($request['order_by'])){
            $order_by = $request['order_by'];
        }


        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getRoundClassSubmissions($user_id, $request->round_id, $request->activity_id, $start_from, $limit, $search,$order_by, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getRoundClassSubmissionsCount(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'activity_id' => 'required|integer|min:0',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $search=null;
        if(!empty($request['search'])){
            $search = $request['search'];
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getRoundClassSubmissionsCount($user_id, $request->round_id, $request->activity_id, $search,$request->is_default);
        return $this->handleResponse($response);
    }

    public function getStudentSubmission(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'submission_id' => 'required|integer|min:0',
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getStudentSubmission($user_id, $request->round_id, $request->submission_id);
        return $this->handleResponse($response);
    }

    public function updateStudentSubmissionEvaluation(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'submission_id' => 'required|integer|min:0',
            'evaluation' => 'required|integer',
            'gmt_difference' => 'required',

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->updateStudentSubmissionEvaluation($user_id, $request->round_id, $request->submission_id, $request->evaluation,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getRoundClassRooms($round_id,$class_id){
//        $v = Validator::make($request->all(), [
//            'round_id' => 'required|integer|min:0',
//            'class_id' => 'required|integer|min:0',
//        ]);
//
//        if ($v->fails()) {
//            return $this->handleValidationError($v->errors()->all());
//        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getRoundClassRooms($user_id, $round_id, $class_id);
        return $this->handleResponse($response);
    }

    public function getTeacherRoundsByStatus(Request $request){
        $v = Validator::make($request->all(), [
            'status' => 'required',
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherRoundsByStatus($user_id, $request->status,$request->gmt_difference);
        return $this->handleResponse($response);

    }

    public function updateRoundClass(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|numeric',
            'classes' =>'array'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->updateRoundClass($user_id, $request->round_id,$request->classes);
        return $this->handleResponse($response);
    }

    public function createRoom(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' =>'required|integer|min:0',
            'starts_at' => 'required|integer|min:0',
            'name' => 'required|string',
            'file' => 'max:10480',
            'gmt_difference'=>'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $file_s3_url = '';
        if($request->file) {
            $file = $request->file;
//            if(preg_match_all("/[^a-z0-9\_\-\s\.]/i",$file->getClientOriginalName())){
//                throw new BadRequestException(trans('locale.file_name_should_not_has_special_chars'));
//            }
            $file_name =  Carbon::now()->timestamp . '_' . $file->getClientOriginalName();
            Config::set('filesystems.disks.s3.bucket', 'robogarden-professional');
            Config::set('filesystems.disks.s3.region', 'us-west-2');
            Storage::disk('s3')->put('presentations'.'/'.$file_name, file_get_contents($file), 'public');
            $file_s3_url = "https://s3-us-west-2.amazonaws.com/robogarden-professional/presentations/" .urlencode($file_name);
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->createRoom($user_id, $request->round_id,$request->class_id, $request->starts_at, $request->name,$request->gmt_difference, $file_s3_url);
        return $this->handleResponse($response);
    }

    public function updateRoom(Request $request){
        $v = Validator::make($request->all(), [
            'room_id' => 'required|integer|min:0',
            'starts_at' => 'integer|min:0',
            'name' => 'string',
            'file' => 'max:10480',
            'file_uploaded' => 'required|boolean'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $file_uploading_flag = $request->file_uploaded;
        $file_s3_url = '';
        if($request->file) {
            $file = $request->file;
//            if(preg_match_all("/[^a-z0-9\_\-\s\.]/i",$file->getClientOriginalName())){
//                throw new BadRequestException(trans('locale.file_name_should_not_has_special_chars'));
//            }
            $file_name =  Carbon::now()->timestamp . '_' . $file->getClientOriginalName();
            Config::set('filesystems.disks.s3.bucket', 'robogarden-professional');
            Config::set('filesystems.disks.s3.region', 'us-west-2');
            Storage::disk('s3')->put('presentations'.'/'.$file_name, file_get_contents($file), 'public');
            $file_s3_url = "https://s3-us-west-2.amazonaws.com/robogarden-professional/presentations/" .urlencode($file_name);
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->updateRoom($user_id, $request->room_id,$request->starts_at, $request->name, $file_s3_url, $file_uploading_flag);
        return $this->handleResponse($response);
    }


    public function uploadMaterialToClass(Request $request) {
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
            'file' => 'required|max:10480'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $file_s3_url = '';
        $file_name = '';
        $file_name_ = '';
        if($request->file) {
            $file = $request->file;
            $file_name_ = $file->getClientOriginalName();
            $file_name =  Carbon::now()->timestamp . '_' . $file->getClientOriginalName();
            Config::set('filesystems.disks.s3.bucket', 'robogarden-professional');
            Config::set('filesystems.disks.s3.region', 'us-west-2');
            Storage::disk('s3')->put('presentations'.'/'.$file_name, file_get_contents($file), 'public');
            $file_s3_url = "https://s3-us-west-2.amazonaws.com/robogarden-professional/presentations/" .urlencode($file_name);
        }

        $response = $this->professionalService->uploadMaterialToClass($user_id, $request->round_id, $request->class_id,$file_name_ , $file_s3_url);
        return $this->handleResponse($response);

    }
    public function deleteMaterialFromClass(Request $request) {
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
            'material_id' => 'required|integer|min:0',
            'force'=>'required'

        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }



        $response = $this->professionalService->deleteMaterialFromClass($user_id, $request->round_id, $request->class_id,$request->material_id,$request->force);
        return $this->handleResponse($response);

    }

    public function getTeacherSubmissionsNotifications(Request $request){
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherSubmissionsNotifications($user_id);
        return $this->handleResponse($response);
    }

    public function getClassTasks(Request $request, $class_id){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|numeric',
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherClassTasks($user_id, $class_id,$request->round_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getClassActivity(Request $request, $class_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getTeacherClassActivity($user_id, $class_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function generalTeacherStatisticsInCampuses(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);
        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->generalTeacherStatisticsInCampuses($user->id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function getTopStudentsInCampusRound(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|numeric|min:1'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getTopStudentsInCampusRound($user->id, $request->round_id);
        return $this->handleResponse($response);
    }

    public function getTeacherSubmissionsChart(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'from_time' => 'required|date',
            'to_time' => 'required|date'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->getSubmissionsChart($user->id,$request->round_id,$request['from_time'],$request['to_time']);
        return $this->handleResponse($response);
    }

    public function getTeacherRecentActivity(Request $request){
        $v = Validator::make($request->all(), [
            'gmt_difference' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }
        $user = JWTAuth::toUser();
        $response = $this->professionalService->getTeacherRecentActivity($user->id,$request->gmt_difference);
        return $this->handleResponse($response);
    }

    public function sendSolveTaskNotifications(Request $request){
        $v = Validator::make($request->all(), [
            'round_id' => 'required|integer|min:0',
            'class_id' => 'required|integer|min:0',
            'task_id' => 'required|integer|min:0',
            'date' => 'required|integer',
            'is_default'=>'required'


        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $response = $this->professionalService->sendSolveTaskNotifications($user->id,$request->round_id,$request->class_id,$request->task_id,
            $request->date,
            $request->is_default);
        return $this->handleResponse($response);

    }

    public function roomWebHook(Request $request){
        $inputJSON = file_get_contents('php://input');
        $out = $this->getStringBetween($inputJSON,"event=",'&');
        $input = json_decode(urldecode(urldecode( ($out))), TRUE);
        //dd($input[0]['data'],$inputJSON,$out,urldecode(urldecode( ($out))));
        $events = null;
        foreach ($input as $event) {
            $event_ = [];
            $event_['type'] = $event['data']['id'];
            $event_['meeting_id'] = $event['data']['attributes']['meeting']['external-meeting-id'];
            $events []= $event_;
        }

        $event = $events[0];
        
        if($event['type'] == 'user-presenter-assigned' || $event['type'] == 'meeting-ended'){
            $response = $this->professionalService->roomWebHook($event);
        }
    }


    //Private Functions
    private function getStringBetween($str,$from,$to){
        $sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
        return substr($sub,0,strpos($sub,$to));
    }
}
