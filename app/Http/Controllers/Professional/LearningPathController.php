<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class LearningPathController extends Controller
{
    private $professionalService;

    public function __construct(IProfessionalMainService $professionalService){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
    }

    public function getStudentLearningPath(){
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getStudentLearningPath($user_id);
        return $this->handleResponse($response);
    }
    public function getAllLearningPath(){
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getAllLearningPath($user_id);
        return $this->handleResponse($response);
    }

    public function getLearningPathById($id){
        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getLearningPathById($user_id,$id);
        return $this->handleResponse($response);
    }

    public function getLearnPathStages(Request $request){
       // return($request->all());
         $user = JWTAuth::toUser();
         $user_id=$user->id;

         $response = $this->professionalService->getLearnPathStages($user_id,$request['learning_path_id']);
         return $this->handleResponse($response);
    }
    public function getTestLearnPathStages(Request $request){
       // return($request->all());
         $user = JWTAuth::toUser();
         $user_id=$user->id;

         $response = $this->professionalService->getTestLearnPathStages($user_id);
         return $this->handleResponse($response);
    }


    public function getStageActivities(Request $request, $learning_path_id, $stage_number){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

         $user = JWTAuth::toUser();
         $user_id=$user->id;

         $response = $this->professionalService->getStageActivities($user_id,$stage_number,$learning_path_id, $request->is_default);
         return $this->handleResponse($response);
    }

    public function getActivity(Request $request, $learning_path_id, $activity_id){

        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();

        $response = $this->professionalService->getActivity($user->id, $learning_path_id, $activity_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getActivityTasks(Request $request, $learning_path_id, $activity_id){

        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getActivityTasks($user->id, $learning_path_id, $activity_id, $request->is_default);
        return $this->handleResponse($response);
    }

    public function getStudentLearningPathLessons(Request $request,$learning_path_id){
        $v = Validator::make($request->all(), [
            'is_default' => 'required'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

        $user = JWTAuth::toUser();
        $user_id=$user->id;

        $response = $this->professionalService->getStudentLearningPathLessons($user->id, $learning_path_id,$request->is_default);
        return $this->handleResponse($response);

    }
}
