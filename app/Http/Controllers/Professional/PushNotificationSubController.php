<?php

namespace App\Http\Controllers\Professional;

use App\ApplicationLayer\Accounts\Dtos\PushNotificationDto;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use App\DomainModelLayer\Accounts\PushNotification;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\Notifications\StartQuiz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Minishlink\WebPush\WebPush;

class PushNotificationSubController extends Controller
{
    private $professionalService;
    private $accountMainRepository;

    public function __construct(IProfessionalMainService $professionalService, IAccountMainRepository $accountMainRepository){
        $this->middleware('jwt.auth');
        $this->middleware('allow-origin');
        $this->professionalService = $professionalService;
        $this->accountMainRepository = $accountMainRepository;
    }

    public function setPushSubscription(Request $request) {
        $user_id = JWTAuth::toUser();
        $user = $this->accountMainRepository->getUserById($user_id->id);
        //$user->updatePushSubscription($request->endpoint, $request->publicKey, $request->authToken);

        $pushNotification = $this->accountMainRepository->getPushNotificationSubByEndpoint($request->endpoint);
        if($pushNotification==null){
            $pushNotDto = new PushNotificationDto();
            $pushNotDto->public_key = null;
            $pushNotDto->auth_token = null;
            $pushNotDto->endpoint = null;
            $pushNot = new PushNotification($pushNotDto,$user);
            $pushNot->setEndPoint($request->endpoint);
            $pushNot->setAuthToken($request->authToken);
            $pushNot->setPublicKey($request->publicKey);
            $this->accountMainRepository->storePushNotificationSub($pushNot);
            return $this->handleResponse('User subscribed');

        }else{
            $pushNotification->setAuthToken($request->authToken);
            $pushNotification->setPublicKey($request->publicKey);
            $pushNotification->setUserId($user_id->id);
            $this->accountMainRepository->storePushNotificationSub($pushNotification);
            return $this->handleResponse('User subscribed');
        }

    }

    public function updatePushSubscription(Request $request) {
        $user_id = JWTAuth::toUser();
        $user = $this->accountMainRepository->getUserById($user_id->id);
        //$user->updatePushSubscription($request->endpoint, $request->publicKey, $request->authToken);
        $pushNot = $this->accountMainRepository->getPushNotificationSubByEndpoint($request->endpoint);
        $pushNot->setAuthToken($request->authToken);
        $pushNot->setPublicKey($request->publicKey);
        $pushNot->setUserId($user_id->id);
        $this->accountMainRepository->storePushNotificationSub($pushNot);
        return $this->handleResponse('User updates subscription');
    }

    public function deletePushSubscription(Request $request) {
        $subscription = $this->accountMainRepository->getPushNotificationSubByEndpoint($request->endpoint);
        $this->accountMainRepository->deletePushNotificationSub($subscription);
        return $this->handleResponse('User un subscribed');
    }

    public function test(Request $request){
        $user_id = JWTAuth::toUser();
        $user = $this->accountMainRepository->getUserById($user_id->id);

        $quiz_id = 1;

        //$user->notify(new StartQuiz($quiz_id));

        $subscriptions = $this->accountMainRepository->getPushNotificationSub($user->id);

        $auth = array(
            'VAPID' => array(
                'subject' => Config('webpush.vapid.subject'),
                'publicKey' => Config('webpush.vapid.public_key'),
                'privateKey' => Config('webpush.vapid.private_key'),
            ),
        );

        $webPush = new WebPush($auth);
        $fails = 0;
        foreach ($subscriptions as $subscription) {
            $res = $webPush->sendNotification(
                $subscription->endpoint,
                "{\"message\":\"Quiz #" . $quiz_id . " has been started\",\"url\": \"".  Config('app.frontend_url')."class/221/mission/2305/65/2\"}",
                $subscription->public_key,
                $subscription->auth_token,
                true
            );
            if($res) {

            }else {
                $fails++;
            }
        }

        return $this->handleResponse('Pushed ,num of fails:' . $fails);

    }

    public function send(Request $request) {
        $user = JWTAuth::toUser();

        $v = Validator::make($request->all(), [
            'message' => 'required',
            'url' => 'required',
            'round_id' => 'required|integer'
        ]);

        if ($v->fails()) {
            return $this->handleValidationError($v->errors()->all());
        }

         $message = $request->message;
         $url = $request->url;
        $round_id = $request->round_id;




        $auth = array(
            'VAPID' => array(
                'subject' => Config('webpush.vapid.subject'),
                'publicKey' => Config('webpush.vapid.public_key'),
                'privateKey' => Config('webpush.vapid.private_key'),
            ),
        );

        $push_counter = 0;
        $success_counter = 0;
        $payload = "{\"message\":\"".$message."\",\"url\":\"".$url."\"}";
         $subscribers = $this->professionalService->getStudentsIDsInRound($user->id, $round_id);
         foreach ($subscribers as $subscriber) {
             $push_counter +=1;
             $webPush = new WebPush($auth);

             $subscriptions = $this->accountMainRepository->getPushNotificationSub($subscriber);
             foreach ($subscriptions as $subscription) {
                 $res = $webPush->sendNotification(
                     $subscription->endpoint,
                     $payload,
                     $subscription->public_key,
                     $subscription->auth_token,
                     true
                 );

                 if ($res) {
                     $success_counter += 1;
                 }
             }
         }
        return $this->handleResponse('Number of Pushes: '. $push_counter . ' Number of Successful Pushes: '. $success_counter);
    }


}
