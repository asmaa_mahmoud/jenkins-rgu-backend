<?php

namespace App\Http\Controllers;

//use GuzzleHttp\Psr7\Request;
use App\ApplicationLayer\Accounts\Dtos\InvitationDto;
use App\ApplicationLayer\Accounts\Dtos\InvitationRequestDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Helpers\ResponseObject;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\InternalErrorException;
use App\Framework\Exceptions\UnauthorizedException;
use Illuminate\Support\Facades\Response;
use App\Helpers\HttpMethods;
use DB;
use LaravelLocalization;
use Excel;

class TempController extends Controller
{
    private $accountService;
    private $schoolService;

    public function __construct(IAccountMainService $accountService,ISchoolMainService $schoolService){
        $this->accountService = $accountService;
        $this->schoolService = $schoolService;
    }

    public function mapInvitedUsers()
    {
        $invited_users = $this->loadCSVFileFromStorage('Book1.csv');
        DB::connection('mysql')->beginTransaction();
// <<<<<<< HEAD
        for ($i = 250;$i < 301;$i++)
// =======
//         for ($i = 300;$i < 301;$i++)
// >>>>>>> 0ba8849e2d0f4a52911439cf5419cd4fd71a475e
        {
            $user_data = $invited_users[$i];
            $account_type = DB::connection('mysql')->table('account_type')->where('name',$user_data[1])->first();

            $invitation_id = DB::connection('mysql')->table('invitation')->insertGetId([
                'code' => $user_data[0],
                'account_type_id' => $account_type->id,
                'ends_at' => '2020-01-01',
                'number_of_users' => 1
            ]);

            DB::connection('mysql')->table('invitation_details')->insert([
                'invitation_id' => $invitation_id,
            ]);

            $journeys = DB::connection('mysql')->table('journey')->whereNull('deleted_at')->where('journey_status_id',2)->get();

            foreach ($journeys as $journey) {
                DB::connection('mysql')->table('invitation_journey')->insert([
                    'invitation_id' => $invitation_id,
                    'progress_lock' => 0,
                    'journey_id' => $journey->id
                ]);
            }

            $user_dto = new UserDto();
            $user_dto->fname = $user_data[2];
            $user_dto->lname = $user_data[3];
            $user_dto->email = $user_data[5];
            $user_dto->username = $user_data[4];
            $user_dto->password = $user_data[6];

            if($account_type->name == 'Individual')
            {
                $this->accountService->registerInvitation($user_dto,$user_data[0]);
            }
            else if($account_type->name == 'Family')
            {
                DB::connection('mysql')->table('family_invitation')->insert([
                    'invitation_id' => $invitation_id,
                    'max_children' => 5
                ]);

                $this->accountService->registerInvitation($user_dto,$user_data[0]);
            }

            else if($account_type->name == 'School')
            {
                DB::connection('mysql')->table('school_invitation')->insert([
                    'invitation_id' => $invitation_id,
                    'max_students' => 20,
                    'max_courses' => 10
                ]);

                $schoolDto = new SchoolDto();
                $schoolDto->country_code = 'JO';

                $this->schoolService->registerInvitedSchool($user_dto,$schoolDto,$user_data[0]);
            }

        }
        DB::connection('mysql')->commit();
        return "Users created successfully";
    }

    //=================== LOAD ALL CSVs IN EVERY ADD QUESTIONS =================
}
