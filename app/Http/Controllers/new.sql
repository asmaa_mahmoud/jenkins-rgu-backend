 create table mission_state (id bigint unsigned not null auto_increment primary key, created_at timestamp null, updated_at timestamp null, name varchar(255) not null, ratio double(8, 2) not null) default character set utf8 collate utf8_unicode_ci;
 create table user_model_answer (id bigint unsigned not null auto_increment primary key, created_at timestamp null, updated_at timestamp null, mission_id bigint unsigned not null, user_id bigint unsigned not null, model_answer text not null) default character set utf8 collate utf8_unicode_ci;
 alter table user_model_answer add constraint user_model_answer_mission_id_foreign foreign key (mission_id) references mission (id) on delete cascade on update cascade;
 alter table user_model_answer add constraint user_model_answer_user_id_foreign foreign key (user_id) references user (id) on delete cascade on update cascade;
 create table user_score (id bigint unsigned not null auto_increment primary key, created_at timestamp null, updated_at timestamp null, xp bigint not null, coins bigint not null, user_id bigint unsigned not null) default character set utf8 collate utf8_unicode_ci;
 alter table user_score add constraint user_score_user_id_foreign foreign key (user_id) references user (id) on delete cascade on update cascade;
 create table model_answer_unlocked (id bigint unsigned not null auto_increment primary key, created_at timestamp null, updated_at timestamp null, mission_id bigint unsigned not null, user_id bigint unsigned not null) default character set utf8 collate utf8_unicode_ci;
 alter table model_answer_unlocked add constraint model_answer_unlocked_mission_id_foreign foreign key (mission_id) references mission (id) on delete cascade on update cascade;
 alter table model_answer_unlocked add constraint model_answer_unlocked_user_id_foreign foreign key (user_id) references user (id) on delete cascade on update cascade;
alter table mission_saving add timetaken int null;
alter table student_progress add blocks_number int null, add task_duration int null;
alter table task_progress add first_success int null, add unlocked_by_coins tinyint(1) not null default '0';
alter table mission add mission_state_id bigint unsigned null;
alter table mission add constraint mission_mission_state_id_foreign foreign key (mission_state_id) references mission_state (id) on delete cascade on update cascade;
INSERT INTO mission_state`(name`, ratio) VALUES ("normal",1.0);
INSERT INTO mission_state`(name`, ratio) VALUES ("super",2.0);
UPDATE mission SET mission_state_id = 1;
UPDATE mission SET weight = 10;
UPDATE task_progress SET first_success = 1;
UPDATE task_progress SET number_of_trials = 1;
INSERT INTO user_score (xp,coins,user_id)
(select CASE WHEN a.xp IS NULL THEN 0 ELSE a.xp END as xp,CASE WHEN a.coins IS NULL THEN 0 ELSE a.coins END as coins,user.id from user
left join
((SELECT  sum(score) as xp,(sum(score))/5 as coins,user_id as id FROM task_progress where deleted_at is null GROUP by user_id)
UNION
(SELECT  sum(score) as xp,(sum(score))/5 as coins,student_id as id FROM student_progress GROUP by student_id)) a
ON a.id = user.id);

UPDATE mission_mcq_question set weight = 10;