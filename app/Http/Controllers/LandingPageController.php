<?php 

namespace App\Http\Controllers;

use App\DomainModelLayer\Accounts\PlanUnlockable;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\Mission;
use App\DomainModelLayer\Journeys\Task;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use LaravelLocalization;
use Analogue;

class LandingPageController extends Controller {

    public function getAllActivities()
    {
    	$defaultActivities = DB::table('default_activity')->where('is_b2c', 1)->where('type', 1)->get();
    	$activityObjects = array();
    	foreach ($defaultActivities as $activity){
    		array_push($activityObjects,$this->createActivityObject($activity,true,false,true));
    	}
        return $this->handleResponse($activityObjects);
    }

    public function showActivity($name)
    {
    	$activity = DB::table('default_activity')->where('id', 
    					function($q) use($name) {
    						$q->select('default_activity_id')->from('activity_translation')->where('name',$name)->whereNull('deleted_at');
    					}
    				)->first();
        return $this->handleResponse($this->createActivityObject($activity,true,true,true));
    }

    function createActivityObject($activity, $show_pre_and_next, $show_related_activities, $show_screenshots){
    	if($activity == null)
    		return null;

    	$language_code = LaravelLocalization::getCurrentLocale();
    	$activity_translation = DB::table('activity_translation')->where('default_activity_id',$activity->id)->where('language_code',$language_code)->whereNull('deleted_at')->first();

        if($activity_translation == null)
            $activity_translation = DB::table('activity_translation')->where('default_activity_id',$activity->id)->where('language_code','en')->whereNull('deleted_at')->first();
        /*$tags = DB::table('tag_translation')->where('language_code',$language_code)->whereIn('tag_id',
                            function($q) use($activity) {
                                $q->select('tag_id')->from('activity_tag')->where('default_activity_id',$activity->id);
                            }
                        )->pluck('name');*/
        $tags = DB::table('tag_translation')->where('language_code',$language_code)->whereIn('tag_id',
                        function($q) use($activity) {
                            $q->select('tag_id')->from('activity_tag')->where('default_activity_id',$activity->id);
                        }
                    )->pluck('name');
        if(count($tags) == 0 || $tags == null){
            $tags = DB::table('tag_translation')->where('language_code','en')->whereIn('tag_id',
                function($q) use($activity) {
                    $q->select('tag_id')->from('activity_tag')->where('default_activity_id',$activity->id);
                }
            )->pluck('name');
        }

        $difficulity = DB::table('difficulty_translation')->where('difficulty_id',$activity->difficulty_id)->where('language_code',$language_code)->first();
        if($difficulity == null)
            $difficulity = DB::table('difficulty_translation')->where('difficulty_id',$activity->difficulty_id)->where('language_code','en')->first();
        $activityObject = [
    		'id' => $activity->id,
    		'name' => $activity_translation->name,
    		'difficulty_translation' => $difficulity->title,
            'difficulty'=>DB::table('difficulty')->where('id',$activity->difficulty_id)->pluck('name')->first(),
    		'age_from' => $activity->age_from,
    		'age_to' => $activity->age_to,
    		'summary' => $activity_translation->summary,
    		'description' => $activity_translation->description,
    		'technical_details' => $activity_translation->technical_details,
    		'icon_url' => $activity->icon_url,
    		'image_url' => $activity->image_url,
    		'tags' => $tags,
    		'concepts' => $this->html_to_string_array($activity_translation->concepts),
    		'screenshots' => DB::table('activity_screenshots')->where('default_activity_id',$activity->id)->pluck('screenshot_url'),
    		'price_family' => DB::table('activity_price')->where('activity_id',$activity->id)->whereIn('period_id', 
    							function($q){
    								$q->select('id')->from('activity_period')->where('period',0)->where('account_type_id',
    									function($q){
    										$q->select('id')->from('account_type')->where('name','Family');
    									}
    								);
    							}
    						)->pluck('price')->first(),
    		'pre_activities' => $show_pre_and_next? $this->get_pre_or_next_activities($activity, 0) : null,
    		'next_activities' => $show_pre_and_next? $this->get_pre_or_next_activities($activity, 1) : null,
            'related_activities' => $show_related_activities? $this->get_related_activities($activity) : null,
            'missions_screenshots' => $show_screenshots? $this->get_missions_screenshots($activity) : null,
            'tasks' => $this->getTasksOfActivity($activity->id),
            'free' => $this->checkIsFreeActivity($activity->id),
    	];

    	return $activityObject;
    }

    function get_pre_or_next_activities($activity, $pre_or_next){
    	$activities = DB::table('default_activity')->whereIn('id',
						function($q) use($activity, $pre_or_next) {
							$q->select('default_activity_id')->from('activity_pre_next')->where('main_default_activity_id',$activity->id)->where('pre_or_next',$pre_or_next);
						}
					)->get();

    	$pre_or_next_activities = array();

    	foreach($activities as $activity){
    		array_push($pre_or_next_activities, $this->createActivityObject($activity, false, false, false));
    	}

    	return $pre_or_next_activities;
    }

    function get_related_activities($activity){
        /*$activities = DB::table('default_activity')->whereIn('id',
            function($q) use($activity) {
                $q->select('default_activity_id')->from('activity_tag')->whereIn('tag_id',
                    function($q) use($activity) {
                        $q->select('tag_id')->from('activity_tag')->where('default_activity_id',$activity->id);
                    }
                )->where('default_activity_id','!=',$activity->id);
            })->get();*/
        $activities = DB::table('default_activity')->whereIn('id',
                            function($q) use($activity){
                                /*$q->table('activity_tag as main')->join('activity_tag as related')->where('main.default_activity_id','related.default_activity_id')->select('related.default_activity_id');*/
                                $q->select('related.default_activity_id')->from('activity_tag as related')->join('activity_tag as main', 'main.default_activity_id', '=', 'related.default_activity_id')->whereIn('related.tag_id',
                                        function($q) use($activity){
                                            $q->select('tag_id')->from('activity_tag')->where('default_activity_id',$activity->id);
                                        }
                                    )->where('related.default_activity_id', '!=', $activity->id)->where('is_b2c', 1)->where('type', 1);
                            }
                        )->get();

        $related_activities = array();

        foreach($activities as $activity){
            array_push($related_activities, $this->createActivityObject($activity, false, false, false));
        }

        return $related_activities;
    }

    function get_missions_screenshots($activity){
        $screenshots = DB::table('mission_screenshot')->whereNull('deleted_at')->whereIn('mission_id',
                            function ($q) use($activity){
                                $q->select('id')->from('mission')->whereNull('deleted_at')->whereIn('task_id',
                                    function($q) use($activity){
                                        $q->select('task_id')->from('activity_task')->whereNull('deleted_at')->where('default_activity_id',$activity->id);
                                    }
                                );
                            }
                        )->pluck('screenshot_url');
        return $screenshots;
    }

    function getTasksOfActivity($activity_id){
        $tasksDto = array();
        $activityMapper = Analogue::mapper(DefaultActivity::class);
        $activity = $activityMapper->query()->find($activity_id);
        foreach ($activity->getTasks() as $task){
            $mission = $task->getMissions()->first();
            $allScreenshots = array();
            if($mission != null) {
                $screenshots = $mission->getScreenshots();
                foreach ($screenshots as $screen){
                    array_push($allScreenshots, $screen->getUrl());
                }
                array_push($tasksDto, [
                    'id' => $mission->getTask()->getId(),
                    'image' => $mission->getIconUrl(),
                    'title' => $mission->getName(),
                    'description' => $mission->getDescription(),
                    'screenshots' => $allScreenshots,
                    'type' => 'mission',
                ]);
            }
            else {
                $mission = $task->getMissionsHtml()->first();
                if($mission != null) {
                    array_push($tasksDto, [
                        'id' => $mission->getTask()->getId(),
                        'image' => $mission->getIconUrl(),
                        'name' => $mission->getTitle(),
                        'type' => "html_mission"
                    ]);
                }
                else {
                    $quiz = $task->getQuizzes()->first();
                    if($quiz != null) {
                        array_push($tasksDto, [
                            'id' => $quiz->getTask()->getId(),
                            'name' => $quiz->getTitle(),
                            'image' => $quiz->geticonURL(),
                            'type' => $quiz->getType()->getName(),
                        ]);
                    }
                }
            }
        }
        return $tasksDto;
    }

    function checkIsFreeActivity($activity_id){
        $free = false;
        $unlockable = DB::table('activity_unlockable')->where('default_activity_id', $activity_id)->first();
        if($unlockable != null){
            $planUnlockable = DB::table('plan_unlockable')->where('unlockable_id', $unlockable->unlockable_id)->where('plan_history_id', function ($q){
                $q->select('id')->from('plan_history')->where('new_name', 'Free')->where('plan_id', function ($query){
                    $query->select('id')->from('plan')->where('name', 'free_learner');
                });
            })->first();
            if($planUnlockable != null){
                $free = true;
            }
        }
        return $free;
    }

    function html_to_string_array($html_p){
        /*$result = $html_p;
        $result = str_replace("<p>","",$result);
        $result = str_replace("</p>", "", $result);
        $result = explode("<br />", $result);
        foreach ($result as $key => $item){
            $result[$key] = trim($item);
        }*/
        $result = str_replace('<br />', '</p>', $html_p);
        $result = str_replace('<p>', '', $result);
        $result = explode('</p>', $result);
        array_pop($result);
        $result = array_map('trim', $result);
        return $result;
    }

    public function getTaskById($task_id){
        $missionData = [];
        $taskMapper = Analogue::mapper(Task::class);
        $task = $taskMapper->query()->find($task_id);
        if($task != null){
            $missionMapper = Analogue::mapper(Mission::class);
            $mission = $missionMapper->query()->where('task_id', $task_id)->first();
            $allScreenshots = [];
            $allCategories = [];
            if($mission != null){
                $screenshots = $mission->getScreenshots();
                foreach ($screenshots as $screen){
                    array_push($allScreenshots, $screen->getUrl());
                }
                $categories = $mission->getBlocklyCategory();
                foreach ($categories as $category){
                    array_push($allCategories, $category->getName());
                }
                array_push($missionData, [
                    'image' => $mission->getIconUrl(),
                    'title' => $mission->getName(),
                    'description' => $mission->getDescription(),
                    'description_speech' => $mission->getDescriptionSpeech(),
                    'categories' => $allCategories,
                    'screenshots' => $allScreenshots,
                    'type' => 'mission',
                ]);
            }
            else {
                $mission = $task->getMissionsHtml()->first();
                if($mission != null) {
                    array_push($missionData, [
                        'image' => $mission->getIconUrl(),
                        'name' => $mission->getTitle(),
                        'type' => "html_mission"
                    ]);
                }
                else {
                    $quiz = $task->getQuizzes()->first();
                    array_push($missionData, [
                        'name' => $quiz->getTitle(),
                        'image' => $quiz->geticonURL(),
                        'type' => $quiz->getType()->getName(),
                    ]);
                }
            }
        }
        return $missionData;
    }


}
