<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;
use Config;

class LTIVerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];


    public function handle($request, Closure $next)
    
    {
        if (
        	isset($request->relaunch_url)||// for first request in lti 1.1.2
            //!isset($request->platform_state)|| // for launch request in lti 1.1.1
            $this->isReading($request) ||
            $this->runningUnitTests() ||
            $this->inExceptArray($request) ||
            $this->tokensMatch($request)
        ) {
            return tap($next($request), function ($response) use ($request) {
                if ($this->shouldAddXsrfTokenCookie()) {
                    $this->addCookieToResponse($request, $response);
                }
            });
        }else{
            $urlString=str_replace("/index", "",Config::get('services.frontend.url')).'d2l?error=true&message=Please upgrade to LTI version 1.1.2.It\'s safer';
            return redirect($urlString);
        }

        throw new TokenMismatchException('CSRF token mismatch.');
    }
}
