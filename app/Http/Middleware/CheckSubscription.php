<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Carbon\Carbon;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Framework\Exceptions\CustomException;

class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    //region Properties
    private $accountService;
    private $accountRepository;
    //endregion

    //region Constructor
    public function __construct(IAccountMainService $accountService,IAccountMainRepository $accountRepository){

        $this->accountService = $accountService;
        $this->accountRepository = $accountRepository;
    }
    //endregion
    
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::toUser();
        $user = $this->accountRepository->getUserById($user->id);
        if($user == null)
            throw new BadRequestException("User is missing");

        $account = $user->getAccount();
        if($account == null)
            throw new BadRequestException("User account not found");

        if($account->getStatus() == 'locked')
            throw new CustomException("You don't have access to this section yet. Please activate your account",297);

        if(count($this->accountRepository->getActiveSubscriptions($account->getId())) > 0) {
            return $next($request);
        }

        $this->accountService->handleSubscriptionEnd($user);
        return $next($request);
    }

}
