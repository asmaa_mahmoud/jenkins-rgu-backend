<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mission';
    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Played()
    {
        return $this->hasMany('App\Gees\MissionProgress','Mission_Id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Adventures()
    {
        return $this->belongsTo('App\Gees\Adventure','Adventure_id');
    }
}
