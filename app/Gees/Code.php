<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table = 'distributor_account_codes';
    public $timestamps = false;
}
