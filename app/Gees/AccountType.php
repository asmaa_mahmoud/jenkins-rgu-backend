<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    protected $table = 'accounttype';
    public $timestamps = false;
}
