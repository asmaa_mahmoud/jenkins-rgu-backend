<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class SchoolPlanSpecial extends Model
{
    protected $table = 'schools_plans_special';

    public function Plan()
    {
        return $this->belongsTo('App\Gees\Plan','plan_id');
    }
}
