<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class ClassRoomMembers extends Model
{
    protected $table = 'classroommembers';
}
