<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    public $timestamps = false;

    public function User()
    {
        return $this->belongsToMany('App\Gees\User','userrole','Role_Id','User_Id');
    }
}
