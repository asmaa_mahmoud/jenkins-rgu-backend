<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'apps_countries';
    //public $timestamps = true;
    
    public function Distributors()
    {
        return $this->hasMany('App\Gees\Distributor','country_id');
    }
}
