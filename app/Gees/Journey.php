<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Journey extends Model
{
    protected $table = 'journey';

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Adventures()
    {
        return $this->hasMany('App\Gees\Adventure','Journey_Id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Classroom()
    {
        return $this->belongsToMany('App\Gees\Classroom','classroomjourneys','journey_id','classroom_id');
    }
}
