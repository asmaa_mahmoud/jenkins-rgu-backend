<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    public $timestamps = false;

    public function Role()
    {
        return $this->belongsToMany('App\Gees\Role','userrole','User_Id','Role_Id');
    }

    public function Classroom()
    {
        return $this->belongsToMany('App\Gees\Classroom','classroommembers','user_id','classroom_id');
    }

    public function Grade()
    {
        return $this->belongsToMany('App\Gees\Grade','studentsgrades','user_id','grade_id');
    }
}
