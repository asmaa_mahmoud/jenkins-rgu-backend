<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'classroomgrades';

    public function User()
    {
        return $this->belongsToMany('App\Gees\User', 'studentsgrades','grade_id','user_id');
    }

    public function Classroom()
    {
        return $this->belongsToMany('App\Gees\Classroom','classroom_grades','grade_id','classroom_id');
    }
}
