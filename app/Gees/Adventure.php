<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Adventure extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'adventure';

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Missions()
    {
        return $this->hasMany('App\Gees\Mission','Adventure_id');
    }
    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Journeys()
    {
        return $this->belongsTo('App\Gees\Journey','Journey_Id');
    }

}
