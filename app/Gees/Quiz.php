<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quiz';
    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function QuizDetails()
    {
        return $this->hasMany('App\Gees\QuizProgress','Quiz_Id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Adventures()
    {
        return $this->belongsTo('App\Gees\Adventure','Adventure_id');
    }
}
