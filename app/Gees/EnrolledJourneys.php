<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class EnrolledJourneys extends Model
{
    protected $table = 'enrolledjourneys';
    public $timestamps = false;
}
