<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $table = 'classroom';
    public $timestamps = true;

    public function Members()
    {
        return $this->belongsToMany('App\Gees\User','classroommembers','classroom_id','user_id');
    }

    public function Journeys()
    {
        return $this->belongsToMany('App\Gees\Journey','classroomjourneys','classroom_id','journey_id');
    }

    public function Grade()
    {
        return $this->belongsToMany('App\Gees\Grade','classroom_grades','classroom_id','grade_id');
    }

}
