<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class MissionProgress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'missionprogress';

    public $timestamps = false;

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function MissionPlayed()
    {
        return $this->belongsTo('App\Gees\Mission','Mission_Id');
    }
}
