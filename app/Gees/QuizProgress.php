<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class QuizProgress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quizprogress';

    public $timestamps = false;

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Quiz()
    {
        return $this->belongsTo('App\Gees\Quiz','Quiz_Id');
    }
}
