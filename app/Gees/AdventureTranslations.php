<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class AdventureTranslations extends Model
{
    protected $table = 'adventure_translations';
}
