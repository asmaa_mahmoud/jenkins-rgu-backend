<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class JourneyTranslation extends Model
{
    protected $table = 'journey_translations';
}
