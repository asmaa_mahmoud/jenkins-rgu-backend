<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    protected $table = 'distributors';
    //public $timestamps = true;
    
    public function Country()
    {
        return $this->belongsTo('App\Gees\Country','country_id');
    }
}
