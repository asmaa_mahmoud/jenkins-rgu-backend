<?php

namespace App\Gees;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';
    public $timestamps = true;

    public function Journey()
    {
        return $this->belongsToMany('App\Gees\Journey','enrolledjourneys','subscription_id','journey_id');
    }
}
