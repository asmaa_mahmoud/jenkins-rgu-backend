<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class Submission extends Notification implements ShouldQueue, ShouldBroadcast
{
    use Queueable, SerializesModels;

    protected $_id;
    protected $course_name;
    protected $course_id;
    protected $round_id;
    protected $lesson_name;
    protected $lesson_id;
    protected $round_lesson_id;
    protected $activity_id;
    protected $submission_no;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($id, $data)
    {
        $this->_id = $id;
        $this->course_name = $data['course_name'];
        $this->course_id = $data['course_id'];
        $this->round_id = $data['round_id'];
        $this->lesson_name = $data['lesson_name'];
        $this->lesson_id = $data['lesson_id'];
        $this->round_lesson_id = $data['round_lesson_id'];
        $this->activity_id = $data['activity_id'];
        $this->submission_no = $data['submission_no'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

//    public function toBroadcast($notifiable)
//    {
//        return new BroadcastMessage([
//            'course_name' => $this->campus_name,
//            'lesson_name' => $this->class_name,
//            'submission_no' => $this->submission_no
//        ]);
//    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            '_id' => $this->_id,
            'course_name' => $this->course_name,
            'course_id' => $this->course_id,
            'round_id' => $this->round_id,
            'lesson_name' => $this->lesson_name,
            'lesson_id' => $this->lesson_id,
            'round_lesson_id' => $this->round_lesson_id,
            'activity_id' => $this->activity_id,
            'submission_no' => $this->submission_no
            ];
    }
}
