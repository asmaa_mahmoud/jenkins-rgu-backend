<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;

class Session extends Notification implements ShouldQueue, ShouldBroadcast
{
    use Queueable, SerializesModels;

    protected $event;
    protected $course_name;
    protected $course_id;
    protected $round_id;
    protected $lesson_name;
    protected $lesson_id;
    protected $round_lesson_id;
    protected $activity_id;
    protected $room_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->event = $data['event'];
        $this->course_name = $data['course_name'];
        $this->course_id = $data['course_id'];
        $this->round_id = $data['round_id'];
        $this->lesson_id = $data['lesson_id'];
        $this->lesson_name = $data['lesson_name'];
        $this->round_lesson_id = $data['round_lesson_id'];
        $this->activity_id = $data['activity_id'];
        $this->room_id = $data['room_id'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'event' => $this->event,
            'course_name' => $this->course_name,
            'course_id' => $this->course_id,
            'round_id' => $this->round_id,
            'lesson_name' => $this->lesson_name,
            'lesson_id' => $this->lesson_id,
            'round_lesson_id' => $this->round_lesson_id,
            'activity_id' => $this->activity_id,
            'room_id' => $this->room_id,
        ];
    }
}
