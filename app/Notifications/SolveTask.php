<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class SolveTask extends Notification implements ShouldQueue, ShouldBroadcast
{
    use Queueable, SerializesModels;

    protected $_id;
    protected $course_name;
    protected $course_id;
    protected $round_id;
    protected $class_name;
    protected $class_id;
    protected $activity_id;
    protected $task_type;
    protected $task_id;
    protected $task_name;
    protected $mission_id;
    protected $username;
    protected $date;
    //protected $task_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($id, $notificationDto)
    {
        $this->_id = $id;
        $this->course_name = $notificationDto['course_name'];
        $this->course_id = $notificationDto['course_id'];
        $this->round_id = $notificationDto['round_id'];
        $this->class_name = $notificationDto['class_name'];
        $this->class_id = $notificationDto['class_id'];
        $this->activity_id = $notificationDto['activity_id'];
        $this->task_type = $notificationDto['task_type'];
        $this->task_id = $notificationDto['task_id'];
        $this->task_name = $notificationDto['task_name'];

        $this->mission_id = $notificationDto['mission_id'];
        $this->username = $notificationDto['username'];
        $this->date = $notificationDto['date'];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            '_id' => $this->_id,
            'course_name' => $this->course_name,
            'course_id' => $this->course_id,
            'round_id' => $this->round_id,
            'class_name' => $this->class_name,
            'class_id' => $this->class_id,
            'task_name' => $this->task_name,
            'task_id' => $this->task_id,
            'activity_id' => $this->activity_id,
            'task_type' => $this->task_type,
            'mission_id' => $this->mission_id,
            'username' => $this->username,
            'date' => $this->date,
        ];
    }
}
