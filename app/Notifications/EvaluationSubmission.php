<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class EvaluationSubmission extends Notification implements ShouldQueue, ShouldBroadcast
{
    use Queueable, SerializesModels;

    protected $_id;
    protected $course_name;
    protected $course_id;
    protected $round_id;
    protected $lesson_name;
    protected $lesson_id;
    protected $round_lesson_id;
    protected $activity_id;
    protected $username;
    protected $date;
    protected $task_name;
    protected $task_type;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($id, $notificationDto)
    {
        $this->_id = $id;
        $this->course_name = $notificationDto['course_name'];
        $this->course_id = $notificationDto['course_id'];
        $this->round_id = $notificationDto['round_id'];
        $this->lesson_name = $notificationDto['lesson_name'];
        $this->lesson_id = $notificationDto['lesson_id'];
        $this->round_lesson_id = $notificationDto['round_lesson_id'];
        $this->activity_id = $notificationDto['activity_id'];
        $this->username = $notificationDto['username'];
        $this->date = $notificationDto['date'];
        $this->task_name = $notificationDto['task_name'];
        $this->task_type = $notificationDto['task_type'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            '_id' => $this->_id,
            'course_name' => $this->course_name,
            'course_id' => $this->course_id,
            'round_id' => $this->round_id,
            'lesson_name' => $this->lesson_name,
            'lesson_id' => $this->lesson_id,
            'round_lesson_id' => $this->round_lesson_id,
            'activity_id' => $this->activity_id,
            'username' => $this->username,
            'date' => $this->date,
            'task_name' => $this->task_name
        ];
    }
}
