<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/17/2018
 * Time: 2:06 PM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisCategoryDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionTranslationDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisUpCategoryDto;
use App\ApplicationLayer\Professional\Dtos\CategoryAnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\CategoryAnalysisQuestionDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;

class RoboscaleService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
    }

    public  function getCategoryQuestions($category_id){
//        $student = $this->accountRepository->getUserById($user_id);
//        if($student == null)
//            throw new BadRequestException(trans('locale.user_not_exist'));

        $questions= $this->professionalRepository->getCategoryQuestions($category_id);
        $category= $this->professionalRepository->getCategoryById($category_id);
        if($category==null)
            throw new BadRequestException('locale.category_not_exist');//TODO::translations

        //THIS  would have been executed by calling $this->getQuestion() unless not same dtos returned
//        $categoryMapped=Mapper::MapClass(AnalysisCategoryDto::class,$category);
//        $startingQuestion= $this->professionalRepository->getQuestion($categoryMapped->startingQuestionId);
//
//        if($startingQuestion==null)
//            throw new BadRequestException('locale.starting_question_not_exist');//TODO::translations
//
//
//        $startingQuestionMapped=Mapper::MapClass(CategoryAnalysisQuestionDto::class,
//            $startingQuestion, [CategoryAnalysisAnswerDto::class]);
//        $categoryDto['startingQuestion'] =$startingQuestionMapped;
//        $categoryDto['categoryDescription'] =$categoryMapped->categoryDescription;


//        $questionsMapped=Mapper::MapEntityCollection(CategoryAnalysisQuestionDto::class,
//            $questions,[CategoryAnalysisAnswerDto::class]);

        $questionsMapped=Mapper::MapEntityCollection(CategoryAnalysisQuestionDto::class,
            $questions,[CategoryAnalysisAnswerDto::class]);

        if($category->is_random){
            shuffle($questionsMapped);
        }else{
            usort($questionsMapped, function ($a, $b){
                return $a->order - $b->order;
            });
        }

        return $questionsMapped;
    }
    public  function getAllCategories(){
//        $student = $this->accountRepository->getUserById($user_id);
//        if($student == null)
//            throw new BadRequestException(trans('locale.user_not_exist'));
        $categories= $this->professionalRepository->getAllCategories();
        //dd($categories);
        $allCategoriesMapped=Mapper::MapEntityCollection(AnalysisUpCategoryDto::class,$categories,[AnalysisCategoryDto::class]);
        return $allCategoriesMapped;

    }
}