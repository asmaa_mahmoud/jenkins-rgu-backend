<?php

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\AnalysisAnswer;
use App\DomainModelLayer\Professional\AnalysisQuestion;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;

use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;


class AnswerService {
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
    }

    public function getAllAnswers($user_id){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $this->accountRepository->beginDatabaseTransaction();

        $answers= $this->professionalRepository->getAllAnswers();

        $this->accountRepository->commitDatabaseTransaction();
        $answersMapped = Mapper::MapEntityCollection(AnalysisAnswerDto::class,
            $answers,[AnalysisQuestionDto::class,AnalysisQuestionDto::class]);
        return $answersMapped;
    }

    public function getAnswer($answer_id){
//        $student = $this->accountRepository->getUserById($user_id);
//        if($student == null)
//            throw new BadRequestException(trans('locale.user_not_exist'));
        $this->accountRepository->beginDatabaseTransaction();
        $answer= $this->professionalRepository->getAnswer($answer_id);
        if($answer == null)
            throw new BadRequestException(trans('locale.answer_not_exist'));

        $this->accountRepository->commitDatabaseTransaction();

        $answerMapped = Mapper::MapClass(AnalysisAnswerDto::class,
            $answer, [AnalysisQuestionDto::class, AnalysisQuestionDto::class]);
        return $answerMapped;
    }


}
