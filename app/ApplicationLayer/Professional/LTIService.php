<?php

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\AnalysisAnswer;
use App\DomainModelLayer\Professional\AnalysisQuestion;
use App\DomainModelLayer\Professional\CampusUser;
use App\DomainModelLayer\Professional\CampusUserStatus;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;

use App\Framework\Exceptions\BadRequestException;

use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\HttpMethods;
use App\Helpers\LTI;
use App\Helpers\ResponseObject;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use IMSGlobal\LTI\ToolProvider\Outcome;
use IMSGlobal\LTI\ToolProvider\ResourceLink;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use IMSGlobal\LTI\ToolProvider;
use IMSGlobal\LTI\ToolProvider\Service;
use Config;
use App\ApplicationLayer\Common\LessonCommonService;
//use App\Helpers\HttpMethods;
use DB;

class LTIService {
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;

        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
    }

    public function routeConsumerUsers($consumer_id,$userDto,$round_lti_id){

        $consumer=$this->professionalRepository->getLTIConsumerById($consumer_id);
        $account=$consumer->getAccount();
        //return "consumer";

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $user_lti_id=$userDto->user_lti_id;
        $user=$this->professionalRepository->getUserByLtiId($user_lti_id);
        $round=$this->professionalRepository->getRoundByLtiId($round_lti_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));



        if($user==null){
            $username=($userDto->username!=='')?$userDto->username:$userDto->fname.'_'.$userDto->lname;
            $user_password='123456';
            $existingUsername = $this->accountRepository->isUserNameAlreadyExist($username);
            if ($existingUsername != null){
                $username=Carbon::now()->timestamp.$username;
            }

            if(isset($userDto->email) && $userDto->email != null  && $userDto->email != "dummymobile@robogarden.ca") {
                $existingUserMail = $this->accountRepository->isAlreadyRegistered($userDto->email);
                if ($existingUserMail != null)
                    throw new BadRequestException(trans('locale.user_email_exists'));
            }
            //dd($userDto);
            $userDto->username=$username;
            $userDto->password=$user_password;
            $userDto->age=null;
            $this->accountRepository->beginDatabaseTransaction();
            $makeNewAccount=true;
            if(in_array('Administrator',$userDto->roles)){
                $user=$this->professionalRepository->checkIfSchoolAccountHasAdmin($account->getId());
                if($user!==null) {
                    $makeNewAccount = false;
                    if($user->getUserLTIId()==null){
                        $user->setUserLTIId($userDto->user_lti_id);
                        $this->accountRepository->storeUser($user);
                    }
                }
                else
                    $role = $this->accountRepository->getRoleByName('school_admin');

            }else if(in_array('Instructor',$userDto->roles)){
                $role = $this->accountRepository->getRoleByName('teacher');
            }else if(in_array('Learner',$userDto->roles)||in_array('Student',$userDto->roles)){
                $role = $this->accountRepository->getRoleByName('student');

                $round->setNoOfStudents($round->getNoOfStudents()+1);
                $this->professionalRepository->storeCampusRound($round);
            }
            if($makeNewAccount) {

                $user = new User($userDto, $account);

                $user->addRole($role);

                $this->accountRepository->AddUser($user);
                $this->accountRepository->commitDatabaseTransaction();
                $user = $this->accountRepository->getUserByUsername($username);
            }




        }
        else{

        }
        $checkStudentInRound=$this->professionalRepository->getCampusRoundUser($round->id,$user->getId());
        if($checkStudentInRound==null){
            //enrole user
            $campUser = new CampusUser($round, $user, $user->getRoles()->first());
            $this->professionalRepository->storeCampusRoundUser($campUser);
            if($user->isStudent()){

                //$campUserStatus = new CampusUserStatus($user->id, $campus->id,0,1);
                //$this->professionalRepository->storeCampusUserStatus($campUserStatus);
                //$this->lessonCommonService->updateCampusUserStatus($studentId,$campusRound->getCampus_id(),0,0);
            }
            
        }
        try {
            //dd(static::where('username',$credentials->username)->first());
            if (! $token = JWTAuth::fromUser($user)) {
                if($user->social_id != null && $user->social_track != null)
                {
                    if (!$token=JWTAuth::fromUser($user)) {
                        throw new UnauthorizedException(trans('locale.wrong_credentials'));
                    }
                }else{
                    throw new UnauthorizedException(trans('locale.wrong_credentials'));
                }
            }
        } catch (JWTException $e) {
            throw new BadRequestException("could_not_create_token");
        }
        

        $response = new ResponseObject();
        $response_campus = new ResponseObject();
        $response->token =(compact('token'));
        $response->user = $user;
        $response->user->role= $user->getRoles()->first()['name'];
        $response->RoboPalAuth = $this->authenticateRobopal($response->user->getId());
        $response->status = $response->user->getAccount()->getStatus();
        $account = $response->user->getAccount();
        $response->use_stripe = $account->isStripeAccount();
        $response->account_type = $response->user->getAccount()->getAccountType()->getName();
        $response->invited = false;
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $response->trialEnd = '1970-01-01 00:00:00';

        if (count($subscriptions) != 0) {
            $subscription = $subscriptions->last();
            $stripe_subscription = $subscription->getStripeSubscriptions()->first();
            if ($stripe_subscription != null)
                $response->trialEnd = $this->accountRepository->getTrialEnd($stripe_subscription);

            $invitation_subscription = $subscription->getInvitationSubscription()->first();
            if($invitation_subscription != null)
                $response->invited = true;
        }
        $lastSubscription = $this->accountRepository->getLastSubscriptionById($account->getId());
        $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
        if($invitationSubscription != null)
            $response->invited = true;


        $response_campus->round_id=$round->getId();
        $response_campus->campus_id=$round->getCampus_id();
        $response->status_code = 200;
        $response->Round = $response_campus;
        $new_response = new ResponseObject();
        $new_response->Object = $response;
        $new_response->status_code = $response->status_code;
        $new_response->error_source = $response->error_source;
        $new_response->errorMessage = $response->errorMessage;
        $this->accountRepository->commitDatabaseTransaction();
        //dd( $response);
        $responseobject = new ResponseObject();
        $responseobject->Object = $response;
        $responseobject->status_code = $response->status_code;
        //$headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type";
        // dd($responseobject);
        $login_data= Response::json($responseobject,$responseobject->status_code);

        return $login_data;

        $fields['login_data']=$login_data;
        $url = config('app.frontend_url_without_index');
        $client=new Client([
            'base_uri'=>$url
        ]);

        try {

            //return ['cookie' => $cookie];

            /*$response = $client->get('/d2l', [
                'form_params' => $fields
            ]);*/

            $response=HttpMethods::post($url.'postd2l',$fields);
            //return $response;
            //dd("response",$response);
        } catch (ClientException $e) {
            dd($e);
            $res = json_decode($e->getResponse()->getBody()->getContents(), true);
            return response($res, $res['status_code']);
        } catch (\Exception $exception) {
            dd($exception);
            //report($exception);
            return $exception->getResponse();
        }

    }
    public function routeConsumerUsers2($userDto,$round_lti_id,$request_data){
        $redirectData=[];
        try {

            $consumer=$this->professionalRepository->getLTIConsumerByKey($request_data["oauth_consumer_key"]);
            $account=$consumer->getAccount();
            if($account==null) {
                $redirectData['error']='true';
                $redirectData['message']=trans('locale.no_account_customer');
                $redirectData['url']=Config::get('services.frontend.url').'d2l';
               
            }

            if($account->getAccountType()->getName() != 'School') {

                $redirectData['error']='true';
                $redirectData['message']=trans('locale.not_school_account');
                $redirectData['url']=Config::get('services.frontend.url').'d2l';

            }
            $school = $account->getSchool();
            if($school == null) {
                $redirectData['error']='true';
                $redirectData['message']=trans('locale.no_school_attached');
                $redirectData['url']=Config::get('services.frontend.url').'d2l';
            }


            $user_lti_id=$userDto->user_lti_id;
            $user=$this->professionalRepository->getUserByLtiId($user_lti_id);

            if($round_lti_id!==null){
                $round=$this->professionalRepository->getRoundByLtiId($round_lti_id);
                if($round == null) {
                    $redirectData['error']='true';
                    $redirectData['message']=trans('locale.campus_round_not_exist');
                    $redirectData['url']=Config::get('services.frontend.url').'d2l';
                }

                if(isset($request_data['resource_link_id'])){
                    $round->setLTIResourceLinkId($request_data['resource_link_id']);
                    $this->professionalRepository->storeCampusRound($round);
                }
            }

            if($user==null){
                $username=($userDto->username!=='')?$userDto->username:$userDto->fname.'_'.$userDto->lname;
                $user_password='123456';
                $existingUsername = $this->accountRepository->isUserNameAlreadyExist($username);
                if ($existingUsername != null){
                    $username=Carbon::now()->timestamp.$username;
                }

                if(isset($userDto->email) && $userDto->email != null  && $userDto->email != "dummymobile@robogarden.ca") {
                    $existingUserMail = $this->accountRepository->isAlreadyRegistered($userDto->email);
                    if ($existingUserMail != null) {
                        $redirectData['error']='true';
                        $redirectData['message']=trans('locale.user_email_exists');
                        $redirectData['url']=Config::get('services.frontend.url').'d2l';
                    }
                }

                $userDto->username=$username;
                $userDto->password=$user_password;
                $userDto->age=null;
                $this->accountRepository->beginDatabaseTransaction();
                $makeNewAccount=true;

                if(in_array('Administrator',$userDto->roles)){

                    $user=$this->professionalRepository->checkIfSchoolAccountHasAdmin($account->getId());
                    if($user!==null) {
                        $makeNewAccount = false;
                        if($user->getUserLTIId()==null){
                            $user->setUserLTIId($userDto->user_lti_id);
                            $this->accountRepository->storeUser($user);
                        }
                    }
                    else{
                        $role = $this->accountRepository->getRoleByName('school_admin');
                        
                    }

                }
                else if(in_array('Instructor',$userDto->roles)){
                    $role = $this->accountRepository->getRoleByName('teacher');
                }else if(in_array('Learner',$userDto->roles)||in_array('Student',$userDto->roles)){
                    $role = $this->accountRepository->getRoleByName('student');


                }else{
                    return redirect(str_replace("/index", "",Config::get('services.frontend.url')).'getd2l/?error='."true&message=".trans('locale.role_not_found'));
                }

                if($makeNewAccount) {

                    $user = new User($userDto, $account);
                    $user->addRole($role);

                    $this->accountRepository->AddUser($user);
                    $this->accountRepository->commitDatabaseTransaction();
                    $user = $this->accountRepository->getUserByUsername($username);
                }
            }
            else{

            }
            if($round_lti_id!=null){
                $checkStudentInRound=$this->professionalRepository->getCampusRoundUser($round->id,$user->getId());
                if($checkStudentInRound==null){
                    //enrole user
                    if($user->getRoles()->first()->getName()=="student"){
                        //A setLoginTime request is hit from front 
                        /*if($user->first_login_time ==null)
                        {
                            $user->first_login_time=Carbon::now()->startOfDay()->timestamp;
                            $this->accountRepository->storeUser($user);
                        }*/
                        //check student in running round
                        $rounds = $round->getCampus()->getRounds();
                        $rounds_id = [];
                        foreach ($rounds as $round_){
                            if($round_->getStatus() == 'running' || $round_->getStatus() == 'locked')
                                $rounds_id[] = $round_->getId();
                        }
                        $enroll=$this->professionalRepository->checkIfStudentInRunningRounds($user->getId(),$rounds_id);
                        //dd("enroll",$enroll);
                        if($enroll===false){
                            //dd("here");
                            $campusUserStatus=$this->professionalRepository->getCampusUserStatus($user->id, $round->getCampus_id());
                            if($campusUserStatus==null){
                                //weird bug these lines doesn't work
                                //$campusUserStatus = new CampusUserStatus($user->id, $campus->id,0,1);                                //$this->professionalRepository->storeCampusUserStatus($campusUserStatus);
                                DB::table('campus_user_status')->insert(
                                    ['campus_id' => $round->getCampus_id(), 'user_id' => $user->id,'rank_level_id'=>'1','xp'=>'0',
                                    'created_at' => Carbon::now(),'updated_at' => Carbon::now()]
                                );
                            }
                            $campUser = new CampusUser($round, $user, $user->getRoles()->first());
                            $this->professionalRepository->storeCampusRoundUser($campUser);
                            $round->setNoOfStudents($round->getNoOfStudents()+1);
                            $this->professionalRepository->storeCampusRound($round);
                           
                        }else{
                            $round=$enroll;
                        }
                    }else {
                        $campUser = new CampusUser($round, $user, $user->getRoles()->first());
                        $this->professionalRepository->storeCampusRoundUser($campUser);
                    }
                }
            }
           // try {
                
                //dd(static::where('username',$credentials->username)->first());
                if (! $token = JWTAuth::fromUser($user)) {
                    if($user->social_id != null && $user->social_track != null)
                    {
                        if (!$token=JWTAuth::fromUser($user)) {
                            $redirectData['error']='true';
                            $redirectData['message']=trans('locale.wrong_credentials');
                            $redirectData['url']=Config::get('services.frontend.url').'d2l';
                        }
                    }else{
                        $redirectData['error']='true';
                        $redirectData['message']=trans('locale.wrong_credentials');
                        $redirectData['url']=Config::get('services.frontend.url').'d2l';

                    }
                }
            //}
            //dd($redirectData);
            $response = new ResponseObject();
            $response_campus = new ResponseObject();
            $response_score = new ResponseObject();
            $response->token =(compact('token'));
            $response->user = $user;
            $response->user->role= $user->getRoles()->first()['name'];
            $response->user->colorPalette =$user->getAccount()->getSchool()->getColorPalette();
            $response->user->schoolLogoUrl= $user->getAccount()->getSchool()->getSchoolLogoUrl();
            $response->RoboPalAuth = $this->authenticateRobopal($response->user->getId());
            $response->status = $response->user->getAccount()->getStatus();
            $account = $response->user->getAccount();
            $response->use_stripe = $account->isStripeAccount();
            $response->account_type = $response->user->getAccount()->getAccountType()->getName();
            $response->invited = false;
            $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
            $response->trialEnd = '1970-01-01 00:00:00';
            if($response->user->colorPalette!==NULL){
                $colorPalette=$response->user->colorPalette->colors;
                $colorPalette=str_replace("#", "palette_hashed", $colorPalette);
                unset($response->user->account->school->colorPalette);
                $response->user->colorPalette->colors=$colorPalette;
            }

            if (count($subscriptions) != 0) {
                $subscription = $subscriptions->last();
                $stripe_subscription = $subscription->getStripeSubscriptions()->first();
                if ($stripe_subscription != null)
                    $response->trialEnd = $this->accountRepository->getTrialEnd($stripe_subscription);

                $invitation_subscription = $subscription->getInvitationSubscription()->first();
                if($invitation_subscription != null)
                    $response->invited = true;
            }
            $lastSubscription = $this->accountRepository->getLastSubscriptionById($account->getId());
            $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
            if($invitationSubscription != null)
                $response->invited = true;
            if($round_lti_id!==null){
                $response_campus->round_id=$round->getId();
                $response_campus->campus_id=$round->getCampus_id();
            }else{
                 $response_campus->campus_id=null;

            }
            $response->status_code = 200;
            $response->Round = $response_campus;
            if($user->isStudent()){
                $response_score->score=$user->getScores();
                $response->score = $response_score;
            }
            $new_response = new ResponseObject();
            $new_response->Object = $response;
            $new_response->status_code = $response->status_code;
            $new_response->error_source = $response->error_source;
            $new_response->errorMessage = $response->errorMessage;
            $this->accountRepository->commitDatabaseTransaction();
            //dd( $response);
            $responseobject = new ResponseObject();
            $responseobject->Object = $response;
            $responseobject->status_code = $response->status_code;


            $login_data1= Response::json($responseobject,$responseobject->status_code);
            $goToCourses=$round_lti_id!==null?false:true;
            $redirectData['error']='false';
            $redirectData['message']='';
            $redirectData['login_data']=json_encode($login_data1->getData());
            $redirectData['url']=Config::get('services.frontend.url').'d2l';

        }
        catch (ClientException $e) {
            $this->accountRepository->rollbackDatabaseTransaction();//doesn't work here
            $msg=trans('locale.something_wrong');
            $redirectData['error']='true';
            $redirectData['message']=$msg;
            $redirectData['url']=Config::get('services.frontend.url').'d2l';
        } catch (\Exception $exception) {
            $this->accountRepository->rollbackDatabaseTransaction();//doesn't work here
            $msg=trans('locale.something_wrong');
            $redirectData['error']='true';
            $redirectData['message']=$msg;
            $redirectData['url']=Config::get('services.frontend.url').'d2l';
        }catch (JWTException $e) {
                $this->accountRepository->rollbackDatabaseTransaction();//doesn't work here
                $redirectData['error']='true';
                $redirectData['message']="Couldn't create token";
                $redirectData['url']=Config::get('services.frontend.url').'d2l';
        }
        finally {
            $urlString=str_replace("/index", "",Config::get('services.frontend.url')).'d2l?';
            $k=1;
            foreach ($redirectData as $key => $value) {

               if($key!='url'){
                    $urlString.=$key.'='.$value;
                    if($k!=(count($redirectData)-1))
                        $urlString.="&";
               }
               $k++;
            }
            return redirect($urlString);
    
        }

    }

    public function authenticateLTIUser($account_id,$userDto,$round_lti_id){
        $account=$this->accountRepository->getAccountById($account_id);
        if($account== null)
            throw new BadRequestException(trans('locale.account_not_found'));

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $user_lti_id=$userDto->user_lti_id;
        $user=($user_lti_id!==null)?$this->professionalRepository->getUserByLtiId($user_lti_id):null;
        $round=$this->professionalRepository->getRoundByLtiId($round_lti_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        //check round for this account

        if($user==null){
            $username=($userDto->username!=='')?$userDto->username:$userDto->fname.'_'.$userDto->lname;
            $user_password='123456';
            $existingUsername = $this->accountRepository->isUserNameAlreadyExist($username);
            if ($existingUsername != null){
                $username=Carbon::now()->timestamp.$username;
            }

            if(isset($userDto->email) && $userDto->email != null  && $userDto->email != "dummymobile@robogarden.ca") {
                $existingUserMail = $this->accountRepository->isAlreadyRegistered($userDto->email);
                if ($existingUserMail != null)
                    throw new BadRequestException(trans('locale.user_email_exists'));
            }
            //dd($userDto);
            $userDto->username=$username;
            $userDto->password=$user_password;
            $userDto->age=null;
            $this->accountRepository->beginDatabaseTransaction();
            $makeNewAccount=true;
            if(in_array('Administrator',$userDto->roles)){
                $user=$this->professionalRepository->checkIfSchoolAccountHasAdmin($account->getId());
                if($user!==null) {
                    $makeNewAccount = false;
                    if($user->getUserLTIId()==null){
                        $user->setUserLTIId($userDto->user_lti_id);
                        $this->accountRepository->storeUser($user);
                    }
                }
                else
                    $role = $this->accountRepository->getRoleByName('school_admin');

            }else if(in_array('Instructor',$userDto->roles)){
                $role = $this->accountRepository->getRoleByName('teacher');
            }else if(in_array('Learner',$userDto->roles)||in_array('Student',$userDto->roles)){
                $role = $this->accountRepository->getRoleByName('student');

                $round->setNoOfStudents($round->getNoOfStudents()+1);
                $this->professionalRepository->storeCampusRound($round);
            }
            if($makeNewAccount) {

                $user = new User($userDto, $account);

                $user->addRole($role);

                $this->accountRepository->AddUser($user);
                $this->accountRepository->commitDatabaseTransaction();
                $user = $this->accountRepository->getUserByUsername($username);
            }




        }
        else{
            //check user in account
            if($user->getAccount()->getId()!=$account_id)
                throw new BadRequestException(trans("locale.user_not_in_account"));
        }

        $checkStudentInRound=$this->professionalRepository->getCampusRoundUser($round->id,$user->getId());
        if($checkStudentInRound==null){
            //enrole user
            if($user->getRoles()->first()->getName()=="student"){
                //check student in running round
                $rounds = $round->getCampus()->getRounds();

                $rounds_id = [];


                foreach ($rounds as $round){
                    if($round->getStatus() == 'running' || $round->getStatus() == 'locked')
                        $rounds_id[] = $round->getId();
                }
                $enroll=$this->professionalRepository->checkIfStudentInRunningRounds($user->getId(),$rounds_id);
                if($enroll===false){
                    $campUser = new CampusUser($round, $user, $user->getRoles()->first());
                    $this->professionalRepository->storeCampusRoundUser($campUser);
                }else{
                    $round=$enroll;
                }
            }else {
                $campUser = new CampusUser($round, $user, $user->getRoles()->first());
                $this->professionalRepository->storeCampusRoundUser($campUser);
            }
        }
        try {
            //dd(static::where('username',$credentials->username)->first());
            if (! $token = JWTAuth::fromUser($user)) {
                if($user->social_id != null && $user->social_track != null)
                {
                    if (!$token=JWTAuth::fromUser($user)) {
                        throw new UnauthorizedException(trans('locale.wrong_credentials'));
                    }
                }else{
                    throw new UnauthorizedException(trans('locale.wrong_credentials'));
                }
            }
        } catch (JWTException $e) {
            throw new BadRequestException("could_not_create_token");
        }

        $response = new ResponseObject();
        $response_campus = new ResponseObject();
        $response_score = new ResponseObject();
        $response->token =(compact('token'));
        $response->user = $user;
        $response->user->role= $user->getRoles()->first()['name'];
        $response->RoboPalAuth = $this->authenticateRobopal($response->user->getId());
        $response->status = $response->user->getAccount()->getStatus();
        $account = $response->user->getAccount();
        $response->use_stripe = $account->isStripeAccount();
        $response->account_type = $response->user->getAccount()->getAccountType()->getName();
        $response->invited = false;
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $response->trialEnd = '1970-01-01 00:00:00';

        if (count($subscriptions) != 0) {
            $subscription = $subscriptions->last();
            $stripe_subscription = $subscription->getStripeSubscriptions()->first();
            if ($stripe_subscription != null)
                $response->trialEnd = $this->accountRepository->getTrialEnd($stripe_subscription);

            $invitation_subscription = $subscription->getInvitationSubscription()->first();
            if($invitation_subscription != null)
                $response->invited = true;
        }
        $lastSubscription = $this->accountRepository->getLastSubscriptionById($account->getId());
        $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
        if($invitationSubscription != null)
            $response->invited = true;


        $response_campus->round_id=$round->getId();
        $response_campus->campus_id=$round->getCampus_id();
        $response->status_code = 200;
        $response->Round = $response_campus;
        $new_response = new ResponseObject();
        $new_response->Object = $response;
        $new_response->status_code = $response->status_code;
        $new_response->error_source = $response->error_source;
        $new_response->errorMessage = $response->errorMessage;
        $this->accountRepository->commitDatabaseTransaction();
        //dd( $response);
        $responseobject = new ResponseObject();
        $responseobject->Object = $response;
        $responseobject->status_code = $response->status_code;

        $login_data= Response::json($responseobject,$responseobject->status_code);

        return $login_data;


    }

    public function routeD2lLti($consumer_id,$request_data){
        $consumer=$this->professionalRepository->getLTIConsumerById($consumer_id);
        $consumer_name=$consumer->name;

        if(!isset($request_data["roles"])|| !isset($request_data["lti_message_type"])||!isset($request_data["lis_person_name_full"])
            ||!isset($request_data["tool_consumer_instance_name"])||!isset($request_data["resource_link_title"])){
            echo "<div style='color:red;text-align: center'>" . "Required data are missing,please specify sending  them in the LTI link" . "</div>";
        }else {

            //dd($consumer_id,$request_data);
            $roles = explode(",", $request_data["roles"]);
            $lastRole = explode("/", $roles[0]);
            $role = end($lastRole);
            $launchMessage = ($request_data["lti_message_type"] == "basic-lti-launch-request") ? " This is a basic LTI launch request " : "";
            $welcomeMessage = "Hello " . '<strong>' . $request_data["lis_person_name_full"] . '</strong>'
                . '<br> This is from <strong>' . $consumer_name . '</strong> account' .
                '<br> You are redirected from ' . $request_data["tool_consumer_instance_name"]
                . "<br>" . $launchMessage
                . "<br>You are requesting to access  Robogarden as `" . $role . "`<br>" .
                "You want to access `" . $request_data["resource_link_title"] . "` course";

            echo "<div style='margin-top:200px;text-align: center'>" . $welcomeMessage . "</div>";
        }
    }


    public function authenticateRobopal($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            return ["permission" => false, "message" => trans('locale.subscription_ended'), "end_date" => null, "cancelled" => true];

        $subscription = $subscriptions->last();

        $item =  $this->accountRepository->getLastRoboPalItem($subscription->getId());

        if($item != null)
        {
            if($item->getEndDate() == null)
                return ["permission" => true, "message" => trans('locale.subscription_robopal_active'), "end_date" => null , "cancelled" => false];
            elseif($item->getEndDate() > Carbon::now())
            {
                if($account->getAccountType()->getName() == 'School')
                {
                    $distributor_sub = $subscription->getDistributorSubscriptions()->first();
                    if($distributor_sub == null)
                        throw BadRequestException(trans('locale.distributor_not_found'));
                    $use_stripe = $distributor_sub->getDistributor()->getStripeCheck();
                    if(!$use_stripe)
                        return ["permission" => true, "message" => trans('locale.subscription_robopal_end'). " " .$item->getEndDate(),"end_date"=>$item->getEndDate(), "cancelled" => false];
                }
                else
                {
                    return ["permission" => true, "message" => trans('locale.subscription_robopal_end'). " " .$item->getEndDate(),"end_date"=>$item->getEndDate(), "cancelled" => true];
                }
            }
            else
                return ["permission" => false, "message" => trans('locale.subscription_robopal_ended'), "end_date"=>$item->getEndDate(), "cancelled" => true];

        }
        foreach($subscriptions as $subscription){
            $unlockables = $subscription->getPlan()->getPlanUnlockables();
            foreach ($unlockables as $unlockable){
                $extraUnlockable = $unlockable->getUnlockable()->getExtraUnlockable();
                if($extraUnlockable != null){
                    $extraName = $extraUnlockable->getExtra()->getName();
                    if($extraName == "RoboPal")
                        return ["permission" => true, "message" => trans('locale.subscription_robopal_active'), "end_date" => null , "cancelled" => false];
                }
            }
        }

        return ["permission" => false, "message" => trans('locale.subscription_not_include_robopal'), "end_date" => null, "cancelled" => false];
    }

    public function ltiSynchBack(){
        $consumer_key="robogarden-test";
        $resource_link_id="657145911";
        $user_id="ebf22362-61e7-43e1-b5af-3f4d32c65337_5475";

        $lti=new LTI();
        $db_connector=$lti->getDataConnector();

        $consumer = new ToolProvider\ToolConsumer($consumer_key, $db_connector);
        $resource_link = ToolProvider\ResourceLink::fromConsumer($consumer, $resource_link_id);

        $user = ToolProvider\User::fromResourceLink($resource_link, $user_id);
        $outcome = new ToolProvider\Outcome();
        $score="before_init";
        if ($resource_link->doOutcomesService(ToolProvider\ResourceLink::EXT_READ, $outcome, $user)) {
            $score = $outcome->getValue();
        }
        $score_=0.7;
        $outcome = new Outcome($score_);
        $ok = $resource_link->doOutcomesService(ResourceLink::EXT_WRITE, $outcome, $user);
        return ["score_before"=>$score,"score_after"=>$score_,"ok"=>$ok];
    }

}
