<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 10:03 AM
 */

namespace App\ApplicationLayer\Professional;

use App\ApplicationLayer\Common\GradingCommonService;
use App\ApplicationLayer\Common\LessonCommonService;
use App\ApplicationLayer\Professional\Dtos\CampusDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundNamesDto;
use App\ApplicationLayer\Professional\Dtos\CampusStudentDto;
use App\ApplicationLayer\Professional\Dtos\RoomDto;
use App\ApplicationLayer\Professional\Dtos\TeacherCampusRoundDto;
use App\ApplicationLayer\Schools\Dtos\ActivityDto;
use App\ApplicationLayer\Schools\Dtos\StudentCampDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\CampusActivityTask;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusRoundActivityTask;
use App\DomainModelLayer\Professional\CampusRoundClass;
use App\Notifications\CourseCompleted;
use App\DomainModelLayer\Professional\Material;
use App\Notifications\EvaluationSubmission;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Room;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use App\Notifications\SessionStarted;
use App\Notifications\SolveTask;
use App\Notifications\StartTask;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use Illuminate\Support\Facades\Notification;
use Ramsey\Uuid\Uuid;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularFilesDto;


class TeacherService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonCommonService;
    private $gradingCommonService;

    const Attendee_Password = '12345';
    const Moderator_Password = '54321';


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;

        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        $this->gradingCommonService = new GradingCommonService($this->accountRepository, $this->professionalRepository);
    }
    public function tasks_cmp($task1,$task2){
        if ($task1['order'] === $task2['order']) {
            return 0;
        }
        if ($task1['order'] < $task2['order']) {
            return -1;
        }else{
            return 1;
        }
    }

    public function getTeacherCampusRound($user_id, $round_id,$gmt_difference){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translations

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $roundDto = [];
        $classes = [];
        foreach ($round->getCampusRoundClasses() as $roundClass){
            $classDto = [];
            $tasks = [];

            $class = $roundClass->getClass();
            $rooms = $roundClass->getRooms()->toArray();

            $classDto['id'] = $class->getId();
            $classDto['round_class_id'] = $roundClass->getId();
            $classDto['name'] = $class->getDefaultActivity()->getName();
            $classDto['starts_at'] = $roundClass->getStartsAt();
            $classDto['ends_at'] = $roundClass->getEndsAt();
            $classDto['duration'] = $roundClass->getDuration();
            $classDto['weight'] = $roundClass->getWeight();
            $classDto['order'] = $class->getOrder();
            $classDto['sessions'] = $rooms;
            $activity = $class->getDefaultActivity();
            $campusTasks = $this->professionalRepository->getCampusActivityTasks($activity->getId(),$round->getCampus()->getId());
            $tasks=[];
            foreach ($campusTasks as $campusTask){
                $taskDto = [];
                $task = $campusTask->getTask();
                $roundTask = $this->professionalRepository->getRoundActivityTask($round_id, $campusTask->getId());
                $taskDto['task_id'] = $task->getId();
                if($task->getMissions()->first() != null) {
                    $mission = $task->getMissions()->first();
                    $taskDto['id'] = $mission->getId();
                    $taskDto['task_name'] = $mission->getName();
                    $taskDto['task_type'] = "mission";
                    $taskDto['due_date'] = $roundTask->getDueDate();
                    $taskDto['weight'] = $roundTask->getWeight();
                    $tasks[] = $taskDto;
                }
                elseif($task->getMissionCoding() != null) {
                    $mission = $task->getMissionCoding();
                    $taskDto['id'] = $mission->getId();
                    $taskDto['model_answer'] = $mission->getModelAnswer();
                    $taskDto['task_name'] = $mission->getTitle();
                    $taskDto['task_type'] = "coding_mission";
                    $taskDto['due_date'] = $roundTask->getDueDate();
                    $taskDto['weight'] = $roundTask->getWeight();
                    $tasks[] = $taskDto;
                }
                elseif($task->getMissionEditor() != null) {
                    $mission = $task->getMissionEditor();
                    $taskDto['id'] = $mission->getId();
                    $taskDto['model_answer'] = $mission->getModelAnswer();
                    $taskDto['task_name'] = $mission->getTitle();
                    $taskDto['task_type'] = "editor_mission";
                    $taskDto['due_date'] = $roundTask->getDueDate();
                    $taskDto['weight'] = $roundTask->getWeight();
                    $tasks[] = $taskDto;
                } elseif($task->getMissionsHtml()->first() != null && $task->getMissionsHtml()->first()->getHtmlMissionType()=='mini_project'  ) {
                    $mission = $task->getMissionsHtml()->first();
                    $taskDto['id'] = $mission->getId();
                    $taskDto['task_name'] = $mission->getTitle();
                    $taskDto['task_type'] = $mission->getHtmlMissionType();
                    $taskDto['due_date'] = $roundTask->getDueDate();
                    $taskDto['weight'] = $roundTask->getWeight();
                    $tasks[] = $taskDto;
                }
                elseif($task->getQuizzes()->first()!=null){
                    $quiz = $task->getQuizzes()->first();
                    $taskDto['id'] = $quiz->getId();
                    $taskDto['task_name'] = $quiz->getTitle();
                    $taskDto['mission_type'] = $quiz->getType()->getName();
                    $taskDto['due_date'] = $roundTask->getDueDate();
                    $taskDto['weight'] = $roundTask->getWeight();
                    $tasks[] = $taskDto;
                }
                else{
                    //I think we should throw error exception
                }
            }
            $classDto['tasks'] = $tasks;
            $classes[] = $classDto;
        }

        $roundDto['id'] = $round->getId();
        $roundDto['campus_id'] = $round->getCampus()->getId();
        $roundDto['name'] = $round->getCampus()->getName();
        $roundDto['starts_at'] = $round->getStartsAt();
        $roundDto['ends_at'] = $round->getEndsAt();
        $roundDto['duration'] = $round->getDuration();
        $roundDto['status'] = $round->getStatus($gmt_difference);
        $roundDto['students_no'] = $round->getNoOfStudents();
        $roundDto['classes'] = $classes;
        $unSortedClasses = $classes;

        usort($unSortedClasses,array($this, 'tasks_cmp'));
        $roundDto['classes'] = $unSortedClasses;

        return $roundDto;
    }

    public function getTeacherCampuses($user_id,$gmt_difference){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        // $rounds = $this->professionalRepository->getUserCampusRounds($user_id);
        $running_rounds = $this->professionalRepository->getTeacherRunningRounds($user_id,$gmt_difference);
        $upcoming_rounds = $this->professionalRepository->getTeacherUpcomingRounds($user_id,$gmt_difference);

        //$roundsDto = Mapper::MapEntityCollection(CampusRoundDto::class, $rounds);
        //$roundsDto=Mapper::MapEntityCollection(CampusRoundNamesDto::class, $rounds);
        $roundsDto=[];
        foreach ($running_rounds as $round){
            $roundDto['id']=$round->getId();
            $roundDto['campusName']=$round->getCampusName();
            $roundDto['campus_id']=$round->getCampus_id();
            $roundDto['status']=$round->getStatus($gmt_difference);
            $roundDto['round_name']=$round->getRound_name();
            $roundsDto[]=$roundDto;

        }
        foreach ($upcoming_rounds as $round){
            $roundDto['id']=$round->getId();
            $roundDto['campusName']=$round->getCampusName();
            $roundDto['campus_id']=$round->getCampus_id();
            $roundDto['status']=$round->getStatus($gmt_difference);
            $roundDto['round_name']=$round->getRound_name();
            $roundsDto[]=$roundDto;

        }
        return $roundsDto;
    }

    public function getStudentsInRound($user_id, $round_id,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $students = [];
        $campus = $round->getCampus();
        foreach ($round->getStudents() as $student){
            $solvedTasksNum = $this->professionalRepository->getStudentSolvedTasksInCampus($student->getId(), $campus->getId(), true);
            $campusTasksNum = $this->professionalRepository->getAllTasksInCampus($campus->getId(), true);

            $solvedTasks = $this->professionalRepository->getStudentSolvedTasksInCampus($student->getId(), $campus->getId());
            $evaluation = 0;
            foreach ($solvedTasks as $task){
                $eval = $task->getEvaluation();
                $campusActivityTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $task->getTask()->getId());
                $activity = $campusActivityTask->getDefaultActivity();
                $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity->getId());
                $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class->getId());
                $roundActivityTask = $this->professionalRepository->getRoundActivityTask($round_id, $campusActivityTask->getId());

                $eval = $eval * $roundActivityTask->getWeight()/100; //task weight in round
                $eval = $eval * $roundClass->getWeight()/100; //class weight in round
                $evaluation += $eval;
            }
            $evaluation = intval($evaluation);

            $student = Mapper::MapClass(CampusStudentDto::class, $student);
            if($campusTasksNum == 0){
                $student->progress = '0%';
            }
            elseif ($campusTasksNum > 0){
                $student->progress = intval(($solvedTasksNum/$campusTasksNum)*100).'%';
            }
            $student->evaluation = $evaluation.'%';
            $students[] = $student;
        }

        $students=json_decode(json_encode($students), True);
        $students=$this->searchStudents($students,$search);
        return $students;
    }

    public function getStudentsIDsInRound($user_id, $round_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $students_ids = [];
        foreach ($round->getStudents() as $student){

            $students_ids[] = $student->getId();
        }

        return $students_ids;
    }

    public function getTeacherRoundClasses($user_id, $round_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation


        $roundClasses = $round->getCampusRoundClasses();

        $classes = [];

        foreach ($roundClasses as $roundClass){

            $classDto = [];
            $class = $roundClass->getClass();

             // get lesson type
            if($class->lessonType() == 0)
            {  $lessonType="normal";
            }
            elseif($class->lessonType() == 1)
            {  $lessonType="project";
            }
            elseif($class->lessonType() == 2)
            {  $lessonType="welcome";
            }else
            {  $lessonType=$class->lessonType();
            }
            
            //$classDto['is_project'] = $class->isProject();
            $classDto['id'] = $class->getId();
            $classDto['activity_id'] = $class->getDefaultActivity()->getId();
            $classDto['description'] = $class->getDefaultActivity()->getDescription();
            $classDto['icon_url'] = $class->getDefaultActivity()->getIconUrl();
            $classDto['name'] = $class->getDefaultActivity()->getName();
            $classDto['order'] = $class->getOrder();
            $classDto['starts_at'] = $roundClass->getStartsAt();
            $classDto['ends_at'] = $roundClass->getEndsAt();
            $classDto['weight'] = $roundClass->getWeight();
            // $classDto['tasks_no'] = $class->getDefaultActivity()->getTasksCount();
            $classDto['duration'] = $roundClass->getDuration();
            $classDto['time_estimate']=$class->getTimeEstimate();
            $classDto['moduleType'] = $lessonType;
            if($class->lessonType() == 1){
                $classDto['video_url'] = $class->getProject()->getVideoUrl();
            }
            $classes[] = $classDto;
        }

        $unSortedClasses = $classes;

        usort($unSortedClasses,array($this, 'tasks_cmp'));

        return $unSortedClasses;
    }
    public function rooms_cmp($room1,$room2){
        if ($room1['starts_at'] === $room2['starts_at']) {
            return 0;
        }
        if ($room1['starts_at'] < $room2['starts_at']) {
            return -1;
        }else{
            return 1;
        }
    }

    public function getTeacherRoundClass($user_id, $round_id, $class_id,$gmt_difference,$is_default=true){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class_id);
        if($roundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation

        $class = $roundClass->getClass();
        $rooms = $roundClass->getRooms();

        if($is_default)
            $activity = $class->getDefaultActivity();
        else
            $activity = $class->getActivity();

        $tasks = $activity->getTasks();

        $tasks_num=0;
        $html_tasks_num=0;
        foreach ($tasks as $task){
            if($task->getMissions()->first() != null) {
               $tasks_num+=1;
            }
            elseif($task->getMissionsHtml()->first() != null &&$task->getMissionsHtml()->first()->getHtmlMissionType()=='html_mission') {
                $html_tasks_num+=1;
            }
            elseif($task->getMissionsHtml()->first() != null &&$task->getMissionsHtml()->first()->getHtmlMissionType()=='mini_project') {
                $tasks_num+=1;
            }
            elseif($task->getMissionCoding() != null) {
                $tasks_num+=1;
            }
            elseif($task->getMissionEditor() != null) {
                $tasks_num+=1;
            }
            elseif($task->getQuizzes()->first()!=null){
                $tasks_num+=1;
            }
        }

        $bbb = new BigBlueButton();
        $roomsDto=[];
        foreach($rooms as $room){
            $recordingParams = new GetRecordingsParameters();
            $recordingParams->setMeetingId($room->getId());
            $response = $bbb->getRecordings($recordingParams);
            $records = false;
            if ($response->getReturnCode() == 'SUCCESS') {
                if(!empty($response->getRecords()))
                    $records = true;
            }
            array_push($roomsDto,
                ['id'=> $room->getId(),
                    'name'=> $room->getName(),
                    'campus_round_class_id' => $room->getRoundClass()->getId(),
                    'starts_at' => $room->getStartsAt(),
                    'file_id' => $room->getMaterialId() ?? null,
                    'is_running' => $room->isRunning(),
                    'has_records' => $records]
            );
        }
        usort($roomsDto,array($this, 'rooms_cmp'));

        $classDto['name'] = $class->getDefaultActivity()->getName();
        $classDto['roundStatus'] = $round->getStatus($gmt_difference);
        $classDto['id'] = $class->getId();
        $classDto['activity_id'] = $class->getDefaultActivity()->getId();
        $classDto['description'] = $class->getDefaultActivity()->getDescription();
        $classDto['starts_at'] = $roundClass->getStartsAt();
        $classDto['ends_at'] = $roundClass->getEndsAt();
        $classDto['weight'] = $roundClass->getWeight();
        $classDto['tasks_no'] = $tasks_num;
        $classDto['html_tasks_no'] = $html_tasks_num;
        $classDto['icon_url'] = $class->getDefaultActivity()->getIconUrl();
        $classDto['rooms'] = $roomsDto;
        $classDto['round_name'] = $round->getRound_name();

        return $classDto;
    }

    public function getRoundClassSubmissions($user_id, $round_id, $activity_id, $start_from, $limit, $search,$order_by, $is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $db_order_by=null;
        if($order_by=='evaluation')
            $db_order_by=$order_by;


        $classSubmissions = $this->professionalRepository->getClassSubmissions($round->getCampus()->getId(), $round_id, $activity_id,$limit, $search,$db_order_by, $is_default);

        $submissions = [];

        foreach ($classSubmissions as $submission){
            $obj = [];
            $task = $submission->getTask();
            $obj['submission_id'] = $submission->getId();
            $obj['task_id'] = $task->getId();
            if($task->getMissions()->first() != null) {
                $obj['task_name'] = $task->getMissions()->first()->getName();
                $obj['mission_id'] = $task->getMissions()->first()->getId();
                $obj['task_type'] = 'mission';
            }
            elseif($task->getMissionCoding() != null) {
                $obj['task_name'] = $task->getMissionCoding()->getTitle();
                $obj['mission_id'] = $task->getMissionCoding()->getId();
                $obj['task_type'] = 'coding_mission';
            }  
            elseif($task->getMissionsHtml()->first() != null) {
                if($task->getMissionsHtml()->first()->getIsMiniProject()=='1' ) {
                    $obj['task_name'] = $task->getMissionsHtml()->first()->getTitle();
                    $obj['mission_id'] = $task->getMissionsHtml()->first()->getId();
                    $obj['task_type'] = 'mini_project';
                }
            }
            elseif($task->getMissionEditor() != null){
                $obj['task_name'] = $task->getMissionEditor()->getTitle();
                $obj['mission_id'] = $task->getMissionEditor()->getId();
                $obj['task_type'] = 'editor_mission';
            }
            elseif($task->getMissionAngular() != null){
                $obj['task_name'] = $task->getMissionAngular()->getTitle();
                $obj['mission_id'] = $task->getMissionAngular()->getId();
                $obj['task_type'] = 'angular_mission';
            }
            elseif($task->getQuizzes()->first()!=null){
                $obj['task_name'] = $task->getQuizzes()->first()->getTitle();
                $obj['mission_id'] = $task->getQuizzes()->first()->getId();
                $obj['task_type'] = $task->getQuizzes()->first()->getType()->getName();
            }
            if($is_default)
            {
                $activity = $submission->getDefaultActivity();
            }else{
                $activity = $submission->getActivity();
            }

            $obj['student_id'] = $submission->getUser()->getId();
            $obj['student_name'] = $submission->getUser()->getUserName();
            $obj['student_full_name'] = $submission->getUser()->getName();
            $obj['code'] = $submission->getUserCode();
            $obj['evaluation'] = $submission->getEvaluation();
            $obj['is_evaluated'] = $submission->getIsEvaluated();
            // $campusActivityTask = $this->professionalRepository->getCampusActivityTask($round->getCampus()->getId(), $task->getId());
            // $campusRoundActivityTask = $this->professionalRepository->getRoundActivityTask($round_id, $campusActivityTask->getId());
            $class = $this->professionalRepository->getCampusClass($submission->getCampus()->getId(),$activity->getId(), $is_default);
            $campusRoundClass = $this->professionalRepository->getCampusRoundClass($round->getId(), $class->getId());
            $on_time = \Carbon\Carbon::parse($submission->getCreatedAt())->timestamp;
            $obj['on_time'] = $on_time ;
            $obj['due_date'] = $campusRoundClass->getDueDate();
            $obj['is_late'] = $submission->getUpdatedAt() > $obj['due_date'] ? true : false;

            array_push($submissions, $obj);
        }

        return $this->sortAndSearchSubmissions($submissions,$order_by,$search);
    }

    public function sortAndSearchSubmissions($submissions,$orderBy=null,$search=null){

        $subs=[];
        if($search!=null) {
            foreach ($submissions as $submission){
                if (strpos(strtolower($submission['task_name']), strtolower($search)) !== false   ||
                    strpos(strtolower($submission['student_name']), strtolower($search)) !== false||
                    strpos(strtolower($submission['evaluation']), strtolower($search)) !== false) {

                    array_push($subs,$submission);

                }

            }
        }
        if($search===null)
            $sortedSubmissions=$submissions;
        else
            $sortedSubmissions=$subs;

        $sorted = false;
        if($orderBy!=null) {
            while (false === $sorted) {
                $sorted = true;
                for ($i = 0; $i < count($sortedSubmissions) - 1; ++$i) {
                    $current = $sortedSubmissions[$i];
                    $next = $sortedSubmissions[$i + 1];
                    if ($orderBy == 'task_name') {
                        if (strcmp($current['task_name'], $next['task_name']) > 0) {
                            //swap
                            $sortedSubmissions[$i] = $next;
                            $sortedSubmissions[$i + 1] = $current;
                            $sorted = false;
                        }

                    }
                    else if ($orderBy == 'student_name') {

                        if (strcmp($current['student_name'], $next['student_name']) > 0) {
                            $sortedSubmissions[$i] = $next;
                            $sortedSubmissions[$i + 1] = $current;
                            $sorted = false;

                        }

                    }

                }
            }
        }

        return $sortedSubmissions;
    }
    public function searchStudents($students,$search=null){

        $subs=[];
        if($search!=null) {
            foreach ($students as $student){
                if (strpos(strtolower($student['username']), strtolower($search)) !== false ) {

                    array_push($subs,$student);

                }

            }
        }
        if($search===null)
            $searcheStudents=$students;
        else
            $searcheStudents=$subs;



        return $searcheStudents;
    }

    public function getRoundClassSubmissionsCount($user_id, $round_id, $activity_id,$search,$is_default=true){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation


        $classSubmissions = $this->professionalRepository->getClassSubmissions($round->getCampus()->getId(),$round_id,$activity_id,null,null ,null, $is_default);

        $submissions = [];

        foreach ($classSubmissions as $submission){
            $obj = [];
            $task = $submission->getTask();
            $obj['submission_id'] = $submission->getId();
            $obj['task_id'] = $task->getId();
            if($task->getMissions()->first() != null) {
                $obj['task_name'] = $task->getMissions()->first()->getName();
                $obj['task_type'] = 'mission';
            }
            elseif($task->getMissionsHtml()->first() != null) {
                $obj['task_name'] = $task->getMissionsHtml()->first()->getTitle();
                $obj['task_type'] = 'html_mission';
            }
            elseif($task->getMissionCoding() != null) {
                $obj['task_name'] = $task->getMissionCoding()->getTitle();
                $obj['task_type'] = 'coding_mission';
            }
            elseif($task->getMissionEditor() != null){
                $obj['task_name'] = $task->getMissionEditor()->getTitle();
                $obj['task_type'] = 'editor_mission';
            }
            elseif($task->getQuizzes()->first()!=null){
                $obj['task_name'] = $task->getQuizzes()->first()->getTitle();
                $obj['task_type'] = $task->getQuizzes()->first()->getType()->getName();
            }
            $obj['student_id'] = $submission->getUser()->getId();
            $obj['student_name'] = $submission->getUser()->getName();
            $obj['code'] = $submission->getUserCode();
            $obj['evaluation'] = $submission->getEvaluation()."%";
            $obj['is_evaluated'] = $submission->getIsEvaluated();
            $campusActivityTask = $this->professionalRepository->getCampusActivityTask($round->getCampus()->getId(), $task->getId());
            $campusRoundActivityTask = $this->professionalRepository->getRoundActivityTask($round_id, $campusActivityTask->getId());
            $obj['due_date'] = $campusRoundActivityTask->getDueDate();
            $obj['is_late'] = $submission->getUpdatedAt() > $obj['due_date'] ? true : false;

            array_push($submissions, $obj);
            // $submissions[] = $obj;
        }

        $searchedSubmissions=$this->sortAndSearchSubmissions($submissions,null,$search);
        $classSubmissionsCount=count( $searchedSubmissions);

        return ["count"=>$classSubmissionsCount];
    }

    public function getStudentSubmission($user_id, $round_id, $campus_activity_progress_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $campusActivityProgress = $this->professionalRepository->getCampusActivityProgressById($campus_activity_progress_id);

        $submission = [];
        if($campusActivityProgress->getTask()->getMissionAngular() != null)
        {
            $mission_id = $campusActivityProgress->getTask()->getMissionAngular()->id;
            
            $submission['student_code']=[];
            $angularUserAnswers = $this->professionalRepository->getMissionAngularUserAnswers($campusActivityProgress->getId());
            foreach($angularUserAnswers as $data){
                $angularFile = $this->journeyRepository->getAngulaFileByIdAndName($mission_id,$data->name);

                array_push($submission['student_code'], [
                    'name' => $data->name,
                    'data' => $data->data,
                    'type' => $angularFile->type
                ]);
            }
            
        }else{
            $submission['student_code']= $campusActivityProgress->getUserCode();
        }
        $submission['learner_id']= $campusActivityProgress->getUser()->getId();
        $submission['learner_name']= $campusActivityProgress->getUser()->getName();
        $submission['trials']= $campusActivityProgress->getNoTrials();
        $submission['elapsed_time']= $campusActivityProgress->getTaskDuration();
        if($campusActivityProgress->getTask()->getMissionCoding() != null)
            $submission['vpl_id'] = $campusActivityProgress->getTask()->getMissionCoding()->getVplId();
        if($campusActivityProgress->getTask()->getMissions()->first() != null)
            $submission['vpl_id'] = $campusActivityProgress->getTask()->getMissions()->first()->getVplId();
        return $submission;
    }

    private function calculateStudentScore(User $user,CampusRound $round,$activity_id,Task $task,$score,$old_score,$is_default){
        $campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$task->getId(),$is_default);
        $old_task_score=$old_score*($campusRoundTask->getWeight() / 100)*($campusRoundClass->getWeight()/100);
        $old_task_xps=round(10*$old_task_score);
        $old_task_coins=round($old_task_xps/20);


        $new_task_score=$score*($campusRoundTask->getWeight() / 100)*($campusRoundClass->getWeight()/100);
        $new_task_xps=round(10*$new_task_score);
        $new_task_coins=round($new_task_xps/20);

//        $task_score=$score_difference*($campusRoundTask->getWeight() / 100)*($campusRoundClass->getWeight()/100);
//        $task_xps=round(10*$task_score);
//        $task_coins=round($task_xps/20);
        ////dd($task_score,$campusRoundTask->getWeight(),)
        $this->accountRepository->beginDatabaseTransaction();

        $userScore=$this->accountRepository->getUserScore($user->getId());

        $userScore->setExperience($userScore->getExperience()-$old_task_xps+$new_task_xps);
        $userScore->setCoins($userScore->getCoins()-$old_task_coins+$new_task_coins);
        $this->accountRepository->storeUserScore($userScore);

        $this->accountRepository->commitDatabaseTransaction();
        return["xp_gained"=>$new_task_xps-$old_task_xps,"coins_gained"=>$new_task_coins-$old_task_coins];


    }

    //Send Mini-Project Notification when teacher evaluate
    private function sendEvaluationSubmissionNotification($user_id, $campus, $round_id, $activity_id,$task_id,$is_default=true){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $task = $this->journeyRepository->getTaskById($task_id);
    

        $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity_id);
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class->getId());
        $noOfNotEvaluatedTasks=$this->professionalRepository->getClassNotEvaluatedTasks($roundClass->getId(),$account->getId(),true,true);
        $teachers = $this->professionalRepository->getCampusRoundTeachers($round_id);
        $round = $this->professionalRepository->getRoundById($round_id);
        $round_name=$round->round_name;
        $mission_id = $task->getTaskNameAndType()['mission_id'];
        $mission = $task->getMissionsHtml()->first();
        $withSteps = $mission->getWithSteps();
            
        // dd($round_name);
        $data['course_name'] = $campus->getName();
        $data['round_name'] = $round_name;
        $data['course_id'] = $campus->getId();
        $data['round_id'] = (int)$round_id;
        $data['username'] = $user->first_name." ".$user->last_name;
        $data['task_name'] = $task->getMissionsHtml()->first()->getTitle();
        $data['task_type'] = $task->getTaskNameAndType()['task_type'];
        $data['date'] = $user->created_at;
        if($is_default==true) {
            $data['lesson_name'] = $class->getDefaultActivity()->getName();
        }else{
            $data['lesson_name'] = $class->getActivity()->getName();
        }
        $data['lesson_id'] = $class->getId();
        $data['round_lesson_id'] = $roundClass->getId();
        $data['activity_id'] = $activity_id;
        $data['submission_no'] = $noOfNotEvaluatedTasks;
        $data['mission_id']=$mission_id;
        $data['with_steps']=$withSteps;

        //Send Notification to learner
            // delete old notification first
            $this->deleteLearnerTaskNotifications($user->getId(), $round_id, $activity_id,$mission_id, EvaluationSubmission::class);

            $notification = new UserNotification($user, Uuid::uuid4()->toString(), json_encode($data), EvaluationSubmission::class);
            $this->accountRepository->storeUserNotification($notification);
            Notification::send($user, new EvaluationSubmission($notification->getId(), $data));
        
    }
    private function deleteTaskNotifications($user_id, $round_id, $activity_id, $task_id, $type){
        $oldNotifications = $this->accountRepository->getNotifications($user_id, $type);

        foreach ($oldNotifications as $oldNotification){
            $old_data = json_decode($oldNotification->getData());
            if(isset($old_data->task_id))
            {
                $old_task_id=$old_data->task_id;
            }elseif(isset($old_data->mission_id)){
                $mission = $this->journeyRepository->getHtmlMissionbyId($old_data->mission_id);
                $task = $mission->getTask();
                $old_task_id=$task->id;
            }
            
            if($old_data->round_id == $round_id && $old_data->activity_id == $activity_id && $old_task_id == $task_id)
                $this->accountRepository->deleteUserNotification($oldNotification);
        }
    }
    private function deleteLearnerTaskNotifications($user_id, $round_id, $activity_id, $mission_id, $type){
        $oldNotifications = $this->accountRepository->getNotifications($user_id, $type);

        foreach ($oldNotifications as $oldNotification){
            $old_data = json_decode($oldNotification->getData());
            
            if($old_data->round_id == $round_id && $old_data->activity_id == $activity_id && $old_data->mission_id == $mission_id)
                $this->accountRepository->deleteUserNotification($oldNotification);
        }
    }

    //Send Notification when learner course completed
    private function sendCourseCompletedNotification($learner, $campus, $round_id, $activity_id,$task_id,$task_progress_id,$isProject=false,$is_default=true){
        $account = $learner->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $task = $this->journeyRepository->getTaskById($task_id);
    

        $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity_id);
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class->getId());
        $teachers = $this->professionalRepository->getCampusRoundTeachers($round_id);
        $round = $this->professionalRepository->getRoundById($round_id);
        $round_name=$round->round_name;
        
        if($isProject){
            $taskType = 'Capstone Project';
        }else{
            $taskType = $task->getTaskNameAndType()['task_type'];
        }
            
        // dd($round_name);
        $data['course_name'] = $campus->getName();
        $data['round_name'] = $round_name;
        $data['course_id'] = $campus->getId();
        $data['round_id'] = $round->getId();
        $data['learner_id'] = $learner->id;
        $data['username'] = $learner->first_name." ".$learner->last_name;
        $data['task_name'] = $task->getMissionsHtml()->first()->getTitle();
        $data['task_type'] = $taskType;
        $data['mission_id'] = $task->getMissionsHtml()->first()->getId();
        $data['date'] = $learner->created_at;
        if($is_default==true) {
            $data['lesson_name'] = $class->getDefaultActivity()->getName();
            $data['activity_id'] = $class->getDefaultActivity()->getId();
        }else{
            $data['lesson_name'] = $class->getActivity()->getName();
            $data['activity_id'] = $class->getActivity()->getId();
        }
        $data['lesson_id'] = $class->getId();
        $data['round_lesson_id'] = $roundClass->getId();
        $data['submission_id'] = $task_progress_id;
        
        //Send Notification to Learner
                $this->deleteTaskNotifications($learner->getId(), $round_id, $activity_id,$task_id, CourseCompleted::class);
                $notification = new UserNotification($learner, Uuid::uuid4()->toString(), json_encode($data), CourseCompleted::class);
                $this->accountRepository->storeUserNotification($notification);
                Notification::send($learner, new CourseCompleted($notification->getId(), $data));
            
        
    }

    public function updateStudentSubmissionEvaluation($user_id, $round_id, $campus_activity_progress_id, $evaluation,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

            
        $campusActivityProgress = $this->professionalRepository->getCampusActivityProgressById($campus_activity_progress_id);
        
        if(!$campusActivityProgress->getFirstSuccess()){
            $numOfTrials=$campusActivityProgress->getNoTrials();
            $campusActivityProgress->setFirstSuccess($numOfTrials);
        }
        $campusActivityProgress->setIsEvaluated(true);

        $task = $this->journeyRepository->getTaskById($campusActivityProgress->task_id);
        $campusUserStatus=null;
        $updatedScore = $this->gradingCommonService->updateScore($campusActivityProgress,$round,$gmt_difference,LessonCommonService::EVALUATED_MISSION, ['evaluation' => $evaluation]);
        $campusActivityProgress = $updatedScore['campusActivityProgress'];
        $campusUserStatus=$updatedScore["campusUserStatus"];

        //$this->professionalRepository->storeCampusUserStatus($campusUserStatus); //Weird bug can't save CampusUserStatus before CampusActivityProgress

        //Notifiy User when mini-project is evaluated
        if($user->isAdmin()){
            $this->sendEvaluationSubmissionNotification($campusActivityProgress->user_id, $round->getCampus(), $round_id, $campusActivityProgress->default_activity_id ,$task->id);
            //Send Course completed notification to learner when course progress > 50%
            $progress = $this->lessonCommonService->getLearnerCampusProgress($campusActivityProgress->user_id,$round)['progress'];
            if($progress > 50){
                $learner = $this->accountRepository->getUserById($campusActivityProgress->user_id);
                $this->sendCourseCompletedNotification($learner, $round->getCampus(), $round_id, $campusActivityProgress->default_activity_id,$task->id,$campusActivityProgress->id,true);
            }
        }

        $this->professionalRepository->storeCampusActivityProgress($campusActivityProgress);
        if($campusUserStatus!==null)
            $this->professionalRepository->storeCampusUserStatus($campusUserStatus);

        $this->accountRepository->commitDatabaseTransaction();

        $gainedScore = $campusActivityProgress->getEvaluation();
        $campusActivityProgress->gained_score=$gainedScore;

        return $campusActivityProgress;
    }

    public function getRoundClassRooms($user_id, $round_id, $class_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class_id);
        if($roundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation

        $rooms = $this->professionalRepository->getRooms($roundClass->getId())->toArray();
        $rooms_ = [];
        $bbb = new  BigBlueButton();
        foreach ($rooms as $room) {
            $recordingParams = new GetRecordingsParameters();
            $recordingParams->setMeetingId($room['id']);
            $response = $bbb->getRecordings($recordingParams);
            if ($response->getReturnCode() == 'SUCCESS') {
                if(count($response->getRecords()) == 0){
                    //$room['material'] = $this->professionalRepository->getRoomById($room['id'])->getMaterial() ?? null;
                    $rooms_ []= $room;
                }
            }
        }
        return $rooms_;
    }

    public function getTeacherRoundsByStatus($user_id, $status,$gmt_difference){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($status==='running')
            $rounds=$this->professionalRepository->getTeacherRunningRounds($user_id,$gmt_difference);
        else if($status==='upcoming')
            $rounds=$this->professionalRepository->getTeacherUpcomingRounds($user_id,$gmt_difference);
        else if($status==='ended')
            $rounds=$this->professionalRepository->getTeacherEndedRounds($user_id,$gmt_difference);
        else if($status==='all')
            $rounds=$this->professionalRepository->getAllTeacherRounds($user_id,$gmt_difference);
        else
            throw new BadRequestException(trans('locale.campus_status_not_exist'));

        // dd($rounds);
        $roundsMapped=Mapper::MapEntityCollection(TeacherCampusRoundDto::class,$rounds,[CampusDto::class]);

        return $roundsMapped;



    }

    private function calculateStudentXpsAndCoins(CampusActivityProgress $progress,CampusRound $round,$activity_id,$newLessonWeight,$oldLessonWeight){
        $is_default=true;
        //$campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$progress->getTask()->getId(),$is_default);
        $old_score=$progress->getEvaluation();

        if($old_score>0){
            $old_task_score=$old_score*($campusRoundTask->getWeight() / 100)*($oldLessonWeight/100);
            $old_task_xps=round(10*$old_task_score);
            $old_task_coins=round($old_task_xps/20);


            $new_task_score=$old_score*($campusRoundTask->getWeight() / 100)*($newLessonWeight/100);
            $new_task_xps=round(10*$new_task_score);
            $new_task_coins=round($new_task_xps/20);
//            if($progress->getUser()->getId()=='100187')
//                dd($old_score,$old_task_score,$old_task_xps,$new_task_score,$new_task_xps);


//            $this->accountRepository->beginDatabaseTransaction();
//
            $userScore=$this->accountRepository->getUserScore($progress->getUser()->getId());

            $userScore->setExperience($userScore->getExperience()-$old_task_xps+$new_task_xps);
            $userScore->setCoins($userScore->getCoins()-$old_task_coins+$new_task_coins);
            $this->accountRepository->storeUserScore($userScore);
//
//            $this->accountRepository->commitDatabaseTransaction();
            return["user_id"=>$progress->getUser()->getId(),"xp_diff"=>($new_task_xps-$old_task_xps),"coins_diff"=>($new_task_coins-$old_task_coins)];
        }
        return["user_id"=>$progress->getUser()->getId(),"xp_diff"=>0,"coins_diff"=>0];

    }

    public function updateRoundClass($user_id, $round_id,array $classes){

        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $updatedClasses=[];
        foreach ($classes as $class){
            $class_id=$class['id'];
            $class_weight=$class['weight'];
            $class = $this->professionalRepository->getCampusClassById($class_id);
            if($class == null)
                throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation

            $roundClass = $this->professionalRepository->getCampusRoundClass($round_id,$class_id);
            $old_class_weight=$roundClass->getWeight();
            if($class_weight!=$old_class_weight){
                $classDto['id']=$class_id;
                $classDto['activity_id']=$class->getDefaultActivity()->getId();
                $classDto['old_weight']=$old_class_weight;
                $classDto['new_weight']=$class_weight;

                $updatedClasses[]=$classDto;
            }
            if($roundClass == null)
                throw new BadRequestException(trans('locale.campus_round_class_not_exist')); //TODO translation

            $roundClass->setWeight($class_weight);
            $this->professionalRepository->storeCampusRoundClass($roundClass);

        }
        foreach ($updatedClasses as $updatedClass){
            //$studentsSolvedTasksInClass=$this->professionalRepository->getStudentSolvedTaskInRound()
            $studentsSolvedTasksInClass=$this->professionalRepository->getStudentSolvedTasksInRoundClass($round,$updatedClass['activity_id']);
            $student_id=null;
            //$score_diff_initial=["user_id"=>$student_id,"xp_diff"=>0,"coins_diff"=>0];
            //dd($studentsSolvedTasksInClass);
            foreach($studentsSolvedTasksInClass as $solvedTasksInClass){
                if($student_id==null ||$student_id!=$solvedTasksInClass->getUser()->getId()){
                    $student_id = $solvedTasksInClass->getUser()->getId();

                }

                $score_diff=$this->calculateStudentXpsAndCoins($solvedTasksInClass,$round,$updatedClass['activity_id'],$updatedClass['new_weight'],$updatedClass['old_weight']);



            }
        }
        $this->accountRepository->commitDatabaseTransaction();
        //return $updatedClasses;
        return trans("locale.campus_classes_updated_successfully");


    }

    public function getTeacherSubmissionsNotifications($user_id){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $teacherRounds=$this->professionalRepository->getAllTeacherRounds($user_id);
        // dd($teacherRounds);
        $notifications=[];
        foreach ($teacherRounds as $teacherRound){
            $roundClasses=$teacherRound->getCampusRoundClasses();
            //dd(($teacherRound));
            foreach ($roundClasses as $roundClass){
                //dd($roundClass);
                $noOfNotEvaluatedTasks=$this->professionalRepository->getClassNotEvaluatedTasks($roundClass->getId(),$account->getId(),true,true);
                //dd($noOfNotEvaluatedTasks);
                if($noOfNotEvaluatedTasks>0) {
                    $notification['campusName'] = $teacherRound->getCampusName();
                    $notification['className'] = $roundClass->getCampusClass()->getDefaultActivity()->getName();
                    $notification['flag'] = 0;
                    $notification['noOfNotEvaluatedSubmissions'] = $noOfNotEvaluatedTasks;

                    // $notifications[]=$notification;
                    array_push($notifications,$notification);
                }
                $stuckStudents=$this->professionalRepository->getStuckStudentsPerClass($roundClass->getId(),$account->getId(),true,null);
                //if(count($stuckStudents)>0)
                //dd($stuckStudents);
                foreach ($stuckStudents as $stuckStudent){
                    $notification_=[];

                    $notification_['campusName'] = $teacherRound->getCampusName();
                    $notification_['className'] = $roundClass->getCampusClass()->getDefaultActivity()->getName();
                    $notification_['flag'] = 1;

                    $notification_['username'] = $stuckStudent->first_name." ".$stuckStudent->last_name;//->getFirstName()." ".$stuckStudent->getLastName();
                    $notification_['date']=$stuckStudent->created_at;

                    $task_id=$stuckStudent->task_id;
                    $task=$this->journeyRepository->getTaskById($task_id);

                    if($task->getMissions()->first() != null) {
                        $notification_['task_name'] = $task->getMissions()->first()->getName();
                        //$notification_['task_type'] = 'mission';
                    }
                    elseif($task->getMissionsHtml()->first() != null) {
                        $notification_['task_name'] = $task->getMissionsHtml()->first()->getTitle();
                        //$notification_['task_type'] = 'html_mission';
                    }
                    elseif($task->getMissionCoding() != null) {
                        $notification_['task_name'] = $task->getMissionCoding()->getTitle();
                        //$notification_['task_type'] = 'coding_mission';
                    }
                    elseif($task->getMissionEditor() != null){
                        $notification_['task_name'] = $task->getMissionEditor()->getTitle();
                        //$notification_['task_type'] = 'editor_mission';
                    }
                    elseif($task->getQuizzes()->first()!=null){
                        $notification_['task_name'] = $task->getQuizzes()->first()->getTitle();
                        //$notification_['task_type'] = $task->getQuizzes()->first()->getType()->getName();
                    }
                    //$notifications[]=$notification;
                    array_push($notifications,$notification_);

                }

                //dd($noOfNotEvaluatedTasks);

            }
            //$teacherRound->getCountOfUnsolved

        }
        return $notifications;


    }

    function sortClassTasks(array $arr) {
        $sorted = false;
        while (false === $sorted) {
            $sorted = true;
            for ($i = 0; $i < count($arr)-1; ++$i) {
                $current = $arr[$i];
                $next = $arr[$i+1];
                if ($next['order'] < $current['order']) {
                    $arr[$i] = $next;
                    $arr[$i+1] = $current;
                    $sorted = false;
                }
            }
        }
        return $arr;
    }

    public function getTeacherClassTasks($user_id, $class_id,$round_id, $is_default=true){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $class = $this->professionalRepository->getCampusClassById($class_id);
        if($class == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        if($is_default)
            $activity = $class->getDefaultActivity();
        else
            $activity = $class->getActivity();

        $tasks = $activity->getTasks();

        $tasksDto = [];
        $htmlTasksDto = [];
        $boosterTasksDto = [];
        $returnTasks = [];
        foreach ($tasks as $task){
            $taskTimeEstimate = $this->professionalRepository->getTaskTypeTimeEstimate($task->getTaskNameAndType()['task_type']);
            $difficulty =  $this->professionalRepository->getTaskDifficulty($task->getId(),$activity->getId());
            $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());

            if($task->getMissions()->first() != null) {
                $mission = $task->getMissions()->first();
                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image' => $mission->getIconUrl(),
                    'type' => 'mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,


                ]);
            }
            elseif($task->getMissionsHtml()->first() != null) {
                $mission = $task->getMissionsHtml()->first();

                if($mission->getWithSteps() == 1)
                $withSteps=true;
                else
                $withSteps=false;

               if( $mission->getIsMiniProject()==0) {
                   array_push($htmlTasksDto, [
                       'id' => $task->getId(),
                       'name' => $mission->getTitle(),
                       'image' => $mission->getIconUrl(),
                       'type' => 'html_mission',
                       'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                       'mission_id' => $mission->getId(),
                       'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                       'lessonType' => $mission->getLessonType(),
                        'withSteps' => $withSteps,
                        'time_estimate' => $taskTimeEstimate,
                        'locked'=>false,

                   ]);
               }elseif($mission->getIsMiniProject()){
                   array_push($tasksDto, [
                       'id' => $task->getId(),
                       'name' => $mission->getTitle(),
                       'image' => $mission->getIconUrl(),
                       'type' => 'mini_project',
                       'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                       'mission_id' => $mission->getId(),
                       'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                       'lessonType' => $mission->getLessonType(),
                        '$withSteps' => $withSteps,
                        'time_estimate' => $taskTimeEstimate,
                        'locked'=>false,

                       //'project_id' => $task->getProject()->first()->getProjectId()
                   ]);

               }
            }
            elseif($task->getMissionCoding() != null) {
                $mission = $task->getMissionCoding();
                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'model_answer' => $mission->getModelAnswer(),
                    'type' => 'coding_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'code' => $mission->getCode(),
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,

                ]);
            }
            elseif($task->getMissionEditor() != null) {
                $mission = $task->getMissionEditor();
                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'model_answer' => $mission->getModelAnswer(),
                    'type' => 'editor_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mode' => $mission->getType(),
                    'html' => $mission->getHtml(),
                    'css' => $mission->getCss(),
                    'js' => $mission->getJs(),
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,

                ]);
            }
            elseif($task->getMissionAngular() != null) {
                $mission = $task->getMissionAngular();
                $missionAngularFilesDto=Mapper::MapEntityCollection(MissionAngularFilesDto::class, $mission->getFiles());
                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'type' => 'angular_mission',
                    'files' => $missionAngularFilesDto,
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,

                ]);
            }
            elseif($task->getQuizzes()->first()!=null){
                $quiz = $task->getQuizzes()->first();
                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $quiz->getTitle(),
                    'image' => $quiz->geticonURL(),
                    'type' => $quiz->getType()->getName(),
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'quiz_id' => $quiz->getId(),
                    'order' => $quiz->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'locked'=>false,
                    'time_estimate' => $taskTimeEstimate,
                ]);
            }elseif($activityTask->booster_type == 1){
                array_push($boosterTasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'type' => 'html_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mission_id' => $mission->getId(),
                    'passed' => $passed,
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'last_seen'=>$lastSeenTime,
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'time_estimate' => $taskTimeEstimate,
                    'mission_steps_count'=> $missionSteps,
                    'current_step' => $missionCurrentStep,
                    'moduleType' => $mission->getLessonType(),
                    'withSteps' => $withSteps,
                    'current_section' => $currentSection,
                    'sections_count' => count($mission->getSections()),
                    'locked'=>false,

                ]);
            }
        }
        $tasksDto=$this->sortClassTasks($tasksDto);
        $htmlTasksDto=$this->sortClassTasks($htmlTasksDto);
        $returnTasks["tasks"]=$tasksDto;
        $returnTasks["html_tasks"]=$htmlTasksDto;

        $roundClass=$this->professionalRepository->getCampusRoundClass($round_id,$class_id);

        $classRooms=$roundClass!==null?$roundClass->getRooms():[];
        $returnTasks['recordedSessions']=null;
        $returnTasks['runningSession']=null;
        $returnTasks['materials']=null;
        $returnTasks["boosters"] = count($boosterTasksDto) >0 ? $boosterTasksDto : null;
        $bbb = new BigBlueButton();
        $materials=$roundClass!==null?$roundClass->getMaterials():[];
        foreach ( $materials as $material){
            $returnTasks['materials'] []= $material;
        }


        foreach($classRooms as $classRoom) {
            //GET RECORDING ROOMS
            $recordingParams = new GetRecordingsParameters();
            $recordingParams->setMeetingId($classRoom->id);
            $response = $bbb->getRecordings($recordingParams);
            $records = [];
//            $material = null;
//            if($classRoom->getFileLink() != null){
//                $material['session_id'] = $classRoom->id;
//                $material['session_name'] = $classRoom->name;
//                $material['material_url'] = $classRoom->getFileLink();
//            }
            if ($response->getReturnCode() == 'SUCCESS') {
                foreach ($response->getRecords()  as $recording) {
                    // process all recording
                    $record_['url'] = $recording->getPlaybackUrl();
                    $record_['session_id'] = $classRoom->id;
                    $record_['session_name'] = $classRoom->name;
                    $records []= $record_;
                }
            }
            if(!empty($records))
                $returnTasks['recordedSessions'] []= $records[0];
//            if ($material != null){
//                $returnTasks['materials'] []= $material;
//            }

            //GET RUNNING ROOMS
            $isRunningParams = new IsMeetingRunningParameters($classRoom->id);
            $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
            if($isRunning) {
                $room_['seesion_id'] = $classRoom->id;
                $room_['session_name'] = $classRoom->name;
                $returnTasks['runningSession'] = $room_;
            }
        }

        return $returnTasks;
    }

    public function getTeacherClassActivity($user_id, $class_id, $is_default=true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if (!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $class = $this->professionalRepository->getCampusClassById($class_id);
        if ($class == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        if ($is_default)
            $activity = $class->getDefaultActivity();
        else
            $activity = $class->getActivity();

        $activityDto['id'] = $activity->getId();
        $activityDto['name'] = $activity->getName();
        $activityDto['description'] = $activity->getDescription();
        $activityDto['imageUrl'] = $activity->getImageUrl();

        return $activityDto;
    }

    public function generalTeacherStatisticsInCampuses($user_id,$gmt_difference){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];
        $school = $data['school'];
        $noOfStudents = 0; $runningCamps = []; $upcomingCamps = [];

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $teacher))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $campusesRounds = $this->professionalRepository->getAllTeacherRounds($user_id,$gmt_difference);
        if(count($campusesRounds) > 0){
            $runningCamps = $this->professionalRepository->getTeacherRunningRounds($user_id,$gmt_difference);
            $upcomingCamps = $this->professionalRepository->getTeacherUpcomingRounds($user_id,$gmt_difference);
            //$camps = $this->schoolRepository->getTeacherCamps($school->getId(), $teacher->getId());
            $students = [];
            $noOfStudents = 0;
            foreach ($campusesRounds as $round){
                foreach ($round->getStudents() as $student){
                    if(!in_array($student->getId(), $students)){
                        $noOfStudents++;
                        array_push($students, $student->getId());
                    }
                }

            }
        }
        return ["noStudents" => $noOfStudents, "runningRound" => count($runningCamps), "upcomingRounds" => count($upcomingCamps)];
    }

    public function getTopStudentsInCampusRound($user_id, $round_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $data = array();
        //$solvedTasksNum = $this->professionalRepository->getStudentSolvedTasksInCampus($student->getId(), $campus->getId(), true);
        $campusTasksNum = $this->professionalRepository->getAllTasksInCampus($round->getCampus()->getId(), true);
        //dd($campusTasksNum);
        $queryData = $this->professionalRepository->getTopStudentsInCampusRound($account,$round->getCampus()->getId(),$round_id);
        // dd($queryData);
        if($campusTasksNum!==0) {
            foreach ($queryData as $student) {

                array_push($data, ["name" => $student->first_name . ' ' . $student->last_name, "campusName" => $round->getCampusName()
                    , "progress" => intval(($student->tasksSolved / $campusTasksNum) * 100) . "%"]);

            }
        }
        return $data;
    }

    public function checkSchoolData($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        return ['user' => $user, 'account' => $account, 'school' => $school];
    }
    public function getTeacherRecentActivity($user_id,$gmt_difference){
        //get rounds ends in 3 days
        //get classes ends in 1 day
        //get tasks(not html) due_date in 1 day
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        $account = $data['account'];

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        //$current_time = Carbon::now()->toDateTimeString();
        $recentsDto=[];

        $rounds=$this->professionalRepository->getRecentActivityRounds($user_id,$gmt_difference);
        foreach ($rounds as $round){
            $roundDto=[];
            $roundDto['id']=$round->getId();
            $roundDto['name']=$round->getCampusName();
            $roundDto['ends_at']=$round->getEndsAt();
            $roundDto['flag']=1;
            $roundDto['round_name']=$round->getRound_name();
            $recentsDto[]=$roundDto;

        }
        $classes=$this->professionalRepository->getRecentActivityClasses($user_id,$gmt_difference);
        foreach ($classes as $class){
            $classDto=[];
            $classDto['id']=$class->getId();
            $classDto['name']=$class->getCampusClass()->getDefaultActivity()->getName();
            $classDto['ends_at']=$class->getEndsAt();
            $classDto['flag']=2;
            $recentsDto[]=$classDto;

        }
        $tasks=$this->professionalRepository->getRecentActivityTasks($user_id,$gmt_difference);
        foreach ($tasks as $task){
            $taskDto=[];
            $task_type=$task->getCampusActivityTask()->getTask()->getTaskNameAndType()['task_type'];
            if($task_type!=='html_mission'){
                $taskDto['id']=$task->getId();
                $taskDto['due_date']=$task->getDueDate();
                $taskDto['name']=$task->getCampusActivityTask()->getTask()->getTaskNameAndType()['task_name'];
                $taskDto['task_type']=$task_type;
                $taskDto['flag']=3;
                $recentsDto[]=$taskDto;

            }

        }
        return $recentsDto;


    }

    public function sendSolveTaskNotifications($user_id,$round_id,$class_id,$task_id, $date,$is_default=true){

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        $account = $data['account'];

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translations

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation
        $course_id=$round->getCampus_id();

        $campus = $this->professionalRepository->getCampusById($course_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); //TODO translation
        $class=$this->professionalRepository->getCampusClassById($class_id);
        if($class==null)
            throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation
        $campusActivityTask=$this->professionalRepository->getCampusActivityTask($course_id,$task_id);
        if($campusActivityTask==null)
            throw new BadRequestException(trans('locale.campus_activity_task_not_exist')); //TODO translation
        $task=$campusActivityTask->getTask();

        $students=$round->getStudents();
        if($is_default==true) {
            $activity_id = $class->getDefaultActivity()->getId();
            $activity = $class->getDefaultActivity();
        }
        else {
            $activity_id = $class->getActivity()->getId();
            $activity = $class->getActivity();
        }


        $notificationDto=[];

        $notificationDto['course_name'] = $campus->getName();
        $notificationDto['course_id'] = $campus->getId();
        $notificationDto['round_id'] = $round_id;
        $notificationDto['class_name'] = $activity->getName();
        $notificationDto['class_id'] = $class->getId();
        $notificationDto['activity_id'] = $activity_id;
        $notificationDto['task_id'] = $task->getId();
        $notificationDto['task_name'] = $task->getTaskNameAndType()['task_name'];
        $notificationDto['task_type'] = $task->getTaskNameAndType()['task_type'];
        $notificationDto['mission_id'] = $task->getTaskNameAndType()['mission_id'];


        $notificationDto['username'] = $user->first_name." ".$user->last_name;
        $notificationDto['date'] = $date;


        foreach ($students as $student) {

            $notification = new UserNotification($student, Uuid::uuid4()->toString(), json_encode($notificationDto), SolveTask::class);
            $this->accountRepository->storeUserNotification($notification);
            Notification::send($student, new SolveTask($notification->getId(), $notificationDto));
        }



    }

    public function uploadMaterialToClass($user_id, $round_id, $class_id,$file_name , $file_s3_url) {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        
        if (!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user) && !$this->accountRepository->isAuthorized('get_AdminDashboard', $user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class_id);
        if($roundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation

        $material = new Material($roundClass);
        $material->setName($file_name);
        $material->setFileLink($file_s3_url);


        $this->professionalRepository->storeMaterial($material);
        $this->accountRepository->commitDatabaseTransaction();
    }
    public function deleteMaterialFromClass($user_id, $round_id, $class_id,$material_id,$force){
        $force=filter_var($force, FILTER_VALIDATE_BOOLEAN);
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if (!($this->accountRepository->isAuthorized('get_TeacherDashboard', $user)
            || $this->accountRepository->isAuthorized('get_AdminDashboard', $user)))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class_id);
        if($roundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        $material=$this->professionalRepository->getMaterialById($material_id);
        if($material == null)
            throw new BadRequestException(trans('locale.material_not_exist'));

        if($roundClass->getId()!=$material->getRoundClass()->getId())
            throw new BadRequestException(trans('locale.material_not_in_class'));
        $rooms=$this->professionalRepository->getRoomsByMaterialId($material_id);
        if(count($rooms)>0 &&!$force)
            throw new BadRequestException(trans('locale.material_in_session'));
        foreach ($rooms as $room){
            $room->setMaterialId(null);
            $this->professionalRepository->storeRoom($room);
        }

        $this->professionalRepository->deleteMaterial($material);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.material_deleted_successfully');



    }


}