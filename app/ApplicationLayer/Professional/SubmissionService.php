<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/9/2018
 * Time: 7:15 PM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQADto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionDto;
use App\ApplicationLayer\Professional\Dtos\SubmissionDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\AnalysisQA;
use App\DomainModelLayer\Professional\LearningPathUser;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Submission;
use Analogue;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;
use App\Helpers\ResponseObject;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Framework\Exceptions\InternalErrorException;
use App\Framework\Exceptions\UnauthorizedException;
use Illuminate\Support\Facades\Response;
use App\Helpers\HttpMethods;
use DB;
use LaravelLocalization;
use Excel;
use Carbon\Carbon;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class SubmissionService
{

    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;

    private $learningPathWeight = [
        'FS' => [
            'R' => 0.50,
            'I' => 0.00,
            'A' => 0.75,
            'S' => 0.75,
            'E' => 1.00,
            'C' => 0.00
        ],
        'PML' => [
            'R' => 0.50,
            'I' => 1.00,
            'A' => 0.25,
            'S' => 0.25,
            'E' => 0.00,
            'C' => 1.00
        ]
    ];

    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;

//        dd($this->learningPathWeight);
    }

    public function submit($user_id,array $answers,$categoryCode){
        //make submission entity instance
        $submissionDto=new SubmissionDto;
        $submissionDto->user_id=$user_id;
        $submissionDto->final_result='';
        $submissionDto->category_code=$categoryCode;
        //dd($submissionDto);
        $submission=new Submission($submissionDto);

//        $data = $this->checkSchoolData($user_id);
//        $user = $data['user'];
        
//        $school = $data['school'];

        $this->accountRepository->beginDatabaseTransaction();

        $this->professionalRepository->deleteUserSubmissions($user_id,$categoryCode);

        $submission=$this->professionalRepository->submit($submission);

//        dd($submission);

        $analysisQAArray = [];
        $answersData = [];
//        dd($answers);
        foreach ($answers as $answer){
            $answerData= $this->professionalRepository->getAnswer($answer->answer_id);

            $answersData[] = $answerData;

            $analysisQADto = new AnalysisQADto;
            $analysisQADto->question_id = $answer->question_id;
            $analysisQADto->answer_id = $answer->answer_id;
            $analysisQADto->submission_id = $submission->id;
            $analysisQAArray[] = new AnalysisQA($analysisQADto);

        }
        $this->professionalRepository->insertUserQA($analysisQAArray);
        $this->accountRepository->commitDatabaseTransaction();

//        dd($answersData);
        return ["answersData"=>$answersData,"submission"=>$submission];

        //calculate score
//        $levelScore=1;
//        $interestScore=1;
//        $careerScore=1;
//        $deductionArray=[];
//        $analysisQAArray=[];
//        $levelAnswers = $categoryAnswers[1];
//        $interestAnswers = $categoryAnswers[2];
//        $CareerAnswers = $categoryAnswers[3];
//
//        foreach ($levelAnswers as $answerId){
//                $answer= $this->professionalRepository->getAnswer($answerId);
//                $answerFactor=$answer->factor;
//                $answerDeduction=$answer->deduction;
//
//                if($answerFactor!=null)
//                    $levelScore+=($levelScore*$answerFactor);
//            if($answerDeduction!=null)
//                $deductionArray[]=$answerDeduction;
//
//            $answersArray[]=$answer->id;
//
//            $analysisQADto=new AnalysisQADto;
//            $analysisQADto->question_id=$answer->question_id;
//            $analysisQADto->answer_id=$answer->id;
//            $analysisQADto->submission_id=$submission->id;
//            $analysisQAArray[]=new AnalysisQA($analysisQADto);
//
//        }
//
//        $level = 'Novice';
//        if($levelScore > 0.5)
//            $level = 'Talented';
//
//        foreach ($interestAnswers as $answerId){
//            $answer= $this->professionalRepository->getAnswer($answerId);
//            $answerFactor=$answer->factor;
//            $answerDeduction=$answer->deduction;
//
//            if($answerFactor!=null)
//                $interestScore+=($interestScore*$answerFactor);
//            if($answerDeduction!=null)
//                $deductionArray[]=$answerDeduction;
//
//            $answersArray[]=$answer->id;
//
//            $analysisQADto=new AnalysisQADto;
//            $analysisQADto->question_id=$answer->question_id;
//            $analysisQADto->answer_id=$answer->id;
//            $analysisQADto->submission_id=$submission->id;
//            $analysisQAArray[]=new AnalysisQA($analysisQADto);
//        }
//
//        foreach ($CareerAnswers as $answerId){
//            $answer= $this->professionalRepository->getAnswer($answerId);
//            $answerFactor=$answer->factor;
//            $answerDeduction=$answer->deduction;
//
//            if($answerFactor!=null)
//                $careerScore+=($careerScore*$answerFactor);
//            if($answerDeduction!=null)
//                $deductionArray[]=$answerDeduction;
//
//            $answersArray[]=$answer->id;
//
//            $analysisQADto=new AnalysisQADto;
//            $analysisQADto->question_id=$answer->question_id;
//            $analysisQADto->answer_id=$answer->id;
//            $analysisQADto->submission_id=$submission->id;
//            $analysisQAArray[]=new AnalysisQA($analysisQADto);
//
//        }
//
//        $path= '';
//        $lang='';
//        //dd(['career_score'=>$careerScore,'level'=>$level,'interestScore'=>$interestScore]);
//        if($level == 'Novice' && $careerScore <= 1){
//            $lang = 'Javascript';
//            $path = 'Full Stack Front-End Development';
//        }
//        elseif ($level == 'Novice' && $careerScore > 1){
//            $lang = 'Python';
//            $path = 'Machine Learning From Scratch';
//        }
//        elseif ($level == 'Talented' && $careerScore <= 1){
//            $lang = 'Javascript';
//            $path = 'Full Stack Mobile Development';
//        }
//
//        elseif ($level == 'Talented' && $careerScore > 1){
//            $lang = 'Python';
//            $path = 'Data Science Specialization';
//        }
//        //dd(["path"=>$path]);
//
//        $finalResult = $level.' '.$lang;
//        $this->professionalRepository->insertUserQA($analysisQAArray);
//        $this->professionalRepository->updateSubmissionResult($submission->id,$finalResult);
//        $learningPath = $this->professionalRepository->getLearningPathByName($path);
//        dd(["path"=>$path,"learningPath"=>$learningPath]);
//
//        $learningPathUser = new LearningPathUser($learningPath,$user);
//        $this->professionalRepository->deleteLearningPathUser($user_id); //delete old learning paths if found
//        $this->professionalRepository->storeLearningPathUser($learningPathUser);
//        $this->accountRepository->commitDatabaseTransaction();
//        return["result"=>$finalResult,"path"=>$path];
    }

    public function deleteCategoryPostSubmissions($user_id,$categoryID){

        $this->accountRepository->beginDatabaseTransaction();
        $category=$this->professionalRepository->getCategoryById($categoryID);
        $postCategories=$this->professionalRepository->getPostCategories($category)->pluck('category_code');
        //dd($postCategories);
        $postCategoriesTrimmed=[];
        foreach ($postCategories as $postCategory){
            $postCategoriesTrimmed[]=trim($postCategory);
        }

        $this->professionalRepository->deleteCategoryPostSubmissions($user_id,$postCategoriesTrimmed);

        $this->accountRepository->commitDatabaseTransaction();

        return trans('locale.user_submissions_deleted');


    }

    public function evaluate($user_id, $visualProfile, $categoryCode, $answers = null){
//        dd($user_id);
        switch ($categoryCode){
            case 'TIA': return $this->evaluateTIA($visualProfile, $answers);
            case 'WPA':
                $userSubmission = $this->getCategoryUserSubmission($user_id, 'TIA');
                if(isset($userSubmission)){
                    $visualProfile = $userSubmission->visual_profile;
                }
                return $this->evaluateWPA($visualProfile, $answers);
            case 'WSI':
                $userSubmission = $this->getCategoryUserSubmission($user_id, 'WPA');
                if(isset($userSubmission)){
                    $visualProfile = $userSubmission->visual_profile;
                }
                return $this->evaluateWSI($visualProfile, $answers);
            case 'EEB':
                $userSubmission = $this->getCategoryUserSubmission($user_id, 'WSI');
                if(isset($userSubmission)){
                    $visualProfile = $userSubmission->visual_profile;
                }
                return $this->evaluateEEB($visualProfile, $answers);
            case 'GSI':
                $userSubmission = $this->getCategoryUserSubmission($user_id, 'EEB');
                if(isset($userSubmission)){
                    $visualProfile = $userSubmission->visual_profile;
                }
                return $this->evaluateGSI($visualProfile, $answers);
            case 'PSA':
                $userSubmission = $this->getCategoryUserSubmission($user_id, 'GSI');
                if(isset($userSubmission)){
                    $visualProfile = $userSubmission->visual_profile;
                }
                return $this->evaluatePSA($visualProfile, $answers);
            default:
                // It's another assessment other than that of e-learning Alchemy
                if(isset($answers)){
                    return $this->evaluateOld($answers);
                }else{
                    throw new BadRequestException("Missing answers or category code");
                }
        }
    }

    public function  saveVisualProfile(Submission $submission,$evalObject){
        $this->accountRepository->beginDatabaseTransaction();
        $submission->setVisualProfile(json_encode($evalObject,true));
        $this->professionalRepository->submit($submission);
        $this->accountRepository->commitDatabaseTransaction();
    }

    private function evaluateTIA($visualProfile, $answers){
        $categoryScore = [
            'R' => 0,
            'I' => 0,
            'A' => 0,
            'S' => 0,
            'E' => 0,
            'C' => 0
        ];
        $LP_score = (object)[
            'FS' => 0,
            'PML' => 0
        ];

        foreach ($answers as $answer){
            $categoryScore[$answer->deduction] += 1;
        }

//        dd($categoryScore);

        $maxCategoryScore = 0;
        $mostDominantStyle = "";
        $mostDominantStyles = [];
        $mostDominantStylesThreshold = 0;
        foreach ($categoryScore as $key => $value){
            $value = round($value * 100 / 21);
            $LP_score->FS += $value * $this->learningPathWeight['FS'][$key];
            $LP_score->PML += $value * $this->learningPathWeight['PML'][$key];

            if($value > $maxCategoryScore){
                $maxCategoryScore = $value;
                $mostDominantStylesThreshold = $maxCategoryScore - $maxCategoryScore*0.1;
                switch ($key){
                    case 'R': $mostDominantStyle = "Practical"; break;
                    case 'I': $mostDominantStyle = "Investigative"; break;
                    case 'A': $mostDominantStyle = "Artistic"; break;
                    case 'S': $mostDominantStyle = "Helping"; break;
                    case 'E': $mostDominantStyle = "Enterprising"; break;
                    case 'C': $mostDominantStyle = "Conventional"; break;
                }
            }

            $categoryScore[$key] = $value;
        }

//        dd($categoryScore);

        $mostDominantStyles[] = $mostDominantStyle;
        foreach ($categoryScore as $key => $value){
            if($value > $mostDominantStylesThreshold && $value != $maxCategoryScore){
                switch ($key){
                    case 'R': $mostDominantStyles[] = "Practical"; break;
                    case 'I': $mostDominantStyles[] = "Investigative"; break;
                    case 'A': $mostDominantStyles[] = "Artistic"; break;
                    case 'S': $mostDominantStyles[] = "Helping"; break;
                    case 'E': $mostDominantStyles[] = "Enterprising"; break;
                    case 'C': $mostDominantStyles[] = "Conventional"; break;
                }
            }
        }

        // Update visual profile to send
        $visualProfile->VisualProfile->TIA_WPA->practical = $categoryScore['R'];
        $visualProfile->VisualProfile->TIA_WPA->investigative = $categoryScore['I'];
        $visualProfile->VisualProfile->TIA_WPA->artistic = $categoryScore['A'];
        $visualProfile->VisualProfile->TIA_WPA->helping = $categoryScore['S'];
        $visualProfile->VisualProfile->TIA_WPA->enterprising = $categoryScore['E'];
        $visualProfile->VisualProfile->TIA_WPA->conventional = $categoryScore['C'];

        $visualProfile->Scoring->learningpath->FS_LP->initialValue = $LP_score->FS;
        $visualProfile->Scoring->learningpath->PML_LP->initialValue = $LP_score->PML;

        $visualProfile->VisualProfile->TIA_WPA->visualSummaryTIA = $mostDominantStyles;

        return $visualProfile;
    }
    private function evaluateWPA($visualProfile, $answers){
        $categoryScore = [
            'R' => 0,
            'I' => 0,
            'A' => 0,
            'S' => 0,
            'E' => 0,
            'C' => 0
        ];
        $LP_score = (object)[
            'FS' => 0,
            'PML' => 0
        ];

        foreach ($answers as $answer){
            $deductionModified = str_replace("'", '"', $answer->deduction);
            $deduction = json_decode($deductionModified);
            $categoryScore[$deduction[0]] += $answer->factor;
            $categoryScore[$deduction[1]] -= $answer->factor;
        }

        foreach ($categoryScore as $key => $value){
//            $value = round($value * 100 / 21);
            $LP_score->FS += $value * $this->learningPathWeight['FS'][$key];
            $LP_score->PML += $value * $this->learningPathWeight['PML'][$key];
        }

        $visualProfile->VisualProfile->TIA_WPA->practical += $categoryScore['R'];
        $visualProfile->VisualProfile->TIA_WPA->practicalAddedValue = $categoryScore['R'];
        $visualProfile->VisualProfile->TIA_WPA->investigative += $categoryScore['I'];
        $visualProfile->VisualProfile->TIA_WPA->investigativeAddedValue = $categoryScore['I'];
        $visualProfile->VisualProfile->TIA_WPA->artistic += $categoryScore['A'];
        $visualProfile->VisualProfile->TIA_WPA->artisticAddedValue = $categoryScore['A'];
        $visualProfile->VisualProfile->TIA_WPA->helping += $categoryScore['S'];
        $visualProfile->VisualProfile->TIA_WPA->helpingAddedValue = $categoryScore['S'];
        $visualProfile->VisualProfile->TIA_WPA->enterprising += $categoryScore['E'];
        $visualProfile->VisualProfile->TIA_WPA->enterprisingAddedValue = $categoryScore['E'];
        $visualProfile->VisualProfile->TIA_WPA->conventional += $categoryScore['C'];
        $visualProfile->VisualProfile->TIA_WPA->conventionalAddedValue  = $categoryScore['C'];

        $visualProfile->Scoring->learningpath->FS_LP->initialValue += $LP_score->FS;
        $visualProfile->Scoring->learningpath->PML_LP->initialValue += $LP_score->PML;

        if($visualProfile->VisualProfile->TIA_WPA->practical > 50
        || $visualProfile->VisualProfile->TIA_WPA->conventional > 50){

            $visualProfile->Scoring->careerpath->PO_CP->initialValue = false;
        }else{
            $visualProfile->Scoring->careerpath->PO_CP->initialValue = true;
        }

        if($visualProfile->VisualProfile->TIA_WPA->practical > 50
        || $visualProfile->VisualProfile->TIA_WPA->helping > 50
        || $visualProfile->VisualProfile->TIA_WPA->enterprising > 50){

            $visualProfile->Scoring->careerpath->PA_CP->initialValue = false;
        }else{
            $visualProfile->Scoring->careerpath->PA_CP->initialValue = true;
        }

        return $visualProfile;
    }
    private function evaluateWSI($visualProfile, $answers){
        $scores = [
            'FS_LP' => 0,
            'PML_LP' => 0,
            'D_FL_CP' => 0,
            'D_C_CP' => 0,
            'PA_CP' => 0,
            'PO_CP' => 0
        ];
        $badges = [];
        $selectA = 0;
        $selectB = 0;

        foreach ($answers as $answer){
            $deductionModified = str_replace("'", '"', $answer->deduction);
            $deduction = json_decode($deductionModified);
            // dd($deduction);

            foreach ($deduction->scores as $path => $score){
                $scores[$path] += $score;
            }

            switch ($deduction->tag){
                case '27-A': $visualProfile->Scoring->careerpath->PA_CP->initialValue = false; break;

                case '2-A': $badges[] = "work_team"; $selectA++; break;
                case '2-B': $badges[] = "work_self"; $selectB++; break;

                case '4-A': $badges[] = "consider_creative"; $selectA++; break;
                case '4-B': $badges[] = "consider_scientific"; $selectB++; break;

                case '8-A': $badges[] = "desire_job"; $selectA++; break;
                case '8-B': $badges[] = "desire_financial"; $selectB++; break;

                case '20-A': $badges[] = "prefer_presenting"; break;
                case '20-B': $badges[] = "prefer_reporting"; break;

                case '26-A': $badges[] = "focus_details"; break;
                case '26-B': $badges[] = "focus_picture"; break;
            }
        }
        $visualProfile->VisualProfile->WSI = $badges;

        $threshold_FS_LP = abs(0.1*$visualProfile->Scoring->learningpath->FS_LP->initialValue);
        $threshold_PML_LP = abs(0.1*$visualProfile->Scoring->learningpath->PML_LP->initialValue);

        if($scores['FS_LP'] >= $threshold_FS_LP || $selectA >=2 ){
            $visualProfile->Scoring->learningpath->FS_LP->recommended = true;
        }
        if($scores['PML_LP'] >= $threshold_PML_LP || $selectB >=2 ){
            $visualProfile->Scoring->learningpath->PML_LP->recommended = true;
        }
        if($scores['PA_CP'] < 10){
            $visualProfile->Scoring->careerpath->PA_CP->initialValue = false;
        }
        if($scores['PO_CP'] < 16){
            $visualProfile->Scoring->careerpath->PO_CP->initialValue = false;
        }
        if($scores['D_FL_CP'] < $scores['D_C_CP']){
            $visualProfile->Scoring->careerpath->D_FL_CP->initialValue = false;
        }
        if($scores['D_C_CP'] < $scores['D_FL_CP']){
            $visualProfile->Scoring->careerpath->D_C_CP->initialValue = false;
        }

        foreach ($visualProfile->Scoring->learningpath as $key => $item){
            $item->initialValue += $scores[$key];
        }
        foreach ($visualProfile->Scoring->careerpath as $key => $item){
            $item->value += $scores[$key];
        }

        return $visualProfile;
    }
    private function evaluateEEB($visualProfile, $answers){
        $answersMapped = Mapper::MapEntityCollection(AnalysisAnswerDto::class,
            $answers, [AnalysisQuestionDto::class, AnalysisQuestionDto::class]);

        $q5count = 0;

        foreach ($answersMapped as $answer){
            $deductionModified = str_replace("'", '"', $answer->deduction);
            $deduction = json_decode($deductionModified);

            switch ($deduction[0]){
                case 2:
                    $visualProfile->VisualProfile->EEB[] = $answer->answerDescription;
                    if($deduction[1] == 'A' || $deduction[1] == 'B' || $deduction[1] == 'D'){
                        $visualProfile->Scoring->careerpath->PO_CP->initialValue = false;
                    }
                    break;
                case 3:
                    $visualProfile->VisualProfile->EEB[] = $answer->answerDescription;
                    break;
                case 4:
                    if($deduction[1] == 'G' || $deduction[1] == 'H' || $deduction[1] == 'I' || $deduction[1] == 'J'){
                        $visualProfile->Scoring->intropath->FS_SI->initialValue = true;
                        $visualProfile->Scoring->intropath->FS_SI->value = 1;
                        $visualProfile->Scoring->intropath->PML_SI->initialValue = true;
                        $visualProfile->Scoring->intropath->PML_SI->value = 1;
                    }else{
                        $visualProfile->Scoring->intropath->FS_SI->value = 0;
                        $visualProfile->Scoring->intropath->PML_SI->value = 0;
                    }
                    break;
                case 5:
                    $q5count++;
                    break;
            }

        }

        if($q5count <= 5){
            $visualProfile->Scoring->careerpath->PA_CP->initialValue = false;
            if($q5count < 4){
                $visualProfile->Scoring->careerpath->PO_CP->initialValue = false;
            }
        }

        $visualProfile->VisualProfile->EEB = array_reverse($visualProfile->VisualProfile->EEB);

        return $visualProfile;
    }
    private function evaluateGSI($visualProfile, $answers){
        $answersMapped = Mapper::MapEntityCollection(AnalysisAnswerDto::class,
            $answers, [AnalysisQuestionDto::class, AnalysisQuestionDto::class]);

        $visualProfile->VisualProfile->GSI = array_map(function ($answer){
            return $answer->answerDescription;
        }, $answersMapped);

        return $visualProfile;
    }
    private function evaluatePSA($visualProfile, $answers){
        foreach ($answers as $answer){
            if($answer->deduction == "1-B"){
                if($visualProfile->Scoring->intropath->FS_SI->value != 1){
                    $visualProfile->Scoring->intropath->FS_SI->initialValue = false;
                }
                if($visualProfile->Scoring->intropath->PML_SI->value != 1){
                    $visualProfile->Scoring->intropath->PML_SI->initialValue = false;
                }
            }
        }

        $answersMapped = Mapper::MapEntityCollection(AnalysisAnswerDto::class,
            $answers, [AnalysisQuestionDto::class, AnalysisQuestionDto::class]);
        $PSAArr=[];
        foreach ($answersMapped as $answer){
            if ($answer->questionId==114)
                $PSAArr[]= $answer->answerDescription;
        }

        $visualProfile->VisualProfile->PSA = $PSAArr;

        return $visualProfile;
    }
    private function evaluateOld($categoryAnswers){}

    public function getUserSubmission($user_id){
        $submission = $this->professionalRepository->getUserSubmission($user_id);
        if($submission == null)
            throw new BadRequestException(trans('locale.submission_not_exist'));

        return $submission;
//        $finalResult = $submission->getFinalResult();
//        $result = explode(' ', $finalResult);
//        $levelResult = $result[0] ?? $finalResult;
//        return["result"=>$levelResult];
    }

    // public function getCategorySubmission($user_id,$category_code){
    //     $submission = $this->professionalRepository->getCategorySubmission($user_id,$category_code);
    //     if($submission == null)
    //         throw new BadRequestException(trans('locale.submission_not_exist'));

    //     return $submission;
    // }


    public function getSectionSubmissions($user_id, $category_id){
        // $SectionSubmissions=array();
        // dd($category_id);
        if($category_id == 1){
            $categories=['TIA','WPA','WSI'];
        }
        elseif($category_id == 2){
            $categories=['EEB','GSI'];
        }
        elseif($category_id == 3){
            $categories=['PSA'];
        }

        $SectionSubmissions = $this->professionalRepository->getSectionSubmissions($user_id, $categories);
        $sectionSubmissionsMapped = [];
        foreach($SectionSubmissions as $s){
            $s['visual_profile']=json_decode($s->visual_profile);
            array_push($sectionSubmissionsMapped, $s);
        }
        return $sectionSubmissionsMapped;
    }

    public function getCategoryUserSubmission($user_id, $category_code){
        $submission = $this->professionalRepository->getCategoryUserSubmission($user_id, $category_code);
        if($submission == null)
           throw new BadRequestException(trans('locale.submission_not_exist'));

        $submission['visual_profile']=json_decode($submission->visual_profile);
        return $submission;
    }

    public function checkSchoolData($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        return ['user' => $user, 'account' => $account, 'school' => $school];
    }
}