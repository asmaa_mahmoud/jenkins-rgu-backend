<?php

namespace App\ApplicationLayer\Professional\Dtos;

class ProductDto
{
    public $id;
    public $name;
    public $product_logo_image;
    public $product_side_image;
}