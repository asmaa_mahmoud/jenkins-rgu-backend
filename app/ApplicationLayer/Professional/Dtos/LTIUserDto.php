<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class LTIUserDto
{
    public $id;
    public $user_lti_id;

    public $roles=[];
    public $fname;
    public $lname;
    public $fullName;
    public $email;
    public $username;
    public $age;
    public $image_link;


}