<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 13/09/2018
 * Time: 11:46 AM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class LearningPathNamesDto
{
    public $id;
    public $name;
}