<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class TeacherCampusRoundDto
{
    public $id;
    public $campusName;
    public $campusIconUrl;
    public $campusHasSubModules;
    public $campus_id;
    public $startsAt;
    public $endsAt;
    public $status;

    public $noOfStudents;
    public $noOfClasses;
    public $duration;
    public $timeEstimate;

    public $round_name;


}