<?php

namespace App\ApplicationLayer\Professional\Dtos;


class LearnerProgressDto
{
    public $round_name;
    public $course_name;
    public $learner;
    // public $modules = [];
    public $completed = [];
    public $in_progress = [];
 


}