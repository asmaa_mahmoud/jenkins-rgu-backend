<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class UserCampusRoundDto
{
    public $id;
    public $campusName;
    public $startsAt;
    public $endsAt;
    public $status;
    public $round_name;
   }