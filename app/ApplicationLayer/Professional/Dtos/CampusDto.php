<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusDto
{
    public $id;
    public $name;
    public $description;
    public $iconUrl;
    public $noOfClasses;
    public $activities;
    public $defaultActivities;
    public $estimateTime;
    public $campusarticleid;
    public $timeEstimate;

}