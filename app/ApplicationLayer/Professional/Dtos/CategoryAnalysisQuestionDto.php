<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/17/2018
 * Time: 1:56 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CategoryAnalysisQuestionDto
{
    public $id;
    public $questionDescription;
    public $isMultiple;
    public $visualFormat;
    public $order;
    public $answers=[];
}