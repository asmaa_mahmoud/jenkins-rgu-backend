<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 10:30 AM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusStudentDto
{
    public $id;
    public $fname;
    public $lname;
    public $username;
    public $email;
    public $image_link;
    public $image;
    public $gender;
    public $role;
}