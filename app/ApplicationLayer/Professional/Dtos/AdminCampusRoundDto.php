<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class AdminCampusRoundDto
{
    public $id;
    public $campusName;
    public $campus_id;
    public $noOfClasses;
    public $startsAt;
    public $endsAt;
    public $status;
    public $students=[];
    public $noOfStudents;
    public $teachers=[];
    public $countOfTeachers;
    public $round_settings;
    public $timeEstimate;
   // public $campusRoundClasses=[];
    public $round_name;

}