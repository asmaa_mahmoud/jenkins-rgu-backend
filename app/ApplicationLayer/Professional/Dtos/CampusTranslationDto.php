<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 11/28/2018
 * Time: 6:32 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusTranslationDto
{
    public $id;
    public $languageCode;
    public $name;
    public $description;
    public $campusId;

}