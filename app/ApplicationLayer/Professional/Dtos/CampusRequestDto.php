<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusRequestDto
{
    public $name;
    public $description;
    public $startdate;
    public $enddate;
    public $numberofstudents;
    public $distributor_id;
    public $students = [];
    public $teachers = [];
    public $activities = [];
    public $iconUrl;
    public $change;
}