<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/9/2018
 * Time: 11:38 AM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class AnalysisAnswerTranslationDto
{
    public $id;
    public $language_code;
    public $answer_description;
    public $answer_id;

}