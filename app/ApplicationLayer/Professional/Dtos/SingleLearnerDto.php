<?php

namespace App\ApplicationLayer\Professional\Dtos;


class SingleLearnerDto
{
    public $id;
    public $user_name;
    public $hideBlocks = false;
    public $solved_missions_count;
    public $course_missions_count;
    public $xp;


}