<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/9/2018
 * Time: 11:30 AM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class AnalysisCategoryTranslationDto
{
    public  $id;
    public $name;
    public $analysis_category;

}