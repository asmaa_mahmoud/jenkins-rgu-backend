<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusRoundNamesDto
{
    public $id;//id of round
    public $campusName;//name of campus
    public $campusIconUrl;
    public $campusHasSubModules;
    public $campus_id;
    public $status;
    public $round_settings;
    public $round_name;
    public $introVideoWatched;
    public $progress;
    // public $solvedTasksCount;
    // public $allTasksCount;
}