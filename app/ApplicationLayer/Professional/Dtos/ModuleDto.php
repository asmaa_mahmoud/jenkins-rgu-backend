<?php

namespace App\ApplicationLayer\Professional\Dtos;


class ModuleDto
{
    public $id;
    public $name;
    public $missions_count;
    public $learners_solved_missions_count;
    public $learners_all_missions_count;

}