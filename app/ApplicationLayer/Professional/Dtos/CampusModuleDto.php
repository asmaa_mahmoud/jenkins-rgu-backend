<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 21/11/2018
 * Time: 4:22 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusModuleDto
{
    public $id;
    public $name;
    public $description;
    public $iconUrl;
    public $moduleCourseObjectives;
    public $moduleObjectives;
    public $submodules=[];
    public $materials=[];
    
}