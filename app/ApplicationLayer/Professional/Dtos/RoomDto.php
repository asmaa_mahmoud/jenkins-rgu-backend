<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class RoomDto
{
    public $id;
    public $name;
    public $BBBId;
    public $startsAt;
    public $endedAt;
    public $fileLink;
}