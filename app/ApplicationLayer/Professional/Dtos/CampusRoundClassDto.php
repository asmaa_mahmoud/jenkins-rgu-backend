<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusRoundClassDto
{
    public $id;

    public $startsAt;
    public $endsAt;

    public $weight;
    public $dueDate;
    //public $campusClass;
    public $campusClass;


}