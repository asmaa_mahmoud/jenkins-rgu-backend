<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusRoundRequestDto
{
    public $campus_id;
    public $round_id;
    public $starts_at;
    public $ends_at;
    public $round_settings;
    public $gmt_difference;
    public $students ;
    public $teachers ;
    public $classes ;
    public $tasks;
    public $sessions =[];
    public $rooms =[];
    public $round_name;

}