<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 25/02/2019
 * Time: 5:00 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class ProjectDto
{
    public $projectId;
    public $iconUrl;
    public $videoUrl;
    public $projectTitle;
    public $projectDescription;
    public $projectResources;
    public $projectTaskId;

}