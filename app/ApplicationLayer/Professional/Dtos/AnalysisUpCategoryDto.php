<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 04-Mar-19
 * Time: 11:04 AM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class AnalysisUpCategoryDto{

    public $id;
    public $title;
    public $description;
    public $iconUrl;
    public $categories = [];
}