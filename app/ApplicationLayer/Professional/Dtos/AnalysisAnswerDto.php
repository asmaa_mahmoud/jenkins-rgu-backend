<?php

namespace App\ApplicationLayer\Professional\Dtos;

class AnalysisAnswerDto
{
    public $id;
    public $answerDescription;
    public $factor;
    public $deduction;
    public $iconUrl;
    public $questionId;
    public $nextQuestionId;




}