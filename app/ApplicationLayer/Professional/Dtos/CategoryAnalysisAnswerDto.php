<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/17/2018
 * Time: 1:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CategoryAnalysisAnswerDto
{
    public $id;
    public $answerDescription;
    public $iconUrl;
    public $questionId;
    public $nextQuestionId;
    public $factor;

}