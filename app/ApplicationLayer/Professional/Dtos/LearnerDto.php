<?php

namespace App\ApplicationLayer\Professional\Dtos;


class LearnerDto
{
    public $firstName;
    public $lastName;
    public $image;
    public $learnerRank;
    public $learnerLevel;


}