<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class MinimizedCampusRoundDto
{
    public $id;
    public $campusName;
    public $campus_id;
    public $campusHasSubModules;
    public $noOfClasses;
    public $startsAt;
    public $endsAt;
    public $status;
   
    public $noOfStudents;
    
    public $countOfTeachers;
    public $round_settings;
    public $timeEstimate;

    public $round_name;

}