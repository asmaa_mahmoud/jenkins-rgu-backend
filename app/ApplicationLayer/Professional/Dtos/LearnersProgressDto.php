<?php

namespace App\ApplicationLayer\Professional\Dtos;


class LearnersProgressDto
{
    public $round_name;
    public $course_name;
    public $learners = [];


}