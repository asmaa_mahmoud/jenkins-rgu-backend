<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 13/03/2019
 * Time: 05:00 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class ColorPaletteDto
{
    public $id;
    public $colors;
}