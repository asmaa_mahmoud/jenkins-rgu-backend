<?php

namespace App\ApplicationLayer\Professional\Dtos;

class AnalysisCategoryDto
{
    public $id;
    public $iconUrl;
    public $categoryName;
    public $categoryCode;
    public $categoryDescription;
   // public $startingQuestionId;
    public $isRandom;
    public $upCategoryId;
    public $order;
    public $questionsCount;
}

