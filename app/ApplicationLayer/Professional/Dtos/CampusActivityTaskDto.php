<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 21/11/2018
 * Time: 4:22 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusActivityTaskDto
{
    public $id;
    public $task_id;
    public $campus_id;
    public $task_name;
    public $task_type;

}