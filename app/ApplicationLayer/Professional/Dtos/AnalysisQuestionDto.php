<?php

namespace App\ApplicationLayer\Professional\Dtos;

class AnalysisQuestionDto
{
    public $id;
    public $analysisCategory;
    public $questionDescription;
    public $isMultiple;
    public $visualFormat;
    public $order;
    public $answers=[];
    //public $translations=[];


    

}