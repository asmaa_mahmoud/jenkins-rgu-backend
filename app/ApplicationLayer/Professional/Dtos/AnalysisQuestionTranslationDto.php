<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/9/2018
 * Time: 11:32 AM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class AnalysisQuestionTranslationDto
{
    public $id;
    public $language_code;
    public $question_description;
    public $question_id;//Question this translation relates to

}