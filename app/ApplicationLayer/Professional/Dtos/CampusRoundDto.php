<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 01/11/2018
 * Time: 2:59 PM
 */

namespace App\ApplicationLayer\Professional\Dtos;


class CampusRoundDto
{
    public $id;
    public $campus;
    public $campusName;
    public $startsAt;
    public $endsAt;
    public $status;
    public $activities;
    public $defaultActivities;
    public $students;
    public $noOfStudents;
    public $round_name;


}