<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/16/2018
 * Time: 1:35 PM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularFilesDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularDependenciesDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\MissionStepDto;
use App\ApplicationLayer\Journeys\Dtos\StepContentDto;
use App\ApplicationLayer\Journeys\Dtos\MissionSectionDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragCellDto;
use App\ApplicationLayer\Journeys\Dtos\DragChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownsDto;
use App\ApplicationLayer\Journeys\Dtos\stepDropdownsChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepMcqQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\McqQuestionChoicesDto;
use App\ApplicationLayer\Journeys\Dtos\StepMatchQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\MatchKeysDto;
use App\ApplicationLayer\Journeys\Dtos\MatchValueDto;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Question;
use App\ApplicationLayer\Professional\Dtos\ProjectDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\ApplicationLayer\Common\LessonCommonService;
use App\ApplicationLayer\Common\TaskCommonService;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\DomainModelLayer\Professional\Campus;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use Carbon\Carbon;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularAnswersDto;


class StudentService
{

    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonService;
    private $lessonCommonService;
    private $taskCommonService;
    private $campusService;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        $this->taskCommonService = new TaskCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        //$this->lessonService = new LessonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        //$this->campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

    }
    public function tasks_cmp($task1,$task2){
        if ($task1->order === $task2->order) {
            return 0;
        }
        if ($task1->order < $task2->order) {
            return -1;
        }else{
            return 1;
        }
    }

    public function getHtmlMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id,$is_default=true){
        
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
            $campusHasSubModules = $campus->hasSubModules();
        }
        
        $campus = mapper(Campus::class)->find($campus_id);
        $activity = mapper(DefaultActivity::class)->find($activity_id);
        $mission = mapper(MissionHtml::class)->find($mission_id);
        $task = mapper(Task::class)->find($mission->task_id);
        $user = mapper(User::class)->find($user_id);       
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        
        if($mission == null)
            throw new BadRequestException(trans('locale.html_mission_not_exist'));

        $task = $mission->getTask();
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules);        
        if($campus_id!==null){  
            $this->updateStudentLastSeen($user_id,$campus_id,$activity_id,$task->getId(),$is_default);
        }
        if($learning_path_id != null)
            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
        elseif ($campus_id != null)
            $taskProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id,$is_default);
        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;

        $missionDto =  Mapper::MapClass(MissionHtmlDto::class,$mission);
        $missionDto->task_id = $task->getId();
        $missionDto->weight = $mission->getWeight();
        $missionDto->moduleType = $mission->getLessonType();
        $missionDto->miniProjectType = $mission->miniProjectType();
        $practiceTask = $mission->getPracticeTask();
        if($practiceTask != null){
            if($practiceTask->getMissions()->first() != null){
                $practice_mission = $practiceTask->getMissions()->first();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getName(), "practice_task_type" => "mission","mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            elseif($practiceTask->getMissionsHtml()->first() != null){
                $practice_mission = $practiceTask->getMissionsHtml()->first();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getTitle(), "practice_task_type" => "html_mission","mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            elseif($practiceTask->getMissionCoding() != null){
                $practice_mission = $practiceTask->getMissionCoding();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getTitle(), "practice_task_type" => "coding_mission","mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            elseif($practiceTask->getMissionEditor() != null){
                //editor_type=$practice_mission->getType()
                $practice_mission = $practiceTask->getMissionEditor();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getTitle(), "practice_task_type" => "editor_mission","editor_type"=>$practice_mission->getType(),"mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            else{
                $practice_quiz = $practiceTask->getQuizzes()->first();
                return ["practice_task_id" => $practice_quiz->getId(), "practice_task_name" => $practice_quiz->getTitle(), "practice_task_type" =>  $practice_quiz->getType()->getName(),"mission_task_id"=>$practice_quiz->getTask()->getId()];
            }
        }
        
        $missionDto->passed = $passed;

        $missionDto->withSteps = $mission->getWithSteps()==1?true:false;

        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;

        $missionCurrentSection = $this->professionalRepository->getTaskCurrentStep($user_id, $task->getId(), $activity->getId(), $campus->getId());
        // $currentSection = $missionCurrentSection ? $missionCurrentSection->getCurrentSection() : 0;
        // dd($mission->getId());
        if($missionCurrentSection){
            $currentSection=$this->lessonCommonService->checkIfSectionExist($missionCurrentSection,$mission->getId());
       
        }else{
            $currentSection=0;
        }

        $missionDto->currentSection=$currentSection;
        
        $missionDto->sections = array();
        foreach ($mission->getSections() as $section){
            
            $missionSectionDto =  Mapper::MapClass(MissionSectionDto::class,$section);
            $missionSectionDto->bricks = array();
            foreach ($section->getSteps() as $step){
                $missionStepDto =  Mapper::MapClass(MissionStepDto::class,$step);
                $type=$missionStepDto->brickType;
                if($type=="text_photo_sidebyside" || $type=="text_photo_stacked" || $type=="text_gallery" || $type=="quote" || $type=="callouts" || $type=="code_snippet" || $type=="flip_cards" || $type=="flip_cards_slider"|| $type=="photo_gallery" || $type=="audio" || $type=="video" || $type=="freelancing_submission"|| $type=="mobile_app_store")
                {
                    $stepContentDto = Mapper::MapClass(StepContentDto::class,$step);
                    if($type=="code_snippet"){
                        $practiceCode = json_decode($step->practice_code);

                        //Check Editor Mode 
                        $editorMode = ($step->practice_code && !!array_filter( (array) $practiceCode)) ? true : false;

                        $stepContentDto->editorMode = $editorMode;
                        $stepContentDto->html = ($practiceCode && $practiceCode->html) ? $practiceCode->html:null;
                        $stepContentDto->css = ($practiceCode && $practiceCode->css) ? $practiceCode->css:null;
                        $stepContentDto->js = ($practiceCode && $practiceCode->js) ? $practiceCode->js:null;

                    }
                    array_push($missionSectionDto->bricks,$stepContentDto);
                }elseif($type=="free"){
                    array_push($missionSectionDto->bricks,Mapper::MapClass(MissionStepDto::class,$step));
                }elseif($type=="github_form" || $type=="report"){
                    $task = $step->getMissionHtml()->getTask();
                    $user_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity_id,true);
                    $stepContentDto = Mapper::MapClass(StepContentDto::class,$step);
                    if($user_progress !=null){
                        $project_url = $user_progress->user_code;
                        $stepContentDto->project_url = $project_url;
                    }
                    array_push($missionSectionDto->bricks,$stepContentDto);
                }
                elseif($type=="quizbricks"){
                    
                    $questionType=$step->getQuestionTask()->question->question_type;
                    
                    if($questionType == "dropdown")
                    {
                        $dropdownStep=$this->lessonCommonService->getDropdownStep($step,$user,$activity,$campus,$mission,$task);
                        array_push($missionSectionDto->bricks,$dropdownStep);
                    }elseif($questionType == "sequence_match")
                    {
                        $dragStep=$this->lessonCommonService->getDragStep($step,$user,$activity,$campus,$mission,$task);
                        array_push($missionSectionDto->bricks,$dragStep);
                    }
                    elseif($questionType == "question")
                    {
                        $mcqQuestionStep=$this->lessonCommonService->getMcqQuestionStep($step,$user,$activity,$campus,$mission,$task);
                        array_push($missionSectionDto->bricks,$mcqQuestionStep);
                    }
                    elseif($questionType == "match")
                    {
                        $matchQuestionStep=$this->lessonCommonService->getMatchQuestionStep($step,$user,$activity,$campus,$mission,$task);
                        array_push($missionSectionDto->bricks,$matchQuestionStep);
                    }

                }


            }
            //Check where to add section by type

            if($section->getType() == 'normal'){
                array_push($missionDto->sections,$missionSectionDto);
            }else{
                //Other type (guide)
                array_push($missionDto->guide,$missionSectionDto);
            }
        }

        // Add welcome_module flag to the returned mission
        $moduleType = $this->professionalRepository->getCampusClass($campus_id, $activity_id, $is_default)->lessonType();
        $missionDto->is_welcome_module = $moduleType == 2;
        if($missionDto->currentSection > count($mission->getSections()))
           $missionDto->currentSection=count($mission->getSections());

        if($mission->getHtmlMissionType() == 'mini_project' && $mission->getQuiz())
            $missionDto->quiz_id = $mission->getQuiz()->getId();
        else
            $missionDto->quiz_id = null;
        
        $missionDto->modalAnswerUrl = ( $mission->getHtmlMissionType() == 'mini_project' ) ? $mission->getModelAnswerLink() : null;
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;
      // dd($missionSectionDto);

        // $missionDto->steps = array();
        // foreach ($mission->getSteps() as $step){
        //     array_push($missionDto->steps,Mapper::MapClass(MissionStepDto::class,$step));
        // }
        // usort($missionDto->steps,array($this, 'tasks_cmp'));
            
        return $missionDto;
    }

    
    private function sortByOrder($a, $b) {
        if($a->order == $b->order){ return 0 ; }
        return ($a->order < $b->order) ? -1 : 1;
    }

    public function getHtmlMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id,$round_id, $user_id=null,$is_default=true){
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist"));

        $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
        
    }

    public function getProjectData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id,$is_default=true){
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
            $campusHasSubModules = $campus->hasSubModules();
        }

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);

        if($mission == null)
            throw new BadRequestException(trans('locale.project_not_exist'));


        $task = $mission->getTask();
         //Check Locker
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules); 

        if($learning_path_id != null)
            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
        elseif ($campus_id != null)
            $taskProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id,$is_default);
        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;

        $project=$task->getProject()->first();
        $missionDto = Mapper::MapClass(ProjectDto::class,$project);
        
        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;

        $missionDto->missionId = $mission->getId();
        $missionDto->isMiniProject = $mission->getIsMiniProject();
        
        
        $allProjectResources = $project->getProjectResources();
        $projectResources = [];
        foreach ($allProjectResources as $projectResource) {
            array_push($projectResources,  ["id"=> $projectResource->getId(),"title" => $projectResource->getTitle(),"url" => $projectResource->getResourceUrl()]);  
        }
        $missionDto->projectResources = $projectResources;
        $missionDto->projectTaskId = $task->getId();

        $missionDto->passed = $passed;
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;
        // $missionDto->steps = array();
        // foreach ($mission->getSteps() as $step){
        //     array_push($missionDto->steps,Mapper::MapClass(MissionStepDto::class,$step));
        // }

        return $missionDto;
    }

    public function getMissionData($learning_path_id=null, $campus_id=null,$activity_id, $mission_id, $user_id=null,$is_default=true){
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
            $campusHasSubModules = $campus->hasSubModules();
        }

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        if($user_id != null){
            $user = $this->accountRepository->getUserById($user_id);
            if ($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            if($campus_id!==null){
                $this->updateStudentLastSeen($user_id,$campus_id,$activity_id,$mission->getTask()->getId(),$is_default);
            }
        }
        //Check Locker
        $task = $mission->getTask();
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules);  

        $openedTask = true;//TODO::check for any closed data
        $missionDto =  Mapper::MapClass(MissionDto::class,$mission,[MissionScreensDto::class]);

        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;

        $missionDto->type = $mission->getType();
        if($missionDto->type == "text")
            $missionDto->textualBlocklyCode = $mission->showModelAnswer();

        if($mission->getProgrammingLanguageType() != null)
            $missionDto->programmingLanguageType =  $mission->getProgrammingLanguageType()->getName();
        $missionDto->blockly_rules = $mission->blockly_rules;
        if($user_id != null) {
            $user = $this->accountRepository->getUserById($user_id);
            if($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            $account = $user->getAccount();
            if ($account == null)
                throw new BadRequestException(trans('locale.no_user_id'));

            $modelAnswerUnlockedForUser = $this->journeyRepository->getModelAnswerUnlocked($mission->getId(),$user->getId());
            $userRoles = $user->getRoles();
            if($mission->getType() == "mcq"){
                $choice_ids = array();
                $missionQuestions = $mission->getMissionMcq()->getQuestions();
                foreach ($missionQuestions as $missionQuestion){
                    $choices = $missionQuestion->getChoices();
                    foreach($choices as $choice){
                        if($choice->isCorrect())
                            array_push($choice_ids, $choice->getId());
                    }

                }
                $hasModelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());
                $missionDto->ModelAnswer = $mission->showModelAnswer();
                if($openedTask){
                    $missionDto->model_choice_ids = $choice_ids;
                }
                else{
                    if($modelAnswerUnlockedForUser != null || $hasModelAnswer != false){
                        $missionDto->model_choice_ids = $choice_ids;
                    }
                    else{
                        $missionDto->model_choice_ids = null;
                    }
                }
            }
            else if($modelAnswerUnlockedForUser == null){
                $missionDto->model_choice_ids = null;
                foreach ($userRoles as $role) {
                    $rolePermissions = $role->getPermissions();
                    foreach ($rolePermissions as $permission) {
                        if($permission->getName() == 'show_ModelAnswer')
                        {
                            $missionDto->ModelAnswer = $mission->showModelAnswer();
                            break;
                        }

                    }
                }
                if($this->hasAccessToModelAnswer($user))
                    $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
            else{
                $missionDto->model_choice_ids = null;
                $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
        }
        else {
            $missionDto->model_choice_ids = null;
            $missionDto->ModelAnswer = $mission->showModelAnswer();
        }
        $missionDto->descriptionSpeech = $mission->getDescriptionSpeech();
        $missionDto->task_id = $mission->getTask()->getId();
        $missionDto->HintModelAnswer = $mission->showModelAnswer();
        $missionDto->GoldenTime = $mission->getGoldenTime();
        $missionDto->MissionState = $mission->getMissionState();
        $missionDto->ModelAnswerNumberOfBlocks = $mission->getModelAnswerNumberOfBlocks();
        $activity=$mission->getTask()->getActivityTask()->getDefaultActivity();
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));


        $userProgress=$this->professionalRepository->getUserTaskProgressActivity($mission->getTask(),$user_id,$activity->id,$is_default);
        if($userProgress == null){
            $missionDto->userTrials = 0;
        }
        else{
            $missionDto->userTrials = $userProgress->getNoTrials();
        }

        $missionDto->maxNoOfTrials = 3;
        $missionDto->sceneName = $mission->getSceneName();
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;

        return $missionDto;

    }

    public function getHandoutMissionData($campus_id=null,$activity_id, $mission_id, $user_id=null,$is_default=true){
        // $round->getCampus_id()
        if ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));
            $campusHasSubModules = $campus->hasSubModules();
        }
        
        $campus = mapper(Campus::class)->find($campus_id);
        $activity = mapper(DefaultActivity::class)->find($activity_id);
        $mission = mapper(MissionHtml::class)->find($mission_id);
        $user = mapper(User::class)->find($user_id);       
        // $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);

        if($mission == null)
            throw new BadRequestException(trans('locale.html_mission_not_exist'));
        if($mission->htmlMissionType() != 'handout')
            throw new BadRequestException(trans('locale.html_mission_not_exist'));

        $task = $mission->getTask();
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules);        
        if($campus_id!==null){  
            $this->updateStudentLastSeen($user_id,$campus_id,$activity_id,$task->getId(),$is_default);
        }

        if($campus_id != null)
            $taskProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id,$is_default);
        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;

        $missionDto =  Mapper::MapClass(MissionHtmlDto::class,$mission);
        $missionDto->task_id = $task->getId();
        $missionDto->weight = $mission->getWeight();
        $missionDto->moduleType = $mission->getLessonType();
        $missionDto->miniProjectType = $mission->miniProjectType();
        $practiceTask = $mission->getPracticeTask();
        if($practiceTask != null){
            if($practiceTask->getMissions()->first() != null){
                $practice_mission = $practiceTask->getMissions()->first();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getName(), "practice_task_type" => "mission","mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            elseif($practiceTask->getMissionsHtml()->first() != null){
                $practice_mission = $practiceTask->getMissionsHtml()->first();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getTitle(), "practice_task_type" => "html_mission","mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            elseif($practiceTask->getMissionCoding() != null){
                $practice_mission = $practiceTask->getMissionCoding();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getTitle(), "practice_task_type" => "coding_mission","mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            elseif($practiceTask->getMissionEditor() != null){
                //editor_type=$practice_mission->getType()
                $practice_mission = $practiceTask->getMissionEditor();
                $missionDto->practiceTask = ["practice_task_id" => $practice_mission->getId(), "practice_task_name" => $practice_mission->getTitle(), "practice_task_type" => "editor_mission","editor_type"=>$practice_mission->getType(),"mission_task_id"=>$practice_mission->getTask()->getId()];
            }
            else{
                $practice_quiz = $practiceTask->getQuizzes()->first();
                return ["practice_task_id" => $practice_quiz->getId(), "practice_task_name" => $practice_quiz->getTitle(), "practice_task_type" =>  $practice_quiz->getType()->getName(),"mission_task_id"=>$practice_quiz->getTask()->getId()];
            }
        }
        
        $missionDto->passed = $passed;

        $missionDto->withSteps = $mission->getWithSteps()==1?true:false;

        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;

        // Add welcome_module flag to the returned mission
        $moduleType = $this->professionalRepository->getCampusClass($campus_id, $activity_id, $is_default)->lessonType();
        $missionDto->is_welcome_module = $moduleType == 2;
        
        $missionDto->pdf_link = $mission->getPdfLink();
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;
        return $missionDto;
    }

    public function getHandoutMissionNext($campus_id=null, $activity_id ,$submodule_id, $mission_id, $round_id, $user_id=null, $is_default=true){
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist"));
        $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
    }

    public function getMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id,$round_id, $user_id=null,$is_default=true){
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

       $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);

    }
    private function isBoostersOpenedInModule($user,$campus,$activity){
        $openBoosters=true;
        foreach ($activity->getMainTasks() as $mainTask) {
            $userProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($mainTask,$user->id,$campus->id,$activity->id,true);
            if($userProgress!==null&&$userProgress->first_success==null){
                $openBoosters=false;
                break;
            }
        }
        return $openBoosters;
    }
    private function checkUserDataForNextTask($learning_path_id=null, $campus_id=null, $activity_id , $mission, $round_id,$user_id=null,$is_default=true){
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
        }
        $showClasses=true;
        if($round_id!=null){
            $round = $this->professionalRepository->getRoundById($round_id);
            if ($round == null)
                throw new BadRequestException(trans('locale.round_not_exist'));//TODO::translation
            $showClasses=$round->showClasses();
        }


        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));
        $task = $mission->getTask();
        $order = $this->journeyRepository->getActivityTaskOrder($task, $activity_id, $is_default)->getOrder();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $isBoostersOpened=true;
        if($user->isStudent()){
            $isBoostersOpened=$this->isBoostersOpenedInModule($user,$campus,$activity);
        }
        $last_task = $this->journeyRepository->getLastMainTaskInActivity($activity_id, $is_default);//don't include 
        /*if($isBoostersOpened)
            $last_task = $this->journeyRepository->getLastTaskInActivity($activity_id, $is_default);//include boosters
        else
            $last_task = $this->journeyRepository->getLastMainTaskInActivity($activity_id, $is_default);//don't include boosters*/

        //Get hideBlocks status only for learners and always showen for admin and teacher
        if($user->isStudent()){
            $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);
            if($campusUserStatus != null){
                $hide_blocks = $campusUserStatus->hide_blocks == 1 ? true :false;    
            }else{
                $hide_blocks = false;
            }
        }else{
            $hide_blocks = false;
        }
        
        $data['last_task']=$last_task;
        $data['order']=$order;
        $data['task']=$task;
        $data['showClasses']=$showClasses;
        $data['hideBlocks']=$hide_blocks;

        return $data;

    }
    private function checkNextTask($order,$activity_id, $is_default,$showClasses,$hideBlocks,$task=null){
        $next_task = $this->journeyRepository->getTaskInActivityByOrder($order, $activity_id, $is_default);
       
        if ($next_task == null)
            return null;
            // throw new BadRequestException(trans('locale.next_task_not_exist'));

        if ($task != null)
            $lastTaskType = $task->getActivityTask()->booster_type == 1 ? 'Booster':'Main_Mission';

        //Check ShowClasses and HideBlocks
        
        $task_type = $next_task->getTaskNameAndType()['task_type'];

        if($task_type == 'mission' && $hideBlocks){
            return $this->checkNextTask($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
            
        }
        if($task_type == 'html_mission' && !$showClasses){
            return $this->checkNextTask($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
        }
        if($task_type == 'mini_project' && !$showClasses){
            return $this->checkNextTask($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
        }
        

        $nextTaskIsBooster = $next_task->getActivityTask()->booster_type == 1 ? true:false;

        //If Booster --Next--> Booster else Main Mission --Next--> Main Mission
        if($lastTaskType == 'Booster'){

            if($nextTaskIsBooster){
                return $next_task;
            }else{
                return $this->checkNextTask($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
            }
        }elseif($lastTaskType == 'Main_Mission'){

            if(!$nextTaskIsBooster){
                return $next_task;
            }else{
                return $this->checkNextTask($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
            }
        }

        return null;
    }

    private function getNextTask($task, $last_task, $order, $activity_id, $is_default, $showClasses, $hideBlocks){
        if($task->getId() != $last_task->getId()) {
            $order=intval($order) + 1;
            $lastTaskOrder = $this->journeyRepository->getActivityTaskOrder($last_task, $activity_id, $is_default)->getOrder();
            while(true) {
                // $next_task = $this->journeyRepository->getTaskInActivityByOrder($order, $activity_id, $is_default);
                // if ($next_task == null)
                //     throw new BadRequestException(trans('locale.next_task_not_exist'));
                
                $next_task = $this->checkNextTask($order, $activity_id, $is_default,$showClasses,$hideBlocks,$task);    //dd($next_task->getTaskNameAndType());
                
                if($next_task == null)
                    return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null/*, 'nextLesson' => $nextClass*/];

                if ($next_task->getMissions()->first() != null) {
                    $next_mission = $next_task->getMissions()->first();
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(), "next_task_type" => "mission"];
                } elseif (($showClasses && $next_task->getMissionsHtml()->first() != null  &&$next_task->getMissionsHtml()->first()->getIsMiniProject()=='0')||
                    ($next_task->getMissionsHtml()->first() != null  &&$next_task->getMissionsHtml()->first()->getIsMiniProject()=='1')){
                    $next_mission = $next_task->getMissionsHtml()->first();
                    $handoutMisssionFlag = $next_task->getMissionsHtml()->first()->miniProjectType() == 'handout';
                    if ($next_mission->getIsMiniProject() == '0' && !$handoutMisssionFlag)
                        return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "html_mission"];
                    elseif ($next_mission->getIsMiniProject() == '0' && $handoutMisssionFlag)
                        return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "handout"];
                    else
                        return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "mini_project"];
                } elseif ($next_task->getMissionCoding() != null) {
                    $next_mission = $next_task->getMissionCoding();
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "coding_mission"];
                } elseif ($next_task->getMissionEditor() != null) {
                    $next_mission = $next_task->getMissionEditor();
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "editor_mission"];
                } elseif ($next_task->getMissionAngular() != null) {
                    $next_mission = $next_task->getMissionAngular();
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "angular_mission"];
                } elseif($next_task->getQuizzes()->first() !=null) {
                    $next_quiz = $next_task->getQuizzes()->first();
                    return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(), "next_task_type" => $next_quiz->getType()->getName()];
                }else{
                    $order++;
                    if($order>$lastTaskOrder){
                        //$nextClass = $this->getNextClass($round_id, $activity_id, $is_default);
                        return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null/*, 'nextLesson' => $nextClass*/];
                    }

                }
            //                    else
            //                        return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
            }
        }
        else
        {
            //$nextClass = $this->getNextClass($round_id, $activity_id, $is_default);
            return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
        }
    }

    public function getQuizNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id, $round_id,$user_id=null,$is_default=true){
        $mission = $this->journeyRepository->getquiz($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
    }

    public function checkMissionAuthorization($learning_path_id, $activity_id , $mission_id, $user_id,$is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);

        if($learning_path == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation

        $checkUserInLearningPath=$this->professionalRepository->checkIfStudentInLearningPath($learning_path_id,$user_id);

        if(!$checkUserInLearningPath)
            return ["mission_authorization" => false];


        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        
        //activity in learningPath
        $activityInLearnPath=$this->professionalRepository->checkIfActivityInLearningPath($learning_path_id,$activity_id,$is_default);

        if(!$activityInLearnPath)
            throw new UnauthorizedException(trans('locale.activity_not_in_learning_path'));//TODO::translation
        return ["mission_authorization" => true];
    }

    public function checkOpenedModelAnswer($user, $mission_id){
        $returned = false;
        $isUser = false;
        $isStudent = false;
        $isChild = false;
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;

            if($role->getName() == 'user' || $role->getName() == 'invited_Individual')
                $isUser = true;

            if($role->getName() == 'student')
                $isStudent = true;

            if($role->getName() == 'child')
                $isChild = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getJourneys();
            if(count($journeys) > 0){
                foreach ($journeys as $invJourney){
                    if($mission->getTask()->getTaskOrder() != null){
                        if($invJourney->getJourney()->getId() == $mission->getTask()->getTaskOrder()->getJourney()->getId()){
                            if($isUser){
                                if($invJourney->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
            $activities = $invitation_subscription->getInvitation()->getDefaultActivities();
            if(count($activities) > 0){
                foreach ($activities as $invActivity){
                    if($mission->getTask()->getActivityTask() != null){
                        if($invActivity->getDefaultActivity()->getId() == $mission->getTask()->getActivityTask()->getDefaultActivity()->getId()){
                            if($isUser){
                                if($invActivity->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
        }
        return $returned;
    }

    public function hasAccessToModelAnswer(User $user)
    {
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription == null)
            return false;
        $invitation = $invitation_subscription->getInvitation();
        $journey = $invitation->getJourneys()->first();
        if($journey == null)
            return false;
        if(!$journey->getProgressLock())
            return true;
        return false;
    }

    public function getCodingMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true){
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
            $campusHasSubModules = $campus->hasSubModules();
        }

        $mission = $this->journeyRepository->getCodingMissionById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $task = $mission->getTask();
        if($campus_id!==null){
            $this->updateStudentLastSeen($user_id,$campus_id,$activity_id,$task->getId(),$is_default);
        }

        //Check Locker
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules); 

        if($learning_path_id != null)
            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
        elseif ($campus_id != null)
            $taskProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id,$is_default);

        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;
        $roles = $user->getRoles();
        $isTeacherOrAdmin = false;
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher"||$teacherRole->getName() == "school_admin")
                $isTeacherOrAdmin = true;
        }

        $missionDto =  Mapper::MapClass(MissionCodingDto::class,$mission, [MissionScreensDto::class]);
        
        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;     

        $missionDto->passed = $passed;
        $missionDto->task_id = $task->getId();
        //$missionDto->screenshots = $mission->getScreenshots();
        if($campus_id!=null){
            $previous_missionprogress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);
            if($previous_missionprogress!==null){
                if($previous_missionprogress->getUserCode()){
                $userCode=json_decode($previous_missionprogress->getUserCode(),true);
                foreach ($userCode as $instance){
                    if($instance['language']=="python")
                        $missionDto->user_code=$instance["code"];

                }
            }
            }

        }
        if($isTeacherOrAdmin===true)
            $missionDto->model_answer = $mission->getModelAnswer();

        $resources=[];
        foreach ($task->getResources() as $resource){
            $resourceDto['id']=$resource->getId();
            $resourceDto['is_url']=$resource->getIsUrl();
            if($resource->getIsUrl()==0){
                $path=explode("/",$resource->getPath());
                $resourceDto['path']=end($path);
            }else {
                $resourceDto['path'] = $resource->getPath();
            }
            $resourceDto['download_link'] = "https://proadmin.rgp-dev.com/assets/pro_resources/python/".$missionDto->vplId."/".$resourceDto['path'];
            $resources[]=$resourceDto;
        }
        $missionDto->resources=$resources;
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;

        return $missionDto;
    }

    public function getCodingMissionNext($learning_path_id=null, $campus_id=null, $activity_id, $submodule_id,$mission_id, $round_id,$user_id,$is_default=true){
        

        $mission = $this->journeyRepository->getCodingMissionById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
    }

    public function getMissionEditorData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true){
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
            $campusHasSubModules = $campus->hasSubModules();
        }

        $mission = $this->journeyRepository->getMissionEditorById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $task = $mission->getTask();

        //Check Locker
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules); 

        if($campus_id!==null){
            $this->updateStudentLastSeen($user_id,$campus_id,$activity_id,$task->getId(),$is_default);
        }
        if($learning_path_id != null)
            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
        elseif ($campus_id != null)
            $taskProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id,$is_default);

        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;
        $roles = $user->getRoles();
        $isTeacherOrAdmin = false;
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher"||$teacherRole->getName() == "school_admin")
                $isTeacherOrAdmin = true;
        }

        $missionDto =  Mapper::MapClass(MissionEditorDto::class, $mission, [MissionScreensDto::class]);
        $missionDto->passed = $passed;

        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;

        $missionDto->task_id = $task->getId();
        $missionDto->tests = $mission->getTests();
        if($campus_id!=null){
            $previous_missionprogress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);
            if($previous_missionprogress!==null){
                $userCode=json_decode($previous_missionprogress->getUserCode(),true);
                //dd($userCode);
                if($userCode!=null){
                    foreach ($userCode as $instance){
                        if($instance['language']=="html")
                            $missionDto->user_html=$instance["code"];
                        else if($instance['language']=="css")
                            $missionDto->user_css=$instance["code"];
                        else if($instance['language']=="js")
                            $missionDto->user_js=$instance["code"];
                    }
                }

            }


        }

            $missionDto->model_answer = $mission->getModelAnswer();
            $missionDto->model_answer_detailed = $mission->getModelAnswerDetailed();


        $resources=[];
        foreach ($task->getResources() as $resource){
            $resourceDto['id']=$resource->getId();
            $resourceDto['is_url']=$resource->getIsUrl();
            if($resource->getIsUrl()==0){
                $path=explode("/",$resource->getPath());
                $resourceDto['path']=end($path);
           }else {
                $resourceDto['path'] = $resource->getPath();
            }
            $resources[]=$resourceDto;
        }
        $missionDto->resources=$resources;
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;

        return $missionDto;
    }

    public function getMissionEditorNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id, $round_id,$user_id,$is_default=true){
        $mission = $this->journeyRepository->getMissionEditorById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

       $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
    }

    public function getAngularMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true){
        if($learning_path_id != null) {
            $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
            if ($learning_path == null)
                throw new BadRequestException(trans('locale.learning_path_not_exist'));//TODO::translation
        }
        elseif ($campus_id != null) {
            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));//TODO::translation
            $campusHasSubModules = $campus->hasSubModules();
        }

        $mission = $this->journeyRepository->getMissionAngularById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $task = $mission->getTask();
        //Check Locker
        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules); 

        if($campus_id!==null){
            $this->updateStudentLastSeen($user_id,$campus_id,$activity_id,$task->getId(),$is_default);
        }
        if($learning_path_id != null)
            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
        elseif ($campus_id != null)
            $taskProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id, $activity_id,$is_default);

        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;
        $roles = $user->getRoles();
        $isTeacherOrAdmin = false;
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher"||$teacherRole->getName() == "school_admin")
                $isTeacherOrAdmin = true;
        }
    

        $missionDto = Mapper::MapClass(MissionAngularDto::class,$mission,[MissionScreensDto::class]);

        //check if is Booster
        $is_booster = $mission->getTask()->getActivityTask()->booster_type == 1 ? true:false ;
        $missionDto->is_booster = $is_booster;

        $missionDto->dependencies = Mapper::MapEntityCollection(MissionAngularDependenciesDto::class, $mission->getDependencies());
        $missionDto->type = 'angular';
        $missionDto->passed = $passed;
        $missionDto->task_id = $task->getId();
        if($campus_id!=null){
            $previous_missionprogress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);
      
            $initialFiles=$mission->getFiles();
            foreach($initialFiles as $file){
                if($previous_missionprogress!=null){
                    $AngularUserAnswer = $this->professionalRepository->getMissionAngularUserAnswerByName($previous_missionprogress->id,$file->name);
                    if($AngularUserAnswer){
                        $userAnswer = $AngularUserAnswer->data;
                    }else{
                        $userAnswer = null;
                    }
                }else{
                    $userAnswer=null;
                }

                array_push($missionDto->files, [
                    'id' => $file->id,
                    'name' => $file->name,
                    'type' => $file->type,
                    'data' => $file->data,
                    'model_answer' => $file->getModelAnswer(),
                    'user_answer' => $userAnswer
                ]);
            }
        }


        $resources=[];
        foreach ($task->getResources() as $resource){
            $resourceDto['id']=$resource->getId();
            $resourceDto['is_url']=$resource->getIsUrl();
            if($resource->getIsUrl()==0){
                $path=explode("/",$resource->getPath());
                $resourceDto['path']=end($path);
           }else {
                $resourceDto['path'] = $resource->getPath();
            }
            $resources[]=$resourceDto;
        }
        $missionDto->resources=$resources;
        $missionDto->campusHasSubModules = $campusHasSubModules ?? 0;

        return $missionDto;
    }

    public function getAngularMissionNext($learning_path_id=null, $campus_id=null, $activity_id , $submodule_id,$mission_id, $round_id,$user_id,$is_default=true){
        $mission = $this->journeyRepository->getMissionAngularById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task_id=$mission->getTask()->getId();

        return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
    }

    public function getNextClass($round_id, $activity_id, $is_default=true){
        $round = $this->professionalRepository->getRoundById($round_id);
        if ($round == null)
            throw new BadRequestException(trans('locale.round_not_exist'));
        $campus_id=$round->getCampus_id();

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $order=$this->professionalRepository->getCampusClass($campus_id,$activity_id,$is_default)->getOrder();
        $nextClass=$this->professionalRepository->getClassInCampusByOrder(intval($order)+1,$campus_id);
        
        if($nextClass==null)
            return null;
        $nextRoundClass=$this->professionalRepository->getCampusRoundClass($round_id,$nextClass->getId());
        $classDto['id']=$nextClass->getId();
        $classDto['name']=$nextClass->getName();
        $classDto['starts_at']=$nextRoundClass->getStartsAt();
        $classDto['ends_at']=$nextRoundClass->getEndsAt();
        $classDto['duration']=$nextRoundClass->getDuration();
        if($is_default) {
            $classDto['description'] = $nextClass->getDefaultActivity()->getDescription();
            $classDto['activity_id']=$nextClassActivityId=$nextClass->getDefaultActivity()->getId();
            $classDto['technical_details']=$nextClass->getDefaultActivity()->getTechnicalDetails();
        }
        else {
            $classDto['description'] = $nextClass->getActivity()->getDescription();
            $classDto['activity_id']=$nextClassActivityId = $nextClass->getActivity()->getId();
            $classDto['technical_details']=$nextClass->getActivity()->getTechnicalDetails();
        }

        $task=$this->journeyRepository->getFirstTaskIdInActivity($nextClassActivityId)->getTask();

        if($task == null)
            throw new BadRequestException(trans('locale.first_task_not_exist'));

        if($task->getMissions()->first() != null) {
            $next_mission = $task->getMissions()->first();
            $classDto["first_task"]= ["task_id"=>$task->getId(),"next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(), "next_task_type" => "mission"];
        }
        elseif($task->getMissionsHtml()->first() != null) {
            $next_mission = $task->getMissionsHtml()->first();
            if($next_mission->getIsMiniProject()=='0')
                $classDto["first_task"] =["task_id"=>$task->getId(),"next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "html_mission"];
            else
                $classDto["first_task"] =["task_id"=>$task->getId(),"next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "mini_project"];

        }
        elseif($task->getMissionCoding() != null) {
            $next_mission = $task->getMissionCoding();
            $classDto["first_task"]= ["task_id"=>$task->getId(),"next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "coding_mission"];
        }
        elseif($task->getMissionEditor() != null) {
            $next_mission = $task->getMissionEditor();
            $classDto["first_task"]= ["task_id"=>$task->getId(),"next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "editor_mission"];
        }
        elseif($task->getQuizzes()->first()!=null){
            $next_quiz = $task->getQuizzes()->first();
            $classDto["first_task"]= ["task_id"=>$task->getId(),"next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(), "next_task_type" => $next_quiz->getType()->getName()];
        }

        else
            $classDto["first_task"]= ["task_id"=>null,"next_task_id" => null, "next_task_name" => null, "next_task_type" => null];

        return $classDto;
    }

    public function checkCampusMissionAuthorization($round_id, $activity_id , $mission_id, $user_id,$is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        $checkUserInCampus=$this->professionalRepository->checkIfStudentInCampusRound($round_id, $user_id);

        $teacherRoles = $user->getRoles();
        $isTeacherOrAdmin = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher"||$teacherRole->getName() == "school_admin")
                $isTeacherOrAdmin = true;
        }
        if($isTeacherOrAdmin)
            return ["mission_authorization" => true];

        if(!$checkUserInCampus)
            return ["mission_authorization" => false];


        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        $campus = $round->getCampus();
        //activity in campus
        $activityInCampus=$this->professionalRepository->checkIfActivityInCampus($campus->getId(),$activity_id,$is_default);

        if(!$activityInCampus)
            throw new UnauthorizedException(trans('locale.activity_not_in_campus'));//TODO::translation
        return ["mission_authorization" => true];
    }

    private function updateStudentLastSeen($user_id,$campus_id,$activity_id,$task_id,$is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if($user->is_student()) {

            $task = $this->journeyRepository->getTaskById($task_id);
            if ($task == null)
                throw new BadRequestException(trans('locale.task_not_exist'));

            $account = $user->getAccount();
            if ($account->getAccountType()->getName() != 'School')
                throw new BadRequestException(trans('locale.not_school_account'));

            $school = $account->getSchool();
            if ($school == null)
                throw new BadRequestException(trans('locale.no_school_attached'));
            if ($is_default)
                $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
            else
                $activity = $this->journeyRepository->getActivityById($activity_id);

            $campus = $this->professionalRepository->getCampusById($campus_id);
            if ($campus == null)
                throw new BadRequestException(trans('locale.campus_not_exist'));

            $campusClass = $this->professionalRepository->getCampusClass($campus_id, $activity_id, $is_default);
            if ($campusClass == null)
                throw new BadRequestException(trans('locale.class_not_exist'));

            $campusRoundTask = $this->professionalRepository->getCampusTaskByActivityId($campus_id, $activity_id, $task_id, $is_default);
            if ($campusRoundTask == null)
                throw new BadRequestException(trans('locale.campus_task_not_exist'));
            $updatedTime = Carbon::now()->timestamp;

            $userLastSeenTask = $this->professionalRepository->getUserLastSeenTask($user_id, $campus_id, $activity_id, $task_id, $is_default);
            $this->accountRepository->beginDatabaseTransaction();

            // if ($userLastSeenTask == null) {
            //     //create new record
            //     $userLastSeenTask = new TaskLastSeen($task, $user, $campus, $updatedTime);
            //     if ($is_default)
            //         $userLastSeenTask->setDefaultActivity($activity);
            //     else
            //         $userLastSeenTask->setActivity($activity);
            //     $this->professionalRepository->storeUserLastSeenTask($userLastSeenTask);

            // } else {
            //     //update old record
            //     $userLastSeenTask->setLastSeen($updatedTime);
            //     $this->professionalRepository->storeUserLastSeenTask($userLastSeenTask);

            // }

            //Create Mission Progress by default when learner view a mission
		    $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user->id, $campus_id, $activity_id,$is_default);
            if($missionProgress==null){
            
                $missionProgress = new CampusActivityProgress($task, $user, $campus,0,0,0);
                $missionProgress->setDefaultActivity($activity);
                $this->professionalRepository->storeRoundActivityProgress($missionProgress);
                
            }
            $this->accountRepository->commitDatabaseTransaction();
            return trans('locale.last_seen_updated_successfully');
        }


    }
}