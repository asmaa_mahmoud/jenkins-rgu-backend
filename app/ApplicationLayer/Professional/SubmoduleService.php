<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/16/2018
 * Time: 1:35 PM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularFilesDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularDependenciesDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\MissionStepDto;
use App\ApplicationLayer\Journeys\Dtos\StepContentDto;
use App\ApplicationLayer\Journeys\Dtos\MissionSectionDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragCellDto;
use App\ApplicationLayer\Journeys\Dtos\DragChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownsDto;
use App\ApplicationLayer\Journeys\Dtos\stepDropdownsChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepMcqQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\McqQuestionChoicesDto;
use App\ApplicationLayer\Journeys\Dtos\StepMatchQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\MatchKeysDto;
use App\ApplicationLayer\Journeys\Dtos\MatchValueDto;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Question;
use App\ApplicationLayer\Professional\Dtos\ProjectDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\ApplicationLayer\Common\LessonCommonService;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\DomainModelLayer\Professional\Campus;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use Carbon\Carbon;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularAnswersDto;
use App\ApplicationLayer\Common\TaskCommonService;


class SubmoduleService
{

    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonService;
    private $lessonCommonService;
    private $campusService;
    private $taskCommonService;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
         $this->taskCommonService = new TaskCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);


    }


    public function getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default=true){
       return $this->taskCommonService->getNextTaskInfo($activity_id ,$submodule_id, $task_id,$round_id, $user_id,$is_default);
    }
}