<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/12/2018
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Journeys\Dtos\BlocksDto;
use App\ApplicationLayer\Journeys\Dtos\MissionToolboxDto;
use App\ApplicationLayer\Professional\Dtos\LearningPathNamesDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Professional\LearningPathUser;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;

class LearnPathService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
    }

    public function getStudentLearningPath($user_id){

        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));


        $account = $student->getAccount();

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        if($user_id=='96677') {

            $learningPaths = $this->professionalRepository->getStudentLearningPaths($student->getId());
            foreach ($learningPaths as $path){
                $path->stages = $this->getLearnPathStages($student->getId(), $path->getId());
            }
            return $learningPaths->toArray();
            //return Mapper::MapEntityCollection(LearningPathNamesDto::class, $learningPaths);
        }
        else {

            //$learningPath = $this->professionalRepository->getStudentLearningPath($student->getId());
            $learningPath=$this->professionalRepository->getLearningPathById(3);

            return Mapper::MapClass(LearningPathNamesDto::class, $learningPath);
        }
    }

    public function getAllLearningPath($user_id){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));


        $account = $student->getAccount();

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $learningPathIds=[8,9,10,11];

        $learningPaths = $this->professionalRepository->getLearningPathsWithIds($learningPathIds);

        foreach ($learningPaths as $path){
            $path->stages = $this->getLearnPathStages($student->getId(), $path->getId());
        }
        return $learningPaths->toArray();
            //return Mapper::MapEntityCollection(LearningPathNamesDto::class, $learningPaths);
     }

    public function getLearningPathById($user_id,$id){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));


        $account = $student->getAccount();

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $learningPathIds=[8,9,10,11];

        $learningPath = $this->professionalRepository->getLearningPathById($id);
        if($learningPath==null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));


        $learningPath->stages = $this->getLearnPathStages($student->getId(), $learningPath->getId());

        return $learningPath;
            //return Mapper::MapEntityCollection(LearningPathNamesDto::class, $learningPaths);
     }



    public function getStudentLearningPathLessons($user_id, $learning_path_id, $is_default){
       // dd ($user_id, $learning_path_id, $is_default);
        //$learnPathStages= $this->getLearnPathStages($user_id,$learning_path_id);
        if($user_id=='96677') {
            $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
        }else{
            $learningPath = $this->professionalRepository->getStudentLearningPath($user_id);
        }



        if ($learningPath == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        $learning_path_id = $learningPath->getId();

        $hasAccess = $this->hasAccessActivityInLearningPath($user_id, $learning_path_id);
        if(!$hasAccess)
            throw new BadRequestException(trans("locale.no_permission"));

        $stages=[];


        $learnPathStages= $this->professionalRepository->getLearnPathStages($learning_path_id);

        //return ($learnPathStages);
        foreach ($learnPathStages as $learnPathStage) {
            $learnPathStagesActivitiesIds= $this->professionalRepository->getStageActivitiesIds($learnPathStage->stage_number,$learning_path_id);


            $stageTasks = $this->professionalRepository->getAllTasksInStage($learning_path_id, $learnPathStage->stage_number,true);


            $stageActivitiesNum=$learnPathStage->activity_count;
            $stageSolvedTasks=0;
            $stage['num_of_activities']=$stageActivitiesNum;
            $stage['stage_number']=$learnPathStage->stage_number;
            $stage['stage_name']=$learnPathStage->stage_name;
            $stage['stage_image_url']=$learnPathStage->stage_image_url;


            //TODO:uncomment

            foreach ($learnPathStagesActivitiesIds as $learnPathStagesActivityId){
                $stageSolvedTasks+=$this->professionalRepository->getSolvedTasksInActivity($user_id,$learning_path_id,$learnPathStagesActivityId,true);

            }
            //dd($stageTasks);
            //dd($stageSolvedTasks,$stageTasks);
            if($stageTasks != null )
                $stage['stage_progress']=intval(($stageSolvedTasks/$stageTasks)*100).'%';
            else
                $stage['stage_progress'] = '0%';

            //get stage activities
                     $activitiesDtos=[];
                    foreach ($learnPathStagesActivitiesIds as $id){

                        if($is_default)
                            $activity = $this->journeyRepository->getDefaultActivityById($id);
                        else
                            $activity = $this->journeyRepository->getActivityById($id);

                        $stageActivity = $this->professionalRepository->getActivityInStage($learning_path_id, $learnPathStage->stage_number, $activity->getId());
                       /* if($stageActivity==null){
                            dd($learning_path_id, $learnPathStage->stage_name, $activity->getId());
                        }
*/
                        $activityDto['id'] = $activity->getId();
                        $activityDto['name'] = $activity->getName();
                        $activityDto['stage_name'] =$stageActivity->getStageName();
                        $activityDto['difficulty'] = $activity->getDifficulty()->getName();
                        $activityDto['ageFrom'] = $activity->getAgeFrom();
                        $activityDto['ageTo'] = $activity->getAgeTo();
                        $activityDto['description'] = $activity->getDescription();
                        $activityDto['imageUrl'] = $activity->getImageUrl();
                        $activityDto['order'] = $stageActivity->order;
                        $activityDto['iconUrl'] = $activity->getIconUrl();
                        //$activityDto['progress'] ='0'.'%';
                        //TODO:uncomment
                        $activityDto['progress'] = intval($this->calculateActivityProgress($user_id, $learning_path_id,$activity->getId())*100).'%';

                        $activityDto['tags'] = [];
                        $activityDto['screenshots'] = [];

                        foreach ($activity->getTags() as $tag){
                            array_push($activityDto['tags'],$tag->getName());
                        }
                        foreach ($activity->getScreenshots() as $screenshot) {
                            array_push($activityDto['screenshots'], $screenshot->getScreenshotUrl());
                        }
                        //fetch activity tasks
                        $activity_id=$activity->getId();

                         $tasks = $activity->getTasks();
                        $tasksDto = [];

                        //$progressUnlocked = $this->checkProgressUnlockedActivity($user_id);
                        foreach ($tasks as $task){
                            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
                            $passed = false;
                            if(isset($taskProgress) && $taskProgress->first_success)
                                $passed = true;

                            $mission = $task->getMissions()->first();
                            if($mission != null) {
                                $locked = false;

                                array_push($tasksDto, [
                                    'id' => $task->getId(),
                                    'name' => $mission->getName(),
                                    'image' => $mission->getIconUrl(),
                                    'locked' => $locked,
                                    'xp' => $mission->getWeight(),
                                    'coins' => $mission->getWeight()/5,
                                    'type' => 'mission',
                                    'mission_id' => $mission->getId(),
                                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                                    'passed' => $passed
                                ]);
                            }
                            else {
                                $mission = $task->getMissionsHtml()->first();
                                if($mission != null) {
                                    $locked = false;

                                    array_push($tasksDto, [
                                        'id' => $task->getId(),
                                        'name' => $mission->getTitle(),
                                        'image' => $mission->getIconUrl(),
                                        'locked' => $locked,
                                        'xp' => $mission->getWeight(),
                                        'coins' => $mission->getWeight()/5,
                                        'type' => 'html_mission',
                                        'mission_id' => $mission->getId(),
                                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                                        'passed' => $passed
                                    ]);
                                }
                                else {
                                    $quiz = $task->getQuizzes()->first();
                                    $locked = false;

                                    array_push($tasksDto, [
                                        'id' => $task->getId(),
                                        'name' => $quiz->getTitle(),
                                        'image' => $quiz->geticonURL(),
                                        'locked' => $locked,
                                        'xp' => $quiz->getQuestionsWeight()['weight'],
                                        'coins' => $quiz->getQuestionsWeight()['weight']/5,
                                        'type' => $quiz->getType()->getName(),
                                        'quiz_id' => $quiz->getId(),
                                        'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default),
                                        'passed' => $passed
                                    ]);
                                }
                            }
                        }
                       // return $tasksDto;
                        $activityDto['lessons']=$tasksDto;
                        $activitiesDtos[]=$activityDto;
                    }
             $stage['activities'] =$activitiesDtos;
            $stages[]=$stage;
            //dd($stage);
        }
        return $stages;

    }
    public function getLearnPathStages($user_id,$learning_path_id=null){
       // dd($learning_path_id);
//        if($user_id=='96677') {
//            $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
//        }else{
//            $learningPath = $this->professionalRepository->getStudentLearningPath($user_id);
//        }

        $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);



        if ($learningPath == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        $learning_path_id = $learningPath->getId();

//        $hasAccess = $this->hasAccessActivityInLearningPath($user_id, $learning_path_id);
//        if(!$hasAccess)
//            throw new BadRequestException(trans("locale.no_permission"));

        $stages=[];


        $learnPathStages= $this->professionalRepository->getLearnPathStages($learning_path_id);

        //return ($learnPathStages);
        foreach ($learnPathStages as $learnPathStage) {
//            $learnPathStagesActivitiesIds= $this->professionalRepository->getStageActivitiesIds($learnPathStage->stage_number,$learning_path_id);
//
//
//            $stageTasks = $this->professionalRepository->getAllTasksInStage($learning_path_id, $learnPathStage->stage_number,true);


//            $stageActivitiesNum=$learnPathStage->activity_count;
//            $stageSolvedTasks=0;
//            $stage['num_of_activities']=$stageActivitiesNum;
            $stage['stage_number']=$learnPathStage->stage_number;
            $stage['stage_name']=$learnPathStage->stage_name;
            $stage['stage_image_url']=$learnPathStage->stage_image_url;

//            foreach ($learnPathStagesActivitiesIds as $learnPathStagesActivityId){
//                $stageSolvedTasks+=$this->professionalRepository->getSolvedTasksInActivity($user_id,$learning_path_id,$learnPathStagesActivityId,true);
//
//
//            }
//            //dd($stageTasks);
//            if($stageTasks != null )
//                $stage['stage_progress']=intval(($stageSolvedTasks/$stageTasks)*100).'%';
//            else
//                $stage['stage_progress'] = '0%';
          $stages[]=$stage;
            //dd($stage);
        }
        return $stages;
    }
    public function getTestLearnPathStages($user_id){
      $path_arr=[8,9,10,11];
        $learningPathDtos=[];
      foreach ($path_arr as $learning_path_id){
          $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
          if ($learningPath == null)
              throw new BadRequestException(trans('locale.learning_path_not_exist'));
          $learningPathDto['id']=$learningPath->getId();
          $learningPathDto['name']=$learningPath->getName();
          $learningPathDto['description']=$learningPath->getDescription();

//          $hasAccess = $this->hasAccessActivityInLearningPath($user_id, $learningPath->getId());
//          if(!$hasAccess)
//              throw new BadRequestException(trans("locale.no_permission"));


          $learnPathStages= $this->professionalRepository->getLearnPathStages($learning_path_id);

          //return ($learnPathStages);
          $stages=[];
          foreach ($learnPathStages as $learnPathStage) {

              $stageActivitiesNum=$learnPathStage->activity_count;
              $stage['num_of_activities']=$stageActivitiesNum;
              $stage['stage_number']=$learnPathStage->stage_number;
              $stage['stage_name']=$learnPathStage->stage_name;
              $stage['stage_image_url']=$learnPathStage->stage_image_url;

              $stages[]=$stage;
             
          }
          $learningPathDto['stages']= $stages;
          $learningPathDtos[]=$learningPathDto;

      }
        return $learningPathDtos;
    }

    public function getStageActivities($user_id,$stage_number,$learning_path_id, $is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $hasAccess = $this->hasAccessActivityInLearningPath($user_id, $learning_path_id);
        if(!$hasAccess)
            throw new BadRequestException(trans("locale.no_permission"));

        $learnPathStagesActivitiesIds= $this->professionalRepository->getStageActivitiesIds($stage_number,$learning_path_id);

        $activitiesDtos=[];
        foreach ($learnPathStagesActivitiesIds as $id){

            if($is_default)
                $activity = $this->journeyRepository->getDefaultActivityById($id);
            else
                $activity = $this->journeyRepository->getActivityById($id);

            $stageActivity = $this->professionalRepository->getActivityInStage($learning_path_id, $stage_number, $activity->getId());

            $activityDto['id'] = $activity->getId();
            $activityDto['name'] = $activity->getName();
            $activityDto['stage_name'] =$stageActivity->getStageName();
            $activityDto['difficulty'] = $activity->getDifficulty()->getName();
            $activityDto['ageFrom'] = $activity->getAgeFrom();
            $activityDto['ageTo'] = $activity->getAgeTo();
            $activityDto['description'] = $activity->getDescription();
            $activityDto['imageUrl'] = $activity->getImageUrl();
            $activityDto['order'] = $stageActivity->order;
            $activityDto['iconUrl'] = $activity->getIconUrl();
            //$activityDto['progress'] ='0'.'%';
            $activityDto['progress'] = intval($this->calculateActivityProgress($user_id, $learning_path_id,$activity->getId())*100).'%';

            $activityDto['tags'] = [];
            $activityDto['screenshots'] = [];

            foreach ($activity->getTags() as $tag){
                array_push($activityDto['tags'],$tag->getName());
            }
            foreach ($activity->getScreenshots() as $screenshot) {
                array_push($activityDto['screenshots'], $screenshot->getScreenshotUrl());
            }
            $activitiesDtos[]=$activityDto;
        }

        return $activitiesDtos;
    }

    public function getActivity($user_id, $learning_path_id, $activity_id, $is_default=true)
    {

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
        if ($learningPath == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if ($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if ($activity == null)
            throw new BadRequestException(trans("locale.activity_id") . $activity_id . trans("locale.doesn't_exist"));

        if (!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $hasAccess = $this->hasAccessActivityInLearningPath($user_id, $learning_path_id);
        if(!$hasAccess)
            throw new BadRequestException(trans("locale.no_permission"));

        $learningPathActivity = $this->professionalRepository->getLearningPathActivity($learningPath->getId(), $activity_id);
        if ($learningPathActivity == null)
            throw new BadRequestException(trans("locale.activity_not_in_learning_path"));

        $activityDto['id'] = $activity->getId();
        $activityDto['name'] = $activity->getName();
        $activityDto['stage_name'] =$learningPathActivity->getStageName();
        $activityDto['stage_number'] =$learningPathActivity->getStageNumber();
        $activityDto['difficulty'] = $activity->getDifficulty()->getName();
        $activityDto['difficultyTitle'] = $activity->getDifficulty()->getTitle();
        $activityDto['ageFrom'] = $activity->getAgeFrom();
        $activityDto['ageTo'] = $activity->getAgeTo();
        $activityDto['description'] = $activity->getDescription();
        $activityDto['imageUrl'] = $activity->getImageUrl();
        $activityDto['iconUrl'] = $activity->getIconUrl();
        $activityDto['tags'] = [];
        $activityDto['screenshots'] = [];
        foreach ($activity->getTags() as $tag) {
            array_push($activityDto['tags'], $tag->getName());
        }
        foreach ($activity->getScreenshots() as $screenshot) {
            array_push($activityDto['screenshots'], $screenshot->getScreenshotUrl());
        }
        return $activityDto;
    }

    public function getActivityTasks($user_id, $learning_path_id, $activity_id, $is_default=true){

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
        if ($learningPath == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if ($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if ($activity == null)
            throw new BadRequestException(trans("locale.activity_id") . $activity_id . trans("locale.doesn't_exist"));

        if (!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $hasAccess = $this->hasAccessActivityInLearningPath($user_id, $learning_path_id);
        if(!$hasAccess)
            throw new BadRequestException(trans("locale.no_permission"));

        $tasks = $activity->getTasks();
        $tasksDto = [];

        //$progressUnlocked = $this->checkProgressUnlockedActivity($user_id);
        foreach ($tasks as $task){
            $taskProgress = $this->professionalRepository->getUserTaskProgress($user_id, $learning_path_id, $task->getId());
            $passed = false;
            if(isset($taskProgress) && $taskProgress->first_success)
                $passed = true;

            $mission = $task->getMissions()->first();
            if($mission != null) {
                $locked = false;

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image' => $mission->getIconUrl(),
                    'locked' => $locked,
                    'xp' => $mission->getWeight(),
                    'coins' => $mission->getWeight()/5,
                    'type' => 'mission',
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                    'passed' => $passed
                ]);
            }
            else {
                $mission = $task->getMissionsHtml()->first();
                if($mission != null) {
                    $locked = false;

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'locked' => $locked,
                        'xp' => $mission->getWeight(),
                        'coins' => $mission->getWeight()/5,
                        'type' => 'html_mission',
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed' => $passed
                    ]);
                }
                else {
                    $quiz = $task->getQuizzes()->first();
                    $locked = false;

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $quiz->getTitle(),
                        'image' => $quiz->geticonURL(),
                        'locked' => $locked,
                        'xp' => $quiz->getQuestionsWeight()['weight'],
                        'coins' => $quiz->getQuestionsWeight()['weight']/5,
                        'type' => $quiz->getType()->getName(),
                        'quiz_id' => $quiz->getId(),
                        'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed' => $passed
                    ]);
                }
            }
        }
        return $tasksDto;
    }

    public function getHtmlBlocklyCategories(){

        $blocklyCategories = $this->professionalRepository->getHtmlBlocklyCategories();
        $mappedObject = Mapper::MapEntityCollection(MissionToolboxDto::class, $blocklyCategories,[BlocksDto::class]);
        return $mappedObject;
    }

    public function checkSchoolData($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        return ['user' => $user, 'account' => $account, 'school' => $school];
    }

    public function calculateActivityProgress($user_id,$learning_path_id,$activity_id){
          $numOfSolvedTasks= $this->professionalRepository->getSolvedTasksInActivity($user_id,$learning_path_id,$activity_id,true);
          $numOfTasks= $this->professionalRepository->getAllTasksInActivity($activity_id, true);
          if($numOfTasks == null)
              return 0;
          return ($numOfSolvedTasks/$numOfTasks);
    }

    public function hasAccessActivityInLearningPath($user_id, $learning_path_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if ($user->is_student()){

            $studentInLearningPath = $this->professionalRepository->checkIfStudentInLearningPath($learning_path_id, $user_id);
            if(!$studentInLearningPath)
                return false;
        }
        else{
            return false;
        }
        return true;
    }

    public function checkProgressUnlockedActivity($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if ($this->accountRepository->isAuthorized('unlock_AllTasks', $user))
            return true;

        return false;
    }
}