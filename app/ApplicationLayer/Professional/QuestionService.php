<?php

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Professional\Dtos\AnalysisCategoryDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisCategoryTranslationDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;

use App\ApplicationLayer\Professional\Dtos\AnalysisQuestionTranslationDto;
use App\ApplicationLayer\Professional\Dtos\CategoryAnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\CategoryAnalysisQuestionDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\AnalysisCategory;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;

class QuestionService {
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
    }

	public function getAllQuestions(){
//        $student = $this->accountRepository->getUserById($user_id);
//        if($student == null)
//            throw new BadRequestException(trans('locale.user_not_exist'));
        $this->accountRepository->beginDatabaseTransaction();

        $questions= $this->professionalRepository->getAllQuestions();

        $this->accountRepository->commitDatabaseTransaction();


		 $questionsMapped=Mapper::MapEntityCollection(AnalysisQuestionDto::class,
             $questions,[AnalysisCategoryDto::class,AnalysisAnswerDto::class,AnalysisQuestionTranslationDto::class]);

    	return $questionsMapped;



	}

	public function getQuestion($question_id){
//        $student = $this->accountRepository->getUserById($user_id);
//        if($student == null)
//            throw new BadRequestException(trans('locale.user_not_exist'));
        $this->accountRepository->beginDatabaseTransaction();

        $question= $this->professionalRepository->getQuestion($question_id);
        if($question == null)
            throw new BadRequestException(trans('locale.question_not_exist'));

        $this->accountRepository->commitDatabaseTransaction();


        $questionMapped = Mapper::MapClass(AnalysisQuestionDto::class,
            $question, [AnalysisCategoryDto::class,AnalysisAnswerDto::class,AnalysisQuestionTranslationDto::class]);


        return $questionMapped;
	}


}
