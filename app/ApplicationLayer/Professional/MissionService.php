<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 19/09/2018
 * Time: 11:12 AM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Journeys\Dtos\ChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\LearningPathActivityProgress;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;

class MissionService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
    }


    public function updateActivityMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$is_default=true){
        $this->accountRepository->beginDatabaseTransaction();

        $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
        if($learning_path == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_account_not_found'));

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission_id);
        $previous_missionprogress = $this->professionalRepository->getUserTaskProgressActivityLearningPath($task, $user_id, $learning_path_id, $activity_id, $is_default);

        $score = $mission->getWeight();
        if($previous_missionprogress == null){
            if($success_flag){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $mission->getWeight();
                }
                $task_progress = new LearningPathActivityProgress($task, $user, $learning_path, $score,1,1, $timeTaken, $timeTaken, $noOfBlocks, $noOfBlocks);

                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);

                $this->professionalRepository->storeLearningPathActivityProgress($task_progress);

                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);

                //store use code if new
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }
            else {
                $task_progress = new LearningPathActivityProgress($task, $user, $learning_path,0,1);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
            }
        }
        else {
            $no_trials = $previous_missionprogress->getNoTrials() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($success_flag && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer){
                        $score += 5;
                    }
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }
            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $success_flag){
                $score = $mission->getWeight();
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->professionalRepository->deleteLearningPathActivityProgress($previous_missionprogress);
            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path, $score, $no_trials, $first_success, $timeTaken, $bestTimeTaken, $noOfBlocks, $bestNoOfBlocks);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function updateActivityHTMLMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$is_default = true){
        $this->accountRepository->beginDatabaseTransaction();

        $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
        if($learning_path == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $score = $mission->getWeight();
        $previous_missionprogress = $this->professionalRepository->getUserTaskProgressActivityLearningPath($task,$user_id,$learning_path_id,$activity_id,$is_default);
        if($previous_missionprogress == null)
        {
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path, $score, 1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        elseif ($previous_missionprogress->getScore() < $score) {
            $addedScore = $score - $previous_missionprogress->getScore();

            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $this->professionalRepository->deleteLearningPathActivityProgress($previous_missionprogress);
            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path_id, $score,1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionHtmlDto::class,$mission);
    }

    public function submitActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $learning_path_id, $activity_id,$noOfBlocks,$timeTaken,$is_default){
        $this->accountRepository->beginDatabaseTransaction();

        $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
        if($learningPath == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $selected_ids_counter = 0;
        $questions_answers = [];

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $task = $mission->getTask();
        $choice_correction = array();
        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getMcqQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getMcqChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            if($use_model && $use_model != "false")
                $choice_correct = $this->journeyRepository->getMcqCorrectChooseChoice($question['question_id']);
            else
                $choice_correct = $this->journeyRepository->getMcqChoiceById($selected_ids[$selected_ids_counter]);
            $question_answer = ["question_id" => $question['question_id'],
                "user_choice" => $choice_instance->choose,
                "choice_correct" => $choice_correct->choose,
                "eval" => false];

            if($choice_correct->id == $question['choice']) {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1;
                $score = $score + $question_instance->getWeight();
            }
            else {
                $choice_message = $this->journeyRepository->getMcqChoiceById($question['choice']);
                $message = [
                    "choice_id" => $question['choice'],
                    "msg" => $choice_message->getCorrectionMsg()
                ];
                array_push($choice_correction, $message);
            }
            $questions_answers[] = $question_answer;
            $selected_ids_counter++;
        }
        // TO-DO Change Logic Of Success
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());

        $baseScore = $score;
        $previous_missionprogress = $this->professionalRepository->getUserTaskProgressActivityLearningPath($mission->getTask(), $user_id, $learning_path_id, $activity_id, $is_default);
        if($previous_missionprogress == null){
            if($passed){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $baseScore;
                }
                $task_progress = new LearningPathActivityProgress($task, $user, $learningPath, $score,1,1, $timeTaken, $timeTaken, $noOfBlocks, $noOfBlocks);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }
            else {
                $task_progress = new LearningPathActivityProgress($task, $user, $learningPath,0,1);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->schoolRepository->storeCampActivityProgress($task_progress);
            }
        }
        else {
            $no_trials = $previous_missionprogress->getNoTrials() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($passed && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                //no of BlocksScore
//                if($mission->getType() != "text"){
//                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
//                        $score +=5;
//                    }
//                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }

            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }

            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $passed){
                $score = $baseScore;
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->professionalRepository->deleteLearningPathActivityProgress($previous_missionprogress);
            $task_progress = new LearningPathActivityProgress($task, $user, $learningPath, $score, $no_trials, $first_success, $timeTaken, $bestTimeTaken, $noOfBlocks, $bestNoOfBlocks);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

        }
        $this->journeyRepository->storeTask($task);
        $this->accountRepository->commitDatabaseTransaction();
        return ["passed" => $passed, "questions_answers" => $questions_answers, "choice_correction" => $choice_correction];
    }

    public function submitActivityQuiz($data, $quiz_id, $user_id, $learning_path_id, $activity_id, $is_default){
        $this->accountRepository->beginDatabaseTransaction();

        $learningPath = $this->professionalRepository->getLearningPathById($learning_path_id);
        if($learningPath == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $questions_answers = [];
        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        $task = $quiz->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            $choice_correct = Mapper::MapEntityCollection(ChoiceDto::class, $this->journeyRepository->getChoiceModelforQuestion($question['question_id']));
            $question_answer = ["question_id" => $question['question_id'],
                "question_body" => $question_instance->getBody(),
                "user_choice" => $choice_instance->getText(),
                "choice_correct" => $choice_correct[0]->text,
                "eval" => false];
            if($choice_correct[0]->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question['weight'];
            }
            $questions_answers[] = $question_answer;
        }
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $quiz_progress_object = $this->professionalRepository->getUserTaskProgressActivityLearningPath($quiz->getTask(), $user_id, $learning_path_id, $activity_id, $is_default);
        if($quiz_progress_object != null && $passed)
        {
            $trails = $quiz_progress_object->getNoTrials() + 1;
            $max_score = ($score > $quiz_progress_object->getScore()? $score:$quiz_progress_object->getScore());

            if($score > $quiz_progress_object->getScore()){
                $addedScore = $score - $quiz_progress_object->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }

            if($quiz_progress_object->getFirstSuccess() != null){
                $firstSuccess = $quiz_progress_object->getFirstSuccess();
            }
            else {
                $firstSuccess = 1;
            }
            $this->professionalRepository->deleteLearningPathActivityProgress($quiz_progress_object);
            $task_progress = new LearningPathActivityProgress($task,$user,$learningPath,$max_score,$trails,$firstSuccess);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        elseif($passed){
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
            $task_progress = new LearningPathActivityProgress($task,$user,$learningPath,$score,1,1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return ["passed" => $passed, "questions_answers" => $questions_answers];
    }

    public function updateActivityMissionCodingProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$timeTaken,$is_default=true){
        $this->accountRepository->beginDatabaseTransaction();

        $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
        if($learning_path == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getCodingMissionById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_account_not_found'));

        $score = $mission->getWeight();
        $previous_missionprogress = $this->professionalRepository->getUserTaskProgressActivityLearningPath($task,$user_id,$learning_path_id,$activity_id,$is_default);

        if($previous_missionprogress == null)
        {
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path, $score, 1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        elseif ($previous_missionprogress->getScore() < $score) {
            $addedScore = $score - $previous_missionprogress->getScore();

            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $this->professionalRepository->deleteLearningPathActivityProgress($previous_missionprogress);
            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path_id, $score,1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionCodingDto::class,$mission);
    }

    public function updateActivityMissionEditorProgress($mission_id,$user_id,$learning_path_id,$activity_id,$is_default=true){
        $this->accountRepository->beginDatabaseTransaction();

        $learning_path = $this->professionalRepository->getLearningPathById($learning_path_id);
        if($learning_path == null)
            throw new BadRequestException(trans('locale.learning_path_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionEditorById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $score = $mission->getWeight();
        $previous_missionprogress = $this->professionalRepository->getUserTaskProgressActivityLearningPath($task,$user_id,$learning_path_id,$activity_id,$is_default);
        if($previous_missionprogress == null)
        {
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path, $score, 1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        elseif ($previous_missionprogress->getScore() < $score) {
            $addedScore = $score - $previous_missionprogress->getScore();

            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $this->professionalRepository->deleteLearningPathActivityProgress($previous_missionprogress);
            $task_progress = new LearningPathActivityProgress($task, $user, $learning_path_id, $score,1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->professionalRepository->storeLearningPathActivityProgress($task_progress);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionEditorDto::class,$mission);
    }

    public function getPracticeMission($mission_id, $type){

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $mission_type = array("mission", "coding_mission", "editor_mission");
        if (!in_array($type, $mission_type)) {
            throw new BadRequestException(trans('locale.mission_type_not_exist'));
        }

        if($type=="mission"){
            $missionDto =  Mapper::MapClass(MissionDto::class,$mission,[MissionScreensDto::class]);
            $missionDto->type = $mission->getType();
            if($missionDto->type == "text")
                $missionDto->textualBlocklyCode = $mission->showModelAnswer();

            if($mission->getProgrammingLanguageType() != null)
                $missionDto->programmingLanguageType =  $mission->getProgrammingLanguageType()->getName();
            $missionDto->blockly_rules = $mission->blockly_rules;

            if($mission->getType() == "mcq"){
                $choice_ids = array();
                $missionQuestions = $mission->getMissionMcq()->getQuestions();
                foreach ($missionQuestions as $missionQuestion){
                    $choices = $missionQuestion->getChoices();
                    foreach($choices as $choice){
                        if($choice->isCorrect())
                            array_push($choice_ids, $choice->getId());
                    }

                }

                $missionDto->ModelAnswer = $mission->showModelAnswer();
                $missionDto->model_choice_ids = $choice_ids;

            }
            $missionDto->sceneName = $mission->getSceneName();
            $missionDto->descriptionSpeech = $mission->getDescriptionSpeech();
            $missionDto->HintModelAnswer = $mission->showModelAnswer();
            $missionDto->GoldenTime = $mission->getGoldenTime();
            $missionDto->MissionState = $mission->getMissionState();
            $missionDto->ModelAnswerNumberOfBlocks = $mission->getModelAnswerNumberOfBlocks();
            // $missionDto->task_id=$mission->getTask()->getId();

            return $missionDto;

        }elseif($type=="coding_mission"){

            $mission = $this->journeyRepository->getCodingMissionById($mission_id);
            if($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $task = $mission->getTask();

            $missionDto =  Mapper::MapClass(MissionCodingDto::class,$mission);

            return $missionDto;
        }elseif($type=="editor_mission"){
            $mission = $this->journeyRepository->getMissionEditorById($mission_id);
            if($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $missionDto =  Mapper::MapClass(MissionEditorDto::class,$mission);

            return $missionDto;
        }
    }

    //private functions
    private function checkActivityB2Authority($user_id, $activity_id, $is_default = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $accountType = $account->getAccountType()->getName();
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = false;
        if($activity->isB2B()){
            if($accountType == 'School')
                $authority = true;
        }
        if($activity->isB2C()){
            if($accountType == 'Family' || $accountType == 'Individual')
                $authority = true;
        }
        return $authority;
    }

    private function checkOpenedModelAnswer($user, $mission_id){
        $returned = false;
        $isUser = false;
        $isStudent = false;
        $isChild = false;
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;

            if($role->getName() == 'user' || $role->getName() == 'invited_Individual')
                $isUser = true;

            if($role->getName() == 'student')
                $isStudent = true;

            if($role->getName() == 'child')
                $isChild = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getJourneys();
            if(count($journeys) > 0){
                foreach ($journeys as $invJourney){
                    if($mission->getTask()->getTaskOrder() != null){
                        if($invJourney->getJourney()->getId() == $mission->getTask()->getTaskOrder()->getJourney()->getId()){
                            if($isUser){
                                if($invJourney->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
            $activities = $invitation_subscription->getInvitation()->getDefaultActivities();
            if(count($activities) > 0){
                foreach ($activities as $invActivity){
                    if($mission->getTask()->getActivityTask() != null){
                        if($invActivity->getDefaultActivity()->getId() == $mission->getTask()->getActivityTask()->getDefaultActivity()->getId()){
                            if($isUser){
                                if($invActivity->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
        }
        return $returned;
    }
}