<?php

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\UserService;
use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundRequestDto;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Submission;


class ProfessionalMainService implements IProfessionalMainService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
    }

    public function getProductAssets($product_url){
        $productService = new ProductService($this->professionalRepository);
        return $productService->getProductAssets($product_url);
    }

    public function unlockStudentRound($user_id,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->unlockStudentRound($user_id,$round_id);

    }

    public function getAllQuestions(){
        $questionService = new QuestionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $questionService->getAllQuestions();
    }

    public function getQuestion($question_id){
        $questionService = new QuestionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $questionService->getQuestion($question_id);
    }

    public function getAllAnswers($user_id){
        $answerService = new AnswerService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $answerService->getAllAnswers($user_id);
    }

    public function getAnswer($answer_id){
        $answerService = new AnswerService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $answerService->getAnswer($answer_id);
    }

    public function submit($user_id, $userAnswers, $categoryCode, $visualProfile, $categoryID){
        $submissionService = new SubmissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
//        return $submissionService->submit($user_id,$answerDtos);
        //dd($userAnswers);
        $submissionArr = $submissionService->submit($user_id,$userAnswers,$categoryCode);
        $evaluation= $submissionService->evaluate($user_id, $visualProfile, $categoryCode, $submissionArr["answersData"]);
        $submissionService->saveVisualProfile($submissionArr["submission"],$evaluation);
        return $evaluation;
    }
    public function deleteCategoryPostSubmissions($user_id, $categoryID){
        $submissionService = new SubmissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return  $submissionService->deleteCategoryPostSubmissions($user_id,$categoryID);

    }
    public  function getCategoryQuestions($category_id){
        $roboscaleService = new RoboscaleService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $roboscaleService->getCategoryQuestions($category_id);
    }

    public function getAllCategories(){
        $roboscaleService = new RoboscaleService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $roboscaleService->getAllCategories();
    }

    public function getStudentLearningPath($student_id){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getStudentLearningPath($student_id);
    }

    public function getAllLearningPath($student_id){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getAllLearningPath($student_id);
    }

    public function getLearningPathById($user_id,$id){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getLearningPathById($user_id,$id);
    }

    public function getStudentLearningPathLessons($user_id, $learning_path_id, $is_default){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getStudentLearningPathLessons($user_id, $learning_path_id, $is_default);
    }
    public function getLearnPathStages($user_id,$learning_path_id=null){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getLearnPathStages($user_id,$learning_path_id);
    }
    public function getTestLearnPathStages($user_id){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getTestLearnPathStages($user_id);
    }

    public function getStageActivities($user_id,$stage_number,$learning_path_id, $is_default){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getStageActivities($user_id,$stage_number,$learning_path_id, $is_default);
    }

    public function getActivity($user_id, $learning_path_id, $activity_id, $is_default){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getActivity($user_id, $learning_path_id, $activity_id, $is_default);
    }

    public function getActivityTasks($user_id, $learning_path_id, $activity_id, $is_default){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getActivityTasks($user_id, $learning_path_id, $activity_id, $is_default);
    }

    public function getHtmlMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id,$is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getHtmlMissionData($learning_path_id, $campus_id, $activity_id, $mission_id,$user_id,$is_default);
    }

    public function getHtmlMissionNext($learning_path_id=null, $campus_id=null, $activity_id , $submodule_id,$mission_id,$round_id,$user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getHtmlMissionNext($learning_path_id, $campus_id, $activity_id,$submodule_id, $mission_id,$round_id,$user_id,$is_default);
    }

    public function getProjectData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id,$is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getProjectData($learning_path_id, $campus_id, $activity_id, $mission_id,$user_id,$is_default);
    }

    public function getMissionData($learning_path_id=null, $campus_id=null,$activity_id, $mission_id, $user_id,$is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getMissionData($learning_path_id, $campus_id,$activity_id, $mission_id,$user_id,$is_default);
    }

    public function getHandoutMissionData($campus_id=null, $activity_id, $mission_id, $user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getHandoutMissionData($campus_id, $activity_id, $mission_id, $user_id, $is_default);
    }

    public function getHandoutMissionNext($campus_id=null, $activity_id , $submodule_id,$mission_id, $round_id, $user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getHandoutMissionNext($campus_id, $activity_id ,$submodule_id, $mission_id, $round_id, $user_id, $is_default);
    }

    public function getMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id,$mission_id,$round_id,$user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getMissionNext($learning_path_id, $campus_id, $activity_id,$submodule_id, $mission_id,$round_id,$user_id,$is_default);
    }
    public function getQuizNext($learning_path_id=null, $campus_id=null, $activity_id , $submodule_id,$mission_id,$round_id,$user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getQuizNext($learning_path_id, $campus_id, $activity_id,$submodule_id, $mission_id,$round_id,$user_id,$is_default);
    }

    public function checkMissionAuthorization($learning_path_id, $activity_id , $mission_id,$user_id, $is_default=true){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->checkMissionAuthorization($learning_path_id, $activity_id, $mission_id,$user_id,$is_default);
    }

    public function getHtmlBlocklyCategories(){
        $learnPathService = new LearnPathService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $learnPathService->getHtmlBlocklyCategories();
    }

    public function updateActivityMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$is_default = true){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->updateActivityMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$is_default);
    }

    public function updateActivityHTMLMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$is_default = true){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->updateActivityHTMLMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$is_default);
    }

    public function submitActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $learning_path_id, $activity_id,$noOfBlocks,$timeTaken,$is_default = true){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->submitActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $learning_path_id, $activity_id,$noOfBlocks,$timeTaken,$is_default);
    }

    public function submitActivityQuiz($data, $quiz_id, $user_id, $learning_path_id, $activity_id,$is_default = true){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->submitActivityQuiz($data, $quiz_id, $user_id, $learning_path_id, $activity_id, $is_default);
    }

    public function getUserSubmission($user_id){
        $submissionService = new SubmissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $submissionService->getUserSubmission($user_id);
    }

    public function getCategorySubmission($user_id,$category_code){
        $submissionService = new SubmissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $submissionService->getCategoryUserSubmission($user_id,$category_code);
    }

    public function getSectionSubmissions($user_id,$category_id){
        $submissionService = new SubmissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $submissionService->getSectionSubmissions($user_id,$category_id);
    }

    public function getCodingMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getCodingMissionData($learning_path_id, $campus_id, $activity_id, $mission_id, $user_id, $is_default);
    }

    public function getMissionEditorData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getMissionEditorData($learning_path_id, $campus_id, $activity_id, $mission_id, $user_id);
    }

    public function getCodingMissionNext($learning_path_id=null, $campus_id=null, $activity_id , $submodule_id,$mission_id, $round_id,$user_id,$is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getCodingMissionNext($learning_path_id, $campus_id, $activity_id,$submodule_id, $mission_id,$round_id,$user_id,$is_default);
    }

    public function getAngularMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getAngularMissionData($learning_path_id, $campus_id, $activity_id, $mission_id, $user_id);
    }

    function getMissionEditorNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id,$round_id,$user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getMissionEditorNext($learning_path_id, $campus_id, $activity_id,$submodule_id, $mission_id,$round_id,$user_id,$is_default);
    }

    function getAngularMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id,$round_id,$user_id, $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getAngularMissionNext($learning_path_id, $campus_id, $activity_id,$submodule_id, $mission_id,$round_id,$user_id,$is_default);
    }

    function getNextClass($round_id=null, $activity_id , $is_default){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->getNextClass( $round_id, $activity_id,$is_default);
    }

    public function updateActivityMissionCodingProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$timeTaken = null,$is_default = true){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->updateActivityMissionCodingProgress($mission_id,$user_id,$learning_path_id,$activity_id,$timeTaken,$is_default);
    }

    public function updateActivityMissionEditorProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$is_default = true){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->updateActivityMissionEditorProgress($mission_id,$user_id,$learning_path_id,$activity_id,$is_default);
    }

    public function getPracticeMission($mission_id, $type){
        $missionService = new MissionService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $missionService->getPracticeMission($mission_id, $type);
    }

    public function checkCampusMissionAuthorization($round_id, $activity_id , $mission_id,$user_id, $is_default=true){
        $studentService = new StudentService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $studentService->checkCampusMissionAuthorization($round_id, $activity_id, $mission_id,$user_id,$is_default);
    }

    //Campus

    public function getAllCampuses($user_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getAllCampuses($user_id);
    }

    public function getSupportCampuses($user,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getSupportCampuses($user,$gmt_difference);
    }



    public function getAllClasses($user_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getAllClasses($user_id);
    }
    public function getAllTasks($user_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getAllTasks($user_id);
    }

    public function getCampusClasses($user_id, $campus_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getCampusClasses($user_id, $campus_id);
    }

    public function getSubmissionsChart($user_id, $round_id, $from_time, $to_time){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getSubmissionsChart($user_id, $round_id, $from_time, $to_time);
    }

    public function getTeacherCampusRound($user_id, $round_id,$gmt_difference){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherCampusRound($user_id, $round_id,$gmt_difference);
    }

    public function getTeacherCampuses($user_id,$gmt_difference){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherCampuses($user_id,$gmt_difference);
    }

    public function getStudentRunningRounds($user_id,$gmt_difference,$count=null){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentRunningRounds($user_id,$gmt_difference, $count);
    }

    public function setFirstLoginTime($user_id,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->setFirstLoginTime($user_id,$gmt_difference);
    }

    public function getStudentsInRound($user_id, $round_id,$search){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getStudentsInRound($user_id, $round_id,$search);
    }

    public function getStudentsIDsInRound($user_id, $round_id){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getStudentsIDsInRound($user_id, $round_id);
    }


    public function getStudentRoundClasses($user_id,$round_id,$is_default=true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentRoundClasses($user_id,$round_id,$is_default);
    }
    public function getStudentRoundClassesData($user_id,$round_id,$is_default=true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentRoundClassesData($user_id,$round_id,$is_default);
    }
    public function getStudentClassTasks($user_id,$round_id, $class_id,$gmt_difference, $is_default){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentClassTasks($user_id,$round_id, $class_id, $gmt_difference,$is_default);
    }

    public function getClassTasks($user_id,$round_id, $class_id, $is_default){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getClassTasks($user_id,$round_id, $class_id,$is_default);
    }

    public function getWelcomeTasks($user_id,$round_id, $class_id, $is_default){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getWelcomeTasks($user_id,$round_id, $class_id,$is_default);
    }

    public function updateRoundActivityMissionProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$userCode=null,$is_default = true,$gmt_difference){

        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityMissionProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$userCode,$is_default,$gmt_difference);
    }

    public function updateRoundActivityMissionHandoutProgress($mission_id,$user_id,$round_id,$activity_id,$is_default = true,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityMissionHandoutProgress($mission_id,$user_id,$round_id,$activity_id,$is_default,$gmt_difference);
    }

    public function updateRoundActivityHTMLMissionProgress($mission_id,$user_id,$round_id,$activity_id,$userCode=null,$is_default = true,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityHTMLMissionProgress($mission_id,$user_id,$round_id,$activity_id,$userCode,$is_default,$gmt_difference);
    }

    public function submitRoundActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $round_id, $activity_id,$noOfBlocks,$timeTaken,$practice=false,$is_default = true,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->submitRoundActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $round_id, $activity_id,$noOfBlocks,$timeTaken,$practice,$is_default,$gmt_difference);
    }

    public function submitRoundActivityQuiz($data, $quiz_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference,$is_default = true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->submitRoundActivityQuiz($data, $quiz_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference, $is_default);
    }

    public function submitRoundActivityMiniProjectQuiz($data, $mission_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference,$is_default = true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->submitRoundActivityMiniProjectQuiz($data, $mission_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference, $is_default);
    }

    public function updateRoundActivityMissionCodingProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$timeTaken = null,$userCode=null,$is_default = true,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityMissionCodingProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$timeTaken,$userCode,$is_default,$gmt_difference);
    }

    public function updateRoundActivityMissionEditorProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default = true,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityMissionEditorProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default,$gmt_difference);
    }

    public function updateRoundActivityMissionAngularProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default = true,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityMissionAngularProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default,$gmt_difference);
    }

    public  function updateRoundActivityMiniProjectProgress($mission_id,$user_id,$round_id,$activity_id,$project_url,$is_default,$duration,$gmt_difference){

        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoundActivityMiniProjectProgress($mission_id,$user_id,$round_id,$activity_id,$project_url,$is_default,$duration,$gmt_difference);


    }

    //Save Watched Video Flag
    public function saveWatchedVideoFlag($round_id,$user_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->saveWatchedVideoFlag($round_id,$user_id);
    }

    public function getIntroDetails($round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getIntroDetails($round_id);

    }


    //admin
    public function  getAdminCampusRounds($user_id,$campus_id,$gmt_difference,$is_default=true){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminCampusRounds($user_id,$campus_id,$gmt_difference,$is_default);
    }

    public function  editAdminCampusRound(CampusRoundRequestDto $campusRoundDto, $user_id,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->editAdminCampusRound( $campusRoundDto, $user_id,$gmt_difference);
    }

    public function  getCalenderData($user_id,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getCalenderData($user_id,$gmt_difference);
    }

    public function  updateCalenderData($user_id,$round_id,$ends_at,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->updateCalenderData($user_id,$round_id,$ends_at,$gmt_difference);
    }

    public function updateCalendarDataInModule($user_id,$round_id,$class_id,$starts_at,$ends_at,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->updateCalendarDataInModule($user_id,$round_id,$class_id,$starts_at,$ends_at,$gmt_difference);
    }

    public function getClassTasksAndRooms($user_id,$class_id,$round_id,$is_default){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getClassTasksAndRooms($user_id,$class_id,$round_id,$is_default);
    }

    public function setAssignedAndUnassignedStudentsInCampusRound($user_id,$campus_round_id, $unAssignedStudents, $assignedStudents,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->setAssignedAndUnassignedStudentsInCampusRound($user_id,$campus_round_id, $unAssignedStudents, $assignedStudents,$gmt_difference);
    }

    public function setAssignedAndUnassignedTeachersInCampusRound($user_id,$campus_round_id, $unAssignedTeachers, $assignedTeachers,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->setAssignedAndUnassignedTeachersInCampusRound($user_id,$campus_round_id, $unAssignedTeachers, $assignedTeachers,$gmt_difference);
    }

    public function  createCampusRound(CampusRoundRequestDto $campusRoundRequestDto,$user_id,$is_default=true){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->createCampusRound($campusRoundRequestDto,$user_id,$is_default);
    }
    public function getRoundData($user_id,$round_id,$gmt_difference,$is_default=true){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $adminService->getRoundData($user_id,$round_id,$gmt_difference,$is_default);

    }
    public function getCampusClassesData($user_id,$campus_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->getCampusClassesData($user_id,$campus_id);

    }

    public function getCampusClassTasks($user_id,$class_id,$is_default){

        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->getCampusClassTasks($user_id,$class_id,$is_default);

    }
    public function getRoundClassTasks($user_id,$round_id,$class_id,$is_default=true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->getRoundClassTasks($user_id,$round_id,$class_id,$is_default);

    }


    public function getCampusClassId($user_id,$campus_id,$activity_id,$gmt_difference,$is_default=true){
        $campusId = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusId->getCampusClassId($user_id,$campus_id,$activity_id,$gmt_difference,$is_default);

    }

    public function updateDayTipStatus($user_id,$show){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->updateDayTipStatus($user_id,$show);

    }


    public function updateStudentCurrentStep($user_id,$round_id,$activity_id,$task_id,$current_step,$gmt_difference,$is_default=true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->updateStudentCurrentStep($user_id,$round_id,$activity_id,$task_id,$current_step,$gmt_difference,$is_default);

    }


    public function updateStudentCurrentSection($user_id,$round_id,$activity_id,$task_id,$current_section,$gmt_difference,$is_default=true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->updateStudentCurrentSection($user_id,$round_id,$activity_id,$task_id,$current_section,$gmt_difference,$is_default);

    }


    public function updateRoundClassTasks($user_id, $round_id,$class_id,$tasks,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->updateRoundClassTasks($user_id, $round_id,$class_id,$tasks,$gmt_difference);

    }

    public function updateStudentLastSeen($user_id,$round_id,$activity_id,$task_id,$gmt_difference,$is_default=true){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->updateStudentLastSeen($user_id,$round_id,$activity_id,$task_id,$gmt_difference,$is_default);

    }
    public function calculateCourseLessonsTime($campus_ids){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $campusService->calculateCourseLessonsTime($campus_ids);

    }
    public function getRoundDataWithoutHtml($user_id,$round_id,$gmt_difference,$is_default=true){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        return $adminService->getRoundDataWithoutHtml($user_id,$round_id,$gmt_difference,$is_default);

    }
    public function getAdminStudents($user_id ,$start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminStudents($user_id ,$start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference);
    }

    public function getCampusAdminStudents($user_id, $campus_id, $start_from, $limit, $search, $order_by,$order_by_type, $gmt_difference,$count=null){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getCampusAdminStudents($user_id, $campus_id, $start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference, $count);
    }

    public function getAdminTeachers($user_id ,$start_from, $limit, $search,$order_by,$order_by_type){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminTeachers($user_id ,$start_from, $limit, $search,$order_by,$order_by_type);
    }

    public function getAdminStudentsCount($user_id ,$search,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminStudents($user_id ,null,null, $search,null,null,$gmt_difference,true);
    }

    public function getAdminTeachersCount($user_id , $search){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminTeachers($user_id ,null,null, $search,null,null,true);
    }

    public function updateCampusStudent($user_id,UserDto  $studentDto){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->updateCampusStudent($user_id, $studentDto);
    }

    public function updateCampusTeacher($user_id,UserDto  $teacherDto){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->updateCampusTeacher($user_id, $teacherDto);
    }

    public function deleteStudents($user_id,$students,$force=false){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->deleteStudents($user_id,$students,$force);
    }

    public  function deleteTeachers($user_id,$teachers,$force=false){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->deleteTeachers($user_id,$teachers,$force);
    }
    public function deleteCampusRound($user_id,$round_id,$force=false){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->deleteCampusRound($user_id,$round_id,$force);

    }
    public function getCampusInfo($user_id,$round_id){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getCampusInfo($user_id,$round_id);

    }

    public function getCampusObjectives($user_id,$round_id){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getCampusObjectives($user_id,$round_id);

    }

    public function getTeacherRoundClasses($user_id, $round_id){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherRoundClasses($user_id, $round_id);
    }

    public function getTeacherRoundClass($user_id, $round_id, $class_id,$gmt_difference,$is_default=true){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherRoundClass($user_id, $round_id, $class_id,$gmt_difference,$is_default);
    }

    public function getRoundClassSubmissions($user_id, $round_id, $activity_id, $start_from, $limit, $search,$order_by, $is_default=true){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getRoundClassSubmissions($user_id, $round_id, $activity_id, $start_from, $limit, $search,$order_by, $is_default);
    }
    public function getRoundClassSubmissionsCount($user_id, $round_id, $activity_id,$search=null,$is_default=true){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getRoundClassSubmissionsCount($user_id, $round_id, $activity_id,$search, $is_default);
    }

    public function getStudentSubmission($user_id, $round_id, $campus_activity_progress_id){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getStudentSubmission($user_id, $round_id, $campus_activity_progress_id);
    }

    public function updateStudentSubmissionEvaluation($user_id, $round_id, $campus_activity_progress_id, $evaluation,$gmt_difference){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->updateStudentSubmissionEvaluation($user_id, $round_id, $campus_activity_progress_id, $evaluation,$gmt_difference);
    }

    public function getRoundClassRooms($user_id, $round_id, $class_id){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getRoundClassRooms($user_id, $round_id, $class_id);
    }

    public function updateCampusRound($user_id, $round_id, $starts_at, $ends_at, $classes,  $tasks,  $rooms,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateCampusRound($user_id, $round_id, $starts_at, $ends_at, $classes, $tasks, $rooms,$gmt_difference);
    }
    public function getTeacherRoundsByStatus($user_id, $status,$gmt_difference){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherRoundsByStatus($user_id, $status,$gmt_difference);

    }
    public function getAdminRoundsByStatus($user_id, $status,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminRoundsByStatus($user_id, $status,$gmt_difference);

    }
    public function updateRoundClass($user_id, $round_id,array $classes){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->updateRoundClass($user_id, $round_id, $classes);
    }

    public function createRoom($user_id, $round_id, $class_id, $starts_at, $name,$gmt_difference ,$file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->createRoom($user_id, $round_id, $class_id, $starts_at, $name,$gmt_difference, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name);
    }
    public function updateRoom($user_id, $room_id,$starts_at, $name, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateRoom($user_id, $room_id,$starts_at, $name, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name);
    }

    public function getTeacherSubmissionsNotifications($user_id){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherSubmissionsNotifications($user_id);
    }

    public function generalTeacherStatisticsInCampuses($user_id,$gmt_difference){
        $teacherService = new TeacherService($this->accountRepository,$this->professionalRepository,$this->journeyRepository);
        return $teacherService->generalTeacherStatisticsInCampuses($user_id,$gmt_difference);
    }
    public function generalAdminStatisticsInCampuses($user_id,$gmt_difference){
        $asminService = new AdminService($this->accountRepository,$this->professionalRepository,$this->journeyRepository);
        return $asminService->generalAdminStatisticsInCampuses($user_id,$gmt_difference);
    }

    public function getTopStudentsInCampusRound($user_id, $round_id){
        $teacherService = new TeacherService($this->accountRepository,$this->professionalRepository,$this->journeyRepository);
        return $teacherService->getTopStudentsInCampusRound($user_id, $round_id);
    }

    public function join($user_id, $room_id){
        $bbbService = new BBBService($this->accountRepository, $this->professionalRepository);
        return $bbbService->join($user_id, $room_id);
    }

    public function getRecords($user_id, $room_id){
        $bbbService = new BBBService($this->accountRepository, $this->professionalRepository);
        return $bbbService->getRecords($user_id, $room_id);
    }

    public function joined($user_id, $room_id){
        $bbbService = new BBBService($this->accountRepository, $this->professionalRepository);
        return $bbbService->joined($user_id, $room_id);
    }

    public function end($user_id, $room_id){
        $bbbService = new BBBService($this->accountRepository, $this->professionalRepository);
        return $bbbService->end($user_id, $room_id);
    }

    public function start($user_id, $room_id, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name,$checkAlreadyJoined=true){
        $bbbService = new BBBService($this->accountRepository, $this->professionalRepository);
        return $bbbService->start($user_id, $room_id, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name,$checkAlreadyJoined);
    }

    public function getTeacherClassTasks($user_id, $class_id,$round_id, $is_default=true){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherClassTasks($user_id, $class_id,$round_id, $is_default);
    }

    public function getTeacherClassActivity($user_id, $class_id, $is_default = true){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherClassActivity($user_id, $class_id, $is_default);
    }

    public function getStudentsInRoundToEnroll($user_id,$round_id,$search,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getStudentsInRoundToEnroll($user_id,$round_id,$search,$gmt_difference);

    }
    public function getTeachersInRoundToEnroll($user_id,$round_id,$search){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getTeachersInRoundToEnroll($user_id, $round_id,$search);

    }

    public function checkRoundInCampus($campus_id,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->checkRoundInCampus($campus_id,$round_id);

    }
    public function checkUserInRound($student_id,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->checkUserInRound($student_id,$round_id);

    }

    public function addTeacher($userId,UserDto $userDto){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->addTeacher($userId,$userDto);
    }

    public function addStudent($userId,UserDto $student){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->addStudent($userId,$student);
    }

    public function addStudentsCSV($userId,$csvData){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->addStudentsCSV($userId,$csvData);
    }
    public function getAdminRecentActivity($user_id,$gmt_difference){
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAdminRecentActivity($user_id,$gmt_difference);
    }

    public function sendProfessionalSupportEmail($user_id,SupportEmailDto $supportEmailDto){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->sendProfessionalSupportEmail($user_id,$supportEmailDto);
    }

    public function getTeacherRecentActivity($user_id,$gmt_difference){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->getTeacherRecentActivity($user_id,$gmt_difference);
    }

    public function roomWebHook($event){
        $bbbService = new BBBService($this->accountRepository, $this->professionalRepository);
        return $bbbService->roomWebHook($event);
    }

    public function sendSolveTaskNotifications($id,$round_id,$class_id,$task_id,$date, $is_default=true){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->sendSolveTaskNotifications($id,$round_id,$class_id,$task_id,$date,$is_default);

    }

    public function getMaterialsClass($round_id,$class_id) {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getMaterialsClass($round_id,$class_id);
    }
    public function getStudentProgress($user_id,$round_id,$class_id,$task_id,$is_default=true)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentProgress($user_id,$round_id,$class_id,$task_id,$is_default);
    }
    public function getProfessionalSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getProfessionalSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count);
    }


    public function getlessonsSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getlessonsSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count);
    }

    public function searchForTasks($search,$start_from,$limit,$user_id,$gmt_difference,$count)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->searchForTasks($search,$start_from,$limit,$user_id,$gmt_difference,$count);
    }

    public function searchForCountTasks($search)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->searchForCountTasks($search);
    }

    public function searchForAll($search,$start_from,$limit,$user_id,$gmt_difference,$count=false)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->searchForAll($search,$start_from,$limit,$user_id,$gmt_difference,$count);
    }

    public function calculateScore($campus_ids)
    {
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->calculateScore($campus_ids);
    }

    public function uploadMaterialToClass($user_id, $round_id, $class_id,$file_name , $file_s3_url) {
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->uploadMaterialToClass($user_id, $round_id, $class_id,$file_name , $file_s3_url);
    }
    public function deleteMaterialFromClass($user_id, $round_id, $class_id,$material_id,$force){
        $teacherService = new TeacherService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $teacherService->deleteMaterialFromClass($user_id, $round_id, $class_id,$material_id,$force);

    }

    public function getAllColorPalettes()
    {
        $adminService = new AdminService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $adminService->getAllColorPalettes();
    }
    public function routeConsumerUsers($consumer_id,$userDto,$round_lti_id){
        $ltiService = new LTIService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $ltiService->routeConsumerUsers($consumer_id,$userDto,$round_lti_id);

    }

    public function routeConsumerUsers2($userDto,$round_lti_id,$request_data){
        $ltiService = new LTIService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $ltiService->routeConsumerUsers2($userDto,$round_lti_id,$request_data);

    }

    public function authenticateLTIUser($account_id,$userDto,$round_lti_id){
        $ltiService = new LTIService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $ltiService->authenticateLTIUser($account_id,$userDto,$round_lti_id);

    }

    public function routeD2lLti($consumer_id,$request_data){
        $ltiService = new LTIService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $ltiService->routeD2lLti($consumer_id,$request_data);

    }public function ltiSynchBack(){
        $ltiService = new LTIService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $ltiService->ltiSynchBack();

    }

    //lesson bricks
    public function submitLessonDragQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id){
        $lessonService = new LessonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $lessonService->submitLessonDragQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);

    }

    
    public function submitLessonDropdownQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id){
        $lessonService = new LessonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $lessonService->submitLessonDropdownQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);

    }
    // MCQ
    public function submitLessonMcqQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id){
        $lessonService = new LessonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $lessonService->submitLessonMcqQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);
    }
    // Matching 
    public function submitLessonMatchQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id){
        $lessonService = new LessonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $lessonService->submitLessonMatchQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);
    }
    public function getCampusDictionary($campus_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getCampusDictionary($campus_id);
    }

    //Student Streaks
    public function getStudentStreaks($user,$round_id,$gmt_difference){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentStreaks($user,$round_id,$gmt_difference);
    }    

    public function getLearnerProgress($user,$student_id,$round_id,$gmt_differences){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getLearnerProgress($user,$student_id,$round_id,$gmt_differences);
    }    

    public function getCourseDetails($user,$campus_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=null){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getCourseDetails($user,$campus_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);
    }    

    public function getCourseLearners($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=null){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getCourseLearners($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);
    }


    public function getCourseModuleDetails($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getCourseModuleDetails($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);
    }

    public function getModuleMissionLearners($user,$campus_id,$module_id,$task_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getModuleMissionLearners($user,$campus_id,$module_id,$task_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);
    }

    public function getRoundLearnersProgress($user,$round_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getRoundLearnersProgress($user,$round_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);

    }

    public function updateHideBlocks($user,$user_id,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->updateHideBlocks($user,$user_id,$round_id);
    }

    public function bulkUpdateHideBlocks($user,$learners,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->bulkUpdateHideBlocks($user,$learners,$round_id);
    }

    public function getStudentCampusStatusData($user,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentCampusStatusData($user,$round_id);
    }

    public function getStudentCampusScoreStatistics($user,$round_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getStudentCampusScoreStatistics($user,$round_id);
    }

    public function getAllStudentCampusStatusData($user){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getAllStudentCampusStatusData($user);
    }

    public function cleanDropdown($user,$delete=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->cleanDropdown($user,$delete);
    }

    public function progressFix($user,$fix=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->progressFix($user,$fix);
    }

    public function lastSeenFix($user,$fix=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->lastSeenFix($user,$fix);
    }

    public function pythonFix($user,$fix=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->pythonFix($user,$fix);
    }

    public function evalFix($user,$fix=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->evalFix($user,$fix);
    }

    public function checkEditorMissionBug($user,$fix=false){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->checkEditorMissionBug($user,$fix);
    }

    public function createSearchVault($course_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->createSearchVault($course_id);
    }

    public function createSearchCoursesVault(){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->createSearchCoursesVault();
    }
    public function getModuleSubModules($user_id,$round_id,$module_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getModuleSubModules($user_id,$round_id,$module_id);

    }
    public function getSubModuleTasks($user_id,$round_id,$module_id,$sub_module_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->getSubModuleTasks($user_id,$round_id,$module_id,$sub_module_id);
    }

    public function studentGiveUpMission($user_id,$round_id,$module_id,$sub_module_id,$mission_type,$task_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->studentGiveUpMission($user_id,$round_id,$module_id,$sub_module_id,$mission_type,$task_id);
    }

    public function studentGiveUpMissionGeneral($user_id,$round_id,$module_id,$mission_type,$task_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->studentGiveUpMissionGeneral($user_id,$round_id,$module_id,$mission_type,$task_id);
    }

    public function createSubModules($campus_id){
        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $campusService->createSubModules($campus_id);
    }

    public function getNextTaskInfo($activity_id ,$sub_module_id, $mission_id,$round_id,$user_id, $is_default){
        $submoduleService = new SubmoduleService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        return $submoduleService->getNextTaskInfo($activity_id,$sub_module_id, $mission_id,$round_id,$user_id,$is_default);
    }
}