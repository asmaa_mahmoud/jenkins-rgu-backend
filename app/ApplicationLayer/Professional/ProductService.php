<?php

namespace App\ApplicationLayer\Professional;

use App\ApplicationLayer\Professional\Dtos\ProductDto;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;


class ProductService {
    private $professionalRepository;

    public function __construct(IProfessionalMainRepository $professionalRepository)
    {
        $this->professionalRepository = $professionalRepository;
    }

    public function getProductAssets($product_url)
    {
        $productAssets = $this->professionalRepository->getProductAssets($product_url);
        if($productAssets)
            $productAssets = Mapper::MapClass(ProductDto::class,$productAssets);
        return $productAssets;
    }

}
