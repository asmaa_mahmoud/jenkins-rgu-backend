<?php

namespace App\ApplicationLayer\Professional\Interfaces;

use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Professional\Dtos\AnalysisAnswerDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundRequestDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;

interface IProfessionalMainService
{
    public function getProductAssets($product_url);

    public function getAllQuestions();
    public function unlockStudentRound($user_id,$round_id);
    public function getCategoryQuestions($category_id);
    public function getAllCategories();
    public function getQuestion($question_id);
    public function getAnswer($answer_id);
    public function getAllAnswers($user_id);
    //public function submit($user_id, array $answerDtos, $categoryCode, $visualProfile);
    public function submit($user_id, $userAnswers, $categoryCode, $visualProfile, $categoryID);
    public function deleteCategoryPostSubmissions($user_id, $categoryID);
    public function getStudentLearningPath($student_id);
    public function getAllLearningPath($student_id);
    public function getLearningPathById($user_id,$id);
    public function getLearnPathStages($user_id,$learning_path_id=null);
    public function getTestLearnPathStages($user_id);
    public function getStageActivities($user_id,$stage_number,$learning_path_id, $is_default);
    public function getActivity($user_id, $learning_path_id, $activity_id, $is_default);
    public function getActivityTasks($user_id, $learning_path_id, $activity_id, $is_default);
    public function getStudentLearningPathLessons($user_id, $learning_path_id, $is_default);
    public function getHtmlMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id,$user_id, $is_default);
    public function getHtmlMissionNext($learning_path_id=null, $campus_id=null, $activity_id , $submodule_id,$mission_id,$round_id,$user_id, $is_default);
    public function getProjectData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id,$user_id, $is_default);
    public function getMissionData($learning_path_id=null, $campus_id=null,$activity_id, $mission_id,$user_id, $is_default);
    public function getHandoutMissionData($campus_id=null,$activity_id, $mission_id,$user_id, $is_default);
    public function getHandoutMissionNext($campus_id=null, $activity_id , $submodule_id,$mission_id,$round_id,$user_id, $is_default);

    public function getMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id,$round_id,$user_id, $is_default);
    public function getQuizNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id, $mission_id,$round_id,$user_id, $is_default);
    public function checkMissionAuthorization($learning_path_id, $activity_id , $mission_id,$user_id, $is_default=true);
    public function getHtmlBlocklyCategories();
    public function updateActivityMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$is_default = true);
    public function updateActivityHTMLMissionProgress($mission_id,$user_id,$learning_path_id,$activity_id,$is_default = true);
    public function submitActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $learning_path_id, $activity_id,$noOfBlocks,$timeTaken,$is_default = true);
    public function submitActivityQuiz($data, $quiz_id, $user_id,$learning_path_id, $activity_id,$is_default = true);
    public function getUserSubmission($user_id);
    public function getCategorySubmission($user_id,$category_code);
    public function getSectionSubmissions($user_id,$categories);
    public function getCodingMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true);
    public function getCodingMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id,$mission_id, $round_id,$user_id,$is_default);
    public function getMissionEditorData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true);
    public function getMissionEditorNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id,$mission_id,$round_id,$user_id, $is_default);
    public function getAngularMissionData($learning_path_id=null, $campus_id=null, $activity_id=null, $mission_id, $user_id, $is_default=true);
    public function getAngularMissionNext($learning_path_id=null, $campus_id=null, $activity_id ,$submodule_id,$mission_id,$round_id,$user_id, $is_default);

    public function getNextClass($round_id, $activity_id , $is_default);
    public function updateActivityMissionCodingProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$timeTaken = null,$is_default = true);
    public function updateActivityMissionEditorProgress($mission_id,$user_id,$learning_path_id,$activity_id,$success_flag,$is_default = true);
    public function getPracticeMission($mission_id, $type);
    public function checkCampusMissionAuthorization($round_id, $activity_id , $mission_id,$user_id, $is_default=true);

    ///////////////////////////Campus//////////////////////////
    public function getAllCampuses($user_id);
    public function getSupportCampuses($user,$gmt_difference);
    public function getAllClasses($user_id);
    public function getAllTasks($user_id);
    public function getCampusClasses($user_id, $campus_id);
    public function getSubmissionsChart($user_id, $round_id, $from_time, $to_time);
    public function getAdminRecentActivity($user_id,$gmt_difference);
    public function sendProfessionalSupportEmail($user_id,SupportEmailDto $supportEmailDto);
    public function getCampusClassId($user_id,$campus_id,$activity_id,$gmt_difference,$is_default);
    //teacher
    public function getTeacherCampusRound($user_id, $round_id,$gmt_difference);
    public function getTeacherCampuses($user_id,$gmt_difference);
    public function getStudentsInRound($user_id, $round_id,$search);
    public function getStudentsIDsInRound($user_id, $round_id);
    public function getTeacherRoundClasses($user_id, $round_id);
    public function getTeacherRoundClass($user_id, $round_id, $class_id,$gmt_difference,$is_default=true);
    public function getRoundClassSubmissions($user_id, $round_id, $activity_id, $start_from, $limit, $search,$order_by, $is_default=true);
    public function getRoundClassSubmissionsCount($user_id, $round_id, $activity_id,$search=null, $is_default=true);
    public function getStudentSubmission($user_id, $round_id, $campus_activity_progress_id);
    public function updateStudentSubmissionEvaluation($user_id, $round_id, $campus_activity_progress_id, $evaluation,$gmt_difference);
    public function getRoundClassRooms($user_id, $round_id, $class_id);
    public function updateCampusRound($user_id, $round_id, $starts_at, $ends_at,  $classes,  $tasks, $rooms,$gmt_difference);
    public function getTeacherRoundsByStatus($user_id, $status,$gmt_difference);
    public function getAdminRoundsByStatus($user_id, $status,$gmt_difference);
    public function updateRoundClass($user_id, $round_id,array $classes);
    public function createRoom($user_id, $round_id,$class_id, $starts_at, $name,$gmt_difference, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name);
    public function getTeacherSubmissionsNotifications($user_id);
    public function getTeacherRecentActivity($user_id,$gmt_difference);
    public function sendSolveTaskNotifications($user_id,$round_id,$class_id,$task_id,$date,$is_default=true);

    public function getTeacherClassTasks($user_id, $class_id,$round_id, $is_default=true);
    public function getTeacherClassActivity($user_id, $class_id, $is_default=true);
    public function updateRoom($user_id, $room_id,$starts_at, $name, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name);
    public function generalTeacherStatisticsInCampuses($user_id,$gmt_difference);
    public function generalAdminStatisticsInCampuses($user_id,$gmt_difference);
    public function getTopStudentsInCampusRound($user_id, $round_id);

    public function getStudentsInRoundToEnroll($user_id,$round_id,$search,$gmt_difference);
    public function getTeachersInRoundToEnroll($user_id,$round_id,$search);


    //student
    public function setFirstLoginTime($user_id,$gmt_difference);
    public function getStudentRunningRounds($user_id,$gmt_difference,$count=null);
    public function getStudentRoundClasses($user_id,$round_id,$is_default=true);
    public function getStudentRoundClassesData($user_id,$round_id,$is_default=true);
    public function getStudentClassTasks($user_id,$round_id, $class_id,$gmt_difference, $is_default);
    public function getClassTasks($user_id,$round_id, $class_id, $is_default);
    public function getWelcomeTasks($user_id,$round_id, $class_id, $is_default);

    public function updateRoundActivityMissionProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$userCode=null,$is_default = true,$gmt_difference);
    public function updateRoundActivityHTMLMissionProgress($mission_id,$user_id,$round_id,$activity_id,$userCode=null,$is_default = true,$gmt_difference);
    public function updateRoundActivityMissionHandoutProgress($mission_id,$user_id,$round_id,$activity_id,$is_default = true,$gmt_difference);
    public function submitRoundActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $round_id, $activity_id,$noOfBlocks,$timeTaken,$practice=false,$is_default = true,$gmt_difference);
    public function submitRoundActivityQuiz($data, $quiz_id, $user_id,$round_id, $activity_id,$duration,$gmt_difference,$is_default = true);
    public function submitRoundActivityMiniProjectQuiz($data, $mission_id, $user_id,$round_id, $activity_id,$duration,$gmt_difference,$is_default = true);
    public function updateRoundActivityMissionCodingProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$timeTaken = null,$userCode=null,$is_default = true,$gmt_difference);
    public function updateRoundActivityMissionEditorProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default = true,$gmt_difference);
    public function updateRoundActivityMissionAngularProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default = true,$gmt_difference);
    public function updateRoundActivityMiniProjectProgress($mission_id,$user_id,$round_id,$activity_id,$project_url,$is_default,$duration,$gmt_difference);


    //admin
    public function getAdminCampusRounds($user_id,$campus_id,$gmt_difference,$is_default=true);
    public function getClassTasksAndRooms($user_id,$class_id,$round_id,$is_default);
    public function setAssignedAndUnassignedStudentsInCampusRound($user_id,$campus_round_id, $unAssignedStudents, $assignedStudents,$gmt_difference);
    public function setAssignedAndUnassignedTeachersInCampusRound($user_id,$campus_round_id, $unAssignedTeachers, $assignedSTeachers,$gmt_difference);
    public function createCampusRound(CampusRoundRequestDto $campusRoundRequestDto,$user_id,$is_default=true);
    public function getRoundData($user_id,$round_id,$gmt_difference,$is_default=true);
    public function getCampusClassesData($user_id,$campus_id);
    public function getCampusClassTasks($user_id,$class_id,$is_default);
    public function getRoundClassTasks($user_id,$round_id,$class_id,$is_default);
    public function getRoundDataWithoutHtml($user_id,$round_id,$gmt_difference,$is_default=true);
    public function getAdminStudents($user_id ,$start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference);
    public function getCampusAdminStudents($user_id, $campus_id, $start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference, $count=null);
    public function getAdminTeachers($user_id ,$start_from, $limit, $search,$order_by,$order_by_type);
    public function getAdminStudentsCount($user_id ,$search,$gmt_difference);
    public function getCalenderData($user_id,$gmt_difference);
    public function  updateCalenderData($user_id,$round_id,$due_date,$gmt_difference);
    public function getAdminTeachersCount($user_id, $search);
    public function updateCampusStudent($user_id,UserDto  $studentDto);
    public function updateCampusTeacher($user_id,UserDto  $teacherDto);
    public function deleteStudents($user_id,$students,$force=false);
    public function deleteTeachers($user_id,$teachers,$force=false);

    public function updateDayTipStatus($user_id,$show);
    public function updateStudentCurrentStep($user_id,$round_id,$activity_id,$task_id,$current_step,$gmt_difference,$is_default=true);
    public function updateStudentCurrentSection($user_id,$round_id,$activity_id,$task_id,$current_section,$gmt_difference,$is_default=true);
        
    public function checkRoundInCampus($campus_id,$round_id);
    public function checkUserInRound($student_id,$round_id);
    public function deleteCampusRound($user_id,$round_id,$force=false);
    public function getCampusInfo($user_id,$round_id);
    public function getCampusObjectives($user_id,$round_id);
    public function updateStudentLastSeen($user_id,$round_id,$activity_id,$task_id,$gmt_difference,$is_default=true);
    public function calculateCourseLessonsTime($campus_ids);

    //BBB
    public function join($user_id, $room_id);
    public function getRecords($user_id, $room_id);
    public function joined($user_id, $room_id);
    public function end($user_id, $room_id);
    public function start($user_id, $room_id, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name,$checkAlreadyJoined=true);

    public function addTeacher($userId,UserDto $userDto);
    public function addStudent($userId,UserDto $studentDto);
    public function addStudentsCSV($userId,$file);
    public function updateRoundClassTasks($user_id, $round_id,$class_id,$tasks,$gmt_difference);

    //Material
    public function getMaterialsClass($round_id,$class_id);
    public function uploadMaterialToClass($user_id, $round_id, $class_id,$file_name , $file_s3_url);
    public function deleteMaterialFromClass($user_id, $round_id, $class_id,$material_id,$force);
    public function getStudentProgress($user_id,$round_id,$class_id,$task_id,$is_default=true);

    //Push
    public function roomWebHook($event);

    //LTI
    public function routeConsumerUsers($consumer_id,$userDto,$round_lti_id);
    public function routeConsumerUsers2($userDto,$round_lti_id,$request_data);
    public function authenticateLTIUser($account_id,$userDto,$round_lti_id);
    public function routeD2lLti($consumer_id,$request_data);
    public function ltiSynchBack();

    public function getProfessionalSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count);

    public function getlessonsSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count);
    public function searchForTasks($search,$start_from,$limit,$user_id,$gmt_difference,$count);

    public function searchForCountTasks($search);
    public function searchForAll($search,$start_from,$limit,$user_id,$gmt_difference,$count=false);
    
    public function calculateScore($campus_ids);

    //Lessson bricks
    public function submitLessonDragQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);
    public function submitLessonDropdownQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);
    public function submitLessonMcqQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);
    public function submitLessonMatchQuestion($data, $mission_id,$question_id, $user_id, $round_id, $activity_id);
    public function saveWatchedVideoFlag($round_id,$user_id);
    public function getIntroDetails($round_id);
    public function getCampusDictionary($campus_id);
    //Streaks
    public function getStudentStreaks($user,$round_id,$gmt_difference);
    
    public function getLearnerProgress($user,$student_id,$round_id,$gmt_difference);
    public function getCourseDetails($user,$campus_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);
    public function getCourseLearners($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count);

    public function getCourseModuleDetails($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false);
    
    public function getModuleMissionLearners($user,$campus_id,$module_id,$task_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false);

    public function getRoundLearnersProgress($user,$round_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false);
    public function updateHideBlocks($user,$learner_id,$round_id);
    public function bulkUpdateHideBlocks($user,$learners,$campus_id);
    public function getStudentCampusStatusData($user,$round_id);
    public function getStudentCampusScoreStatistics($user,$round_id);
    public function getAllStudentCampusStatusData($user);
    public function cleanDropdown($user,$delete=false);
    public function progressFix($user,$fix);
    public function lastSeenFix($user,$fix=false);
    public function pythonFix($user,$fix=false);
    public function evalFix($user,$fix=false);
    public function checkEditorMissionBug($user,$fix=false);
    public function createSearchVault($course_id);
    public function createSearchCoursesVault();
    public function getModuleSubModules($user_id,$round_id,$module_id);
    public function getSubModuleTasks($user_id,$round_id,$module_id,$sub_module_id);
    public function studentGiveUpMission($user_id,$round_id,$module_id,$sub_module_id,$mission_type,$task_id);
    public function studentGiveUpMissionGeneral($user_id,$round_id,$module_id,$mission_type,$task_id);

    public function createSubModules($campus_id);
    public function getNextTaskInfo($activity_id , $sub_module_id,$mission_id,$round_id,$user_id, $is_default);
}