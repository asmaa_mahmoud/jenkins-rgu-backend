<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 08/11/2018
 * Time: 2:57 PM
 */

namespace App\ApplicationLayer\Professional;

use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\RoomUser;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Notifications\Session;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;


class BBBService
{
    const Attendee_Password = '12345';
    const Moderator_Password = '54321';
    private $accountRepository;
    private $professionalRepository;

    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
    }


    public function join($user_id, $room_id,$checkAlreadyJoined=true){

        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $room = $this->professionalRepository->getRoomById($room_id);

        if($room == null)
            throw new BadRequestException(trans('locale.room_not_exist')); //TODO translations

        $bbb = new BigBlueButton();
        $username = $user->getUsername();
        // $moderator_password for moderator
        $isTeacher = false;
        $roles = $user->getRoles();
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        $meetingID = $room_id;
        $response = [];
        if($isTeacher)
            $role = $this->accountRepository->getRoleByName("teacher");
        else
            $role = $this->accountRepository->getRoleByName("student");
        if($checkAlreadyJoined==true) {

            $roomUser = $this->professionalRepository->getRoomUser($room, $user, $role);
            if ($roomUser == null) {
                $this->accountRepository->beginDatabaseTransaction();
                $roomUser = new RoomUser($room, $user, $role);
                $this->professionalRepository->storeRoomUser($roomUser);
                $this->accountRepository->commitDatabaseTransaction();
            } else
                throw new BadRequestException(trans('locale.already_joined')); //TODO translation

        }

        if($isTeacher) {
            $name =$username;
            $password = $this::Moderator_Password;
            $joinMeetingParams = new JoinMeetingParameters($meetingID, $name, $password);
            $joinMeetingParams->setRedirect(true);
            $url = $bbb->getJoinMeetingURL($joinMeetingParams);

            $this->accountRepository->commitDatabaseTransaction();
        }else {
            $isRunningParams = new IsMeetingRunningParameters($meetingID);
            $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
            if($isRunning) {
                $name = $username;
                $password = $this::Attendee_Password;
                $joinMeetingParams = new JoinMeetingParameters($meetingID, $name, $password);
                $joinMeetingParams->setRedirect(true);
                $url = $bbb->getJoinMeetingURL($joinMeetingParams);
                $this->accountRepository->commitDatabaseTransaction();
            }
            else {
                throw new BadRequestException(trans('locale.cannot_join_room')); //TODO translation
            }
        }

        $response['url'] = $url;

        return $response;
    }

    public function getRecords($user_id, $room_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $meetingID = $room_id;
        $recordingParams = new GetRecordingsParameters();
        $recordingParams->setMeetingId($meetingID);
        $bbb = new BigBlueButton();
        $response = $bbb->getRecordings($recordingParams);
        $records = [];
        if ($response->getReturnCode() == 'SUCCESS') {
            foreach ($response->getRawXml()->recordings->recording as $recording) {
                // process all recording
                $records []= $recording;
            }
        }

        return $records;
    }

    public function joined($user_id, $room_id){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $room = $this->professionalRepository->getRoomById($room_id);
        if($room == null)
            throw new BadRequestException(trans('locale.room_not_exist')); //TODO translations

        $isTeacher = false;
        $roles = $user->getRoles();
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }
        if($isTeacher)
            $role = $this->accountRepository->getRoleByName("teacher");
        else
            $role = $this->accountRepository->getRoleByName("student");

        if($role == null)
            throw new BadRequestException(trans("locale.no_role_teacher"));

        $roomUser = new RoomUser($room, $user, $role);
        $this->professionalRepository->storeRoomUser($roomUser);
        $room->setIsRunning(1);
        $this->professionalRepository->storeRoom($room);
        $this->accountRepository->commitDatabaseTransaction();

        return $roomUser;
    }

    public function start($user_id, $room_id, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name,$checkAlreadyJoined=true){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $room = $this->professionalRepository->getRoomById($room_id);

        if($room == null)
            throw new BadRequestException(trans('locale.room_not_exist')); //TODO translations

//        if($file_s3_url != ''){
//            $room->setFileLink($file_s3_url);
//        }
        if($file_uploading_flag) {
            $material = new Material($room->getRoundClass());
            $material->setName($file_name);
            $material->setFileLink($file_s3_url);
            $this->professionalRepository->storeMaterial($material);
            $room->setMaterialId($material->getId());
        }elseif ($file_from_list_flag){
            $room->setMaterialId($file_id);
        }else {
            $room->setMaterialId(null);
        }
        $this->professionalRepository->storeRoom($room);
        $bbb = new BigBlueButton();

        $isTeacher = false;
        $roles = $user->getRoles();
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        if($isTeacher) {
            $isRunningParams = new IsMeetingRunningParameters($room->id);
            $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
            if($isRunning) {
                throw new BadRequestException(trans('locale.room_is_running')); //TODO translation
            }
            //check if other rooms are running in same lesson
            $lesson=$room->getRoundClass();
            $lessonRooms=$lesson->getRooms();
            foreach ($lessonRooms as $lessonRoom){
                $roomIsRunningParams = new IsMeetingRunningParameters($lessonRoom->id);
                $roomIsRunning = $bbb->isMeetingRunning($roomIsRunningParams)->isRunning();
                if($roomIsRunning) {
                    throw new BadRequestException(trans('locale.room_named')." ".$lessonRoom->name." ".trans('locale.is_running_in_lesson')); //TODO translation
                }
            }

            $createMeetingParams = new CreateMeetingParameters($room->id, $room->name);
            $createMeetingParams->setAttendeePassword($this::Attendee_Password);
            $createMeetingParams->setModeratorPassword($this::Moderator_Password);
            if($room->getFileLink() != ''){
                $createMeetingParams->addPresentation($room->getFileLink());
            }
            // $createMeetingParams->setDuration($duration);
            // $createMeetingParams->setLogoutUrl($urlLogout);
            $isRecordingTrue = true;
            if ($isRecordingTrue) {
                $createMeetingParams->setRecord(true);
                $createMeetingParams->setAllowStartStopRecording(true);
                $createMeetingParams->setAutoStartRecording(true);
            }

            $response = $bbb->createMeeting($createMeetingParams);

            if ($response->getReturnCode() == 'FAILED') {
                throw new BadRequestException(trans('locale.cannot_create_room')); //TODO translation
            } else {
                // process after room created
                //$res = "Created Meeting with ID: " . $response->getInternalMeetingId();
                return $this->join($user_id, $room_id,$checkAlreadyJoined);
            }
        }else {

            throw new BadRequestException(trans('locale.cannot_create_room')); //TODO translation
        }

    }

    public function end($user_id, $room_id){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));


        $bbb = new BigBlueButton();
        // $moderator_password for moderator
        $isTeacher = false;
        $roles = $user->getRoles();
        foreach ($roles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        $meetingID = $room_id;
        if($isTeacher) {
            $moderator_password = $this::Moderator_Password;
            $endMeetingParams = new EndMeetingParameters($meetingID, $moderator_password);
            $response = $bbb->endMeeting($endMeetingParams);
            if ($response->getReturnCode() != 'SUCCESS') {
                throw new BadRequestException(trans('locale.cannot_end_room')); //TODO translation
            }
        }else {

            throw new BadRequestException(trans('locale.cannot_end_room')); //TODO translation
        }

        $this->accountRepository->commitDatabaseTransaction();
        $room = $this->professionalRepository->getRoomById($room_id);
        $room->setIsRunning(0);
        $this->professionalRepository->storeRoom($room);

        $response = [];
        return $response;
    }

    public function roomWebHook($event) {
        if($event['type'] == 'user-presenter-assigned')
            $msg = 'meeting_started';
        elseif ($event['type'] == 'meeting-ended')
            $msg = 'meeting_ended';
        else
            $msg = $event['type'];

        $room_id = $event['meeting_id'];
        $room = $this->professionalRepository->getRoomById($room_id);
        if($room == null)
            throw new BadRequestException(trans('locale.room_not_exist')); //TODO translations

        $roundClass = $room->getRoundClass();

        $class = $roundClass->getCampusClass();

        $activity = $class = $class->getDefaultActivity();

        $round = $roundClass->getCampusRound();

        $campus = $round->getCampus();

        $teachers = $this->professionalRepository->getCampusRoundTeachers($round->getId());


        $data['course_name'] = $campus->getName();
        $data['course_id'] = $campus->getId();
        $data['round_id'] = $round->getId();
        $data['lesson_name'] = $class->getName();
        $data['lesson_id'] = $class->getId();
        $data['round_lesson_id'] = $roundClass->getId();
        $data['activity_id'] = $activity->getId();
        $data['event'] = $msg;
        $data['room_id'] = $room_id;

        Notification::send($teachers, new Session($data));
    }

}