<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/12/2018
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\Professional;

use Analogue\ORM\EntityCollection;
use App\DomainModelLayer\Professional\DropdownQuestionAnswer;
use App\DomainModelLayer\Professional\MatchQuestionAnswer;
use App\DomainModelLayer\Professional\McqQuestionAnswer;
use App\DomainModelLayer\Professional\DragQuestionAnswer;
use App\DomainModelLayer\Professional\SearchVault;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Common\GradingCommonService;
use App\ApplicationLayer\Professional\Dtos\CampusActivityTaskDto;
use App\ApplicationLayer\Professional\Dtos\DictionaryWordDto;
use App\ApplicationLayer\Professional\Dtos\CampusClassDto;
use App\ApplicationLayer\Professional\Dtos\CampusDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\CampusModuleDto;
use App\ApplicationLayer\Professional\Dtos\MaterialDto;
use App\ApplicationLayer\Professional\Dtos\SubmoduleDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundNamesDto;
use App\ApplicationLayer\Professional\Dtos\CampusReducedDto;
use App\ApplicationLayer\Professional\Dtos\LearnerProgressDto;
use App\ApplicationLayer\Professional\Dtos\LearnersProgressDto;
use App\ApplicationLayer\Professional\Dtos\LearnerDto;
use App\ApplicationLayer\Professional\Dtos\TaskDto;
use App\ApplicationLayer\Professional\Dtos\miniRoundDto;
use App\ApplicationLayer\Professional\Dtos\ClassDto;
use App\ApplicationLayer\Professional\Dtos\CourseDetailsDto;
use App\ApplicationLayer\Professional\Dtos\ModuleDto;
use App\ApplicationLayer\Professional\Dtos\CourseLearnersDto;
use App\ApplicationLayer\Professional\Dtos\CourseLearnerDto;
use App\ApplicationLayer\Professional\Dtos\ModuleMissionLearnersDto;
use App\ApplicationLayer\Professional\Dtos\SingleLearnerDto;

use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\MissionAngularUserAnswer;
use App\DomainModelLayer\Professional\CampusActivityQuestion;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\ApplicationLayer\Common\LessonCommonService;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\DomainModelLayer\Journeys\Quiz;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;

use App\ApplicationLayer\Journeys\Dtos\MissionAngularAnswersDto;

use Illuminate\Pagination\Paginator;

use App\ApplicationLayer\Journeys\Dtos\ChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\CampusUserStatusDto;
use App\Helpers\LTI;
use App\Notifications\StudentStuck;
use App\Notifications\Submission;
use App\Notifications\TaskSubmission;
use App\Notifications\CourseCompleted;
use App\Notifications\EvaluationSubmission;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Carbon\Carbon;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use Illuminate\Support\Facades\Notification;
use Ramsey\Uuid\Uuid;
use App\ApplicationLayer\Professional\Dtos\RoomDto;
use IMSGlobal\LTI\ToolProvider\Outcome;
use IMSGlobal\LTI\ToolProvider\ResourceLink;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use IMSGlobal\LTI\ToolProvider;

use Illuminate\Database\QueryException;
use App\DomainModelLayer\Journeys\MatchKey;

use App\ApplicationLayer\Journeys\Dtos\MissionAngularDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularFilesDto;
use App\Helpers\Mapper;
use Illuminate\Http\Request;
use DB;


class CampusService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonCommonService;
    private $gradingCommonService;

    const Attendee_Password = '12345';
    const Moderator_Password = '54321';


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        $this->gradingCommonService = new GradingCommonService($this->accountRepository, $this->professionalRepository);

    }
    public function unlockStudentRound($user_id,$round_id){//just for dev
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));
        $campus=$round->getCampus();
        $calculatedScore=0;
        $this->accountRepository->beginDatabaseTransaction();
        foreach ($round->getCampus()->getClasses() as $campusClassKey => $campusClass) {
            $activity=$campusClass->getDefaultActivity();
            $activityTasks=$campusClass->getDefaultActivity()->getActivityTasks();
            foreach ($activityTasks as $activityTaskKey => $activityTask) {
                $task=$activityTask->getTask();
                if($activityTask->booster_type!='1'){
                    //not booster 
                    //1-solve it
                     //1.1 solve all bricks in html mission
                    //1.2 last seen
                    //1.3 solve quiz and emulator questions
                    //2-increase score

                    $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($activityTask->getTask(), $user_id, $campus->id, $activity->id,true);
                    $fullMark=$this->lessonCommonService->getMainTaskFullMark($user,$task,$activity,$round);
                    if($fullMark==null)
                        $fullMark=0;
                    if($missionProgress==null){
                        
                        $missionProgress = new CampusActivityProgress($task, $user, $campus,$fullMark,1,1,$fullMark,1,100,100,null,null,null);
                        $missionProgress->setDefaultActivity($activity);
                        $this->professionalRepository->storeRoundActivityProgress($missionProgress);
            
                    }else{
                        $missionProgress->setFirstSuccess(1);
                        $missionProgress->setScore($fullMark);
                        $missionProgress->setEvaluation($fullMark);
                        $missionProgress->setBestTaskDuration(100);
                        $missionProgress->setTaskDuration(100);
                        $this->professionalRepository->storeRoundActivityProgress($missionProgress);

                    }
                    $calculatedScore+=$fullMark;

                    if($task->getTaskNameAndType()['task_type']=="html_mission"){
                        $mission=$task->getMissionsHtml()->first();
                        $missionHtmlSteps=$mission->getSteps();
                        foreach($missionHtmlSteps as $data)
                        {
                            if($data->getBrickType() == 'quizbricks'){
                                /*try{
                                    $question_id=$data->getQuestionTask()->question_id;
                                }catch(\Exception $e){
                                    dd($e,$data);
                                }*/
                                if($question_id=$data->getQuestionTask()!==null){
                                $question_id=$data->getQuestionTask()->question_id;
                                $question= $this->journeyRepository->getQuestionById($question_id);
                                if($question == null)
                                    throw new BadRequestException(trans('locale.question_not_exist'));

                                $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$question);
                                if($campusActivityQuestion == null){

                                    $campusActivityQuestion=new CampusActivityQuestion($missionProgress,$question);
                                    $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
                                }
                                $campusActivityQuestionId=$campusActivityQuestion->id;
                                $questionType=$data->getQuestionTask()->question->question_type;
                                if($questionType == 'question'){
                                    $mcqQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'multiple_choice');

                                    if(count($mcqQuestionAnswers)>0){
                                        $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'multiple_choice');
                                    }
                                    $mcqQuestionId=$data->getQuestion()->id;
                                    $correctChoices = $this->journeyRepository->getChoiceModelforMcqQuestion($mcqQuestionId)->toArray();

                                    foreach ($correctChoices as $correct_choice) {
                                        $choice_instance = $this->journeyRepository->getChoiceById($correct_choice['id']);
                                        $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice_instance,'1');
                                        $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
                                            
                                    }

                                }elseif($questionType == 'sequence_match'){
                                    $dragQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'sequence_match');

                                    if(count($dragQuestionAnswers)>0){
                                        $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'sequence_match');
                                    }
                                    $dragCells=$question->getDragQuestion()->getDragCells();
                                    foreach ($dragCells as $key=>$dragCell) {
                                        $cellChoice=$dragCell->getDragChoice();
                                        $dragQuestionAnswer=new DragQuestionAnswer($campusActivityQuestion,$dragCell,$cellChoice);
                                        $this->professionalRepository->storeDragQuestionAnswer($dragQuestionAnswer);
                                        
                                    }
                                }elseif($questionType == 'dropdown'){
                                    $dropdownQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'dropdown');
                                    if(count($dropdownQuestionAnswers)>0){
                                        $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'dropdown');
                                    }
                                    $dropdowns=$question->getDropdownQuestion()->getDropdown();

                                    foreach ($dropdowns as $dropdown) {
                                        
                                        $dropdownChoice = $dropdown->getDropdownChoice();
                                        if($dropdownChoice == null)
                                            throw new BadRequestException(trans('locale.dropdown_choice_not_exist'));

                                        $dropdownCorrectChoice = $this->journeyRepository->getDropdownCorrectAnswer($dropdown->id);
                                        if($dropdownCorrectChoice == null)
                                            throw new BadRequestException(trans('locale.dropdown_correct_choice_not_exist').$dropdown->id);

                                        $dropdownQuestionAnswer=new DropdownQuestionAnswer($campusActivityQuestion,$dropdown,$dropdownCorrectChoice);
                                        $this->professionalRepository->storeDropdownQuestionAnswer($dropdownQuestionAnswer);
                                    }
                                }elseif($questionType == 'match'){
                                    $matchQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'match');

                                    if(count($matchQuestionAnswers)>0){
                                        $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'match');
                                    }
                                    $matchQuestion=$question->getMatchQuestion();

                                    foreach($matchQuestion->getMatchKeys() as $matchKey){
                                        $matchValue=$matchKey->getMatchValue();
                                        $matchQuestionAnswer=new MatchQuestionAnswer($campusActivityQuestion,$matchKey,$matchValue);
                                        $this->professionalRepository->storeMatchQuestionAnswer($matchQuestionAnswer);
                                    }
                                }
                            }

                            }
                        }

                    }if($task->getTaskNameAndType()['task_type']=="mini_project"){
                        $missionProgress->setUserCode("https://github.com/nohadrweesh/ASN");
                        $this->professionalRepository->storeRoundActivityProgress($missionProgress);

                    }
                
                }

            }
        }
        //check for rank and level
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusUserStatus->xp=$calculatedScore;
        $rankLevelId = $this->lessonCommonService->calculateUserRankLevel($calculatedScore, $campus);
        $campusUserStatus->setRankLevelId($rankLevelId);

        $campusUserStatus->rank_level_id=ceil($calculatedScore/$campus->xps);
        $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
        $this->accountRepository->commitDatabaseTransaction();

    }

    public function setFirstLoginTime($user_id,$gmt_difference){
        $user = $this->accountRepository->getUserById($user_id);

        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if($user->isStudent()){
            if($user->first_login_time ==null)
            {
                $user->first_login_time=Carbon::now()->addHours($gmt_difference)->startOfDay()->timestamp;
                $this->accountRepository->storeUser($user);
            }
        }
        return trans('locale.login_time_updated_successfully');


    }

    public function getStudentRunningRounds($user_id,$gmt_difference,$count=null){
        $student = $this->accountRepository->getUserById($user_id);

        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));
        $campus_rounds = $this->professionalRepository->getStudentRunningRounds($student->getId(),$gmt_difference,$count);
       
        if($count===null){
            $rounds = Mapper::MapEntityCollection(CampusRoundNamesDto::class, $campus_rounds);
            
            foreach ($rounds as $round) {
                $watched = $this->professionalRepository->getWatchedVideoFlag($round->id,$student->getId());
                $campusRound = $this->professionalRepository->getRoundById($round->id);
                $progress = $this->lessonCommonService->getLearnerCampusProgress($student->getId(),$campusRound)['progress'];
                // $solvedTasksCount = $this->professionalRepository->getSolvedTasksInRound($round->campus_id,$student->getId());
                // $allTasksCount = $this->professionalRepository->getAllTasksInRound($round->campus_id);
                if($watched == 1){
                    $round->introVideoWatched = true;
                }else{
                    $round->introVideoWatched = false;
                }

                // $round->solvedTasksCount = $solvedTasksCount;
                // $round->allTasksCount = $allTasksCount;
                // $round->progress = round(($solvedTasksCount/$allTasksCount)*100);
                $round->progress = $progress;
            }
            return $rounds;
        }else
            return $campus_rounds;
    }

    public function getStudentRoundClasses($user_id,$round_id,$is_default){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();

        $checkRoundInCampus=$this->checkRoundInCampus($campus_id,$round_id);
        if(!$checkRoundInCampus)
            throw new BadRequestException(trans("locale.round_is_not_in_campus"));

        $checkStudentInRound=$this->checkUserInRound($student->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));


        $roundClasses = $this->professionalRepository->getRoundClasses($round_id);
        $bbb = new BigBlueButton();
        $classDtos=[];
        foreach ($roundClasses as $roundClass){

            $class=$roundClass->getCampusClass();

            if($is_default) {
                $activity_id = $class->getDefaultActivity()->getId();
                $activity= $class->getDefaultActivity();
            }
            else {
                $activity_id = $class->getActivity()->getId();
                $activity = $class->getActivity();
            }
            $class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$class->getCampus()->getId(),$is_default);
            $classDto=[];

            $classDto['id']=$class->getId();
            $classDto['name']=$activity->getName();
            $classDto['description']=$activity->getDescription();
            $classDto['order']=$class->getOrder();
            $classDto['starts_at']=$roundClass->getStartsAt();
            $classDto['ends_at']=$roundClass->getEndsAt();
            $classDto['due_date']=$roundClass->getDueDate();

            $classDto['duration'] = $roundClass->getDuration();
            if($is_default)
                $classDto['activity_id'] = $class->getDefaultActivity()->getId();
            else
                $classDto['activity_id'] = $class->getActivity()->getId();
            $solvedTasksNum = $this->professionalRepository->getStudentSolvedTasksInClass($student, $campus_id,$activity_id,$round,$is_default, true);
            $campusTasksNum = $this->professionalRepository->getAllTasksInClass($campus_id,$activity_id,$student,$round,$is_default, true);
            if($campusTasksNum!==0)
                $classDto['progress'] = intval(($solvedTasksNum/$campusTasksNum)*100).'%';
            else
                $classDto['progress'] ='0%';


            $tasksDto=[];
            $htmlTasksDto=[];
            //TODO:these nested if-else needs refactoring
            foreach ($class_tasks as $class_task){
                $task=$class_task->getTask();
                $previous_progress=$this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity_id,$is_default);
                if($previous_progress!=null)
                    $passed=$previous_progress->getFirstSuccess()!==null?true:false;
                else
                    $passed = 0;

                if($task->getMissions()->first() != null) {
                    $mission = $task->getMissions()->first();

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getName(),
                        'image' => $mission->getIconUrl(),

                        'type' => 'mission',
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed'=>$passed

                    ]);
                }
                elseif($task->getMissionsHtml()->first()!=null) {
                    $mission = $task->getMissionsHtml()->first();
                    array_push($htmlTasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),

                        'type' => 'html_mission',
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed'=>$passed
                    ]);

                }
                elseif($task->getQuizzes()->first()!=null) {
                    $quiz = $task->getQuizzes()->first();

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $quiz->getTitle(),
                        'image' => $quiz->geticonURL(),

                        'type' => $quiz->getType()->getName(),
                        'quiz_id' => $quiz->getId(),
                        'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed'=>$passed
                    ]);
                }
                elseif($task->getMissionsCoding()->first()!=null){
                    $codingMission=$task->getMissionsCoding()->first();

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $codingMission->getTitle(),
                        'image' => $codingMission->getIconURL(),

                        'type' => 'coding_mission',
                        'mission_id' => $codingMission->getId(),
                        'order' => $codingMission->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed'=>$passed
                    ]);
                }
                elseif($task->getMissionsEditor()->first()!=null){
                    $editorMission=$task->getMissionsEditor()->first();

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $editorMission->getTitle(),
                        'image' => $editorMission->getIconURL(),
                        'mode' => $editorMission->getType(),
                        'type' => 'editor_mission',
                        'mission_id' => $editorMission->getId(),
                        'order' => $editorMission->getTask()->getActivityOrder($activity_id, $is_default),
                        'passed'=>$passed
                    ]);
                }

                $classDto['tasks']=$tasksDto;

            }

            $unordered_tasks=  $tasksDto;
            $classDto['tasks']=$this->sortClassTasks($unordered_tasks);

            $unordered_html_tasks=  $htmlTasksDto;
            $classDto['html_tasks']=$this->sortClassTasks($unordered_html_tasks);
            //uasort($ordered_tasks,array($this, 'tasks_cmp'));
            // $classDto['ordered_tasks']=$ordered_tasks;
            //order tasks by order attr

            //$classRoom = $roundClass->getRoom();

            $classRooms=$roundClass!==null?$roundClass->getRooms():[];
            $materials=$roundClass!==null?$roundClass->getMaterials():[];
            $classDto['recordedSessions']=null;
            $classDto['runningSession']=null;
            $classDto['materials']=null;
            foreach ( $materials as $material){
                $classDto['materials'] []= $material;
            }


            foreach($classRooms as $classRoom) {
                //GET RECORDING ROOMS
                $recordingParams = new GetRecordingsParameters();
                $recordingParams->setMeetingId($classRoom->id);
                $response = $bbb->getRecordings($recordingParams);
                $records = [];
//                $material = null;
//                if($classRoom->getFileLink() != null){
//                    $material['session_id'] = $classRoom->id;
//                    $material['session_name'] = $classRoom->name;
//                    $material['material_url'] = $classRoom->getFileLink();
//                }
//
                if ($response->getReturnCode() == 'SUCCESS') {
                    foreach ($response->getRecords()  as $recording) {
                        // process all recording
                        $record_['url'] = $recording->getPlaybackUrl();
                        $record_['session_id'] = $classRoom->id;
                        $record_['session_name'] = $classRoom->name;
                        $records []= $record_;
                    }
                }
                if(!empty($records))
                    $classDto['recordedSessions'] []= $records[0];
//                if ($material != null)
//                    $classDto['materials'] []= $material;
                //GET RUNNING ROOMS
                $isRunningParams = new IsMeetingRunningParameters($classRoom->id);
                $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
                if($isRunning) {
                    $room_['seesion_id'] = $classRoom->id;
                    $room_['session_name'] = $classRoom->name;
                    $classDto['runningSession'] = $room_;
                }
            }
            $classDtos[]=$classDto;

        }
        $unSortedClasses = $classDtos;

        usort($unSortedClasses,array($this, 'tasks_cmp'));
        return $unSortedClasses;
    }
    public function getStudentRoundClassesData($user_id,$round_id,$is_default){
        
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();

        $checkRoundInCampus=$this->checkRoundInCampus($campus_id,$round_id);
        if(!$checkRoundInCampus)
            throw new BadRequestException(trans("locale.round_is_not_in_campus"));

        $checkStudentInRound=$this->checkUserInRound($student->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));

        
        //Set Course first seen for learner
        $course_firstseen = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);

        if($course_firstseen->course_seen == null){
            $course_firstseen->course_seen = true;
            $this->professionalRepository->storeCampusUserStatus($course_firstseen);
        }

        $roundClasses = $this->professionalRepository->getRoundClasses($round_id);
        $bbb = new BigBlueButton();
        $classDtos=[];
        foreach ($roundClasses as $roundClass){

            $class=$roundClass->getCampusClass();

            if($is_default)
                $activity_id=$class->getDefaultActivity()->getId();
            else
                $activity_id=$class->getActivity()->getId();
            //$class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$class->getCampus()->getId(),$is_default);

            // get lesson type
            if($class->lessonType() == 0)
            {
                $lessonType="normal";
            }
            elseif($class->lessonType() == 1)
            {
                $lessonType="project";
            }
            elseif($class->lessonType() == 2)
            {
                $lessonType="welcome";
            }else
            {
                $lessonType=$class->lessonType();
            }


            $classDto=[];
            
            $classDto['id']=$class->getId();
           // $classDto['is_project'] = $class->isProject();
            $classDto['order']=$class->getOrder();
            $classDto['starts_at']=$roundClass->getStartsAt();
            $classDto['ends_at']=$roundClass->getEndsAt();
            $classDto['due_date']=$roundClass->getDueDate();
            $classDto['round_has_instructor']=$round->hasInstructor();
            $classDto['time_estimate'] = $class->getTotalTimeEstimates();
            $classDto['duration'] = $roundClass->getDuration();
            $classDto['moduleType'] = $lessonType;
             if($class->lessonType() == 1){
                $classDto['video_url'] = $class->getProject()->getVideoUrl();
            }

            if($is_default) {
                $classDto['activity_id'] = $class->getDefaultActivity()->getId();
                $classDto['description'] = $class->getDefaultActivity()->getDescription();
                $classDto['name']=$class->getDefaultActivity()->getName();
            }
            else {
                $classDto['activity_id'] = $class->getActivity()->getId();
                $classDto['description'] = $class->getActivity()->getDescription();
                $classDto['name']=$class->getActivity()->getName();
            }
            // $solvedTasksNum = $this->professionalRepository->getStudentSolvedTasksInClass($student->getId(), $campus_id,$activity_id,$is_default, true);
            $campusTasksNum = $this->professionalRepository->getAllTasksInClass($campus_id,$activity_id,$student,$round,$is_default, true);
            if($campusTasksNum!==0)
                // $classDto['progress'] = intval(($solvedTasksNum/$campusTasksNum)*100);
                $classDto['progress'] = $this->lessonCommonService->getLearnerClassProgress($student->getId(),$campus_id,$activity_id,$is_default)['progress'];
            else
                $classDto['progress'] =0;




            $classDtos[]=$classDto;

        }
        $unSortedClasses = $classDtos;

        usort($unSortedClasses,array($this, 'tasks_cmp'));

        $campusArticle = $round->getCampus()->getHelpCenterArticles()->first();
        if($campusArticle){
            $welcomLesson = ['article_id' => $campusArticle->getId(), 'article_title' => $campusArticle->getTitle()];
        }else{
            $welcomLesson = null;
        }
      //  array_unshift($unSortedClasses,$welcomLesson);
        
        $returned["classes"]= $unSortedClasses ;
        $returned["article"]= $welcomLesson ;
        //$returnTasks["tasks"]=$tasksDto;


        return $returned;
    }

    private function getLastseenString($lastSeenTime){
        $array=[];
        if($lastSeenTime!==null) {
            $currentDate=Carbon::now();
            $lastSeenTimeDate=Carbon::createFromTimestamp($lastSeenTime);
            $diffDateInterval=$currentDate->diff($lastSeenTimeDate);
            if($diffDateInterval->y>0){
                $array["number"]=($diffDateInterval->m>=6)?$diffDateInterval->y+1:$diffDateInterval->y;
                $array["label"]=($array["number"]>1)?"years":"year";

            }elseif($diffDateInterval->m>0){
                $array["number"]=($diffDateInterval->d>=15)?$diffDateInterval->m+1:$diffDateInterval->m;
                $array["label"]=($array["number"]>1)?"months":"month";

            }elseif($diffDateInterval->d>0&&$diffDateInterval->d<7){
                $array["number"]=($diffDateInterval->h>=12)?$diffDateInterval->d+1:$diffDateInterval->d;
                $array["label"]=($array["number"]>1)?"days":"day";

            }elseif($diffDateInterval->d>0&&$diffDateInterval->d>=7){
                $w=round($diffDateInterval->d/7);
                $array["number"]=$w;
                $array["label"]=($array["number"]>1)?"weeks":"week";

            }elseif($diffDateInterval->h>0){
                $array["number"]=($diffDateInterval->i>=30)?$diffDateInterval->h+1:$diffDateInterval->h;
                $array["label"]=($array["number"]>1)?"hours":"hour";

            }elseif($diffDateInterval->i>0){
                $array["number"]=($diffDateInterval->s>=30)?$diffDateInterval->i+1:$diffDateInterval->i;
                $array["label"]=($array["number"]>1)?"minutes":"minute";

            }elseif($diffDateInterval->s>0){
                $array["number"]=$diffDateInterval->s;
                $array["label"]=($array["number"]>1)?"seconds":"second";
            }else{
                $array["number"]="1";
                $array["label"]="second";
            }
            return $array["number"]." ".$array["label"];

        }else{
            return null;
        }
    }

    public function checkMissionsLock($returnTasks,$user,$campus_id,$activity_id,$is_default){
        $finalTasks = [];
        //Mission lock check
        // dd($returnTasks['tasks']);

        foreach($returnTasks['tasks'] as $order => $task){

            if($task['order'] == 1){
                $task['locked'] = false;    
            }else{
                if($user->isStudent()){
                    $previous_task = $returnTasks['tasks'][$order-1];

                    if($previous_task['last_seen'] == 'completed' || $previous_task['passed'] == true){
                        $task['locked'] = false;
                    }elseif($previous_task['last_seen'] == 'in_progress' && $previous_task['type'] == 'mini_project'){
                        //When Mini-Project submited
                        $projectTask = $this->journeyRepository->getTaskById($previous_task['id']);
                        $progress = $this->professionalRepository->getUserRoundTaskProgressActivity($projectTask,$user->id,$campus_id,$activity_id,$is_default);

                        if($progress != null && $progress->user_code != null){
                            $task['locked'] = false;
                        }else{
                            $task['locked'] = true;
                        }
                    }else{
                        $task['locked'] = true;
                    }
                }else{
                    $task['locked'] = false;
                }
            }
            array_push($finalTasks,$task);
        }
        return $finalTasks;
    }

    public function getStudentClassTasks($user_id,$round_id, $class_id,$gmt_difference, $is_default){
        
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        $checkStudentInRound=$this->checkUserInRound($user->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();

        $class = $this->professionalRepository->getCampusClassById($class_id);
        if($class == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        if($is_default)
            $activity = $class->getDefaultActivity();
        else
            $activity = $class->getActivity();

        $tasks = $activity->getTasks();
        
        //Check User Hide Blocks & ShowClasses Status
        $hideBlocksStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id)->hide_blocks == 1 ? true:false ;
        $showClassesStatus = $round->showClasses();
        

        $tasksDto = [];
        $htmlTasksDto = [];
        $boosterTasksDto = [];
        $returnTasks = [];
        foreach ($tasks as $task){
            // $userLastSeen=$this->professionalRepository->getUserLastSeenTask($user_id,$round->getCampus_id(),$activity->getId(),$task->getId(),$is_default);
            // $lastSeenTime=($userLastSeen!=null)?$userLastSeen->getLastSeen():null;

            $difficulty =  $this->professionalRepository->getTaskDifficulty($task->getId(),$activity->getId());
            $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());

//            if($task->getTaskNameAndType()['task_type'] == 'tutorial'){
//                dd($task);
//            }
            $taskTimeEstimate = $this->professionalRepository->getTaskTypeTimeEstimate($task->getTaskNameAndType()['task_type']);
            $previous_progress=$this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity->getId(),$is_default);

            $lastSeenTime=($previous_progress!=null)?$previous_progress:null;

            if($previous_progress!=null)
                $passed=$previous_progress->getFirstSuccess()!==null?true:false;
            else
                $passed = 0;
            if($task->getMissions()->first() != null && !$hideBlocksStatus ) {
                if($lastSeenTime!==null ){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                else{
                    if($passed)
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissions()->first();
                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image' => $mission->getIconUrl(),
                    'type' => 'mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mission_id' => $mission->getId(),
                    'passed'=>$passed,
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'last_seen'=>$lastSeenTime,
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,


                ];

                if($activityTask->booster_type != 1){
                    array_push($tasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    // get full mark
                    $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                    $taskDtoInst['xps']=$xps;
                    array_push($boosterTasksDto,$taskDtoInst );
                }

            }
            elseif($task->getMissionsHtml()->first() != null) {
                $mission = $task->getMissionsHtml()->first();
                $handoutMissionFlag = $mission->htmlMissionType() == 'handout';
                $type = ($handoutMissionFlag) ? 'handout' : 'html_mission';
                $missionSteps=$this->professionalRepository->getMissionStepsCount($mission->getId());
                $missionCurrentStep = $this->professionalRepository->getTaskCurrentStep($user_id, $task->getId(), $activity->getId(), $round->getCampus_id());
                $missionCurrentSection = $this->professionalRepository->getTaskCurrentStep($user_id, $task->getId(), $activity->getId(), $round->getCampus_id());

                $missionCurrentStep = $missionCurrentStep ? $missionCurrentStep->getCurrentStep() : 0;

                if($missionCurrentSection){
                    $currentSection=$this->checkIfSectionExist($missionCurrentSection,$mission->getId());
               
                }else{
                    $currentSection=0;
                }


                if($passed && $missionCurrentStep==0)
                    $missionCurrentStep=$missionSteps;

                if($mission->getWithSteps() == 1)
                $withSteps=true;
                else
                $withSteps=false;
                //HTML Mission and not a Mini-Project
                if( $mission->getIsMiniProject()==0 && $showClassesStatus) {
                    if($lastSeenTime!==null ){
                        if(!$passed)
                            $lastSeenTime="in_progress";
                        else
                            $lastSeenTime="completed";
                    }
                    else{
                        if($passed)
                            $lastSeenTime="completed";
                    }
                        

                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'type' => $type,
                    'mission_id' => $mission->getId(),
                    'passed' => $passed,
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'last_seen'=>$lastSeenTime,
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'time_estimate' => $taskTimeEstimate,
                    'mission_steps_count'=> $missionSteps,
                    'current_step' => $missionCurrentStep,
                    'moduleType' => $mission->getLessonType(),
                    'withSteps' => $withSteps,
                    'current_section' => $currentSection,
                    'sections_count' => count($mission->getSections()),
                    'locked'=>false,

                ];

                if($activityTask->booster_type != 1){
                    array_push($htmlTasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    // get full mark
                    $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                    $taskDtoInst['xps']=$xps;
                    array_push($boosterTasksDto,$taskDtoInst );
                }

                }
                elseif($mission->getIsMiniProject()){
                    if($lastSeenTime!==null){
                        if(!$passed)
                            $lastSeenTime="in_progress";
                        else
                            $lastSeenTime="completed";
                    }else{
                        if($passed)
                            $lastSeenTime="completed";
                    }

                    $taskDtoInst = [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'mini_project',
                        'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                        'mission_id' => $mission->getId(),
                        'passed' => $passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate,
                        'lessonType' => $mission->getLessonType(),
                        'with_steps' => $withSteps,
                        'locked'=>false,

                        //"test"=>$lastSeenTime=$this->getLastseenString($lastSeenTimeTest)
                        //'project_id' => $task->getProject()->first()->getProjectId()
                    ];

                    if($activityTask->booster_type != 1){
                        array_push($tasksDto,$taskDtoInst );
                    }elseif($activityTask->booster_type == 1){
                        // get full mark
                        $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                        $taskDtoInst['xps']=$xps;
                        array_push($boosterTasksDto,$taskDtoInst );
                    }

                }
            }
            elseif($task->getMissionCoding() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }else{
                    if($passed)
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionCoding();
                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'model_answer' => $mission->getModelAnswer(),
                    'type' => 'coding_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'code' => $mission->getCode(),
                    'mission_id' => $mission->getId(),
                    'passed'=>$passed,
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'last_seen'=>$lastSeenTime,
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,

                ];

                if($activityTask->booster_type != 1){
                    array_push($tasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    // get full mark
                    $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                    $taskDtoInst['xps']=$xps;
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            elseif($task->getMissionEditor() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }else{
                    if($passed)
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionEditor();
                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'model_answer' => $mission->getModelAnswer(),
                    'type' => 'editor_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mode' => $mission->getType(),
                    'html' => $mission->getHtml(),
                    'css' => $mission->getCss(),
                    'js' => $mission->getJs(),
                    'mission_id' => $mission->getId(),
                    'passed'=>$passed,
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'last_seen'=>$lastSeenTime,
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,

                ];

                if($activityTask->booster_type != 1){
                    array_push($tasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    // get full mark
                    $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                    $taskDtoInst['xps']=$xps;
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            elseif($task->getMissionAngular() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }else{
                    if($passed)
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionAngular();
                $missionAngularFilesDto=Mapper::MapEntityCollection(MissionAngularFilesDto::class, $mission->getFiles());

                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
                    'image' => $mission->getIconUrl(),
                    'type' => 'angular_mission',
                    'files' => $missionAngularFilesDto,
                    'mission_id' => $mission->getId(),
                    'passed'=>$passed,
                    'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'last_seen'=>$lastSeenTime,
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,

                ];


                if($activityTask->booster_type != 1){
                    array_push($tasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    // get full mark
                    $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                    $taskDtoInst['xps']=$xps;
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            elseif($task->getQuizzes()->first()!=null){
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }else{
                    if($passed)
                        $lastSeenTime="completed";
                }
                $quiz = $task->getQuizzes()->first();
                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $quiz->getTitle(),
                    'image' => $quiz->geticonURL(),
                    'type' => $quiz->getType()->getName(),
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'quiz_id' => $quiz->getId(),
                    'passed'=>$passed,
                    'order' => $quiz->getTask()->getActivityOrder($activity->getId(), $is_default),
                    'last_seen'=>$lastSeenTime,
                    'time_estimate' => $taskTimeEstimate,
                    'locked'=>false,
                    
                ];

                if($activityTask->booster_type != 1){
                    array_push($tasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    // get full mark
                    $xps=$this->lessonCommonService->getBoosterTaskFullMark( $user, $task,$activity,$round);
                    $taskDtoInst['xps']=$xps;
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
        }
        // if($round->hasInstructor()){
        //     $tasksDto=$this->sortClassTasks($tasksDto);
        //     $htmlTasksDto=$this->sortClassTasks($htmlTasksDto);
        //     $returnTasks["tasks"]=$tasksDto;
        //     $returnTasks["html_tasks"]=$htmlTasksDto;
        //     $returnTasks["boosters"] = count($boosterTasksDto) >0 ? $boosterTasksDto : null;
        // }
        // else{
            if($round->showClasses()==true)
                $tasksDto=$this->sortClassTasks(array_merge($tasksDto, $htmlTasksDto));
            $returnTasks["tasks"]=$tasksDto;
            
            $returnTasks["boosters"] = count($boosterTasksDto) >0 ? $boosterTasksDto : null;
        // }


        $roundClass=$this->professionalRepository->getCampusRoundClass($round_id,$class_id);

        $classRooms=$roundClass!==null?$roundClass->getRooms():[];
        $returnTasks['recordedSessions']=null;
        $returnTasks['runningSession']=null;
        $returnTasks['materials']=null;
        $bbb = new BigBlueButton();
        $materials=$roundClass!==null?$roundClass->getMaterials():[];
        foreach ( $materials as $material){
            $returnTasks['materials'] []= $material;
        }


//         foreach($classRooms as $classRoom) {
//             //GET RECORDING ROOMS
//             $recordingParams = new GetRecordingsParameters();
//             $recordingParams->setMeetingId($classRoom->id);
//             $response = $bbb->getRecordings($recordingParams);
//             $records = [];
// //            $material = null;
// //            if($classRoom->getFileLink() != null){
// //                $material['session_id'] = $classRoom->id;
// //                $material['session_name'] = $classRoom->name;
// //                $material['material_url'] = $classRoom->getFileLink();
// //            }
//             if ($response->getReturnCode() == 'SUCCESS') {
//                 foreach ($response->getRecords()  as $recording) {
//                     // process all recording
//                     $record_['url'] = $recording->getPlaybackUrl();
//                     $record_['session_id'] = $classRoom->id;
//                     $record_['session_name'] = $classRoom->name;
//                     $records []= $record_;
//                 }
//             }
//             if(!empty($records))
//                 $returnTasks['recordedSessions'] []= $records[0];
// //            if ($material != null){
// //                $returnTasks['materials'] []= $material;
// //            }

//             //GET RUNNING ROOMS
//             $isRunningParams = new IsMeetingRunningParameters($classRoom->id);
//             $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
//             if($isRunning) {
//                 $room_['session_id'] = $classRoom->id;
//                 $room_['session_name'] = $classRoom->name;
//                 $returnTasks['runningSession'] = $room_;
//             }
//         }
        

        $returnTasks['tasks'] = $this->checkMissionsLock($returnTasks,$user,$campus_id,$activity->id,$is_default);
        // return $finalTasks;
        return $returnTasks;

    }

    public function getTaskDto($taskArray)
    {

    }

    public function checkIfSectionExist($missionCurrentSection,$mission_id){

        $currentSection=$missionCurrentSection->getCurrentSection();
        if($currentSection > 0){
            $sectionId=$missionCurrentSection->getSectionId();
            $sectionExist=$this->professionalRepository->getMissionSection($mission_id,$sectionId)->first();
            if($sectionExist != null){
                $currentSection=$currentSection;
            }else{
                $missionSections=$this->professionalRepository->getMissionSectionsCount($mission_id,false)->toArray(); 
                if(!empty($missionSections)){
                    $newCurrentSection=$currentSection-1;
                    for($i=$newCurrentSection;$i>=0;$newCurrentSection--){
                        if($newCurrentSection == 0)
                        {
                            $currentSection=$newCurrentSection;
                            break;
                        }else{
                            $missionOrder=$this->professionalRepository->getSectionWithOrder($mission_id,$newCurrentSection);

                            if($missionOrder != null)
                            {
                                $currentSection= $missionOrder->getOrder();
                                $sectionId= $missionOrder->getId(); 

                                $missionCurrentSection->setCurrentSection($currentSection);
                                $missionCurrentSection->setSectionId($sectionId);
                                $this->professionalRepository->storeUserTaskCurrentStep($missionCurrentSection);

                                break;
                            }
                        }
                    } 
                }else{
                    $currentSection=0;
                }  
            }
        }else{
        $currentSection=0;
        }

        return $currentSection;
    }

    public function getClassTasks($user_id,$round_id, $activity_id, $is_default){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        $checkStudentInRound=$this->checkUserInRound($user->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

//        $campus_id=$round->getCampus()->getId();
//
//        $class = $this->professionalRepository->getCampusClassById($class_id);
//        if($class == null)
//            throw new BadRequestException(trans('locale.class_not_exist'));


        $campus_id=$round->getCampus()->getId();

        $class = $this->professionalRepository->getCampusClass($campus_id,$activity_id);

        if($is_default)
            $activity = $class->getDefaultActivity();
        else
            $activity = $class->getActivity();

        $tasks = $activity->getTasks();

        //Check User Hide Blocks & ShowClasses Status
        $campusUserStatusInst = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);
        if($campusUserStatusInst){
            $hideBlocksStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id)->hide_blocks == 1 ? true:false ;
        }else{
            $hideBlocksStatus = false;
        }
        $showClassesStatus = $round->showClasses();
        

        // $solvedTasksNum = $this->professionalRepository->getStudentSolvedTasksInClass($user->getId(), $campus_id,$activity_id,$is_default, true);
        $campusTasksNum = $this->professionalRepository->getAllTasksInClass($campus_id,$activity_id,$user,$round,$is_default, true);
        if($campusTasksNum!==0)
            // $progress = intval(($solvedTasksNum/$campusTasksNum)*100).'%';
            $progress = $this->lessonCommonService->getLearnerClassProgress($user->getId(),$campus_id,$activity_id,$is_default)['progress'].'%';
        else
            $progress ='0%';



        $tasksDto = [];
        //$htmlTasksDto = [];
        $returnTasks = [];
        $returnTasks["id"]=$class->getId();
        $returnTasks["name"]=$class->getName($is_default);
        $returnTasks["description"]=$class->getDescription($is_default);
        $returnTasks['progress']=$progress;
        foreach ($tasks as $task){
            // $userLastSeen=$this->professionalRepository->getUserLastSeenTask($user_id,$round->getCampus_id(),$activity->getId(),$task->getId(),$is_default);
            // $lastSeenTime=($userLastSeen!=null)?$userLastSeen->getLastSeen():null;

            $taskTimeEstimate = $this->professionalRepository->getTaskTypeTimeEstimate($task->getTaskNameAndType()['task_type']);
            $previous_progress=$this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity->getId(),$is_default);
            $lastSeenTime=($previous_progress!=null)?$previous_progress:null;

            $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
            if($previous_progress!=null)
                $passed=$previous_progress->getFirstSuccess()!==null?true:false;
            else
                $passed = 0;
            if($task->getMissions()->first() != null && !$hideBlocksStatus) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissions()->first();
                if($activityTask->booster_type != 1){
                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getName(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'mission',
                        'mission_id' => $mission->getId(),
                        'passed'=>$passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate
                    ]);

                }
            }
            elseif($task->getMissionsHtml()->first() != null) {
                $mission = $task->getMissionsHtml()->first();
                if( $mission->getIsMiniProject()==0 && $showClassesStatus) {
                    $handoutMissionFlag = $mission->htmlMissionType() == 'handout';
                    $type = ($handoutMissionFlag) ? 'handout' : 'html_mission';
                    if($lastSeenTime!==null){
                        if(!$passed)
                            $lastSeenTime="in_progress";
                        else
                            $lastSeenTime="completed";
                    }
                    if($activityTask->booster_type != 1){
                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $mission->getTitle(),
                            'image' => $mission->getIconUrl(),
                            'type' => $type,
                            'mission_id' => $mission->getId(),
                            'passed' => $passed,
                            'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                            'last_seen'=>$lastSeenTime,
                            'time_estimate' => $taskTimeEstimate,
                            'with_steps'=>$mission->getWithSteps()==1?true:false,
                        ]);
                    }
                }elseif($mission->getIsMiniProject()){
                    if($lastSeenTime!==null){
                        if(!$passed)
                            $lastSeenTime="in_progress";
                        else
                            $lastSeenTime="completed";
                    }
                    if($activityTask->booster_type != 1){
                            array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'mini_project',
                        'mission_id' => $mission->getId(),
                        'passed' => $passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate,
                        'with_steps'=>$mission->getWithSteps()==1?true:false,
                        //"test"=>$lastSeenTime=$this->getLastseenString($lastSeenTimeTest)
                        //'project_id' => $task->getProject()->first()->getProjectId()
                            ]);
                    }

                }
            }
            elseif($task->getMissionCoding() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionCoding();
                if($activityTask->booster_type != 1){
                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'model_answer' => $mission->getModelAnswer(),
                        'type' => 'coding_mission',
                        'code' => $mission->getCode(),
                        'mission_id' => $mission->getId(),
                        'passed'=>$passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate
                    ]);
                }
            }
            elseif($task->getMissionEditor() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionEditor();
                if($activityTask->booster_type != 1){
                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'model_answer' => $mission->getModelAnswer(),
                        'type' => 'editor_mission',
                        'mode' => $mission->getType(),
                        'html' => $mission->getHtml(),
                        'css' => $mission->getCss(),
                        'js' => $mission->getJs(),
                        'mission_id' => $mission->getId(),
                        'passed'=>$passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate
                    ]);
                 }   
            }
            elseif($task->getMissionAngular() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionAngular();
                $missionAngularFilesDto=Mapper::MapEntityCollection(MissionAngularFilesDto::class, $mission->getFiles());
                if($activityTask->booster_type != 1){
                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'angular_mission',
                        'files' => $missionAngularFilesDto,
                        'mission_id' => $mission->getId(),
                        'passed'=>$passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate
                    ]);
                 }   
            }
            elseif($task->getQuizzes()->first()!=null){
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                $quiz = $task->getQuizzes()->first();
                if($activityTask->booster_type != 1){
                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $quiz->getTitle(),
                        'image' => $quiz->geticonURL(),
                        'type' => $quiz->getType()->getName(),
                        'quiz_id' => $quiz->getId(),
                        'passed'=>$passed,
                        'order' => $quiz->getTask()->getActivityOrder($activity->getId(), $is_default),
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate
                    ]);
                }
            }
        }
        
        $tasksDto=$this->sortClassTasks($tasksDto);
        $returnTasks["tasks"]=$tasksDto;

        $returnTasks['tasks'] = $this->checkMissionsLock($returnTasks,$user,$campus_id,$activity->id,$is_default);

        return $returnTasks;

    }

    public function getWelcomeTasks($user_id,$round_id, $activity_id, $is_default){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        $checkStudentInRound=$this->checkUserInRound($user->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();

        $class = $this->professionalRepository->getCampusClass($campus_id,$activity_id);

        $campusUser = $this->professionalRepository->getCampusRoundUser($round->getId(),$user_id);
        if($campusUser->getHasSeen() == 1)
            $hasSeen=true;
        else
            $hasSeen=false;

        $welcomeModules=$class->getCampusWelcomeModules();
        if($welcomeModules==null)
            throw new BadRequestException(trans("locale.campus_welcome_module_not_exist"));

        if($is_default)
            $activity = $class->getDefaultActivity();
        else
            $activity = $class->getActivity();

        $tasks = $activity->getTasks();



        $tasksDto = [];
        $returnTasks = [];
        $returnTasks["classId"]=$class->getId();
        $returnTasks["className"]=$class->getName($is_default);
        $returnTasks["moduleId"]=$welcomeModules->getId();
            array_push($tasksDto, [
                'videoUrl' => $welcomeModules->getIntroductoryVideoUrl(),
                'image' => 'https://robogarden-professional.s3-us-west-2.amazonaws.com/Roadmap-Images/Welcome-Video.png',
                'type' => 'video',
                'watched' =>$hasSeen,
                'last_seen'=>'completed',
                'passed'=>true,
                'order'=>1,
            ]);
        // $returnTasks["orientationVideoUrl"]=$welcomeModules->getOrientationVideoUrl();
        // $returnTasks["introductoryText"]=$welcomeModules->getText();
        foreach ($tasks as $task){
            // $userLastSeen=$this->professionalRepository->getUserLastSeenTask($user_id,$round->getCampus_id(),$activity->getId(),$task->getId(),$is_default);
            // $lastSeenTime=($userLastSeen!=null)?$userLastSeen->getLastSeen():null;

            $taskTimeEstimate = $this->professionalRepository->getTaskTypeTimeEstimate($task->getTaskNameAndType()['task_type']);
            $previous_progress=$this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity->getId(),$is_default);
            $lastSeenTime=($previous_progress!=null)?$previous_progress:null;

            if($previous_progress!=null)
                $passed=$previous_progress->getFirstSuccess()!==null?true:false;
            else
                $passed = 0;
           if($task->getMissionsHtml()->first() != null) {
                if($lastSeenTime!==null){
                    if(!$passed)
                        $lastSeenTime="in_progress";
                    else
                        $lastSeenTime="completed";
                }
                else{
                    if($passed)
                        $lastSeenTime="completed";
                }
                $mission = $task->getMissionsHtml()->first();
                if( $mission->getIsMiniProject()==0) {
                    //$lastSeenTime=$this->getLastseenString($lastSeenTime);
                    $difficulty =  $this->professionalRepository->getTaskDifficulty($task->getId(),$activity->getId());
                   //return null in case of overview
                   if($mission->lesson_type == 'overview')
                        $difficulty = null;
                    
                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'html_mission',
                        'mission_id' => $mission->getId(),
                        'passed' => $passed,
                        'order' => $mission->getTask()->getActivityOrder($activity->getId(), $is_default)+1,
                        'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                        'lesson_type'=>$mission->lesson_type,
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $taskTimeEstimate
                    ]);
                }
            }
          
        }

        // $tasksDto=$this->sortClassTasks($tasksDto);
        
        $returnTasks["tasks"]=$tasksDto;
        $returnTasks['tasks'] = $this->checkMissionsLock($returnTasks,$user,$campus_id,$activity->id,$is_default);

        return $returnTasks;

    }

    public function tasks_cmp($task1,$task2){
        if ($task1['order'] === $task2['order']) {
            return 0;
        }
        if ($task1['order'] < $task2['order']) {
            return -1;
        }else{
            return 1;
        }
    }

    function sortClassTasks(array $arr) {
        $sorted = false;
        while (false === $sorted) {
            $sorted = true;
            for ($i = 0; $i < count($arr)-1; ++$i) {
                $current = $arr[$i];
                $next = $arr[$i+1];
                if ($next['order'] < $current['order']) {
                    $arr[$i] = $next;
                    $arr[$i+1] = $current;
                    $sorted = false;
                }
            }
        }
        return $arr;
    }

    public function getStudentEvaluationDetails($progress,$type,$questions=null,$blocks=null){
        
        $duration = $progress->task_duration;
        $trails = $progress->no_of_trials;
        $noOfQuestions = null;
        $confidence = null;
        $max_duration = null;

        if($type == 'quiz'){
            //Calculate max duration 
            $timeConst = $this->professionalRepository->getScoringConstant('defaultQuizQuestion_time')->value;
            $max_duration = $timeConst*count($questions);

            //Calcualte confidence
             $confident=0;
             $somewhat=0;
             $notConfident=0;   
             foreach($questions as $question){
                switch ($question['confidence']) {
                    case 1:
                        $confident++;
                        break;
                    case 0.5:
                        $somewhat++;
                        break;
                    case 0.25:
                        $notConfident++;
                        break;
                }
             }
             $confidence = ['confident'=>$confident,'somewhat'=>$somewhat,'not_confrident'=>$notConfident];

        }elseif($type == 'lesson'){
            $noOfQuestions = $questions;
        }

        $data = ['duration'=>$duration,'max_duration'=>$max_duration,'trials'=>$trails,'confidence'=>$confidence,'blocks'=>$blocks,'no_of_question'=>$noOfQuestions];
        return $data;
    }

    public function checkRoundInCampus($campus_id,$round_id){
        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); //TODO translations

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);

        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if($campus->getId()==$campus_round->getCampus()->getId())
            return true;
        return false;
    }

    public function checkUserInRound($student_id,$round_id){
        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $user_in_round = $this->professionalRepository->checkStudentInRound($student_id,$round_id);

        if(count($user_in_round)>0){
            return true;
        }else{
            return false;
        }
    }

    public function checkClassInRound($class_id,$round_id){
        $class = $this->professionalRepository->getCampusClassById($class_id);
        if($class == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $class_in_round = $this->professionalRepository->checkClassInRound($class_id,$round_id);
        if(count($class_in_round)>0){
            return true;
        }else{
            return false;
        }
    }

    public function checkActivityInCampus($activity_id,$campus_id,$is_default){
        $activity_in_campus = $this->professionalRepository->getCampusActivityTasks($activity_id,$campus_id,$is_default);
        if(count($activity_in_campus)>0){
            return true;
        }else{
            return false;
        }
    }

    public function checkTaskInActivity($campus_id,$activity_id,$task_id,$is_default){
        $activity_in_campus = $this->professionalRepository->checkTaskInActivity($campus_id,$activity_id,$task_id,$is_default);
        if(count($activity_in_campus)>0){
            return true;
        }else{
            return false;
        }
    }


    public function updateRoundActivityMissionProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$userCode,$is_default=true,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        if(!$isStudent)
            throw new BadRequestException(trans("locale.no_permission"));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));


        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

       
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();

        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));



        $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus->getId(), $activity_id, $is_default);

        if($task_progress == null){
            $task_progress = new CampusActivityProgress($task, $user, $campus,0,0,1,0,null,null,null,null,null,null);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
        }
        $no_trials = $task_progress->getNoTrials() + 1;

        $task_progress->setNoTrials($no_trials);
        $task_progress->setNoBlocks($noOfBlocks);
        $task_progress->setTaskDuration($timeTaken);
        $task_progress->setUserCode($userCode);

        $gainedScore = 0;
        $campusUserStatus=null;
        $userCodeBlocks = [];
        $modelAnswerBlocks = [];
        if($success_flag){
            $task_progress = $this->handleUserSuccessInTask($task_progress, $timeTaken, $noOfBlocks);

            $updatedScore = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference, LessonCommonService::BLOCKLY_MISSION);
            $task_progress = $updatedScore["campusActivityProgress"];
            $campusUserStatus = $updatedScore["campusUserStatus"];
            $gainedScore = $updatedScore["gainedXps"];

            //$this->professionalRepository->storeCampusUserStatus($campusUserStatus);

            //store user code if new
            if($xmlCode == null)
                throw new BadRequestException('XML code is missing');

            $missionModelAnswer = $mission->showModelAnswer();
            preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
            preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);

            if(count($userCodeBlocks)>0){
                $previouslyStoredCode = true;
                if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                    $previouslyStoredCode = false;
                }
                else{
                    $counter = 0;
                    foreach ($userCodeBlocks as $userCodeBlock){
                        if($userCodeBlock != $modelAnswerBlocks[$counter]){
                            $previouslyStoredCode = false;
                            break;
                        }
                        $counter ++;
                    }
                }

                if(!$previouslyStoredCode){
                    $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                    foreach ($userModelAnswers as $userModelAnswer){
                        $previouslyStoredCode = true;
                        if(count($userCodeBlocks) != count($userModelAnswer)){
                            $previouslyStoredCode = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userModelAnswer){
                                if($userCodeBlock != $userModelAnswer[$counter]){
                                    $previouslyStoredCode = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                    }
                    if(!$previouslyStoredCode){
                        $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                    }
                }
            }

        }

        $this->professionalRepository->storeRoundActivityProgress($task_progress);
        if($campusUserStatus!==null)
            $this->professionalRepository->storeCampusUserStatus($campusUserStatus);//weird bug

        //Save a student streak
        $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);

        //-----SEND NOTIFICATIONS-------
        $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
        // $this->sendStudentStuckNotification($account, $campus, $round_id, $activity_id);
        $this->sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task->getId());

        $this->accountRepository->commitDatabaseTransaction();
        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $blocksStats = ['noOfBlocks'=>count($userCodeBlocks),'maxNoOfBlocks'=>2*count($modelAnswerBlocks)];
        $evalDetails = $this->getStudentEvaluationDetails($task_progress,'blocks',null,$blocksStats);

        $ok=false;
        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        //lti_synched
        $missionDto= Mapper::MapClass(MissionDto::class,$mission);
        $missionDto->lti_synched=$ok;
        $missionDto->gained_score=$gainedScore;
        $firstPass = $task_progress->getFirstPass();
        return ["passed" => $success_flag,"gained_score"=>$gainedScore,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"eval_details"=>$evalDetails,"first_pass"=>$firstPass];
//        return $missionDto;
    }

    public function updateRoundActivityHTMLMissionProgress($mission_id,$user_id,$round_id,$activity_id,$userCode=null,$is_default = true,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        if(!$isStudent)
            throw new BadRequestException(trans("locale.no_permission"));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        $previous_missionprogress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);
        if($previous_missionprogress == null)
        {
            $score=100;
            $evaluation=$score;

            try{
                $task_progress = new CampusActivityProgress($task, $user, $campus, $score, 1, 0,$evaluation,1,null,null,null,null,$userCode);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->professionalRepository->storeRoundActivityProgress($task_progress);
                //Save a student streak
                $gainedScore = 0;
                $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
                //Save campus user status
                //  $this->professionalRepository->storeCampusUserStatus($user_id,$campus->id,$gainedScore['xp_gained']);
                 $this->lessonCommonService->updateCampusUserStatus($user_id,$campus->id,$gainedScore['xp_gained'],0);


            }catch(QueryException $ex){}

        }
        else {
            $score=100;
            $evaluation=$score;
            $no_of_trials=$previous_missionprogress->getNoTrials()+1;

            $this->professionalRepository->deleteRoundActivityProgress($previous_missionprogress);

            try{
                $task_progress = new CampusActivityProgress($task, $user, $campus, $score,$no_of_trials, 0,$evaluation,1,null,null,null,null,$userCode);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->professionalRepository->storeRoundActivityProgress($task_progress);
            }catch(QueryException $ex){}
        }

       
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionHtmlDto::class,$mission);
    }
    public function updateRoundActivityMissionHandoutProgress($mission_id,$user_id,$round_id,$activity_id,$is_default = true,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        if(!$isStudent)
            throw new BadRequestException(trans("locale.no_permission"));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        $previous_missionprogress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);
        if($previous_missionprogress == null)
        {

            try{
                $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 1, 1,0,1,null,null,null,null,null);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                
                $updatedScore = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference, GradingCommonService::HANDOUT_MISSION);
                $task_progress=$updatedScore["campusActivityProgress"];
                $gainedScore = $updatedScore["gainedXps"];
        
                //Save a student streak
                $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
                $this->professionalRepository->storeRoundActivityProgress($task_progress);
                
            }catch(QueryException $ex){}

        }
        else {
            $no_of_trials=$previous_missionprogress->getNoTrials()+1;

            $this->professionalRepository->deleteRoundActivityProgress($previous_missionprogress);

            try{
                $task_progress = new CampusActivityProgress($task, $user, $campus, 0,$no_of_trials, 0,0,1,null,null,null,null,null);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);

                $updatedScore = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference, GradingCommonService::HANDOUT_MISSION);
                $task_progress=$updatedScore["campusActivityProgress"];
                $gainedScore = $updatedScore["gainedXps"];
        
                //Save a student streak
                $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
                $this->professionalRepository->storeRoundActivityProgress($task_progress);
            }catch(QueryException $ex){}
        }

       
        $this->accountRepository->commitDatabaseTransaction();
        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $eval_details = ['duration'=>null,'max_duration'=>null,'trials'=>null,'confidence'=>null,'blocks'=>null,'no_of_question'=>null];
        $firstPass = $task_progress->getFirstPass();
        return ["passed" => 1, "gained_score"=>$gainedScore,"lti_synched"=>$ok,"taskFullMark"=>$gainedScore,"score"=>$campusScoreData,"failedToPass"=>[],'eval_details'=>$eval_details,"first_pass"=>$firstPass];
        // return Mapper::MapClass(MissionHtmlDto::class,$mission);
    }

    public function updateRoundActivityMiniProjectProgress($mission_id,$user_id,$round_id,$activity_id,$project_url,$is_default,$duration,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();
        
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));


        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        if($mission->getIsMiniProject() =='0')
            throw new BadRequestException(trans('locale.mission_not_mini_project'));
            
        $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity_id);

        $task = $mission->getTask();
        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        $gainedScore = 0;
        $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);

        //Save a student streak

        if($task_progress == null)
        {
            $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0,0,0,null,null,null,null,null,null);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);

        }
        $no_trials = $task_progress->getNoTrials() + 1;
        $task_progress->setNoTrials($no_trials);

        if(isset($duration) && !empty($duration))
            $task_progress->setTaskDuration($duration);

        $prev_project_url = $task_progress->getUserCode();
        if($prev_project_url != $project_url){
            $task_progress->setIsEvaluated(0);
            $task_progress->setUserCode(trim($project_url));
            $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
        }

        $this->professionalRepository->storeRoundActivityProgress($task_progress);

        // |Capstone Project| Notifiy Admin 
        if($class->lessonType() == 1){
                $this->sendTaskSubmissionNotification($user, $campus, $round_id, $activity_id,$task->id,$task_progress->id,true);
        }else{
            $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
            $this->sendTaskSubmissionNotification($user, $campus, $round_id, $activity_id,$task->id,$task_progress->id,false);
        }

        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);

        $this->accountRepository->commitDatabaseTransaction();

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);

        $ok=false;
        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        //lti_synched

        $firstPass = $task_progress->getFirstPass();
        return ["passed" => 0, "gained_score"=>$gainedScore, "lti_synched"=>$ok, "taskFullMark"=>$fullMark, "score"=>$campusScoreData, "first_pass"=>$firstPass];
    }
    private function ltiSynchBack(User $user,CampusRound $round){
        $user_lti_id=$user->getUserLTIId();
        $resource_link_id=$round->getLTIResourceLinkId();
        $campusUserStatus=$this->professionalRepository->getCampusUserStatus($round->getCampus_id(),$user->id);
        //dd($campusUserStatus);
        $ok=false;
        if($user_lti_id!==null && $resource_link_id!=null && $user->getRoles()->first()->getName()=='student'){
            $consumer=$this->professionalRepository->getLTIConsumerByAccountId($user->getAccount()->getId());
            $consumer_key=$consumer->consumer_key256;
            //$resource_link_id="657145911";

            $lti=new LTI();
            $db_connector=$lti->getDataConnector();

            $consumer = new ToolProvider\ToolConsumer($consumer_key, $db_connector);
            $resource_link = ToolProvider\ResourceLink::fromConsumer($consumer, $resource_link_id);

            $user_lti = ToolProvider\User::fromResourceLink($resource_link, $user_lti_id);
            $outcome = new ToolProvider\Outcome();
            $score="before_init";
            if ($resource_link->doOutcomesService(ToolProvider\ResourceLink::EXT_READ, $outcome, $user_lti)) {
                $score = $outcome->getValue();
            }
            
            $score_=$campusUserStatus->getXps()/$round->getCampus()->getXps();
            $outcome = new Outcome($score_);
            $ok = $resource_link->doOutcomesService(ResourceLink::EXT_WRITE, $outcome, $user_lti);
            if($ok==true)
                return "synched with score ".$score_;
            return $ok;
        }
        return $ok;
    }
    public function submitRoundActivityMcqMission($data,$selected_ids,$use_model, $mission_id, $user_id, $round_id, $activity_id,$noOfBlocks,$timeTaken,$practice,$is_default,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();
        
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $campus=$campus_round->getCampus();

        $checkActivityInCamp = $this->checkActivityInCampus($activity_id, $campus->getId(), $is_default);
        if (!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if ($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if ($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $passed = 0;
        $mark_counter = 0;
        $selected_ids_counter = 0;
        $questions_answers = [];

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $task = $mission->getTask();
        $checkActivityInCamp = $this->checkTaskInActivity($campus->getId(), $activity_id, $task->getId(), $is_default);
        if (!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getMcqQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getMcqChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            if($use_model && $use_model != "false")
                $choice_correct = $this->journeyRepository->getMcqCorrectChooseChoice($question['question_id']);
            else
                $choice_correct = $this->journeyRepository->getMcqChoiceById($selected_ids[$selected_ids_counter]);
            $question_answer = ["question_id" => $question['question_id'],
                "user_choice" => $choice_instance->choose,
                "choice_correct" => $choice_correct->choose,
                "eval" => false,
                "explanation"=>$choice_instance->getCorrectionMsg()];

            if($choice_correct->id == $question['choice']) {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1;
            }
            $questions_answers[] = $question_answer;
            $selected_ids_counter++;
        }

        if($mark_counter >= (sizeof($data)/2)) {
            $passed = 1;
        }

        $ok=false;
        $isStudent = $user->isStudent();
        if(!$isStudent)
        return ["passed" => $passed, "questions_answers" => $questions_answers,"lti_synched"=>$ok];

        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }

        $gainedScore = 0;
        if(!$practice) {
            $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($mission->getTask(), $user_id, $campus->id, $activity_id, $is_default);
            if ($task_progress == null){
                $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0, 1, 0, null, null, null, null, null);
                if ($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
            }
            $no_trials = $task_progress->getNoTrials() + 1;

            $task_progress->setNoTrials($no_trials);
            $task_progress->setNoBlocks($noOfBlocks);
            $task_progress->setTaskDuration($timeTaken);
            $campusUserStatus=null;
            if($passed==1){
                $task_progress = $this->handleUserSuccessInTask($task_progress, $timeTaken, $noOfBlocks);

                $updatedScore = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference, LessonCommonService::BLOCKLY_MCQ_MISSION);
                $task_progress=$updatedScore["campusActivityProgress"];
                $campusUserStatus=$updatedScore["campusUserStatus"];
                $gainedScore = $updatedScore["gainedXps"];

                //$this->professionalRepository->storeCampusUserStatus($campusUserStatus);

                //Save a student streak
                $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);

            }

            $this->professionalRepository->storeRoundActivityProgress($task_progress);
            if($campusUserStatus!==null)
                $this->professionalRepository->storeCampusUserStatus($campusUserStatus);

            //-----SEND NOTIFICATIONS-------
//            $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
//            $this->sendStudentStuckNotification($account, $campus, $round_id, $activity_id);

        }
        $this->accountRepository->commitDatabaseTransaction();
        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $evalDetails = $this->getStudentEvaluationDetails($task_progress,'blocks',null,$noOfBlocks);
        $firstPass = $task_progress->getFirstPass();
        
        return ["passed" => $passed, "questions_answers" => $questions_answers,"gained_score"=>$gainedScore,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"eval_details"=>$evalDetails,"first_pass"=>$firstPass];
    }

    //Save MCQ Quiz Answers
    //This only works for first time 
    public function saveMcqQuizAnswers($task_progress,$data){
         //Saving Student Answers
        foreach ($data as $question) {
            //Create new Campus Activity Question for each question
            $mcq_question_instance = $this->journeyRepository->getQuestionMcqById($question['question_id']);
            $question_instance=$mcq_question_instance->getQuestion();
            $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($task_progress,$question_instance);
       
                if($campusActivityQuestion==null){
                    $campusActivityQuestion=new CampusActivityQuestion($task_progress,$question_instance);
                    $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
                }
                else{
                    $mcqQuestionAnswer=$this->professionalRepository->getOldMcqQuestionAnswer($campusActivityQuestion);

                    //Delete old MCQ Answer record
                    foreach ($mcqQuestionAnswer as $old_answer) {
                        $this->professionalRepository->removeOldMcqQuestionAnswer($old_answer);
                    }
                }

                //Saveing Answers
                foreach($question['choice'] as $choice){
                    $choice_instance = $this->journeyRepository->getChoiceById($choice);
                    $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice_instance,$question['confidence']);
                    $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
                }   
            }         
    }
    public function saveMcqQuizModelAnswers($task_progress,$data){
        //Saving Student Answers
        $confidence=1;
        foreach ($data as $mcq_question_instance) {
            //Create new Campus Activity Question for each question
            
            $question_instance=$mcq_question_instance->getQuestion();
            $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($task_progress,$question_instance);
       
                if($campusActivityQuestion==null){
                    $campusActivityQuestion=new CampusActivityQuestion($task_progress,$question_instance);
                    $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
                }
                else{
                    $mcqQuestionAnswer=$this->professionalRepository->getOldMcqQuestionAnswer($campusActivityQuestion);

                    //Delete old MCQ Answer record
                    foreach ($mcqQuestionAnswer as $old_answer) {
                        $this->professionalRepository->removeOldMcqQuestionAnswer($old_answer);
                    }
                }

                //Saveing Answers
                foreach($mcq_question_instance->getChoices() as $choice_instance){
                    $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice_instance,$confidence);
                    $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
                }   
            }         
    }

    public function submitRoundActivityQuiz($data, $quiz_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference, $is_default=true){
        $this->accountRepository->beginDatabaseTransaction();
    
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        
        $passed = 0;
        $mark_counter = 0;
        $questions_answers = [];
        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        $task = $quiz->getTask();
        $checkTaskInActivityInCampus=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkTaskInActivityInCampus)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        if(count($data)!= count($quiz->getQuestionTasks()))
            throw new BadRequestException(trans('locale.all_questions_should_be_answered'));

        $quizCorrectnessAndConfidencePercentage=0;
        $passed_flag = 1;

        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getQuestionMcqById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $confidence = $this->journeyRepository->getConfidenceByValue($question['confidence']);
            if($confidence == null)
                throw new BadRequestException(trans('locale.confidence_not_exist'));
            $confidenceName=$confidence->getName();

            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));
            //Get User All Choices
            $userChoices =[];
            $userChoicesIds =[];
            $all_explanations =[];
            $correctChoices = [];
            $correctChoicesIds = [];
            $allChoices = [];
            foreach($question['choice'] as $choice){
                $choice_instance = $this->journeyRepository->getChoiceById($choice);

                if($choice == null)
                    throw new BadRequestException(trans('locale.choice_not_exist'));
                
                array_push($userChoices,$choice_instance->getText());
                array_push($userChoicesIds,$choice_instance->getId());
            }    

            //Get All Choices and check what user has choosen
            foreach($question_instance->getChoices() as $choice){
                //$choice_instance = $this->journeyRepository->getChoiceById($choice->id);
                if(in_array($choice->getId(),$userChoicesIds)){
                    $allChoices [] = [$choice->getText(),'selected'=>true];
                }else{
                    $allChoices [] = [$choice->getText(),'selected'=>false];
                }
            }
                
            
            //Get all explanations for choices
            $questionChoices = $this->journeyRepository->getChoicesforMcqQuestionById($question['question_id']);
            foreach($questionChoices as $choice){
                array_push($all_explanations,$choice->getExplanation());
            }
        
            
            $choices_correct = $this->journeyRepository->getChoiceModelforMcqQuestion($question['question_id']);
            
            //Add all correct chocies
            foreach($choices_correct as $choice){
                array_push($correctChoices,$choice->getText());
                array_push($correctChoicesIds,$choice->getId());
            }
            // dd(array_diff($userChoicesIds,$correctChoicesIds),'Correct',$correctChoicesIds,'User Choices',$userChoicesIds);
            // $eval = empty(array_diff($userChoicesIds,$correctChoicesIds)) ? true : false; 
            sort($userChoicesIds);
            sort($correctChoicesIds);
            $eval = ($userChoicesIds == $correctChoicesIds ) ? true : false;
            $passed_flag = ($userChoicesIds != $correctChoicesIds ) ? 0 : $passed_flag;
            $questionCorrectness=$eval?1:0;
            $quizCorrectnessAndConfidencePercentage+=$question['confidence']*$questionCorrectness;
            if($eval){
               $mark_counter +=1;
            }

            $question_answer = ["question_id" => $question['question_id'],
                "question_body" => $question_instance->getBody(),
                "user_choice" => $userChoices,
                "choices"   => $allChoices,
                "user_choice_explanation" => $choice_instance->getExplanation(),
                "choice_correct" => $correctChoices,
                "confidence" => $confidenceName,
                "all_explanations" =>$all_explanations,
                "selectMany"=>$question_instance->getSelectMany(),
                "eval" => $eval];

            $questions_answers[] = $question_answer;
        }
        $quizCorrectnessAndConfidencePercentage=$quizCorrectnessAndConfidencePercentage/count($quiz->getQuestionTasks());
        $scoreData['quizCorrectnessAndConfidencePercentage']=$quizCorrectnessAndConfidencePercentage;
        $scoreData['passed']=$passed_flag;
        $scoreData['duration']=$duration;
        $scoreData['quiz']=$quiz;
        
        if($mark_counter >= 1)
            $passed = 1;

        $isStudent = $user->isStudent();
        
        $ok=false;

        if(!$isStudent)
            return ["passed" => $passed, "questions_answers" => $questions_answers,"lti_synched"=>$ok];

        
        $quiz_progress_object = $this->professionalRepository->getUserRoundTaskProgressActivity($quiz->getTask(), $user_id, $campus->getId(), $activity_id, $is_default);
        $oldScore=0;
        if($quiz_progress_object==null){
            $quiz_progress_object = new CampusActivityProgress($task,$user,$campus,0,1,1,0,NULL,$duration);//score & evaluation is 0(actual value will be calculated) //trials & firstSucess=NULL  //isEvaluated=1
            $quiz_progress_object->setDefaultActivity($activity);
            $quiz_progress_object->setBestTaskDuration($duration);
            
        }else{
            $quiz_progress_object->setNoTrials($quiz_progress_object->getNoTrials()+1);
            $quiz_progress_object->setTaskDuration($duration);
            $oldScore=$quiz_progress_object->getEvaluation();

            if($duration<$quiz_progress_object->getBestTaskDuration())
                $quiz_progress_object->setBestTaskDuration($duration);
        }

        $returnedData=$this->gradingCommonService->updateScore($quiz_progress_object, $campus_round,$gmt_difference, LessonCommonService::QUIZ_MISSION,$scoreData);
        $quiz_progress_object=$returnedData["campusActivityProgress"];
        $campusUserStatus=$returnedData["campusUserStatus"];
        $fullMark=$returnedData["fullMark"];
        $campusScoreData=$returnedData["scoreData"];
        $gainedXps=$returnedData["gainedXps"];
        $currentXps=$returnedData["currentXps"];
        
        if($fullMark > 0){
            $passed=$currentXps>0?1:0;
        }
        //Save a student streak
        $gainedScore=$quiz_progress_object->getEvaluation();
        $this->updateStudentStreaks($user,$gainedXps-$oldScore,$campus->id,$gmt_difference);
        //Save campus user status
        $this->saveMcqQuizAnswers($quiz_progress_object,$data);
        $this->professionalRepository->storeRoundActivityProgress($quiz_progress_object);
        $this->professionalRepository->storeCampusUserStatus($campusUserStatus);

        //-----SEND NOTIFICATIONS-------
        //$this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
        $this->sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task->getId());
        $this->accountRepository->commitDatabaseTransaction();

        $evalDetails = $this->getStudentEvaluationDetails($quiz_progress_object,'quiz',$data);
        $firstPass = $quiz_progress_object->getFirstPass();
        
        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        return ["passed" => $passed, "questions_answers" => $questions_answers,"gained_score"=>$gainedXps,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"eval_details"=>$evalDetails,"first_pass"=>$firstPass];
    }

    public function submitRoundActivityMiniProjectQuiz($data, $mission_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference, $is_default=true){
        $this->accountRepository->beginDatabaseTransaction();
        
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist"));
        
        $task = $mission->getTask();
        $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);

        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }

        $duration=trim($duration);

        $gainedScore = 0;
        $passed = 0;
        $questions_answers = [];
        $mark_counter = 0;

        $quiz = $mission->getQuiz();
        if ($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getQuestionMcqById($question['question_id']);
            if ($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
            if ($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            $choice_correct = $this->journeyRepository->getChoiceModelforMcqQuestion($question['question_id']);
            $question_answer = [
                "question_id" => $question['question_id'],
                "question_body" => $question_instance->getBody(),
                "user_choice" => $choice_instance->getText(),
                "user_choice_explanation" => $choice_instance->getExplanation(),
                "choice_correct" => $choice_correct->first()->getText(),
                "eval" => false
            ];
            // TODO::Multiple choices corrections
            foreach ($question['choice'] as $choice) {
                if ($choice_correct->first()->id == $choice) {
                    $question_answer['eval'] = true;
                    $mark_counter = $mark_counter + 1;
                }
            }
            $questions_answers[] = $question_answer;
        }

        if ($mark_counter >= (sizeof($data) / 2))
            $passed = 1;

        if(!$isStudent)
            return ["passed" => $passed, "questions_answers" => $questions_answers];

        if($task_progress == null)
        {
            $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0,0,0, null,null,null,null,null,null);

            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
        }
        $no_trials = $task_progress->getNoTrials() + 1;
        $task_progress->setNoTrials($no_trials);

        if(isset($duration) && !empty($duration))
            $task_progress->setTaskDuration($duration);

        $campusUserStatus=null;
        if($passed == 1){
            $task_progress = $this->handleUserSuccessInTask($task_progress, $duration);
            $updateScoreData = [
                'correctAnsCount' => $mark_counter,
                'questionsCount' => sizeof($data)
            ];
            $updateEvaluation = !$task_progress->getIsEvaluated();
            $returnedData = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference,LessonCommonService::EDITOR_MISSION, $updateScoreData, $updateEvaluation);

            $task_progress=$returnedData["campusActivityProgress"];
            $campusUserStatus=$returnedData["campusUserStatus"];
            //$this->professionalRepository->storeCampusUserStatus($campusUserStatus);
            //Save a student streak
            $gainedScore = $returnedData["gainedXps"];
            $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
        }

        $this->professionalRepository->storeRoundActivityProgress($task_progress);
        if($campusUserStatus)
            $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
        


        //-----SEND NOTIFICATIONS-------
        $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
        // $this->sendStudentStuckNotification($account, $campus, $round_id, $activity_id);
        $this->sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task->getId());

        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);

        $this->accountRepository->commitDatabaseTransaction();

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $evalDetails = $this->getStudentEvaluationDetails($task_progress,'mini_project');
        $firstPass = $task_progress->getFirstPass();

        $ok=false;
        if($round_id!=null && $data!==null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        //lti_synched

        return ["passed" => $passed, "questions_answers" => $questions_answers,"gained_score"=>$gainedScore,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"eval_details"=>$evalDetails, "correct_answers"=>$mark_counter,"first_pass"=>$firstPass];
    }

    private function checkAndGetSchoolUserData($user_id,$round_id,$acttivity_id,$is_default=true){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $userData["user"]=$user;
        $userData["campus_round"]=$campus_round;
        $userData["activity"]=$activity;

        return $userData;
    }

    public function _submitRoundActivityQuiz($data, $quiz_id, $user_id, $round_id, $activity_id,$duration,$gmt_difference, $is_default=true){
        
        $this->accountRepository->beginDatabaseTransaction();
    
        $userData=$this->checkAndGetSchoolUserData($user_id,$round_id,$activity_id,$is_default=true);

        $user=$userData["user"];
        $campus_round=$userData["campus_round"];
        $activity=$userData["activity"];

        $campus=$campus_round->getCampus();

        
        $passed = 0;
        $mark_counter = 0;
        $questions_answers = [];
        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        $task = $quiz->getTask();
        $checkTaskInActivityInCampus=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkTaskInActivityInCampus)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        if(count($data)!= count($quiz->getQuestionTasks()))
            throw new BadRequestException(trans('locale.all_questions_should_be_answered'));

        $quizCorrectnessAndConfidencePercentage=0;

        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getQuestionMcqById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $confidence = $this->journeyRepository->getConfidenceByValue($question['confidence']);
            if($confidence == null)
                throw new BadRequestException(trans('locale.confidence_not_exist'));
            $confidenceName=$confidence->getName();

            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));
            //Get User All Choices
            $userChoices =[];
            $userChoicesIds =[];
            $all_explanations =[];
            $correctChoices = [];
            $correctChoicesIds = [];
            $allChoices = [];
            foreach($question['choice'] as $choice){
                $choice_instance = $this->journeyRepository->getChoiceById($choice);

                if($choice == null)
                    throw new BadRequestException(trans('locale.choice_not_exist'));
                
                array_push($userChoices,$choice_instance->getText());
                array_push($userChoicesIds,$choice_instance->getId());
            }    

            //Get All Choices and check what user has choosen
            foreach($question_instance->getChoices() as $choice){
                //$choice_instance = $this->journeyRepository->getChoiceById($choice->id);
                if(in_array($choice->getId(),$userChoicesIds)){
                    $allChoices [] = [$choice->getText(),'selected'=>true];
                }else{
                    $allChoices [] = [$choice->getText(),'selected'=>false];
                }
            }
                
            
            //Get all explanations for choices
            $questionChoices = $this->journeyRepository->getChoicesforMcqQuestionById($question['question_id']);
            foreach($questionChoices as $choice){
                array_push($all_explanations,$choice->getExplanation());
            }
        
            
            $choices_correct = $this->journeyRepository->getChoiceModelforMcqQuestion($question['question_id']);
            
            //Add all correct chocies
            foreach($choices_correct as $choice)
                array_push($correctChoices,$choice->getText());
                array_push($correctChoicesIds,$choice->getId());

            $eval = empty(array_diff($correctChoicesIds,$userChoicesIds)) ? true : false; 
            $questionCorrectness=$eval?1:0;
            $quizCorrectnessAndConfidencePercentage+=$question['confidence']*$questionCorrectness;
            if($eval){
               $mark_counter +=1;
            }

            $question_answer = ["question_id" => $question['question_id'],
                "question_body" => $question_instance->getBody(),
                "user_choice" => $userChoices,
                "choices"   => $allChoices,
                "user_choice_explanation" => $choice_instance->getExplanation(),
                "choice_correct" => $correctChoices,
                "confidence" => $confidenceName,
                "all_explanations" =>$all_explanations,
                "selectMany"=>$question_instance->getSelectMany(),
                "eval" => $eval];

            $questions_answers[] = $question_answer;
        }
        
        $quizCorrectnessAndConfidencePercentage=$quizCorrectnessAndConfidencePercentage/count($quiz->getQuestionTasks());
        $scoreData['quizCorrectnessAndConfidencePercentage']=$quizCorrectnessAndConfidencePercentage;
        $scoreData['duration']=$duration;
        $scoreData['quiz']=$quiz;

        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $isStudent = $user->isStudent();
        
        $ok=false;

        if(!$isStudent)
            return ["passed" => $passed, "questions_answers" => $questions_answers,"lti_synched"=>$ok];

        
        $quiz_progress_object = $this->professionalRepository->getUserRoundTaskProgressActivity($quiz->getTask(), $user_id, $campus->getId(), $activity_id, $is_default);
        $oldScore=0;
        if($quiz_progress_object==null){
            $quiz_progress_object = new CampusActivityProgress($task,$user,$campus,0,1,1,0,NULL,$duration);//score & evaluation is 0(actual value will be calculated) //trials & firstSucess=NULL  //isEvaluated=1
            $quiz_progress_object->setDefaultActivity($activity);
            $quiz_progress_object->setBestTaskDuration($duration);
            
        }else{
            $quiz_progress_object->setNoTrials($quiz_progress_object->getNoTrials()+1);
            $quiz_progress_object->setTaskDuration($duration);
            $oldScore=$quiz_progress_object->getEvaluation();

            if($duration<$quiz_progress_object->getBestTaskDuration())
                $quiz_progress_object->setBestTaskDuration($duration);
        }

        $returnedData=$this->gradingCommonService->updateScore($quiz_progress_object, $campus_round,$gmt_difference, LessonCommonService::QUIZ_MISSION,$scoreData);
        $quiz_progress_object=$returnedData["campusActivityProgress"];
        $campusUserStatus=$returnedData["campusUserStatus"];
        $fullMark=$returnedData["fullMark"];
        $campusScoreData=$returnedData["scoreData"];
        $gainedXps=$returnedData["gainedXps"];
        $currentXps=$returnedData["currentXps"];

        if($currentXps>0)
            $passed=1;
        else{
            if($fullMark==0)
                $passed=1;
            else
                $passed=0;
        }

        //Save a student streak
        $gainedScore=$quiz_progress_object->getEvaluation();
        $this->updateStudentStreaks($user,$gainedXps-$oldScore,$campus->id,$gmt_difference);
        //Save campus user status
        $this->saveMcqQuizAnswers($quiz_progress_object,$data);
        $this->professionalRepository->storeRoundActivityProgress($quiz_progress_object);
        $this->professionalRepository->storeCampusUserStatus($campusUserStatus);

        //-----SEND NOTIFICATIONS-------
        //$this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
        $this->sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task->getId());
        $this->accountRepository->commitDatabaseTransaction();

        
        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        return ["passed" => $passed, "questions_answers" => $questions_answers,"gained_score"=>$gainedXps,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData];
    }

    public function updateRoundActivityMissionCodingProgress($mission_id,$user_id,$round_id,$activity_id,$success_flag,$timeTaken=null,$userCode=null,$is_default=true,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        if(!$isStudent)
            throw new BadRequestException(trans("locale.no_permission"));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getCodingMissionById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();

        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);
        if($task_progress == null){
            $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0,0,0, null,null,null,null,null,null);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);

        }
        $no_trials = $task_progress->getNoTrials() + 1;
        $task_progress->setNoTrials($no_trials);
        $task_progress->setTaskDuration($timeTaken);

        $prev_user_code = $task_progress->getUserCode();
        if($prev_user_code != $userCode){
            $task_progress->setUserCode($userCode);
            $task_progress->setIsEvaluated(0);
        }


        $campusUserStatus = null;
        $gainedScore = 0;
        
        if($success_flag == 1){
            $task_progress = $this->handleUserSuccessInTask($task_progress, $timeTaken);
            $updatedScore = $this->gradingCommonService->updateScore($task_progress,$campus_round,$gmt_difference,LessonCommonService::PYTHON_MISSION,[],false);
            $task_progress = $updatedScore["campusActivityProgress"];
            $campusUserStatus = $updatedScore["campusUserStatus"];
            $gainedScore = $updatedScore["gainedXps"];

            //Save a student streak
            $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
        }

        $this->professionalRepository->storeRoundActivityProgress($task_progress);
        if($campusUserStatus)
            $this->professionalRepository->storeCampusUserStatus($campusUserStatus);

        //-----SEND NOTIFICATIONS-------
        $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
        // $this->sendStudentStuckNotification($account, $campus, $round_id, $activity_id);
        $this->sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task->getId());

        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);

        $this->accountRepository->commitDatabaseTransaction();

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $evalDetails = $this->getStudentEvaluationDetails($task_progress,'editor');
        $firstPass = $task_progress->getFirstPass();

        $ok=false;
        if($round_id!=null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
       //lti_synched

        return ["passed" => $success_flag, "gained_score"=>$gainedScore, "lti_synched"=>$ok, "taskFullMark"=>$fullMark, "score"=>$campusScoreData,"eval_details"=>$evalDetails, "first_pass"=>$firstPass];
    }

    public function updateRoundActivityMissionEditorProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default=true,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));


        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getMissionEditorById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();

        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);

        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }

        $duration=trim($duration);

        $gainedScore = 0;
        $passed = 0;
        $questions_answers = [];
        if($data==null){
            if($isStudent) {
                $passed = 1;
                if ($task_progress == null) {
                    $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0, 0, 0, null, null, null, null, null, $userCode);
                    if ($is_default)
                        $task_progress->setDefaultActivity($activity);
                    else
                        $task_progress->setActivity($activity);
                }

                if(isset($duration) && !empty($duration))
                    $task_progress->setTaskDuration($duration);

                $prev_user_code = $task_progress->getUserCode();
                if ($prev_user_code != $userCode) {
                    $task_progress->setIsEvaluated(0);
                    $score = $task_progress->getScore();
                    $task_progress->setEvaluation($score);
                    $task_progress->setUserCode($userCode);
                }

                $task_progress = $this->handleUserSuccessInTask($task_progress, $duration);
                $this->professionalRepository->storeRoundActivityProgress($task_progress);
            }
            else 
                return ["passed"=>'1'];
        }
        else {
            $mark_counter = 0;

            $quiz = $mission->getQuiz();
            if ($quiz == null)
                throw new BadRequestException(trans('locale.quiz_not_exist'));

            foreach ($data as $question) {
                $question_instance = $this->journeyRepository->getQuestionMcqById($question['question_id']);
                if ($question_instance == null)
                    throw new BadRequestException(trans('locale.question_not_exist'));

                $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
                if ($choice_instance == null)
                    throw new BadRequestException(trans('locale.choice_not_exist'));

                $choice_correct = $this->journeyRepository->getChoiceModelforMcqQuestion($question['question_id']);
                $question_answer = [
                    "question_id" => $question['question_id'],
                    "question_body" => $question_instance->getBody(),
                    "user_choice" => $choice_instance->getText(),
                    "user_choice_explanation" => $choice_instance->getExplanation(),
                    "choice_correct" => $choice_correct->first()->getText(),
                    "eval" => false
                ];
                if ($choice_correct->first()->id == $question['choice']) {
                    $question_answer['eval'] = true;
                    $mark_counter = $mark_counter + 1;
                }
                $questions_answers[] = $question_answer;
            }

            if ($mark_counter >= (sizeof($data) / 2))
                $passed = 1;

            if(!$isStudent)
                return ["passed" => $passed, "questions_answers" => $questions_answers];

            if($task_progress == null)
            {
                $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0,0,0, null,null,null,null,null,null);

                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
            }
            $no_trials = $task_progress->getNoTrials() + 1;
            $task_progress->setNoTrials($no_trials);

            if(isset($duration) && !empty($duration))
                $task_progress->setTaskDuration($duration);

            $prev_user_code = $task_progress->getUserCode();
            if($prev_user_code!=$userCode){
                $task_progress->setIsEvaluated(0);
                $score = $task_progress->getScore();
                $task_progress->setEvaluation($score);
                $task_progress->setUserCode($userCode);
            }
            $campusUserStatus=null;
            if($passed == 1){
                $task_progress = $this->handleUserSuccessInTask($task_progress, $duration);
                $updateScoreData = [
                    'correctAnsCount' => $mark_counter,
                    'questionsCount' => sizeof($data)
                ];
                $updateEvaluation = !$task_progress->getIsEvaluated();
                $returnedData = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference,LessonCommonService::EDITOR_MISSION, $updateScoreData, $updateEvaluation);

                $task_progress=$returnedData["campusActivityProgress"];
                $campusUserStatus=$returnedData["campusUserStatus"];
                //$this->professionalRepository->storeCampusUserStatus($campusUserStatus);
                //Save a student streak
                $gainedScore = $returnedData["gainedXps"];
                $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
            }

            $this->professionalRepository->storeRoundActivityProgress($task_progress);
            if($campusUserStatus)
                $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
        }


        //-----SEND NOTIFICATIONS-------
        $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
        // $this->sendStudentStuckNotification($account, $campus, $round_id, $activity_id);
        $this->sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task->getId());

        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);

        $this->accountRepository->commitDatabaseTransaction();

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $evalDetails = $this->getStudentEvaluationDetails($task_progress,'editor');
        $firstPass = $task_progress->getFirstPass();

        $ok=false;
        if($round_id!=null && $data!==null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        //lti_synched

        return ["passed" => $passed, "questions_answers" => $questions_answers,"gained_score"=>$gainedScore,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"eval_details"=>$evalDetails,"first_pass"=>$firstPass];
    }


    public function updateRoundActivityMissionAngularProgress($mission_id,$user_id,$round_id,$activity_id,$duration,$userCode,$data,$is_default=true,$gmt_difference){

        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));


        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getMissionAngularById($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();

        $checkActivityInCamp=$this->checkTaskInActivity($campus->getId(),$activity_id,$task->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.task_not_in_activity'));

        $task_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus->getId(),$activity_id,$is_default);

        $isStudent = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        
        $duration=trim($duration);

        $passed = 0;
        $questions_answers = [];
        $gainedScore = 0;
        if($data == null){
            if($isStudent){
                if ($task_progress == null) {
                    $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0, 0, 0, null, null, null, null, null, null);
                    if ($is_default)
                        $task_progress->setDefaultActivity($activity);
                    else
                        $task_progress->setActivity($activity);
                }
                if($duration!==null && !empty($duration))
                    $task_progress->setTaskDuration($duration);
                $prev_user_code = $task_progress->getUserCode();
                if ($prev_user_code != $userCode) {
                    $task_progress->setIsEvaluated(0);
                    $score = $task_progress->getScore();
                    $task_progress->setEvaluation($score);
                    $task_progress->setUserCode($userCode);
                }
                if($duration!==null)
                    $task_progress = $this->handleUserSuccessInTask($task_progress, $duration);
                    
                $userCodeItems = json_decode($userCode,true);
                foreach($userCodeItems as $userCodeItem){
                    $angularUserAnswer = $this->professionalRepository->getMissionAngularUserAnswerByName($task_progress->id, $userCodeItem['name']);
                    if($angularUserAnswer == null){
                        $angularUserAnswer = new MissionAngularUserAnswer($task_progress, null, null);
                        $angularUserAnswer->setName($userCodeItem['name']);
                        $angularUserAnswer->setData($userCodeItem['data']);
                    }else{
                        if($userCodeItem['data'] != $angularUserAnswer->data)
                            $task_progress->setIsEvaluated(0);

                        $angularUserAnswer->setData($userCodeItem['data']);
                    }
                    $this->professionalRepository->storeMissionAngularUserAnswer($angularUserAnswer);
                }

                $this->professionalRepository->storeRoundActivityProgress($task_progress);
            }else{
                return ["passed" => 1];
            }
        }else{
            $mark_counter = 0;

            $quiz = $mission->getQuiz();
            if ($quiz == null)
                throw new BadRequestException(trans('locale.quiz_not_exist'));

            foreach ($data as $question) {
                $question_instance = $this->journeyRepository->getQuestionMcqById($question['question_id']);
                if ($question_instance == null)
                    throw new BadRequestException(trans('locale.question_not_exist'));

                $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
                if ($choice_instance == null)
                    throw new BadRequestException(trans('locale.choice_not_exist'));

                $choice_correct = $this->journeyRepository->getChoiceModelforMcqQuestion($question['question_id']);
                $question_answer = ["question_id" => $question['question_id'],
                    "question_body" => $question_instance->getBody(),
                    "user_choice" => $choice_instance->getText(),
                    "user_choice_explanation" => $choice_instance->getExplanation(),
                    "choice_correct" => $choice_correct->first()->getText(),
                    "eval" => false];
                if ($choice_correct->first()->id == $question['choice']) {
                    $question_answer['eval'] = true;
                    $mark_counter = $mark_counter + 1;
                }
                $questions_answers[] = $question_answer;
            }
            if ($mark_counter >= (sizeof($data) / 2))
                $passed = 1;

            if(!$isStudent)
                return ["passed" => $passed, "questions_answers" => $questions_answers];

            if($task_progress == null)
            {
                $task_progress = new CampusActivityProgress($task, $user, $campus, 0, 0,0,0, null,null,null,null,null,null);

                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
            }
            $no_trials = $task_progress->getNoTrials() + 1;
            $task_progress->setNoTrials($no_trials);
            if($duration!==null && !empty($duration))
               $task_progress->setTaskDuration($duration);

            $prev_user_code = $task_progress->getUserCode();
            if($prev_user_code!=$userCode){
                $task_progress->setIsEvaluated(0);
                $score = $task_progress->getScore();
                $task_progress->setEvaluation($score);
                $task_progress->setUserCode($userCode);
            }

            $userCodeItems = json_decode($userCode,true);
            foreach($userCodeItems as $userCodeItem){
                $angularUserAnswer = $this->professionalRepository->getMissionAngularUserAnswerByName($task_progress->id, $userCodeItem['name']);
                if($angularUserAnswer == null){
                    $angularUserAnswer = new MissionAngularUserAnswer($task_progress, null, null);
                    $angularUserAnswer->setName($userCodeItem['name']);
                    $angularUserAnswer->setData($userCodeItem['data']);
                }else{
                    if($userCodeItem['data'] != $angularUserAnswer->data)
                        $task_progress->setIsEvaluated(0);

                    $angularUserAnswer->setData($userCodeItem['data']);
                }
                $this->professionalRepository->storeMissionAngularUserAnswer($angularUserAnswer);
            }
            $campusUserStatus=null;
            if($passed == 1){
                if($duration!==null)
                    $task_progress = $this->handleUserSuccessInTask($task_progress, $duration);
                $updateScoreData = [
                    'correctAnsCount' => $mark_counter,
                    'questionsCount' => sizeof($data)
                ];
                $updateEvaluation = !$task_progress->getIsEvaluated();
                $returnedData = $this->gradingCommonService->updateScore($task_progress, $campus_round,$gmt_difference,LessonCommonService::EDITOR_MISSION, $updateScoreData, $updateEvaluation);

                $task_progress=$returnedData["campusActivityProgress"];
                $campusUserStatus=$returnedData["campusUserStatus"];
                //$this->professionalRepository->storeCampusUserStatus($campusUserStatus);
                //Save a student streak
                $gainedScore = $returnedData["gainedXps"];
                $this->updateStudentStreaks($user,$gainedScore,$campus->id,$gmt_difference);
            }

            $this->professionalRepository->storeRoundActivityProgress($task_progress);
            if($campusUserStatus!==null)
                $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
        }


        //-----SEND NOTIFICATIONS-------

//        $this->sendSubmissionNotification($account, $campus, $round_id, $activity_id);
//        $this->sendStudentStuckNotification($account, $campus, $round_id, $activity_id);

        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $task_progress, $campus_round);

        $this->accountRepository->commitDatabaseTransaction();

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $evalDetails = $this->getStudentEvaluationDetails($task_progress,'editor');
        $firstPass = $task_progress->getFirstPass();

        $ok=false;
        if($round_id!=null && $data!==null) {
            $ok= $this->ltiSynchBack($user, $campus_round);
        }
        //lti_synched

        return ["passed" => $passed, "questions_answers" => $questions_answers,"gained_score"=>$gainedScore,"lti_synched"=>$ok,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"eval_details"=>$evalDetails,"first_pass"=>$firstPass];
    }



    public function updateCampusRound($user_id, $round_id, $starts_at, $ends_at,  $classes,  $tasks,  $rooms,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        $status = $round->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));
        $startsChangedFlag=false;
        if($starts_at!=null){
            $round->setStartsAt($starts_at);
            $startsChangedFlag=true;
        }
        $endsChangedFlag=false;
        if($ends_at!=null){
            $round->setEndsAt($ends_at);
            $endsChangedFlag=true;
        }

        if($round->getEndsAt() <= $round->getStartsAt())
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

        $classesArr=(!empty($classes))?json_decode($classes,true):[];
        $tasksArr=(!empty($tasks))?json_decode($tasks,true):[];
        $roomsArr=(!empty($rooms))?json_decode($rooms,true):[];
        $c=0;
        $classesCount=count($classesArr);
        $roundStartsDate=$round->getStartsAt();
        $roundEndsDate=$round->getEndsAt();
        if($gmt_difference!=null)
            $today=Carbon::today()->addHour(-($gmt_difference))->timestamp;
        else
            throw new BadRequestException(trans("locale.gmt_difference_should_be_valid_number"));
        foreach ($classesArr as $class){

            $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class['id']);
            if($roundClass == null)
                throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation
            if(!empty($class['starts_at']) && $roundClass->getStartsAt()!= $class['starts_at']){
//                if($class['starts_at']<$today )
//                    throw new BadRequestException(trans('locale.class_start_before_today'));
                $roundClass->setStartsAt($class['starts_at']);
                if($c===0)
                    $roundStartsDate=$class['starts_at'];

            }
            if(!empty($class['ends_at']) && $roundClass->getEndsAt()!= $class['ends_at']){
//                if($class['ends_at']<$today) {
//                    throw new BadRequestException(trans('locale.class_end_before_today'));
//                }
                $roundClass->setEndsAt($class['ends_at']);
                if($c==($classesCount-1))
                    $roundEndsDate=$class['ends_at'];
            }

            if($class['ends_at'] <= $class['starts_at'])
                throw new BadRequestException(trans("locale.end_date_start_date_min_time"));
            if(!empty($class['weight']))
                $roundClass->setWeight($class['weight']);

            $this->professionalRepository->storeCampusRoundClass($roundClass);
            $c++;
        }
        $round->setStartsAt($roundStartsDate);
        $round->setEndsAt($roundEndsDate);

        $campus = $round->getCampus();
        $tasks_weights=0;

//        foreach ($tasksArr as $task)
//            $tasks_weights+=$task['weight'];

//        if($tasks_weights!==100)
//            throw new BadRequestException(trans('locale.tasks_weights_should_equal_100')); //TODO translation

        foreach ($tasksArr as $task){
            $campusTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $task['task_id']);
            if($campusTask == null)
                throw new BadRequestException(trans('locale.task_with_id'.$task['task_id'].'not_exist')); //TODO translation

            $roundTask = $this->professionalRepository->getRoundActivityTask($round_id, $campusTask->getId());
            if(!empty($task['due_date']))
                $roundTask->setDueDate($task['due_date']);
            if(!empty($task['weight']) && $campusTask->getTask()->getMissionsHtml()->first() == null)
                $roundTask->setWeight($task['weight']);

            if($campusTask->getTask()->getMissionsHtml()->first())
                $roundTask->setWeight(0);
            if(!empty($task['weight_overdue']))
                $roundTask->setWeightOverdue($task['weight_overdue']);
            $this->professionalRepository->storeCampusRoundActivityTask($roundTask);
        }
        foreach ($roomsArr as $s){
            $room = $this->professionalRepository->getRoomById($s['id']);
            if(!empty($s['starts_at']))
                $room->setStartsAt($s['starts_at']);
            if(!empty($s['name']))
                $room->setName($s['name']);
            $this->professionalRepository->storeRoom($room);
        }

        $this->professionalRepository->storeCampusRound($round);

        $this->accountRepository->commitDatabaseTransaction();

        $roundDto= Mapper::MapClass(CampusRoundDto::class, $round,[CampusDto::class]);

        return $roundDto;
    }

    public function getAllCampuses($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user)&&!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        //Get Users school applicable courses
        $schoolCoursesIds = $this->professionalRepository->getProductCoursesIds($school->product_id);
        $schoolCoursesIds = empty($schoolCoursesIds) ? ['empty_school'] : $schoolCoursesIds;

        $campuses = $this->professionalRepository->getAllCampuses($schoolCoursesIds);

        $campuses = Mapper::MapEntityCollection(CampusDto::class, $campuses);
        return $campuses;
    }

//for test
    public function getSupportCampuses($user_id,$gmt_difference){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        //if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user)&&!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
           // throw new UnauthorizedException(trans("locale.no_permission"));
           $user_role = $user->getRoles();   
        $campuses = $this->professionalRepository->getSupportCampuses($user,$gmt_difference);
        
        $campuses = Mapper::MapEntityCollection( CampusReducedDto::class, $campuses);

        //If student return enrolled running rounds
        if($user->isStudent()){
            $runningRoundsData = $this->professionalRepository->getStudentRunningRounds($user_id, $gmt_difference);
            $runningRounds = [];
            foreach($runningRoundsData as $round){
                $roundData = [
                    'id'=>$round->id,
                    'round_name'=> $round->round_name.' | '.$round->campus->getName(),
                ];
                array_push($runningRounds,$roundData);
            }


            return $runningRounds;
        }else{
        //Admin & Teacher return only running rounds in courses
             $roundsData=$this->professionalRepository->getAllAdminRounds($user_id,$gmt_difference);
             $allRounds = [];
             foreach($roundsData as $round){
                $roundData = [
                    'id'=>$round->id,
                    'round_name'=> $round->round_name.' | '.$round->campus->getName(),
                ];
                array_push($allRounds,$roundData);
            }

             return $allRounds;
        }
    }

    public function saveWatchedVideoFlag($round_id,$user_id){
        
        $round = $this->professionalRepository->getCampusRoundById($round_id);
        
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); 

        $roundData = $this->professionalRepository->saveWatchedVideoFlag($round_id,$user_id);
        return $roundData;
    }

    public function getIntroDetails($round_id){
        //TODO: Validation

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        /*if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round'));*/

         
        $welcomeModule= $this->professionalRepository->getIntroDetails($round_id);
        $data["course_name"]=$round->getCampus()->getName();
        $data["course_id"]=$round->getCampus()->getId();
        $data["introductory_video_url"]=$welcomeModule==null?null:$welcomeModule->introductory_video_url;
        return $data;

    }

    public function searchForCountCampuses($search,$user_id,$gmt_difference){
        $student = $this->accountRepository->getUserById($user_id);
        if ($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campusesCount = $this->professionalRepository->searchForCountCampuses($search,$user_id,$gmt_difference);

        return $campusesCount;
    }

    public function getAllClasses($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user)&&!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $campuses = $this->professionalRepository->getAllClasses();

        $campuses = Mapper::MapEntityCollection(CampusClassDto::class, $campuses);
        return $campuses;
    }

    public function getAllTasks($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user)&&!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $campuses = $this->professionalRepository->getAllTasks();

        $campuses = Mapper::MapEntityCollection(CampusActivityTaskDto::class, $campuses);
        return $campuses;
    }

    public function getCampusClasses($user_id, $campus_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); //TODO translation

        $classes = $this->professionalRepository->getCampusClasses($campus_id);
        $classes = Mapper::MapEntityCollection(CampusClassDto::class, $classes);
        $unSortedClasses = json_decode(json_encode($classes), True);

        usort($unSortedClasses,array($this, 'tasks_cmp'));
        return $unSortedClasses;
    }

    public function getSubmissionsChart($user_id, $round_id, $from_time, $to_time){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        $campus = $round->getCampus();
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); //TODO translation

        $noOfSubmissions = $this->professionalRepository->getRoundSubmissions($campus->getId(), $round_id,$from_time,$to_time);
        $noOfStudentsSubmitted = $this->professionalRepository->getRoundStudentsSubmissions($campus->getId(), $round_id,$from_time,$to_time);

        $startDate = Carbon::parse($from_time);

        $endDate = Carbon::parse($to_time);
        $data = array();

        for($date = $startDate; $date->lte($endDate); $date->addDay()) {
            $noOfSubmissionsPerDayNumber = 0;
            $noOfStudentsSubmittedPerDayNumber = 0;
            $currentDate = $date->format('Y-m-d');
            foreach ($noOfSubmissions as $noOfSubmissionsPerDay){
                if($noOfSubmissionsPerDay->time == $currentDate)
                    $noOfSubmissionsPerDayNumber = $noOfSubmissionsPerDay->number;
            }

            foreach ($noOfStudentsSubmitted as $noOfStudentsSubmittedPerDay) {
                if($noOfStudentsSubmittedPerDay->time == $currentDate)
                    $noOfStudentsSubmittedPerDayNumber = $noOfStudentsSubmittedPerDay->number;
            }
            array_push($data,array('date'=>$currentDate,'no_submissions'=>$noOfSubmissionsPerDayNumber,'no_students'=>$noOfStudentsSubmittedPerDayNumber));
        }
        return $data;
    }


    //private functions
    private function checkOpenedModelAnswer($user, $mission_id){
        $returned = false;
        $isUser = false;
        $isStudent = false;
        $isChild = false;
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;

            if($role->getName() == 'user' || $role->getName() == 'invited_Individual')
                $isUser = true;

            if($role->getName() == 'student')
                $isStudent = true;

            if($role->getName() == 'child')
                $isChild = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getJourneys();
            if(count($journeys) > 0){
                foreach ($journeys as $invJourney){
                    if($mission->getTask()->getTaskOrder() != null){
                        if($invJourney->getJourney()->getId() == $mission->getTask()->getTaskOrder()->getJourney()->getId()){
                            if($isUser){
                                if($invJourney->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
            $activities = $invitation_subscription->getInvitation()->getDefaultActivities();
            if(count($activities) > 0){
                foreach ($activities as $invActivity){
                    if($mission->getTask()->getActivityTask() != null){
                        if($invActivity->getDefaultActivity()->getId() == $mission->getTask()->getActivityTask()->getDefaultActivity()->getId()){
                            if($isUser){
                                if($invActivity->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
        }
        return $returned;
    }

    private function sendSubmissionNotification($account, $campus, $round_id, $activity_id,$is_default=true){
        $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity_id);
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class->getId());
        $noOfNotEvaluatedTasks=$this->professionalRepository->getClassNotEvaluatedTasks($roundClass->getId(),$account->getId(),true,true);
        $teachers = $this->professionalRepository->getCampusRoundTeachers($round_id);
        $round = $this->professionalRepository->getRoundById($round_id);
        $round_name=$round->round_name;
        // dd($round_name);
        $data['course_name'] = $campus->getName();
        $data['round_name'] = $round_name;
        $data['course_id'] = $campus->getId();
        $data['round_id'] = $round_id;
        if($is_default==true) {
            $data['lesson_name'] = $class->getDefaultActivity()->getName();
        }else{
            $data['lesson_name'] = $class->getActivity()->getName();
        }
        $data['lesson_id'] = $class->getId();
        $data['round_lesson_id'] = $roundClass->getId();
        $data['activity_id'] = $activity_id;
        $data['submission_no'] = $noOfNotEvaluatedTasks;

        foreach ($teachers as $teacher){
            $this->deleteNotifications($teacher->getId(), $round_id, $roundClass->getId(), $activity_id, Submission::class);

            $notification = new UserNotification($teacher, Uuid::uuid4()->toString(), json_encode($data), Submission::class);
            $this->accountRepository->storeUserNotification($notification);
            Notification::send($teacher, new Submission($notification->getId(), $data));
        }
    }


    private function sendStudentStuckNotification($user,$account, $campus, $round_id, $activity_id,$task_id,$is_default=true){

        $task = $this->journeyRepository->getTaskById($task_id);

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
            
        $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity_id);
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class->getId());
        $teachers = $this->professionalRepository->getCampusRoundTeachers($round_id);
        $stuckStudents=$this->professionalRepository->getStuckStudentsPerClass($roundClass->getId(),$account->getId(),true,null);

        //Check learner trials
        $learnerProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task,$user->id,$campus->id,$activity_id,$is_default);
        if($learnerProgress){
            if($learnerProgress->no_of_trials >=3 && $learnerProgress->first_success == null){


                $round = $this->professionalRepository->getRoundById($round_id);
                $round_name=$round->round_name;
        
                $notificationDto=[];
                $notificationDto['course_name'] = $campus->getName();
                $notificationDto['round_name'] = $round_name;
                $notificationDto['course_id'] = $campus->getId();
                $notificationDto['round_id'] = $round->getId();
                if($is_default==true) {
                    $notificationDto['lesson_name'] = $class->getDefaultActivity()->getName();
                    $notificationDto['activity_id'] = $class->getDefaultActivity()->getId();
                }else{
                    $notificationDto['lesson_name'] = $class->getActivity()->getName();
                    $notificationDto['activity_id'] = $class->getActivity()->getId();
                }
                $notificationDto['lesson_id'] = $class->getId();
                $notificationDto['round_lesson_id'] = $roundClass->getId();
                $notificationDto['student_id'] = $user->id;
                $notificationDto['username'] = $user->first_name." ".$user->last_name;
                $notificationDto['date'] = $user->created_at;
        
                $notificationDto['task_id']=$task->getId();
                $notificationDto['mission_id']= $task->getTaskNameAndType()['mission_id'];
                $notificationDto['task_name'] = $task->getTaskNameAndType()['task_name'];
                $notificationDto['task_type'] = $task->getTaskNameAndType()['task_type'];
        
                foreach ($teachers as $teacher) {
                    $this->deleteTaskNotifications($teacher->getId(), $round_id, $activity_id,$task_id, StudentStuck::class);
        
                    $notification = new UserNotification($teacher, Uuid::uuid4()->toString(), json_encode($notificationDto), StudentStuck::class);
                    $this->accountRepository->storeUserNotification($notification);
                    Notification::send($teacher, new StudentStuck($notification->getId(), $notificationDto));
                }
        
                //Notifiy School Admin
                //Get School Admin
                $schoolAdmin = $this->accountRepository->getSchoolAdmin($account->id);  
                $this->deleteTaskNotifications($schoolAdmin->getId(), $round_id, $activity_id,$task_id, StudentStuck::class);
        
                //Send Notification to Admin
                $adminNotification = new UserNotification($schoolAdmin, Uuid::uuid4()->toString(), json_encode($notificationDto), StudentStuck::class);
                $this->accountRepository->storeUserNotification($adminNotification);
                 Notification::send($schoolAdmin, new StudentStuck($adminNotification->getId(), $notificationDto));
        
                //delete Notifications if no stuck student found
                if(count($stuckStudents)==0){
                    foreach ($teachers as $teacher)
                        $this->deleteNotifications($teacher->getId(), $round_id, $roundClass->getId(), $activity_id, Submission::class);
                }

            }
        }

    }

    //Send Mini-Project Notification when learner make a submission
    private function sendTaskSubmissionNotification($user, $campus, $round_id, $activity_id,$task_id,$task_progress_id,$isProject=false,$is_default=true){
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $task = $this->journeyRepository->getTaskById($task_id);
    

        $class = $this->professionalRepository->getCampusClass($campus->getId(), $activity_id);
        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class->getId());
        $teachers = $this->professionalRepository->getCampusRoundTeachers($round_id);
        $round = $this->professionalRepository->getRoundById($round_id);
        $round_name=$round->round_name;
        
        if($isProject){
            $taskType = 'Capstone Project';
        }else{
            $taskType = $task->getTaskNameAndType()['task_type'];
        }
            
        // dd($round_name);
        $data['course_name'] = $campus->getName();
        $data['round_name'] = $round_name;
        $data['course_id'] = $campus->getId();
        $data['round_id'] = $round->getId();
        $data['learner_id'] = $user->id;
        $data['username'] = $user->first_name." ".$user->last_name;
        $data['task_name'] = $task->getMissionsHtml()->first()->getTitle();
        $data['task_type'] = $taskType;
        $data['mission_id'] = $task->getMissionsHtml()->first()->getId();
        $data['date'] = $user->created_at;
        if($is_default==true) {
            $data['lesson_name'] = $class->getDefaultActivity()->getName();
            $data['activity_id'] = $class->getDefaultActivity()->getId();
        }else{
            $data['lesson_name'] = $class->getActivity()->getName();
            $data['activity_id'] = $class->getActivity()->getId();
        }
        $data['lesson_id'] = $class->getId();
        $data['round_lesson_id'] = $roundClass->getId();
        $data['submission_id'] = $task_progress_id;

        //Get School Admin
        $schoolAdmin = $this->accountRepository->getSchoolAdmin($account->id);  
        
        //Send Notification to Admin

                // $this->deleteNotifications($schoolAdmin->getId(), $round_id, $roundClass->getId(), $activity_id, TaskSubmission::class);
                $this->deleteTaskNotifications($schoolAdmin->getId(), $round_id, $activity_id,$task_id, TaskSubmission::class);
                $notification = new UserNotification($schoolAdmin, Uuid::uuid4()->toString(), json_encode($data), TaskSubmission::class);
                $this->accountRepository->storeUserNotification($notification);
                Notification::send($schoolAdmin, new TaskSubmission($notification->getId(), $data));
            
    }

    private function deleteNotifications($user_id, $round_id, $round_class_id, $activity_id, $type){
        $oldNotifications = $this->accountRepository->getNotifications($user_id, $type);

        foreach ($oldNotifications as $oldNotification){
            $old_data = json_decode($oldNotification->getData());
            if($old_data->round_id == $round_id && $old_data->round_lesson_id == $round_class_id && $old_data->activity_id == $activity_id)
                $this->accountRepository->deleteUserNotification($oldNotification);
        }
    }

    private function deleteTaskNotifications($user_id, $round_id, $activity_id, $task_id, $type){
        $oldNotifications = $this->accountRepository->getNotifications($user_id, $type);

        foreach ($oldNotifications as $oldNotification){
            $old_data = json_decode($oldNotification->getData());
            if(isset($old_data->task_id))
            {
                $old_task_id=$old_data->task_id;
            }elseif(isset($old_data->mission_id)){
                $mission = $this->journeyRepository->getHtmlMissionbyId($old_data->mission_id);
                $task = $mission->getTask();
                $old_task_id=$task->id;
            }
            
            if($old_data->round_id == $round_id && $old_data->activity_id == $activity_id && $old_task_id == $task_id)
                $this->accountRepository->deleteUserNotification($oldNotification);
        }
    }

    public function sendProfessionalSupportEmail($user_id, SupportEmailDto $supportEmailDto)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
            
        $round_id=$supportEmailDto->round_id;
        $round = $this->professionalRepository->getRoundById($round_id);
        $campus = $round->getCampus();

        if($campus !== null){
            $campus_name = $campus->getName();
            $supportEmailDto->info = "Course Name: ".$campus_name; 
            
            if($round){
                $roundInfo = preg_split ("/\-/", $round->round_name); 
                $courseNumber = $roundInfo[0]; 
                    if(count($roundInfo) > 1){
                        $courseSection = $roundInfo[1]; 
                    }else{
                        $courseSection = 'N/A'; 
                    }

                $supportEmailDto->courseNumber = $courseNumber;      
                $supportEmailDto->courseSection = $courseSection;      
            }

            
        }else{
            $supportEmailDto->type = "General"; 
        }
       
        $this->accountRepository->sendSupportEmail($user, $supportEmailDto);
        return trans('locale.support_email_sent');
    }

    public function getCampusClassesData($user_id,$campus_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user) &&
            !$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); //TODO translation

        $classes = $this->professionalRepository->getCampusClasses($campus_id);
        $classes = Mapper::MapEntityCollection(CampusClassDto::class, $classes);
        $unSortedClasses = json_decode(json_encode($classes), True);

        usort($unSortedClasses,array($this, 'tasks_cmp'));
        return $unSortedClasses;
    }


    public  function  getCampusClassTasks($user_id,$class_id,$is_default){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user) &&
            !$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));


        $class = $this->professionalRepository->getCampusClassTasksAndRooms($class_id);

        $classDto=[];

        if($is_default)
            $activity_id=$class->getDefaultActivity()->getId();
        else
            $activity_id=$class->getActivity()->getId();
        $class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$class->getCampus()->getId(),$is_default);
        //dd($class_tasks);


        $tasksDto=[];
        $htmlTasksDto=[];
        foreach ($class_tasks as $class_task){
            $task=$class_task->getTask();
            if($task->getMissions()->first() != null) {
                $mission = $task->getMissions()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
//                    'image' => $mission->getIconUrl(),
//
//                    'type' => 'mission',
//                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)

                ]);
            }
            elseif($task->getMissionsHtml()->first()!=null) {
                $mission = $task->getMissionsHtml()->first();
                array_push($htmlTasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getTitle(),
//                    'image' => $mission->getIconUrl(),
//
//                    'type' => 'html_mission',
//                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);

            }
            elseif($task->getQuizzes()->first()!=null) {
                $quiz = $task->getQuizzes()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $quiz->getTitle(),
//                    'image' => $quiz->geticonURL(),
//
//                    'type' => $quiz->getType()->getName(),
//                    'quiz_id' => $quiz->getId(),
                    'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            elseif($task->getMissionsCoding()->first()!=null){
                $codingMission=$task->getMissionsCoding()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $codingMission->getTitle(),
//                    'image' => $codingMission->getIconURL(),
//
//                    'type' => 'coding_mission',
//                    'mission_id' => $codingMission->getId(),
                    'order' => $codingMission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            elseif($task->getMissionsEditor()->first()!=null){
                $editorMission=$task->getMissionsEditor()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $editorMission->getTitle(),
//                    'image' => $editorMission->getIconURL(),
//                    'mode' => $editorMission->getType(),
//                    'type' => 'editor_mission',
//                    'mission_id' => $editorMission->getId(),
                    'order' => $editorMission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            $classDto['tasks']=$tasksDto;
        }
        $unordered_tasks=  $tasksDto;
        $classDto['tasks']=$this->sortClassTasks($unordered_tasks);
        $unordered_html_tasks=  $htmlTasksDto;
        $classDto['html_tasks']=$this->sortClassTasks($unordered_html_tasks);


        return $classDto;


    }
    public function getRoundClassTasks($user_id,$round_id,$class_id,$is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user) &&
            !$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

      $campus_id = $this->professionalRepository->getCampusRoundById($round_id)->getCampus_id();
        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round'));

        if(!$this->checkClassInRound($class_id,$round_id))
            throw new BadRequestException(trans('locale.class_in_this_round'));

//        $roundClass = $this->professionalRepository->getClassTasksAndRooms($class_id);
//
//        $class=$roundClass->getCampusClass();

        $class = $this->professionalRepository->getCampusClassById($class_id);
        $classDto=[];

        if($is_default)
            $activity_id=$class->getDefaultActivity()->getId();
        else
            $activity_id=$class->getActivity()->getId();
        $class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$campus_id,$is_default);


        $tasksDto=[];

        foreach ($class_tasks as $class_task){
            $task=$class_task->getTask();
            $campusRoundActivityTask = $this->professionalRepository->getRoundActivityTask($round_id, $class_task->getId());
            if($task->getMissions()->first() != null) {
                $mission = $task->getMissions()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image' => $mission->getIconUrl(),

                    'weight'=>$campusRoundActivityTask->getWeight(),
                    'type' => 'mission',
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)

                ]);
            }
//            elseif($task->getMissionsHtml()->first()!=null) {
//                $mission = $task->getMissionsHtml()->first();
//                if($mission->getIsMiniProject()==0) {//normal html
//                    array_push($htmlTasksDto, [
//                        'id' => $task->getId(),
//                        'name' => $mission->getTitle(),
//                        'iconUrl' => $mission->getIconUrl(),
//
//                        'type' => 'html_mission',
//                        'mission_id' => $mission->getId(),
//                        // 'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
//                    ]);
//                }elseif($mission->getIsMiniProject()){
//                    array_push($tasksDto, [
//                        'id' => $task->getId(),
//                        'name' => $mission->getTitle(),
//                        'iconUrl' => $mission->getIconUrl(),
//
//                        'type' => 'mini_project',
//                        'mission_id' => $mission->getId(),
//                        // 'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
//                        //'project_id' => $task->getProject()->first()->getProjectId()
//                    ]);
//                }
//
//            }
            elseif($task->getQuizzes()->first()!=null) {
                $quiz = $task->getQuizzes()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $quiz->getTitle(),
                    'image' => $quiz->geticonURL(),
                    'weight'=>$campusRoundActivityTask->getWeight(),
                    'type' => $quiz->getType()->getName(),
                    'quiz_id' => $quiz->getId(),
                    'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            elseif($task->getMissionsCoding()->first()!=null){
                $codingMission=$task->getMissionsCoding()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $codingMission->getTitle(),
                    'image' => $codingMission->getIconURL(),
                    'weight'=>$campusRoundActivityTask->getWeight(),
                    'type' => 'coding_mission',
                    'mission_id' => $codingMission->getId(),
                    'order' => $codingMission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            elseif($task->getMissionsEditor()->first()!=null){
                $editorMission=$task->getMissionsEditor()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $editorMission->getTitle(),
                    'image' => $editorMission->getIconURL(),
                    'weight'=>$campusRoundActivityTask->getWeight(),
                    'mode' => $editorMission->getType(),
                    'type' => 'editor_mission',
                    'mission_id' => $editorMission->getId(),
                    'order' => $editorMission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            elseif($task->getMissionsHtml()->first()!=null && $task->getMissionsHtml()->first()->getHtmlMissionType()=='mini_project' ){
                $htmlMission=$task->getMissionsHtml()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $htmlMission->getTitle(),
                    'image' => $htmlMission->getIconURL(),
                    'weight'=>$campusRoundActivityTask->getWeight(),
                    'type' => $task->getMissionsHtml()->first()->getHtmlMissionType(),
                    'mission_id' => $htmlMission->getId(),
                    'order' => $htmlMission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            $classDto['tasks']=$tasksDto;
        }
        $unordered_tasks=  $tasksDto;
        $classDto['tasks']=$this->sortClassTasks($unordered_tasks);


        return $classDto;



    }

    public function updateRoundClassTasks($user_id, $round_id,$class_id,$tasks,$gmt_difference){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user) &&
            !$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));


        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));


        $status = $round->getStatus($gmt_difference);
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round'));

        if(!$this->checkClassInRound($class_id,$round_id))
            throw new BadRequestException(trans('locale.class_in_this_round'));

        $tasksArr=(!empty($tasks))?json_decode($tasks,true):[];

        $campus = $round->getCampus();
        $tasks_weights=0;
        foreach ($tasksArr as $task)
            $tasks_weights+=$task['weight'];

        if($tasks_weights!==100)
            throw new BadRequestException(trans('locale.tasks_weights_should_equal_100')); //TODO translation

        $this->accountRepository->beginDatabaseTransaction();
        $updatedTasks=[];
        foreach ($tasksArr as $task){
            $campusTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $task['task_id']);
            if($campusTask == null)
                throw new BadRequestException(trans('locale.task_with_id'.$task['task_id'].'not_exist')); //TODO translation

            $roundTask = $this->professionalRepository->getRoundActivityTask($round_id, $campusTask->getId());

            if(!empty($task['weight']) && $campusTask->getTask()->getTaskNameAndType()["task_type"]!="html_mission") {
               // dd($task['task_id'],$roundTask->getWeight(),$task['task_weight']);
                $taskDto['id']=$task['task_id'];
                $taskDto['old_weight']=$roundTask->getWeight();
                $taskDto['new_weight']=$task['task_weight'];
                if($taskDto['old_weight']!==$taskDto['new_weight'])
                    $updatedTasks[]=$taskDto;

                $roundTask->setWeight($task['task_weight']);

            }

            if($campusTask->getTask()->getMissionsHtml()->first()!==null&&$campusTask->getTask()->getMissionsHtml()->first()->getHtmlMissionType()=="html_mission")
                $roundTask->setWeight(0);
            if(!empty($task['weight_overdue']))
                $roundTask->setWeightOverdue($task['weight_overdue']);
            $this->professionalRepository->storeCampusRoundActivityTask($roundTask);
        }
        $defaultActivityId=$this->professionalRepository->getCampusClassById($class_id)->getDefaultActivity()->getId();
        $arr=[];
        //dd($updatedTasks);
        foreach ($updatedTasks as $updatedTask){
            $studentsSolvedTask=$this->professionalRepository->getStudentSolvedTaskInRound($round,$defaultActivityId,$updatedTask['id']);
            foreach ($studentsSolvedTask as $studentSolvedTask) {

                $diffInCoinsAndXps=$this->calculateStudentXpsAndCoins($studentSolvedTask,$round,$defaultActivityId,$updatedTask['new_weight'],$updatedTask['old_weight']);
//                $diffInCoinsAndXps["user_id"]=$studentSolvedTask->getUser()->getId();
//                $arr[]=$diffInCoinsAndXps;
            }
        }

        $this->accountRepository->commitDatabaseTransaction();
        //return $arr;
        return trans("locale.campus_classes_updated_successfully");
    }

    public function updateDayTipStatus($user_id,$show){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));


        if($show=='true')
        {
            $show_num=1;
        }
        elseif($show=='false')
        {
            $show_num=0;
        }
        else{
            throw new BadRequestException('show_value_must_be_true_or_false');
        }


        $userData=$this->accountRepository->getUserById($user_id);
        $this->accountRepository->beginDatabaseTransaction();
       
            $userData->setShowDayTip($show_num);
            $this->accountRepository->storeUser($userData);


        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.tip_of_day_updated_successfully');



    }

    //update task current step
    public function updateStudentCurrentStep($user_id,$round_id,$activity_id,$task_id,$current_step,$gmt_difference,$is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $task = $this->journeyRepository->getTaskById($task_id);
        if ($task == null)
            throw new BadRequestException(trans('locale.task_not_exist'));
        $mission=$task->getMissionsHtml()->first();
        $missionSteps=$this->professionalRepository->getMissionStepsCount($mission->getId());

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if($is_default)
            $activity=$this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity=$this->journeyRepository->getActivityById($activity_id);

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));


        $status = $round->getStatus($gmt_difference);
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round'));

        $campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        if($campusRoundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$task_id,$is_default);
        if($campusRoundTask == null)
            throw new BadRequestException(trans('locale.round_task_not_exist'));
        $this->updateStudentLastSeen($user_id,$round_id,$activity_id,$task_id,$is_default);


        $userTaskCurrentStep=$this->professionalRepository->getTaskCurrentStep($user_id,$task_id,$activity_id,$round->getCampus_id(),$is_default);

        $this->accountRepository->beginDatabaseTransaction();
        $current_step=($current_step<=$missionSteps)?$current_step:$missionSteps;
        if($userTaskCurrentStep==null){

            $userTaskCurrentStep=new TaskCurrentStep($task,$user,$round->getCampus(),$current_step,0);
            if($is_default)
                $userTaskCurrentStep->setDefaultActivity($activity);
            else
                $userTaskCurrentStep->setActivity($activity);
            $this->professionalRepository->storeUserTaskCurrentStep($userTaskCurrentStep);

        }else{
            $userTaskCurrentStep->setCurrentStep($current_step);
            $this->professionalRepository->storeUserTaskCurrentStep($userTaskCurrentStep);
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.current_step_updated_successfully');


    }


    //update task current section
    public function updateStudentCurrentSection($user_id,$round_id,$activity_id,$task_id,$current_section,$gmt_difference,$is_default=true){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'school_admin')
                throw new BadRequestException(trans('locale.admin_not_allowed'));
            if($role->getName() == 'teacher')
                throw new BadRequestException(trans('locale.teacher_not_allowed'));
        }
        $task = $this->journeyRepository->getTaskById($task_id);
        if ($task == null)
            throw new BadRequestException(trans('locale.task_not_exist'));
        $mission=$task->getMissionsHtml()->first();
        
        $missionSectionsCount=$this->professionalRepository->getMissionSectionsCount($mission->getId());
     
        $currentSection = $this->professionalRepository->getSectionWithOrder($mission->getId(),$current_section);
        if($currentSection == null)
            throw new BadRequestException(trans('locale.current_section_not_exist_in_mission'));
        else
        $section_id= $currentSection->getId();
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if($is_default)
            $activity=$this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity=$this->journeyRepository->getActivityById($activity_id);
        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$round->getCampus();
        $status = $round->getStatus($gmt_difference);
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));
        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round'));
        $campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        if($campusRoundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist'));
        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$task_id,$is_default);
        if($campusRoundTask == null)
            throw new BadRequestException(trans('locale.round_task_not_exist'));

        $sections=$this->professionalRepository->getMissionSectionsCount($mission->getId(),false);

        $last_section_id=$sections->last()->getId();

        $userTaskCurrentSection=$this->professionalRepository->getTaskCurrentStep($user_id,$task_id,$activity_id,$round->getCampus_id(),$is_default);

        $oldCurrentSection=$userTaskCurrentSection['current_section'];
         
        
        $htmlSection=$this->professionalRepository->getSectionById($section_id);
        $missionHtmlSteps=$htmlSection->getSteps();
        $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus->getId(), $activity_id, $is_default);
        $failedToPass =[];
        $passed = 0;
        $first_success = null;
        $gainedScore = 0;
        $noOfQuestion = 0;
        if($last_section_id != $section_id){ //When is not the last section
            foreach($missionHtmlSteps as $data)
            {
                if($data->getBrickType() == 'quizbricks'){
                    if($missionProgress == null)
                        throw new BadRequestException(trans('locale.task_progress_not_exist'));

                    $question_id=$data->getQuestionTask()->question_id;
                    $question= $this->journeyRepository->getQuestionById($question_id);
                    if($question == null){
                       throw new BadRequestException(trans('locale.question_not_exist'));
                    }
                    else{
                       $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$question);
                       if($campusActivityQuestion == null)
                            throw new BadRequestException(trans('locale.question_progress_not_exist'));
                    }
                }
            }
        }else{
            $first_success=1;
            
            
            foreach($sections as $section){
                $htmlMissionSection=$this->professionalRepository->getSectionById($section->getId());
                $htmlMissionSectionSteps=$htmlMissionSection->getSteps();
                $passed = 1;
                
                foreach($htmlMissionSectionSteps as $data)
                {
                    if($data->getBrickType() == 'quizbricks'){
                        $noOfQuestion++;
                        // if($missionProgress == null)
                        //     throw new BadRequestException(trans('locale.task_progress_not_exist'));

                        $question_id=$data->getQuestionTask()->question_id;
                        $question= $this->journeyRepository->getQuestionById($question_id);

                        // if($question == null)
                        //     throw new BadRequestException(trans('locale.question_not_exist'));

                        $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$question);
                        $questionType=$data->getQuestionTask()->question->question_type;
                        if($campusActivityQuestion == null)
                        {
                            $passed = 0;
                            array_push($failedToPass,['questionType'=>$questionType,'id'=>$data->getQuestionTask()->question->id,'section'=>$section->order]);
                            break;
                            //     throw new BadRequestException(trans('locale.question_progress_not_exist'));
                        }
                        else{
                            $campusActivityQuestionId=$campusActivityQuestion->id;
                            if($questionType == 'question'){
                                $correctChoicesIds = [];
                                $mcqQuestionAnswerIds = [];
                                $mcqQuestionAnswer=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'multiple_choice');
                                foreach ($mcqQuestionAnswer as $answer) {array_push($mcqQuestionAnswerIds,$answer['choice_id']);}
                                $mcqQuestionId=$data->getQuestion()->id;
                                $correctChoices = $this->journeyRepository->getChoiceModelforMcqQuestion($mcqQuestionId)->toArray();
                                foreach ($correctChoices as $correctChoice) {array_push($correctChoicesIds,$correctChoice['id']);}
                                $answerDifferance = array_diff($correctChoicesIds,$mcqQuestionAnswerIds);
                                if(empty($answerDifferance)){
                                    $passed = 1;
                                }else{
                                    $passed = 0;
                                    //Add to failed to pass
                                    array_push($failedToPass,['questionType'=>'Mcq Question','id'=>$campusActivityQuestion->question_id,'section'=>$section->order]);
                                    break;
                                }

                            }elseif($questionType == 'sequence_match'){
                                $dragQuestionAnswer=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'sequence_match');
                                $dragCells=$question->getDragQuestion()->getDragCells()->toArray();
                                foreach ($dragQuestionAnswer as $key=>$cell) {
                                    $dragCell = $this->journeyRepository->getDragCellById($dragCells[$key]);
                                    // if($dragCell == null)
                                    //     throw new BadRequestException(trans('locale.drag_cell_not_exist'));

                                    $cellChoice = $this->journeyRepository->getDragChoiceById($cell['drag_choice_id']);
                                    // if($cellChoice == null)
                                    //     throw new BadRequestException(trans('locale.drag_choice_not_exist'));

                                    if($dragCell->getDragChoice()->id != $cellChoice->id){
                                        $passed = 0;
                                            //Add to failed to pass
                                            array_push($failedToPass,['questionType'=>'Drag','id'=>$campusActivityQuestion->question_id,'section'=>$section->order]);
                                        break;
                                    }
                                }
                            }elseif($questionType == 'dropdown'){
                                $dropdownQuestionAnswer=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'dropdown');
                                foreach ($dropdownQuestionAnswer as $dropdownData) {
                                    $dropdown = $this->journeyRepository->getDropdownById($dropdownData['dropdown_id']);
                                    // if($dropdown == null)
                                    //     throw new BadRequestException(trans('locale.dropdown_not_exist'));

                                    $dropdownChoice = $this->journeyRepository->getDropdownChoiceById($dropdownData['dropdown_choice_id']);
                                    // if($dropdownChoice == null)
                                    //     throw new BadRequestException(trans('locale.dropdown_choice_not_exist'));

                                    $dropdownCorrectChoice = $this->journeyRepository->getDropdownCorrectAnswer($dropdownData['dropdown_id']);
                                    // if($dropdownCorrectChoice == null)
                                    //     throw new BadRequestException(trans('locale.dropdown_correct_choice_not_exist'));

                                    if($dropdownCorrectChoice->id != $dropdownChoice->id){
                                        $passed = 0;
                                            //Add to failed to pass
                                            array_push($failedToPass,['questionType'=>'Dropdown','id'=>$campusActivityQuestion->question_id,'section'=>$section->order]);                                    
                                        break;
                                    }
                                }
                            }elseif($questionType == 'match'){
                                $matchQuestionAnswer=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'match');
                                foreach($matchQuestionAnswer as $key){
                                    $matchKey = mapper(MatchKey::class)->find($key['key_id']);
                                    if($key['value_id'] != $matchKey->getMatchValue()->id){
                                        $passed = 0;
                                            //Add to failed to pass
                                            array_push($failedToPass,['questionType'=>'Match','id'=>$campusActivityQuestion->question_id,'section'=>$section->order]); 
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }  
            
            if(!empty($failedToPass)){
                $passed = 0;
            }
            // update campus_activity_progress.first success = true

            //Check if Mini-Project
            if($task->getMissionsHtml()->first()!=null){
                 $mission = $task->getMissionsHtml()->first();
            }
           
            



            if($passed == 1){
                $mission=$task->getMissionsHtml()->first();
                $miniProject=$mission->getIsMiniProject();
                if($miniProject==1){
                    $numOfTrials=1;
                    if($missionProgress == null){
                        $score=0;
                        $firstSuccess=null;
                        $missionProgress = new CampusActivityProgress($task,$user,$campus,$score,$numOfTrials,0);
                        $missionProgress->setDefaultActivity($activity);
                        $this->professionalRepository->storeRoundActivityProgress($missionProgress);
                        $passed = 0;
                    }else{
                        if($missionProgress->is_evaluated != null){
                            $passed =1;
                        }else{
                            $passed =0;
                        }
                    }
                }
                else{
                    $campusUserStatus=null;
                    if($missionProgress == null){
                            $missionProgress = new CampusActivityProgress($task,$user,$campus,0,1,1,0,1);
                            $missionProgress->setDefaultActivity($activity);
                    }else{
                            $missionProgress->no_of_trials++;
                            if(!($missionProgress->first_success > 0))
                                $missionProgress->first_success = $missionProgress->no_of_trials;
                    }
                    if($mission->getLessonType() == 'normal'){
                        $returnedData = $this->gradingCommonService->updateScore($missionProgress,$round,$gmt_difference, LessonCommonService::HTML_MISSION);
                        $missionProgress = $returnedData["campusActivityProgress"];
                        $campusUserStatus = $returnedData["campusUserStatus"];
                        $gainedScore = $returnedData["gainedXps"];

                        $this->updateStudentStreaks($user,$gainedScore, $campus->id,$gmt_difference);
                    }

                    $this->professionalRepository->storeRoundActivityProgress($missionProgress);
                    if($campusUserStatus!==null)
                        $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
                }
            }
        }
            
        $this->updateStudentLastSeen($user_id,$round_id,$activity_id,$task_id,$is_default);

        $current_section=($current_section<=$missionSectionsCount)?$current_section:$missionSectionsCount;
        if($userTaskCurrentSection==null){
            $userTaskCurrentSection=new TaskCurrentStep($task,$user,$round->getCampus(),0,$current_section,$section_id);
            if($is_default)
                $userTaskCurrentSection->setDefaultActivity($activity);
            else
                $userTaskCurrentSection->setActivity($activity);
            $this->professionalRepository->storeUserTaskCurrentStep($userTaskCurrentSection);
        }else{
            if($oldCurrentSection < $current_section){
                $userTaskCurrentSection->setCurrentSection($current_section);
                $userTaskCurrentSection->setSectionId($section_id);
                $this->professionalRepository->storeUserTaskCurrentStep($userTaskCurrentSection);
            }
        }
        if($missionProgress == null){
            // $missionProgress = new CampusActivityProgress($task,$user,$campus,0,1,1,0,1);
            $missionProgress = new CampusActivityProgress($task,$user,$campus,0,1,1,0,$first_success);
            $missionProgress->setDefaultActivity($activity);
            $this->professionalRepository->storeRoundActivityProgress($missionProgress);
        }
        $lti_synched=$this->ltiSynchBack($user,$round);
        $this->accountRepository->commitDatabaseTransaction();
        $fullMark = $this->gradingCommonService->getTaskFullMarkInCampusRound($user, $missionProgress, $round);
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $campusScoreData = $this->lessonCommonService->getUserScoreInCampus($campusUserStatus,$gmt_difference);
        $evalDetails = $this->getStudentEvaluationDetails($missionProgress,'lesson',$noOfQuestion,null);
        $firstPass = $missionProgress->getFirstPass();

        return ["passed" => $passed, "gained_score"=>$gainedScore,"lti_synched"=>$lti_synched,"taskFullMark"=>$fullMark,"score"=>$campusScoreData,"failedToPass"=>$failedToPass,'eval_details'=>$evalDetails,'first_pass'=>$firstPass];
    }



    public function updateStudentLastSeen($user_id,$round_id,$activity_id,$task_id,$gmt_difference,$is_default=true){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $task = $this->journeyRepository->getTaskById($task_id);
        if ($task == null)
            throw new BadRequestException(trans('locale.task_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        if($is_default)
            $activity=$this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity=$this->journeyRepository->getActivityById($activity_id);

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));


        $status = $round->getStatus($gmt_difference);
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round'));

        $campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        if($campusRoundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$task_id,$is_default);
        if($campusRoundTask == null)
            throw new BadRequestException(trans('locale.round_task_not_exist'));
        $updatedTime=Carbon::now()->timestamp;

        $userLastSeenTask=$this->professionalRepository->getUserLastSeenTask($user_id,$round->getCampus_id(),$activity_id,$task_id,$is_default);
        $this->accountRepository->beginDatabaseTransaction();

        // if($userLastSeenTask==null){
        //     //create new record
        //     $userLastSeenTask=new TaskLastSeen($task,$user,$round->getCampus(),$updatedTime);
        //     if($is_default)
        //         $userLastSeenTask->setDefaultActivity($activity);
        //     else
        //         $userLastSeenTask->setActivity($activity);
        //     $this->professionalRepository->storeUserLastSeenTask($userLastSeenTask);

        // }else{
        //     //update old record
        //     $userLastSeenTask->setLastSeen($updatedTime);
        //     $this->professionalRepository->storeUserLastSeenTask($userLastSeenTask);

        // }

        //Create Mission Progress by default when learner view a mission
		$missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $round->getCampus()->getId(), $activity_id,$is_default);
        if($missionProgress==null){
        
            $missionProgress = new CampusActivityProgress($task, $user, $round->getCampus(),0,0,0);
            $missionProgress->setDefaultActivity($activity);
            $this->professionalRepository->storeRoundActivityProgress($missionProgress);
            
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.last_seen_updated_successfully');


    }

    public function createRoom($user_id, $round_id, $class_id, $starts_at, $name,$gmt_difference, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user)
            && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist')); //TODO translation

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class_id);
        if(Carbon::now()->timestamp>$starts_at)
            throw new BadRequestException(trans('locale.session_start_before_now'));


        $this->accountRepository->beginDatabaseTransaction();
        $room = new Room($roundClass);
        $is_existing_name=$this->professionalRepository->checkRoomNameExists($roundClass->id,$name);
        if($is_existing_name)
            throw new BadRequestException(trans('locale.session_name_already_exists'));

        $is_existing_room_same_time=$this->professionalRepository->checkRoomTimeExists($roundClass->id,$name,$starts_at);
        if($is_existing_room_same_time)
            throw new BadRequestException(trans('locale.session_time_already_exists'));
        $room->setName($name);
        $room->setStartsAt($starts_at);
//        if($file_s3_url != ''){
//            $room->setFileLink($file_s3_url);
//        }
        if($file_uploading_flag) {
            $material = new Material($roundClass);
            $material->setName($file_name);
            $material->setFileLink($file_s3_url);
            $this->professionalRepository->storeMaterial($material);
            $room->setMaterialId($material->getId());
        }elseif ($file_from_list_flag){
            $room->setMaterialId($file_id);
        }else {
            $room->setMaterialId(null);
        }
        $this->professionalRepository->storeRoom($room);

        $bbb = new BigBlueButton();
        $createMeetingParams = new CreateMeetingParameters($room->getId(), $room->getName());
        $createMeetingParams->setAttendeePassword($this::Attendee_Password);
        $createMeetingParams->setModeratorPassword($this::Moderator_Password);
        $createMeetingParams->setRecord(true);
        $createMeetingParams->setAllowStartStopRecording(true);
        $createMeetingParams->setAutoStartRecording(true);
        $response = $bbb->createMeeting($createMeetingParams);
        if ($response->getReturnCode() == 'FAILED')
            throw new BadRequestException(trans('locale.cannot_create_room')); //TODO translation

        $bbb_id = $response->getInternalMeetingId();
        $room->setBBBId($bbb_id);
        $this->professionalRepository->storeRoom($room);
        $this->accountRepository->commitDatabaseTransaction();

        return $room;
    }

    public function updateRoom($user_id, $room_id,$starts_at, $name, $file_s3_url, $file_uploading_flag, $file_from_list_flag, $file_id, $file_name)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if (!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user)
            && !$this->accountRepository->isAuthorized('get_AdminDashboard', $user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $room = $this->professionalRepository->getRoomById($room_id);
        if ($room === null)
            throw new BadRequestException(trans('locale.room_not_exist'));
        $campusRoundId = $room->getRoundClass()->getCampusRound()->getId();

        $checkTeacherResponsibleForRoom = $this->professionalRepository->checkUserInRound($user->getId(), $campusRoundId);
        if (!$checkTeacherResponsibleForRoom)
            throw new BadRequestException(trans('locale.user_not_in_this_round'));
        if (Carbon::now()->timestamp > $starts_at)
            throw new BadRequestException(trans('locale.session_start_before_now'));

        $roundClassId=$room->getRoundClass()->getId();
        if ($name != null){
            $is_existing_room_same_time=$this->professionalRepository->checkRoomTimeExists($roundClassId,$name,$starts_at,$room_id);
            if($is_existing_room_same_time)
                throw new BadRequestException(trans('locale.session_time_already_exists'));
            $room->setName($name);
        }

        if ($starts_at != null) {
            $is_existing_name=$this->professionalRepository->checkRoomNameExists($roundClassId,$name,$room_id);
            if($is_existing_name)
                throw new BadRequestException(trans('locale.session_name_already_exists'));

            $room->setStartsAt($starts_at);
        }
//        if ($file_uploading_flag && ($file_s3_url != '' || $file_s3_url != null)) {
//            $room->setFileLink($file_s3_url);
//        } else {
//            $room->setFileLink(null);
//        }
        if($file_uploading_flag) {
            $material = new Material($room->getRoundClass());
            $material->setName($file_name);
            $material->setFileLink($file_s3_url);
            $this->professionalRepository->storeMaterial($material);
            $room->setMaterialId($material->getId());
        }elseif ($file_from_list_flag){
            $room->setMaterialId($file_id);
        }else {
            $room->setMaterialId(null);
        }
        $this->professionalRepository->storeRoom($room);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(RoomDto::class, $room);
    }
    public function getMaterialsClass($round_id,$class_id) {

        $roundClass = $this->professionalRepository->getCampusRoundClass($round_id, $class_id);
        if($roundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist')); //TODO translation
        $outDto['materials'] = null;
        $materials = $this->professionalRepository->getMaterialsClass($roundClass->id);
        foreach ( $materials as $material){
            $outDto['materials'] []= $material;
        }

        return $outDto;

    }
    public function getStudentProgress($user_id,$round_id,$class_id,$task_id,$is_default=true)
    {
        $student = $this->accountRepository->getUserById($user_id);

        if ($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));


        $round = $this->professionalRepository->getRoundById($round_id);

        if ($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id = $round->getCampus()->getId();

        $checkStudentInRound = $this->checkUserInRound($student->getId(), $round_id);
        if (!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));


        $class = $this->professionalRepository->getCampusClassById($class_id);
        if ($class == null)
            throw new BadRequestException(trans('locale.class_not_exist'));

        $task=$this->journeyRepository->getTaskById($task_id);
        if ($task == null)
            throw new BadRequestException(trans('locale.task_not_exist'));
        if ($is_default) {
            $activity_id = $class->getDefaultActivity()->getId();
            $activity = $class->getDefaultActivity();
        }
        else {
            $activity_id = $class->getActivity()->getId();
            $activity = $class->getActivity();
        }
        $taskType=$task->getTaskNameAndType()['task_type'];

        if($taskType=='quiz'){
            $quizTaskId=$task->getTaskNameAndType()['task_id'];
            $quizId=$task->getTaskNameAndType()['mission_id'];
            //dd($quizId);
            $checkTaskInActivity=$this->checkTaskInActivity($campus_id,$activity_id,$quizTaskId,$is_default);
            if(!$checkTaskInActivity){
                $editorMission=$this->professionalRepository->getEditorMissionByQuiz($campus_id,$activity_id,$quizId,$is_default);
                if($editorMission==null)
                    throw new BadRequestException(trans('locale.no_editor_attached'));
                else{
                    $task=$editorMission->getTask();
                }
            }
        }
        


        $previous_progress=$this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity_id,$is_default);
       
        $progressArr = [];
        $solvedTasksNum = $this->professionalRepository->getStudentSolvedTasksInClass($student, $campus_id, $activity_id,$round, $is_default, true);
        $classTasksNum = $this->professionalRepository->getAllTasksInClass($campus_id, $activity_id,$student,$round, $is_default, true);
        ///////////////////////////////////////////////////////////
        $completedClassesNum = $this->professionalRepository->getStudentCompletedClassesInClass($student->getId(), $campus_id, $activity_id, $is_default, true);
        $totalLessonClasses = count($this->professionalRepository->getAllClassesInClass($campus_id,$activity_id,$is_default, true));
        $solvedMissionsNum = $this->professionalRepository->getStudentSolvedMissionsInClass($student->getId(), $campus_id, $activity_id, $is_default, true);
        $totalLessonMissions = count($this->professionalRepository->getAllMissionsInClass($campus_id,$activity_id,$is_default, true));
        //////////////////////////////////////////////////////////
        if ($classTasksNum !== 0) {


            $progressArr['lesson_before'] = intval(($solvedTasksNum / $classTasksNum) * 100) . '%';
            if ($solvedTasksNum < $classTasksNum)
                $progressArr['lesson_after'] = intval((($solvedTasksNum + 1) / $classTasksNum) * 100) . '%';
            else
                $progressArr['lesson_after'] = intval(($solvedTasksNum / $classTasksNum) * 100) . '%';

        } else {
            $progressArr['lesson_before'] = '0%';
            $progressArr['lesson_after'] = '0%';
        }


        $campusTasksNum = $this->professionalRepository->getAllTasksInCampus($campus_id, true);
        $solvedTasks = $this->professionalRepository->getStudentSolvedTasksInCampus($student->getId(), $round->getCampus()->getId());
        if ($campusTasksNum != 0){
            $progressArr['round_before'] = intval((count($solvedTasks) / $campusTasksNum) * 100) . '%';
           
            if(count($solvedTasks)< $campusTasksNum)
                $progressArr['round_after'] = intval(((count($solvedTasks)+1) / $campusTasksNum) * 100) . '%';
            else
                $progressArr['round_after'] = intval((count($solvedTasks) / $campusTasksNum) * 100) . '%';
        }
        else{
            $progressArr['round_before']="0%";
            $progressArr['round_after']="0%";
            //throw new UnauthorizedException(trans("locale.campus_has_no_tasks"));
        }

        if($previous_progress!=null ){
            if($previous_progress->getFirstSuccess()!=null) {
                $progressArr['lesson_after'] = $progressArr['lesson_before'];
                $progressArr['round_after'] = $progressArr['round_before'];
            }
        }


        //dd($solvedTasksNum,$classTasksNum,count($solvedTasks),$campusTasksNum);
        /////////////////////////////////////////////////////////////////////////////////
        $progressArr['completed_lesson_classes'] = $completedClassesNum;
        $progressArr['total_lesson_classes'] = $totalLessonClasses;
        $progressArr['solved_lesson_missions'] = $solvedMissionsNum;
        $progressArr['total_lesson_missions'] = $totalLessonMissions;
        ////////////////////////////////////////////////////////////////////////////////
        $progressArr['course_name']=$round->getCampus()->getName();
        $progressArr['class_name']=$activity->getName();
        return $progressArr;

    }

    private function calculateStudentScore(User $user,CampusRound $round,$activity_id,Task $task,$score,$old_score,$is_default){
        $campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$task->getId(),$is_default);
        $score_difference=$score-$old_score;
        //dd($score,$old_score,$score_difference);
        if($score_difference>0){
            $task_old_score=$old_score*($campusRoundTask->getWeight() / 100)*($campusRoundClass->getWeight()/100);
            $task_new_score=$score*($campusRoundTask->getWeight() / 100)*($campusRoundClass->getWeight()/100);
            if($old_score>=50){
                // dd($old_score);
                 $task_old_xps=round(10*$task_old_score);
                 $task_old_coins=round($task_old_xps/20);
            }else{
                $task_old_xps=0;
                $task_old_coins=0;
            }
           
            $task_new_xps=round(10*$task_new_score);
            $task_new_coins=round($task_new_xps/20);
            //dd($task_score,$campusRoundTask->getWeight(),)
            // dd('oldcoins',$task_old_coins);
            // dd('newcoins',$task_new_coins);

            $this->accountRepository->beginDatabaseTransaction();

            $userScore=$this->accountRepository->getUserScore($user->getId());

            $userScore->setExperience($userScore->getExperience()-$task_old_xps+$task_new_xps);
            $userScore->setCoins($userScore->getCoins()-$task_old_coins+$task_new_coins);
            $this->accountRepository->storeUserScore($userScore);

            $this->accountRepository->commitDatabaseTransaction();
            return["xp_gained"=>$task_new_xps-$task_old_xps,"coins_gained"=>$task_new_coins-$task_old_coins];
        }
        return["xp_gained"=>0,"coins_gained"=>0];

    }

    private function calculateStudentXpsAndCoins(CampusActivityProgress $progress,CampusRound $round,$activity_id,$newTaskWeight,$oldTaskWeight){
        $is_default=true;
        $campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        $old_score=$progress->getEvaluation();

        if($old_score>0){
            $old_task_score=$old_score*($oldTaskWeight / 100)*($campusRoundClass->getWeight()/100);
            $old_task_xps=round(10*$old_task_score);
            $old_task_coins=round($old_task_xps/20);


            $new_task_score=$old_score*($newTaskWeight/ 100)*($campusRoundClass->getWeight()/100);
            $new_task_xps=round(10*$new_task_score);
            $new_task_coins=round($new_task_xps/20);
//            if($progress->getUser()->getId()=='100187')
//                dd($old_score,$old_task_score,$old_task_xps,$new_task_score,$new_task_xps);


            $this->accountRepository->beginDatabaseTransaction();

            $userScore=$this->accountRepository->getUserScore($progress->getUser()->getId());

            $userScore->setExperience($userScore->getExperience()-$old_task_xps+$new_task_xps);
            $userScore->setCoins($userScore->getCoins()-$old_task_coins+$new_task_coins);
            $this->accountRepository->storeUserScore($userScore);

            $this->accountRepository->commitDatabaseTransaction();
            return["xp_diff"=>($new_task_xps-$old_task_xps),"coins_diff"=>($new_task_coins-$old_task_coins)];
        }
        return["xp_diff"=>0,"coins_diff"=>0];

    }


    public function calculateCourseLessonsTime($campus_ids){
        $campuses=$this->professionalRepository->getAllCampuses($campus_ids);
        $this->accountRepository->beginDatabaseTransaction();
        foreach ($campuses as $campus){
            $lessons=$campus->getClasses();
            foreach ($lessons as $lesson) {
                $lessonTime=$this->calculateLessonTime($lesson);
                $lesson->setTimeEstimate($lessonTime);
                $this->professionalRepository->storeCampusClass($lesson);
                
            }
        }
        $this->accountRepository->commitDatabaseTransaction();

    }
    private function calculateLessonTime(CampusClass $lesson){
        $activity_tasks=$lesson->getDefaultActivity()->getTasks();
        $estimated_time=0;
        foreach ($activity_tasks as $task){
            $taskType=$task->getTaskNameAndType()['task_type'];
            $estimated_time+=$this->professionalRepository->getTaskTypeTimeEstimate($taskType);

        }
        return $estimated_time;
    }


    public  function getCampusClassId($user_id , $campus_id , $class_id,$gmt_difference,$is_default)
    {
        $student = $this->accountRepository->getUserById($user_id);

        if ($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist'));

        $classData = $this->professionalRepository->getCampusClassid($campus_id,$class_id,$is_default);
        
        //Check Crown for student
        $gainedCrown = $this->lessonCommonService->CheckStudentCrown($user_id,$campus_id,$gmt_difference);
        $classData->gained_crown = $gainedCrown;
        
        return $classData ;

    }


    public function getlessonsSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count){
        //Clean Search 
        $search = $this->cleanSearch($search);
    
        $student = $this->accountRepository->getUserById($user_id);
        if ($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));


        // return 0;
       if($limit != null) {
           $page = intval($start_from / $limit) + 1;
           $currentPage = $page;
           Paginator::currentPageResolver(function () use ($currentPage) {
               return $currentPage;
           });
       }

        $isStudent = false;
        foreach ($student->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }

       $searchResults=$this->professionalRepository->getlessonsSearch($search,$limit,$student->getId(),$gmt_difference,$count,$isStudent);

        if($count == false) {
           // $mappedClasses=Mapper::MapEntityCollection(CampusClassDto::class,$searchResults);
           // return $mappedClasses;
        $moduleDto=[];
        foreach ($searchResults as $data){
            // dd($data);
            if($data->activity_id){
                $is_default=true;
                $activity_id=$data->activity_id;
            }
            else{
                $is_default=false;
                $activity_id=$data->activity_id;
            }

            $campus=$this->professionalRepository->getCampusById($data->campus_id);
            $class=$this->professionalRepository->getCampusClassId($campus->getId(),$activity_id,$is_default);
            // get lesson type
            $lessonType=$this->getLessonTypeName($class->lessonType());

            array_push($moduleDto, [
                'id' => $class->getId(),
                'name' => $class->getName(),
                'campusId' => $campus->getId(),
                'courseName' => $campus->getName(),
                'roundId' => $data->campus_round_id,
                'activityId' => $activity_id,
                'order' =>$class->getOrder(),
                'moduleType' => $lessonType,
                'description' => $data->description
            ]);
        }
        return $moduleDto;
        }elseif($count == true)
        {
           return $searchResults;
        }
    }

    private function cleanSearch($search)
    {
        // allow only letters
        $res = preg_replace("/[^a-zA-Z]/", " ", $search);

        return $res;
    }

    public function getProfessionalSearch($search,$start_from,$limit,$user_id,$gmt_difference,$count){
        $student = $this->accountRepository->getUserById($user_id);
        if ($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));


        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $isStudent = false;
        foreach ($student->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }

        //Clean Search
        $search = $this->cleanSearch($search);

        $searchResults=$this->professionalRepository->getProfessionalCampusesSearch($search,$limit,$student->getId(),$gmt_difference,$count,$isStudent);
        // dd($searchResults);

        if($count == false) {
        // $mappedCampuses=Mapper::MapEntityCollection(CampusDto::class,$searchResults);
        //    return $mappedCampuses;
        $campusesDto=[];
        foreach ($searchResults as $data){
            
            $campus=$this->professionalRepository->getCampusById($data->campus_id);
                array_push($campusesDto, [
                    'id' => $campus->getId(),
                    'campusHasSubModules' => $campus->hasSubModules(),
                    'roundId' => $data->campus_round_id,
                    'round_name' => $data->round_name,
                    'name' => $campus->getName(),
                    'description' => $data->description,
                ]);
        }
        return $campusesDto;

        }elseif($count == true){
            return $searchResults;
        }
    }

    public function searchForTasks($search,$start_from,$limit,$user_id,$gmt_difference,$count){

        //Clean Search 
        $search = $this->cleanSearch($search);
    
        
        $user = $this->accountRepository->getUserById($user_id);
        
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $user_role = $user->getRoles();    
            
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        
        $class_tasks=$this->professionalRepository->searchForTasks($search,$limit,$user_id,$user_role,$gmt_difference,$count);
        // dd($class_tasks);
        //Return Count if is true
        if($count == true)
            return $class_tasks;

        $tasksDto=[];
        foreach ($class_tasks as $class_task){

            $campus=$this->professionalRepository->getCampusById($class_task->campus_id);

            if($class_task->activity_id) {
                $activity_id = $class_task->activity_id;
                $is_default = true;
            }
            else {
                $activity_id = $class_task->activity_id;
                $is_default = false;
            }

            //To show only accessible sections 
            $userTaskCurrentSection=$this->professionalRepository->getTaskCurrentStep($user_id,$class_task->task_id,$class_task->activity_id,$campus->id,$is_default);
            if($userTaskCurrentSection){
                $userCurrentSectionOrder = $userTaskCurrentSection->current_section;
            }else{
                $userCurrentSectionOrder = 100; // Set as default for admin
            }
                
    
            $class=$this->professionalRepository->getCampusClass($campus->getId(), $activity_id, $is_default);
            // $task=$class_task->getTask();
            $task=$this->journeyRepository->getTaskById($class_task->task_id);

            if($task->getMissions()->first() != null) {
                $mission = $task->getMissions()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'iconUrl' => $mission->getIconUrl(),
                    'type' => 'mission',
                    'description'=>$class_task->description,
                    'missionId' => $mission->getId(),
                    'courseId' => $campus->getId(),
                    'courseName' => $campus->getName(),
                    'roundId' => $class_task->campus_round_id,
                    'activityId' => $activity_id,
                    'moduleName' => $class->getName(),
                    'description' =>  preg_replace("/<img[^>]+\>/i", "", $mission->getDescription())
                ]);
            }
            elseif($task->getMissionsHtml()->first()!=null) {
                
                $TaskSection = $this->professionalRepository->getSectionById($class_task->section_id);
                $sectionOrder =  $TaskSection ? $TaskSection->order : null;
                if($sectionOrder <= $userCurrentSectionOrder){
                $mission = $task->getMissionsHtml()->first();
                    if($mission->getIsMiniProject()){
                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $mission->getTitle(),
                            'description'=>$class_task->description,
                            'iconUrl' => $mission->getIconUrl(),
                            'type' => 'mini_project',
                            'missionId' => $mission->getId(),
                            'courseId' => $campus->getId(),
                            'roundId' => $class_task->campus_round_id,
                            'courseName' => $campus->getName(),
                            'activityId' => $activity_id,
                            'moduleName' => $class->getName()
                        ]);
                    }else{
                        
                        $htmlSection=$this->professionalRepository->getSectionById($class_task->section_id);
                        if($htmlSection){
                            $htmlSectionOrder = $htmlSection->order;
                        }else{
                            $htmlSectionOrder = 1;
                        }
                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $mission->getTitle(),
                            'description'=>$class_task->description,
                            'iconUrl' => $mission->getIconUrl(),
                            'type' => 'html_mission',
                            'missionId' => $mission->getId(),
                            'courseId' => $campus->getId(),
                            'courseName' => $campus->getName(),
                            'sectionNumber'=>$htmlSectionOrder,
                            'roundId' => $class_task->campus_round_id,
                            'activityId' => $activity_id,
                            'moduleName' => $class->getName()
                        ]);
                    }
                }
            }
            elseif($task->getQuizzes()->first()!=null) {
                $quiz = $task->getQuizzes()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $quiz->getTitle(),
                    'description'=>$class_task->description,
                    'quizId' => $quiz->getId(),
                    'iconUrl' => $quiz->geticonURL(),
                    'type' => $quiz->getType()->getName(),
                    'courseId' => $class_task->campus_id,
                    'courseName' => $campus->getName(),
                    'roundId' => $class_task->campus_round_id,
                    'activityId' => $activity_id,
                    'moduleName' => $class->getName()
                ]);
            }
            elseif($task->getMissionsCoding()->first()!=null){
                $codingMission=$task->getMissionsCoding()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $codingMission->getTitle(),
                    'description'=>$class_task->description,                    
                    'iconUrl' => $codingMission->getIconURL(),
                    'type' => 'coding_mission',
                    'missionId' => $codingMission->getId(),
                    'courseId' => $campus->getId(),
                    'courseName' => $campus->getName(),
                    'roundId' => $class_task->campus_round_id,
                    'activityId' => $activity_id,
                    'moduleName' => $class->getName(),
                    'description' => preg_replace("/<img[^>]+\>/i", "", $codingMission->getDescription())
                ]);
            }
            elseif($task->getMissionsEditor()->first()!=null){
                $editorMission=$task->getMissionsEditor()->first();

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $editorMission->getTitle(),
                    'description'=>$class_task->description,
                    'iconUrl' => $editorMission->getIconURL(),
                    'mode' => $editorMission->getType(),
                    'type' => 'editor_mission',
                    'missionId' => $editorMission->getId(),
                    'courseId' => $campus->getId(),
                    'courseName' => $campus->getName(),
                    'roundId' => $class_task->campus_round_id,
                    'activityId' => $activity_id,
                    'moduleName' => $class->getName(),
                    'description' => preg_replace("/<img[^>]+\>/i", "", $editorMission->getDescription())
                ]);
            }
        }
        return $tasksDto;
    }


    public function searchForCountTasks($search){

        $tasksCount = $this->professionalRepository->searchForCountTasks($search);

        return $tasksCount;
    }

    public function searchForAll($search,$start_from,$limit,$user_id,$gmt_difference,$count=false){
        $student = $this->accountRepository->getUserById($user_id);
        if ($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));


        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $isStudent = false;
        foreach ($student->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }

        $resultDto=[];

        //$courseResults=$this->professionalRepository->searchForCoursesModulesTasks($search,$limit,$student->getId(),$gmt_difference,$count,$isStudent);
        //dd($courseResults);
        
        $courseResults=$this->professionalRepository->getProfessionalCampusesSearch($search,null,$student->getId(),$gmt_difference,$count,$isStudent);
        if(!$count){

            foreach($courseResults as $result){
                    $campus=$this->professionalRepository->getCampusById($result->campus_id);
                    array_push($resultDto, [
                        'id' => $campus->getId(),
                        'searchType' => 'course',
                        'roundId' => $result->id,
                        'name' => $campus->getName(),
                        'description' => $campus->getDescription()
                    ]);
            }
        }


        $moduleResults=$this->professionalRepository->getlessonsSearch($search,null,$student->getId(),$gmt_difference,$count,$isStudent);
        if(!$count){
            foreach ($moduleResults as $data){
                if($data->default_activity_id){
                $is_default=true;
                $activity_id=$data->default_activity_id;
                }
                else{
                    $is_default=false;
                    $activity_id=$data->activity_id;
                }
                $campus=$this->professionalRepository->getCampusById($data->campus_id);
                $class=$this->professionalRepository->getCampusClassId($campus->getId(),$activity_id,$is_default);

                // get lesson type
                $lessonType=$this->getLessonTypeName($class->lessonType());

                array_push($resultDto, [
                    'id' => $class->getId(),
                    'searchType' => 'module',
                    'name' => $class->getName(),
                    'campusId' => $campus->getId(),
                    'courseName' => $campus->getName(),
                    'roundId' => $data->round_id,
                    'activityId' => $activity_id,
                    'order' =>$class->getOrder(),
                    'moduleType' => $lessonType,
                    'description' => $class->getDescription()
                ]);
            }

        }
        
        $user_role = $student->getRoles(); 

        $tasksResults=$this->professionalRepository->searchForTasks($search,null,$user_id,$user_role,$gmt_difference,$count);
        if(!$count){

            foreach ($tasksResults as $class_task){

                $campus=$this->professionalRepository->getCampusById($class_task->campus_id);
                 if($class_task->default_activity_id) {
                $activity_id = $class_task->default_activity_id;
                $is_default = true;
                }
                else {
                    $activity_id = $class_task->activity_id;
                    $is_default = false;
                }
                $class=$this->professionalRepository->getCampusClass($campus->getId(), $activity_id, $is_default);
                // $task=$class_task->getTask();
                $task=$this->journeyRepository->getTaskById($class_task->task_id);
                if($task->getMissions()->first() != null) {
                    $mission = $task->getMissions()->first();

                    array_push($resultDto, [
                        'id' => $task->getId(),
                        'searchType' => 'task',
                        'name' => $mission->getName(),
                        'iconUrl' => $mission->getIconUrl(),
                        'type' => 'mission',
                        'missionId' => $mission->getId(),
                        'courseId' => $class_task->campus_id,
                        'courseName' => $campus->getName(),
                        'roundId' => $class_task->campus_round_id,
                        'activityId' => $activity_id,
                        'moduleName' => $class->getName(),
                        'description' =>  preg_replace("/<img[^>]+\>/i", "", $mission->getDescription())
                    ]);
                }
                elseif($task->getMissionsHtml()->first()!=null) {
                    $mission = $task->getMissionsHtml()->first();
                    if($mission->getIsMiniProject()){
                        array_push($resultDto, [
                            'id' => $task->getId(),
                            'searchType' => 'task',
                            'name' => $mission->getTitle(),
                            'iconUrl' => $mission->getIconUrl(),
                            'type' => 'mini_project',
                            'missionId' => $mission->getId(),
                            'courseId' => $class_task->campus_id,
                            'courseName' => $campus->getName(),
                            'roundId' => $class_task->campus_round_id,
                            'activityId' => $activity_id,
                            'moduleName' => $class->getName()
                        ]);
                    }else{
                        array_push($resultDto, [
                            'id' => $task->getId(),
                            'searchType' => 'task',
                            'name' => $mission->getTitle(),
                            'iconUrl' => $mission->getIconUrl(),
                            'type' => 'html_mission',
                            'missionId' => $mission->getId(),
                            'courseId' => $class_task->campus_id,
                            'courseName' => $campus->getName(),
                            'roundId' => $class_task->campus_round_id,
                            'activityId' => $activity_id,
                            'moduleName' => $class->getName()
                        ]);
                    }
                }
                elseif($task->getQuizzes()->first()!=null) {
                    $quiz = $task->getQuizzes()->first();

                    array_push($resultDto, [
                        'id' => $task->getId(),
                        'searchType' => 'task',
                        'name' => $quiz->getTitle(),
                        'quizId' => $quiz->getId(),
                        'iconUrl' => $quiz->geticonURL(),
                        'type' => $quiz->getType()->getName(),
                        'courseId' => $class_task->campus_id,
                        'courseName' => $campus->getName(),
                        'roundId' => $class_task->campus_round_id,
                        'activityId' => $activity_id,
                        'moduleName' => $class->getName()
                    ]);
                }
                elseif($task->getMissionsCoding()->first()!=null){
                    $codingMission=$task->getMissionsCoding()->first();

                    array_push($resultDto, [
                        'id' => $task->getId(),
                        'searchType' => 'task',
                        'name' => $codingMission->getTitle(),
                        'iconUrl' => $codingMission->getIconURL(),
                        'type' => 'coding_mission',
                        'missionId' => $codingMission->getId(),
                        'courseId' => $class_task->campus_id,
                        'courseName' => $campus->getName(),
                        'roundId' => $class_task->campus_round_id,
                        'activityId' => $activity_id,
                        'moduleName' => $class->getName(),
                        'description' => preg_replace("/<img[^>]+\>/i", "", $codingMission->getDescription())
                    ]);
                }
                elseif($task->getMissionsEditor()->first()!=null){
                    $editorMission=$task->getMissionsEditor()->first();

                    array_push($resultDto, [
                        'id' => $task->getId(),
                        'searchType' => 'task',
                        'name' => $editorMission->getTitle(),
                        'iconUrl' => $editorMission->getIconURL(),
                        'mode' => $editorMission->getType(),
                        'type' => 'editor_mission',
                        'missionId' => $editorMission->getId(),
                        'courseId' => $class_task->campus_id,
                        'courseName' => $campus->getName(),
                        'roundId' => $class_task->campus_round_id,
                        'activityId' => $activity_id,
                        'moduleName' => $class->getName(),
                        'description' => preg_replace("/<img[^>]+\>/i", "", $editorMission->getDescription())
                    ]);
                }
            }
        }

        if(!$count)

            return array_slice($resultDto,$start_from,$limit);
        else
            return $courseResults+$moduleResults+$tasksResults;
        // return["course"=>$courseDto,"module"=>$moduleDto];


    }

    private function calculateStudentXpsAndCoinsInClass(CampusActivityProgress $progress,CampusRound $round,$activity_id,$lessonWeight){
        $is_default=true;
        //$campusRoundClass=$this->professionalRepository->getCampusRoundClassByActivityId($round,$activity_id,$is_default);
        $campusRoundTask=$this->professionalRepository->getCampusRoundTaskByActivityId($round,$activity_id,$progress->getTask()->getId(),$is_default);
        $old_score=$progress->getEvaluation();

        if($old_score>=50){
            $old_task_score=$old_score*($campusRoundTask->getWeight() / 100)*($lessonWeight/100);
            $old_task_xps=round(10*$old_task_score);
            $old_task_coins=round($old_task_xps/20);


            $userScore=$this->accountRepository->getUserScore($progress->getUser()->getId());

            $userScore->setExperience($userScore->getExperience()+$old_task_xps);
            $userScore->setCoins($userScore->getCoins()+$old_task_coins);
            $this->accountRepository->storeUserScore($userScore);

            return["user_id"=>$progress->getUser()->getId(),"xp_diff"=>($old_task_xps),"coins_diff"=>($old_task_coins)];
        }
        return["user_id"=>$progress->getUser()->getId(),"xp_diff"=>0,"coins_diff"=>0];

    }


    public function calculateScore($campus_ids){
        //$campuses=$this->professionalRepository->getAllCampuses($campus_ids);
        $rounds=$this->professionalRepository->getRoundsInCampuses($campus_ids);
        //dd($rounds->pluck('id'));
        $this->accountRepository->beginDatabaseTransaction();
        foreach ($rounds as $round){
            $students=$round->getStudents();
            foreach ($students as $student){
               // dd($student);
                $userScore=$this->accountRepository->getUserScore($student->getId());

                $userScore->setExperience(0);
                $userScore->setCoins(0);
                $this->accountRepository->storeUserScore($userScore);

            }
        }
        //dd("end");
        foreach ($rounds as $round){
            $roundClasses=$round->getCampusRoundClasses();
            foreach ($roundClasses as $roundClass){
                $class_activity_id=$roundClass->getCampusClass()->getDefaultActivity()->getId();
                $studentsSolvedTasksInClass=$this->professionalRepository->getStudentSolvedTasksInRoundClass($round,$class_activity_id);

                foreach($studentsSolvedTasksInClass as $solvedTasksInClass){
                    $student_id = $solvedTasksInClass->getUser()->getId();
                    //if($student_id==100)
                    $score_diff=$this->calculateStudentXpsAndCoinsInClass($solvedTasksInClass,$round,$class_activity_id,$roundClass->getWeight());


                }
            }
        }
        $this->accountRepository->commitDatabaseTransaction();
    }
    public function getCampusDictionary($campus_id){
        
        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 
        $dictionaryDto=[];
        foreach ($campus->getClasses() as  $campusClass) {
            $activity=$campusClass->getDefaultActivity();
            $activity_dictionary_words=$this->professionalRepository->getCampusDictionaryWordsInActivity($campus_id,$activity->id);
            $mapped_dictionary_words = Mapper::MapEntityCollection(DictionaryWordDto::class, $activity_dictionary_words); 
            $dictionaryDto[$activity->id]=$mapped_dictionary_words;

        }

        //$dictionary_words=$this->professionalRepository->getCampusDictionaryWords($campus_id);   
        //$mapped_dictionary_words = Mapper::MapEntityCollection(DictionaryWordDto::class, $dictionary_words); 
        return  $dictionaryDto;


    }

    public function _getStudentStreaks($user,$round_id,$gmt_difference){
        $round=$this->professionalRepository->getRoundById($round_id);

        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id = $round->getCampus()->id;

        $active = $this->professionalRepository->getStreaksActiveStatus($user->id,$campus_id,$gmt_difference);
        $count = $this->professionalRepository->getStreaksCount($user->id,$campus_id,$gmt_difference);
        $week = $this->professionalRepository->getStreaksWeek($user->id,$campus_id,$gmt_difference);

        //Enhanced first login
        //$userFirstLogindate = Carbon::createFromTimestamp($user->first_login_time);
        $todayTimestamp=Carbon::now()->addHours($gmt_difference)->startOfDay()->timestamp;
        if($user->first_login_time==$todayTimestamp){
            $first_login_time = true;
        }else{
            $first_login_time = false;
        }


        //Check for Crown when it's Friday
        if((Carbon::Today()->addHours($gmt_difference))->isFriday()){
            $this->lessonCommonService->CheckUserCrownAndStatus($user->id,$campus_id,$gmt_difference);
        }


        return ['active'=>$active,'count'=>$count,'xp_gained'=>20,'week_streaks'=>$week,'first_login'=>$first_login_time];
    }

    // ============== Student streaks ============== //

    public function updateStudentStreaks($user,$gainedScore,$campus_id,$gmt_difference){

        //Check if Student has a streak this day
        $userStreak = $this->professionalRepository->checkTodayStudentStreak($user->id,$gainedScore,$gmt_difference);
        
        if($userStreak){
            $userStreak = $this->professionalRepository->updateStudentStreak($userStreak,$gainedScore);

        }else{
            //create new Streak
            $userStreak = $this->professionalRepository->storeStudentStreak($user->id,$gainedScore,$campus_id);
            $count = $this->professionalRepository->getStreaksCount($user->id,$campus_id,$gmt_difference);

            //Check if It's a first streak
            if($count == 1 ){
                $this->lessonCommonService->updateCampusUserStatus($user->id,$campus_id,0,0,$userStreak->id);
            }
            
    }


        $latestStreakPeriod = $this->professionalRepository->getLatestStreakPeriod($user->id, $campus_id);
        $isCurrentStreakPeriod = $this->isCurrentStreak($latestStreakPeriod, $gmt_difference);
        
        if(!$isCurrentStreakPeriod){
            $this->lessonCommonService->updateCampusUserStatus($user->id,$campus_id,0,0,$userStreak->id);
            //Reset Old streaks
            $this->professionalRepository->resetStudentStreaks($user,$campus_id,$gmt_difference);
        }


    }    

    public function checkStreaksReset($user,$campus_id,$gmt_difference){
        $comparingDay = Carbon::today();
        $lastAddedStreak = $this->professionalRepository->getLastAddedStreak($user->id,$campus_id);
        if($lastAddedStreak){
            if($comparingDay->diffInDays(Carbon::createFromTimestamp($lastAddedStreak->date)->startOfDay()) > 1){
                //Reset Old streaks
                $this->professionalRepository->resetStudentStreaks($user,$campus_id,$gmt_difference);
          }
        }

    }


    public function getStudentStreaks($user,$round_id,$gmt_difference){
       
        $round=$this->professionalRepository->getRoundById($round_id);

        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id = $round->getCampus()->id;

        //Check Reset
        $this->checkStreaksReset($user,$campus_id,$gmt_difference);

        $showStreaks = $this->professionalRepository->checkStudentStreaksLastSeen($user,$campus_id,$gmt_difference);
        $course_firstseen = $this->professionalRepository->checkStudentCourseSeen($user,$campus_id);

        $active = $this->professionalRepository->getStreaksActiveStatus($user->id,$campus_id,$gmt_difference);

        $latestStreakPeriod = $this->professionalRepository->getLatestStreakPeriod($user->id, $campus_id);
        $isCurrentStreakPeriod = $this->isCurrentStreak($latestStreakPeriod, $gmt_difference);
        $todayUtc = Carbon::now();
        $todayLocalTimeZone = $todayUtc->addHours($gmt_difference);
        $week = [];
        $latestStreakPeriodLength = $latestStreakPeriod->count();
        // dd($latestStreakPeriodLength);
        $gainedCrown=false;
        if($isCurrentStreakPeriod){
            $streakStartDay = Carbon::createFromTimestamp($latestStreakPeriod->last()->date)->addHours($gmt_difference);
            // dd($streakStartDay);
            for($i = 0; $i < 7; $i++){
                $dayInStreak = $streakStartDay->copy()->addDays($i);
                $dayInStreakName = $dayInStreak->format('D');

                if($i < $latestStreakPeriodLength){
                    $week[$dayInStreakName] = true;
                }else{
                    $week[$dayInStreakName] = false;
                }
            }
            
        }else{
            for($i = 0; $i < 7; $i++){
                $dayInStreak = $todayLocalTimeZone->copy()->addDays($i);
                $dayInStreakName = $dayInStreak->format('D');

                $week[$dayInStreakName] = false;
            }
        }

        // Give user a crown if completed 7-day streak
        if($latestStreakPeriodLength == 7){
                //$this->lessonCommonService->updateCampusUserStatus($user->id,$campus_id,0, 1);
                $gainedCrown=true;
        }

        //Reset Streaks incase in-Active
        if(!$active){
            $this->professionalRepository->resetStudentStreaks($user,$campus_id,$gmt_difference);
        }
        
        //Enhanced first login
        $todayTimestamp = $todayLocalTimeZone->startOfDay()->timestamp;
        if($user->first_login_time == $todayTimestamp){
            $first_login_time = true;
        }else{
            $first_login_time = false;
        }
        $count = $this->professionalRepository->getStreaksCount($user->id,$campus_id,$gmt_difference);

        return ['active'=>$active,'show_streaks'=>$showStreaks,'count'=>$count,'gainedCrown'=>$gainedCrown,'week_streaks'=>$week,'course_firstseen'=>$course_firstseen,'first_login'=>$first_login_time];
    }
    public function isCurrentStreak(EntityCollection $streakPeriod, $gmt_difference){
        $todayUtc = Carbon::now();
        $todayLocalTimeZone = $todayUtc->addHours($gmt_difference);
        $streakPeriodLength = $streakPeriod->count();

        // If last streak period is less than 7 days long, and last day has gained XPs,
        if($streakPeriodLength > 0 && $streakPeriodLength <= 7){
            // then most likely today belongs to this streak period
            $lastDayInStreakLocalTimeZone = Carbon::createFromTimestamp($streakPeriod->first()->date)->addHours($gmt_difference);

            // If last day in streak is yesterday,
            if($lastDayInStreakLocalTimeZone->diffInDays($todayLocalTimeZone, false) == 1
                || $lastDayInStreakLocalTimeZone->diffInDays($todayLocalTimeZone, false) == 0){
                // then today belongs to this streak.
                return true;
            }
            // Else, if last day in streak is further than yesterday,
            elseif ($lastDayInStreakLocalTimeZone->diffInDays($todayLocalTimeZone, false) > 1){
                // then today doesn't belong to this streak.

                return false;
            }
            // Else, if last day in streak comes after today,
            elseif ($lastDayInStreakLocalTimeZone->diffInDays($todayLocalTimeZone, false) < 0){
                // then there's something wrong with dates.
                throw new BadRequestException('Error in dates ! Today comes before last day in latest streak');
            }
        }else{
            // Then today doesn't belong to this streak.
            return false;
        }
    }
    // ============== Student streaks ============== //

    public function getLessonTypeName($type)
    {
         if($type == 0)
            {
                $lessonType="normal";
            }
            elseif($type == 1)
            {
                $lessonType="project";
            }
            elseif($type == 2)
            {
                $lessonType="welcome";
            }else
            {
                $lessonType=$type;
            }

            return $lessonType;
    }

    public function getLearnerProgress($user,$student_id,$round_id,$gmt_difference){
        
        $student = $this->accountRepository->getUserById($student_id);
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $round=$this->professionalRepository->getRoundById($round_id);

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));
        
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
            
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        // $campusData = Mapper::MapEntityCollection(LearnerProgressDto::class, $test); 

        $campus = $round->getCampus();
        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);

        $learnerProgressDto = new LearnerProgressDto;

        //learner Info.
        $learner = Mapper::MapClass(LearnerDto::class, $student);
        //Rank & Level Added
        $rankLevelId = $this->professionalRepository->getUserRankLevelInCourse($student->id,$campus->id);
        $rankLevel = $this->professionalRepository->getRankLevelById($rankLevelId);

        $learner->learnerRank = $rankLevel->rank->getName();
        $learner->learnerLevel = $rankLevel->level->getName();
        
        $learnerProgress = Mapper::MapClass(LearnerProgressDto::class, $campus_round);

        $learnerProgress->course_name = $campus->getName();
        $learnerProgress->learner = $learner;

        $completedActivityProgress = $this->professionalRepository->getStudentTasksByStatus($round->campus_id,$student_id,true);
        // $inProgressActivityProgress = $this->professionalRepository->getStudentTasksByStatus($round->campus_id,$student_id,false);


        $campusRoundClass = $this->professionalRepository->getRoundClasses($campus_round->getId());

        foreach($campusRoundClass as $campusClass){
            
            $completed = [];
            

            $class = $this->professionalRepository->getCampusClassById($campusClass->class_id);
            if($class == null)
                throw new BadRequestException(trans('locale.class_not_exist'));

            if($class->getDefaultActivity() != null)
            {
                $activity = $class->getDefaultActivity();
                $is_default=true;
            }else{
                $activity = $class->getActivity();
                $is_default=false;
            }
            
            $completedActivityProgress = $this->professionalRepository->getStudentTasksInClassByStatus($round->campus_id,$student_id,true,$activity->getId(),$is_default);
            // $inProgressActivityProgress = $this->professionalRepository->getStudentTasksInClassByStatus($round->campus_id,$student_id,false,$activity->getId(),$is_default);
            
            $module = new \stdClass;
            $module->name = $class->getName();
                        
            //Completed Tasks
            if($completedActivityProgress != null){
                foreach ($completedActivityProgress as $progress) {

                    if($progress->task->getTaskNameAndType()['task_type'] != 'html_mission'){
                        if($progress->task_duration==null){
                           $timeSpent=0;
                        }
                        else{
                            $timeSpent=$progress->task_duration;
                        }
                        if($progress->no_of_trials==null){
                            $timeSpent=0;
                        }else{
                            $trialsNum=$progress->no_of_trials;   
                        }    

                        // $userLastSeen=$this->professionalRepository->getUserLastSeenTask($student_id,$round->getCampus_id(),$progress->default_activity_id,$progress->task->getId(),true);
                        // $lastSeenTime=($userLastSeen!=null)?$userLastSeen->getLastSeen():null;
                        $lastSeenTime = Carbon::parse($progress->updated_at)->addHours($gmt_difference)->timestamp;

                        $completedTask = new TaskDto;
                        $completedTask->order = $progress->task->getActivityOrder($activity->id);
                        $completedTask->task = $progress->task->getTaskNameAndType()['task_name'];
                        $completedTask->score = $progress->evaluation.' XP';
                        $completedTask->timeSpent = $timeSpent;
                        $completedTask->trialsNum = $trialsNum;
                        $completedTask->seen = $lastSeenTime;

                        array_push($completed,$completedTask);
                    }
                   
                }
                
                
            }
            //Ordering
            usort($completed, function ($task1, $task2) { 
                return $task1->order > $task2->order; 
            } ); 

            if(!empty($completed)){
                $module->tasks = $completed;
                array_push($learnerProgress->completed,$module);
            }
            
        }
        
        //InProgress
        foreach($campusRoundClass as $campusClass){
            $in_progress = [];
            $class = $this->professionalRepository->getCampusClassById($campusClass->class_id);
            
            if($class == null)
                throw new BadRequestException(trans('locale.class_not_exist'));

            if($class->getDefaultActivity() != null)
            {
                $activity = $class->getDefaultActivity();
                $is_default=true;
            }else{
                $activity = $class->getActivity();
                $is_default=false;
            }
            
            $inProgressActivityProgress = $this->professionalRepository->getStudentTasksInClassByStatus($round->campus_id,$student_id,false,$activity->getId(),$is_default);
            
            $module = new \stdClass;
            $module->id = $class->id;
            $module->name = $class->getName();
                        
            //inProgress Tasks
            if($inProgressActivityProgress != null){
                foreach ($inProgressActivityProgress as $progress) {
                
                    if($progress->task->getTaskNameAndType()['task_type'] != 'html_mission'){
                        if($progress->task_duration==null)
                           $timeSpent=0;
                        else
                           $timeSpent=$progress->task_duration;

                        if($progress->no_of_trials==null)
                           $timeSpent=0;
                        else
                           $trialsNum=$progress->no_of_trials;   


                    // $userLastSeen=$this->professionalRepository->getUserLastSeenTask($student_id,$round->getCampus_id(),$progress->default_activity_id,$progress->task->getId(),true);
                    // $lastSeenTime=($userLastSeen!=null)?$userLastSeen->getLastSeen():null;

                    $lastSeenTime = Carbon::parse($progress->updated_at)->addHours($gmt_difference)->timestamp;

                    $inProgressTask = new TaskDto;
                    $inProgressTask->task = $progress->task->getTaskNameAndType()['task_name'];
                    $inProgressTask->task_type = $progress->task->getTaskNameAndType()['task_type'];
                    $inProgressTask->order = $progress->task->getActivityOrder($activity->id);
                    $inProgressTask->course_id = $round->getCampus_id();
                    $inProgressTask->round_id = $round->getId();
                    $inProgressTask->mission_id = $progress->task->getTaskNameAndType()['mission_id'];
                    $inProgressTask->activity_id = $progress->default_activity_id;
                    $inProgressTask->learner_id = $progress->user_id;
                    $inProgressTask->submission_id = $progress->id;
                    $inProgressTask->seen = $lastSeenTime;
                    $inProgressTask->timeSpent = $timeSpent;
                    $inProgressTask->trialsNum = $trialsNum;

                    array_push($in_progress,$inProgressTask);

                    }
                }
            }
             //Ordering
            usort($in_progress, function ($task1, $task2) { 
                return $task1->order > $task2->order; 
            } ); 
            if(!empty($in_progress)){
                $module->tasks = $in_progress;
                array_push($learnerProgress->in_progress,$module);
                
            }
            

        }
        
        return $learnerProgress;
    }   

    public function studentsCompletedCourseCountCheck($account_id,$module,$start_from=null,$limit=null,$search=null,$order_by=null,$order_by_type=null,$status) {
        $completedCounter = 0;

        $students = $this->professionalRepository->getEnrolledStudentsPerCourseCount($account_id,$module->getCampus()->id,false,$limit,$search,$order_by,$order_by_type);
        // $allTasksPerCourseCount = $this->professionalRepository->getAllTasksPerModuleCount($module->getCampus()->id,$module->default_activity_id);

        $enrolledLearnersResult = [];
        
            foreach ($students as $student) {
                
                $campusRoundId = $this->professionalRepository->getLearnerCampusUser($student->id,$module->getCampus()->id)->campus_round_id;
                $campusRound = $this->professionalRepository->getCampusRoundById($campusRoundId);

                if($campusRound == null)
                    throw new BadRequestException(trans("locale.campus_round_not_exist"));

                $user = $this->accountRepository->getUserById($student->id);

                $allTasksPerCourseCount = $this->professionalRepository->getAvailaibleTasksInCampus($user,$campusRound,true);
                //Get All activity progress count for student in a module
                $progressCount = $this->professionalRepository->getProgressCountforStudentInModule($student->id,$module->getCampus()->id,$module->default_activity_id);
                    $enrolledLearner = new CourseLearnerDto;
                    $enrolledLearner->user_name = $student->username;
                    $enrolledLearner->completed = false; // Set it as default

                if($allTasksPerCourseCount == $progressCount){
                    $completedCounter++;
                    $enrolledLearner->completed = true;
                }
                array_push($enrolledLearnersResult,$enrolledLearner);
            }
            if($status){
                return $completedCounter; 
            }else{
                return $enrolledLearnersResult;
            }  
    }

    public function getCourseDetails($user,$campus_id,$start_from, $limit, $search,$order_by,$order_by_type,$count){
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $campus = $this->professionalRepository->getCampusById($campus_id);

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 
        
            if($limit != null) {
                $page = intval($start_from / $limit) + 1;
                $currentPage = $page;
                Paginator::currentPageResolver(function () use ($currentPage) {
                    return $currentPage;
                });
            }

        // $campusModules = $campus->getClasses(); //Todo : Use repo Method
        $campusModules = $this->professionalRepository->getCampusModules($campus->id, $limit, $search,$order_by,$order_by_type,$count);
        if(!$count){
            $courseDetails = new CourseDetailsDto;
            $courseDetails->course_name = $campus->getName();
            // $courseDetails->modules = $campus->getClasses();
    
            //Adding Modules
            foreach ($campusModules as $module) {
                $courseModule = new ModuleDto;
                $courseModule->name = $module->getName();
                $courseModule->id = $module->getId();
                $courseModule->missions_count = $this->professionalRepository->getMissionPerModuleCount($module->default_activity_id);
                $courseModule->submodules_count = $module->getSubmodules()->count();
                $courseModule->learners_all_missions_count = $this->professionalRepository->getEnrolledStudentsPerCourseCount($user->account_id,$module->getCampus()->id,true);
                $courseModule->learners_solved_missions_count = $this->studentsCompletedCourseCountCheck($user->account_id,$module,$start_from,$limit,$search,$order_by,$order_by_type,true);
                array_push($courseDetails->modules,$courseModule);
            }
    
            return $courseDetails;
        }else{
            return count($campusModules);
        }

    }  

    public function getCourseLearners($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count){
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $campus = $this->professionalRepository->getCampusById($campus_id);
        $modulesIds = [];
        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 

        $module = $this->professionalRepository->getCampusClassById($module_id);
       
        if($module == null)
            throw new BadRequestException(trans('locale.class_not_exist'));  
        
        $campusModules = $this->professionalRepository->getCampusClasses($campus_id)->toArray();    
        foreach($campusModules as $campusModule){ array_push($modulesIds,$campusModule['id']); }
        
        if(!in_array($module_id,$modulesIds))
            throw new BadRequestException(trans('locale.class_not_belong to_to_course'));  
 
            if($limit != null) {
                $page = intval($start_from / $limit) + 1;
                $currentPage = $page;
                Paginator::currentPageResolver(function () use ($currentPage) {
                    return $currentPage;
                });
            }

            //Course Info section
            $courseLearners = new CourseLearnersDto;
            $courseLearners->course_name = $campus->getName();
            //Adding Learners with status section
            $enrolledLearnersResult = $this->studentsCompletedCourseCountCheck($user->account_id,$module,$start_from,$limit,$search,$order_by,$order_by_type,false);
            $courseLearners->learners = $enrolledLearnersResult;
        if(!$count){
            return $courseLearners;
        }else{
            return count($enrolledLearnersResult);
        }        

    }

    public function getRoundLearnersProgress($user,$round_id,$start_from, $limit, $search,$order_by,$order_by_type,$count){
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $round=$this->professionalRepository->getRoundById($round_id);
    
        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));

     
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $campus = $this->professionalRepository->getCampusById($round->campus_id);

        $roundLearners = $this->professionalRepository->getRoundLearners($round_id, $limit, $search,$order_by,$order_by_type,false);
        if(!$count){
            $learnersProgress = new LearnersProgressDto;
            $learnersProgress->course_name = $campus->getName();
            $learnersProgress->round_name = $round->round_name;
    
            foreach($roundLearners as $learner){
                $campusUserStatus = $this->professionalRepository->getCampusUserStatus($round->campus_id,$learner->user_id);
                $learnerInst = $this->accountRepository->getUserById($learner->user_id);
                $newLearner = new SingleLearnerDto;
                $progress = $this->lessonCommonService->getLearnerCampusProgress($learnerInst->getId(),$round);

                $newLearner->id = $learner->getUser()->id;
                $newLearner->user_name = $learner->getUser()->username;
                $newLearner->xp = $campusUserStatus ? $campusUserStatus->xp : null ;
                $newLearner->hideBlocks = $campusUserStatus ? $campusUserStatus->hide_blocks ? true:false : null ;
                // $newLearner->course_missions_count = $this->professionalRepository->getAvailaibleTasksInCampus($learnerInst,$round,true);
                // $newLearner->solved_missions_count = $this->professionalRepository->getSolvedTasksInRound($round->campus_id,$learnerInst->getId());
                $newLearner->course_missions_count = $progress['all'] ;
                $newLearner->solved_missions_count = $progress['solved'];
                $newLearner->progress = $progress['progress'];
                array_push($learnersProgress->learners,$newLearner);
            }
            return $learnersProgress;
        }else{
            return count($roundLearners);
        }

    }

    public function updateHideBlocks($user,$learner_id,$round_id){
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $learner = $this->accountRepository->getUserById($learner_id);
        $round=$this->professionalRepository->getRoundById($round_id);
        
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus=$round->getCampus();

        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));


        if($learner == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        //Get learner status
        $learnerStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$learner->id);
        
        if($learnerStatus == null)
            throw new BadRequestException(trans('locale.user_status_not_exist'));

        $toggleHideBlocks = $this->professionalRepository->toggleHideBlocks($learnerStatus);  
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($round->campus_id,$learner->id); 

        $newLearner = new SingleLearnerDto;

        $newLearner->id = $learner->id;
        $newLearner->user_name = $learner->username;
        $newLearner->xp = $campusUserStatus ? $campusUserStatus->xp : null ;
        $newLearner->hideBlocks = $campusUserStatus ? $campusUserStatus->hide_blocks ? true:false : null ;
        $newLearner->course_missions_count = $this->professionalRepository->getAvailaibleTasksInCampus($learner,$round,true);
        $newLearner->solved_missions_count = $this->professionalRepository->getSolvedTasksInRound($round,$learner);

        return $newLearner;
    }

    public function bulkUpdateHideBlocks($user,$learners,$round_id){
        $loggedUser = $this->accountRepository->getUserById($user->id);

        $round=$this->professionalRepository->getRoundById($round_id);
        
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus=$round->getCampus();

        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));

        //Check all learners & update bulk of learners
        foreach($learners as $learner){
            $learnerInst = $this->accountRepository->getUserById($learner['id']);
            if($learnerInst == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            //Get learner status
            $learnerStatus = $this->professionalRepository->getCampusUserStatus($campus->id,$learner['id']); 
            $learnerStatus->hide_blocks = $learner['status'];
            $this->professionalRepository->storeCampusUserStatus($learnerStatus);
            
        }

        return 'Updated Successfully';
    }

    public function getCourseModuleDetails($user,$campus_id,$module_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false){
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $campus = $this->professionalRepository->getCampusById($campus_id);
        $modulesIds = [];
        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 

        $module = $this->professionalRepository->getCampusClassById($module_id);

        if($module == null)
            throw new BadRequestException(trans('locale.class_not_exist'));  

        $campusModules = $this->professionalRepository->getCampusClasses($campus_id)->toArray();    
        foreach($campusModules as $campusModule){ array_push($modulesIds,$campusModule['id']); }

        if(!in_array($module_id,$modulesIds))
             throw new BadRequestException(trans('locale.class_not_belong to_to_course'));  

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $students = $this->professionalRepository->getEnrolledStudentsPerCourseCount($user->account_id,$campus->getId(),true);

        $moduleMissions = [];
        $moduleTasks= $this->professionalRepository->getCampusActivityTasks($module->default_activity_id,$campus->getId(),true,$limit, $search , $order_by , $order_by_type );
        $moduleMission=[];
        foreach($moduleTasks as $moduleTask)
        {
            $task=$this->journeyRepository->getTaskById($moduleTask->task_id);
            $learnersCompletedMission=$this->professionalRepository->getAllStudentsSolvingTaskInCampus($user->account_id,$campus->getId(), $module->default_activity_id, $moduleTask->task_id);
            $completedPercentage=floor((count($learnersCompletedMission)/$students)*100) . '%';

            if($task->getMissions()->first() != null) {
                $mission = $task->getMissions()->first();

                array_push($moduleMission, [
                    'id' => $task->getId(),
                    'mission_id' => $mission->getId(),
                    'name' => $mission->getName(),
                    'type' => 'mission',
                    'all_learners_count' => $students,
                    'learners_completed_mission_count' =>count($learnersCompletedMission),
                    'completed_percentage' => $completedPercentage
                ]);
            }
            elseif($task->getMissionsHtml()->first()!=null) {
                $mission = $task->getMissionsHtml()->first();
                if($mission->getIsMiniProject()){
                    array_push($moduleMission, [
                        'id' => $task->getId(),
                        'mission_id' => $mission->getId(),
                        'name' => $mission->getTitle(),
                        'type' => 'mini_project',
                        'all_learners_count' => $students,
                        'learners_completed_mission_count' =>count($learnersCompletedMission),
                        'completed_percentage' => $completedPercentage
                    ]);
                }else{
                    array_push($moduleMission, [
                        'id' => $task->getId(),
                        'mission_id' => $mission->getId(),
                        'name' => $mission->getTitle(),
                        'type' => 'html_mission',
                        'all_learners_count' => $students,
                        'learners_completed_mission_count' =>count($learnersCompletedMission),
                        'completed_percentage' => $completedPercentage
                    ]);
                }
            }
            elseif($task->getQuizzes()->first()!=null) {
                $quiz = $task->getQuizzes()->first();

                array_push($moduleMission, [
                    'id' => $task->getId(),
                    'quiz_id' => $quiz->getId(),
                    'name' => $quiz->getTitle(),
                    'type' => $quiz->getType()->getName(),
                    'all_learners_count' => $students,
                    'learners_completed_mission_count' =>count($learnersCompletedMission),
                    'completed_percentage' => $completedPercentage
                ]);
            }
            elseif($task->getMissionsCoding()->first()!=null){
                $codingMission=$task->getMissionsCoding()->first();

                array_push($moduleMission, [
                    'id' => $task->getId(),
                    'mission_id' => $codingMission->getId(),
                    'name' => $codingMission->getTitle(),
                    'type' => 'coding_mission',
                    'all_learners_count' => $students,
                    'learners_completed_mission_count' =>count($learnersCompletedMission),
                    'completed_percentage' => $completedPercentage
                ]);
            }
            elseif($task->getMissionsEditor()->first()!=null){
                $editorMission=$task->getMissionsEditor()->first();

                array_push($moduleMission, [
                    'id' => $task->getId(),
                    'mission_id' => $editorMission->getId(),
                    'name' => $editorMission->getTitle(),
                    'type' => 'editor_mission',
                    'all_learners_count' => $students,
                    'learners_completed_mission_count' =>count($learnersCompletedMission),
                    'completed_percentage' => $completedPercentage
                ]);
            }
        }

        $submodules = $this->professionalRepository->getModuleSubmodules($module->id,$limit,$search,$order_by,$order_by_type,$count);
        $submodulesData = [];
        foreach ($submodules as $submodule) {
            $students = $this->professionalRepository->getEnrolledStudentsPerSubmodule($user->account_id, $campus->getId(), $module_id, $submodule->getId());
            $tasksIds = $submodule->getTasks()->pluck('id');
            $defaultActivityId = $module->getDefaultActivity()->getId();
            $learnersCompletedSubModuleCount = 0;
            foreach ($students as $student) {
                $checkSolvedAllTasksFlag = $this->professionalRepository->checkStudentSolvedAllSubModuleTasks($student->id, $defaultActivityId, $tasksIds);
                if($checkSolvedAllTasksFlag)
                    $learnersCompletedSubModuleCount++;
            }
            $completedPercentage=floor( $learnersCompletedSubModuleCount / count($students)  * 100 ) . '%';

            array_push($submodulesData, [
                'id' => $submodule->getId(),
                'name' => $submodule->getName(),
                'all_learners_count' => count($students),
                'learners_completed_submodule_count' => $learnersCompletedSubModuleCount,
                'completed_percentage' => $completedPercentage,
                'missions_count' => count($submodule->getTasks())
            ]);
        }

        $moduleMissions["id"]=$module_id;
        $moduleMissions["module_name"]=$module->getName();
        $moduleMissions["mission"]=$moduleMission;
        $moduleMissions["submodules"]=$submodulesData;
        
        if($count == true)
            return count($moduleMissions["mission"]);
        else
            return $moduleMissions;
    }

    public function getModuleMissionLearners($user,$campus_id,$module_id,$task_id,$start_from, $limit, $search,$order_by,$order_by_type,$count=false){
        $loggedUser = $this->accountRepository->getUserById($user->id);
        $campus = $this->professionalRepository->getCampusById($campus_id);
        $modulesIds = [];
        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$loggedUser) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$loggedUser))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); 

        $module = $this->professionalRepository->getCampusClassById($module_id);

        if($module == null)
            throw new BadRequestException(trans('locale.class_not_exist'));  

        $campusModules = $this->professionalRepository->getCampusClasses($campus_id)->toArray();    
        foreach($campusModules as $campusModule){ array_push($modulesIds,$campusModule['id']); }

        if(!in_array($module_id,$modulesIds))
            throw new BadRequestException(trans('locale.class_not_belong_to_course'));  

        $task = $this->journeyRepository->getTaskById($task_id);
        if($task == null)
            throw new BadRequestException(trans('locale.task_not_exist'));

        $CampusActivityTask = $this->professionalRepository->getCampusTaskByActivityId($campus->getId(),$module->default_activity_id,$task->getId(),true);
        if($CampusActivityTask == null)
            throw new BadRequestException(trans('locale.mission_not_belong_to_module'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $students = $this->professionalRepository->getEnrolledStudentsPerCourseCount($user->account_id,$campus->getId(),false,$limit,$search,$order_by,$order_by_type);

        $enrolledLearnersResult = [];
        foreach ($students as $student) {

            $enrolledLearner = new CourseLearnerDto;
            $enrolledLearner->user_name = $student->username;
            $checkTaskSolved= $this->professionalRepository->checkIfStudentSolveTask($student->id,$campus->getId(), $module->default_activity_id, $task->getId());
            if($checkTaskSolved == null){
               $enrolledLearner->completed = false;
            }else{
                $enrolledLearner->completed = true;
            }
            array_push($enrolledLearnersResult,$enrolledLearner);
        }

        $moduleMissionLearners = new ModuleMissionLearnersDto;
        $moduleMissionLearners->id = $module->getId();
        $moduleMissionLearners->module_name = $module->getName();
        $moduleMissionLearners->learners = $enrolledLearnersResult;

        
        if($count == true)
            return count($enrolledLearnersResult);
        else
            return $moduleMissionLearners;
    }

    public function getStudentCampusStatusData($user,$round_id){
        $student = $this->accountRepository->getUserById($user->id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user->id);
        $rankLevel = $this->professionalRepository->getRankLevelById($campusUserStatus->rank_level_id);
        
        $data = new CampusUserStatusDto;

        $data->campus_id = $campusUserStatus->id;
        $data->rank = $rankLevel->rank->getName();
        $data->level = $rankLevel->level->getName();
        $data->xp = $campusUserStatus->xp;
        return $data;
    }

    public function getStudentCampusScoreStatistics($user,$round_id){
        $student = $this->accountRepository->getUserById($user->id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user->id);
        $rankLevel = $this->professionalRepository->getRankLevelById($campusUserStatus->rank_level_id);
        $studentsNum= $round->getNoOfStudents();
        $campusStudents= $round->getStudents();
        $usersXps=0;
        
        foreach( $campusStudents as $student)
        {
            $userStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$student->id);
            if($userStatus)
              $usersXps += $userStatus->xp;
        }

        $average_xp= $usersXps / $studentsNum;
        $max_xp=$round->getCampus()->getXps();
        $current_xp=$campusUserStatus->getXps();
        $countRankLevels=$this->professionalRepository->countRankLevels();
        $levelValue=$max_xp / $countRankLevels;
        // $countSpecificLevels = $this->professionalRepository->countLevelsByRankId($rankLevel->rank_id);
        $maxRankLevel = $this->professionalRepository->getMaxRankLevel($rankLevel->rank_id);
        $ranksValue=$levelValue*$maxRankLevel;
        $xpsLeft=$ranksValue - $current_xp;
        $nextRankRecord = $this->professionalRepository->getNextRank($rankLevel->rank_id);
        if($nextRankRecord == null)
            $nextRank = null;
        else
            $nextRank =$nextRankRecord->getName();

        $data = [];
        $data['max_xp'] = $max_xp;
        $data['current_xp'] = $current_xp;
        $data['average_xp'] = $average_xp;
        $data['xps_left'] = $xpsLeft;
        $data['next_rank'] = $nextRank;

        return $data;
    }
    
    public function getAllStudentCampusStatusData($user){
        $student = $this->accountRepository->getUserById($user->id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        
        $allCampusUserStatus = $this->professionalRepository->getAllCampusUserStatus($user->id);
        $allCampusUserStatusArr=[];
        foreach ($allCampusUserStatus as $key => $campusUserStatus) {
            $rankLevel = $this->professionalRepository->getRankLevelById($campusUserStatus->rank_level_id);
        
            $data = [];

            $data['campus_id'] = $campusUserStatus->campus_id;
            $data['rank'] = $rankLevel->rank->getName();
            $data['level'] = $rankLevel->level->getName();
            $data['xp'] = $campusUserStatus->xp;
            $data['crowns'] = $campusUserStatus->streak_xp;

            $allCampusUserStatusArr[]=$data;

        }
        return $allCampusUserStatusArr;
    }

    private function handleUserSuccessInTask(CampusActivityProgress $campusActivityProgress, $duration = null, $noOfBlocks = null){
        if(!$campusActivityProgress->getFirstSuccess()){
            $noOfTrials = $campusActivityProgress->getNoTrials();
            if($noOfTrials > 0)
                $campusActivityProgress->setFirstSuccess($noOfTrials);
        }

        $bestTimeTaken = $campusActivityProgress->getBestTaskDuration();
        if($bestTimeTaken != null){
            if($duration != null && !empty($duration) && $duration < $bestTimeTaken){
                $campusActivityProgress->setBestTaskDuration($duration);
            }
        }else{
            if(!empty($duration))
            $campusActivityProgress->setBestTaskDuration($duration);
        }
        


        $bestNoOfBlocks = $campusActivityProgress->getBestBlocksNumber();
        if($bestNoOfBlocks != null){
            if($noOfBlocks != null && $noOfBlocks < $bestNoOfBlocks){
                $campusActivityProgress->setBestBlocksNumber($noOfBlocks);
            }
        }else{
            $campusActivityProgress->setBestBlocksNumber($noOfBlocks);
        }

        return $campusActivityProgress;
    }

    public function cleanDropdown($user,$delete){
        $globalCounter = 0;
        $dropdownTranslations = $this->professionalRepository->getAllDropdownTranslations();
        $returnedData=[];
        foreach($dropdownTranslations as $key=>$dropdownTranslation){
            $dropdownId = $dropdownTranslation->dropdown_question_id;
            //Get Question Id
            $dropdownQuestion = $this->professionalRepository->getDropdownQuestionById($dropdownId);
            $question= $this->journeyRepository->getQuestionById($dropdownQuestion->question_id);
            $allDropdowns = $question->getDropdownQuestion()->getDropdown();
            $all_pattern="/;;;;(.*?);;;;/sm";
            preg_match_all($all_pattern, $dropdownTranslation->title_description, $all_match );
            $confirmedDropdownsIds = $all_match[1];
            $unConfirmedDropdownsIds=[];
            $localDeleteCOunter=0;
            foreach($allDropdowns as $dropdown){
                if(!in_array($dropdown->id,$confirmedDropdownsIds)){
                    //Delete old dropdown
                    if($delete==true)
                        $this->professionalRepository->deleteDropdown($dropdown);
                    $unConfirmedDropdownsIds[]=$dropdown->id;
                    $localDeleteCOunter ++;
                    $globalCounter ++;
                }
            }

            $returnedData[$key]=["confirmedDropdownsIds"=>$confirmedDropdownsIds,"unConfirmedDropdownsIds"=>$unConfirmedDropdownsIds,"deleteCounter"=>$localDeleteCOunter];

        }
        
        return ["returnedDatat"=>$returnedData,"CountOfDeletes"=>$globalCounter];
    }


    public function progressFix($user,$fix=false){
        
        $allProgress = $this->professionalRepository->getAllProgress();
        $counter = 0;
        foreach($allProgress as $progress){
            $learner = $this->accountRepository->getUserById($progress->user_id);
            $showBlocks = $learner->showBlocks($progress->campus_id);
            $currentScore = $progress->score;
            $campusActivityTask = $this->professionalRepository->getCampusActivityTask($progress->campus_id, $progress->task_id);
            $fullMark = $campusActivityTask->getTaskFullMark(true, $showBlocks);
            
            if($fullMark){
                if($currentScore > $fullMark){
                    if($fix){
                        $progress->score = $fullMark;
                        $progress->evaluation = $fullMark;
                        $this->professionalRepository->storeCampusActivityProgress($progress);
                    }
                    $counter++;
                }
            }
        }
        return $counter.' progress affected.';
    }

   public function lastSeenFix($user,$fix=false){
       $allLastSeen = $this->professionalRepository->getAllLastSeen();
       $counter = 0;
       foreach($allLastSeen as $lastSeen){
           $user_id = $lastSeen->user_id;
           $campus_id = $lastSeen->campus_id;
           $activity_id = $lastSeen->default_activity_id;
           $task = $this->journeyRepository->getTaskById($lastSeen->task_id);
           $learner = $this->accountRepository->getUserById($lastSeen->user_id);
           $campus = $this->professionalRepository->getCampusById($campus_id);

            if($campus && $task && $learner){
           //Check if there is a Progress recored.
           $progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus_id,$activity_id,true);
        
           if(!$progress){
               //Create a new campusActivityProgress
                if($fix == true){
                    $activity=$this->journeyRepository->getDefaultActivityById($lastSeen->default_activity_id);
                    $missionProgress = new CampusActivityProgress($task, $learner, $campus,0,0,0);
                    $missionProgress->setDefaultActivity($activity);
                    
                    $this->professionalRepository->storeRoundActivityProgress($missionProgress);
                }
               $counter++;
           }
        }


       }
       return $counter.' progress created.';

   } 

   public function checkEditorMissionBug($user,$fix){

    $editorMissions = $this->professionalRepository->getEditorMissions();

    $counter = 0;
    foreach($editorMissions as $mission){
        $quiz_id = $mission->quiz_id;
        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz){
            $questionsByQuiz = $this->journeyRepository->getQuestionsByQuiz($quiz);

            if(count($questionsByQuiz) > 1){
    
                $counter++;
            }
        }
    }
    return $counter.' editor missions needs to be fixed';
    
}

public function pythonFix($user,$fix){
    
    $pythonMissions = $this->professionalRepository->getAllMissionCoding('python');

    $initCounter = 0;
    $modelAnswerCounter = 0;

    //Check if code has main() call at the end
    foreach($pythonMissions as $mission){
        $initCode = $mission->code;
        $modelAnswerCode = $mission->model_answer;

        //Check Init Code 
        if( (trim(substr($initCode,-10)) != 'main()') ){

            if($fix == true){
                //Add main() at the end of code
                $initCode .= PHP_EOL.'main()'.PHP_EOL;
                $mission->code = $initCode;

                $this->professionalRepository->storeMissionCoding($mission);
            }
            $initCounter++;
        }

        //Check Model answer Code 
        if( (trim(substr($modelAnswerCode,-10)) != 'main()') ){
            if($fix == true){
                //Add main() at the end of code
                $modelAnswerCode .= PHP_EOL.'main()'.PHP_EOL;
                $mission->model_answer = $modelAnswerCode;

                $this->professionalRepository->storeMissionCoding($mission);
            }
            $modelAnswerCounter++;
        }
    }

    return ['Fixed'=>$fix,'InitCount'=>$initCounter,'ModelAnswerCounter'=>$modelAnswerCounter];
    
}

    public function evalFix($user,$fix){
        $allProgress = $this->professionalRepository->getAllProgress();
        $counter = 0;
        foreach($allProgress as $progress){         
            if(!$progress->evaluation && $progress->score > 0){
                if($fix == true){
                    $progress->evaluation = $progress->score;
                    $this->professionalRepository->storeRoundActivityProgress($progress);

                }
                $counter++;
            }
        }

        return $counter.' progress affected.';
    }

    public function createSearchCoursesVault(){
        
        //Get all courses
        $courses = $this->professionalRepository->getAllCampuses();
        $coursesCounter = 0;
        foreach($courses as $course){
            $courseName = $course->getName();
            $courseDesc = strip_tags($course->getDescription());
            //Create Course record
                //Search Vault record 
                $vault = new SearchVault($courseName,'course',$courseDesc,$course->id,null,null,null);
                $this->professionalRepository->storeSearchVault($vault);

            $courseModules = $course->getClasses();            
            foreach($courseModules as $module){
                $moduleName = $module->getName();
                $moduleDesc = strip_tags($module->getDescription());           
                $activity = $module->getDefaultActivity();
                
                //Search Vault record 
                $vault = new SearchVault($moduleName,'module',$moduleDesc,$course->id,$activity->id,null,null);
                $this->professionalRepository->storeSearchVault($vault);
            }
            $coursesCounter++;
        }

        return 'Done, with '.$coursesCounter.' courses affected.';
    }

    public function createSearchVault($course_id){
        
        //Get Course tasks 
        $course = $this->professionalRepository->getCampusById($course_id);
        $courseModules = $course->getClasses();
        $tasksCounter = 0;
        
        foreach($courseModules as $module){
            $activity = $module->getDefaultActivity();
            $tasks = $activity->getTasks();
           
            foreach($tasks as $task){
                $task_type = $task->getTaskNameAndType()['task_type'];
                $task_title = $task->getTaskNameAndType()['task_name'];
                $desc = '';
            //Types -> angular_mission / emulator / editor_mission / coding_mission / quiz / mission / mini_project
            // =============== Quiz/Emulator ================//
                if($task_type == 'quiz' || $task_type == 'emulator'){
                    // Save quiz title and questions head
                    $quiz = $task->getQuizzes()->first();
                    $questions = $this->journeyRepository->getQuestionsByQuiz($quiz);

                        //Save each quation record
                        foreach($questions as $question){
                            $question_body = strip_tags($question->getBody(),'<xmp>');
                            $desc .= $question_body.'...';
                        }
                     //Search Vault record 
                    $vault = new SearchVault($task_title,$task_type,$desc,$course_id,$activity->id,$task->id,null);
                    $this->professionalRepository->storeSearchVault($vault);
                }
            // =============== Blockly ================//
                elseif($task_type == 'mission'){
                    $mission = $task->getMissions()->first();
                    $desc = strip_tags($mission->getDescription(),'<xmp>');
                    //Search Vault record 
                    $vault = new SearchVault($task_title,$task_type,$desc,$course_id,$activity->id,$task->id,null);
                    $this->professionalRepository->storeSearchVault($vault);
                }

                elseif($task_type == 'editor_mission' || $task_type == 'angular_mission' || $task_type == 'coding_mission'){
                    switch ($task_type) {
                        case "editor_mission":
                            $mission = $task->getMissionEditor();
                            break;
                        case "angular_mission":
                            $mission = $task->getMissionAngular();
                            break;
                        case "coding_mission":
                            $mission = $task->getMissionCoding();
                            break;
                    }

                    $desc = strip_tags($mission->getDescription());
                    //Search Vault record 
                    $vault = new SearchVault($task_title,$task_type,$desc,$course_id,$activity->id,$task->id,null);
                    $this->professionalRepository->storeSearchVault($vault);
                }
                elseif($task_type == 'html_mission' || $task_type == 'mini_project'){
                    //Create first record for mission title
                    $vault_title = new SearchVault($task_title,$task_type,null,$course_id,$activity->id,$task->id,null);
                    $this->professionalRepository->storeSearchVault($vault_title);
                    //Create records for sections
                    $mission = $task->getMissionsHtml()->first();
                    $sections = $mission->getSections();

                    foreach($sections as $section){
                        $section_title = $section->getTitle();
                        $section_steps = $section->getSteps();
                        $section_content = '';
                        foreach($section_steps as $step){
                            $section_content .= strip_tags($step->getContent(),'<xmp>').'...';
                        }
                        $section_content = trim(preg_replace('/\s+/', ' ', $section_content));
                        //Save section record
                        $vault_title = new SearchVault($section_title,$task_type,$section_content,$course_id,$activity->id,$task->id,$section->id);
                        $this->professionalRepository->storeSearchVault($vault_title);
                    }
                }
                else{
                    dd($task_type.' not handled');
                }

                $tasksCounter++;
            }
        }
      
        return 'Done, with '.$tasksCounter.' tasks affected.';
    }
    
    private function isLockedSubmodule($user,$round,$module,$submodule) {
        if(!$user->isStudent())
            return false;
        //Check if first submodule -->locked=true
        //Check if not first submodule -->check previous module locked or not(all missions are solved)
        $firstSubmoduleInModule=$this->professionalRepository->getFirstSubmoduleInModule($module);
        if($submodule->getId() !== $firstSubmoduleInModule->getId()){
            $previousSubModule=$this->professionalRepository->getSubmoduleInModuleByOrder($module,intval($submodule->getOrder())-1);
            $hasFinishedSubmoduleTasks=$this->professionalRepository->hasFinishedSubmoduleTasks($user,$round->getCampus_id(),$previousSubModule);
            return !$hasFinishedSubmoduleTasks;

        }else{
            return false;
        }
        
    }
    private function getSubmoduleProgress($user,$round,$module,$submodule) {
        if(!$user->isStudent())
            return null;
        //Check if first submodule -->locked=true
        //Check if not first submodule -->check previous module locked or not(all missions are solved)
        $mainTasksCount=$this->professionalRepository->getSubmoduleMainTasks($submodule,true);
        $userSolvedMainTasksCount=$this->professionalRepository->getUserSolvedSubmoduleMainTasks($user,$round,$submodule,true);
        if($module->lessonType == 2){
            $welcomeModule = $this->professionalRepository->getIntroDetails($round->getId());
            if($welcomeModule && $welcomeModule->introductory_video_url !=null){
                $userSolvedMainTasksCount+=1;
                $mainTasksCount+=1;
            }
        }

        if($mainTasksCount!==0)
            return round(($userSolvedMainTasksCount/$mainTasksCount)*100);
        return 0;
        
        
    }
    
    public function isPassedSubmodule($user,$round,$module,$submodule){
        if(!$user->isStudent())
            return true;
        
        return $this->professionalRepository->hasFinishedSubmoduleTasks($user,$round->getCampus_id(),$submodule);
        
    }
    public function getModuleSubModules($user_id,$round_id,$module_id){

        $user = $this->accountRepository->getUserById($user_id);
         if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $round = $this->professionalRepository->getCampusRoundById($round_id);

        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        //TODO:CHECK IF USER IS IN ROUND
        $module=$this->professionalRepository->getCampusClassById($module_id);
        $campusRoundClass=$this->professionalRepository->getCampusRoundClass($round_id, $module_id);
        //$moduleMapped=Mapper::MapClass(CampusModuleDto::class,$module,[SubmoduleDto::class]);
        $moduleMapped=Mapper::MapClass(CampusModuleDto::class,$module);
        //dd($campusRoundClass);
        $moduleMapped->materials=Mapper::MapEntityCollection(MaterialDto::class,$campusRoundClass->getMaterials());
        foreach ($module->getSubmodules() as $key => $submodule) {
            $submoduleMapped=Mapper::MapClass(SubmoduleDto::class,$submodule);
            $submoduleMapped->locked=$this->isLockedSubmodule($user,$round,$module,$submodule);
            $submoduleMapped->passed=$this->isPassedSubmodule($user,$round,$module,$submodule);
            $moduleMapped->submodules[]=$submoduleMapped;
        }

        return $moduleMapped;

    }
    public function getSubModuleTasks($user_id,$round_id,$module_id,$sub_module_id){
        $user = $this->accountRepository->getUserById($user_id);
         if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $round = $this->professionalRepository->getCampusRoundById($round_id);

        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $module=$this->professionalRepository->getCampusClassById($module_id);
        //TODO:CHECK IF USER IS IN ROUND
        //TODO:CHECK USER IS ALLOWED TO VIEW THIS SUBMODULE
        
        $module=$this->professionalRepository->getCampusClassById($module_id);
        $submodule=$this->professionalRepository->getSubmoduleById($sub_module_id);

        $allowedToViewSubModule=!$this->isLockedSubmodule($user,$round,$module,$submodule);
        if($allowedToViewSubModule === false)
            throw new BadRequestException(trans('locale.not_authorized'));

        $activity=$module->getDefaultActivity();
        $tasks=$submodule->getTasks();
        $campus_id=$round->getCampus_id();
        $is_default=true;
        $mainTasksDto=[];
        $boostersTasksDto=[];
        $submoduleMapped=Mapper::MapClass(SubmoduleDto::class,$submodule);
        $submoduleMapped->progress=$this->getSubmoduleProgress($user,$round,$module,$submodule);

        $welcomeModules=$module->getCampusWelcomeModules();
        if($welcomeModules!==null && $submodule->getOrder()==1){//TODO:check for module type
            $campusUser = $this->professionalRepository->getCampusRoundUser($round->getId(),$user_id);
            $hasSeen=($campusUser->getHasSeen() ==1)?true:false;
            

            array_push($mainTasksDto, [
                'videoUrl' => $welcomeModules->getIntroductoryVideoUrl(),
                'image' => 'https://robogarden-professional.s3-us-west-2.amazonaws.com/Roadmap-Images/Welcome-Video.png',
                'type' => 'video',
                'watched' =>$hasSeen,
                'last_seen'=>'completed',
                'passed'=>$hasSeen,
                'locked'=>false,
                'time_estimate' => null,
                'xps' => null
            ]);
        }

        foreach ($tasks as $task){
           
            $previous_progress=$this->professionalRepository->getUserRoundTaskProgressActivity($task,$user_id,$campus_id,$activity->getId(),$is_default);
            $lastSeenTime=($previous_progress!=null)?$previous_progress:null;

            $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
            $difficulty =  $this->professionalRepository->getTaskDifficulty($task->getId(),$activity->getId());
            $difficulty_title=($difficulty)?$difficulty->getTitle():null;
            
            if($previous_progress!=null)
                $passed=$previous_progress->getFirstSuccess()!==null?true:false;
            else
                $passed = 0;

            if($lastSeenTime!==null){
                if(!$passed)
                    $lastSeenTime="in_progress";
                else
                    $lastSeenTime="completed";
            }
            $commonTaskAttribute=[
                        'passed'=>$passed,
                        'last_seen'=>$lastSeenTime,
                        'time_estimate' => $activityTask->getTimeEstimate(),
                        'xps' => $activityTask->getXps(),
                        'difficulty'=>$difficulty_title ];
            if($task->getMissions()->first() != null ) {
                
                $mission = $task->getMissions()->first();
                $taskDto=array_merge($commonTaskAttribute,[
                        'id' => $task->getId(),
                        'name' => $mission->getName(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'mission',
                        'mission_id' => $mission->getId()                                            
                    ]);
                if($activityTask->booster_type != 1)
                    array_push($mainTasksDto, $taskDto);
                else
                    array_push($boostersTasksDto, $taskDto);

            }
            elseif($task->getMissionsHtml()->first() != null) {
                $mission = $task->getMissionsHtml()->first();

                if( $mission->getIsMiniProject()==0) {
                    $handoutMissionFlag = $mission->htmlMissionType() == 'handout';
                    $type = ($handoutMissionFlag) ? 'handout' : 'html_mission';
                    $taskDto= array_merge($commonTaskAttribute,[
                            'id' => $task->getId(),
                            'name' => $mission->getTitle(),
                            'image' => $mission->getIconUrl(),
                            'type' => $type,
                            'mission_id' => $mission->getId(),
                            'with_steps'=>$mission->getWithSteps()==1?true:false,
                        ]);
                    if(!$handoutMissionFlag)
                        $taskDto['lesson_type'] = $mission->getLessonType();
                    
                    if($activityTask->booster_type != 1)
                        array_push($mainTasksDto, $taskDto);
                    else
                        array_push($boostersTasksDto, $taskDto);
                }elseif($mission->getIsMiniProject()){
                    $taskDto=array_merge($commonTaskAttribute,[
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'mini_project',
                        'mission_id' => $mission->getId(),
                        'with_steps'=>$mission->getWithSteps()==1?true:false]);
                    
                    if($activityTask->booster_type != 1)
                        array_push($mainTasksDto, $taskDto);
                    else
                        array_push($boostersTasksDto, $taskDto);

                }
            }
            elseif($task->getMissionCoding() != null) {
                
                $mission = $task->getMissionCoding();
                $taskDto=array_merge($commonTaskAttribute,[
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'model_answer' => $mission->getModelAnswer(),
                        'type' => 'coding_mission',
                        'code' => $mission->getCode(),
                        'mission_id' => $mission->getId()                        
                    ]);
                if($activityTask->booster_type != 1)
                        array_push($mainTasksDto, $taskDto);
                else
                    array_push($boostersTasksDto, $taskDto);
            }
            elseif($task->getMissionEditor() != null) {
                
                $mission = $task->getMissionEditor();
                $taskDto=array_merge($commonTaskAttribute,[
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'model_answer' => $mission->getModelAnswer(),
                        'type' => 'editor_mission',
                        'mode' => $mission->getType(),
                        'html' => $mission->getHtml(),
                        'css' => $mission->getCss(),
                        'js' => $mission->getJs(),
                        'mission_id' => $mission->getId()
                    ]); 
                if($activityTask->booster_type != 1)
                        array_push($mainTasksDto, $taskDto);
                else
                    array_push($boostersTasksDto, $taskDto);    
            }
            elseif($task->getMissionAngular() != null) {
                
                $mission = $task->getMissionAngular();
                $missionAngularFilesDto=Mapper::MapEntityCollection(MissionAngularFilesDto::class, $mission->getFiles());
                $taskDto=array_merge($commonTaskAttribute,[
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'type' => 'angular_mission',
                        'files' => $missionAngularFilesDto,
                        'mission_id' => $mission->getId()
                    ]);
                if($activityTask->booster_type != 1)
                        array_push($mainTasksDto, $taskDto);
                else
                    array_push($boostersTasksDto, $taskDto);   
            }
            elseif($task->getQuizzes()->first()!=null){
                
                $quiz = $task->getQuizzes()->first();
                $taskDto=array_merge($commonTaskAttribute,[
                        'id' => $task->getId(),
                        'name' => $quiz->getTitle(),
                        'image' => $quiz->geticonURL(),
                        'type' => $quiz->getType()->getName(),
                        'quiz_id' => $quiz->getId()
                    ]);
                if($activityTask->booster_type != 1)
                    array_push($mainTasksDto, $taskDto);
                else
                    array_push($boostersTasksDto, $taskDto);    
            }
        }
        $submoduleMapped->mainTasks=$mainTasksDto;
        $viewBoosters=$this->isPassedSubmodule($user,$round,$module,$submodule);
        $submoduleMapped->viewBoosters=$viewBoosters;
        if($viewBoosters)
            $submoduleMapped->boostersTasks=$boostersTasksDto;
        else
            $submoduleMapped->boostersTasks=[];
        return $submoduleMapped;

    }

    public function studentGiveUpMission($user_id,$round_id,$module_id,$sub_module_id,$mission_type,$task_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $round = $this->professionalRepository->getCampusRoundById($round_id);

        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $module=$this->professionalRepository->getCampusClassById($module_id);
        //TODO:CHECK IF USER IS IN ROUND
        //TODO:CHECK USER IS ALLOWED TO VIEW THIS SUBMODULE
        
        $module=$this->professionalRepository->getCampusClassById($module_id);
        $submodule=$this->professionalRepository->getSubmoduleById($sub_module_id);

        $allowedToViewSubModule=!$this->isLockedSubmodule($user,$round,$module,$submodule);
        if($allowedToViewSubModule === false)
            throw new BadRequestException(trans('locale.not_authorized'));

        //TODO:Chek if booster_mission so no_give_up

        $activity=$module->getDefaultActivity();
        $campus=$round->getCampus();
        $campus_id=$campus->getId();
        $is_default=true;
        $task=$this->journeyRepository->getTaskById($task_id);
        //GET mission progress and create one if not exist (with has_given_up=1)
        $progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user->id, $campus_id,$activity->id,true);
        if($progress!==null && $progress->getFirstSuccess()!==null)
            throw new BadRequestException(trans('locale.mission_already_solved').$progress->id);
        $this->accountRepository->beginDatabaseTransaction();
        if($progress===null){

            //TODO:store time taken
            $progress = new CampusActivityProgress($task, $user, $campus,0,1,0,0,1);
            $progress->setDefaultActivity($activity);
            $progress->setHasGivenUp(true);
        }else{
            //setFirstSucess,trials,evaluation
            $trials=$progress->getNoTrials()===null?1:$progress->getNoTrials()+1;
            $progress->setNoTrials($trials);
            $progress->getFirstSuccess($progress->getFirstSuccess());
            $progress->setScore(0);
            $progress->setEvaluation(0);
            $progress->setIsEvaluated(0);
            $progress->setHasGivenUp(true);
            $progress->setFirstSuccess(1);

        }
        
        //switch for all types to get mission suitable model answer 
        $return_object=new \StdClass;
        if($mission_type=="mission" ) {
            $mission = $task->getMissions()->first();
            //store model_answer
            $mission_model_answer=$mission->showModelAnswer();
            $progress->setUserCode($mission_model_answer);
            //return model_answer
            $return_object->model_answer=$mission_model_answer;
        }
        elseif($mission_type=="html_mission") {
            $mission = $task->getMissionsHtml()->first();
            if( $mission->getIsMiniProject()==0) {
                //TODO:store solution for all quiz bricks
                $missionHtmlSteps=$mission->getSteps();
                foreach($missionHtmlSteps as $data)
                {
                    if($data->getBrickType() == 'quizbricks'){
                        /*try{
                            $question_id=$data->getQuestionTask()->question_id;
                        }catch(\Exception $e){
                            dd($e,$data);
                        }*/
                        if($question_id=$data->getQuestionTask()!==null){
                            $question_id=$data->getQuestionTask()->question_id;
                            $question= $this->journeyRepository->getQuestionById($question_id);
                            if($question == null)
                                throw new BadRequestException(trans('locale.question_not_exist'));

                            $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($progress,$question);
                            if($campusActivityQuestion == null){

                                $campusActivityQuestion=new CampusActivityQuestion($progress,$question);
                                $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
                            }
                            $campusActivityQuestionId=$campusActivityQuestion->id;
                            $questionType=$data->getQuestionTask()->question->question_type;
                            if($questionType == 'question'){
                                $mcqQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'multiple_choice');

                                if(count($mcqQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'multiple_choice');
                                }
                                $mcqQuestionId=$data->getQuestion()->id;
                                $correctChoices = $this->journeyRepository->getChoiceModelforMcqQuestion($mcqQuestionId)->toArray();

                                foreach ($correctChoices as $correct_choice) {
                                    $choice_instance = $this->journeyRepository->getChoiceById($correct_choice['id']);
                                    $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice_instance,'1');
                                    $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
                                        
                                }

                            }elseif($questionType == 'sequence_match'){
                                $dragQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'sequence_match');

                                if(count($dragQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'sequence_match');
                                }
                                $dragCells=$question->getDragQuestion()->getDragCells();
                                foreach ($dragCells as $key=>$dragCell) {
                                    $cellChoice=$dragCell->getDragChoice();
                                    $dragQuestionAnswer=new DragQuestionAnswer($campusActivityQuestion,$dragCell,$cellChoice);
                                    $this->professionalRepository->storeDragQuestionAnswer($dragQuestionAnswer);
                                    
                                }
                            }elseif($questionType == 'dropdown'){
                                $dropdownQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'dropdown');
                                if(count($dropdownQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'dropdown');
                                }
                                $dropdowns=$question->getDropdownQuestion()->getDropdown();

                                foreach ($dropdowns as $dropdown) {
                                    
                                    $dropdownChoice = $dropdown->getDropdownChoice();
                                    if($dropdownChoice == null)
                                        throw new BadRequestException(trans('locale.dropdown_choice_not_exist'));

                                    $dropdownCorrectChoice = $this->journeyRepository->getDropdownCorrectAnswer($dropdown->id);
                                    if($dropdownCorrectChoice == null)
                                        throw new BadRequestException(trans('locale.dropdown_correct_choice_not_exist').$dropdown->id);

                                    $dropdownQuestionAnswer=new DropdownQuestionAnswer($campusActivityQuestion,$dropdown,$dropdownCorrectChoice);
                                    $this->professionalRepository->storeDropdownQuestionAnswer($dropdownQuestionAnswer);
                                }
                            }elseif($questionType == 'match'){
                                $matchQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'match');

                                if(count($matchQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'match');
                                }
                                $matchQuestion=$question->getMatchQuestion();

                                foreach($matchQuestion->getMatchKeys() as $matchKey){
                                    $matchValue=$matchKey->getMatchValue();
                                    $matchQuestionAnswer=new MatchQuestionAnswer($campusActivityQuestion,$matchKey,$matchValue);
                                    $this->professionalRepository->storeMatchQuestionAnswer($matchQuestionAnswer);
                                }
                            }
                    }

                    }
                }
            }elseif($mission->getIsMiniProject()){
               //TODO:ask if I should store mini_project link

            }
        }
        elseif($mission_type=="mini_project") {
            $mission = $task->getMissionsHtml()->first();
            if( $mission->getIsMiniProject()==0) {
                //TODO:store solution for all quiz bricks
            }elseif($mission->getIsMiniProject()){
               //TODO:ask if I should store mini_project link

            }
        }
        elseif($mission_type=="coding_mission") {
            
            $mission = $task->getMissionCoding();
            //store model_answer
            $mission_model_answer=$mission->getModelAnswer();
            $progress->setUserCode($mission_model_answer);
            //return model_answer
            $return_object->model_answer=$mission_model_answer;
            
        }
        elseif($mission_type=="editor_mission") {
            
            $mission = $task->getMissionEditor();
            
            $progress->setUserCode($mission->getModelAnswerStructuredAsUserCode());
            $return_object->model_answer=$mission->getModelAnswer();
            
               
        }
        elseif($mission_type=="angular_mission") {
            
            $mission = $task->getMissionAngular();

            foreach($mission->getFiles() as $angularMissionFile){
                $angularUserAnswer = $this->professionalRepository->getMissionAngularUserAnswerByName($progress->id, $angularMissionFile->getName());
                if($angularUserAnswer == null){
                    $angularUserAnswer = new MissionAngularUserAnswer($progress, $angularMissionFile->getName(), $angularMissionFile->getData());
                }else{
                    $angularUserAnswer->setData($angularMissionFile->getData());
                }
                $this->professionalRepository->storeMissionAngularUserAnswer($angularUserAnswer);
            }
               
        }
        elseif($mission_type=="quiz"){
            
            $quiz = $task->getQuizzes()->first();
            $questionsByQuiz = $this->journeyRepository->getQuestionsByQuiz($quiz);
            $confidence_value=1;
            $this->saveMcqQuizModelAnswers($progress,$questionsByQuiz);
                
        }
        $this->professionalRepository->storeRoundActivityProgress($progress);
        $this->accountRepository->commitDatabaseTransaction();
        return $return_object;
            
        
    }

    public function studentGiveUpMissionGeneral($user_id,$round_id,$module_id,$mission_type,$task_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $round = $this->professionalRepository->getCampusRoundById($round_id);

        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        //TODO:CHECK IF USER IS IN ROUND
        //TODO:CHECK USER IS ALLOWED TO VIEW THIS MODULE/SUBMODULE
        
        $module=$this->professionalRepository->getCampusClassById($module_id);
        $activity=$module->getDefaultActivity();
        $campus=$round->getCampus();
        $campusHasSubModules = $campus->hasSubModules();
        $campus_id=$campus->getId();
        $is_default=true;
        $task=$this->journeyRepository->getTaskById($task_id);

        $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity->id,$user_id,$is_default,$campusHasSubModules);

        //TODO:Chek if booster_mission so no_give_up

        //GET mission progress and create one if not exist (with has_given_up=1)
        $progress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user->id, $campus_id,$activity->id,true);
        if($progress!==null && $progress->getFirstSuccess()!==null)
            throw new BadRequestException(trans('locale.mission_already_solved').$progress->id);
        $this->accountRepository->beginDatabaseTransaction();
        if($progress===null){

            //TODO:store time taken
            $progress = new CampusActivityProgress($task, $user, $campus,0,1,0,0,1);
            $progress->setDefaultActivity($activity);
            $progress->setHasGivenUp(true);
        }else{
            //setFirstSucess,trials,evaluation
            $trials=$progress->getNoTrials()===null?1:$progress->getNoTrials()+1;
            $progress->setNoTrials($trials);
            $progress->getFirstSuccess($progress->getFirstSuccess());
            $progress->setScore(0);
            $progress->setEvaluation(0);
            $progress->setIsEvaluated(0);
            $progress->setHasGivenUp(true);
            $progress->setFirstSuccess(1);

        }
        
        //switch for all types to get mission suitable model answer 
        $return_object=new \StdClass;
        if($mission_type=="blockly" ) {
            $mission = $task->getMissions()->first();
            //store model_answer
            $mission_model_answer=$mission->showModelAnswer();
            $progress->setUserCode($mission_model_answer);
            //return model_answer
            $return_object->model_answer=$mission_model_answer;
        }
        elseif($mission_type=="html_mission") {
            $mission = $task->getMissionsHtml()->first();
            if( $mission->getIsMiniProject()==0) {
                //TODO:store solution for all quiz bricks
                $missionHtmlSteps=$mission->getSteps();
                foreach($missionHtmlSteps as $data)
                {
                    if($data->getBrickType() == 'quizbricks'){
                        /*try{
                            $question_id=$data->getQuestionTask()->question_id;
                        }catch(\Exception $e){
                            dd($e,$data);
                        }*/
                        if($question_id=$data->getQuestionTask()!==null){
                            $question_id=$data->getQuestionTask()->question_id;
                            $question= $this->journeyRepository->getQuestionById($question_id);
                            if($question == null)
                                throw new BadRequestException(trans('locale.question_not_exist'));

                            $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($progress,$question);
                            if($campusActivityQuestion == null){

                                $campusActivityQuestion=new CampusActivityQuestion($progress,$question);
                                $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
                            }
                            $campusActivityQuestionId=$campusActivityQuestion->id;
                            $questionType=$data->getQuestionTask()->question->question_type;
                            if($questionType == 'question'){
                                $mcqQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'multiple_choice');

                                if(count($mcqQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'multiple_choice');
                                }
                                $mcqQuestionId=$data->getQuestion()->id;
                                $correctChoices = $this->journeyRepository->getChoiceModelforMcqQuestion($mcqQuestionId)->toArray();

                                foreach ($correctChoices as $correct_choice) {
                                    $choice_instance = $this->journeyRepository->getChoiceById($correct_choice['id']);
                                    $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice_instance,'1');
                                    $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
                                        
                                }

                            }elseif($questionType == 'sequence_match'){
                                $dragQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'sequence_match');

                                if(count($dragQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'sequence_match');
                                }
                                $dragCells=$question->getDragQuestion()->getDragCells();
                                foreach ($dragCells as $key=>$dragCell) {
                                    $cellChoice=$dragCell->getDragChoice();
                                    $dragQuestionAnswer=new DragQuestionAnswer($campusActivityQuestion,$dragCell,$cellChoice);
                                    $this->professionalRepository->storeDragQuestionAnswer($dragQuestionAnswer);
                                    
                                }
                            }elseif($questionType == 'dropdown'){
                                $dropdownQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'dropdown');
                                if(count($dropdownQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'dropdown');
                                }
                                $dropdowns=$question->getDropdownQuestion()->getDropdown();

                                foreach ($dropdowns as $dropdown) {
                                    
                                    $dropdownChoice = $dropdown->getDropdownChoice();
                                    if($dropdownChoice == null)
                                        throw new BadRequestException(trans('locale.dropdown_choice_not_exist'));

                                    $dropdownCorrectChoice = $this->journeyRepository->getDropdownCorrectAnswer($dropdown->id);
                                    if($dropdownCorrectChoice == null)
                                        throw new BadRequestException(trans('locale.dropdown_correct_choice_not_exist').$dropdown->id);

                                    $dropdownQuestionAnswer=new DropdownQuestionAnswer($campusActivityQuestion,$dropdown,$dropdownCorrectChoice);
                                    $this->professionalRepository->storeDropdownQuestionAnswer($dropdownQuestionAnswer);
                                }
                            }elseif($questionType == 'match'){
                                $matchQuestionAnswers=$this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,'match');

                                if(count($matchQuestionAnswers)>0){
                                    $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,'match');
                                }
                                $matchQuestion=$question->getMatchQuestion();

                                foreach($matchQuestion->getMatchKeys() as $matchKey){
                                    $matchValue=$matchKey->getMatchValue();
                                    $matchQuestionAnswer=new MatchQuestionAnswer($campusActivityQuestion,$matchKey,$matchValue);
                                    $this->professionalRepository->storeMatchQuestionAnswer($matchQuestionAnswer);
                                }
                            }
                    }

                    }
                }
            }elseif($mission->getIsMiniProject()){
               //TODO:ask if I should store mini_project link

            }
        }
        elseif($mission_type=="mini_project") {
            $mission = $task->getMissionsHtml()->first();
            if( $mission->getIsMiniProject()==0) {
                //TODO:store solution for all quiz bricks
            }elseif($mission->getIsMiniProject()){
               //TODO:ask if I should store mini_project link

            }
        }
        elseif($mission_type=="coding_mission") {
            
            $mission = $task->getMissionCoding();
            //store model_answer
            $mission_model_answer=$mission->getModelAnswer();
            $user_code_array['language'] = 'python';
            $user_code_array['code'] = json_encode($mission_model_answer);
            $userCode = json_encode([$user_code_array]);
            $progress->setUserCode($userCode);
            //return model_answer
            $return_object->model_answer=$mission_model_answer;
            
        }
        elseif($mission_type=="editor_mission") {
            
            $mission = $task->getMissionEditor();
            
            $progress->setUserCode($mission->getModelAnswerStructuredAsUserCode());
            $return_object->model_answer=$mission->getModelAnswer();
            
               
        }
        elseif($mission_type=="angular_mission") {
            
            $mission = $task->getMissionAngular();

            foreach($mission->getFiles() as $angularMissionFile){
                $angularUserAnswer = $this->professionalRepository->getMissionAngularUserAnswerByName($progress->id, $angularMissionFile->getName());
                if($angularUserAnswer == null){
                    $angularUserAnswer = new MissionAngularUserAnswer($progress, $angularMissionFile->getName(), $angularMissionFile->getData());
                }else{
                    $angularUserAnswer->setData($angularMissionFile->getData());
                }
                $this->professionalRepository->storeMissionAngularUserAnswer($angularUserAnswer);
            }
               
        }
        elseif($mission_type=="quiz"){
            
            $quiz = $task->getQuizzes()->first();
            $questionsByQuiz = $this->journeyRepository->getQuestionsByQuiz($quiz);
            $confidence_value=1;
            $this->saveMcqQuizModelAnswers($progress,$questionsByQuiz);
                
        }
        $this->professionalRepository->storeRoundActivityProgress($progress);
        $this->accountRepository->commitDatabaseTransaction();
        return $return_object;
    }
    public function createSubModules($campus_id){
        $ideal_submodule_tasks_count=5;
        $campus=$this->professionalRepository->getCampusById($campus_id);
        $modules=$campus->getClasses();

        $firstOverviewFlag = 1;
        $firstModulesInsertions = [];
        $subModuleMainActivityTasks = [];

        foreach($modules as $module){
            $submodules = $module->getSubmodules();
            $this->professionalRepository->deleteAllModuleSubmodules($submodules);
            
            $mainActivityTasks=$module->getDefaultActivity()->getMainActivityTasks();

            //if this is a project or welcome module ,make it has only one submodule
            if($module->lessonType() == 1 || $module->lessonType() == 2)
            {
                $this->createSubmoduleForWelcomeOrProjectModule($module, $mainActivityTasks);
                continue;
            }

            $taskOrder = 1;
            $boosterTaskOrder = 1;            
            foreach ($mainActivityTasks as $mainActivityTask) {
                $task = $mainActivityTask->getTask();
                if($missionHtml = $task->getMissionsHtml()->first()) {
                    /**
                    * if task is overview or lesson or first-miniproject THEN
                    * create new submodule and assign tasks to it by order
                    */
                    if($missionHtml->getLessonType() == 'overview' || ($missionHtml->getLessonType() == 'normal' && $missionHtml->getHtmlMissionType() != 'mini_project') || ($missionHtml->getHtmlMissionType() == 'mini_project' && $previousTaskType != "mini_project")) {
                        
                        // Handle first overview in first module
                        if($missionHtml->getLessonType() == 'overview' && $firstOverviewFlag ) {
                            $subModuleMainActivityTasks[] = $mainActivityTask;
                            $moduleId = $module->getId();
                            $firstOverviewFlag = 0;
                        }
                        else {
                            // fix submodule order to be reset with first moduleId used
                            if(!in_array($moduleId, $firstModulesInsertions)) {
                                $firstModulesInsertions[] = $moduleId;
                                $submoduleOrder = 1;
                            }

                            $sub_module_id = DB::table('sub_module')->insertGetId([
                                "class_id"=>$moduleId,
                                "order"=>$submoduleOrder
                            ]);

                            DB::table('sub_module_translation')->insertGetId([
                                "sub_module_id"=>$sub_module_id,
                                "language_code"=>"en",
                                "name"=>"Sub module ".strval($submoduleOrder),
                                "description"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop",
                            ]);

                            foreach ($subModuleMainActivityTasks as $activityTask) {
                                DB::table('sub_module_task')->insertGetId([
                                    "sub_module_id"=>$sub_module_id,
                                    "task_id"=>$activityTask->task_id,
                                    "order"=>$taskOrder++
                                ]);
                                    //check if has booster add it also
                                if($activityTask->booster_type =="2"){
                                    DB::table('sub_module_task')->insertGetId([
                                        "sub_module_id"=>$sub_module_id,
                                        "task_id"=>$activityTask->booster_task_id,
                                        "order"=>$boosterTaskOrder++
                                    ]);
            
                                }
                            }
                            $taskOrder = 1;
                            $boosterTaskOrder = 1;
                            $submoduleOrder++;
                            if(! $firstOverviewFlag) {
                                $subModuleMainActivityTasks = [$mainActivityTask];
                                $moduleId = $module->getId();
                            }
                        }
                        
                    }
                }
                else {
                    $subModuleMainActivityTasks[] = $mainActivityTask;
                    $moduleId = $module->getId();
                }
                $previousTaskType = $task->getTaskNameAndType()['task_type'];
            }
            
        }
        // Handle last mini-project in last module
        if(count($subModuleMainActivityTasks) > 0) {
            if(!in_array($moduleId, $firstModulesInsertions)) {
                $firstModulesInsertions[] = $moduleId;
                $submoduleOrder = 1;
            }
            $sub_module_id = DB::table('sub_module')->insertGetId([
                "class_id"=>$moduleId,
                "order"=>$submoduleOrder
            ]);
    
            DB::table('sub_module_translation')->insertGetId([
                "sub_module_id"=>$sub_module_id,
                "language_code"=>"en",
                "name"=>"Sub module ".strval($submoduleOrder),
                "description"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop",
            ]);
    
            foreach ($subModuleMainActivityTasks as $activityTask) {
                DB::table('sub_module_task')->insertGetId([
                    "sub_module_id"=>$sub_module_id,
                    "task_id"=>$activityTask->task_id,
                    "order"=>$taskOrder++
                ]);
                    //check if has booster add it also
                if($activityTask->booster_type =="2"){
                    DB::table('sub_module_task')->insertGetId([
                        "sub_module_id"=>$sub_module_id,
                        "task_id"=>$activityTask->booster_task_id,
                        "order"=>$boosterTaskOrder++
                    ]);
    
                }
            }
        }
        return "Submodules created successfully";
    }

    private function createSubmoduleForWelcomeOrProjectModule($module, $mainActivityTasks) {
        $sub_module_id = DB::table('sub_module')->insertGetId([
            "class_id"=>$module->getId(),
            "order"=>1
        ]);

        DB::table('sub_module_translation')->insertGetId([
            "sub_module_id"=>$sub_module_id,
            "language_code"=>"en",
            "name"=>"Sub module 1",
            "description"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop",
        ]);
        
        $taskOrder = 1;
        $boosterTaskOrder = 1;
        foreach ($mainActivityTasks as $mainActivityTask) {
            DB::table('sub_module_task')->insertGetId([
                "sub_module_id"=>$sub_module_id,
                "task_id"=>$mainActivityTask->task_id,
                "order"=>$taskOrder++
            ]);
            if($mainActivityTask->booster_type =="2"){
                DB::table('sub_module_task')->insertGetId([
                    "sub_module_id"=>$sub_module_id,
                    "task_id"=>$mainActivityTask->booster_task_id,
                    "order"=>$boosterTaskOrder++
                ]);
            }
        }
    }




}