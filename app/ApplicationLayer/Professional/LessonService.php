<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/12/2018
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Professional\Dtos\CampusActivityTaskDto;
use App\ApplicationLayer\Professional\Dtos\CampusClassDto;
use App\ApplicationLayer\Professional\Dtos\CampusDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundNamesDto;


use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\MatchValue;
use App\DomainModelLayer\Journeys\MatchQuestion;
use App\DomainModelLayer\Journeys\Choice;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\CampusActivityQuestion;
use App\DomainModelLayer\Professional\DragQuestionAnswer;
use App\DomainModelLayer\Professional\DropdownQuestionAnswer;
use App\DomainModelLayer\Professional\MatchQuestionAnswer;
use App\DomainModelLayer\Professional\McqQuestionAnswer;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;

use Illuminate\Pagination\Paginator;

use App\ApplicationLayer\Journeys\Dtos\ChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragCellDto;
use App\ApplicationLayer\Journeys\Dtos\DragChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownsDto;
use App\ApplicationLayer\Journeys\Dtos\stepDropdownsChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepMcqQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\McqQuestionChoicesDto;
use App\ApplicationLayer\Journeys\Dtos\StepMatchQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\MatchKeysDto;
use App\ApplicationLayer\Journeys\Dtos\MatchValueDto;
use App\Helpers\LTI;
use App\Helpers\Mapper;
use App\Notifications\StudentStuck;
use App\Notifications\Submission;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Carbon\Carbon;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use Illuminate\Support\Facades\Notification;
use Ramsey\Uuid\Uuid;
use App\ApplicationLayer\Professional\Dtos\RoomDto;
use IMSGlobal\LTI\ToolProvider\Outcome;
use IMSGlobal\LTI\ToolProvider\ResourceLink;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use IMSGlobal\LTI\ToolProvider;

use Illuminate\Database\QueryException;

use App\ApplicationLayer\Common\LessonCommonService;

class LessonService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonCommonService;
    

    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

    }
    private function checkActivityInCampus($activity_id,$campus_id,$is_default){
        $activity_in_campus = $this->professionalRepository->getCampusActivityTasks($activity_id,$campus_id,$is_default);
        if(count($activity_in_campus)>0){
            return true;
        }else{
            return false;
        }
    }

    private function checkUserRoundData($user_id,$round_id,$activity_id,$mission_id,$question_id,$is_default=true){
       
        $user = $this->accountRepository->getUserById($user_id,$round_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        $campus_round = $this->professionalRepository->getCampusRoundById($round_id);
        if($campus_round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
        $campus=$campus_round->getCampus();
        //$campus_activity_tasks=$campus->getCampusActivityTasks();

        $checkActivityInCamp=$this->checkActivityInCampus($activity_id,$campus->getId(),$is_default);
        if(!$checkActivityInCamp)
            throw new BadRequestException(trans('locale.activity_not_in_campus'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $question= $this->journeyRepository->getQuestionById($question_id);

        if($question == null)
            throw new BadRequestException(trans('locale.question_not_exist'));

        $checkQuestionInLesson=$this->journeyRepository->checkQuestionInLesson($mission_id,$question_id);


        if($checkQuestionInLesson == null)
            throw new BadRequestException(trans('locale.question_not_in_lesson'));

    
        $data['round']=$campus_round;
        $data['campus']=$campus;
        $data['user']=$user;
        $data['activity']=$activity;
        $data['question']=$question;
        $data['mission']=$mission;

        return $data;
    }
   

    public function getUserTaskProgressData(Task $task, User $user, Campus $campus, DefaultActivity $activity,Question $question, $is_default=true,$create=true){

        $score=0;
        $firstSuccess=null;

        $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user->id, $campus->getId(), $activity->id, $is_default);
        if($missionProgress==null ){

            $missionProgress = new CampusActivityProgress($task,$user,$campus,$score,1,1,$score,$firstSuccess);
            $missionProgress->setDefaultActivity($activity);
            
            $this->professionalRepository->storeCampusActivityProgress($missionProgress);
        }

        $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$question);
        if($campusActivityQuestion==null && $create){
            $campusActivityQuestion=new CampusActivityQuestion($missionProgress,$question);
            $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);

        }
        $progressData["missionProgress"]=$missionProgress;
        $progressData["campusActivityQuestion"]=$campusActivityQuestion;
        return $progressData;
    }


    public function submitLessonDragQuestion($answersData, $mission_id,$question_id, $user_id, $round_id, $activity_id){
       
        $is_default=true;
        $data=$this->checkUserRoundData($user_id,$round_id,$activity_id,$mission_id,$question_id,$is_default);

        $campus_round=$data['round'];
        $campus=$data['campus'];
        $user=$data['user'];
        $activity=$data['activity'];
        $question=$data['question'];
        $mission=$data['mission'];
        $step_id = $this->professionalRepository->getStepIdForQuestion($question->id);
        $step = $this->professionalRepository->getStepById($step_id);
        if($question->question_type!=="sequence_match")
            throw new BadRequestException(trans('locale.not_sequence_match_question'));

        $task=$mission->getTask();
        
        $this->accountRepository->beginDatabaseTransaction();

        if($user->isStudent()){
            $progressData=$this->getUserTaskProgressData($task, $user, $campus, $activity,$question, $is_default);
            $missionProgress=$progressData["missionProgress"];
            $campusActivityQuestion=$progressData["campusActivityQuestion"];
        }
        $dragCells=$question->getDragQuestion()->getDragCells()->toArray();
        $submissionArr=[];
        foreach ($answersData as $key=>$cell) {
            
            $dragCell = $this->journeyRepository->getDragCellById($dragCells[$key]);
            if($dragCell == null)
                throw new BadRequestException(trans('locale.drag_cell_not_exist'));

            $cellChoice = $this->journeyRepository->getDragChoiceById($cell['choice_id']);
            if($cellChoice == null)
                throw new BadRequestException(trans('locale.drag_choice_not_exist'));
            // construct submissions array to be returned

            if($dragCell->getShowAnswer()==0){
                $submissionDto['cell_id']=$dragCell->id;
                $submissionDto['correct_choice_id']=$dragCell->getDragChoice()->id;
                $submissionDto['user_choice_id']=$cellChoice->id;
                $submissionDto['eval']=$dragCell->getDragChoice()->id == $cellChoice->id;
                $submissionArr[]=$submissionDto;

                if($user->isStudent()){
                    //only save answers if not saved before and if saved only modify
                    $dragQuestionAnswer=$this->professionalRepository->getDragQuestionAnswer($campusActivityQuestion,$dragCell);
                    if($dragQuestionAnswer==null){
                        $dragQuestionAnswer=new DragQuestionAnswer($campusActivityQuestion,$dragCell,$cellChoice);
                        $this->professionalRepository->storeDragQuestionAnswer($dragQuestionAnswer);

                    }else{
                        $dragQuestionAnswer->setDragChoice($cellChoice);
                        $this->professionalRepository->storeDragQuestionAnswer($dragQuestionAnswer);

                    }
                   
                }
            }
            
        }
        $this->accountRepository->commitDatabaseTransaction();
        $dragQuestionStep=$this->lessonCommonService->getDragStep($step,$user,$activity,$campus,$mission,$task);
        
        // return ["questions_answers"=>$submissionArr,"brick"=>$dragQuestionStep];
        return ["brick"=>$dragQuestionStep];
       
       
    }

    public function submitLessonDropdownQuestion($answersData, $mission_id,$question_id, $user_id, $round_id, $activity_id){

        $is_default=true;
        $data=$this->checkUserRoundData($user_id,$round_id,$activity_id,$mission_id,$question_id,$is_default);

        $campus_round=$data['round'];
        $campus=$data['campus'];
        $user=$data['user'];
        $activity=$data['activity'];
        $question=$data['question'];
        $mission=$data['mission'];
        $step_id = $this->professionalRepository->getStepIdForQuestion($question->id);
        $step = $this->professionalRepository->getStepById($step_id);
        if($question->question_type!=="dropdown")
            throw new BadRequestException(trans('locale.not_dropdown_question'));


        $task=$mission->getTask();
        
        $this->accountRepository->beginDatabaseTransaction();

        if($user->isStudent()){
            $progressData=$this->getUserTaskProgressData($task, $user, $campus, $activity,$question, $is_default);
            $missionProgress=$progressData["missionProgress"];
            $campusActivityQuestion=$progressData["campusActivityQuestion"];
        }
        
        $submissionArr=[];

        foreach ($answersData as $dropdown_data) {
            
            $dropdown = $this->journeyRepository->getDropdownById($dropdown_data['dropdown_id']);
            if($dropdown == null)
                throw new BadRequestException(trans('locale.dropdown_not_exist'));

            $dropdownChoice = $this->journeyRepository->getDropdownChoiceById($dropdown_data['choice_id']);
            if($dropdownChoice == null)
                throw new BadRequestException(trans('locale.dropdown_choice_not_exist'));

            $dropdownCorrectChoice = $this->journeyRepository->getDropdownCorrectAnswer($dropdown_data['dropdown_id']);
            if($dropdownCorrectChoice == null)
                throw new BadRequestException(trans('locale.dropdown_correct_choice_not_exist'));

            $submissionDto['dropdown_id']=$dropdown->id;
            $submissionDto['correct_choice_id']=$dropdownCorrectChoice->id;
            $submissionDto['user_choice_id']=$dropdownChoice->id;
            $submissionDto['eval']=$dropdownCorrectChoice->id == $dropdownChoice->id;
            $submissionArr[]=$submissionDto;

            if($user->isStudent()){
                
                $dropdownQuestionAnswer=$this->professionalRepository->getDropdownQuestionAnswer($campusActivityQuestion,$dropdown);
                if($dropdownQuestionAnswer==null){
                    $dropdownQuestionAnswer=new DropdownQuestionAnswer($campusActivityQuestion,$dropdown,$dropdownChoice);
                    $this->professionalRepository->storeDropdownQuestionAnswer($dropdownQuestionAnswer);

                }else{
                    $dropdownQuestionAnswer->setDropdownChoice($dropdownChoice);
                    $this->professionalRepository->storeDropdownQuestionAnswer($dropdownQuestionAnswer);

                }
            
            }

            
        }
        $this->accountRepository->commitDatabaseTransaction();
        $dropdownQuestionStep=$this->lessonCommonService->getDropdownStep($step,$user,$activity,$campus,$mission,$task);

        return ["questions_answers" => $submissionArr,'brick'=>$dropdownQuestionStep];
       
       
    }

    //MCQ
    public function submitLessonMcqQuestion($mcqChoices,$mission_id,$question_id, $user_id, $round_id, $activity_id)
    {
        
        $is_default=true;
        $data=$this->checkUserRoundData($user_id,$round_id,$activity_id,$mission_id,$question_id,$is_default);

        $campus_round=$data['round'];
        $campus=$data['campus'];
        $user=$data['user'];
        $activity=$data['activity'];
        $question=$question_id;
        $mission=$data['mission'];
        $mark_counter = 0;
        $task=$mission->getTask();
        $score=0;
        $ok=false;
        $passed = 0;
        $firstSuccess=null;
        $step_id = $this->professionalRepository->getStepIdForQuestion($question);
        $step = $this->professionalRepository->getStepById($step_id);

        $question_instance = $this->journeyRepository->getQuestionById($question);
        
        if($question_instance == null)
        throw new BadRequestException(trans('locale.question_not_exist'));

        $mcq_question_id = $question_instance->getMcqQuestion()->id;
        $correct_choices = $this->journeyRepository->getChoiceModelforMcqQuestion($mcq_question_id)->toArray();
        
        foreach ($mcqChoices as $choice) {

            $choice_instance = $this->journeyRepository->getChoiceById($choice);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));
            
            $question_answer = [
                "choice_id" => $choice,
                "eval" => false,
            
            ];
            
            foreach ($correct_choices as $correct_choice) {
                
                if($correct_choice['id'] == $choice)
                {
                    $question_answer['eval'] = true;
                }
               
            }
            $questions_answers[] = $question_answer;
        }

            if($user->isStudent()){
                //TODO : Save to DB
                $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus->getId(), $activity_id, $is_default);
                if($missionProgress==null){
    
                    $missionProgress = new CampusActivityProgress($task,$user,$campus,$score,1,1,$score,$firstSuccess);
                    $missionProgress->setDefaultActivity($activity);
                    
                    $this->professionalRepository->storeCampusActivityProgress($missionProgress);
                }

            //Saving to MCQ question answeres
            $this->accountRepository->beginDatabaseTransaction();
            $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$data['question']);
           
            if($campusActivityQuestion==null){
                $campusActivityQuestion=new CampusActivityQuestion($missionProgress,$data['question']);
            foreach ($mcqChoices as $mcqChoice) {
                
                $choice = mapper(Choice::class)->find($mcqChoice);
                $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
                
                $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice,1);
                $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);
                    
                 }
            }else{
                // if saved only modify
                    
                    $mcqQuestionAnswer=$this->professionalRepository->getOldMcqQuestionAnswer($campusActivityQuestion);
                    
                    $oldcampusActivityQuestionId = $campusActivityQuestion->id;
                    //Delete old MCQ Answer record
                    foreach ($mcqQuestionAnswer as $old_answer) {
                        $this->professionalRepository->removeOldMcqQuestionAnswer($old_answer);
                    }
                    

                    //Store new MCQ records
                    foreach ($mcqChoices as $mcqChoice) {
                        $choice = mapper(Choice::class)->find($mcqChoice);
                        
                        $mcqQuestionAnswer=new McqQuestionAnswer($campusActivityQuestion,$choice,1);
                        $this->professionalRepository->storeMcqQuestionAnswer($mcqQuestionAnswer);

                 }   
            }
            $this->accountRepository->commitDatabaseTransaction();

            }
            $mcqQuestionStep=$this->lessonCommonService->getMcqQuestionStep($step,$user,$activity,$campus,$mission,$task);
            return ["answers" => $questions_answers,"brick"=>$mcqQuestionStep];

    }


    // Matching
    public function submitLessonMatchQuestion($matchData,$mission_id,$question_id, $user_id, $round_id, $activity_id)
    {
        $is_default=true;
        $data=$this->checkUserRoundData($user_id,$round_id,$activity_id,$mission_id,$question_id,$is_default);

        $campus_round=$data['round'];
        $campus=$data['campus'];
        $user=$data['user'];
        $activity=$data['activity'];
        $question=$data['question'];
        $mission=$data['mission'];
        $mark_counter = 0;
        $task=$mission->getTask();
        $score=0;
        $passed = 0;
        $firstSuccess=null;
        $matchResult = [];
        
        $step_id = $this->professionalRepository->getStepIdForQuestion($question->id);
        $step = $this->professionalRepository->getStepById($step_id);
        
        //Check Submited Answeres
        foreach($matchData as $key){
            $matchKey = mapper(MatchKey::class)->find($key['id']);
            if($key['match_value_id'] == $matchKey->getMatchValue()->id){
                $matchResult += [$key['id'] => true];
            }else{
                $matchResult += [$key['id'] => false];
            }
        }
        //Check if Passed
        if (!in_array(false, $matchResult,true)) {$passed = 1;}

        if($user->isStudent()){
            $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user_id, $campus->getId(), $activity_id, $is_default);
            if($missionProgress==null){

                $missionProgress = new CampusActivityProgress($task,$user,$campus,$score,1,1,$score,$firstSuccess);
                $missionProgress->setDefaultActivity($activity);
                
                $this->professionalRepository->storeCampusActivityProgress($missionProgress);
            }
        }
        
        if($user->isStudent()){
            //Saving to Match question answeres
            $this->accountRepository->beginDatabaseTransaction();
            $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$question);
            
            if($campusActivityQuestion==null){
                $campusActivityQuestion=new CampusActivityQuestion($missionProgress,$question);
            foreach ($matchData as $key) {
                $matchKey = mapper(MatchKey::class)->find($key['id']);
                $matchValue = mapper(MatchValue::class)->find($key['match_value_id']);
                   
                        
                        $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);
    
                        $matchQuestionAnswer=new MatchQuestionAnswer($campusActivityQuestion,$matchKey,$matchValue);
                        $this->professionalRepository->storeMatchQuestionAnswer($matchQuestionAnswer);
                    
                 }
            }else{
                foreach ($matchData as $key) {
                   // Only save answers if not saved before and if saved only modify
                   $matchKey = mapper(MatchKey::class)->find($key['id']);
                   $matchValue = mapper(MatchValue::class)->find($key['match_value_id']);
                   
                   $MatchQuestionAnswer=$this->professionalRepository->getMatchQuestionAnswer($campusActivityQuestion,$matchKey,$matchValue);

                   if($MatchQuestionAnswer!=null){
                        $MatchQuestionAnswer->setMatchValue($matchValue);
                        
                        $this->professionalRepository->storeMatchQuestionAnswer($MatchQuestionAnswer);
                   }
                }
            }
            $this->accountRepository->commitDatabaseTransaction();
        }
        $matchQuestionStep=$this->lessonCommonService->getMatchQuestionStep($step,$user,$activity,$campus,$mission,$task);

        return ["passed" => $passed, "questions_answers" => $matchResult,'brick'=>$matchQuestionStep];
    }

}