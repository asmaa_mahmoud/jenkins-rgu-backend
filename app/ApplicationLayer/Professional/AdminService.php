<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 06/11/2018
 * Time: 10:03 AM
 */

namespace App\ApplicationLayer\Professional;


use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Professional\Dtos\AdminCampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\MinimizedCampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\CampusDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\TeacherCampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundRequestDto;
use App\ApplicationLayer\Professional\Dtos\CampusUserDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundClassDto;
use App\ApplicationLayer\Professional\Dtos\UserCampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\ColorPaletteDto;
use App\ApplicationLayer\Common\LessonCommonService;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\CampusActivityTask;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusRoundActivityTask;
use App\DomainModelLayer\Professional\CampusRoundClass;
use App\DomainModelLayer\Professional\CampusUser;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\SchoolCampus;
use App\DomainModelLayer\Schools\School;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\CustomException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use Ramsey\Uuid\Uuid;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularFilesDto;

class AdminService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonCommonService;


    const Attendee_Password = '12345';
    const Moderator_Password = '54321';

    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

    }
    public function tasks_cmp($task1,$task2){
        if ($task1['order'] === $task2['order']) {
            return 0;
        }
        if ($task1['order'] < $task2['order']) {
            return -1;
        }else{
            return 1;
        }
    }
    public function sortAndSearchCollectionStudents($students,$search){

        $subs=[];
        if($search!=null) {
            foreach ($students as $student){
                if (strpos(strtolower($student['username']), strtolower($search)) !== false ) {
                    array_push($subs,$student);
                }else{
                    foreach ($student['rounds'] as $studentRound){
                        if (strpos(strtolower($studentRound['campusName']), strtolower($search)) !== false ) {

                            array_push($subs,$student);
                            break;

                        }

                    }

                }


            }
        }
        //dd($subs);
        if($search===null)
            $sortedSubmissions=$students;
        else
            $sortedSubmissions=$subs;



        return $sortedSubmissions;
    }

    public function getAdminCampusRounds($user_id, $campus_id,$gmt_difference,$is_default=true){
        $data=$this->checkSchoolData($user_id);
        $user = $data['user'];

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist')); //TODO translations

        $rounds = $campus->getRounds();
        //dd($rounds);
        //$roundsDto = Mapper::MapEntityCollection(AdminCampusRoundDto::class, $rounds, [CampusUserDto::class, CampusUserDto::class,CampusRoundClassDto::class]);
        // return $roundsDto;
        $roundsDto=[];
        foreach ($rounds as $round){
            $roundDto=[];
            $roundDto['id']=$round->getId();
            $roundDto['campusName']=$round->getCampusName();
            $roundDto['starts_at']=$round->getStartsAt();
            $roundDto['ends_at']=$round->getEndsAt();
            $roundDto['status']=$round->getStatus($gmt_difference);
            $students=$round->getStudents();
            $roundDto['students']=$students;
            $teachers=$round->getTeachers();
            $roundDto['teachers']=$teachers;
            //TODO:Comment/Uncomment

            $bbb = new BigBlueButton();
            $classDtos=[];
            $roundClasses=$round->getCampusRoundClasses();
            foreach ($roundClasses as $roundClass){

                $class=$roundClass->getCampusClass();

                if($is_default)
                    $activity_id=$class->getDefaultActivity()->getId();
                else
                    $activity_id=$class->getActivity()->getId();
                $class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$class->getCampus()->getId(),$is_default);
                $classDto=[];

                $classDto['id']=$class->getId();

                $classDto['order']=$class->getOrder();
                $classDto['starts_at']=$roundClass->getStartsAt();
                $classDto['ends_at']=$roundClass->getEndsAt();
                $classDto['due_date']=$roundClass->getDueDate();

                $classDto['duration'] = $roundClass->getDuration();
                if($is_default) {
                    $classDto['activity_id'] = $class->getDefaultActivity()->getId();
                    $classDto['name']=$class->getDefaultActivity()->getName();
                    $classDto['description']=$class->getDefaultActivity()->getDescription();
                }
                else {
                    $classDto['activity_id'] = $class->getActivity()->getId();
                    $classDto['name']=$class->getActivity()->getName();
                    $classDto['description']=$class->getActivity()->getDescription();
                }

                $tasksDto=[];

                foreach ($class_tasks as $class_task){
                    $task=$class_task->getTask();

                    if($task->getMissions()->first() != null) {
                        $mission = $task->getMissions()->first();

                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $mission->getName(),
                            'image' => $mission->getIconUrl(),

                            'type' => 'mission',
                            'mission_id' => $mission->getId(),
                            'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)

                        ]);
                    }
                    elseif($task->getMissionsHtml()->first()!=null) {
                        $mission = $task->getMissionsHtml()->first();
                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $mission->getTitle(),
                            'image' => $mission->getIconUrl(),

                            'type' => 'html_mission',
                            'mission_id' => $mission->getId(),
                            'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                        ]);

                    }
                    elseif($task->getQuizzes()->first()!=null) {
                        $quiz = $task->getQuizzes()->first();

                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $quiz->getTitle(),
                            'image' => $quiz->geticonURL(),

                            'type' => $quiz->getType()->getName(),
                            'quiz_id' => $quiz->getId(),
                            'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default)
                        ]);
                    }
                    elseif($task->getMissionsCoding()->first()!=null){
                        $codingMission=$task->getMissionsCoding()->first();

                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $codingMission->getTitle(),
                            'image' => $codingMission->getIconURL(),

                            'type' => 'coding_mission',
                            'mission_id' => $codingMission->getId(),
                            'model_answer' => $codingMission->getModelAnswer(),
                            'order' => $codingMission->getTask()->getActivityOrder($activity_id, $is_default)
                        ]);
                    }
                    elseif($task->getMissionsEditor()->first()!=null){
                        $editorMission=$task->getMissionsEditor()->first();

                        array_push($tasksDto, [
                            'id' => $task->getId(),
                            'name' => $editorMission->getTitle(),
                            'image' => $editorMission->getIconURL(),
                            'mode' => $editorMission->getType(),
                            'type' => 'editor_mission',
                            'mission_id' => $editorMission->getId(),
                            'model_answer' => $editorMission->getModelAnswer(),
                            'order' => $editorMission->getTask()->getActivityOrder($activity_id, $is_default)
                        ]);
                    }

                    $classDto['tasks']=$tasksDto;

                }

                $unordered_tasks=  $tasksDto;
                $classDto['tasks']=$this->sortClassTasks($unordered_tasks);


                $classRooms=$roundClass!==null?$roundClass->getRooms():[];
                $classDto['recordedSessions']=null;
                $classDto['runningSession']=null;
                //TODO:Comment/Uncomment

                foreach($classRooms as $classRoom) {
                    //GET RECORDING ROOMS
                    $recordingParams = new GetRecordingsParameters();
                    $recordingParams->setMeetingId($classRoom->id);
                    $response = $bbb->getRecordings($recordingParams);
                    $records = [];
                    if ($response->getReturnCode() == 'SUCCESS') {
                        foreach ($response->getRecords()  as $recording) {
                            // process all recording
                            $record_['url'] = $recording->getPlaybackUrl();
                            $record_['seesion_id'] = $classRoom->id;
                            $record_['session_name'] = $classRoom->name;
                            $records []= $record_;
                        }
                    }
                    if(!empty($records))
                        $classDto['recordedSessions'] []= $records[0];
                    //GET RUNNING ROOMS
                    $isRunningParams = new IsMeetingRunningParameters($classRoom->id);
                    $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
                    if($isRunning) {
                        $room_['seesion_id'] = $classRoom->id;
                        $room_['session_name'] = $classRoom->name;
                        $classDto['runningSession'] = $room_;
                    }
                }

                $classDtos[]=$classDto;

            }
            $roundDto['classes']=$classDtos;
            $unSortedClasses = $classDtos;

            usort($unSortedClasses,array($this, 'tasks_cmp'));
            $roundDto['classes']=$unSortedClasses;
            $roundDtos[]=$roundDto;

        }
        //$roundsDto = Mapper::MapEntityCollection(AdminCampusRoundDto::class, $rounds, [CampusUserDto::class, CampusUserDto::class,CampusRoundClassDto::class]);
        return $roundDtos;
    }
    public function editAdminCampusRound(CampusRoundRequestDto $campusRoundDto, $user_id,$gmt_difference)
    {
       
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
            
        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user) && !$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
            
        $round = $this->professionalRepository->getCampusRoundById($campusRoundDto->round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if($this->professionalRepository->checkRoundNameAvailability($campusRoundDto->campus_id,$campusRoundDto->round_name,$user)){
                throw new BadRequestException(trans("locale.round_name_exists"));
            }
        $roundStartsDate=$round->getStartsAt();
        $roundEndsDate=$round->getEndsAt();
        if($campusRoundDto->ends_at <= $campusRoundDto->starts_at)
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

        /*if($gmt_difference!=null)
            $today=Carbon::today()->addHour(($gmt_difference))->timestamp;
        else
            throw new BadRequestException(trans("locale.gmt_difference_should_be_valid_number"));
*/
        if($campusRoundDto->starts_at != $roundStartsDate)
        {   
            $todayUTC=Carbon::now()->startOfDay()->addHours(-($gmt_difference))->timestamp;

            if($campusRoundDto->starts_at < $todayUTC)
            {
                throw new BadRequestException(trans("locale.round_canot_start_before_today"));
            }
        }
        
      
        $round->setRoundName($campusRoundDto->round_name);
        $round->setStartsAt($campusRoundDto->starts_at);
        $round->setEndsAt($campusRoundDto->ends_at);

        $classes=$campusRoundDto->classes;
        $roundClasses=$round->getCampusRoundClasses();
        $index=0;
        foreach($roundClasses as $roundClass){
            $roundClass->starts_at=$classes[$index]['starts_at'];
            $roundClass->ends_at= $classes[$index]['ends_at'];
            //$roundClass->due_date= $classes[$index]['due_date'];
           
           $this->professionalRepository->storeCampusRoundClass($roundClass);
           $index++;
        }
       
        $this->professionalRepository->storeCampusRound($round);
        
        $this->accountRepository->commitDatabaseTransaction();

        //$roundDto= Mapper::MapClass(CampusRoundDto::class, $round,[CampusDto::class]);
        return trans("locale.round_updated_successfully");


    }
 
    public function sortClassTasks(array $arr) {
        $sorted = false;
        while (false === $sorted) {
            $sorted = true;
            for ($i = 0; $i < count($arr)-1; ++$i) {
                $current = $arr[$i];
                $next = $arr[$i+1];
                if ($next['order'] < $current['order']) {
                    $arr[$i] = $next;
                    $arr[$i+1] = $current;
                    $sorted = false;
                }
            }
        }
        return $arr;
    }


    public function getClassTasksAndRooms($user_id,$class_id,$round_id,$is_default){
        //TODO:Comment/Uncomment
        $bbb = new BigBlueButton();
        $data=$this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));


        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));
        
        $campusRoundClass = $this->professionalRepository->getCampusClassById($class_id);
        
        if($campusRoundClass == null)
            throw new BadRequestException(trans('locale.class_not_exist'));
        //check user is in this campus


       // $roundClass = $this->professionalRepository->getClassTasksAndRooms($class_id);
        $class=$campusRoundClass;
        //$class=$roundClass->getCampusClass();
        
        $classDto=[];
        $classDto['id']=$class->getId();

        if($is_default) {
            $activity_id = $class->getDefaultActivity()->getId();
            $classDto['name']=$class->getDefaultActivity()->getName();
            $classDto['icon']=$class->getDefaultActivity()->getIconUrl();
            $classDto['description']=$class->getDefaultActivity()->getDescription();
            $activity = $class->getDefaultActivity();
        }
        else {
            $activity_id = $class->getActivity()->getId();
            $classDto['name']=$class->getActivity()->getName();
            $classDto['icon']=$class->getActivity()->getIconUrl();
            $classDto['description']=$class->getActivity()->getDescription();
            $activity = $class->getActivity();
        }
        $class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$class->getCampus()->getId(),$is_default);


        $tasksDto=[];
        $htmlTasksDto=[];
        $boosterTasksDto = [];
        foreach ($class_tasks as $class_task){
            $task=$class_task->getTask();
            $taskTimeEstimate = $this->professionalRepository->getTaskTypeTimeEstimate($task->getTaskNameAndType()['task_type']);
            $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
            $difficulty =  $this->professionalRepository->getTaskDifficulty($task->getId(),$activity->getId());

            if($task->getMissions()->first() != null) {
                $mission = $task->getMissions()->first();
                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image' => $mission->getIconUrl(),
                    'type' => 'mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                    'locked'=>false,


                ];

                if($activityTask->booster_type != 1){
                    array_push($htmlTasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    array_push($boosterTasksDto,$taskDtoInst );
                }

            }
            elseif($task->getMissionsHtml()->first()!=null) {
                $mission = $task->getMissionsHtml()->first();

               if($mission->getWithSteps() == 1)
                $withSteps=true;
                else
                $withSteps=false;

                if($mission->getIsMiniProject()==0) {//normal html

                    $taskDtoInst = [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'time_estimate' => $taskTimeEstimate,
                        'type' => 'html_mission',
                        'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                        'lessonType' => $mission->getLessonType(),
                        'withSteps' => $withSteps,
                        'locked'=>false,

                    ];

                    if($activityTask->booster_type != 1){
                        array_push($htmlTasksDto,$taskDtoInst );
                    }elseif($activityTask->booster_type == 1){
                        array_push($boosterTasksDto,$taskDtoInst );
                    }


                }elseif($mission->getIsMiniProject()){

                    $taskDtoInst = [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'time_estimate' => $taskTimeEstimate,
                        'type' => 'mini_project',
                        'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                        'lessonType' => $mission->getLessonType(),
                        'withSteps' => $withSteps,
                        'locked'=>false,

                        //'project_id' => $task->getProject()->first()->getProjectId()
                    ];

                    if($activityTask->booster_type != 1){
                        array_push($htmlTasksDto,$taskDtoInst );
                    }elseif($activityTask->booster_type == 1){
                        array_push($boosterTasksDto,$taskDtoInst );
                    }
                }

            }
            elseif($task->getQuizzes()->first()!=null) {
                $quiz = $task->getQuizzes()->first();

                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $quiz->getTitle(),
                    'image' => $quiz->geticonURL(),
                    'time_estimate' => $taskTimeEstimate,
                    'type' => $quiz->getType()->getName(),
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'quiz_id' => $quiz->getId(),
                    'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default),
                    'locked'=>false,

                ];


                if($activityTask->booster_type != 1){
                    array_push($htmlTasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            elseif($task->getMissionsCoding()->first()!=null){
                $codingMission=$task->getMissionsCoding()->first();

                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $codingMission->getTitle(),
                    'image' => $codingMission->getIconURL(),
                    'time_estimate' => $taskTimeEstimate,
                    'type' => 'coding_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'mission_id' => $codingMission->getId(),
                    'order' => $codingMission->getTask()->getActivityOrder($activity_id, $is_default),
                    'locked'=>false,

                ];

                if($activityTask->booster_type != 1){
                    array_push($htmlTasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            elseif($task->getMissionsEditor()->first()!=null){
                $editorMission=$task->getMissionsEditor()->first();

                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $editorMission->getTitle(),
                    'image' => $editorMission->getIconURL(),
                    'mode' => $editorMission->getType(),
                    'type' => 'editor_mission',
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'time_estimate' => $taskTimeEstimate,
                    'mission_id' => $editorMission->getId(),
                    'order' => $editorMission->getTask()->getActivityOrder($activity_id, $is_default),
                    'locked'=>false,

                ];

                if($activityTask->booster_type != 1){
                    array_push($htmlTasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            elseif($task->getMissionAngular()!=null){
                $angularMission=$task->getMissionAngular();
                $missionAngularFilesDto=Mapper::MapEntityCollection(MissionAngularFilesDto::class, $angularMission->getFiles());

                $taskDtoInst = [
                    'id' => $task->getId(),
                    'name' => $angularMission->getTitle(),
                    'image' => $angularMission->getIconURL(),
                    'type' => 'angular_mission',
                    'files' => $missionAngularFilesDto,
                    'difficulty'=>($difficulty) ? $difficulty->translations->first()->getTitle():null,
                    'time_estimate' => $taskTimeEstimate,
                    'mission_id' => $angularMission->getId(),
                    'order' => $angularMission->getTask()->getActivityOrder($activity_id, $is_default),
                    'locked'=>false,

                ];

                if($activityTask->booster_type != 1){
                    array_push($htmlTasksDto,$taskDtoInst );
                }elseif($activityTask->booster_type == 1){
                    array_push($boosterTasksDto,$taskDtoInst );
                }
            }
            $classDto['tasks']=$tasksDto;

        }
        $roundClass=$this->professionalRepository->getCampusRoundClass($round_id,$class_id);

        $unordered_tasks=  $tasksDto;
        // if($round->hasInstructor()){
        //     $classDto['tasks']=$this->sortClassTasks($unordered_tasks);
        // }
        $unordered_html_tasks=  $htmlTasksDto;
        //Merge if hasn't Instructor
        // if(!$round->hasInstructor()){
            $mergedTasks = $this->sortClassTasks(array_merge($unordered_tasks,$unordered_html_tasks));
            $classDto['tasks'] = $mergedTasks;
        // }else{
        //     $classDto['html_tasks']=$this->sortClassTasks($unordered_html_tasks);
        // }
        $classDto['boosters']= empty($boosterTasksDto) ? null : $this->sortClassTasks($boosterTasksDto);
        $classDto['materials']=null;
        $materials=$roundClass!==null?$roundClass->getMaterials():[];
        foreach ( $materials as $material){
            $classDto['materials'] []= $material;
        }

        return $classDto;
    }

    public function setAssignedAndUnassignedStudentsInCampusRound($user_id,$round_id, $unAssignedStudents, $assignedStudents,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        $account = $data['account'];

        $campusRound = $this->professionalRepository->getRoundById($round_id);
        if($campusRound == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $campus = $campusRound->getCampus();
        $campus_id=$campus->getId();
        if($campus == null)
            throw  new BadRequestException(trans("locale.campus_not_exist")); //TODO translations
        $schoolCampus = $campus->getSchoolCampus();
        if($schoolCampus == null)
            throw  new BadRequestException(trans("locale.campus_not_exist_school"));

//        $campusInSchool = $this->professionalRepository->checkIfCampusInSchool($campus_id, $school->getId());
//        if(!$campusInSchool)
//            throw new BadRequestException(trans("locale.campus_not_exist_school"));


        $status = $campusRound->getStatus($gmt_difference);
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));

        //for assigning students
        $role = $this->accountRepository->getRoleByName("student");
        if($role == null)
            throw new BadRequestException(trans("locale.no_role_same_student"));

        $countTotalStudents = $campusRound->getNoOfStudents()===null?0:$campusRound->getNoOfStudents();
        $newStudentsCount=$countTotalStudents;
        $rounds=$campus->getRounds();
        $rounds_id = [];
        foreach ($rounds as $round){
            if($round->getStatus($gmt_difference) == 'running' || $round->getStatus($gmt_difference) == 'locked')
                $rounds_id[] = $round->getId();
        }

        if(count($unAssignedStudents) > 0){
            foreach ($unAssignedStudents as $unAssignedStudent){
                $student = $this->accountRepository->getUserById($unAssignedStudent);
                if($student === null)
                    throw new BadRequestException(trans("locale.student_with_id") ." ". $unAssignedStudent ." ". trans("locale.doesn't_exist"));

                $studentInCampusRound = $this->professionalRepository->checkIfStudentInCampusRound($round_id, $unAssignedStudent);
                if($studentInCampusRound){
                    //check same account
                    if($student->getAccount()->getId()===$account->getId()){
                        $campusUser = $this->professionalRepository->getCampusRoundUser($round_id, $unAssignedStudent);
                        $this->professionalRepository->deleteCampusRoundUser($campusUser);
                        $newStudentsCount--;

                    }else{
                        throw new BadRequestException(trans("locale.student_with_id") ." ". $unAssignedStudent ." ". trans("locale.not_in_same_account"));
                    }
                }else{
                    throw new BadRequestException(trans("locale.student_with_id") ." ". $unAssignedStudent ." ". trans("locale.not_in_campus_round"));
                }
            }
        }

        if(count($assignedStudents) > 0){
            foreach ($assignedStudents as $studentId){
                $student = $this->accountRepository->getUserById($studentId);
                if($student === null)
                    throw new BadRequestException(trans("locale.student_with_id") ." ". $studentId ." ". trans("locale.doesn't_exist"));


                $isStudent = $this->checkUserRole($student,'student');
                if(!$isStudent)
                    throw new BadRequestException(trans("locale.student_with_id") ." ". $studentId ." ". trans("locale.not_student"));

                if($student->getAccount()->getId()===$account->getId()){
                    $studentInRound=$this->professionalRepository->getCampusRoundUser($round_id,$studentId);
                    if($studentInRound!==null)
                        throw new BadRequestException(trans("locale.student_with_id") ." ". $studentId ." ". trans("locale.already_in_campus_round"));
                    $enroll=$this->professionalRepository->checkStudentInRunningCampuses($studentId,$rounds_id,$round_id,$gmt_difference);
                    if($enroll==true) {
                        throw new BadRequestException(trans("locale.student_with_id") . " " . $studentId . " " . trans("locale.already_in_another_round_of_campus"));
                    }
                     
                    $campUser = new CampusUser($campusRound, $student, $role);
                    $this->professionalRepository->storeCampusRoundUser($campUser);
                    
                        //Add Campus User Status records
                        $this->lessonCommonService->updateCampusUserStatus($student->id,$campus_id,0,0);

                    $newStudentsCount++;

                }else{
                    throw new BadRequestException(trans("locale.student_with_id") ." ". $studentId ." ". trans("locale.not_in_same_account"));
                }
            }
        }
        $campusRound->setNoOfStudents($newStudentsCount);
        $this->professionalRepository->storeCampusRound($campusRound);

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.student_updated_successfully");
    }

    public function setAssignedAndUnassignedTeachersInCampusRound($user_id,$round_id, $unAssignedTeachers, $assignedTeachers,$gmt_difference){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        $account = $data['account'];

        $campusRound = $this->professionalRepository->getRoundById($round_id);
        if($campusRound == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        $campus = $campusRound->getCampus();
        $campus_id=$campus->getId();
        if($campus == null)
            throw  new BadRequestException(trans("locale.campus_not_exist")); //TODO translations
        $schoolCampus = $campus->getSchoolCampus();
        if($schoolCampus == null)
            throw  new BadRequestException(trans("locale.school_campus_not_exist"));

//        $campusInSchool = $this->professionalRepository->checkIfCampusInSchool($campus_id, $school->getId());
//        if(!$campusInSchool)
//            throw new BadRequestException(trans("locale.campus_not_exist_school"));


        $status = $campusRound->getStatus($gmt_difference);
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));

        //for assigning students
        $role = $this->accountRepository->getRoleByName("teacher");
        if($role == null)
            throw new BadRequestException(trans("locale.no_role_same_teacher"));


        if(count($unAssignedTeachers) > 0){
            foreach ($unAssignedTeachers as $unAssignedTeacher){

                $teacher = $this->accountRepository->getUserById($unAssignedTeacher);
                if($teacher === null)
                    throw new BadRequestException(trans("locale.teacher_with_id") ." ". $unAssignedTeacher ." ". trans("locale.doesn't_exist"));

                $teacherInCampusRound = $this->professionalRepository->checkIfTeacherInCampusRound($round_id, $unAssignedTeacher);
                if($teacherInCampusRound){
                    //check same account
                    if($teacher->getAccount()->getId()===$account->getId()){
                        $campusUser = $this->professionalRepository->getCampusRoundUser($round_id, $unAssignedTeacher);
                        $this->professionalRepository->deleteCampusRoundUser($campusUser);


                    }else{
                        throw new BadRequestException(trans("locale.teacher_with_id") ." ". $unAssignedTeacher ." ". trans("locale.not_in_same_account"));
                    }
                }else{
                    throw new BadRequestException(trans("locale.teacher_with_id") ." ". $unAssignedTeacher ." ". trans("locale.not_in_campus_round"));
                }

            }
        }

        if(count($assignedTeachers) > 0){
            foreach ($assignedTeachers as $teacherId){
                $teacher = $this->accountRepository->getUserById($teacherId);
                if($teacher == null)
                    throw new BadRequestException(trans("locale.teacher_with_id") . $teacherId . trans("locale.doesn't_exist"));

                $isTeacher = $this->checkUserRole($teacher,'teacher');
                if(!$isTeacher)
                    throw new BadRequestException(trans("locale.teacher_with_id") ." ". $teacherId ." ". trans("locale.not_teacher"));

                if($teacher->getAccount()->getId()===$account->getId()){
                    $teacherInRound=$this->professionalRepository->getCampusRoundUser($round_id,$teacherId);
                    if($teacherInRound!==null)
                        throw new BadRequestException(trans("locale.teacher_with_id") ." ". $teacherId ." ". trans("locale.already_in_campus_round"));

                    $campUser = new CampusUser($campusRound, $teacher, $role);
                    $this->professionalRepository->storeCampusRoundUser($campUser);

                }else{
                    throw new BadRequestException(trans("locale.teacher_with_id") ." ". $teacherId ." ". trans("locale.not_in_same_account"));
                }

            }
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.teacher_updated_successfully");
    }

    public function createCampusRound(CampusRoundRequestDto $campusRoundDto, $user_id,$is_default=true){
        // dd($user_id);
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        $account = $data['account'];
        $campus = $this->professionalRepository->getCampusById($campusRoundDto->campus_id);
        if($campus == null)
            throw  new BadRequestException(trans("locale.campus_not_exist")); //TODO translations

        if(!$this->accountRepository->isAuthorized('create_Classroom', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $campusInSchool = $this->professionalRepository->checkIfCampusInSchool($campusRoundDto->campus_id, $school->getId());
        if(!$campusInSchool){
            //throw new BadRequestException(trans("locale.campus_doesnot_exist_school"));
            $schoolCampus=new SchoolCampus($school,$campus,$user,count($campus->getClasses()),'school_admin');
            $this->professionalRepository->storeSchoolCampus($schoolCampus);
        }

        if($campusRoundDto->ends_at <= $campusRoundDto->starts_at)
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));


        $campus_user_round = $this->professionalRepository->getCampusUserRound($campusRoundDto->campus_id,$user_id);
        // dd($campus_user_round);
        $campus_name=$campus->getName();
        //$campus_round_num=count($campus_user_round);
        //$round_name= $campus_name . ' - ' . ($campus_round_num+1);
        // dd($round_name);

        $this->accountRepository->beginDatabaseTransaction();
        
            if($this->professionalRepository->checkRoundNameAvailability($campusRoundDto->campus_id,$campusRoundDto->round_name,$user)){
                throw new BadRequestException(trans("locale.round_name_exists"));
            };
        $campusRound = new CampusRound($campus,$campusRoundDto->starts_at,$campusRoundDto->ends_at, count($campusRoundDto->students),$campusRoundDto->round_settings,Uuid::uuid4()->toString(),$campusRoundDto->round_name);
        $this->professionalRepository->storeCampusRound($campusRound);
        //assign this admin to campusRound
        $adminRole=$this->accountRepository->getRoleByName('school_admin');
        $campusUser=new CampusUser($campusRound,$user,$adminRole);
        $this->professionalRepository->storeCampusRoundUser($campusUser);

            if(count($campusRoundDto->classes)>0)
                $this->addCampusRoundClass($campusRound,$campusRoundDto->classes,$is_default);

            if(count($campusRoundDto->teachers)>0)
                $this->addTeachersToCampusRound($campusRound,$campusRoundDto->teachers,$account);

        $this->addStudentsToCampusRound($campusRound,$campusRoundDto->students,$account);

        $this->accountRepository->commitDatabaseTransaction();

        

        $campMapped = Mapper::MapClass(AdminCampusRoundDto::class, $campusRound,[CampusUserDto::class,CampusUserDto::class]);

        return $campMapped;
    }

    public function getRoundData($user_id,$round_id,$gmt_difference,$is_default=true){
        //TODO:Comment/Uncomment
        //$bbb = new BigBlueButton();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $campusRound = $this->professionalRepository->getRoundById($round_id);
        if($campusRound == null)
            throw  new BadRequestException(trans("locale.campus_round_not_exist"));


        $campus_id=$campusRound->getCampus()->getId();
        $campusInSchool = $this->professionalRepository->checkIfCampusInSchool($campus_id, $school->getId());
        if(!$campusInSchool)
            throw new BadRequestException(trans("locale.school_campus_not_exist"));

        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        $checkStudentInRound=$campusService->checkUserInRound($user->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));

        $roundClasses = $this->professionalRepository->getRoundClasses($round_id);

        $classDtos=[];
        $roundDto['id']=$campusRound->getId();
        $roundDto['icon_url'] = $campusRound->getCampus()->getIconUrl();
        $roundDto['name'] = $campusRound->getCampus()->getName();
        $roundDto['starts_at']=$campusRound->getStartsAt();
        $roundDto['ends_at']=$campusRound->getEndsAt();
        $roundDto['duration']=$campusRound->getDuration();
        $roundDto['time_estimate']=$campusRound->getCampus()->getEstimateTime();
        $roundDto['has_instructor']=$campusRound->hasInstructor();
        $roundDto['status']=$campusRound->getStatus($gmt_difference);
        $roundDto['students_no']=$campusRound->getNoOfStudents();
        
        foreach ($roundClasses as $roundClass){
            $class=$roundClass->getCampusClass();

                 // get lesson type
            if($class->lessonType() == 0)
            {  $lessonType="normal";
            }
            elseif($class->lessonType() == 1)
            {  $lessonType="project";
            }
            elseif($class->lessonType() == 2)
            {  $lessonType="welcome";
            }else
            {  $lessonType=$class->lessonType();
            }

            $classDto=[];
            $classDto['id']=$class->getId();
            $classDto['round_class_id'] = $roundClass->getId();

            if($is_default==true) {


                $classDto['activity_id'] = $class->getDefaultActivity()->getId();
                $classDto['description'] = $class->getDefaultActivity()->getDescription();
                $classDto['name']=$class->getDefaultActivity()->getName();
                $classDto['icon']=$class->getDefaultActivity()->getIconUrl();
            }
            else {
                $classDto['activity_id'] = $class->getActivity()->getId();
                $classDto['description'] = $class->getActivity()->getDescription();
                $classDto['name']=$class->getActivity()->getName();
                $classDto['icon']=$class->getActivity()->getIconUrl();
            }

            $classDto['starts_at']=$roundClass->getStartsAt();
            $classDto['ends_at']=$roundClass->getEndsAt();
            $classDto['duration']=$roundClass->getDuration();
            $classDto['time_estimate']=$class->getTimeEstimate();
            $classDto['weight']=$roundClass->getWeight();
            $classDto['order']=$class->getOrder();
            $classDto['due_date']=$roundClass->getDueDate();
            if($class->lessonType() == 1){
                $classDto['video_url'] = $class->getProject()->getVideoUrl();
            }
            // $classDto['is_project'] = $class->isProject();
            $classDto['moduleType'] = $lessonType;
            if($is_default==true)
                $activity_id=$class->getDefaultActivity()->getId();
            else
                $activity_id=$class->getActivity()->getId();
            $classDtos[]=$classDto;
        }
        $roundDto['classes']=$classDtos;

        $unSortedClasses = $classDtos;
        usort($unSortedClasses,array($this, 'tasks_cmp'));
        $roundDto['classes']=$unSortedClasses;
        return $roundDto;

    }

    public function getRoundDataWithoutHtml($user_id,$round_id,$gmt_difference,$is_default=true){
        //TODO:Comment/Uncomment
        $bbb = new BigBlueButton();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];
        if( !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $campusRound = $this->professionalRepository->getRoundById($round_id);
        if($campusRound == null)
            throw  new BadRequestException(trans("locale.campus_round_not_exist"));


        $campus_id=$campusRound->getCampus()->getId();
        $campusInSchool = $this->professionalRepository->checkIfCampusInSchool($campus_id, $school->getId());
        if(!$campusInSchool)
            throw new BadRequestException(trans("locale.school_campus_not_exist"));

        $campusService = new CampusService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);

        $checkStudentInRound=$campusService->checkUserInRound($user->getId(),$round_id);
        if(!$checkStudentInRound)
            throw new BadRequestException(trans("locale.user_not_in_this_round"));


        $roundClasses = $this->professionalRepository->getRoundClasses($round_id);

        $classDtos=[];
        $roundDto['id']=$campusRound->getId();
        $roundDto['starts_at']=$campusRound->getStartsAt();
        $roundDto['ends_at']=$campusRound->getEndsAt();
        $roundDto['duration']=$campusRound->getDuration();
        $roundDto['status']=$campusRound->getStatus($gmt_difference);
        $roundDto['students_no']=$campusRound->getNoOfStudents();
        $roundDto['hasInstructor']=$campusRound->hasInstructor();
        foreach ($roundClasses as $roundClass){

            $class=$roundClass->getCampusClass();
            $classDto=[];
            $classDto['id']=$class->getId();
            $classDto['round_class_id'] = $roundClass->getId();

            if($is_default==true) {
                $activity_id=$class->getDefaultActivity()->getId();
                $classDto['activity_id'] = $class->getDefaultActivity()->getId();
                $classDto['name']=$class->getDefaultActivity()->getName();
                $classDto['icon']=$class->getDefaultActivity()->getIconUrl();
            }
            else {
                $activity_id=$class->getActivity()->getId();
                $classDto['activity_id'] = $class->getActivity()->getId();
                $classDto['name']=$class->getActivity()->getName();
                $classDto['icon']=$class->getActivity()->getIconUrl();
            }

            $classDto['starts_at']=$roundClass->getStartsAt();
            $classDto['ends_at']=$roundClass->getEndsAt();
            $classDto['duration']=$roundClass->getDuration();
            $classDto['weight']=$roundClass->getWeight();
            $classDto['order']=$class->getOrder();
            $classDto['due_date']=$roundClass->getDueDate();

            $rooms = $roundClass->getRooms()->toArray();
            $classDto["sessions"]=$rooms;
            $class_tasks = $this->professionalRepository->getCampusClassTasks($activity_id,$class->getCampus()->getId(),$is_default);

            $class_due_date=$roundClass->getDueDate();

            $tasksDto=[];
            foreach ($class_tasks as $class_task){
                $task=$class_task->getTask();
                $campusRoundActivityTask = $this->professionalRepository->getRoundActivityTask($round_id, $class_task->getId());

                if($task->getMissions()->first() != null) {
                    $mission = $task->getMissions()->first();

                    array_push($tasksDto, [
                        'task_id' => $task->getId(),
                        'mission_id' => $mission->getId(),
                        'task_name' => $mission->getName(),
                        'type' => "mission",
                        'image' => $mission->getIconUrl(),
                        'due_date'=>$campusRoundActivityTask->getDueDate(),
                        'weight'=>$campusRoundActivityTask->getWeight(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }

                elseif($task->getQuizzes()->first()!=null) {
                    $quiz = $task->getQuizzes()->first();

                    array_push($tasksDto, [
                        'task_id' => $task->getId(),
                        'mission_id' => $quiz->getId(),
                        'task_name' => $quiz->getTitle(),
                        'type'=>$quiz->getType()->getName(),
                        'image' => $quiz->getIconUrl(),
                        'due_date'=>$campusRoundActivityTask->getDueDate(),
                        'weight'=>$campusRoundActivityTask->getWeight(),
                        'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }
                elseif($task->getMissionsCoding()->first()!=null){
                    $codingMission=$task->getMissionsCoding()->first();

                    array_push($tasksDto, [
                        'task_id' => $task->getId(),
                        'mssion_id' => $codingMission->getId(),
                        'task_name' => $codingMission->getTitle(),
                        'type'=>"coding_mission",
                        'image' => $codingMission->getIconUrl(),
                        'model_answer' => $codingMission->getModelAnswer(),
                        'due_date'=>$campusRoundActivityTask->getDueDate(),
                        'weight'=>$campusRoundActivityTask->getWeight(),
                        'order' => $codingMission->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }
                elseif($task->getMissionsEditor()->first()!=null){
                    $editorMission=$task->getMissionsEditor()->first();

                    array_push($tasksDto, [
                        'task_id' => $task->getId(),
                        'mission_id' => $editorMission->getId(),
                        'task_name' => $editorMission->getTitle(),
                        'type'=>"editor_mission",
                        'image' => $editorMission->getIconUrl(),
                        'model_answer' => $editorMission->getModelAnswer(),
                        'due_date'=>$campusRoundActivityTask->getDueDate(),
                        'weight'=>$campusRoundActivityTask->getWeight(),
                        'order' => $editorMission->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }elseif($task->getMissionsHtml()->first() != null && $task->getMissionsHtml()->first()->getHtmlMissionType()=='mini_project'  ){
                    $htmlMission=$task->getMissionsHtml()->first();
                    array_push($tasksDto, [
                        'task_id' => $task->getId(),
                        'mission_id' => $htmlMission->getId(),
                        'task_name' => $htmlMission->getTitle(),
                        'type'=>"mini_project",
                        'image' => $htmlMission->getIconUrl(),
                        'due_date'=>$campusRoundActivityTask->getDueDate(),
                        'weight'=>$campusRoundActivityTask->getWeight(),
                        'order' => $htmlMission->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);

                }
                $classDto['tasks']=$tasksDto;
            }
            $unordered_tasks=  $tasksDto;
            $classDto['tasks']=$this->sortClassTasks($unordered_tasks);

            /*$classRooms=$roundClass!==null?$roundClass->getRooms():[];
            $classDto['recordedSessions']=null;
            $classDto['runningSession']=null;
            //TODO:Comment/Uncomment

            foreach($classRooms as $classRoom) {
                //GET RECORDING ROOMS
                $recordingParams = new GetRecordingsParameters();
                $recordingParams->setMeetingId($classRoom->id);
                $response = $bbb->getRecordings($recordingParams);
                $records = [];
                if ($response->getReturnCode() == 'SUCCESS') {
                    foreach ($response->getRecords()  as $recording) {
                        // process all recording
                        $record_['url'] = $recording->getPlaybackUrl();
                        $record_['seesion_id'] = $classRoom->id;
                        $record_['session_name'] = $classRoom->name;
                        $records []= $record_;
                    }
                }
                if(count($records)>0)
                    $classDto['recordedSessions'] []= $records[0];
                //GET RUNNING ROOMS
                $isRunningParams = new IsMeetingRunningParameters($classRoom->id);
                $isRunning = $bbb->isMeetingRunning($isRunningParams)->isRunning();
                if($isRunning) {
                    $room_['seesion_id'] = $classRoom->id;
                    $room_['session_name'] = $classRoom->name;
                    $classDto['runningSession'] = $room_;
                }

            }
*/
            $classDtos[]=$classDto;

        }
        $roundDto['classes']=$classDtos;

        $unSortedClasses = $classDtos;

        usort($unSortedClasses,array($this, 'tasks_cmp'));
        $roundDto['classes']=$unSortedClasses;
        return $roundDto;

    }

    public function getAdminStudents($user_id ,$start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference,$count=null){

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        //$students
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        if($count===null) {
            $students = $this->professionalRepository->getAllStudentsInCampuses($school->getId(), $limit, $search,$order_by,$order_by_type,$gmt_difference,null);
            //dd($students);
            $studentsDto = [];
            foreach ($students as $student) {
                $studentDto = [];
                $studentDto['id'] = $student->getId();
                $studentDto['fname'] = $student->getFirstName();
                $studentDto['lname'] = $student->getLastName();
                $studentDto['email'] = $student->getEmail();
                $studentDto['profile_image'] = $student->getImage();
                $studentDto['username'] = $student->getUsername();
                //$rounds = $student->getCampusRounds();
                $rounds=$this->professionalRepository->getStudentRunningRounds($student->getId(),$gmt_difference);
                $roundsDto = [];
                foreach ($rounds as $round) {
                    $roundDto = [];
                    $roundDto['id'] = $round->getId();
                    $roundDto['campus_id'] = $round->getCampus()->getId();
                    $roundDto['campusName'] = $round->getCampusName();
                    $roundDto['round_name'] = $round->getRound_name();
                    //$campusTasksNum = $this->professionalRepository->getAllTasksInCampus($round->getCampus()->getId(), true);
                    $campusTasksNum = $this->professionalRepository->getAvailaibleTasksInCampus($user, $round, true);
//                    $solvedTasks = $this->professionalRepository->getStudentSolvedTasksInCampus($student->getId(), $round->getCampus()->getId(),true);
                    $solvedTasks = $this->professionalRepository->getSolvedTasksInRound($round, $student);
                        $roundDto['completed_missions_count'] =  $solvedTasks;
                        $roundDto['all_missions_count']= $campusTasksNum;
                        $roundDto['progress']= round(($solvedTasks / $campusTasksNum) * 100 );

                        $CampusUserStatus=$this->professionalRepository->getCampusUserStatus($round->getCampus()->getId(),$student->getId());
                   if($CampusUserStatus!=null)
                   {
                        $score=$CampusUserStatus->getXps();
                   }else{
                       $score=0;
                   }
                    $roundDto['score'] = $score;
                    $roundsDto[] = $roundDto;

                }
                $studentDto['rounds'] = $roundsDto;
                $studentsDto[] = $studentDto;
            }

            return $studentsDto;
        }
        else{
            $students = $this->professionalRepository->getAllStudentsInCampuses($school->getId(), $limit, $search,$order_by,$order_by_type,$gmt_difference,true);
            return ['count'=>$students];
        }
        //return Mapper::MapEntityCollection(CampusUserDto::class, $students, [UserCampusRoundDto::class]);
    }

    public function getCampusAdminStudents($user_id ,$campus_id, $start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference,$count=null){
        //dd($user_id ,$campus_id, $start_from, $limit, $search,$order_by,$order_by_type,$gmt_difference);
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $campus = $this->professionalRepository->getCampusById($campus_id);
        if($campus == null)
            throw  new BadRequestException(trans("locale.campus_not_exist")); //TODO translations

        $rounds = $campus->getRounds();

        $rounds_id = [];


        foreach ($rounds as $round){
            if($round->getStatus($gmt_difference) == 'running' || $round->getStatus($gmt_difference) == 'locked')
                $rounds_id[] = $round->getId();
        }

        //$students
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $students = $this->professionalRepository->getAllStudentsNotInRounds($school->getId(), $rounds_id, $limit, $search,$order_by,$order_by_type,$count);
        // return $students;
        //dd($students);
        if($count===null) {
            $studentsDto = [];
            foreach ($students as $student) {
                $studentDto = [];
                $studentDto['id'] = $student->getId();
                $studentDto['fname'] = $student->getFirstName();
                $studentDto['lname'] = $student->getLastName();
                $studentDto['email'] = $student->getEmail();
                $studentDto['profile_image'] = $student->getImage();
                $studentDto['username'] = $student->getUsername();
                $rounds = $student->getCampusRounds();
//                $roundsDto = [];
//                foreach ($rounds as $round) {
//                    $roundDto = [];
//                    $roundDto['id'] = $round->getId();
//                    $roundDto['campusName'] = $round->getCampusName();
//                    $campusTasksNum = $this->professionalRepository->getAllTasksInCampus($round->getCampus()->getId(), true);
//                    $solvedTasks = $this->professionalRepository->getStudentSolvedTasksInCampus($student->getId(), $round->getCampus()->getId());
//                    if ($campusTasksNum != 0)
//                        $roundDto['progress'] = intval((count($solvedTasks) / $campusTasksNum) * 100) . '%';
//                    //TODO:UnComment
//                    else {
//                        dd($round,$campusTasksNum);
//                        throw new UnauthorizedException(trans("locale.campus_has_no_tasks"));
//                    }
//                    $evaluation = 0;
//                    foreach ($solvedTasks as $task) {
//                        $eval = $task->getEvaluation();
//                        $campusActivityTask = $this->professionalRepository->getCampusActivityTask($round->getCampus()->getId(), $task->getTask()->getId());
//                        $activity = $campusActivityTask->getDefaultActivity();
//                        $class = $this->professionalRepository->getCampusClass($round->getCampus()->getId(), $activity->getId());
//                        $roundClass = $this->professionalRepository->getCampusRoundClass($round->getId(), $class->getId());
//                        $roundActivityTask = $this->professionalRepository->getRoundActivityTask($round->getId(), $campusActivityTask->getId());
//                        if ($roundActivityTask !== null) {
//                            $eval = $eval * $roundActivityTask->getWeight() / 100; //task weight in round
//                            $eval = $eval * $roundClass->getWeight() / 100; //class weight in round
//                            $evaluation += intval($eval);
//
//                        } //TODO:UnComment
//                        else
//                            throw new UnauthorizedException(trans("locale.round_activity_task_does_not_exist"));
//
//
//                    }
//                    $roundDto['evaluation'] = $evaluation;
//                    $roundsDto[] = $roundDto;
//
//                }
//                $studentDto['rounds'] = $roundsDto;
                $studentsDto[] = $studentDto;
            }
            return $studentsDto;
        }
        else{
            return ['count'=>count($students)];
        }
        //return Mapper::MapEntityCollection(CampusUserDto::class, $students, [UserCampusRoundDto::class]);
    }

    public function getAdminTeachers($user_id ,$start_from, $limit, $search,$order_by,$order_by_type,$count=null){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AllTeachers',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
        //$students
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $teachers = $this->professionalRepository->getAllTeachersInCampuses($school->getId(), $limit, $search,null,$order_by,$order_by_type,$count);

        if($count===null)
            return Mapper::MapEntityCollection(CampusUserDto::class, $teachers, [UserCampusRoundDto::class]);
        else
            return ['count'=>$teachers];
    }

    public function updateCampusStudent($user_id,UserDto  $studentDto){
        $this->accountRepository->beginDatabaseTransaction();

        $loggedInUser = $this->accountRepository->getUserById($user_id);
        if($loggedInUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('update_Student',$loggedInUser))
            throw new UnauthorizedException(trans("locale.no_permission_operation"));

        $school = $loggedInUser->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $student = $this->accountRepository->getUserById($studentDto->id);
        if($student == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        if($student->getUsername() != $studentDto->username){
            $existingUsername = $this->professionalRepository->UserNameAlreadyExist($studentDto);
            if($existingUsername)
                throw new BadRequestException(trans('locale.username_exists'));
        }

        if($student->getAccount()->getId() != $loggedInUser->getAccount()->getId()){
            throw new BadRequestException(trans("locale.student_school_mismatch"));
        }

        $studentRoles = $loggedInUser->getRoles();
        $isStudentLoggedIn = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudentLoggedIn = true;
        }

        $studentRoles = $student->getRoles();
        $isStudent = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudent = true;
        }

        if(!$isStudent)
            throw new BadRequestException(trans("locale.user_not_student"));


        if($isStudentLoggedIn && $isStudent && ($user_id != $studentDto->id))
            throw  new UnauthorizedException(trans("locale.cant_update_another_student_data"));

        $student->setFirstName($studentDto->fname);
        $student->setLastName($studentDto->lname);
        $student->setUsername($studentDto->username);


        if($studentDto->password != null)
            $student->setpassword(bcrypt($studentDto->password));

        $role = $this->accountRepository->getRoleByName('student');
        if($role == null)
            throw  new BadRequestException(trans("locale.role_not_found"));

        $this->accountRepository->storeUser($student);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(CampusUserDto::class, $student, [UserCampusRoundDto::class]);

    }

    public function updateCampusTeacher($user_id,UserDto  $teacherDto){
        $this->accountRepository->beginDatabaseTransaction();

        $loggedInUser = $this->accountRepository->getUserById($user_id);
        if($loggedInUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('update_Student',$loggedInUser))
            throw new UnauthorizedException(trans("locale.no_permission_operation"));

        $school = $loggedInUser->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $teacher = $this->accountRepository->getUserById($teacherDto->id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        if($teacher->getUsername() != $teacherDto->username){
            $existingUsername = $this->professionalRepository->UserNameAlreadyExist($teacherDto);
            if($existingUsername)
                throw new BadRequestException(trans('locale.username_exists'));
        }
        if($teacherDto->email!=null && ($teacher->getEmail() != $teacherDto->email)){
            $existingUserMail = $this->accountRepository->AlreadyRegistered($teacherDto);
            if($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));
        }

        if($teacher->getAccount()->getId() != $loggedInUser->getAccount()->getId()){
            throw new BadRequestException(trans("locale.teacher_school_mismatch"));
        }

        $studentRoles = $loggedInUser->getRoles();
        $isStudentLoggedIn = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudentLoggedIn = true;
        }

        $studentRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "teacher")
                $isTeacher = true;
        }

        if(!$isTeacher)
            throw new BadRequestException(trans("locale.user_not_student"));


        if($isStudentLoggedIn && $isTeacher && ($user_id != $teacherDto->id))
            throw  new UnauthorizedException(trans("locale.cant_update_another_student_data"));

        $teacher->setFirstName($teacherDto->fname);
        $teacher->setLastName($teacherDto->lname);
        $teacher->setUsername($teacherDto->username);
        $teacher->setEmail($teacherDto->email);


        if($teacherDto->password != null)
            $teacher->setpassword(bcrypt($teacherDto->password));

        $role = $this->accountRepository->getRoleByName('teacher');
        if($role == null)
            throw  new BadRequestException(trans("locale.role_not_found"));

        $this->accountRepository->storeUser($teacher);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(CampusUserDto::class, $teacher, [UserCampusRoundDto::class]);

    }

    public function checkSchoolData($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.no_school_attached"));

        return ['user' => $user, 'account' => $account, 'school' => $school];
    }

    public function addCampusRoundClass(CampusRound $round, array $classes,$is_default=true){
        $counter=0;
        $campusClassesCount=count($round->getCampus()->getClasses());
        $classesCount=count($classes);
        if($campusClassesCount!==$classesCount)
            throw new BadRequestException(trans("locale.class_count_mismatch"));
        $roundClassWeight=intval(100/$campusClassesCount);
        $lastRoundClassWeight=$roundClassWeight+(100-($campusClassesCount*$roundClassWeight));
        foreach ($classes as $roundClass){
            if(!empty($roundClass['id'])){
                $classId=$roundClass['id'];
                $class=$this->professionalRepository->getCampusClassById($classId);
                if($class==null)
                    throw new BadRequestException(trans("locale.class_not_exist"));
            }

            else
                throw new BadRequestException(trans("locale.class_id_not_found"));
            if(!empty($roundClass['starts_at']))
                $roundClassStartsAt=$roundClass['starts_at'];
            else
                $roundClassStartsAt=$round->getStartsAt();
            if(!empty($roundClass['ends_at']))
                $roundClassEndsAt=$roundClass['ends_at'];
            else
                $roundClassEndsAt=$round->getEndsAt();
            if(!empty($roundClass['due_date']))
                $roundClassDueDate=$roundClass['due_date'];
            else
                $roundClassDueDate=null;

            /*if(!empty($roundClass['weight']))
                $roundClassWeight=$roundClass['weight'];
            else
                throw new BadRequestException(trans("locale.class_weight_not_exist"));*/

            if($roundClass['ends_at'] <= $roundClass['starts_at'])
                throw new BadRequestException(trans("locale.end_date_start_date_min_time"));
            //check class in campus
            $checkClassInCampus=$this->professionalRepository->checkClassInCampus($classId,$round->getCampus()->getId());
            if(!$checkClassInCampus)
                throw new BadRequestException(trans("locale.class_id")." ". $classId." " .trans("locale.is_not_in_campus"));

            $class = $this->professionalRepository->getCampusClassById($roundClass['id']);
            if($class == null)
                throw new BadRequestException(trans("locale.class_id"). $classId .trans("locale.doesn't_exist"));
            if($counter===$campusClassesCount-1)
                $weight=$lastRoundClassWeight;
            else
                $weight=$roundClassWeight;

            $campusRoundClass = new CampusRoundClass($class, $round,$weight,$roundClassStartsAt,$roundClassEndsAt,$roundClassDueDate);
            //get activity_tasks and store CampusRoundActivityTasks
            if($is_default==true) {
                $activity_id = $class->getDefaultActivity()->getId();
                $activity = $class->getDefaultActivity();
            }
            else {
                $activity_id = $class->getActivity()->getId();
                $activity = $class->getActivity();
            }
            $activity_tasks=$this->professionalRepository->getCampusActivityTasks($activity_id,$round->getCampus()->getId(),$is_default);
            $classTasksCount=0;
            foreach ($activity_tasks as $activity_task){
                $taskNameAndType=$activity_task->getTask()->getTaskNameAndType();
                if($taskNameAndType['task_type']!='html_mission')
                    $classTasksCount++;
            }
            if($classTasksCount>0){
                $classTaskWeight=intval(100/$classTasksCount);
                $lastTaskWeight=$classTaskWeight+(100-($classTasksCount*$classTaskWeight));

            }else{
                $classTaskWeight=0;
                $lastTaskWeight=0;
            }
            //dd($classTaskWeight,$lastTaskWeight);

            $tasksCounter=0;
            foreach ($activity_tasks as $activity_task){
                $taskDueDate=$roundClassDueDate;
                if($activity_task->getTask()->getTaskNameAndType()['task_type']=='html_mission'){
                    $taskWeight=0;
                    $taskDueDate=null;
                }

                else{
                    if($tasksCounter===$classTasksCount-1)
                        $taskWeight=$lastTaskWeight;
                    else
                        $taskWeight=$classTaskWeight;
                    $tasksCounter++;
                }

                $campusRoundActivityTask=new CampusRoundActivityTask($activity_task,$round,$taskWeight,null,$taskDueDate);
                $this->professionalRepository->storeCampusRoundActivityTask($campusRoundActivityTask);
            }

            //TODO:Comment/Uncomment
            /*
            $room_starts_at=(intval($roundClassStartsAt + $roundClassEndsAt)/2);
            $room = new Room($campusRoundClass);
            $roomCounter = $counter+1;
            $room->setName('Session #'.$roomCounter);
            $room->setStartsAt($room_starts_at);
            $this->professionalRepository->storeRoom($room);

            $bbb = new BigBlueButton();
            $createMeetingParams = new CreateMeetingParameters($room->getId(), $activity->getName());
            $createMeetingParams->setAttendeePassword($this::Attendee_Password);
            $createMeetingParams->setModeratorPassword($this::Moderator_Password);
            $isRecordingTrue = true;
            if ($isRecordingTrue) {
                $createMeetingParams->setRecord(true);
                $createMeetingParams->setAllowStartStopRecording(true);
                $createMeetingParams->setAutoStartRecording(true);
            }
            $bbbResponse = $bbb->createMeeting($createMeetingParams);
            if ($bbbResponse->getReturnCode() == 'FAILED')
                throw new BadRequestException(trans("locale.cannot_create_room")); //TODO translation
            $room->setBBBId($bbbResponse->getInternalMeetingId());
            $this->professionalRepository->storeRoom($room);
*/

            $this->professionalRepository->storeCampusRoundClass($campusRoundClass);
            $counter++;
        }
    }

    public function addCampusRoundTask(CampusRound $round, array $tasks){
        $counter=0;
        foreach ($tasks as $classTask){
            if(!empty($classTask['id'])){
                $classTaskId=$classTask['id'];
                $activityTask=$this->professionalRepository->getCampusActivityTask($round->getCampus()->getId(),$classTaskId);
                if($activityTask==null)
                    throw new BadRequestException(trans("locale.activity_task_not_exist"));
            }

            else
                throw new BadRequestException(trans("locale.activity_task_id_not_found"));

            if(!empty($classTask['due_date']))
                $activityTaskDueDate=$classTask['due_date'];
            else
                $activityTaskDueDate=null;//TODO:make it class Due Date

            if(!empty($classTask['weight']))
                $activityTaskWeight=$classTask['weight'];
            else
                throw new BadRequestException(trans("locale.activity_task_weight_not_found"));

            if(!empty($classTask['weight_overdue']))
                $activityTaskWeightOverdue=$classTask['weight_overdue'];

            $campusRoundActivityTask=new CampusRoundActivityTask($activityTask,$round,$activityTaskWeight,$activityTaskWeightOverdue,$activityTaskDueDate);
            $this->professionalRepository->storeCampusRoundActivityTask($campusRoundActivityTask);

        }
    }

    public function addTeachersToCampusRound(CampusRound $campusRound, $teachers,Account $account){

        foreach ($teachers as $teacherInstance){
            $teacherId=$teacherInstance['id'];

            $teacher = $this->accountRepository->getUserById($teacherId);
            if($teacher == null)
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans("locale.doesn't_exist"));

            $isTeacher=$this->checkUserRole($teacher,'teacher');
            if(!$isTeacher)
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans("locale.not_teacher"));

            if($campusRound->isMember($teacher->getId()))
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans('locale.already_in_campus_round'));

            $role = $this->accountRepository->getRoleByName("teacher");
            if($role == null)
                throw new BadRequestException(trans("locale.no_role_teacher"));

            if($teacher->getAccount()->getId()===$account->getId()){

                $campusUser = new CampusUser($campusRound, $teacher, $role);
                $this->professionalRepository->storeCampusRoundUser($campusUser);
            }else{
                throw new BadRequestException(trans("locale.teacher_with_id") ." ". $teacherId ." ". trans("locale.not_in_same_account"));
            }
        }

    }

    public function addStudentsToCampusRound(CampusRound $campusRound, $students,Account $account){
        $student_num=0;
        if(count($students)>0) {
            foreach ($students as $studentInstance) {
                $studentId=$studentInstance['id'];
                $student = $this->accountRepository->getUserById($studentId);
                if ($student == null)
                    throw new BadRequestException(trans('locale.student_with_id') . " " . $studentId . " " . trans("locale.doesn't_exist"));

                $isStudent = $this->checkUserRole($student, 'student');
                if (!$isStudent)
                    throw new BadRequestException(trans('locale.student_with_id') . " " . $studentId . " " . trans("locale.is_not_student"));

                if ($campusRound->isMember($student->getId()))
                    throw new BadRequestException(trans('locale.student_with_id') . " " . $studentId . " " . trans('locale.already_in_campus_round'));

                $role = $this->accountRepository->getRoleByName("student");
                if ($role == null)
                    throw new BadRequestException(trans("locale.no_role_same_student"));
                if ($student->getAccount()->getId() === $account->getId()) {

                    $campusUser = new CampusUser($campusRound, $student, $role);
                    $this->professionalRepository->storeCampusRoundUser($campusUser);

                    //Add Campus User Status records
                    $this->lessonCommonService->updateCampusUserStatus($studentId,$campusRound->getCampus_id(),0,0);
            
                    $student_num++;
                } else {
                    throw new BadRequestException(trans("locale.student_with_id") . " " . $studentId . " " . trans("locale.not_in_same_account"));
                }
            }
        }
    }

    public function deleteStudents($user_id,$students,$force=false)
    {
        $force=$force==null?false:$force;
        $force=filter_var($force, FILTER_VALIDATE_BOOLEAN);
        //$this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Student',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $has_progress=$this->checkStudentsHaveProgress($students);
        if($has_progress==true && !$force)
            return  ['error'=>true,'error_type'=>'progress','message'=>trans("locale.student_has_progress")];


        foreach ($students as $student){
            $this->deleteStudent($school,$student,$force);
        }

        return  ['error'=>false,'message'=>trans("locale.student_delete_successfull")];
    }
    private function checkStudentsHaveProgress($students)
    {

        foreach ($students as $student_id){
            $student = $this->accountRepository->getUserById($student_id);
            //dd($student_id);
            if($student == null)
                throw new BadRequestException(trans("locale.student_doesn't_exist"));
            $arr=[];
            foreach ($student->getCampusProgress() as $p)
                array_push($arr,$p->getId());

            if(count($arr)>0 )
                return true;

        }
        return false;

    }


    public function deleteStudent(School $userSchool,$student_id,$force=false)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $student = $this->accountRepository->getUserById($student_id);
        //dd($student_id);
        if($student == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        $studentSchool = $student->getAccount()->getSchool();
        if($studentSchool == null)
            throw new BadRequestException(trans("locale.student_no_school"));

        if($studentSchool->getId() != $userSchool->getId())
            throw new BadRequestException(trans("locale.student_school_mismatch"));

        $studentRoles = $student->getRoles();
        $isStudent = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudent = true;
        }
        if(!$isStudent)
            throw  new BadRequestException(trans("locale.student_wrong_role"));
        //dd($student->getCampusProgress());
//        $arr=[];
//        foreach ($student->getCampusProgress() as $p)
//            array_push($arr,$p->getId());
//
//        if(count($arr)>0 &&!$force){
//            return  ['error'=>true,'error_type'=>'progress','message'=>trans("locale.student_has_progress")];
//            //throw  new BadRequestException(trans("locale.student_has_progress"));
//
//        }


        //$this->professionalRepository->deleteCampusUsers($student,true);

        $campusUsers  = $student->getAllCampusUsers();
        $campusProgress  = $student->getCampusProgress();
        $this->accountRepository->deleteUser($student);

        foreach($campusUsers as $campusUser){

            $round = $campusUser->getCampusRound();
            if($round!==null){
                $new_studentsNum=($round->getNoOfStudents())==0?0:$round->getNoOfStudents()-1;
                $round->setNoOfStudents($new_studentsNum);
                $this->professionalRepository->storeCampusRound($round);

            }


            $this->professionalRepository->deleteCampusUser($campusUser);

        }
        foreach ($campusProgress as $campusProgress) {
            $this->professionalRepository->deleteRoundActivityProgress($campusProgress);

        }

        $this->accountRepository->commitDatabaseTransaction();
        return  ['error'=>false,'message'=>trans("locale.student_delete_successfull")];

    }

    public function deleteTeachers($user_id,$teachers,$force = false)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Teacher',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        foreach ($teachers as $teacher){

            $this->deleteTeacher($school,$teacher,$force);

        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.teachers_delete_successfull");
    }

    public function deleteTeacher(School $userSchool,$teacher_id,$force = false)
    {

        $teacher = $this->accountRepository->getUserById($teacher_id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.teacher_not_exist"));
        $teacherSchool = $teacher->getAccount()->getSchool();
        if($teacherSchool == null)
            throw new BadRequestException(trans("locale.teacher_no_school"));
        if($teacherSchool->getId() != $userSchool->getId())
            throw new BadRequestException(trans("locale.teacher_school_mismatch"));

        $teacherRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }
        if(!$isTeacher)
            throw  new BadRequestException(trans("locale.teacher_wrong_role"));

        $campusUsers  = $teacher->getAllCampusUsers();
        $this->accountRepository->deleteUser($teacher);
        foreach ($campusUsers as $campusUser){
            $this->professionalRepository->deleteCampusUser($campusUser);
        }
        //$this->professionalRepository->deleteCampusUsers($teacher,false);


        return trans("locale.teacher_delete_successfull");
    }

    public function getAdminRoundsByStatus($user_id, $status,$gmt_difference){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($status==='running')
            $rounds=$this->professionalRepository->getAdminRunningRounds($user_id,$gmt_difference);
        else if($status==='upcoming')
            $rounds=$this->professionalRepository->getAdminUpcomingRounds($user_id,$gmt_difference);
        else if($status==='ended')
            $rounds=$this->professionalRepository->getAdminEndedRounds($user_id,$gmt_difference);
        else if($status==='all')
            $rounds=$this->professionalRepository->getAllAdminRounds($user_id,$gmt_difference);
        else
            throw new BadRequestException(trans('locale.campus_status_doesnot_exist'));
    

        $roundsMapped=Mapper::MapEntityCollection(MinimizedCampusRoundDto::class,$rounds);
        return $roundsMapped;
    }

    public function checkUserRole(User $user,$role_name){
        $roles=$user->getRoles();
        foreach ($roles as $role){
            if($role->getName()===$role_name)
                return true;
        }
        return false;
    }

    public function addTeacher($userId,UserDto $userDto){
        $this->accountRepository->beginDatabaseTransaction();
        $mainUser = $this->accountRepository->getUserById($userId);
        if($mainUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("create_Teacher",$mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));
        $userDto->role = "teacher";

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $account = $mainUser->getAccount();
        $user = new User($userDto, $account);

        $role = $this->accountRepository->getRoleByName($userDto->role);

        $user->addRole($role);

        $this->accountRepository->addUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return UserDtoMapper::CustomerMapper($user);
    }

    public function addStudent($userId,UserDto $studentDto){

        $data = $this->checkSchoolData($userId);
        $mainUser = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $this->accountRepository->beginDatabaseTransaction();
        if(!$this->accountRepository->isAuthorized("create_Student", $mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));

        $studentDto->role = "student";

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($studentDto);
        if($existingUsername)
            throw new BadRequestException(trans('locale.username_exists'));

        $user = new User($studentDto, $account);

        $role = $this->accountRepository->getRoleByName($studentDto->role);
        $user->addRole($role);

        $this->accountRepository->addUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(CampusUserDto::class, $user);
    }

    public function addStudentsCSV($userId,$file){

        //check user
        $this->accountRepository->beginDatabaseTransaction();
        $mainUser = $this->accountRepository->getUserById($userId);
        $role = $this->accountRepository->getRoleByName("student");
        $account = $mainUser->getAccount();
        if($mainUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("create_Student",$mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));

        //check file
        $data = json_encode(Excel::load($file,function($reader){
            $results = $reader->get();
            return $results;
        }));
        $data = json_decode($data,true);

        $data = $data['parsed'];

        if(sizeof($data) == 0)
            throw new BadRequestException(trans("locale.file_empty"));

        if(sizeof(array_intersect(array_keys($data[0]),['firstname','lastname','username','password'])) != 4)
            throw new BadRequestException(trans("locale.first_row_need_params_student_csv"));

        $total = sizeof($data);

        $account = $mainUser->getAccount();
        /*
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.subscription_perios_ended"));
        $subscription = $subscriptions->last();
        $plan_info = $subscription->getPlan()->getSchoolInfo();
        if($plan_info == null)
        {
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription == null){
                $schoolCharge = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                    ->first()->getSchoolCharge();
                if($schoolCharge == null){
                    $distibutorSubscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                        ->first()->getDistributorSubscriptions()->first();
                    if($distibutorSubscription == null)
                        throw new BadRequestException(trans("locale.school_plan_information_missing"));
                }
            }
            else{
                $plan_info = $invitationSubscription->getInvitation()->getSchoolInfo();
                if($plan_info == null)
                    throw new BadRequestException(trans("locale.school_plan_information_missing"));
            }

        }
        */
//        $students_count = count($this->schoolRepository->getAllStudents($account->getId()));
//
//        if($plan_info != null)
//            $max_students = $plan_info->getMaxNumberOfStudents();
//        else
//            $max_students = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxStudents"];
//
//        if($max_students == null)
//            $max_students = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxStudents"];
//
//        if(($students_count+$total) > $max_students)
//            throw new BadRequestException(trans("locale.csv_data_exceed_student_count"));

        $totalSuccess = 0;
        $totalFailure = 0;
        $errors = array();
        for($i = 0 ; $i<$total;$i++) {
            $data[$i]['firstname']  = trim($data[$i]['firstname']);
            $data[$i]['lastname']  = trim($data[$i]['lastname']);
            $data[$i]['username']  = trim($data[$i]['username']);
            $v = Validator::make($data[$i], [
                'firstname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'lastname' => 'sometimes|nullable|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'password' => 'required|min:6|max:24',

            ]);

            $errorsArray = array();
            if ($v->fails()) {
                $errorsArray = array_merge($errorsArray,$v->errors()->all());

            }

            $studentDto = new StudentDto();
            $studentDto->fname = $data[$i]['firstname'];
            $studentDto->lname = ($data[$i]['lastname'] != null? $data[$i]['lastname'] : "");
            $studentDto->username = $data[$i]['username'];
            $studentDto->password = $data[$i]['password'];
            $studentDto->role = "student";


            $existingUsername = $this->accountRepository->UserNameAlreadyExist($studentDto->username);
            if($existingUsername)
                array_push($errorsArray,trans('locale.username_exists'));


            $school = $account->getSchool();
            if($school == null)
                throw new BadRequestException(trans('locale.no_school_attached'));


            if(sizeof($errorsArray)>0){
                $totalFailure++;
                array_push($errors, ['rownumber'=>$i+2,'errors'=>$errorsArray]);
            }
            else{

                $user = new User($studentDto, $account);
                $user->addRole($role);
                $this->accountRepository->addUser($user);
                $totalSuccess++;
            }

        }
        if($totalFailure == 0 && $total != 0 )
            $this->accountRepository->commitDatabaseTransaction();
        return ['total'=>$total,'totalSuccess'=>$totalSuccess,'totalFailure'=>$totalFailure,'errors'=>$errors];

    }

    public function generalAdminStatisticsInCampuses($user_id,$gmt_difference){
        $data = $this->checkSchoolData($user_id);
        $admin = $data['user'];
        $school = $data['school'];
        $account = $data['account'];
        $noOfStudents = 0; $runningCamps = []; $upcomingCamps = [];$noOfTeachers = 0;

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard', $admin))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $campusesRounds = $this->professionalRepository->getAllAdminRounds($user_id,$gmt_difference);

        if(count($campusesRounds) > 0){
            $runningCamps = $this->professionalRepository->getAdminRunningRounds($user_id,$gmt_difference);
            $upcomingCamps = $this->professionalRepository->getAdminUpcomingRounds($user_id,$gmt_difference);
            //$camps = $this->schoolRepository->getTeacherCamps($school->getId(), $teacher->getId());
            /*$students = [];
            $teachers = [];
            $noOfStudents = 0;
            $noOfTeachers = 0;
            foreach ($campusesRounds as $round){
                foreach ($round->getStudents() as $student){
                    if(!in_array($student->getId(), $students)){
                        $noOfStudents++;
                        array_push($students, $student->getId());
                    }
                }
                foreach ($round->getTeachers() as $teacher){
                    if(!in_array($teacher->getId(), $teachers)){
                        $noOfTeachers++;
                        array_push($teachers, $teacher->getId());
                    }
                }

            }*/
        }

        $noOfTeachers = $this->accountRepository->getNumberOfUsersWithRole($account,'teacher');
        $noOfStudents = $this->accountRepository->getNumberOfUsersWithRole($account,'student');
        return ["noStudents" => $noOfStudents,"noTeachers"=>$noOfTeachers, "runningRound" => count($runningCamps), "upcomingRounds" => count($upcomingCamps)];
    }

    public function getAdminRecentActivity($user_id,$gmt_difference){
        //get rounds ends in 3 days
        //get classes ends in 1 day
        //get tasks(not html) due_date in 1 day
        $data = $this->checkSchoolData($user_id);
        $admin = $data['user'];
        $school = $data['school'];
        $account = $data['account'];

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard', $admin))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        //$current_time = Carbon::now()->toDateTimeString();
        $recentsDto=[];

        $rounds=$this->professionalRepository->getRecentActivityRounds($user_id,$gmt_difference);
        foreach ($rounds as $round){
            $roundDto=[];
            $roundDto['id']=$round->getId();
            $roundDto['name']=$round->getCampusName();
            $roundDto['ends_at']=$round->getEndsAt();
            $roundDto['flag']=1;
            $roundDto['round_name']=$round->getRound_name();
            $recentsDto[]=$roundDto;

        }

        $classes=$this->professionalRepository->getRecentActivityClasses($user_id,$gmt_difference);
        foreach ($classes as $class){
            $classDto=[];
            $classDto['id']=$class->getId();
            if($class->getCampusClass()==null)
                dd($class);
            $classDto['name']=$class->getCampusClass()->getDefaultActivity()->getName();
            $classDto['ends_at']=$class->getEndsAt();
            $classDto['flag']=2;
            $recentsDto[]=$classDto;

        }

        $tasks=$this->professionalRepository->getRecentActivityTasks($user_id,$gmt_difference);
        foreach ($tasks as $task){
            $taskDto=[];
            $task_type=$task->getCampusActivityTask()->getTask()->getTaskNameAndType()['task_type'];
            if($task_type!=='html_mission'){
                $taskDto['id']=$task->getId();
                $taskDto['due_date']=$task->getDueDate();
                $taskDto['name']=$task->getCampusActivityTask()->getTask()->getTaskNameAndType()['task_name'];
                $taskDto['task_type']=$task_type;
                $taskDto['flag']=3;
                $recentsDto[]=$taskDto;

            }

        }
        return $recentsDto;


    }

    public function getStudentsInRoundToEnroll($user_id,$round_id,$search,$gmt_difference){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));


        // return $students;
        $rounds=$round->getCampus()->getRounds();

        //$rounds = $campus->getRounds();

        $rounds_id = [];
        foreach ($rounds as $round_){
            if($round_->getId()!=$round_id&&($round_->getStatus($gmt_difference) == 'running' || $round_->getStatus($gmt_difference) == 'locked'))
                $rounds_id[] = $round_->getId();
        }

        $students = $this->professionalRepository->getAllStudentsNotInRounds($school->getId(),$rounds_id, null,$search,null,null,null);

        $studentsDto = [];
        foreach ($students as $student) {
            $studentDto = [];
            $studentDto['id'] = $student->getId();
            $studentDto['fname'] = $student->getFirstName();
            $studentDto['lname'] = $student->getLastName();
            $studentDto['email'] = $student->getEmail();
            $studentDto['profile_image'] = $student->getImage();
            $studentDto['username'] = $student->getUsername();
            if(!$round->isMember($student->getId()))
                $studentDto['enrolled']=false;
            else
                $studentDto['enrolled']=true;


            $studentsDto[] = $studentDto;
        }
        return $studentsDto;


    }

    public function getTeachersInRoundToEnroll($user_id,$round_id,$search){

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        if(!$this->accountRepository->isAuthorized('get_AllTeachers',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $teachers = $this->professionalRepository->getAllTeachersInCampuses($school->getId(), null,$search,"username",null,null,null);

        $teachersDto = [];
        foreach ($teachers as $teacher) {
            $teacherDto = [];
            $teacherDto['id'] = $teacher->getId();
            $teacherDto['fname'] = $teacher->getFirstName();
            $teacherDto['lname'] = $teacher->getLastName();
            $teacherDto['email'] = $teacher->getEmail();
            $teacherDto['profile_image'] = $teacher->getImage();
            $teacherDto['username'] = $teacher->getUsername();
            if(!$round->isMember($teacher->getId()))
                $teacherDto['enrolled']=false;
            else
                $teacherDto['enrolled']=true;

            $teachersDto[] = $teacherDto;
        }
        return $teachersDto;

    }

    public function deleteCampusRound($user_id,$round_id,$force=false){
        $force = filter_var($force, FILTER_VALIDATE_BOOLEAN);
        //dd($force);

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation

        if(!$this->accountRepository->isAuthorized('delete_Classroom',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        //check students progress exist or not
        $students_progress=$this->professionalRepository->getStudentsProgressInRound($user_id,$account->getId(),$round_id,$round->getCampus_id());
        if(count($students_progress)>0&&!$force){
            throw new BadRequestException(trans('locale.round_students_have_progress'));

        }

        else{
            //delete course
            $this->accountRepository->beginDatabaseTransaction();
            //delete classes
            $campusRoundClasses=$round->getCampusRoundClasses();
            foreach ( $campusRoundClasses as $campusRoundClass){
                $roundClassRooms=$campusRoundClass->getRooms();
                foreach ($roundClassRooms as $roundClassRoom){
                    $roomUsers=$roundClassRoom->getRoomUsers();
                    foreach ($roomUsers as $roomUser)
                        $this->professionalRepository->deleteRoomUser($roomUser);

                    $this->professionalRepository->deleteRoom($roundClassRoom);
                }

                $this->professionalRepository->deleteCampusRoundClass($campusRoundClass);
            }

            //delete round tasks
            $campusRoundActivityTasks=$round->getCampusRoundActivityTasks();
            foreach ($campusRoundActivityTasks as $campusRoundActivityTask)
                $this->professionalRepository->deleteCampusRoundActivityTask($campusRoundActivityTask);
            $roundUsers=$round->getCampusUsers();
            foreach($roundUsers as $roundUser)
                $this->professionalRepository->deleteCampusRoundUser($roundUser);


            //delete round
            $this->professionalRepository->deleteCampusRound($round);
            $this->accountRepository->commitDatabaseTransaction();
            return trans("locale.round_has_been_deleted");

        }




    }

    public function getCampusInfo($user_id,$round_id){

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); 

      $courseDto=[];
      $courseDto["name"]=$round->getCampus()->getName();
      $courseDto["icon"]=$round->getCampus()->getIconUrl();
      $courseDto["round_name"]=$round->getRound_name();
      $courseDto["round_settings"]=$round->getRound_settings();
      return $courseDto;
    }


    public function getCampusObjectives($user_id,$round_id){

        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $round = $this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));

        if(!$round->isMember($user_id))
            throw new BadRequestException(trans('locale.user_not_in_this_round')); //TODO translation


        $courseObjectivesDto=[];
        $courseObjectives=$round->getCampus()->getObjectives();
        dd($courseObjectives);
      // $courseDto=[];
      // $courseDto["name"]=$round->getCampus()->getName();
      // $courseDto["icon"]=$round->getCampus()->getIconUrl();
      // $courseDto["round_name"]=$round->getRound_name();
      // return $courseDto;
    }


    public function getAllColorPalettes(){
       $colorPalettes = $this->professionalRepository->getAllColorPalettes();
       $colorPaletteDto = Mapper::MapEntityCollection(ColorPaletteDto::class, $colorPalettes);
       
       return $colorPaletteDto;
    }

    public function getCalenderData($user_id,$gmt_difference){

        $data=$this->checkSchoolData($user_id);
        $user = $data['user'];

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

         if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $runningRound = $this->professionalRepository->getAdminRunningRounds($user_id,$gmt_difference,false);
        $upcomingRound=$this->professionalRepository->getAdminUpcomingRounds($user_id, $gmt_difference, false);
       $roundsDto=[];
        foreach ($runningRound  as $round){
            if($round->hasInstructor()){
            $roundDto=[];
            $roundDto['id']=$round->getId();
            $roundDto['roundName']=$round->getRound_name();
            $roundDto['starts_at']=$round->getStartsAt();
            $roundDto['ends_at']=$round->getEndsAt();
            $roundDto['status']=$round->getStatus($gmt_difference);
            $roundDto['classes']=[];

           
           $roundClasses=$round->getCampusRoundClasses();
            foreach ($roundClasses as $roundClass){

                $class=$roundClass->getCampusClass();
                $classDto=[];
                $classDto['id']=$class->getId();
                $classDto['mission_name']=$class->getName();
                $classDto['starts_at']=$roundClass->getStartsAt();
                $classDto['ends_at']=$roundClass->getEndsAt();
                $classDto['due_date']=$roundClass->getDueDate();
                array_push( $roundDto['classes'],$classDto);
            }
                   array_push($roundsDto,$roundDto);
        }   
               
        }  
        foreach ($upcomingRound as $round){
            if($round->hasInstructor()){
            $roundDto=[];
            $roundDto['id']=$round->getId();
            $roundDto['roundName']=$round->getRound_name();
            $roundDto['starts_at']=$round->getStartsAt();
            $roundDto['ends_at']=$round->getEndsAt();
            $roundDto['status']=$round->getStatus($gmt_difference);
            $roundDto['classes']=[];

           
           $roundClasses=$round->getCampusRoundClasses();
            foreach ($roundClasses as $roundClass){

                $class=$roundClass->getCampusClass();
                $classDto=[];
                $classDto['id']=$class->getId();
                $classDto['mission_name']=$class->getName();
                $classDto['starts_at']=$roundClass->getStartsAt();
                $classDto['ends_at']=$roundClass->getEndsAt();
                $classDto['due_date']=$roundClass->getDueDate();
                array_push( $roundDto['classes'],$classDto);
            }
            
            array_push($roundsDto,$roundDto);
                
            }
        }  

      return  $roundsDto;
     
    }

     
     public function  updateCalenderData($user_id,$round_id,$ends_at,$gmt_difference){

        $data=$this->checkSchoolData($user_id);
        $user = $data['user'];

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

         if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
            $round = $this->professionalRepository->getCampusRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
            $status = $round->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));
            $roundEndsDate=$round->getEndsAt();

        if($gmt_difference!=null)
            $today=Carbon::today()->addHour(-($gmt_difference))->timestamp;
        else
            throw new BadRequestException(trans("locale.gmt_difference_should_be_valid_number"));
            if($ends_at < $roundEndsDate)
        {
            throw new BadRequestException(trans("locale.round_canot_edit"));
        }
        elseif($ends_at >= $roundEndsDate)
        {   $date=Carbon::now()->addHours(-($gmt_difference))->startOfDay()->timestamp;
            if($ends_at >= $date)
            {
                $roundEndsDate=$ends_at;
            }

        }
        $round->setEndsAt($roundEndsDate);
        $this->professionalRepository->storeCampusRound($round);
        $roundDto= Mapper::MapClass(CampusRoundDto::class, $round,[CampusDto::class]);
        return $roundDto;
           

     }

     public function  updateCalendarDataInModule($user_id,$round_id,$class_id,$starts_at,$ends_at,$gmt_difference){

        $data=$this->checkSchoolData($user_id);
        $user = $data['user'];

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));
        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

         if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user) && !$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));
            $round = $this->professionalRepository->getCampusRoundById($round_id);
            
        if($round == null)
            throw new BadRequestException(trans('locale.campus_round_not_exist'));
            $status = $round->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_campus"));
            $roundEndsDate=$round->getEndsAt();

        if($gmt_difference!=null)
            $today=Carbon::today()->addHour(-($gmt_difference))->timestamp;
        else
            throw new BadRequestException(trans("locale.gmt_difference_should_be_valid_number"));
        $firstRoundClasses=$round->getCampusRoundClasses()->first();
       
        $roundClasses=$round->getCampusRoundClasses()->last();
        $firstClassId=$firstRoundClasses-> getId();
        $lastClassId=$roundClasses-> getId();
        $startDateClass=$roundClasses->getStartsAt();
        $endDateClass=$roundClasses->getEndsAt();
        $startDateRound=$round->getStartsAt();
        $endDateRound=$round->getEndsAt();

        if($round->getEndsAt() <= $round->getStartsAt())
        throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

        if($class_id ==$firstClassId){

            $startDateClass=$starts_at;
        }
        elseif($class_id !=$firstClassId && $starts_at< $startDateRound){
            throw new BadRequestException(trans("locale.module_canot_edits"));

        }elseif($class_id !=$firstClassId && $starts_at>= $startDateRound)
        {
            $startDateClass=$starts_at;
        }
       

        if($class_id ==$lastClassId)
        {
            $endDateClass=$ends_at;
        }
        elseif($class_id !=$lastClassId && $ends_at>$endDateRound){
            throw new BadRequestException(trans("locale.module_canot_edit"));

        }
        elseif($class_id !=$lastClassId && $ends_at<=$endDateRound)
        {
            $endDateClass=$ends_at; 
        }

        $roundClasses->setStartsAt($startDateClass);
        $roundClasses->setEndsAt($endDateClass);
        $this->professionalRepository-> storeCampusRoundClass($roundClasses);
        //$this->professionalRepository->storeCampusRound($round);
       

        $roundDto= Mapper::MapClass(CampusRoundDto::class, $roundClasses,[CampusDto::class]);
        return $roundDto;
           
         
          
           

     }
}
