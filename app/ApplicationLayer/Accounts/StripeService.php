<?php

namespace App\ApplicationLayer\Accounts;

use Analogue;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Http\Request;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\CustomMappers\InvoiceDetailsDtoMapper;
use App\Helpers\Repository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\StripeEvent;
use Illuminate\Support\Facades\Response;
use App\Helpers\ResponseObject;
use App\Helpers\Mapper;
use App\Framework\Exceptions\UnauthorizedException;
use App\ApplicationLayer\Accounts\Dtos\UserPlansDto;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\ApplicationLayer\Accounts\Dtos\SubscriptionDto;
use App\ApplicationLayer\Accounts\Dtos\UserLoginDto;
use Carbon\Carbon;
use DB;
use Hash;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionHistory;
use App\DomainModelLayer\Accounts\FreeSubscription;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\ApplicationLayer\Accounts\Dtos\UserJourneyRequestDto;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\ApplicationLayer\Accounts\Dtos\UserBasicInfoDto;
use App\ApplicationLayer\Accounts\Dtos\StripeEventDto;
use App\ApplicationLayer\Accounts\Dtos\PlanPeriodDto;
use App\ApplicationLayer\Accounts\Dtos\PlanHistoryDto;
use App\DomainModelLayer\Accounts\UserCreated;
use App\DomainModelLayer\Accounts\UserCreatedHandle;
use App\ApplicationLayer\Accounts\Dtos\InvitationDto;
use App\ApplicationLayer\Accounts\Dtos\NotificationDto;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\NotificationTranslation;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use App\DomainModelLayer\Accounts\UserPosition;

class StripeService
{
	//region Properties
    private $accountRepository;
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository){
        $this->accountRepository = $accountRepository;
    }

    public function handleWebhookEvent(StripeEventDto $stripeEventDto){

    	//create a new stripe Event from the dto
    	$stripeEvent = new StripeEvent($stripeEventDto);
    	//add this new stripe event to the database
    	//$this->accountRepository->storeStripeEvent($stripeEvent);

    	if($stripeEvent->title == "invoice.payment_failed"){

    		//get account by customer id
    		$body = json_decode($stripeEvent->body);
    		$customerId = $body->data->object->customer;
    		$attemptCount = $body->data->object->attempt_count;
    		$subscriptionId = $body->data->object->lines->data[0]->id;
    		$account = $this->accountRepository->getAccountByCustomerId($customerId);
    		$stripeSubscription = $this->accountRepository->getStripeSubsriptionByStripeId($subscriptionId);
    		if($account == null)
    			throw new BadRequestException(trans('locale.no_account_customer_id'));
    		if($stripeSubscription == null)
    			throw new BadRequestException(trans('locale.no_subscription_with_id'));

    		$subscription = $stripeSubscription->getSubscription();
    		if($account->getAccountType()->getName() == "Family"){
    			$users = $account->getUsers();
    			foreach ($users as $key => $user) {
    				//trying to find the parent
    				if($user->getRoles()->first()['name'] == "parent"){
    				    $this->changeLanguage($user);
    					$this->accountRepository->sendPaymentFailedEmail($user,$attemptCount);
    				}
    			}
    		}
    		else{
    			//send to the first user
    			$user = $account->getUsers()->first();
                $this->changeLanguage($user);
    			$this->accountRepository->sendPaymentFailedEmail($user,$attemptCount);

    		}
    		//unsubscribe the account if it's the forth attempt
    		if($attemptCount == 4){
				$subscription->setEndDate(Carbon::now());
				$this->accountRepository->storeSubscription($subscription);
    		}

    	}
    	else if($stripeEvent->title == "invoice.payment_succeeded"){


    	    $invoiceId =json_decode($stripeEvent->body)->data->object->id;
            $output = $this->accountRepository->retrieveInvoiceDetails($invoiceId);

            $invoiceDetails = InvoiceDetailsDtoMapper::PaymentModuleMapper($output['invoice'],$output['details']);
            $invoiceDetails->id = $invoiceId;

            if($invoiceDetails->subtotal == 0)
                throw new BadRequestException(trans('locale.charge_amount_zero'));
            $account = $this->accountRepository->getAccountByCustomerId($invoiceDetails->customer);
            if($account == null)
                throw new BadRequestException(trans('locale.no_account_customer'));
            $users = $account->getUsers();
            $user = null;
            foreach ($users as $userx) {
                if($userx->getEmail() != null)
                    $user = $userx;
            }
            if($user == null)
                throw new BadRequestException(trans('locale.no_user_customer'));
            $this->changeLanguage($user);

            $this->accountRepository->sendInvoiceEmail($invoiceDetails,$user);

        }

    	return trans('locale.webhook_handled');
    }

    public function changeLanguage(User $user)
    {
        $code = $user->getLanguageCode();
        app()->setLocale($code);
    }
}