<?php

namespace App\ApplicationLayer\Accounts;

use App\ApplicationLayer\Accounts\Dtos\GuestDto;
use App\ApplicationLayer\Accounts\Dtos\SuperAdminUserDto;
use App\ApplicationLayer\Accounts\Dtos\WorldUserDto;
use App\ApplicationLayer\Accounts\Interfaces\IAccountMainService;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Accounts\Dtos\UserLoginDto;
use App\ApplicationLayer\Accounts\Dtos\UserJourneyRequestDto;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\UserEventDto;
use App\ApplicationLayer\Accounts\Dtos\UserBasicInfoDto;
use App\ApplicationLayer\Accounts\Dtos\StripeEventDto;
use App\DomainModelLayer\Accounts\UserCreatedHandle;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\ApplicationLayer\Accounts\Dtos\InvitationRequestDto;
use App\DomainModelLayer\Accounts\User;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;

class AccountMainService implements IAccountMainService
{
    //region Properties
    private $accountRepository;
    private $journeyRepository;
    private $usercreatedhandle;
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository, IJourneyMainRepository $journeyRepository, UserCreatedHandle $usercreatedhandle)
    {
        $this->accountRepository = $accountRepository;
        $this->journeyRepository = $journeyRepository;
        $this->usercreatedhandle = $usercreatedhandle;
    }

    //endregion

    public function Login(UserLoginDto $userLoginDto,$gmt_difference)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->Login($userLoginDto,$gmt_difference);
    }

    public function loginFacebook($code, $clientId, $redirectUri, $clientSecret)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->loginFacebook($code, $clientId, $redirectUri, $clientSecret);
    }

    public function loginTwitter($oauthToken, $oauthVerifier, $redirectUri, $consumerKey, $consumerSecret)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->loginTwitter($oauthToken, $oauthVerifier, $redirectUri, $consumerKey, $consumerSecret);
    }

    public function loginGoogle($code, $clientId, $redirectUri, $clientSecret)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->loginGoogle($code, $clientId, $redirectUri, $clientSecret);
    }

    public function getUserScores($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserScores($user_id);
    }

    public function getUserJourneys($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserJourneys($user_id);
    }

    public function addJourney(UserJourneyRequestDto $userJourneyRequestDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addJourney($userJourneyRequestDto);
    }

    public function Add(UserDto $userDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->Add($userDto);
    }

    public function activate($token_url)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->activate($token_url);
    }

    public function getBasicInfo($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getBasicInfo($user_id);
    }

    public function updateBasicInfo($user_id, UserBasicInfoDto $userBasicInfoDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateBasicInfo($user_id, $userBasicInfoDto);
    }

    public function updatePassword($user_id, $old_password, $new_password)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updatePassword($user_id, $old_password, $new_password);
    }

    public function updateProfileImage($user_id, $image_name, $image_data)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateProfileImage($user_id, $image_name, $image_data);
    }

    public function updateSchoolLogo($user_id, $image_name, $image_data)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateSchoolLogo($user_id, $image_name, $image_data);
    }

    public function getAllPlanPeriods()
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getAllPlanPeriods();
    }

    public function getAvailablePlans($accountType_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getAvailablePlans($accountType_id);
    }

    public function getOrderedAvailablePlans($accountType_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getOrderedAvailablePlans($accountType_id);
    }

    public function getAvailablePlansByAccountName($accountType_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getAvailablePlansByAccountName($accountType_id);
    }

    public function subscribe(UserSubscriptionDto $userSubscriptionDto, $trial = false, $coupon_id = null)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->subscribe($userSubscriptionDto, $trial, $coupon_id);
    }

    public function getUserSubscriptions($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserSubscriptions($user_id);
    }

    public function getAccountTypes()
    {
        $accountTypeService = new AccountTypeService($this->accountRepository);
        return $accountTypeService->getAccountTypes();
    }

    public function unsubscribe($user_id, $subscription_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->unsubscribe($user_id, $subscription_id);
    }

    public function upgradePlan($user_id, $subscription_id, $plan_name, $period, $stripe_token = null)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->upgradePlan($user_id, $subscription_id, $plan_name, $period, $stripe_token);
    }

    public function getPlanByName($name)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getPlanByName($name);
    }

    public function storeInvitation(InvitationRequestDto $invitation_dto)
    {
        $invitationService = new InvitationService($this->accountRepository, $this->usercreatedhandle);
        return $invitationService->storeInvitation($invitation_dto);
    }

    public function validateCode($code)
    {
        $invitationService = new InvitationService($this->accountRepository, $this->usercreatedhandle);
        return $invitationService->validateCode($code);
    }

    public function registerInvitation(UserDto $user_dto, $code)
    {
        $invitationService = new InvitationService($this->accountRepository, $this->usercreatedhandle);
        return $invitationService->registerInvitation($user_dto, $code);
    }

    public function registerIndividual(UserDto $user_dto, $code)
    {
        $invitationService = new InvitationService($this->accountRepository, $this->usercreatedhandle);
        return $invitationService->registerIndividual($user_dto, $code);
    }

    public function registerStudent(UserDto $user_dto, $code)
    {
        $invitationService = new InvitationService($this->accountRepository, $this->usercreatedhandle);
        return $invitationService->registerStudent($user_dto, $code);
    }

    public function getInvitationByUser($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getInvitationByUser($user_id);
    }

    public function handleSubscriptionEnd(User $user)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->handleSubscriptionEnd($user);
    }

    public function getNotifications($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getNotifications($user_id);
    }

    public function confirmNotification($id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->confirmNotification($id);
    }

    public function delayNotification($id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->delayNotification($id);
    }

    public function updateUserPosition($user_id, $journey_id, $adventure_id, $id, $type, $index)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateUserPosition($user_id, $journey_id, $adventure_id, $id, $type, $index);
    }

    public function getParentChildren($id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getParentChildren($id);
    }

    public function addChild($parent, $userDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addChild($parent, $userDto);
    }

    public function getUserType($email)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserType($email);
    }

    public function addUserChild($user_id, $child_id, $stripe_token = null)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addUserChild($user_id, $child_id, $stripe_token);
    }

    public function handleWebhookEvent(StripeEventDto $stripeEventDto)
    {
        $stripeService = new StripeService($this->accountRepository);
        return $stripeService->handleWebhookEvent($stripeEventDto);
    }

    public function convertToFamily($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->convertToFamily($user_id);
    }

    public function activateParent($child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->activateParent($child_id);
    }

    public function addChildManually($parent_id, UserDto $childDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addChildManually($parent_id, $childDto);
    }

    public function customerInfo($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->customerInfo($user_id);
    }

    public function customerInvoices($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->customerInvoices($user_id);
    }

    public function getCustomerInvoiceById($user_id, $invoice_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getCustomerInvoiceById($user_id, $invoice_id);
    }

    public function sendContactUsEmail(SupportEmailDto $supportEmailDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->sendContactUsEmail($supportEmailDto);
    }

    public function requestFromParent($user_id, $journey_category)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->requestFromParent($user_id, $journey_category);
    }

    public function requestFromParentActivity($user_id, $activity_id, $is_default = true)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->requestFromParentActivity($user_id, $activity_id, $is_default);
    }

    public function sendSupportEmail($user_id, SupportEmailDto $supportEmailDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->sendSupportEmail($user_id, $supportEmailDto);
    }

    public function handleAllUsersSubscriptions()
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->handleAllUsersSubscriptions();
    }

    public function validateUserEmail($email)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->validateUserEmail($email);
    }

    public function validateUsername($username)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->validateUsername($username);
    }

    public function updateCreditCard($user_id, $stripe_token)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateCreditCard($user_id, $stripe_token);
    }

    public function resetPassword($email)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->resetPassword($email);
    }

    public function confirmResetPassword(PasswordResetDto $resetPasswordDto, $newPassword)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->confirmResetPassword($resetPasswordDto, $newPassword);
    }

    public function markUnsubscribeNotificationsAsRead(User $user)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->markUnsubscribeNotificationsAsRead($user);
    }

    public function downloadInvoicePDF($invoiceId)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->downloadInvoicePDF($invoiceId);
    }

    public function resetChildsSupervisorEmail($supervisorEmail, $old_email)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->resetChildsSupervisorEmail($supervisorEmail, $old_email);
    }

    public function requestCountryChange($user_id, $newCountry)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->requestCountryChange($user_id, $newCountry);
    }

    public function getRbf($user_id, $code)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getRbf($user_id, $code);
    }

    public function authenticateRobopal($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->authenticateRobopal($user_id);
    }

    public function addExtraPlan($user_id, $plan_name)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addExtraPlan($user_id, $plan_name);
    }

    public function removeExtraPlan($user_id, $plan_name)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->removeExtraPlan($user_id, $plan_name);
    }

    public function getExtraPlans($accounttypename)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getExtraPlans($accounttypename);
    }

    public function getLanguages()
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getLanguages();
    }

    public function updateUserLanguages($user_id, $language_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateUserLanguages($user_id, $language_id);
    }

    public function reportBug($error)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->reportBug($error);
    }

    public function resendActivationMail($email)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->resendActivationMail($email);
    }

    public function sendFeedbackMail($name, $feeling, $email, $msg)
    {
        $guestService = new GuestService($this->accountRepository);
        return $guestService->sendFeedbackMail($name, $feeling, $email, $msg);
    }

    public function UserSubscription($action, $user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->UserSubscription($action, $user_id);
    }

    public function getChildJourneyAndActivities($parent_id, $child_id, $is_mobile = true)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildJourneyAndActivities($parent_id, $child_id, $is_mobile);
    }

    public function getChildProgress($parent_id, $journey_id, $child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildProgress($parent_id, $journey_id, $child_id);
    }

    public function getChildStatistics($parent_id, $journey_id, $child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildStatistics($parent_id, $journey_id, $child_id);
    }

    public function getChildFeeds($parent_id, $journey_id, $child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildFeeds($parent_id, $journey_id, $child_id);
    }

    public function getMaxChildren($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getMaxChildren($user_id);
    }

    public function unlockModelAnswer($user_id, $mission_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->unlockModelAnswer($user_id, $mission_id);
    }

    public function getCountriesUsersCount()
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getCountriesUsersCount();
    }

    // new payment + new registration
    public function AddUser(UserDto $userDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->AddUser($userDto);
    }

    public function getUserJourneysIncludingLocked($user_id, $minified = null)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserJourneysIncludingLocked($user_id);
    }

    public function getCurrentExtras($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getCurrentExtras($user_id);
    }

    public function checkIfHaveCreditCard($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->checkIfHaveCreditCard($user_id);
    }

    public function getNumberOfChildren($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getNumberOfChildren($user_id);
    }

    public function updateUserEmail($user_id, $email)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateUserEmail($user_id, $email);
    }

    public function checkStripeSubscription($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->checkStripeSubscription($user_id);
    }

    public function getChildrenCount($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildrenCount($user_id);
    }

    public function purchaseActivity($user_id, $activity_id, $stripe_token = null, $is_mobile = true)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->purchaseActivity($user_id, $activity_id, $stripe_token, $is_mobile);
    }

    public function getChildStatisticsActivity($parent_id, $activity_id, $child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildStatisticsActivity($parent_id, $activity_id, $child_id);
    }

    public function getChildProgressActivity($parent_id, $activity_id, $child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildProgressActivity($parent_id, $activity_id, $child_id);
    }

    public function getChildFeedsActivity($parent_id, $activity_id, $child_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getChildFeedsActivity($parent_id, $activity_id, $child_id);
    }

    public function checkUserActivation($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->checkUserActivation($user_id);
    }

    public function addUserCoupon(UserDto $userDto, $coupon_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addUserCoupon($userDto, $coupon_id);
    }

    // world cup services
    public function getFeaturedMatch()
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getFeaturedMatch();
    }

    public function getMatchesPerDay()
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getMatchesPerDay();
    }

    public function getTotalNoSubmission()
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getTotalNoSubmission();
    }

    public function getLeaderBoardData($limit)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getLeaderBoardData($limit);
    }

    public function countLeaderBoardData()
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->countLeaderBoardData();
    }

    public function getUserInLeaderBoard($user_id)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getUserInLeaderBoard($user_id);
    }

    public function getAllParameters()
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getAllParameters();
    }

    public function getUserPrediction($user_id)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getUserPrediction($user_id);
    }

    public function getWorldUserData($user_id)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getWorldUserData($user_id);
    }

    public function updateWorldUserData($user_id, WorldUserDto $worldUserDto)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->updateWorldUserData($user_id, $worldUserDto);
    }

    public function updateUsersScore()
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->updateUsersScore();
    }

    public function predictWorldUser($user_id, $parameters)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->predictWorldUser($user_id, $parameters);
    }

    public function getSharedPredictionData($user_id = null)
    {
        $userService = new WorldCupService($this->accountRepository);
        return $userService->getSharedPredictionData($user_id);
    }

    public function loginRoboGardenFacebook(UserDto $userDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->loginRoboGardenFacebook($userDto);
    }

    public function getUserData($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserData($user_id);
    }

    public function predictWorldCupAutomatic($size, $from)
    {
        $worldCupService = new WorldCupService($this->accountRepository);
        return $worldCupService->predictWorldCupAutomatic($size, $from);
    }

    public function getGuestData($ip_address)
    {
        $guestService = new GuestService($this->accountRepository);
        return $guestService->getGuestData($ip_address);
    }

    public function saveGuestData(GuestDto $guestDto)
    {
        $guestService = new GuestService($this->accountRepository);
        return $guestService->saveGuestData($guestDto);
    }

    public function saveGuestRegisteredScore($user_id, $coins, $xp)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->saveGuestRegisteredScore($user_id, $coins, $xp);
    }

    public function updateMobileUser($user_id, UserDto $userDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->updateMobileUser($user_id, $userDto);
    }

    public function getUserNotifications($user_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->getUserNotifications($user_id);
    }

    public function deleteUserNotification($id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->deleteUserNotification($id);
    }

    public function markNotificationAsRead($user_id,$notification_id)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->markNotificationAsRead($user_id,$notification_id);
    }

    //    ===============     Superadmin      ===============

    public function addSuperAdminUser(UserDto $userDto)
    {
        $userService = new UserService($this->accountRepository, $this->journeyRepository, $this->usercreatedhandle);
        return $userService->addSuperAdminUser($userDto);

    }
}