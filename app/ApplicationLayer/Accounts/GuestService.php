<?php
namespace App\ApplicationLayer\Accounts;

use App\DomainModelLayer\Accounts\GuestData;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;

class GuestService
{
    private $accountRepository;

    public function __construct(IAccountMainRepository $accountRepository){
        $this->accountRepository = $accountRepository;
    }

    public function sendFeedbackMail($name, $feeling, $email, $msg){
        $this->accountRepository->sendFeedbackMail($name, $feeling, $email, $msg);
        return trans('locale.feedback_email_sent');
    }

    public function getGuestData($ip_address){
        $user = $this->accountRepository->getUserByIpAddress($ip_address);
        if($user == null)
            return [];

        $userData['ip_address'] = $user->getIpAddress();
        $userData['coins'] = $user->getCoins();
        $userData['xp'] = $user->getXp();
        return $userData;
    }

    public function saveGuestData(GuestData $guestDto){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserByIpAddress($guestDto->ip_address);
        if($user != null){
            $user->setCoins($guestDto->coins);
            $user->setXp($guestDto->xp);
        }
        else{
            $user = new GuestData($guestDto->ip_address, $guestDto->coins, $guestDto->xp);
        }
        $this->accountRepository->storeGuestData($user);
        $this->accountRepository->commitDatabaseTransaction();

        return trans('locale.user_data_updated');
    }
}