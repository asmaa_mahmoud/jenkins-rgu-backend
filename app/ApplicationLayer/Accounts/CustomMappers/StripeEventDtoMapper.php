<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/9/2017
 * Time: 5:10 PM
 */

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\DomainModelLayer\Accounts\StripeEvent;
use App\ApplicationLayer\Accounts\Dtos\StripeEventDto;

class StripeEventDtoMapper
{
    
    public static function RequestMapper($request){
        $jsonStipeEvent = json_decode($request->getContent());

        $stripeEventDto = new StripeEventDto();
        $stripeEventDto->title = $jsonStipeEvent->type;
        $stripeEventDto->body = $request->getContent();
        if($jsonStipeEvent->livemode == false)
            $stripeEventDto->type = "test" ;
        else if($jsonStipeEvent->livemode == true)
            $stripeEventDto->type = "live";
        
        return $stripeEventDto;
    }
}