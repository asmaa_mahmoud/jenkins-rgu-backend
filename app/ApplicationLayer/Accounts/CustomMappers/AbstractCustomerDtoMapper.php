<?php

namespace App\ApplicationLayer\Customers;

use App\DomainModelLayer\Accounts\User;
use Illuminate\Http\Request;

abstract class AbstractCustomerDtoMapper implements IUserDtoMapper
{
	public static function MapCustomer(User $customer){
        return static::CustomerMapper($customer);
    }

    public static function MapRequest(Request $request){
        return static::RequestMapper($request);
    }
}	
