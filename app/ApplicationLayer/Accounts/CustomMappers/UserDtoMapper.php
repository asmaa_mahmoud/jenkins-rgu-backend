<?php

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\ApplicationLayer\Accounts\Dtos\SuperAdminUserDto;
use App\DomainModelLayer\Accounts\User;
use App\ApplicationLayer\Accounts\Dtos\UserDto;

class UserDtoMapper
{
    public static function CustomerMapper(User $user){
        $userDto = new UserDto();
        $userDto->id = $user->getId();
        $userDto->fname = $user->getFirstName();
        $userDto->lname = $user->getLastName();
        $userDto->email = $user->getEmail();
        $userDto->supervisorEmail = $user->getSupervisorEmail();
        $userDto->gender = $user->getGender();
        $userDto->age = $user->getAge();
        $userDto->role = $user->getRoles();
        $userDto->image_link = $user->getImage();
        $userDto->username = $user->getUsername();
        $userDto->score = $user->getScores();
        $userDto->social_id = $user->getSocialId();
        $userDto->social_track = $user->getSocialTrack();
        $userDto->languageCode = $user->getLanguageCode();
        return $userDto;
    }

    public static function RequestMapper($request){
        $userDto = new UserDto();
        $userDto->id = $request['id'];
        $userDto->fname = $request['fname'];
        $userDto->lname = $request['lname'];
        $userDto->email = $request['email'];
        $userDto->supervisorEmail = $request['supervisorEmail'];
        $userDto->gender = $request['gender'];
        $userDto->age = $request['age'];
        $userDto->password = $request['password'];
        $userDto->username = $request['username'];
        $userDto->accounttype = $request['accounttype'];
        $userDto->role = $request['role'];
        $userDto->stripe_token = $request['stripe_token'];
        $userDto->plan = $request['plan'];
        $userDto->period = $request['period'];
        $userDto->image_link = $request['image_link'];
        $userDto->child_id = $request['child_id'];
        $userDto->social_id = $request['social_id'];
        $userDto->social_track = $request['social_track'];
        $userDto->country = $request['country'];
        $userDto->planname = $request['planname'];
        $userDto->school_name = $request['school_name'];
        $userDto->is_cookie = $request['is_cookie'];

        if($request->has('stripe_token')){
            $userDto->stripe_token  = $request['stripe_token'];
        }

        if($request->has('activity_id')){
            $userDto->activity_id  = $request['activity_id'];
        }

        $extra_plans = [];

        if($request->has('extra_plans'))
        {
            if(is_array($request['extra_plans']))
                $extra_plans_object = $request['extra_plans'];
            else
                $extra_plans_object = json_decode($request['extra_plans']);
            foreach ($extra_plans_object as $extra_plan) {
                $extra_plans[] = $extra_plan; 
            }

            $userDto->extra_plans = $extra_plans;
        }

        $classrooms = [];

        if($request->has('classrooms'))
        {
            if(is_array($request['classrooms']))
                $classrooms_object = $request['classrooms'];
            else
                $classrooms_object = json_decode($request['classrooms']);
            foreach ($classrooms_object as $classroom) {
                $classrooms[] = $classroom; 
            }

            $userDto->classrooms = $classrooms;
        }

        return $userDto;
    }

    public static function RegisterSuperAdminMapper($request){
        $superAdminUser = new SuperAdminUserDto();

        $superAdminUser->fname = $request['Fname'];
        $superAdminUser->lname = $request['Lname'];
        $superAdminUser->username = $request['username'];
        $superAdminUser->email = $request['email'];
        $superAdminUser->password = $request['password'];
        $superAdminUser->gender = $request['gender'];

        return $superAdminUser;
    }

    public static function CSVMapper($data){
        $userDto = new UserDto();
        $userDto->fname = $data['firstname'];
        $userDto->lname = $data['lastname'];
        $userDto->email = $data['email'];
        $userDto->password = $data['password'];
        $userDto->username = $data['username'];
        return $userDto;
    }
}