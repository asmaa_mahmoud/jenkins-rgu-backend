<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/9/2017
 * Time: 5:10 PM
 */

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\DomainModelLayer\Accounts\StripeEvent;
use App\ApplicationLayer\Accounts\Dtos\InvoiceDto;

class InvoicesDtoMapper
{
    
    public static function PaymentModuleMapper($response){

        $jsonInvoices = $response;

        $invoices = $jsonInvoices->data;
        $invoiceDtos = [];
        foreach ($invoices as $key => $invoice) {
            if($invoice->paid){
                $invoiceDto = new InvoiceDto();
                $invoiceDto->id = $invoice->id;
                $invoiceDto->date = date('Y-m-d H:i:s',$invoice->date);
                $invoiceDto->number_of_items = $invoice->lines->total_count;
                $invoiceDto->amount = intval($invoice->total) / 100;
                $invoiceDto->next_payment = date('Y-m-d H:i:s',$invoice->lines->data[0]->period->end);
                $invoiceDto->tax = ($invoice->tax == null? 0:$invoice->tax);
                $invoiceDto->plan_name = '';
                $invoiceDto->plan_name = str_replace('_', ' ', $invoice->lines->data[0]->plan->name);
                $invoiceDtos[] = $invoiceDto;
            }

        }
        return $invoiceDtos;
    }
}