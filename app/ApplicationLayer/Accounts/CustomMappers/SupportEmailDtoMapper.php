<?php

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;

class SupportEmailDtoMapper
{
    public static function RequestMapperContactUs($request){

        $supportEmailDto = new SupportEmailDto();
        $supportEmailDto->email = $request['email'];
        $supportEmailDto->name = $request['name'];
        $supportEmailDto->message = $request['message'];
        $supportEmailDto->supportEmail = "info@robogarden.ca";

        return $supportEmailDto;
    }

    public static function RequestMapperSupport($request){
    	$supportEmailDto = new SupportEmailDto();
    	$supportEmailDto->type = $request['type'];
    	$supportEmailDto->message = $request['message'];
    	$supportEmailDto->supportEmail = config("app.MAIN_MAIL");
    	if($request['type'] == "adventure"){
    		$supportEmailDto->info = "Journey: ".$request['info'][0]["journey"]['Name'].' -> ';
    		$supportEmailDto->info = $supportEmailDto->info."Adventure: ".$request["info"][0]["adventure"].' -> ';
    		if(array_key_exists("mission",$request["info"][0])){
    			$supportEmailDto->info = $supportEmailDto->info."mission number: ".$request["info"][0]["mission"];
    		}
    		else if(array_key_exists("tutorial_quiz",$request["info"][0])){
    			$supportEmailDto->info = $supportEmailDto->info."category: tutorial quiz \r\n";
    		}
    		else if(array_key_exists("adventure_quiz",$request["info"][0])){
    			$supportEmailDto->info = $supportEmailDto->info."category: adventure quiz \r\n";
    		}
    		else if(array_key_exists("others",$request["info"][0])){
    			$supportEmailDto->info = $supportEmailDto->info."category: others \r\n";
    		}
    	}
    	else if($request['type'] == "score"  || $request['type'] == "others"){
    		$supportEmailDto->info = "";

    	}
    	else{
    		$supportEmailDto->info = implode(",",$request["info"]);
    	}
        return $supportEmailDto;
    }

    public static function RequestMapperProfessionalSupport($request){
        $request['type'] ="course";
        $supportEmailDto = new SupportEmailDto();
        $supportEmailDto->type = $request['type'];
        $supportEmailDto->message = $request['message'];
        $supportEmailDto->name= $request['name'];
        $supportEmailDto->email = $request['email'];
        $supportEmailDto->round_id= $request['round_id'];
        $supportEmailDto->supportEmail = config("app.MAIN_MAIL");
        
        return $supportEmailDto;
    }
}