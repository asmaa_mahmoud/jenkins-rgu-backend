<?php

namespace App\ApplicationLayer\Customers;

use App\DomainModelLayer\Customers\User;

class UserLoginDtoMapper
{
    public static function RequestMapper($request){

        $userloginDto = new UserLoginDto();
        $userloginDto->password = $request['password'];
        $userloginDto->username = $request['username'];

        return $userloginDto;
    }
}