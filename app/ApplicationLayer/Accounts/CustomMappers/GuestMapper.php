<?php

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\ApplicationLayer\Accounts\Dtos\GuestDto;

class GuestMapper
{
    public static function RequestMapper($request){
        $guestDto = new GuestDto();
        $guestDto->ip_address = $request['ip_address'];
        $guestDto->name = $request['name'];
        $guestDto->coins = $request['coins'];
        $guestDto->xp = $request['xp'];
        return $guestDto;
    }

}