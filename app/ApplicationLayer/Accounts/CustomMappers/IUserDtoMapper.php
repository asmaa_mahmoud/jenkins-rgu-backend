<?php

namespace App\ApplicationLayer\Customers;

use Illuminate\Http\Request;
use App\DomainModelLayer\Customers\User;

interface IUserDtoMapper
{
	public static function MapCustomer(User $customer);
	public static function MapRequest(Request $request);
}
