<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/9/2017
 * Time: 5:10 PM
 */

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\DomainModelLayer\Accounts\StripeEvent;
use App\ApplicationLayer\Accounts\Dtos\InvoiceDetailsDto;
use App\ApplicationLayer\Accounts\Dtos\InvoiceLineDto;

class InvoiceDetailsDtoMapper
{
    
    public static function PaymentModuleMapper($responseInvoice,$reponseInvoiceLines){
        $jsonInvoice = $responseInvoice;
        $jsonInvoiceLines = $reponseInvoiceLines;
        $invoiceDetailsDto  = new InvoiceDetailsDto();
        $invoiceDetailsDto->amount_due = $jsonInvoice->amount_due;
        $invoiceDetailsDto->charge = $jsonInvoice->charge;
        $invoiceDetailsDto->customer = $jsonInvoice->customer;
        $invoiceDetailsDto->date = date('Y-m-d H:i:s',$jsonInvoice->date);
        $invoiceDetailsDto->description = $jsonInvoice->description;
        $invoiceDetailsDto->starting_balance = $jsonInvoice->starting_balance;
        $invoiceDetailsDto->ending_balance = $jsonInvoice->ending_balance;
        $invoiceDetailsDto->subtotal = $jsonInvoice->subtotal;
        $invoiceDetailsDto->tax = $jsonInvoice->tax;
        $invoiceDetailsDto->tax_percent = $jsonInvoice->tax_percent;
        $invoiceDetailsDto->total = $jsonInvoice->total;
        $invoiceDetailsDto->paid = $jsonInvoice->paid;
        $invoiceDetailsDto->currency = $jsonInvoice->currency;
        $invoiceLines = $jsonInvoiceLines->data;
        foreach ($invoiceLines as $key => $invoiceLine) {
        	$invoiceLineDto = new InvoiceLineDto();
        	$invoiceLineDto->id = $invoiceLine->id;
            $invoiceLineDto->amount = $invoiceLine->amount;
            $invoiceLineDto->currency = $invoiceLine->currency;
            $invoiceLineDto->description = $invoiceLine->description;
            $invoiceLineDto->period_start = date('Y-m-d H:i:s',$invoiceLine->period->start);
            $invoiceLineDto->period_end = date('Y-m-d H:i:s',$invoiceLine->period->end);
            $invoiceLineDto->plan_id = $invoiceLine->plan->id;
            $invoiceLineDto->plan_amount = $invoiceLine->plan->amount;
            $invoiceLineDto->plan_name = $invoiceLine->plan->name;
            $invoiceLineDto->subscription = $invoiceLine->subscription;
            $invoiceLineDto->subscription_item = $invoiceLine->subscription_item;
            $invoiceLineDto->type = $invoiceLine->type;
        	$invoiceDetailsDto->invoice_lines[] = $invoiceLineDto;
        }
        return $invoiceDetailsDto;
    }
}