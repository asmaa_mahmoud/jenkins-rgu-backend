<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 4/2/18
 * Time: 3:15 PM
 */

namespace App\ApplicationLayer\Accounts\CustomMappers;


use App\ApplicationLayer\Accounts\Dtos\ChargeDto;

class SingleChargeDtoMapper
{
    public static function PaymentModuleMapper($response){

        $jsonCharge = $response;

        $charge = $jsonCharge;
        $chargeDto = [];
        //foreach ($charges as $key => $charge) {
            if($charge->paid && $charge->invoice == null){
                $chargeDto = new ChargeDto();
                $chargeDto->id = $charge->id;
                $chargeDto->date = date('Y-m-d H:i:s',$charge->created);
                $chargeDto->amount = intval($charge->amount) / 100;
                $chargeDto->description = $charge->description;
                $chargeDto->customer_id = $charge->customer;
                $chargeDto->currency = $charge->currency;
                //$chargeDtos[] = $chargeDto;
            }
        //}
        return $chargeDto;
    }
}