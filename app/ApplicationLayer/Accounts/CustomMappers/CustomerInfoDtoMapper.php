<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/9/2017
 * Time: 5:10 PM
 */

namespace App\ApplicationLayer\Accounts\CustomMappers;

use App\DomainModelLayer\Accounts\StripeEvent;
use App\ApplicationLayer\Accounts\Dtos\CustomerInfoDto;

class CustomerInfoDtoMapper
{
    
    public static function PaymentModuleMapper($response){
        $jsonCustomerInfo = $response;

        $customerInfoDto = new CustomerInfoDto();
        $customerInfoDto->id = $jsonCustomerInfo->id;
        $customerInfoDto->accountBalance = $jsonCustomerInfo->account_balance;
        $customerInfoDto->currency = $jsonCustomerInfo->currency;
        $customerInfoDto->description = $jsonCustomerInfo->description;
        if(count($jsonCustomerInfo->sources->data) > 0)
        {
            $customerInfoDto->cardBrand = $jsonCustomerInfo->sources->data[0]->brand;
            $customerInfoDto->country = $jsonCustomerInfo->sources->data[0]->country;
            $customerInfoDto->expMonth = $jsonCustomerInfo->sources->data[0]->exp_month;
            $customerInfoDto->expYear = $jsonCustomerInfo->sources->data[0]->exp_year;
            $customerInfoDto->lastFour = $jsonCustomerInfo->sources->data[0]->last4;
        }
        return $customerInfoDto;
    }
}