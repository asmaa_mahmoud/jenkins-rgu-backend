<?php

namespace App\ApplicationLayer\Accounts;

use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\ApplicationLayer\Accounts\Dtos\InvitationRequestDto;
use App\ApplicationLayer\Accounts\Dtos\InvitationDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\Framework\Exceptions\BadRequestException;
use App\DomainModelLayer\Accounts\Invitation;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserCreatedHandle;
use App\DomainModelLayer\Accounts\UserCreated;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\SubscriptionDto;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\InvitationSubscription;
use Carbon\Carbon;
use App\ApplicationLayer\Accounts\Dtos\NotificationDto;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\NotificationTranslation;

class InvitationService
{
    private $accountRepository;
    private $usercreatedhandle;

    public function __construct(IAccountMainRepository $accountRepository,UserCreatedHandle $usercreatedhandle){
        $this->accountRepository = $accountRepository;
        $this->usercreatedhandle = $usercreatedhandle;
    }

    public function storeInvitation(InvitationRequestDto $invitationRequestDto)
    {
        $account_type = $this->accountRepository->getAccountTypeByName($invitationRequestDto->account_type);
        if($account_type == null)
            throw new BadRequestException(trans('locale.account_type_not_exist'));
        $previous_invitation = $this->accountRepository->getInvitationByCode($invitationRequestDto->invitation_code);
        if($previous_invitation != null)
            throw new BadRequestException(trans('locale.invitation_with_code_exists'));
        $invitation_dto = new InvitationDto();
        $invitation_dto->Code = $invitationRequestDto->invitation_code;
        $invitation_dto->Name = $invitationRequestDto->invitation_name;
        $invitation_dto->EndDate = $invitationRequestDto->end_date;
        $invitation_dto->NumberOfUsers = $invitationRequestDto->number_of_users;

        $invitation =  new Invitation($invitation_dto,$account_type);

        $this->accountRepository->storeInvitation($invitation);

        return Mapper::MapClass(InvitationDto::class,$invitation);

    }

    public function validateCode($code)
    {
        $invitation = $this->accountRepository->getInvitationByCode($code);
        if($invitation == null)
            throw new BadRequestException(trans('locale.invitation_not_exist'));
        else if($invitation->getEndDate() != null)
        {
            if($invitation->getEndDate() < Carbon::now())
                throw new BadRequestException(trans('locale.invitation_expired'));
        }
        

        $remaining_users= intval($invitation->getNumberOfUsers()) - count($invitation->getSubscriptions());
        $invitation_result = ["type"=>$invitation->getAccountType(),"remaining_users"=>$remaining_users,"total_users"=>$invitation->getNumberOfUsers()];
        return $invitation_result;
    }

    public function registerInvitation(UserDto $userDto,$code)
    {
        $invitation = $this->accountRepository->getInvitationByCode($code);
        if($invitation == null)
            throw new BadRequestException(trans('locale.invitation_not_exist'));

        $subscriptions = $invitation->getSubscriptions();

        if(count($subscriptions) == $invitation->getNumberOfUsers())
            throw new BadRequestException(trans('locale.invitation_reached_max_users'));

        $account_type=$this->accountRepository->getAccountTypeByName($invitation->getAccountType());
        if($userDto->social_id != null)
        {
            $status = $this->accountRepository->getStatusByName('active');         
        }
        else
            $status = $this->accountRepository->getStatusByName('active');
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));
        $account = new Account($userDto, $account_type,$status);
        $user = new User($userDto, $account);

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        if($invitation->getAccountType() == 'Family')
            $role =$this->accountRepository->getRoleByName('parent');
        else
            $role =$this->accountRepository->getRoleByName('invited_'.$invitation->getAccountType());
        if($role == null)
            throw new BadRequestException(trans('locale.role_not_found'));

        $user->addRole($role);
        if($invitation->getAccountType() == 'Family')
            $plan = $this->accountRepository->getPlanByName('invitation_family');
        else
            $plan = $this->accountRepository->getPlanByName('invitation_individual');
        if($plan == null)
            throw new BadRequestException(trans('locale.plan_not_exist'));

        $updated_plan = $this->accountRepository->getlastupdated($plan,$account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans('locale.updated_plan_not_exist'));

        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = $invitation->getEndDate();

        $subscription = new Subscription($subscriptionDto,$updated_plan,$account);

        $invitation_subscription = new InvitationSubscription($subscription,$invitation);

        $subscription->addInvitationSubscription($invitation_subscription);

        $account->addSubscription($subscription);

        $dto = new NotificationDto();
        $dto->StartDate = Carbon::parse($invitation->getEndDate())->subDays(7);
        $dto->EndDate = $invitation->getEndDate();
        $dto->Type = "warning";

        $notification = new Notification($dto,$user);

        $translations = $this->accountRepository->translateNotification('invitation_end', 'invited_account_expire_at');
        foreach ($translations as $translation) {
            $dto->Title = $translation['title'];
            $dto->Message = $translation['message'] . ' ' . $invitation->getEndDate();

            $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
            $notification->addTranslation($notification_translation);
        }

        $user->addNotification($notification);

        $this->accountRepository->AddUser($user);

        //$event = new UserCreated($account,$user);
        //$this->usercreatedhandle->handle($event);

        // $this->accountRepository->emailSubscription($user->getName(),'Invitation',0,'usd',$invitation->getEndDate(),$invitation->getNumberOfUsers(),$user->getEmail());
        
        return UserDtoMapper::CustomerMapper($user);
    }

    public function registerIndividual(UserDto $userDto,$code)
    {
        $invitation = $this->accountRepository->getInvitationByCode($code);
        if($invitation == null)
            throw new BadRequestException(trans('locale.invitation_not_exist'));

        if($invitation->getAccountType() != 'Individual')
            throw new BadRequestException(trans('locale.invitation_must_individual'));
        return $this->registerInvited($userDto,$invitation);
    }

    public function registerStudent(UserDto $userDto,$code)
    {
        $invitation = $this->accountRepository->getInvitationByCode($code);
        if($invitation == null)
            throw new BadRequestException(trans('locale.invitation_not_exist'));

        if($invitation->getAccountType() != 'School')
            throw new BadRequestException(trans('locale.invitation_must_school'));
        return $this->registerInvited($userDto,$invitation);
    }

    public function registerInvited(UserDto $userDto,Invitation $invitation)
    {
        $subscription = $invitation->getSubscriptions()->first();
        if($subscription == null)
            throw new BadRequestException(trans('locale.supervisor_must_registered'));

        $invitation_users = $subscription->getSubscription()->getAccount()->getUsers();
        if(count($invitation_users) == $invitation->getNumberOfUsers())
            throw new BadRequestException(trans('locale.invitation_reached_max_users'));

        $account = $subscription->getSubscription()->getAccount();
        $user = new User($userDto, $account);

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $role =$this->accountRepository->getRoleByName('invited_'.$invitation->getAccountType());
        if($role == null)
            throw new BadRequestException(trans('locale.role_not_found'));

        $user->addRole($role);

        $dto = new NotificationDto();
        $dto->StartDate = Carbon::parse($invitation->getEndDate())->subDays(7);
        $dto->EndDate = $invitation->getEndDate();
        $dto->Type = "warning";

        $notification = new Notification($dto,$user);

        $translations = $this->accountRepository->translateNotification('invitation_end', 'invited_account_expire_at');
        foreach ($translations as $translation) {
            $dto->Title = $translation['title'];
            $dto->Message = $translation['message'] . ' ' . $invitation->getEndDate();

            $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
            $notification->addTranslation($notification_translation);
        }

        $user->addNotification($notification);

        $this->accountRepository->AddUser($user);

        // if($userDto->social_id == null)
        // {
        //     $event = new UserCreated($account,$user);
        //     $this->usercreatedhandle->handle($event);
        // }
        
        return trans('locale.user_created');
    }
}