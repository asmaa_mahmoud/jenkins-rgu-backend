<?php

namespace App\ApplicationLayer\Accounts\Interfaces;

use App\ApplicationLayer\Accounts\Dtos\GuestDto;
use App\ApplicationLayer\Accounts\Dtos\SuperAdminUserDto;
use App\ApplicationLayer\Accounts\Dtos\UserLoginDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\UserJourneyRequestDto;
use App\ApplicationLayer\Accounts\Dtos\UserBasicInfoDto;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\ApplicationLayer\Accounts\Dtos\InvitationRequestDto;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use App\ApplicationLayer\Accounts\Dtos\WorldUserDto;
use App\DomainModelLayer\Accounts\User;
use App\ApplicationLayer\Accounts\Dtos\StripeEventDto;
use App\DomainModelLayer\Accounts\UserScore;

interface IAccountMainService
{
    public function Login(UserLoginDto $userloginDto,$gmt_difference);
    public function getUserScores($user_id);
    public function getUserJourneys($user_id);
    public function addJourney(UserJourneyRequestDto $userJourneyRequestDto);
    public function Add(UserDto $userDto);
    public function activate($token_url);
    public function getBasicInfo($user_id);
    public function updateBasicInfo($user_id, UserBasicInfoDto $userBasicInfoDto);
    public function updatePassword($user_id,$old_password,$new_password);
    public function updateProfileImage($user_id,$image_name,$image_data);
    public function updateSchoolLogo($user_id,$image_name,$image_data);
    public function getAllPlanPeriods();
    public function getAvailablePlans($accountType_id);
    public function getOrderedAvailablePlans($accountType_id);
    public function getAvailablePlansByAccountName($accountType_id);
    public function subscribe(UserSubscriptionDto $userSubscriptionDto,$trial=false,$coupon_id = null);
    public function getUserSubscriptions($user_id);
    public function getAccountTypes();
    public function unsubscribe($user_id,$subscription_id);
    public function upgradePlan($user_id,$subscription_id,$plan_name,$period,$stripe_token= null);
    public function loginFacebook($code,$clientId,$redirectUri,$clientSecret);
    public function loginTwitter($oauthToken,$oauthVerifier,$redirectUri,$consumerKey,$consumerSecret);
    public function loginGoogle($code,$clientId,$redirectUri,$clientSecret);
    public function getPlanByName($name);
    public function storeInvitation(InvitationRequestDto $invitation_dto);
    public function validateCode($code);
    public function registerInvitation(UserDto $user_dto,$code);
    public function registerIndividual(UserDto $user_dto,$code);
    public function registerStudent(UserDto $user_dto,$code);
    public function getInvitationByUser($user_id);

    public function handleSubscriptionEnd(User $user);

    public function getNotifications($user_id);
    public function confirmNotification($id);
    public function delayNotification($id);

    public function updateUserPosition($user_id,$journey_id,$adventure_id,$id,$type,$index);
    public function getParentChildren($id);
    public function getUserType($email);
    public function addUserChild($user_id,$child_id,$stripe_token = null);
    public function convertToFamily($user_id);
    public function handleWebhookEvent(StripeEventDto $stripeEventDto);
    public function activateParent($child_id);
    public function addChildManually($parent_id,UserDto $childDto);

    public function customerInfo($user_id);
    public function customerInvoices($user_id);
    public function getCustomerInvoiceById($user_id,$invoice_id);
    public function requestFromParent($user_id,$journey_category);
    public function requestFromParentActivity($user_id,$activity_id,$is_default = true);
    public function sendContactUsEmail(SupportEmailDto $supportEmailDto);
    public function sendSupportEmail($user_id,SupportEmailDto $supportEmailDto);
    public function handleAllUsersSubscriptions();

    public function updateCreditCard($user_id,$stripe_token);

    public function validateUserEmail($email);
    public function validateUsername($username);
    public function resetPassword($email);
    public function confirmResetPassword(PasswordResetDto $resetPasswordDto,$newPassword);
    public function markUnsubscribeNotificationsAsRead(User $user);
    public function resetChildsSupervisorEmail($supervisorEmail,$old_email);
    public function downloadInvoicePDF($invoiceId);
    public function requestCountryChange($user_id,$newCountry);

    public function getRbf($user_id, $code);
    public function authenticateRobopal($user_id);
    public function addExtraPlan($user_id, $plan_name);
    public function removeExtraPlan($user_id, $plan_name);
    public function getExtraPlans($accounttypename);

    public function getLanguages();
    public function updateUserLanguages($user_id,$language_id);
    public function reportBug($error);
    public function resendActivationMail($email);
    public function sendFeedbackMail($name, $feeling, $email, $msg);
    public function UserSubscription($action, $user_id);
    public function getChildJourneyAndActivities($parent_id, $child_id, $is_mobile = true);
    public function getChildProgress($parent_id, $journey_id, $child_id);
    public function getChildStatistics($parent_id, $journey_id, $child_id);
    public function getChildFeeds($parent_id, $journey_id, $child_id);
    public function getMaxChildren($user_id);
    public function getCountriesUsersCount();
    public function AddUser(UserDto $userDto);
    public function unlockModelAnswer($user_id,$mission_id);
    public function getUserJourneysIncludingLocked($user_id, $minified = null);
    public function getCurrentExtras($user_id);
    public function checkIfHaveCreditCard($user_id);
    public function getNumberOfChildren($user_id);
    public function updateUserEmail($user_id, $email);
    public function checkStripeSubscription($user_id);
    public function getChildrenCount($user_id);
    public function purchaseActivity($user_id,$activity_id,$stripe_token = null, $is_mobile= true);
    public function getChildStatisticsActivity($parent_id,$activity_id,$child_id);
    public function getChildProgressActivity($parent_id, $activity_id, $child_id);
    public function getChildFeedsActivity($parent_id, $activity_id, $child_id);
    public function checkUserActivation($user_id);
    public function addUserCoupon(UserDto $userDto,$coupon_id);
    public function getUserNotifications($user_id);
    public function deleteUserNotification($id);

    // world cup services
    public function getFeaturedMatch();
    public function getMatchesPerDay();
    public function getTotalNoSubmission();
    public function getLeaderBoardData($limit);
    public function countLeaderBoardData();
    public function getUserInLeaderBoard($user_id);
    public function getAllParameters();
    public function getUserPrediction($user_id);
    public function getWorldUserData($user_id);
    public function updateWorldUserData($user_id, WorldUserDto $worldUserDto);
    public function updateUsersScore();
    public function predictWorldUser($user_id, $parameters);
    public function getSharedPredictionData($user_id = null);
    public function loginRoboGardenFacebook(UserDto $userDto);
    public function getUserData($user_id);
    public function predictWorldCupAutomatic($size,$from);
    public function getGuestData($ip_address);
    public function saveGuestData(GuestDto $guestDto);
    public function saveGuestRegisteredScore($user_id, $coins, $xp);
    public function updateMobileUser($user_id, UserDto $userDto);
    public function markNotificationAsRead($user_id,$notification_id);

    //    ===============     Superadmin      ===============

    public function addSuperAdminUser(UserDto $userDto);


}