<?php
/**
 * Created by PhpStorm.
 * User: RVM-5
 * Date: 2/13/2017
 * Time: 5:08 PM
 */

namespace App\ApplicationLayer\Accounts;


use App\DomainModelLayer\Accounts\IAccountRepository;

class AccountService
{
    private $accountRepository;

    public function __construct(IAccountRepository $accountRepository){
        $this->accountRepository = $accountRepository;
    }

    public function Activate($token_url)
    {
        $check = $this->accountRepository->validate_url($token_url);
        if(!is_null($check)){
            $this->accountRepository->activate($token_url);
        }
    }
}