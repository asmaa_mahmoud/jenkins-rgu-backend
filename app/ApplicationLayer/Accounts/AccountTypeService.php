<?php

namespace App\ApplicationLayer\Accounts;

use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\ApplicationLayer\Accounts\Dtos\AccountTypeDto;

class AccountTypeService
{
    private $accountRepository;

    public function __construct(IAccountMainRepository $accountRepository){
        $this->accountRepository = $accountRepository;
    }

    public function getAccountTypes()
    {
        return Mapper::MapEntityCollection(AccountTypeDto::class,$this->accountRepository->getAllTypes());

    }
}