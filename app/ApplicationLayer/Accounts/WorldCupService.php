<?php

namespace App\ApplicationLayer\Accounts;

use App\ApplicationLayer\Accounts\Dtos\WorldUserDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\WorldUserDetails;
use App\DomainModelLayer\Accounts\WorldUserParameter;
use App\DomainModelLayer\Accounts\WorldUserPrediction;
use App\DomainModelLayer\Accounts\WorldUserScore;
use Carbon\Carbon;
use App\Framework\Exceptions\BadRequestException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class WorldCupService
{
    private $accountRepository;
    private $pyScript;
    private $roundNo = 1;

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
//        $today = Carbon::now('UTC')->format('Y-m-d');
        $this->pyScript = "python business.py";
//        if ($today >= '2018-06-14' && $today <= '2018-06-26') {
            $this->roundNo = 1;
//        } elseif ($today >= '2018-06-30' && $today <= '2018-07-03') {
//            $this->roundNo = 2;
//        } elseif ($today >= '2018-07-06' && $today <= '2018-07-07') {
//            $this->roundNo = 3;
//        } elseif ($today >= '2018-07-10' && $today <= '2018-07-11') {
//            $this->roundNo = 4;
//        } elseif ($today == '2018-07-14') {
//            $this->roundNo = 5;
//        } elseif ($today == '2018-07-15') {
//            $this->roundNo = 6;
//        }
    }

    public function getMatchPrediction($match_id)
    {
        $drawCount = $this->accountRepository->getUsersPredictionInMatch($match_id, 0, true);
        $firstCountryCount = $this->accountRepository->getUsersPredictionInMatch($match_id, 1, true);
        $secondCountryCount = $this->accountRepository->getUsersPredictionInMatch($match_id, 2, true);
        return ['firstCountryCount' => $firstCountryCount, 'secondCountryCount' => $secondCountryCount, 'drawCount' => $drawCount];
    }

    public function getNextMatchDate()
    {
        $nextMatch = $this->accountRepository->getNextMatchDate();
        if ($nextMatch == null)
            return null;
        return ['startOfDay' => Carbon::parse($nextMatch->getMatchTime())->startOfDay()->toDateTimeString(),
            'endOfDay' => Carbon::parse($nextMatch->getMatchTime())->endOfDay()->toDateTimeString()];
    }

    public function getMatchData($match_id)
    {
        $match = $this->accountRepository->getMatchById($match_id);
        if ($match == null)
            throw new BadRequestException(trans('locale.match_not_found'));

        $firstCountry = $match->getFirstCountry();
        $secondCounty = $match->getSecondCountry();
        if ($firstCountry == null || $secondCounty == null)
            throw new BadRequestException(trans('locale.county_not_found'));

        $winner_id = null;
        $is_draw = false;
        if ($match->getWinner() == 1)
            $winner_id = $firstCountry->getId();

        elseif ($match->getWinner() == 2)
            $winner_id = $secondCounty->getId();

        elseif ($match->getWinner() != null)
            $is_draw = true;

        $predictionCounts = $this->getMatchPrediction($match->getId());

        return ['flag' => true, 'winner_id' => $winner_id, 'is_draw' => $is_draw,
            'first_country_id' => $firstCountry->getId(), 'first_country_name' => $firstCountry->getName(), 'first_country_flag' => $firstCountry->getFlag(),
            'second_country_id' => $secondCounty->getId(), 'second_country_name' => $secondCounty->getName(), 'second_country_flag' => $secondCounty->getFlag(),
            'first_country_count' => $predictionCounts['firstCountryCount'], 'second_country_count' => $predictionCounts['secondCountryCount'], 'draw_count' => $predictionCounts['drawCount']
        ];
    }

    public function getFeaturedMatch()
    {
        $match = $this->accountRepository->getNextMatch();
        if ($match == null) {
            return ['flag' => false];
        }
        return $this->getMatchData($match->getId());
    }

    public function getMatchesPerDay()
    {
        $nextMatch = $this->accountRepository->getNextMatch();
        if ($nextMatch == null)
            throw new BadRequestException(trans('locale.no_matches'));

        $firstOfDay = Carbon::parse($nextMatch->getMatchTime())->toDateTimeString();
        $endOfDay = Carbon::parse($nextMatch->getMatchTime())->endOfDay()->toDateTimeString();
        $matches = $this->accountRepository->getMatchesInDay($firstOfDay, $endOfDay);
        if (count($matches) <= 0) {
            $nextMatchDate = $this->getNextMatchDate();
            if ($nextMatchDate != null) {
                $matches = $this->accountRepository->getMatchesInDay($nextMatchDate['startOfDay'], $nextMatchDate['endOfDay']);
                if (count($matches) <= 0)
                    return ['flag' => false];
            } else {
                return ['flag' => false];
            }
        }

        $matchesData = array();
        foreach ($matches as $match) {
            $matchData = $this->getMatchData($match->getId());
            if ($matchData != null)
                array_push($matchesData, $matchData);
        }
        return $matchesData;
    }

    public function getTotalNoSubmission()
    {
        $submissionCount = $this->accountRepository->getNoSubmission();
        $usersCount = $this->accountRepository->getNoWorldUsers();
        return ['submissions_count' => $submissionCount, 'users_count' => $usersCount];
    }

    public function getLeaderBoardData($limit)
    {
        $usersData = array();
        $usersScore = $this->accountRepository->getWorldUsersScore($this->roundNo, null, $limit);
        if (count($usersScore) <= 0)
            return ['flag' => false];

        foreach ($usersScore as $userScore) {
            $user = $userScore->getUser();
            $worldUserData = $this->accountRepository->getWorldUserData($user->getId());
            if ($worldUserData != null) {
                array_push($usersData, ['nickname' => $worldUserData->getName(), 'name' => $user->getFirstName(),
                    'image' => $worldUserData->getImage(), 'username' => $user->getUsername(),
                    'country' => $user->getAccount()->getCountry(),
                    'score' => ($this->roundNo == 1 ? $userScore->getScoreOne() : $usersScore->getFinalScore()),
                    'email' => $user->getEmail()
                ]);
            } else {
                array_push($usersData, ['nickname' => null, 'name' => $user->getFirstName(), 'image' => null,
                    'username' => $user->getUsername(), 'country' => $user->getAccount()->getCountry(),
                    'score' => ($this->roundNo == 1 ? $userScore->getScoreOne() : $usersScore->getFinalScore()),
                    'email' => $user->getEmail()
                ]);
            }
        }
        return $usersData;
    }

    public function countLeaderBoardData()
    {
        $usersCount = $this->accountRepository->getWorldUsersScore($this->roundNo, null, null, true);
        return ['count' => $usersCount];
    }

    public function getUserInLeaderBoard($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $userKey = 0;
        $data = array();
        $usersScore = $this->accountRepository->getUsersScore($this->roundNo);
        if ($usersScore != null) {
            $userKey = array_search($user->getId(), $usersScore);
            if (is_int($userKey))
                $userKey++;
        }

        $userScore = $this->accountRepository->getWorldUsersScore($this->roundNo, $user->getId())->first();
        $worldUserData = $this->accountRepository->getWorldUserData($user->getId());
        if ($userScore != null) {
            array_push($data, ['rank' => $userKey, 'nickname' => ($worldUserData ? $worldUserData->getName() : ''),
                'name' => $user->getFirstName(), 'username' => $user->getUsername(), 'image' => ($worldUserData ? $worldUserData->getImage() : null),
                'country' => $user->getAccount()->getCountry(), 'score' => ($this->roundNo == 1 ? $userScore->getScoreOne() : $userScore->getFinalScore())
            ]);
        } else {
            array_push($data, ['rank' => $userKey, 'nickname' => ($worldUserData ? $worldUserData->getName() : ''),
                'name' => $user->getFirstName(), 'username' => $user->getUsername(), 'image' => ($worldUserData ? $worldUserData->getImage() : null),
                'country' => $user->getAccount()->getCountry(), 'score' => 0
            ]);
        }
        return $data;
    }

    public function getAllParameters()
    {
        $parametersData = array();
        $parameters = $this->accountRepository->getAllParameters();
        foreach ($parameters as $parameter) {
            array_push($parametersData, ['id' => $parameter->getId(), 'name' => $parameter->getName(), 'title' => $parameter->getTitle()]);
        }
        return $parametersData;
    }

    public function getUserPrediction($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $predictions = $this->accountRepository->countUserPredictInRound($user_id, $this->roundNo);
        if (count($predictions) <= 0)
            return [];

        $nextMatch = $this->accountRepository->getNextMatch();
        if ($nextMatch == null)
            throw new BadRequestException(trans('locale.no_matches'));

        $firstOfDay = Carbon::parse($nextMatch->getMatchTime())->toDateTimeString();
        $endOfDay = Carbon::parse($nextMatch->getMatchTime())->endOfDay()->toDateTimeString();
        $matches = $this->accountRepository->getMatchesInDay($firstOfDay, $endOfDay);
        $matchIds = array();
        foreach ($matches as $match) {
            array_push($matchIds, $match->getId());
        }

        $predictions = $this->accountRepository->getUserPredictionInMatches($user_id, $matchIds);
        $matchesData = array();
        foreach ($predictions as $prediction) {
            $match = $prediction->getMatch();
            $firstCountry = $match->getFirstCountry();
            $secondCounty = $match->getSecondCountry();
            $winner_id = null;
            $is_draw = false;
            if ($match->getWinner() == 1) {
                $winner_id = $firstCountry->getId();
            } elseif ($match->getWinner() == 2) {
                $winner_id = $secondCounty->getId();
            } elseif ($match->getWinner() != null) {
                $is_draw = true;
            }

            $my_winner = null;
            $my_draw = false;
            if(is_int($prediction->getWinner())){
                if ($prediction->getWinner() == 1) {
                    $my_winner = $firstCountry->getId();
                } elseif ($prediction->getWinner() == 2) {
                    $my_winner = $secondCounty->getId();
                } else {
                    $my_draw = true;
                }
            }
            array_push($matchesData, ['match_id' => $match->getId(), 'first_country_id' => $firstCountry->getId(), 'first_country_name' => $firstCountry->getName(),
                'first_country_flag' => $firstCountry->getFlag(), 'second_country_id' => $secondCounty->getId(), 'second_country_name' => $secondCounty->getName(),
                'second_country_flag' => $secondCounty->getFlag(), 'winner_id' => $winner_id, 'is_draw' => $is_draw, 'my_winner' => $my_winner, 'my_draw' => $my_draw
            ]);
        }

        $parameters = $this->accountRepository->getUserParameters($user_id);
        $parametersData = array();
        foreach ($parameters as $parameter) {
            $parameter->getParameter();
            array_push($parametersData, ['parameter_id' => $parameter->getParameter()->getId(), 'parameter_title' => $parameter->getParameter()->getTitle(),
                'parameter_name' => $parameter->getParameter()->getName(), 'weight' => $parameter->getWeight()]);
        }
        return ['matches' => $matchesData, 'parameters' => $parametersData];
    }

    public function getWorldUserData($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $userData = $this->accountRepository->getWorldUserData($user_id);
        $data = array();
        if ($userData != null) {
            $worldUserData = $this->accountRepository->getWorldUserData($user->getId());
            array_push($data, ['nickname' => ($worldUserData ? $worldUserData->getName() : ''),
                'name' => $userData->getName(), 'image' => $userData->getImage(),
                'country' => $user->getAccount()->getCountry(), 'address' => $userData->getAddress(),
                'phone' => $userData->getPhone(), 'email' => $user->getEmail()
            ]);
        }
        return $data;
    }

    public function updateWorldUserData($user_id, WorldUserDto $worldUserDto)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $this->accountRepository->beginDatabaseTransaction();
        $userDetails = $this->accountRepository->getWorldUserData($user_id);
        if ($userDetails != null) {
            $userDetails->setAddress($worldUserDto->address);
            $userDetails->setPhone($worldUserDto->phone);
            $userDetails->setName($worldUserDto->name);
            $userDetails->setImage($worldUserDto->image);
            $userDetails->setPredictor($worldUserDto->predictor);
        } else {
            $userDetails = new WorldUserDetails($user, $worldUserDto->name, $worldUserDto->address, $worldUserDto->phone, $worldUserDto->image, $worldUserDto->predictor);
        }
        $this->accountRepository->storeWorldUserDetails($userDetails);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.user_data_updated');
    }

    public function updateUsersScore()
    {
        $this->accountRepository->beginDatabaseTransaction();
        $predictions = $this->accountRepository->getUnScoredPredictions();
        foreach ($predictions as $prediction) {
            $match = $prediction->getMatch();
            $winner = $match->getWinner();
            $user = $prediction->getUser();
            $score = $user->getWorldScore();
            if ($score == null) {
                $score = new WorldUserScore($user);
                $this->accountRepository->storeWorldUserScore($score);
            }

            if (!is_int($winner))
                continue;

            if (($winner == 1 && $prediction->getWinner() == 1) || ($winner == 2 && $prediction->getWinner() == 2) || ($winner == 0 && $prediction->getWinner() == 0)) {
                if ($match->getRoundNo() == 1) {
                    $score->setScoreOne($score->getScoreOne() + 10);
                } else {
                    $score->setFinalScore($score->getFinalScore() + 10);
                }
                $this->accountRepository->storeWorldUserScore($score);
            }
            $prediction->setScored(1);
            $this->accountRepository->storeUserPrediction($prediction);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.user_data_updated');
    }

    public function pythonScript($predictor, $gidTeam1, $gidTeam2,
                                 $fifaRank1, $fifaRank2, $fifaRankWeight,
                                 $goal1, $goal2, $goalWeight,
                                 $history, $historyWeight,
                                 $matchesPlayed1, $matchesPlayed2, $matchesPlayedWeight,
                                 $participation1, $participation2, $participationWeight,
                                 $rationWinning1, $rationWinning2, $ratioWinningWeight,
                                 $win1, $win2, $winWeight)
    {
        $process = new Process("python business.py " . $predictor . " " . $gidTeam1 . " " . $gidTeam2 . " " . $fifaRank1 . " " . $fifaRank2 . " " . $fifaRankWeight . " " . $goal1
            . " " . $goal2 . " " . $goalWeight . " " . $history . " " . $historyWeight . " " . $matchesPlayed1 . " " . $matchesPlayed2 . " " . $matchesPlayedWeight . " " . $participation1 . " "
            . $participation2 . " " . $participationWeight . " " . $rationWinning1 . " " . $rationWinning2 . " " . $ratioWinningWeight . " " . $win1 . " " . $win2 . " " . $winWeight);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }

    public function runPythonScript($script){
        $process = new Process($script);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return $process->getOutput();
    }

    public function getSharedPredictionData($user_id = null)
    {
        $nextMatch = $this->accountRepository->getNextMatch();
        if ($nextMatch == null)
            return ['flag' => false];

        if ($user_id != null) {
            $isDraw = null;
            $winner = null;
            $user = $this->accountRepository->getUserById($user_id);
            if ($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            $userPredict = $this->accountRepository->getUserPredictionInMatches($user_id, [$nextMatch->getId()])->first();
            if ($userPredict == null)
                return ['firstCounty' => $nextMatch->getFirstCountry()->getName(), 'secondCountry' => $nextMatch->getSecondCountry()->getName()];

            if ($userPredict->getWinner() == 0) {
                $isDraw = true;
                $image = $userPredict->getMatch()->getDrawImage();
            } elseif ($userPredict->getWinner() == 1) {
                $winner = $nextMatch->getFirstCountry()->getName();
                $image = $userPredict->getMatch()->getFirstImage();
            } elseif ($userPredict->getWinner() == 2) {
                $winner = $nextMatch->getSecondCountry()->getName();
                $image = $userPredict->getMatch()->getSecondImage();
            }

            return ['firstCounty' => $nextMatch->getFirstCountry()->getName(), 'secondCountry' => $nextMatch->getSecondCountry()->getName(),
                'isDraw' => ($isDraw ? $isDraw : null), 'winner' => ($winner ? $winner : null), 'image' => $image ? $image : null];
        }

        return ['firstCounty' => $nextMatch->getFirstCountry()->getName(), 'secondCountry' => $nextMatch->getSecondCountry()->getName()];
    }

    public function predictWorldUser($user_id, $parameters)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $userDetails = $this->accountRepository->getWorldUserData($user_id);
        if ($userDetails == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if ($userDetails->getPredictor() == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $timeNow = Carbon::now('UTC');
        // update user parameters
        $this->accountRepository->deleteUserParameters($user_id);
        $fifaWeight = 0;
        $historyWeight = 0;
        $winWeight = 0;
        $participationWeight = 0;
        $goalsWeight = 0;
        $ratioWeight = 0;
        $matchesWeight = 0;
        foreach ($parameters as $parameter) {
            $actualParameter = $this->accountRepository->getParameterById($parameter['id']);
            if ($actualParameter == null)
                throw new BadRequestException(trans('locale.parameter_not_found'));

            if ($actualParameter->getName() == 'fifa_rank')
                $fifaWeight = $parameter['weight'];
            elseif ($actualParameter->getName() == 'history')
                $historyWeight = $parameter['weight'];
            elseif ($actualParameter->getName() == 'games_win')
                $winWeight = $parameter['weight'];
            elseif ($actualParameter->getName() == 'participation')
                $participationWeight = $parameter['weight'];
            elseif ($actualParameter->getName() == 'world_goals')
                $goalsWeight = $parameter['weight'];
            elseif ($actualParameter->getName() == 'ratio_winning')
                $ratioWeight = $parameter['weight'];
            elseif ($actualParameter->getName() == 'matches_played')
                $matchesWeight = $parameter['weight'];

            $userParameter = new WorldUserParameter($user, $actualParameter, $parameter['weight']);
            $this->accountRepository->storeUserParameter($userParameter);
        }
        // get next match in day
        $nextMatch = $this->accountRepository->getNextMatchDate();
        if ($nextMatch == null)
            throw new BadRequestException(trans('locale.no_matches'));
        // get matches in day
        $matches = $this->accountRepository->getMatchesInDay(Carbon::parse($nextMatch->getMatchTime()), Carbon::parse($nextMatch->getMatchTime())->copy()->addDay(1)->endOfDay());
        foreach ($matches as $match){
            if ($timeNow->lt(Carbon::parse($match->getMatchTime()))){
                $next = $match;
                break;
            }
        }
        // check if user doesn't predict for any matches in round before
        $predictionCount = $this->accountRepository->countUserPredictInRound($user_id, $next->getRoundNo());
        $userScore = $this->accountRepository->getWorldUsersScore($this->roundNo, $user_id);
        $matches = $this->accountRepository->getMatchesByRoundNoFromDate($next->getMatchTime(), $next->getRoundNo());
        $toBePredictedMatches = [];
        if (count($predictionCount) <= 0) {
            $script = $this->pyScript . " " . count($matches) . " " . $userDetails->getPredictor();
            foreach ($matches as $key => $match) {
                // update here which winner according to sfe engine
                $firstCountry = $match->getFirstCountry();
                $secondCountry = $match->getSecondCountry();
                if ($firstCountry->GetGID() > $secondCountry->GetGID()) {
                    $match->first = true;
                    $script .= " ". $firstCountry->GetGID() ." ". $secondCountry->GetGID() ." ".
                        $firstCountry->getPoints() ." ". $secondCountry->getPoints() ." ". $fifaWeight ." ".
                        $firstCountry->getGoalsScore() ." ". $secondCountry->getGoalsScore() ." ". $goalsWeight ." ".
                        ($match->getWinCount() - $match->getLoseCount()) ." ". $historyWeight ." ".
                        $firstCountry->getMatchesPlayed() ." ". $secondCountry->getMatchesPlayed() ." ". $matchesWeight ." ".
                        $firstCountry->getParticipation() ." ".  $secondCountry->getParticipation() ." ". $participationWeight ." ".
                        ($firstCountry->getMatchesPlayed() != 0 ? $firstCountry->getGamesWon() / $firstCountry->getMatchesPlayed() : 0) ." ".
                        ($secondCountry->getMatchesPlayed() != 0 ? $secondCountry->getGamesWon() / $secondCountry->getMatchesPlayed() : 0) ." ". $ratioWeight ." ".
                        $match->getWinCount() ." ". $match->getLoseCount() ." ". $winWeight;
                }
                else {
                    $script .= " ". $secondCountry->GetGID() ." ". $firstCountry->GetGID() ." ".
                        $secondCountry->getPoints() ." ". $firstCountry->getPoints() ." ". $fifaWeight ." ".
                        $secondCountry->getGoalsScore() ." ". $firstCountry->getGoalsScore() ." ". $goalsWeight ." ".
                        ($match->getLoseCount() - $match->getWinCount()) ." ". $historyWeight ." ".
                        $secondCountry->getMatchesPlayed() ." ". $firstCountry->getMatchesPlayed() ." ". $matchesWeight ." ".
                        $secondCountry->getParticipation() ." ". $firstCountry->getParticipation() ." ". $participationWeight ." ".
                        ($secondCountry->getMatchesPlayed() != 0 ? $secondCountry->getGamesWon() / $secondCountry->getMatchesPlayed() : 0) ." ".
                        ($firstCountry->getMatchesPlayed() != 0 ? $firstCountry->getGamesWon() / $firstCountry->getMatchesPlayed() : 0) ." ". $ratioWeight ." ".
                        $match->getLoseCount() ." ". $match->getWinCount() ." ". $winWeight;
                }
            }

            $results = $this->runPythonScript($script);
            if(isset($results)){
                $result = explode(",", $results);
                foreach ($matches as $key => $match){
                    if(isset($result[$key])){
                        if(isset($match->first) && $match->first = true){
                            if ($result[$key] == 1)
                                $winner = 1;
                            elseif ($result[$key] == -1)
                                $winner = 2;
                            else{
                                $winner = 0;
                                if(Carbon::parse($match->getMatchTime()) > Carbon::parse("2018-06-29 00:00:00")){
                                    $winner = 1;
                                }
                            }

                        }
                        else{
                            if ($result[$key] == 1)
                                $winner = 2;
                            elseif ($result[$key] == -1)
                                $winner = 1;
                            else
                            {
                                $winner = 0;
                                if(Carbon::parse($match->getMatchTime()) > Carbon::parse("2018-06-29 00:00:00")){
                                    $winner = 2;
                                }
                            }

                        }
                        $prediction = new WorldUserPrediction($user, $match, $winner);
                        if($key == 1)
                            $prediction->setCounter(1);
                        $this->accountRepository->storeUserPrediction($prediction);
                    }
                }
            }
        } // if user has prediction before
        else {
            $matchesCount = 0;
            $script = "";
            foreach ($matches as $key => $match) {
                $prediction = $this->accountRepository->getMatchPrediction($user_id, $match->getId());
                if ($prediction != null) {
                    if($prediction->getScored() != 1){
                        $match = $prediction->getMatch();
                        array_push($toBePredictedMatches,$match);
                        $matchesCount++;
                        // update here which winner according to sfe engine
                        $firstCountry = $match->getFirstCountry();
                        $secondCountry = $match->getSecondCountry();
                        if ($firstCountry->GetGID() > $secondCountry->GetGID()) {
                            $script .= " ". $firstCountry->GetGID() ." ". $secondCountry->GetGID() ." ".
                                $firstCountry->getPoints() ." ". $secondCountry->getPoints() ." ". $fifaWeight ." ".
                                $firstCountry->getGoalsScore() ." ". $secondCountry->getGoalsScore() ." ". $goalsWeight ." ".
                                ($match->getWinCount() - $match->getLoseCount()) ." ". $historyWeight ." ".
                                $firstCountry->getMatchesPlayed() ." ". $secondCountry->getMatchesPlayed() ." ". $matchesWeight ." ".
                                $firstCountry->getParticipation() ." ".  $secondCountry->getParticipation() ." ". $participationWeight ." ".
                                ($firstCountry->getMatchesPlayed() != 0 ? $firstCountry->getGamesWon() / $firstCountry->getMatchesPlayed() : 0) ." ".
                                ($secondCountry->getMatchesPlayed() != 0 ? $secondCountry->getGamesWon() / $secondCountry->getMatchesPlayed() : 0) ." ". $ratioWeight ." ".
                                $match->getWinCount() ." ". $match->getLoseCount() ." ". $winWeight;
                        }
                        else {
                            $script .= " ". $secondCountry->GetGID() ." ". $firstCountry->GetGID() ." ".
                                $secondCountry->getPoints() ." ". $firstCountry->getPoints() ." ". $fifaWeight ." ".
                                $secondCountry->getGoalsScore() ." ". $firstCountry->getGoalsScore() ." ". $goalsWeight ." ".
                                ($match->getLoseCount() - $match->getWinCount()) ." ". $historyWeight ." ".
                                $secondCountry->getMatchesPlayed() ." ". $firstCountry->getMatchesPlayed() ." ". $matchesWeight ." ".
                                $secondCountry->getParticipation() ." ". $firstCountry->getParticipation() ." ". $participationWeight ." ".
                                ($secondCountry->getMatchesPlayed() != 0 ? $secondCountry->getGamesWon() / $secondCountry->getMatchesPlayed() : 0) ." ".
                                ($firstCountry->getMatchesPlayed() != 0 ? $firstCountry->getGamesWon() / $firstCountry->getMatchesPlayed() : 0) ." ". $ratioWeight ." ".
                                $match->getLoseCount() ." ". $match->getWinCount() ." ". $winWeight;
                        }
                    }
                }
                else {
                    $matchesCount++;
                    // update here which winner according to sfe engine
                    $firstCountry = $match->getFirstCountry();
                    array_push($toBePredictedMatches,$match);
                    $secondCountry = $match->getSecondCountry();
                    if ($firstCountry->GetGID() > $secondCountry->GetGID()) {
                        $script .= " ". $firstCountry->GetGID() ." ". $secondCountry->GetGID() ." ".
                            $firstCountry->getPoints() ." ". $secondCountry->getPoints() ." ". $fifaWeight ." ".
                            $firstCountry->getGoalsScore() ." ". $secondCountry->getGoalsScore() ." ". $goalsWeight ." ".
                            ($match->getWinCount() - $match->getLoseCount()) ." ". $historyWeight ." ".
                            $firstCountry->getMatchesPlayed() ." ". $secondCountry->getMatchesPlayed() ." ". $matchesWeight ." ".
                            $firstCountry->getParticipation() ." ".  $secondCountry->getParticipation() ." ". $participationWeight ." ".
                            ($firstCountry->getMatchesPlayed() != 0 ? $firstCountry->getGamesWon() / $firstCountry->getMatchesPlayed() : 0) ." ".
                            ($secondCountry->getMatchesPlayed() != 0 ? $secondCountry->getGamesWon() / $secondCountry->getMatchesPlayed() : 0) ." ". $ratioWeight ." ".
                            $match->getWinCount() ." ". $match->getLoseCount() ." ". $winWeight;
                    }
                    else {
                        $script .= " ". $secondCountry->GetGID() ." ". $firstCountry->GetGID() ." ".
                            $secondCountry->getPoints() ." ". $firstCountry->getPoints() ." ". $fifaWeight ." ".
                            $secondCountry->getGoalsScore() ." ". $firstCountry->getGoalsScore() ." ". $goalsWeight ." ".
                            ($match->getLoseCount() - $match->getWinCount()) ." ". $historyWeight ." ".
                            $secondCountry->getMatchesPlayed() ." ". $firstCountry->getMatchesPlayed() ." ". $matchesWeight ." ".
                            $secondCountry->getParticipation() ." ". $firstCountry->getParticipation() ." ". $participationWeight ." ".
                            ($secondCountry->getMatchesPlayed() != 0 ? $secondCountry->getGamesWon() / $secondCountry->getMatchesPlayed() : 0) ." ".
                            ($firstCountry->getMatchesPlayed() != 0 ? $firstCountry->getGamesWon() / $firstCountry->getMatchesPlayed() : 0) ." ". $ratioWeight ." ".
                            $match->getLoseCount() ." ". $match->getWinCount() ." ". $winWeight;
                    }
                }
            }
            $results = $this->runPythonScript($this->pyScript ." ". $matchesCount ." ". $userDetails->getPredictor(). $script);
            if(isset($results)){
                $predictionCounter = 0;
                $result = explode(",", $results);
                foreach ($toBePredictedMatches as $key => $match){
                    if(isset($result[$key])){
                        if(isset($match->first) && $match->first = true){
                            if ($result[$key] == 1)
                                $winner = 1;
                            elseif ($result[$key] == -1)
                                $winner = 2;
                            else{
                                $winner = 0;
                                if(Carbon::parse($match->getMatchTime()) > Carbon::parse("2018-06-29 00:00:00")){
                                    $winner = 1;
                                }
                            }

                        }
                        else{
                            if ($result[$key] == 1)
                                $winner = 2;
                            elseif ($result[$key] == -1)
                                $winner = 1;
                            else {
                                $winner = 0;
                                if(Carbon::parse($match->getMatchTime()) > Carbon::parse("2018-06-29 00:00:00")){
                                    $winner = 2;
                                }
                            }
                        }

                        $prediction = $this->accountRepository->getMatchPrediction($user_id, $match->getId());
                        if ($prediction != null) {
                            if($prediction->getScored() != 1){
                                $prediction->setWinner($winner);
                                if($predictionCounter == 0){
                                    if($prediction->getCounter() != null)
                                        $prediction->setCounter($prediction->getCounter() + 1);
                                    else
                                        $prediction->setCounter(1);
                                }
                                $predictionCounter++;
                                $this->accountRepository->storeUserPrediction($prediction);
                            }
                        }
                        else{
                            $prediction = new WorldUserPrediction($user, $match, $winner);
                            if($key == 1)
                                $prediction->setCounter(1);
                            $this->accountRepository->storeUserPrediction($prediction);
                        }
                    }
                }
            }
        }
        // update user score for first time
        if (count($userScore) <= 0) {
            if ($this->roundNo == 1)
                $score = new WorldUserScore($user, 0);
            else
                $score = new WorldUserScore($user, null, 0);
            $this->accountRepository->storeWorldUserScore($score);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.predict_successfully');
    }

    public function predictWorldCupAutomatic($size,$from){
        $usersWithScore = $this->accountRepository->getWorldUsersScore(1);
        $to = $size+$from;
        if($to>sizeof($usersWithScore))
            $to = sizeof($usersWithScore);
        $i = 0;
        foreach ($usersWithScore as $userScore){
            if($i>=$from && $i<$to){
                $user_id = $userScore->getUser()->getId();
                $parameters = $this->accountRepository->getUserParameters($user_id);
                $apiParameters = [];
                foreach ($parameters as $parameter) {
                    $parameterId = $parameter->getParameter()->getId();
                    $parameterWeight = $parameter->getWeight();
                    array_push($apiParameters, ["id" => $parameterId, "weight" => $parameterWeight]);

                }
                $this->predictWorldUser($user_id, $apiParameters);
            }
            $i++;
        }
        return "done!";
    }


}