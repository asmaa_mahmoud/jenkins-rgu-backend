<?php

namespace App\ApplicationLayer\Accounts;

use App\ApplicationLayer\Accounts\Dtos\SuperAdminUserDto;
use App\Helpers\HttpMethods;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use Analogue;
use App\ApplicationLayer\Accounts\CustomMappers\ChargeDtoMapper;
use App\ApplicationLayer\Accounts\CustomMappers\SingleChargeDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\GuestDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomNameDto;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use App\DomainModelLayer\Accounts\GuestData;
use App\DomainModelLayer\Accounts\ModelAnswerUnlocked;
use App\DomainModelLayer\Schools\School;
use App\Framework\Exceptions\CustomException;
use App\ApplicationLayer\Accounts\Dtos\LanguageDto;
use Illuminate\Support\Facades\Session;
use PDF;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\AccountType;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Http\Request;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Accounts\Dtos\ExtraDto;
use App\ApplicationLayer\Accounts\Dtos\ResetPasswordDto;
use App\ApplicationLayer\Accounts\CustomMappers\CustomerInfoDtoMapper;
use App\ApplicationLayer\Accounts\CustomMappers\InvoiceDetailsDtoMapper;
use App\ApplicationLayer\Accounts\CustomMappers\InvoicesDtoMapper;
use App\Helpers\Repository;
use App\DomainModelLayer\Accounts\User;
use Illuminate\Support\Facades\Response;
use App\Helpers\ResponseObject;
use App\Helpers\Mapper;
use App\Framework\Exceptions\UnauthorizedException;
use App\ApplicationLayer\Accounts\Dtos\UserPlansDto;
use App\ApplicationLayer\Accounts\Dtos\UserSubscriptionDto;
use App\DomainModelLayer\Accounts\PlanHistory;
use App\ApplicationLayer\Accounts\Dtos\SubscriptionDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\ApplicationLayer\Accounts\Dtos\UserLoginDto;
use Carbon\Carbon;
use DB;
use Hash;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionHistory;
use App\DomainModelLayer\Accounts\FreeSubscription;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\ApplicationLayer\Accounts\Dtos\UserJourneyRequestDto;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyMinifiedDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\ApplicationLayer\Accounts\Dtos\UserBasicInfoDto;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\PlanPeriodDto;
use App\ApplicationLayer\Accounts\Dtos\PlanHistoryDto;
use App\DomainModelLayer\Accounts\UserCreated;
use App\DomainModelLayer\Accounts\UserCreatedHandle;
use App\ApplicationLayer\Accounts\Dtos\AccountTypeDto;
use App\ApplicationLayer\Accounts\Dtos\InvitationDto;
use App\ApplicationLayer\Accounts\Dtos\NotificationDto;
use App\ApplicationLayer\Accounts\Dtos\UserNotificationDto;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\NotificationTranslation;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use App\DomainModelLayer\Accounts\UserPosition;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\ApplicationLayer\Accounts\Dtos\PasswordResetDto;
use App\Helpers\PaymentModule;
use View;

class UserService
{
    //region Properties
    private $accountRepository;
    private $journeyRepository;
    private $usercreatedhandle;

    public $default_emails = ['info@robogarden.ca','support@robogarden.ca'];
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository, IJourneyMainRepository $journeyRepository, UserCreatedHandle $usercreatedhandle)
    {
        $this->accountRepository = $accountRepository;
        $this->journeyRepository = $journeyRepository;
        $this->usercreatedhandle = $usercreatedhandle;
    }
    //endregion

    //region Functions
    public function IsEmailAvailable($email)
    {
        $existingCustomer = $this->accountRepository->AlreadyRegistered($email);
        if ($existingCustomer == null)
            return true;
        return false;
    }

    public function addUserCoupon(UserDto $userDto,$coupon_id){
        $this->accountRepository->beginDatabaseTransaction();
        $userDto->accounttype = "Family";
        $userDto->plan = "Rising Coder";
        $userDto->role = "parent";

        $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);
        if($userDto->social_id != null) {
            if(isset($userDto->email) && $userDto->email != null){
                $status = $this->accountRepository->getStatusByName('active');
            } else {
                $status = $this->accountRepository->getStatusByName('wait_activation');
            }
        }
        else {
            $status = $this->accountRepository->getStatusByName('wait_activation');
        }
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));
        
        $account = new Account($userDto, $account_type,$status);

        //Creating a new user for this account
        $user = new User($userDto, $account);
        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if ($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));


        if($userDto->social_id != null && isset($userDto->email) && $userDto->email != null && $userDto->role != 'parent') {
            $userScore = $this->accountRepository->getUserScore($user->getId());
            if($userScore != null){
                $newCoins = $userScore->getCoins() + 60;
                $userScore->setCoins($newCoins);
            }
            else{
                $userScore = new UserScore($user, 0, 60);
            }
            $this->accountRepository->storeUserScore($userScore);
        }

        //Creating a new role for this user
        $role = $this->accountRepository->getRoleByName($userDto->role);
        //$user_role = new UserRole($role);
        $user->addRole($role);

        DB::transaction(function () use ($user, $userDto, $account_type,$coupon_id) {

            $this->accountRepository->AddUser($user);
            $dto = new UserSubscriptionDto();
            $dto->stripe_token = $userDto->stripe_token;
            $dto->plan = $userDto->plan;
            $dto->no_of_children = -1;
            $dto->period = "monthly";
            $dto->accounttype_id = $account_type->getId();
            $dto->user = $user;
            $this->subscribe($dto, false,$coupon_id);

        });

        $event = new UserCreated($account, $user);
        $this->usercreatedhandle->handle($event);
        $this->accountRepository->commitDatabaseTransaction();
        return UserDtoMapper::CustomerMapper($user);
    }

    /*Adding a new user with an account and user role*/
    public function Add(UserDto $userDto)
    {
        $this->accountRepository->beginDatabaseTransaction();
        if ($userDto->role == "child" && $userDto->accounttype == "Family") {
            if(!$this->checkSupervisorEmail($userDto->supervisorEmail))
                throw new CustomException(trans('locale.supervisor_email_failed',451));
            //get childs for this supervisor email
            $childs = $this->accountRepository->getChildsBySupervisorEmail($userDto->supervisorEmail);
            if (sizeof($childs) > 0) {
                $userDto->childId = $this->addChildToDefaultAccount($userDto);
            } else {
                $userDto->childId = $this->addFirstChild($userDto);
            }
            if ($this->accountRepository->supervisorEmailExists($userDto->supervisorEmail)) {
                $parent = $this->accountRepository->getUserByEmail($userDto->supervisorEmail);
                $parentName = $parent->getName();
                $parentAccountTypeName = $parent->getAccount()->getAccountType()->getName();
                $childObject = $this->accountRepository->getUserById($userDto->childId);
                $this->accountRepository->requestParentEmail($childObject, $userDto->supervisorEmail, $parentAccountTypeName, $parentName);
            } else {
                $parentAccountTypeName = "Unregistered";
                $parentName = $userDto->fname . ' ' . $userDto->lname . ' Parent';
                $childObject = $this->accountRepository->getUserById($userDto->childId);
                $this->accountRepository->requestParentEmail($childObject, $userDto->supervisorEmail, '', '');
            }
            $this->accountRepository->commitDatabaseTransaction();
            return trans('locale.child_added');
        }
        else {
            //Getting account ready
            $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);
            if($userDto->social_id != null)
            {
                $status = $this->accountRepository->getStatusByName('active');
            }
            else
                $status = $this->accountRepository->getStatusByName('suspended');
            if($status == null)
                throw new BadRequestException(trans('locale.status_not_exist'));
            $account = new Account($userDto, $account_type,$status);
            //$account is added for that user

            //Creating a new user for this account
            $user = new User($userDto, $account);
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if ($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));

            $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
            if ($existingUsername != null)
                throw new BadRequestException(trans('locale.username_exists'));
            //$user is ready now

            //Creating a new role for this user
            $role = $this->accountRepository->getRoleByName($userDto->role);
            //$user_role = new UserRole($role);
            $user->addRole($role);
            //$user has a user role now

            DB::transaction(function () use ($user, $userDto, $account_type) {
                //Store Account, User and User Role in database
                $this->accountRepository->AddUser($user);
                //User is stored now with his role and account

                //create a dto for subscription
                $dto = new UserSubscriptionDto();
                $dto->stripe_token = $userDto->stripe_token;
                $dto->plan = $userDto->plan;
                $dto->period = "monthly";
                $dto->accounttype_id = $account_type->getId();

                // add user to dto
                $dto->user = $user;

                // passing dto to subscribe function to handle subscription
                $this->subscribe($dto, true);

                if ($userDto->child_id != null)
                    $this->addUserChild($user->getId(), $userDto->child_id);

                if ($userDto->extra_plans != null && count($userDto->extra_plans) > 0) {
                    foreach ($userDto->extra_plans as $extra_plan) {
                        $this->addExtraPlan($user->getId(), $extra_plan);
                    }
                }

            });

            //Firing event after creating the user (Sending Confirmation Mail)
            if ($userDto->social_id == null) {
                $event = new UserCreated($account, $user);
                $this->usercreatedhandle->handle($event);
                //Now the user has to activate his account
            }
            $this->accountRepository->commitDatabaseTransaction();
            return UserDtoMapper::CustomerMapper($user);
        }
    }

    /*Adding user function is finished now*/

    public function addChildManually($parent_id, UserDto $childDto)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $parent = $this->accountRepository->getUserById($parent_id);
        if ($parent == null)
            throw new BadRequestException(trans('locale.parent_not_exist'));

        $account = $parent->getAccount();
        if ($account->getAccountType()->getName() != "Family")
            throw new BadRequestException(trans('locale.parent_account_not_family'));

        $subscription = $this->accountRepository->getActiveSubscriptions($account->getId())->last();
        if(count($subscription) > 0){
//            $subscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
//            if(count($subscriptions) == 0){
//                if(count($account->getChildren()) >= 1)
//                    throw new BadRequestException(trans('locale.subscribe_to_add'));
//            }
            $max_children = 1;
            $invitation_subscription = $subscription->getInvitationSubscription()->first();
            if($invitation_subscription != null)
            {
                $invitation = $invitation_subscription->getInvitation();
                if($invitation->getAccountType() == 'Family')
                {
                    $max_children = $invitation->getFamilyInfo()->getMaxChildren();
                    $children_count = count($account->getUsers()) - 1;
                    if($children_count >= $max_children)
                        throw new BadRequestException(trans('locale.invitation_max_children'));
                }
            }
            $childId = $this->addChildToDefaultAccount($childDto);
            $this->addUserChild($parent->getId(), $childId, null, $max_children,true);
            $this->accountRepository->commitDatabaseTransaction();
            return trans('locale.child_added');
        }
        throw new BadRequestException(trans('locale.subscribe_to_add'));
    }

    public function addStudent(UserDto $StudentDto)
    {
        $user = new User($StudentDto);
        $classroom = $this->classroomrepository->findClassroomById($StudentDto->classroom_id);
        if ($classroom == null)
            throw new BadRequestException(trans('locale.classroom_not_exist'));

        $user->addClassroom($classroom);

        $this->userRepository->addstudent($user);
        return UserDtoMapper::CustomerMapper($user);
    }

    public function loginGoogle($code, $clientId, $redirectUri, $clientSecret)
    {
        $params = [
            'code' => $code,
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'redirect_uri' => $redirectUri,
            'grant_type' => 'authorization_code',
        ];

        $profile = $this->accountRepository->getGoogleProfileResponse($params);
        $alreadyRegistered = $this->accountRepository->checkSocialUserRegistered($profile['sub'], 'google');
        if (!$alreadyRegistered) {
            $userDto = new UserDto();
            $userDto->fname = $profile['given_name'];
            $userDto->lname = $profile['family_name'];
            $userDto->email = $profile['email'];
            if (!$this->IsEmailAvailable($userDto)) {
                throw new BadRequestException(trans('locale.email_exists'));
            }
            $userDto->image_link = str_replace('sz=50', 'sz=250', $profile['picture']);
            $userDto->username = 'google' . strtolower($profile['given_name']) . $profile['sub'];
            $userDto->password = $profile['sub'];
            $userDto->social_id = $profile['sub'];
            $userDto->social_track = 'Google';
            //$userDto->role = 'user';
            //$userDto->plan = 'Free';
            return $userDto;
            // return $this->Add($userDto);
        } else {
            $userDto = new UserLoginDto();
            $userDto->username = $profile['sub'];
            $userDto->password = 'Google';
            return $this->Login($userDto);
        }
    }

    public function loginFacebook($code, $clientId, $redirectUri, $clientSecret)
    {
        $params = [
            'code' => $code,
            'client_id' => $clientId,
            'redirect_uri' => $redirectUri,
            'client_secret' => $clientSecret
        ];
        // Step 2. Retrieve profile information about the current user.
        $profile = $this->accountRepository->getFacebookProfileResponse($params);
        $alreadyRegistered = $this->accountRepository->checkSocialUserRegistered($profile['id'], 'facebook');
        if (!$alreadyRegistered) {
            $userDto = new UserDto();
            $userDto->fname = $profile['first_name'];
            $userDto->lname = $profile['last_name'];
            // if(!array_key_exists('email', $profile))
            // {
            //     throw new BadRequestException("You Must Provide Your mail");
            // }
            if (array_key_exists('email', $profile)) {
                $userDto->email = $profile['email'];
                if (!$this->IsEmailAvailable($userDto)) {
                    throw new BadRequestException(trans('locale.email_exists'));
                }
            } else {
                $userDto->email = null;
            }
            // $userDto->email = $profile['email'];
            $userDto->image_link = $profile['picture'];
            $userDto->username = $profile['username'];
            $userDto->password = $profile['password'];
            $userDto->social_id = $profile['id'];
            $userDto->social_track = 'Facebook';
            //$userDto->accounttype = 'Individual';
            //$userDto->role = 'user';
            //$userDto->plan = 'Free';
            return $userDto;
            // return $this->Add($userDto);
        } else {
            // $userDto = new UserDto();
            // $userDto->social_id = $profile['id'];
            // $userDto->social_track = 'Facebook';
            $userDto = new UserLoginDto();
            $userDto->username = $profile['id'];
            $userDto->password = 'Facebook';
            return $this->Login($userDto);
        }
    }

    public function loginTwitter($oauthToken, $oauthVerifier, $redirectUri, $consumerKey, $consumerSecret)
    {
        // Part 1 of 2: Initial request from Satellizer.
        if (!$oauthToken || !$oauthVerifier) {
            $tokenOAuth = $this->accountRepository->requestTwitterTokenOauth($consumerKey, $consumerSecret, $redirectUri);
            return $tokenOAuth;
        } // Part 2 of 2: Second request after Authorize app is clicked.
        else {
            $profile = $this->accountRepository->getTwitterAccessTokenOauth($consumerKey, $consumerSecret, $oauthToken, $oauthVerifier);
            $alreadyRegistered = $this->accountRepository->checkSocialUserRegistered($profile['id'], 'twitter');
            if (!$alreadyRegistered) {

                $userDto = new UserDto();
                $userDto->fname = $profile['name'];
                $userDto->lname = '';
                $userDto->email = null;
                $userDto->image_link = str_replace('_normal', '', $profile['profile_image_url']);
                $userDto->username = 'twitter' . strtolower($profile['screen_name']) . $profile['id'];
                $userDto->password = $profile['id'];
                $userDto->social_id = $profile['id'];
                $userDto->social_track = 'Twitter';
//                $userDto->accounttype = 'Individual';
//                $userDto->role = 'user';
//                $userDto->plan = 'Free';
                return $userDto;
                // return $this->Add($userDto);
            } else {
                $userDto = new UserLoginDto();
                $userDto->username = $profile['id'];
                $userDto->password = 'Twitter';
                return $this->Login($userDto);
            }
        }
    }

    public function Login(UserLoginDto $userloginDto,$gmt_difference)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $response = new ResponseObject();
        $response->token = $this->accountRepository->LoginUser($userloginDto,$gmt_difference);
        $response->user = $this->accountRepository->GetUser($userloginDto->username);
        
        $account = $response->user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $hostnameIsRestricted = env('HOSTNAME_RESTRICTED',false);
        //Check if user entering from the right school hostname
        if($hostnameIsRestricted){
            $userHostName = parse_url($_SERVER['HTTP_REFERER'])['host'];
            $schoolHostName = $school->host_name;
            
            if($userHostName != $schoolHostName){
                throw new UnauthorizedException(trans('locale.wrong_credentials'));
            }
        }


        $response->host_name = $school->host_name;

        $isStudent = false;
        foreach ($response->user->getRoles() as $role) {
            if($role->getName() == 'student')
                $isStudent = true;
        }
        if($isStudent)
        {
            if($response->user->showDayTip == 1)
                $response->user->showDayTip=true;
            else
                $response->user->showDayTip=false;
        }else{
            unset($response->user['showDayTip']);
        }
        $response->RoboPalAuth = $this->authenticateRobopal($response->user->getId());
        $response->status = $response->user->getAccount()->getStatus();
        $account = $response->user->getAccount();
        $response->use_stripe = $account->isStripeAccount();
        $response->account_type = $response->user->getAccount()->getAccountType()->getName();
        $response->invited = false;
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $response->trialEnd = '1970-01-01 00:00:00';
        if (count($subscriptions) == 0) {
            $users = $account->getUsers();
            $child = false;
            foreach ($response->user->getRoles() as $role) {
                if ($role->getName() == 'child')
                    $child = true;
            }
            if($response->user->getSupervisorEmail() != null && $child && count($users) == 1)
            {
                $status = $this->accountRepository->getStatusByName('suspended');
                if($status == null)
                    throw new BadRequestException(trans('locale.status_not_exist'));

                $account->setStatus($status);
                $this->accountRepository->storeAccount($account);
                throw new UnauthorizedException(trans('locale.trial_period_end'));
            }
        } else {
            $subscription = $subscriptions->last();
            $stripe_subscription = $subscription->getStripeSubscriptions()->first();
            if ($stripe_subscription != null)
                $response->trialEnd = $this->accountRepository->getTrialEnd($stripe_subscription);

            $invitation_subscription = $subscription->getInvitationSubscription()->first();
            if($invitation_subscription != null)
                $response->invited = true;
        }
 


        $lastSubscription = $this->accountRepository->getLastSubscriptionById($account->getId());
        if($lastSubscription!==null) {
            $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
            if ($invitationSubscription != null)
                $response->invited = true;
        }

        $response->status_code = 200;
        $new_response = new ResponseObject();
        $new_response->Object = $response;
        $new_response->status_code = $response->status_code;
        $new_response->error_source = $response->error_source;
        $new_response->errorMessage = $response->errorMessage;
        $this->accountRepository->commitDatabaseTransaction();
        return $response;
    }

    public function Remove($customerId)
    {
        $existingCustomer = $this->userRepository->RemoveCustomer($customerId);
        return trans('locale.customer_deleted');
    }

    public function GetAllCustomers()
    {
        return (trans('locale.hello_world'));
        // return CustomerDtoMapper::CustomerMapper();
    }

    public function Get($customerId)
    {
        return UserDtoMapper::CustomerMapper($this->userRepository->FindById($customerId));
    }

    public function Edit($customerId)
    {
        return IUserDtoMapper::map($this->userRepository->FindById($customerId));
    }

    public function Update(UserDto $customerDto, $customerId)
    {
        $existingCustomer = $this->userRepository->FindById($customerId);
        if ($existingCustomer == null)
            throw new \InvalidArgumentException(trans('locale.customer_not_exist'));
        $this->userRepository->Update($customerId, $customerDto);
    }

    /**
     * @return IUserRepository
     */
    public function activate($token_url)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $check = $this->accountRepository->validate_url($token_url);
        if (!is_null($check)) {
            $account = $this->accountRepository->activate($token_url);
            $users = $account->getUsers();
            $user = $users->first();
            $account_type = $account->getAccountType()->getName();
            $invited = false;
            if ($account_type == 'Family') {
                foreach ($users as $account_user) {
                    foreach ($account_user->getRoles() as $role) {
                        if ($role->getName() == 'parent') {
                            $user = $account_user;
                            break;
                        }
                        else if($role->getName() == 'child'){
                            $userScore = $this->accountRepository->getUserScore($account_user->id);
                            if($userScore != null){
                                $newCoins = $userScore->getCoins() + 60;
                                $userScore->setCoins($newCoins);
                            }
                            else{
                                $userScore = new UserScore($account_user, 0, 60);
                            }
                            $this->accountRepository->storeUserScore($userScore);
                        }
                    }
                }
            }
            else if($account_type == 'Individual'){
                $userScore = $this->accountRepository->getUserScore($user->id);
                if($userScore != null){
                    $newCoins = $userScore->getCoins() + 60;
                    $userScore->setCoins($newCoins);
                }
                else{
                    $userScore = new UserScore($user, 0, 60);
                }
                $this->accountRepository->storeUserScore($userScore);
            }
            $user['role'] = $user->getRoles()->first()['name'];
            $user['country'] = $user->getAccount()->getCountry();
            $user['status'] = $account->getStatus();
            $user['account_type'] = $account_type;

            $lastSubscription = $this->accountRepository->getLastSubscription($account);
            if($lastSubscription->getInvitationSubscription()->first() != null){
                $invited = true;
            }
            $user['invited'] = $invited;

            $token = $this->accountRepository->getTokenfromUser($user->getId());
            $this->accountRepository->commitDatabaseTransaction();

            return ["user" => $user, "token" => $token, "RoboPalAuth" => $this->authenticateRobopal($user->getId())];
        } else {
            throw new BadRequestException(trans('locale.account_not_found'));
        }
    }

    public function activateParent($child_id)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $child = $this->accountRepository->getUserById($child_id);
        if ($child == null)
            throw new BadRequestException(trans('locale.child_not_exist'));

        $email = $child->getSupervisorEmail();

        $user = $this->accountRepository->getUserByEmail($email);

        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $status = $this->accountRepository->getStatusByName('active');
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));

        $account->setStatus($status);

        $token = $this->accountRepository->getTokenfromUser($user->getId());
        $this->accountRepository->storeAccount($account);
        $this->accountRepository->commitDatabaseTransaction();
        return ["user" => $user, "token" => $token];
    }

    /**
     * get all plans for a specific account type
     * @param  int $accountType_id
     * @return UserDto  $object
     */
    public function getAvailablePlans($accountType_name)
    {
        $accountType = $this->accountRepository->getAccountTypeByName($accountType_name);
        if ($accountType == null)
            throw new BadRequestException(trans('locale.account_name_not_found'));
        return Mapper::MapEntityCollection(UserPlansDto::class, $this->accountRepository->getPlansByAccountType($accountType->id));
    }

    public function getOrderedAvailablePlans($accountType_name)
    {
        $newPlan = [];
        $accountType = $this->accountRepository->getAccountTypeByName($accountType_name);
        if ($accountType == null)
            throw new BadRequestException(trans('locale.account_name_not_found'));
        $plans = Mapper::MapEntityCollection(UserPlansDto::class, $this->accountRepository->getPlansByAccountType($accountType->id));
        foreach ($plans as $plan) {
            if($plan->Name == 'Rising Coder'){
                array_splice($newPlan, 1, 0, array($plan));
            } else {
                $newPlan[] = $plan;
            }
        }
        return $newPlan;
    }

    public function getAvailablePlansByAccountName($accountType_id)
    {
        return Mapper::MapEntityCollection(UserPlansDto::class, $this->accountRepository->getPlansByAccountType($accountType_id));
    }

    /**
     * subscrube a user to a new plan
     * @param  UserSubscriptionDto $userSubscriptionDto
     * @return Subscription         $subscription
     */
    public function subscribe(UserSubscriptionDto $userSubscriptionDto, $trial = false,$coupon_id = null)
    {
        $this->accountRepository->beginDatabaseTransaction();

        if ($userSubscriptionDto->plan == null)
            throw new BadRequestException(trans('locale.plan_missing'));

        // get user from dto
        $user = $this->accountRepository->getUserById($userSubscriptionDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_missing'));

        // get the account of user
        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_account_not_found'));

        // get plan from name
        $plan = $this->accountRepository->getPlanByName($userSubscriptionDto->plan);
        if ($plan == null)
            throw new BadRequestException(trans('locale.plan_not_found'));

        // get the last updated version of plan from plan history
        $accounttype = $account->getAccountType();
        $updated_plan = $this->accountRepository->getlastupdated($plan, $accounttype->getId());
        if ($updated_plan == null)
            throw new BadRequestException(trans('locale.version_plan_not_found'));

        $subscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())->first();
        if($subscription != null){
            $old_plan = $subscription->getPlan();
            if($old_plan->getId() == $plan->getId())
                throw new BadRequestException(trans('locale.plan_is_same'));

            if ($old_plan->getCreditPerMonth() > $updated_plan->getCreditPerMonth())
                throw new BadRequestException(trans('locale.not_downgrade_plan'));
        }

        // check if user have already subscribed to this plan before
        if ($this->accountRepository->checkSubscription($updated_plan, $account))
            throw new BadRequestException(trans('locale.subscribed_plan'));

        // handle the subscription logic
        if($account->getStripeId() != null){
            if ($userSubscriptionDto->stripe_token != null){
                $newAccount = $this->updateCreditCardToken($account, $userSubscriptionDto->stripe_token);
                $this->accountRepository->storeAccount($newAccount);
            }
        }
        else {
            if ($userSubscriptionDto->stripe_token != null){
                $newAccount = $this->addCreditCardToken($account, $userSubscriptionDto->stripe_token);
                $this->accountRepository->storeAccount($newAccount);
            }
            else{
                throw new BadRequestException(trans('locale.have_credit_token'));
            }
        }

        // change role of invited individual to user
        $InvitedRole = false;
        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'invited_Individual')
                $InvitedRole = $role;
        }
        if($InvitedRole){
            $userRole = $this->accountRepository->getRoleByName('user');
            if($userRole != null){
                $user->removeRole($InvitedRole);
                $user->addRole($userRole);
                $this->accountRepository->storeUser($user);
            }
        }

        if($account->getAccountType()->getName() == 'Family'){

            if(($userSubscriptionDto->no_of_children == null || $userSubscriptionDto->no_of_children == "")){
                throw new BadRequestException(trans('locale.no_children_required'));
            }
            if($userSubscriptionDto->no_of_children > 5)
                throw new BadRequestException(trans('locale.max_children_to_add_is_five'));

//            $cancelledSubscription = $this->accountRepository->getCancelledSubscription($account->getId());
//            if($cancelledSubscription != null){
                if($userSubscriptionDto->no_of_children < 0)
                    $userSubscriptionDto->no_of_children = 0;
                $childrenCount = count($account->getChildren()) - 1;
                if($userSubscriptionDto->no_of_children < $childrenCount){
                    throw new BadRequestException(trans('locale.add_more_children'));
                }
//            }

            if($userSubscriptionDto->no_of_children > 0){
                $subscription = $this->handlePaidSubscription($account, $userSubscriptionDto->period, $updated_plan, null, $user, $trial, $userSubscriptionDto->no_of_children);
                $subscriptionObject = $this->accountRepository->getActiveSubscriptions($account->getId())->last();
                $subscription_item = new SubscriptionItem($subscriptionObject, $updated_plan, $subscription->stripeResult["childItemId"], $userSubscriptionDto->no_of_children);
                $subscriptionObject->addSubscriptionItem($subscription_item);
                $this->accountRepository->storeSubscription($subscriptionObject);
            }
            else{
                $subscription = $this->handlePaidSubscription($account, $userSubscriptionDto->period, $updated_plan, null, $user, $trial,0,$coupon_id);
            }
        }
        else{
            $subscription = $this->handlePaidSubscription($account, $userSubscriptionDto->period, $updated_plan, null, $user, $trial,0,$coupon_id);
        }

        $this->markUnsubscribeNotificationsAsRead($user);
        $this->accountRepository->commitDatabaseTransaction();
        return $subscription;
    }

    public function markUnsubscribeNotificationsAsRead(User $user)
    {
        $users = $user->getAccount()->getUsers();
        foreach ($users as $user) {
            $userNotifications = $this->accountRepository->getUserUnreadNotications($user->getId());
            foreach ($userNotifications as $userNotification) {
                $translations = $userNotification->getTranslations();
                foreach ($translations as $translation) {
                    if (($translation->getLanguageCode() == "en" && ($translation->getTitle() == "Unsubscription" || $translation->getTitle() == "Parent Confirmation Requested")) ||
                        ($translation->getLanguageCode() == "en" && ($translation->getTitle() == "Invitation End"))) {
                        $userNotification->confirmed();
                        $this->accountRepository->storeNotification($userNotification);
                        break;
                    }
                }
            }
        }
    }

    /**
     * handle free subscription logic in database
     * @param  Account $account
     * @param  PlanHistory $updated_plan
     * @return Subscription $subscription
     */
    public function handleFreeSubscription(Account $account, PlanHistory $updated_plan, User $user)
    {
        // create a new subsciption dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = Carbon::now()->addDays($updated_plan->getTrialPeriod());

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto, $updated_plan, $account);

        $free_subscription = new FreeSubscription($subscription);

        $subscription->addFreeSubscription($free_subscription);

        $dto = new NotificationDto();
        $dto->StartDate = Carbon::parse($subscriptionDto->EndDate)->subDays(7);
        $dto->EndDate = $subscriptionDto->EndDate;
        $dto->Type = "warning";

        $notification = new Notification($dto, $user);

        $translations = $this->accountRepository->translateNotification('trial_end', 'our_trial_subscription_expire_at');
        foreach ($translations as $translation){
            $dto->Title = $translation['title'];
            $dto->Message = $translation['message'] . " " . $subscriptionDto->EndDate;

            $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
            $notification->addTranslation($notification_translation);
        }

        $user->addNotification($notification);

        // passing the subscription instance to subscription repository to be saved
        DB::transaction(function () use ($subscription, $user) {
            $this->accountRepository->storeSubscription($subscription);
            $this->accountRepository->storeUser($user);
        });

        // send an email to user through repository
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        $this->accountRepository->emailSubscription($user_name, $updated_plan->getName(), 0, 'usd', Carbon::parse($subscriptionDto->EndDate)->diffInDays(Carbon::parse($subscriptionDto->StartDate)) . ' days', 1, $user->getEmail());
        return $subscriptionDto;
    }

    /**
     * handle paid subscription logic in database and on stripe
     * @param  Account $account
     * @param  int $plan_period
     * @param  PlanHistory $updated_plan
     * @param  string $stripe_token
     * @return Subscription $subscription
     */
    public function handlePaidSubscription(Account $account, $period, PlanHistory $updated_plan, $stripe_token, User $user, $trial = false,$noOfChildren = 0,$coupon_id = null)
    {
        if ($period == null)
            throw new BadRequestException(trans('locale.plan_period_missing'));

        $plan_period = $this->accountRepository->getPlanPeriodByName($period);
        if ($plan_period == null)
            throw new BadRequestException(trans('locale.plan_period_not_exist'));

        // handle subscription on stripe
        $stripe_result = $this->accountRepository->handlePaidSubscription($stripe_token, $updated_plan->getName(), $period, $account->getAccountType()->getName(), $account->getStripeId(), $trial, $account->getCountry(),$noOfChildren,$coupon_id);

        $activeSubscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())->first();

        if($activeSubscription != null){
            $activeSubscription->setEndDate(Carbon::now());
            $this->accountRepository->storeSubscription($activeSubscription);
        }

        // create a new subsciption dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now()->startOfDay();
        $subscriptionDto->EndDate = null;

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto, $updated_plan, $account);

        $stripe_subscription = new StripeSubscription($subscription, $plan_period, $stripe_result['subscription_id']);

        $subscription->addStripeSubscription($stripe_subscription);

        // update account with the new stripe id
        //$subscription->account->setStripeId($stripe_result['customer_id']);

        $this->accountRepository->storeSubscription($subscription);

        // passing the subscription instance to subscription repository to be saved
        //$this->accountRepository->storeAccount($account);

        if($user->getEmail() != null){
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $this->accountRepository->emailSubscription($user_name,$updated_plan->getName(),$stripe_result['subscription_amount'],$stripe_result['subscription_currency'],'Per '.$stripe_result['subscription_interval'],1,$user->getEmail(),$trial);
        }
        $subscriptionDto->stripeResult = $stripe_result;
        return $subscriptionDto;
    }


    // public function addChild($parent, UserDto $userDto)
    // {
    //     $account = $this->accountRepository->getAccountByUser($parent);
    //     //Getting account ready
    //     //Creating a new user for this account
    //     $user = new User($userDto, $account);
    //     $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
    //     if($existingUserMail != null)
    //         throw new BadRequestException("User with this email already exists");

    //     $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
    //     if($existingUsername != null)
    //         throw new BadRequestException("User with this username already exists");
    //     //$user is ready now

    //     //Creating a new role for this user
    //     $role = $this->accountRepository->getRoleByName($userDto->role);
    //     //$user_role = new UserRole($role);
    //     $user->addRole($role);
    //     //$user has a user role now

    //     DB::transaction(function () use ($user,$userDto){
    //         //Store Account, User and User Role in database
    //         $this->accountRepository->AddUser($user);
    //         //User is stored now with his role and account
    //     });

    //     return UserDtoMapper::CustomerMapper($user);
    // }

    /**
     * enroll a user to a new journey
     * @param  UserJourneyRequestDto $userJourneyRequestDto
     * @return JourneyDto              $object
     */

    public function addJourney(UserJourneyRequestDto $userJourneyRequestDto)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($userJourneyRequestDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);
        $max_journeys = $subscription->getPlan()->getMaximumNoJourneys();
        $count = 0;
        foreach ($userJourneyRequestDto->journeys as $selected_journey) {
            if ((count($subscription->getJourneys()) + $count) == $max_journeys)
                throw new BadRequestException(trans('locale.not_add_journey'));

            $journey = $this->journeyRepository->getJourneyById($selected_journey["journey_id"]);
            if ($journey == null)
                throw new BadRequestException(trans('locale.journey_not_exist'));

            if ($subscription->isEnrolledJourney($journey))
                throw new BadRequestException(trans('locale.user_enrolled_journey'));

            $subscription->addJourney($journey);
        }

        $this->accountRepository->storeSubscription($subscription);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapEntityCollection(JourneyDto::class, $subscription->getJourneys(), [JourneyCategoryDto::class]);
    }

//    public function getUserJourneys($user_id, $minified = null)
//    {
//        $user = $this->accountRepository->getUserById($user_id);
//        if ($user == null)
//            throw new BadRequestException(trans('locale.user_not_exist'));
//
//        $account = $user->getAccount();
//        //$subscription = $this->accountRepository->getLastSubscription($account);
//        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
//        $journeys_dtos = [];
//        $journeyCounter = 0;
//        $enrolled_count = 0;
//        $remaining_count = 0;
//        foreach ($subscriptions as $subscription) {
//
//            if($subscription->getStripeSubscriptions()->first() == null) {
//                $invitation_subscription = $subscription->getInvitationSubscription()->first();
//                if ($invitation_subscription != null) {
//                    $invitation = $invitation_subscription->getInvitation();
//                    //$enrolled_count = count($invitation->getJourneys());
//                    //$remaining_count = 0;
//                    foreach ($invitation->getJourneys() as $invitation_journey) {
//                        if($invitation_journey->getJourney()->getStatus() == 'published'){
//                            if($minified){
//                                $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $invitation_journey->getJourney());
//                            } else {
//                                $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $invitation_journey->getJourney(), [JourneyCategoryDto::class]);
//                            }
//                            $journeys_dtos[$journeyCounter]->gradeNumber = $invitation_journey->getJourney()->getData()->getGrade();
//                            $journeyCounter++;
//                        }
//
//                    }
//
//                    $plans = $invitation->getPlans();
//                    foreach ($plans as $plan) {
//                        $categories = $plan->getCategories();
//                        foreach ($categories as $category) {
//                            $journeys = $category->getJourneys();
//                            foreach ($journeys as $journey) {
//                                if($journey->getStatus() == 'published'){
//                                    if($minified) {
//                                        $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
//                                    } else {
//                                        $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
//                                    }
//                                    $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
//                                    $journeyCounter++;
//                                }
//
//                            }
//                        }
//                    }
//                    // return ["enrolled_count"=>$enrolled_count,"remaining_count"=>$remaining_count,"journeys"=>$journeys_dtos];
//
//                }
//            }else {
//                $categories = $subscription->getPlan()->getCategories();
//                foreach ($categories as $category) {
//                    $journeys = $category->getJourneys();
//                    foreach ($journeys as $journey) {
//                        if($journey->getStatus() == 'published'){
//                            if($minified) {
//                                $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
//                            } else {
//                                $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
//                            }
//                            $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
//                            $journeyCounter++;
//                        }
//
//                    }
//                }
//
//                $items = $subscription->getItems();
//                foreach ($items as $item) {
//                    if ($item->getEndDate() == null || $item->getEndDate() > Carbon::now()) {
//                        $categories = $item->getPlan()->getCategories();
//                        foreach ($categories as $category) {
//                            $journeys = $category->getJourneys();
//                            foreach ($journeys as $journey) {
//                                if($journey->getStatus() == 'published'){
//                                    if($minified) {
//                                        $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
//                                    } else {
//                                        $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
//                                    }
//                                    $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
//                                    $journeyCounter++;
//                                }
//
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        $grades = array();
//        foreach ($journeys_dtos as $key => $row) {
//            $grades[$key] = $row->gradeNumber;
//        }
//        array_multisort($grades, SORT_ASC, $journeys_dtos);
//
//        //$enrolled_count = count($subscription->getJourneys());
//
//        //$remaining_count = intval($subscription->getPlan()->getMaximumNoJourneys()) - intval($enrolled_count);
//        // return ["enrolled_count"=>$enrolled_count,"remaining_count"=>$remaining_count,"journeys"=>Mapper::MapEntityCollection(JourneyDto::class,$subscription->getJourneys(),[JourneyCategoryDto::class])];
//        return ["enrolled_count" => $enrolled_count, "remaining_count" => $remaining_count, "journeys" => $journeys_dtos];
//    }

    public function getUserJourneys($user_id, $minified = null){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        //$subscription = $this->accountRepository->getLastSubscription($account);
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $journeys_dtos = [];
        $unlockedJourneyIds = [];
        $journeyCounter = 0;
        $enrolled_count = 0;
        $remaining_count = 0;

        foreach ($subscriptions as $subscription) {
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription != null){
                $invitation = $invitationSubscription->getInvitation();
                //$enrolled_count = count($invitation->getJourneys());
                //$remaining_count = 0;
                foreach ($invitation->getJourneys() as $invitation_journey) {

                    if($invitation_journey->getJourney()->getStatus() == 'published'){

                        if(!in_array($invitation_journey->getJourney()->getId(),$unlockedJourneyIds)){
                            if($minified) {
                                $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $invitation_journey->getJourney());
                            } else {
                                $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $invitation_journey->getJourney(), [JourneyCategoryDto::class]);
                            }
                            $journeys_dtos[$journeyCounter]->gradeNumber = $invitation_journey->getJourney()->getData()->getGrade();
                            $journeys_dtos[$journeyCounter]->locked = false;
                            $journeyCounter++;
                            array_push($unlockedJourneyIds,$invitation_journey->getJourney()->getId());
                        }
                    }

                }

                $plans = $invitation->getPlans();
                foreach ($plans as $plan) {
                    $categories = $plan->getCategories();
                    foreach ($categories as $category) {
                        $journeys = $category->getJourneys();
                        foreach ($journeys as $journey) {

                            if($journey->getStatus() == 'published'){
                                if(!in_array($journey->getId(),$unlockedJourneyIds)){
                                    //$enrolled_count++;
                                    if($minified) {
                                        $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
                                    } else {
                                        $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
                                    }
                                    $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
                                    $journeys_dtos[$journeyCounter]->locked = false;
                                    $journeyCounter++;
                                    array_push($unlockedJourneyIds,$journey->getId());
                                }

                            }

                        }
                    }
                    $unlockables = $plan->getPlanUnlockables();
                    foreach ($unlockables as $unlockable){
                        $journeyUnlockable = $unlockable->getUnlockable()->getJourneyUnlockable();
                        if($journeyUnlockable != null){
                            $journey = $journeyUnlockable->getJourney();
                            if($journey->getStatus() == 'published'){
                                if(!in_array($journey->getId(),$unlockedJourneyIds)){
                                    if($minified) {
                                        $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
                                    } else {
                                        $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
                                    }
                                    $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
                                    $journeys_dtos[$journeyCounter]->locked = false;
                                    $journeyCounter++;
                                    array_push($unlockedJourneyIds,$journey->getId());
                                }

                            }

                        }
                    }
                }
            }
            else{
                $plan = $subscription->getPlan();
                $categories = $plan->getCategories();
                foreach ($categories as $category) {
                    $journeys = $category->getJourneys();
                    foreach ($journeys as $journey) {
                        if($journey->getStatus() == 'published'){
                            if(!in_array($journey->getId(),$unlockedJourneyIds)){
                                if($minified) {
                                    $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
                                } else {
                                    $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
                                }
                                $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
                                $journeys_dtos[$journeyCounter]->locked = false;
                                $journeyCounter++;
                                array_push($unlockedJourneyIds,$journey->getId());
                            }
                        }

                    }
                }

                $unlockables = $plan->getPlanUnlockables();
                foreach ($unlockables as $unlockable){
                    $journeyUnlockable = $unlockable->getUnlockable()->getJourneyUnlockable();
                    if($journeyUnlockable != null){
                        $journey = $journeyUnlockable->getJourney();
                        if($journey->getStatus() == 'published'){
                            if(!in_array($journey->getId(),$unlockedJourneyIds)){
                                if($minified) {
                                    $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
                                } else {
                                    $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
                                }
                                $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
                                $journeys_dtos[$journeyCounter]->locked = false;
                                $journeyCounter++;
                                array_push($unlockedJourneyIds,$journey->getId());
                            }

                        }

                    }
                }

                $items = $subscription->getItems();
                foreach ($items as $item) {
                    if ($item->getEndDate() == null || $item->getEndDate() > Carbon::now()) {
                        $categories = $item->getPlan()->getCategories();
                        foreach ($categories as $category) {
                            $journeys = $category->getJourneys();
                            foreach ($journeys as $journey) {
                                if($journey->getStatus() == 'published'){
                                    if(!in_array($journey->getId(),$unlockedJourneyIds)){
                                        if($minified) {
                                            $journeys_dtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $journey);
                                        } else {
                                            $journeys_dtos[] = Mapper::MapClass(JourneyDto::class, $journey, [JourneyCategoryDto::class]);
                                        }
                                        $journeys_dtos[$journeyCounter]->gradeNumber = $journey->getData()->getGrade();
                                        $journeys_dtos[$journeyCounter]->locked = false;
                                        $journeyCounter++;
                                        array_push($unlockedJourneyIds,$journey->getId());
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        $grades = array();
        foreach ($journeys_dtos as $key => $row) {
            $grades[$key] = $row->gradeNumber;
        }
        array_multisort($grades, SORT_ASC, $journeys_dtos);

        $locked = array();
        foreach ($journeys_dtos as $key => $row) {
            $locked[$key] = $row->locked;
        }
        array_multisort($locked, SORT_DESC, $journeys_dtos);
        //$enrolled_count = count($subscription->getJourneys());

        //$remaining_count = intval($subscription->getPlan()->getMaximumNoJourneys()) - intval($enrolled_count);
        // return ["enrolled_count"=>$enrolled_count,"remaining_count"=>$remaining_count,"journeys"=>Mapper::MapEntityCollection(JourneyDto::class,$subscription->getJourneys(),[JourneyCategoryDto::class])];
        return ["enrolled_count" => count($journeys_dtos), "remaining_count" => $remaining_count, "journeys" => $journeys_dtos,"unlocked_ids"=>$unlockedJourneyIds];
    }

    public function getUserJourneysIncludingLocked($user_id, $minified = null){
        $unlockedJourneys = $this->getUserJourneys($user_id, $minified);
        $lockedJourneysDtos = [];
        $journeyCounter = 0;
        $lockedJourneys = $this->journeyRepository->getJourneyNotInIds($unlockedJourneys['unlocked_ids']);

        foreach ($lockedJourneys as $lockedJourney){
            if($lockedJourney->getStatus() == 'published') {
                if ($minified) {
                    $lockedJourneysDtos[] = Mapper::MapClass(JourneyMinifiedDto::class, $lockedJourney);
                } else {
                    $lockedJourneysDtos[] = Mapper::MapClass(JourneyDto::class, $lockedJourney, [JourneyCategoryDto::class]);
                }
                $lockedJourneysDtos[$journeyCounter]->gradeNumber = $lockedJourney->getData()->getGrade();
                $lockedJourneysDtos[$journeyCounter]->locked = true;
                $journeyCounter++;
            }
        }
        $merged = array_merge($unlockedJourneys['journeys'],$lockedJourneysDtos);
        $grades = array();
        foreach ($merged as $key => $row) {
            $grades[$key] = $row->gradeNumber;
        }
        array_multisort($grades, SORT_ASC, $merged);

        $locked = array();
        foreach ($merged as $key => $row) {
            $locked[$key] = $row->locked;
        }
        array_multisort($locked, SORT_DESC, $merged);
        return $merged;

    }

    public function updateBasicInfo($user_id, UserBasicInfoDto $userBasicInfoDto)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($userBasicInfoDto->email == null)
        {
            $required = true;
            $userRoles = $user->getRoles();
            foreach ($userRoles as $role) {
                if($role->getName() == 'child' || $role->getName() == 'student')
                    $required = false;
            }
            if($required)
                throw new BadRequestException(trans('locale.email_field_missing'));
        }

        $existingUsername = $this->accountRepository->UserNameAlreadyExistAnotherUser($userBasicInfoDto->username , $user);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        if($userBasicInfoDto->email != null)
        {
            $existingUserMail = $this->accountRepository->AlreadyRegisteredAnotherUser($userBasicInfoDto->email , $user);
            if($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));

            if($user->getEmail() != $userBasicInfoDto->email){
                $this->resetChildsSupervisorEmail($userBasicInfoDto->email,$user->getEmail());
            }
        }

        $user->setFirstName($userBasicInfoDto->fname);
        $user->setLastName($userBasicInfoDto->lname);
        $user->setEmail($userBasicInfoDto->email);
        $user->setUsername($userBasicInfoDto->username);
        $user->setSocialId($userBasicInfoDto->socialId);
        $this->accountRepository->storeUser($user);

        $userBasicInfoDto =  Mapper::MapClass(UserBasicInfoDto::class,$user);
        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);
        $userBasicInfoDto->planType = $subscription->getPlan()->getName();
        $this->accountRepository->commitDatabaseTransaction();
        return $userBasicInfoDto;
    }

    public function updatePassword($user_id, $old_password, $new_password)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if (!Hash::check($old_password, $user->getPassword()))
            throw new BadRequestException(trans('locale.password_not_match'));
        $user->setPassword(Hash::make($new_password));
        $this->accountRepository->storeUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(UserBasicInfoDto::class, $user);
    }

    public function updateProfileImage($user_id, $image_name, $image_data)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $url = $this->accountRepository->saveProfileImage($user, $image_name, $image_data);
        $user->setImage($url);
        $this->accountRepository->storeUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(UserBasicInfoDto::class, $user);
    }

    public function updateSchoolLogo($user_id, $image_name, $image_data)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $url = $this->accountRepository->saveProfileImage($user, $image_name, $image_data);
        $user->getAccount()->getSchool()->setSchoolLogoUrl($url);
        $this->accountRepository->storeUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        $userMapper = Mapper::MapClass(UserBasicInfoDto::class, $user);
        $userMapper->schoolIconUrl = $url;
        return $userMapper;
    }

    public function getBasicInfo($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $userBasicInfoDto = Mapper::MapClass(UserBasicInfoDto::class, $user,[ClassroomNameDto::class]);
        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);
        $userBasicInfoDto->planType = $subscription->getPlan()->getName();
        $userBasicInfoDto->stripe_token_exists = ($account->getStripeId() != null ? true : false);
        return $userBasicInfoDto;
    }

    public function getUserScores($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        return $user->getScores();
    }

    public function getAllPlanPeriods()
    {
        return Mapper::MapEntityCollection(PlanPeriodDto::class, $this->accountRepository->getAvailblePlanPeriods());
    }

    public function getUserSubscriptions($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription_dtos = Mapper::MapEntityCollection(SubscriptionDto::class, $subscriptions);
        $counter = 0;

        foreach ($subscriptions as $subscription) {
            if ($subscription_dtos[$counter]->EndDate == null)
                $subscription_dtos[$counter]->Active = true;

            elseif ($subscription_dtos[$counter]->EndDate > Carbon::now())
                $subscription_dtos[$counter]->Cancelled = true;

            $subscription_dtos[$counter]->plan_id = $subscription->getPlan()->getId();
            $subscription_dtos[$counter]->price = $subscription->getPlan()->getCreditPerMonth();
            $counter ++;
        }

        foreach($subscription_dtos as $elementKey => $element) {
            foreach($element as $valueKey => $value) {
                if($valueKey == 'PlanName' && $value == 'Free'){
                    //delete this particular object from the $array
                    unset($subscription_dtos[$elementKey]);
                }
            }
        }

        $subscriptions = [];
        foreach ($subscription_dtos as $subscription_dto){
            array_push($subscriptions,$subscription_dto);
        }
        return $subscriptions;
    }

    public function unsubscribe($user_id, $subscription_id, $if_child = false)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $subscription = $this->accountRepository->getSubscriptionById($subscription_id);
        if ($subscription == null)
            throw new BadRequestException(trans('locale.subscription_not_exist'));

        $stripe_subscription = $subscription->getStripeSubscriptions()->first();
        $subsciption_result = $this->accountRepository->handleCancelPaidSubscription($stripe_subscription);
        $subscription_status = $subsciption_result['cancel_at_period_end'];
        if (!$subscription_status)
            throw new BadRequestException(trans('locale.subscription_cancel_failed'));
        $end_date = date('Y-m-d H:i:s', $subsciption_result['period_end']);
        $subscription->setEndDate($end_date);

        $old_plan = $subscription->getPlan();

        DB::transaction(function () use ($user, $end_date, $subscription, $if_child) {
            $this->accountRepository->storeSubscription($subscription);
            $dto = new NotificationDto();
            if (!$if_child) {
                $dto->StartDate = Carbon::parse($end_date)->subDays(7);
                $dto->EndDate = $end_date;
                $dto->Type = "warning";
            } else {
                $dto->StartDate = Carbon::parse($end_date)->subDays(7);
                $dto->EndDate = $end_date;
                $dto->Type = "warning";
            }

            $notification = new Notification($dto, $user);

            if (!$if_child) {
                $translations = $this->accountRepository->translateNotification('unsubscription', 'account_expire_at');
                foreach ($translations as $translation) {
                    $dto->Title = $translation['title'];
                    $dto->Message = $translation['message'] . " " . $end_date;

                    $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
                    $notification->addTranslation($notification_translation);
                }
            } else {
                $translations = $this->accountRepository->translateNotification('parent_confirmation_requested', ['remind_parent_add_account', $end_date, 'parent_not_confirm']);
                foreach ($translations as $translation) {
                    $dto->Title = $translation['title'];
                    $dto->Message = $translation['message'];

                    $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
                    $notification->addTranslation($notification_translation);
                }
            }

            $user->addNotification($notification);
            $this->accountRepository->storeUser($user);

        });
        $items = $subscription->getItems();
        foreach ($items as $item) {
            if($item->getEndDate() == null)
            {
                $old_item = $item;
                $item->setEndDate($subscription->getEndDate());
                $subscription->removeSubscriptionItem($old_item);
                $subscription->addSubscriptionItem($item);
            }
        }
        $this->accountRepository->storeSubscription($subscription);

        // send an email to user through repository
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        $this->accountRepository->emailUnsubscription($user_name, $old_plan->getName(), $subscription->getEndDate(), $user->getEmail());
        $this->accountRepository->commitDatabaseTransaction();
        return ["ends_at" => $subscription->getEndDate()];
    }

    public function upgradePlan($user_id, $subscription_id, $plan_name, $period, $stripe_token = null)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account == null)
            throw new BadRequestException(trans('locale.account_not_found'));

        $subscription = $this->accountRepository->getSubscriptionById($subscription_id);
        if ($subscription == null)
            throw new BadRequestException(trans('locale.subscription_not_exist'));

        if($subscription->getAccount()->getId() != $account->getId())
            throw new BadRequestException(trans('locale.not_have_subscription'));

        if(!$this->accountRepository->isAuthorized('upgrade_subscription', $user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $plan = $this->accountRepository->getPlanByName($plan_name);
        if ($plan == null)
            throw new BadRequestException(trans('locale.plan_not_found'));

        $plan_period = $this->accountRepository->getPlanPeriodByName($period);
        if ($plan_period == null)
            throw new BadRequestException(trans('locale.plan_period_not_exist'));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $account->getAccountType()->getId());
        if ($updated_plan == null)
            throw new BadRequestException(trans('locale.version_plan_not_found'));

        $old_plan = $subscription->getPlan();
        if($old_plan->getId() == $plan->getId())
            throw new BadRequestException(trans('locale.plan_is_same'));

        if ($old_plan->getCreditPerMonth() > $updated_plan->getCreditPerMonth())
            throw new BadRequestException(trans('locale.not_downgrade_plan'));

        if ($stripe_token != null && !empty($stripe_token)){
            $accounttypeunt = $this->updateCreditCardToken($account, $stripe_token);
            $this->accountRepository->storeAccount($accounttypeunt);
        }

        $stripe_subscription = $subscription->getStripeSubscriptions()->first();
        if ($stripe_subscription != null) {
            $plan_id = $this->accountRepository->handleUpgradeSubscription($stripe_subscription, $updated_plan->getName(), $period, $user->getAccount()->getAccountType()->getName())["plan_id"];
            $mode = config('services.payment.mode');
            $new_name = $plan_name.'_'.$user->getAccount()->getAccountType()->getName().'_'.$period.'_'.$mode;
            if ($plan_id != $new_name)
                throw new BadRequestException(trans('locale.subscription_upgrading_failed'));

            $subscription_history = new SubscriptionHistory($subscription);
            $subscription->addHistory($subscription_history);
            $subscription->setPlan($updated_plan);
            $subscription->removeEndsAt();

            $item = $this->accountRepository->getChildItem($subscription);
            if($item != null) {
                $item_plan = $this->accountRepository->getPlanByName($updated_plan->getName() . '_child');
                if ($item_plan == null)
                    throw new BadRequestException(trans('locale.plan_not_found'));

                $item_updated_plan = $this->accountRepository->getlastupdated($item_plan, $user->getAccount()->getAccountType()->getId());
                if ($item_updated_plan == null)
                    throw new BadRequestException(trans('locale.version_plan_not_found'));
                $this->accountRepository->removeSubscriptionItem($item->getStripeId());
                $stripe_item = $this->accountRepository->addSubscriptionItem($stripe_subscription, $item_updated_plan->getName(), 'monthly', 'Family', true, $item->getQuantity());
                $subscription_item = new SubscriptionItem($subscription, $item_updated_plan, $stripe_item->id, $item->getQuantity());
                $subscription->removeSubscriptionItem($item);
                $this->accountRepository->deleteSubscriptionItem($item);
                $subscription->addSubscriptionItem($subscription_item);
            }
            $this->accountRepository->storeSubscription($subscription);

            // send an email to user through repository
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $this->accountRepository->emailUpgrade($user_name, $updated_plan->getName(), $old_plan->getName(), $user->getEmail());
            $this->accountRepository->commitDatabaseTransaction();
            return trans('locale.subscription_upgraded');
        }
        throw new BadRequestException(trans('locale.something_wrong'));
    }

    public function getPlanByName($name, $account_type = 'Individual')
    {
        $accounttype = $this->accountRepository->getAccountTypeByName($account_type);
        if ($accounttype == null)
            throw new BadRequestException(trans('locale.account_type_not_found'));

        $plan = $this->accountRepository->getPlanByName($name);
        if ($plan == null)
            throw new BadRequestException(trans('locale.plan_not_found'));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $accounttype->getId());
        if ($updated_plan == null)
            throw new BadRequestException(trans('locale.version_plan_not_found'));
        return Mapper::MapClass(PlanHistoryDto::class, $updated_plan);
    }

    public function getInvitationByUser($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $subscription = $user->getAccount()->getSubscriptions()->first();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();

        if ($invitation_subscription == null)
            throw new BadRequestException(trans('locale.subscription_not_invitation'));

        $invitation = $invitation_subscription->getInvitation();

        return Mapper::MapClass(InvitationDto::class, $invitation);
    }

    public function handleSubscriptionEnd(User $user)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $account = $user->getAccount();
        $users = $account->getUsers();
        $child = false;
        foreach ($user->getRoles() as $role) {
            if ($role->getName() == 'child')
                $child = true;
        }
        if($user->getSupervisorEmail() != null && $child && count($users) == 1)
        {
            $status = $this->accountRepository->getStatusByName('suspended');
            if($status == null)
                throw new BadRequestException(trans('locale.status_not_exist'));

            $account->setStatus($status);
            $this->accountRepository->storeAccount($account);
            throw new UnauthorizedException(trans('locale.subscription_period_ended'));
        }

        $subscription = $this->accountRepository->getLastSubscription($account);
        $invitationSubscription = $subscription->getInvitationSubscription()->first();
        if($invitationSubscription != null){
            if($user->getAccount()->getAccountType()->getName() == 'School'){
                throw new CustomException(trans('locale.school_invitation_expired_register'), 406);
            }
            else if ($user->getAccount()->getAccountType()->getName() == 'Family'){
                throw new CustomException(trans('locale.invitation_expired_register'), 406);
            }
            else {
                throw new CustomException(trans('locale.individual_invitation_expired_register'), 406);
            }
        }
        else {
            if($user->getAccount()->getAccountType()->getName() == 'School')
            {
                $status = $this->accountRepository->getStatusByName('suspended');
                if($status == null)
                    throw new BadRequestException(trans('locale.status_not_exist'));

                $account->setStatus($status);
                $this->accountRepository->storeAccount($account);

                foreach ($user->getRoles() as $role){
                    if($role->getName() == 'student'){
                        throw new CustomException(trans('locale.school_subscription_ended_contact_teacher'), 407);
                    }
                    if($role->getName() == 'teacher'){
                        throw new CustomException(trans('locale.school_subscription_ended_contact_school'), 407);
                    }
                }
                throw new CustomException(trans('locale.school_subscription_ended_contact_distributor'), 407);
            }

            $this->accountRepository->commitDatabaseTransaction();
            throw new UnauthorizedException(trans('locale.subscription_period_ended'));
        }

    }

    public function getNotifications($user_id)
    {
        $notifications = $this->accountRepository->getUserNotifications($user_id);
        if (count($notifications) == 0)
            return [];
        $current_notification = $notifications->first();
        $notifications->remove($current_notification);
        $count = 1;

        foreach ($notifications as $notification) {
            $notification->setStartDate(Carbon::now()->addMinutes(5 * ($count++)));
        }

        return [Mapper::MapClass(NotificationDto::class, $current_notification)];
    }

    public function confirmNotification($id)
    {
        $notification = $this->accountRepository->getNotificationById($id);
        if ($notification == null)
            throw new BadRequestException(trans('locale.notification_not_exist'));

        $notification->confirmed();
        $this->accountRepository->storeNotification($notification);
        return trans('locale.notification_confirmed');
    }

    public function delayNotification($id)
    {
        $notification = $this->accountRepository->getNotificationById($id);
        if ($notification == null)
            throw new BadRequestException(trans('locale.notification_not_exist'));

        $notification->setStartDate(Carbon::now()->addHours(2));
        $this->accountRepository->storeNotification($notification);
        return trans('locale.notification_delayed');
    }

    public function updateUserPosition($user_id, $journey_id, $adventure_id, $id, $type, $index)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if ($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        if ($type == 'mission') {
            $mission = $this->journeyRepository->getMissionById($id);
            if ($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $task = $mission->getTask();
        }
        elseif($type == "html_mission"){
            $mission = $this->journeyRepository->getHtmlMissionbyId($id);
            if ($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $task = $mission->getTask();

        }
        else {
            $quiz = $this->journeyRepository->getquiz($id);
            if ($quiz == null)
                throw new BadRequestException(trans('locale.quiz_not_exist'));
            $task = $quiz->getTask();
        }

        $position_x = $this->journeyRepository->getTaskOrder($task, $journey->getId(), $adventure_id)->getPositionX();
        $position_y = $this->journeyRepository->getTaskOrder($task, $journey->getId(), $adventure_id)->getPositionY();

        $userpositiondto = new UserPositionDto();
        $userpositiondto->PositionX = str_replace("px", "", $position_x);
        $userpositiondto->PositionY = str_replace("px", "", $position_y);
        $userpositiondto->Index = $index;
        $position = new UserPosition($userpositiondto, $user, $journey);
        $old_positions = $user->getPosition($journey);
        //dd($position);
        foreach ($old_positions as $old_position){
            $this->accountRepository->deletePosition($old_position);
        }

        $this->accountRepository->storePosition($position);
        //$this->accountRepository->storeUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.position_updated');
    }

    public function getParentChildren($id)
    {
        $children = $this->accountRepository->getParentChildren($id);
        $counter = 0;
        $results = array();
        foreach ($children as $child) {
            $results[$counter] = $child;
            $results[$counter]['total'] = ($this->getUserJourneys($child->id)['enrolled_count']) * 40;
            $userDefaultActivities = $this->getUserDefaultActivities($child->id);

            $journeys_progress = [];
            $tutorialsSolvedCounter = 0;
            $quizzesSolvedCounter = 0;
            $innercounter = 0;
            $childProgress = 0;

            foreach ($userDefaultActivities  as $userDefaultActivity){
                $results[$counter]['total'] += count($userDefaultActivity->getTasks());
                $childProgress += count($this->journeyRepository->getSolvedTasksInActivity($child->id,$userDefaultActivity->getId(),true));
            }
            foreach ($this->getUserJourneys($child->id)['journeys'] as $journeydto) {
                $journeys_progress[] = $child->getJourneyProgress($journeydto);
                $tutorialsSolvedCounter += $journeys_progress[$innercounter]['tutorials'];
                $quizzesSolvedCounter += $journeys_progress[$innercounter]['mainQuizzes'];
                $childProgress += $journeys_progress[$innercounter]['missions'];
                $innercounter++;
            }
            $results[$counter]['child_progress'] = $childProgress;
            $results[$counter]['journeys_progress'] = $journeys_progress;
            $results[$counter]['quizzes'] = $quizzesSolvedCounter;
            $results[$counter]['tutorials'] = $tutorialsSolvedCounter;
            $counter++;
        }
        return $results;
    }

    public function addChildToDefaultAccount($userDto)
    {
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $account = $this->accountRepository->getDefaultAccount();
        $role = $this->accountRepository->getRoleByName($userDto->role);
        if ($role == null)
            throw new BadRequestException(trans('locale.role_not_found'));

        $user = new User($userDto, $account);
        $user->addRole($role);
        $this->accountRepository->AddUser($user);
        return $user->getId();
    }

    public function addFirstChild(UserDto $userDto)
    {
        $account_type=$this->accountRepository->getAccountTypeByName('Family');
        if($userDto->social_id != null)
        {
            $status = $this->accountRepository->getStatusByName('active');
        }
        else
            $status = $this->accountRepository->getStatusByName('suspended');
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));
        $account = new Account($userDto, $account_type,$status);

        //add the customer id to child account
        $account = $this->addCreditCardToken($account, null);

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $role = $this->accountRepository->getRoleByName('child');
        if ($role == null)
            throw new BadRequestException(trans('locale.role_not_found'));

        $plan = $this->accountRepository->getPlanByName('default');
        if ($plan == null)
            throw new BadRequestException(trans('locale.plan_not_found'));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $account->getAccountType()->getId());
        if ($updated_plan == null)
            throw new BadRequestException(trans('locale.version_plan_not_found'));

        $stripe_result = $this->accountRepository->handlePaidSubscription(null, $updated_plan->getName(), "monthly", $account->getAccountType()->getName(), $account->getStripeId(), true);

        // create a new subsciption dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = null;

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto, $updated_plan, $account);
        $plan_period = $this->accountRepository->getPlanPeriodByName("monthly");
        $stripe_subscription = new StripeSubscription($subscription, $plan_period, $stripe_result['subscription_id']);

        $subscription->addStripeSubscription($stripe_subscription);

        // update account with the new stripe id
        $subscription->account->setStripeId($stripe_result['customer_id']);

        //$free_subscription = new FreeSubscription($subscription);
        //$subscription->addFreeSubscription($free_subscription);

        $account->addSubscription($subscription);
        $status = $this->accountRepository->getStatusByName('active');
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));

        $account->setStatus($status);

        $user = new User($userDto, $account);
        $user->addRole($role);

        // $dto = new NotificationDto();
        // $dto->Title = "Invitation End";
        // $dto->Message = "Your Invited account will expire at ".$invitation->getEndDate();
        // $dto->StartDate = Carbon::parse($invitation->getEndDate())->subDays(7);
        // $dto->EndDate = $invitation->getEndDate();
        // $dto->Type = "warning";
        // $notification = new Notification($dto,$user);
        // $notification_translation = new NotificationTranslation($dto,'en',$notification);
        // $notification->addTranslation($notification_translation);
        // $dto->Title = "?????? ??????";
        // $dto->Message = "????? ??? ????? ?? ".$end_date;
        // $notification_translation = new NotificationTranslation($dto,'ar',$notification);
        // $notification->addTranslation($notification_translation);
        // $user->addNotification($notification);
        $this->accountRepository->AddUser($user);

        //unsubscribe first child
        $this->unsubscribe($user->getId(), $subscription->getId(), true);

        // $event = new UserCreated($account,$user);
        // $this->usercreatedhandle->handle($event);

        // $this->accountRepository->emailSubscription($user->getName(),'Invitation',0,'usd',$invitation->getEndDate(),$invitation->getNumberOfUsers(),$user->getEmail());

        return $user->getId();
    }

    public function addCreditCardToken(Account $account, $stripe_token)
    {
        $customer_id = $this->accountRepository->createStripeCustomer($stripe_token);
        $account->setStripeId($customer_id);
        return $account;
    }

    public function updateCreditCardToken(Account $account, $stripe_token)
    {
        $customer_id = $this->accountRepository->updateStripeCustomer($account->getStripeId(), $stripe_token);
        $account->setStripeId($customer_id);
        return $account;
    }

    public function updateCreditCard($user_id, $stripe_token)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        if ($account->getStripeId() == null) {
            $account = $this->addCreditCardToken($account, $stripe_token);
        } else {
            $account = $this->updateCreditCardToken($account, $stripe_token);
        }

        $this->accountRepository->storeAccount($account);
        return trans('locale.payment_info_updated');
    }

    public function getUserType($child_id)
    {
        $child = $this->accountRepository->getUserById($child_id);
        if ($child == null)
            throw new BadRequestException(trans('locale.child_not_exist'));

        $email = $child->getSupervisorEmail();

        $user = $this->accountRepository->getUserByEmail($email);
        if ($user == null)
            return ["type" => false, "max_users" => 0];

        $account = $user->getAccount();
        $accounttype = $account->getAccountType()->getName();

        if ($accounttype != 'Individual' && $accounttype != 'Family')
            throw new BadRequestException(trans('locale.account_type_not_supported'));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());

        if (count($subscriptions) == 0)
            throw new BadRequestException(trans('locle.subscription_ended'));

        $subscription = $subscriptions->last();

        $subscription_items = $subscription->getItems();

        $max_users = 2 + count($subscription_items);

        return ["type" => $accounttype, "max_users" => $max_users];
    }

    public function addUserChild($user_id, $child_id, $stripe_token = null, $max_children = 1, $invited = false)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $child = $this->accountRepository->getUserById($child_id);
        if ($child == null)
            throw new BadRequestException(trans('locale.child_not_exist'));

        $account = $user->getAccount();

        if (!$invited && count($this->accountRepository->getActiveSubscriptions($account->getId())) == 0) {
            throw new BadRequestException(trans('locale.not_add_child'));
        }

        $child_account = $child->getAccount();

        if ($account->getId() == $child_account->getId())
            throw new BadRequestException(trans('locale.parent_of_child'));

        if ($account->getAccountType()->getName() != 'Family')
            throw new BadRequestException(trans('locale.not_family_account'));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if (count($subscriptions) == 0)
            throw new BadRequestException(trans('locale.subscription_ended'));

        // add 60 coin for each new user
        if ($account->getStatus() == 'active'){
            $lastSubscription = $this->accountRepository->getLastSubscriptionById($account->getId());
            $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
            if($invitationSubscription == null){
                $userScore = $this->accountRepository->getUserScore($child->getId());
                if($userScore != null){
                    $newCoins = $userScore->getCoins() + 60;
                    $userScore->setCoins($newCoins);
                } else {
                    $userScore = new UserScore($child, 0, 60);
                }
                $this->accountRepository->storeUserScore($userScore);
            }
        }

        $subscription = $subscriptions->last();
        $max_users = $max_children + 1;
        $has_items = false;
        $subscription_item = $this->accountRepository->getChildItem($subscription);

        if ($subscription_item != null) {
            $max_users += $subscription_item->getQuantity();
            $has_items = true;
        }
        $account_users = count($account->getUsers());
        $remaining_users = $max_users - $account_users;
        $old_account = $child->getAccount();
        if ($remaining_users > 0) {
            $this->accountRepository->storeAccount($account);
            $child->setAccount($account);
            $this->accountRepository->storeUser($child);
        }
        else {
            if($max_users - 1 > 5)
                throw new BadRequestException(trans('locale.max_children_to_add_is_five'));
            $notCancelledSubscription = $this->accountRepository->getNotCancelledSubscriptionsWithoutFree($account->getId());
            if($notCancelledSubscription != null){
                $old_subscription = $subscription;
                if ($account->getStripeId() == null) {
                    throw new BadRequestException(trans('locale.have_credit_token'));
                }

                $plan = $this->accountRepository->getPlanByName($subscription->getPlanName() . '_child');
                if ($plan == null)
                    throw new BadRequestException(trans('locale.plan_not_found'));

                $updated_plan = $this->accountRepository->getlastupdated($plan, $account->getAccountType()->getId());
                if ($updated_plan == null)
                    throw new BadRequestException(trans('locale.version_plan_not_found'));

                // trial subscription
                if ($subscription->getStripeSubscriptions()->first() != null)
                    $stripe_subscription = $subscription->getStripeSubscriptions()->first();

                else {
                    throw new BadRequestException(trans('locale.subscribe_to_add'));
                }

                if (!$has_items) {
                    $stripe_item = $this->accountRepository->addSubscriptionItem($stripe_subscription, $updated_plan->getName(), 'monthly');
                    $subscription_item = new SubscriptionItem($subscription, $updated_plan, $stripe_item->id);
                } else {
                    $this->accountRepository->changeSubscriptionItemQuantity($subscription_item->getStripeId());
                    $old_subscription_item = $subscription_item;
                    $subscription_item->setQuantity($subscription_item->getQuantity() + 1);
                    $subscription->removeSubscriptionItem($old_subscription_item);
                }

                $subscription->addSubscriptionItem($subscription_item);

                $account->removeSubscription($old_subscription);

                $account->addSubscription($subscription);

                $this->accountRepository->storeAccount($account);
                $child->setAccount($account);
                $this->accountRepository->storeUser($child);
                if ($old_account->getStripeId() != 'default')
                    $this->accountRepository->deleteAccount($old_account);
            }
            else{
                throw new BadRequestException(trans('locale.reached_max_children'));
            }
        }

        if ($user->getEmail() != $child->getSupervisorEmail() && $user->getEmail() != null) {
            $this->resetChildsSupervisorEmail($user->getEmail(), $child->getSupervisorEmail());
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.child_added');
    }

    public function convertToFamily($user_id)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $userAccountTypeName = $user->getAccount()->getAccountType()->getName();
        if ($userAccountTypeName != "Individual")
            throw new BadRequestException(trans('locale.ccount_not_individual'));
        $familyAccountType = $this->accountRepository->getFamilyAccountType();
        $userAccount = $user->getAccount();
        $userAccount->setAccountType($familyAccountType);

        $subscriptions = $this->accountRepository->getActiveSubscriptions($userAccount->getId());

        if (count($subscriptions) == 0)
            throw new BadRequestException(trans('locale.subscription_ended'));

        $subscription = $subscriptions->last();

        $old_subscription = $subscription;

        $plan = $this->accountRepository->getPlanByName($subscription->getPlanName());
        if ($plan == null)
            throw new BadRequestException(trans('locale.plan_not_found'));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $userAccount->getAccountType()->getId());

        if ($updated_plan == null)
            throw new BadRequestException(trans('locale.version_plan_not_found'));

        $stripe_subscription = $subscription->getStripeSubscriptions()->first();

        if ($stripe_subscription != null) {
            $plan_id = $this->accountRepository->handleUpgradeSubscription($stripe_subscription, $updated_plan->getName(), 'monthly', 'Family')["plan_id"];
            // if($plan_id != $subscription->getPlanName().'_monthly')
            //     throw new BadRequestException("Subscription Upgrading failed");

            $subscription->removeEndsAt();

            $items = $subscription->getItems();

            foreach ($items as $item) {
                $item_plan = $this->accountRepository->getPlanByName($item->getPlan()->getName());
                if ($item_plan == null)
                    throw new BadRequestException(trans('locale.item_plan_not_found'));

                $item_updated_plan = $this->accountRepository->getlastupdated($item_plan, $familyAccountType->getId());
                $this->accountRepository->removeSubscriptionItem($item->getStripeId());
                $stripe_item = $this->accountRepository->addSubscriptionItem($stripe_subscription, $item_updated_plan->getName(), 'monthly', 'Family');
                $subscription_item = new SubscriptionItem($subscription, $item_updated_plan, $stripe_item->id);
                $subscription->removeSubscriptionItem($item);
                $this->accountRepository->deleteSubscriptionItem($item);
                $subscription->addSubscriptionItem($subscription_item);
            }

        } else {
            $this->unsubscribe($user->getId(), $old_subscription->getId());
            $plan_period = $this->accountRepository->getPlanPeriodByName('monthly');
            if ($plan_period == null)
                throw new BadRequestException(trans('locale.plan_period_not_exist'));
            $stripe_result = $this->accountRepository->handlePaidSubscription(null, $updated_plan->getName(), 'monthly', 'Family', $userAccount->getStripeId(), false, $userAccount->getCountry());

            $stripe_subscription = new StripeSubscription($subscription, $plan_period, $stripe_result['subscription_id']);

            $subscription->addStripeSubscription($stripe_subscription);

            $subscription->removeEndsAt();
        }

        $subscription->setPlan($updated_plan);

        $userAccount->removeSubscription($old_subscription);

        $userAccount->addSubscription($subscription);

        $parentRole = $this->accountRepository->getRoleByName("parent");
        $user->removeRole($user->getRoles()->first());
        $user->addRole($parentRole);
        $this->accountRepository->storeUser($user);

        $this->accountRepository->storeAccount($userAccount);

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.account_type_changed');
    }

    public function customerInfo($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $customerId = $user->getAccount()->getStripeId();
        if ($customerId == null){
            foreach ($user->getRoles() as $role){
                if(strpos(strtolower($role->getName()), 'invited') !== false){
                    return trans('locale.invited_user');
                }
            }
            throw new BadRequestException(trans('locale.not_have_customer_id'));
        }

        $output = $this->accountRepository->customerInfo($customerId);
        return CustomerInfoDtoMapper::PaymentModuleMapper($output);
    }

    public function customerInvoices($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $customerId = $user->getAccount()->getStripeId();
        if ($customerId == null) {
            foreach ($user->getRoles() as $role) {
                if (strpos(strtolower($role->getName()), 'invited') !== false) {
                    return trans('locale.invited_user');
                }
            }
            throw new BadRequestException(trans('locale.not_have_customer_id'));
        }
        $output = $this->accountRepository->customerInvoices($customerId);
        $outputCharges = $this->accountRepository->customerCharges($customerId);

        return array_merge(InvoicesDtoMapper::PaymentModuleMapper($output),ChargeDtoMapper::PaymentModuleMapper($outputCharges));
    }

    public function getCustomerInvoiceById($user_id, $invoice_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $output = $this->accountRepository->retrieveInvoiceDetails($invoice_id);
        return InvoiceDetailsDtoMapper::PaymentModuleMapper($output['invoice'], $output['details']);
    }

    public function requestFromParent($user_id,$journey_category)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $parent_email = $user->getSupervisorEmail();

        if ($parent_email == null)
            throw new BadRequestException(trans('locale.parent_email_not_found'));

        $parentObject = $this->accountRepository->getUserByEmail($user->getSupervisorEmail());

        $category = $this->accountRepository->getCategoryByName($journey_category);
        $plan = $this->accountRepository->getPlanByCategoryAndAccountType($category->getId(),$parentObject->getAccount()->getAccountType()->getId());

        if ($parentObject == null) {
            $parent_name = '';
            $account_type = '';
            $this->accountRepository->requestParentEmail($user, $parent_email, $account_type, $parent_name);
        } else {
            if ($parentObject->getAccount()->getId() != $user->getAccount()->getId()) {
                $parent_name = $parentObject->getName();
                $account_type = $parentObject->getAccount()->getAccountType()->getName();
                $this->accountRepository->requestParentEmail($user, $parent_email, $account_type, $parent_name);
            } else {
                $this->accountRepository->requestParentToSubscribe($user, $parentObject,$journey_category,true);
            }
        }

        return trans('locale.request_send');
    }

    public function requestFromParentActivity($user_id,$activity_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2C())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $parent_email = $user->getSupervisorEmail();

        if ($parent_email == null)
            throw new BadRequestException(trans('locale.parent_email_not_found'));

        $parentObject = $this->accountRepository->getUserByEmail($user->getSupervisorEmail());

        if ($parentObject == null) {
            $parent_name = '';
            $account_type = '';
            $this->accountRepository->requestParentEmail($user, $parent_email, $account_type, $parent_name);
        } else {
            if ($parentObject->getAccount()->getId() != $user->getAccount()->getId()) {
                $parent_name = $parentObject->getName();
                $account_type = $parentObject->getAccount()->getAccountType()->getName();
                $this->accountRepository->requestParentEmail($user, $parent_email, $account_type, $parent_name);
            } else {
                $this->accountRepository->requestParentToSubscribe($user, $parentObject,$activity->getName(),false);
            }
        }

        return trans('locale.request_send');
    }

    public function sendContactUsEmail(SupportEmailDto $supportEmailDto)
    {
        $mail=$supportEmailDto->email;
         if(in_array($mail,$this->default_emails))
            throw new BadRequestException(trans("locale.no_robogarden_emails"));

        $this->accountRepository->sendContactUsEmail($supportEmailDto);
        return trans('locale.contact_email_sent');
    }

    public function sendSupportEmail($user_id, SupportEmailDto $supportEmailDto)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $this->accountRepository->sendSupportEmail($user, $supportEmailDto);
        return trans('locale.support_email_sent');
    }

//    public function handleAllUsersSubscriptions()
//    {
//        $users = $this->accountRepository->getAllUsers();
//        $logFile = storage_path('logs/schedulerlog.txt');
//        foreach ($users as $user) {
//            $account = $user->getAccount();
//            if ($account == null)
//                fwrite($logFile, "account not found for user" . $user->getName() . "\r\n");
//            if (count($this->accountRepository->getActiveSubscriptions($account->getId())) > 0) {
//                fwrite($logFile, "user already has a subscription" . $user->getName() . "\r\n");
//            } else {
//                try {
//                    $this->accountService->handleSubscriptionEnd($user);
//                } catch (\Exception $e) {
//                    fwrite($logFile, "error happended" . $e->getMessage() . "\r\n");
//                }
//            }
//        }
//        fclose($logFile);
//    }

    public function validateUserEmail($email)
    {
        $userDto = new UserDto();
        $userDto->email = $email;
        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if ($existingUserMail != null)
            return true;

        return false;
    }

    public function validateUsername($username)
    {
        $userDto = new UserDto();
        $userDto->username = $username;
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            return true;

        return false;
    }

    public function resetPassword($email)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserByEmail($email);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_with_email'));
        $username = $user->getFname() ? $user->getFname() : $user->getUsername();
        $resetPasswordDto = $this->accountRepository->createPasswordReset($email, $username);
        $this->accountRepository->sendResetPasswordEmail($resetPasswordDto);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.reset_password_email_sent');
    }

    public function confirmResetPassword(PasswordResetDto $resetPasswordDto, $newPassword)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserByEmail($resetPasswordDto->email);
        if ($user == null)
            throw BadRequestException(trans('locale.no_user_with_email'));

        $this->accountRepository->confirmResetPassword($resetPasswordDto, $newPassword);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.password_reset');
    }

    public function resetChildsSupervisorEmail($supervisorEmail, $old_email)
    {
        //DB::beginTransaction();
        $this->accountRepository->resetChildsSupervisorEmail($supervisorEmail, $old_email);
        //DB::commit();
    }

    public function downloadInvoicePDF($invoiceId)
    {
        if (strpos($invoiceId, 'in_') !== false) {
            $output = $this->accountRepository->retrieveInvoiceDetails($invoiceId);
            $invoiceDetails = InvoiceDetailsDtoMapper::PaymentModuleMapper($output['invoice'], $output['details']);
            $account = $this->accountRepository->getAccountByCustomerId($invoiceDetails->customer);
            if ($account == null)
                throw new BadRequestException(trans('locale.no_account_customer'));
            $users = $account->getUsers();
            $user = null;
            foreach ($users as $userx) {
                if ($userx->getEmail() != null)
                    $user = $userx;
            }
            //return url('/logo.png');
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $html = View::make('download.invoice',
                ['invoiceDetails' => $invoiceDetails,
                    'invoiceId' => $invoiceId,
                    'userEmail' => $user->getEmail(),
                    'userName' => $user_name,
                    'companyName' => "RoboGarden Inc.",
                    'companyAddress' => 'Suite 400, 1716-16 Ave NW Calgary, Alberta T2M 0L7',
                    'companyEmail' => 'info@robogarden.ca',
                    'companyPhone' => '+1 (403) 457-3112',
                    'imagePath' => url('/logo.png')
                ])->render();
            //return $html;
            $pdf = PDF::loadHTML($html);
            return $pdf->download(date("Y/m/d") . '_invoice.pdf');
        }
        else if(strpos($invoiceId, 'ch_') !== false){
            $output = $this->accountRepository->retrieveChargeDetails($invoiceId);
            $chargeDetails = SingleChargeDtoMapper::PaymentModuleMapper($output);

            $account = $this->accountRepository->getAccountByCustomerId($chargeDetails->customer_id);
            if ($account == null)
                throw new BadRequestException(trans('locale.no_account_customer'));
            $users = $account->getUsers();
            $user = null;
            foreach ($users as $userx) {
                if ($userx->getEmail() != null)
                    $user = $userx;
            }
            //return url('/logo.png');
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $html = View::make('download.charge',
                ['chargeDetails' => $chargeDetails,
                    'chargeId' => $chargeDetails->id,
                    'userEmail' => $user->getEmail(),
                    'userName' => $user_name,
                    'companyName' => "RoboGarden Inc.",
                    'companyAddress' => 'Suite 400, 1716-16 Ave NW Calgary, Alberta T2M 0L7',
                    'companyEmail' => 'info@robogarden.ca',
                    'companyPhone' => '+1 (403) 457-3112',
                    'imagePath' => url('/logo.png')
                ])->render();
            //return $html;
            $pdf = PDF::loadHTML($html);
            return $pdf->download(date("Y/m/d") . '_invoice.pdf');
        }

    }

    public function requestCountryChange($user_id, $newCountry)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw BadRequestException(trans('locale.no_user_id'));
        $oldCountry = $user->getAccount()->getCountry();
        if ($oldCountry != null) {
            if ($oldCountry == $newCountry)
                throw new BadRequestException(trans('locale.new_country_same'));
        }

        ///$this->accountRepository->requestCountryChange($user,$newCountry);
        $account = $this->accountRepository->changeCountryForUser($user, $newCountry);
        $this->accountRepository->storeAccount($account);
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if ($subscriptions == null)
            throwException(trans('locale.no_subscriptions_account'));

        $subscription = $subscriptions->last();
        $this->accountRepository->changeSubscriptionTax($subscription->getStripeId(), $newCountry);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.request_send');
    }

    public function getRbf($user_id, $code)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw BadRequestException(trans('locale.no_user_id'));

        return $this->accountRepository->getRbf($code);
    }

    public function authenticateRobopal($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            return ["permission" => false, "message" => trans('locale.subscription_ended'), "end_date" => null, "cancelled" => true];

        $subscription = $subscriptions->last();

        $item =  $this->accountRepository->getLastRoboPalItem($subscription->getId());

        if($item != null)
        {
            if($item->getEndDate() == null)
                return ["permission" => true, "message" => trans('locale.subscription_robopal_active'), "end_date" => null , "cancelled" => false];
            elseif($item->getEndDate() > Carbon::now())
            {
                if($account->getAccountType()->getName() == 'School')
                {
                    $distributor_sub = $subscription->getDistributorSubscriptions()->first();
                    if($distributor_sub == null)
                        throw BadRequestException(trans('locale.distributor_not_found'));
                    $use_stripe = $distributor_sub->getDistributor()->getStripeCheck();
                    if(!$use_stripe)
                        return ["permission" => true, "message" => trans('locale.subscription_robopal_end'). " " .$item->getEndDate(),"end_date"=>$item->getEndDate(), "cancelled" => false];
                }
                else
                {
                    return ["permission" => true, "message" => trans('locale.subscription_robopal_end'). " " .$item->getEndDate(),"end_date"=>$item->getEndDate(), "cancelled" => true];
                }
            }
            else
                return ["permission" => false, "message" => trans('locale.subscription_robopal_ended'), "end_date"=>$item->getEndDate(), "cancelled" => true];

        }
        foreach($subscriptions as $subscription){
            $unlockables = $subscription->getPlan()->getPlanUnlockables();
            foreach ($unlockables as $unlockable){
                $extraUnlockable = $unlockable->getUnlockable()->getExtraUnlockable();
                if($extraUnlockable != null){
                    $extraName = $extraUnlockable->getExtra()->getName();
                    if($extraName == "RoboPal")
                        return ["permission" => true, "message" => trans('locale.subscription_robopal_active'), "end_date" => null , "cancelled" => false];
                }
            }
        }

        return ["permission" => false, "message" => trans('locale.subscription_not_include_robopal'), "end_date" => null, "cancelled" => false];
    }

//    public function addExtraPlan($user_id, $plan_name)
//    {
//        $user = $this->accountRepository->getUserById($user_id);
//        if ($user == null)
//            throw BadRequestException(trans('locale.no_user_id'));
//
//        $account = $user->getAccount();
//
//        $plan = $this->accountRepository->getPlanByName($plan_name);
//        if ($plan == null)
//            throw new BadRequestException(trans('locale.plan_not_found'));
//
//        // get the last updated version of plan from plan history
//        $updated_plan = $this->accountRepository->getlastupdated($plan, $account->getAccountType()->getId());
//        if ($updated_plan == null)
//            throw new BadRequestException(trans('locale.version_plan_not_found'));
//
//        if($account->getAccountType()->getName() == 'School')
//        {
//            $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
//        }
//
//        else
//        {
//            $subscriptions = $this->accountRepository->getNotCancelledSubscriptions($account->getId());
//        }
//
//        if (count($subscriptions) == 0)
//            throw new BadRequestException(trans('locale.subscription_ended'));
//
//        $subscription = $subscriptions->last();
//
//        $items = $subscription->getItems();
//
//        foreach ($items as $item) {
//            if ($item->getPlan()->getName() == $plan_name && $item->getEndDate() == null)
//                throw new BadRequestException(trans('locale.you_have_item'));
//        }
//
//        $old_subscription = $subscription;
//
//        if ($account->getStripeId() == null) {
//            //if($stripe_token == null)
//            throw new BadRequestException(trans('locale.have_credit_token'));
//            // else
//            //     $account = $this->addCreditCardToken($account, $stripe_token);
//        }
//
//        if($subscription->getStripeSubscriptions()->first() == null)
//        {
//            if($account->getAccountType()->getName() == 'School')
//            {
//                $subscription_item = new SubscriptionItem($subscription,$updated_plan,'no_stripe');
//
//
//                $subscription_item->setEndDate($subscription->getEndDate());
//            }
//            else
//            {
//                throw new BadRequestException(trans('locale.not_add_extra_item_plans'));
//            }
//        }
//
//        else
//        {
//            $stripe_subscription = $subscription->getStripeSubscriptions()->first();
//
//            $stripe_item = $this->accountRepository->addSubscriptionItem($stripe_subscription,$updated_plan->getName(),'monthly',$account->getAccountType()->getName(),false);
//            $subscription_item = new SubscriptionItem($subscription,$updated_plan,$stripe_item->id);
//        }
//
//        $subscription->addSubscriptionItem($subscription_item);
//
//        $account->removeSubscription($old_subscription);
//
//        $account->addSubscription($subscription);
//
//        $this->accountRepository->storeAccount($account);
//
//        if($account->getAccountType()->getName() == 'School'){
//            return ["permission" => true, "message" => trans('locale.extra_item_added'), "end_date" => $subscription->getEndDate(), "cancelled" => false];
//        }
//
//        return ["permission" => true, "message" => trans('locale.extra_item_added'), "end_date" => null,"cancelled" => false];
//    }

//    public function removeExtraPlan($user_id, $plan_name)
//    {
//        $this->accountRepository->beginDatabaseTransaction();
//        $user = $this->accountRepository->getUserById($user_id);
//        if ($user == null)
//            throw BadRequestException(trans('locale.no_user_id'));
//
//        $account = $user->getAccount();
//        $plan = $this->accountRepository->getPlanByName($plan_name);
//        if ($plan == null)
//            throw new BadRequestException(trans('locale.plan_not_found'));
//
//        // get the last updated version of plan from plan history
//        $updated_plan = $this->accountRepository->getlastupdated($plan, $account->getAccountType()->getId());
//        if ($updated_plan == null)
//            throw new BadRequestException(trans('locale.version_plan_not_found'));
//
//        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
//
//        if (count($subscriptions) == 0)
//            throw new BadRequestException(trans('locale.subscription_ended'));
//
//        $subscription = $subscriptions->last();
//
//        if($subscription->getEndDate() != null)
//        {
//            if($subscription->getEndDate() > Carbon::now())
//            {
//                return ["permission" => true, "message" => trans('locale.extra_removed'), "end_date" => $subscription->getEndDate(), "cancelled" => true];
//            }
//
//            else
//            {
//                return ["permission" => false, "message" => trans('locale.extra_removed'), "end_date" => $subscription->getEndDate(), "cancelled" => true];
//            }
//        }
//
//        $plan_item = $this->accountRepository->getLastRoboPalItem($subscription->getId());
//
//        if ($plan_item == null)
//            throw new BadRequestException(trans('locale.subscription_not_include_plan'));
//
//        elseif ($plan_item->getEndDate() != null)
//            throw new BadRequestException(trans('locale.already_unsubscribed_plan'));
//
//        $old_subscription = $subscription;
//
//        if ($account->getStripeId() == null) {
//            //if($stripe_token == null)
//            throw new BadRequestException(trans('locale.have_credit_token'));
//            // else
//            //     $account = $this->addCreditCardToken($account, $stripe_token);
//        }
//
//        $old_item = $plan_item;
//
//        $stripe_subscription = $subscription->getStripeSubscriptions()->first();
//
//        if($stripe_subscription != null)
//        {
//            $subscription_data = $this->accountRepository->retrieveSubscription($stripe_subscription->getStripeId());
//
//
//            $this->accountRepository->removeSubscriptionItem($plan_item->getStripeId(),false);
//
//
//            $end_date = date('Y-m-d H:i:s',$subscription_data->current_period_end);
//        }
//
//        else
//        {
//            $end_date = date('Y-m-d H:i:s',$subscription->getEndDate());
//        }
//
//        $plan_item->setEndDate($end_date);
//
//        $subscription->removeSubscriptionItem($old_item);
//
//        $this->accountRepository->deleteSubscriptionItem($old_item);
//
//        $subscription->addSubscriptionItem($plan_item);
//
//        $account->removeSubscription($old_subscription);
//
//        $account->addSubscription($subscription);
//
//        $this->accountRepository->storeAccount($account);
//
//        $this->accountRepository->commitDatabaseTransaction();
//
//        if($end_date > Carbon::now())
//        {
//            return ["permission" => true, "message" => trans('locale.extra_removed'), "end_date" => $end_date, "cancelled" => true];
//        }
//
//        return ["permission" => false, "message" => trans('locale.extra_removed'), "end_date" => $end_date, "cancelled" => true];
//    }

//    public function getExtraPlans($accounttypename)
//    {
//        $accountType = $this->accountRepository->getAccountTypeByName($accounttypename);
//        if ($accountType == null) throw new BadRequestException(trans('locale.account_name_not_found'));
//        return Mapper::MapEntityCollection(UserPlansDto::class, $this->accountRepository->getExtraPlans($accountType->getId()));
//    }

    public function getLanguages()
    {
        return Mapper::MapEntityCollection(LanguageDto::class, $this->accountRepository->getLanguages());
    }

    public function updateUserLanguages($user_id,$language_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $language = $this->accountRepository->getLanguageById($language_id);
        if ($language == null)
            throw new BadRequestException(trans('locale.no_language'));

        $user->setLanguage($language);

        $this->accountRepository->storeUser($user);

        return trans('locale.user_language_update');
    }

    public function reportBug($error){
        $this->accountRepository->reportBug($error);
        return trans('locale.bug_reported');
    }

    public function checkSupervisorEmail($supervisor_email)
    {
        $user = $this->accountRepository->getUserByEmail($supervisor_email);
        if($user == null)
            return true;
        $account_type = $user->getAccount()->getAccountType()->getName();
        if($account_type == 'Individual' || $account_type == 'Family')
            return true;
        return false;
    }

    public function addChildrenToSubscription(Account $account,Subscription $subscription,StripeSubscription $stripe_subscription)
    {
        $children = $account->getChildren();
        if(count($children) <  2)
            return true;
        $plan = $this->accountRepository->getPlanByName($subscription->getPlanName() . '_child');
        if ($plan == null)
            throw new BadRequestException(trans('locale.plan_not_found'));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $account->getAccountType()->getId());
        if ($updated_plan == null)
            throw new BadRequestException(trans('locale.version_plan_not_found'));
        $stripe_item = $this->accountRepository->addSubscriptionItem($stripe_subscription, $updated_plan->getName(), 'monthly');
        $subscription_item = new SubscriptionItem($subscription, $updated_plan, $stripe_item->id);
        if(count($children) > 2)
        {
            $remaining_children = count($children) - 2;
            $this->accountRepository->changeSubscriptionItemQuantity($subscription_item->getStripeId(),$remaining_children);
            $old_subscription_item = $subscription_item;
            $subscription_item->setQuantity($subscription_item->getQuantity() + $remaining_children);
            $subscription->removeSubscriptionItem($old_subscription_item);
        }
        $old_subscription = $subscription;
        $subscription->addSubscriptionItem($subscription_item);

        $account->removeSubscription($old_subscription);

        //$account->addSubscription($subscription);


        $this->accountRepository->storeAccount($account);
        $this->accountRepository->storeSubscription($subscription);
        return true;
    }

    public function resendActivationMail($email)
    {
        $user = $this->accountRepository->getUserByEmail($email);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_with_email'));
        $account = $user->getAccount();
        if($account->getActive())
            throw new BadRequestException(trans('locale.user_already_active'));
        $event = new UserCreated($account, $user);
        $this->usercreatedhandle->handle($event);
        return trans('locale.activation_mail_resend_success');
    }

    public function UserSubscription($action, $user_id){
        $user = $this->accountRepository->UserSubscription($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));
        $user->setMailSubscription($action == 1 ? 1 : 0);
        $this->accountRepository->storeUser($user);
        return true;
    }

    public function checkParentChild($parent_id, $child_id){
        $parent = $this->accountRepository->getUserById($parent_id);
        if($parent == null){
            throw new BadRequestException(trans('locale.no_user_id'));
        }
        $child = $this->accountRepository->getUserById($child_id);
        if($child == null){
            throw new BadRequestException(trans('locale.child_not_exist'));
        }
        $children = $this->accountRepository->getParentChildren($parent_id);
        $exist = false;
        foreach ($children as $child) {
            if($child_id == $child->id){
                $exist = true;
            }
        }
        return $exist;
    }

    public function getChildJourneyAndActivities($parent_id, $child_id, $is_mobile = true){
        $exist = $this->checkParentChild($parent_id, $child_id);
        if($exist){
            $child = $this->accountRepository->getUserById($child_id);
            $result['name'] = $child->first_name . ' ' . $child->last_name;
            $result['journeys'] = $this->getUserJourneys($child_id, 'minified')['journeys'];
            $activities = $this->getUserDefaultActivities($child_id,$is_mobile);
            $result['activities'] = [];
            foreach ($activities as $activity){
                array_push($result['activities'],[
                    'name'=>$activity->getName(),'iconUrl'=>$activity->getIconUrl(),
                    'imageUrl'=>$activity->getImageUrl(),'id'=>$activity->getId()
                ]);
            }
            $grades = array();
            foreach ($result['journeys'] as $key => $row) {
                $grades[$key] = $row->gradeNumber;
            }
            array_multisort($grades, SORT_ASC, $result['journeys']);
            return $result;
        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getChildProgress($parent_id, $journey_id, $child_id){
        $exist = $this->checkParentChild($parent_id, $child_id);
        if($exist){
            $key = 1;
            $resultArray = array();
            $journey = $this->journeyRepository->getJourneyById($journey_id);
            if($journey != null){
                $child = $this->accountRepository->getUserById($child_id);
                $progress = $child->getTotalProgress();
                $adventures = $this->journeyRepository->getAdventuresByOrder($journey_id);
                foreach ($adventures as $adventure){
                    $tasks = $this->journeyRepository->getTasksByOrder($journey_id, $adventure->id);
                    $dataArray = array();
                    foreach ($tasks as $task){
                        $dataSuccess = false;
                        foreach ($progress as $singleProgress){
                            if($task->id  == $singleProgress->getTaskId() && $singleProgress->getFirstSuccess() != null){
                                $dataSuccess = true;
                                break;
                            }
                        }
                        array_push($dataArray,array('type' => $task->getTaskType(), 'success' => $dataSuccess));
                    }
                    array_push($resultArray,array('adventure'=>trans('locale.adventure') . " " . ($key),'data'=>$dataArray));
                    $key++;
                }
                return $resultArray;
            } else {
                throw new BadRequestException(trans('locale.journey_not_exist'));
            }
        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getChildProgressActivity($parent_id, $activity_id, $child_id){
        $exist = $this->checkParentChild($parent_id, $child_id);
        if($exist){
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
            if($activity != null) {
                $tasks = $this->journeyRepository->getActivityTasksInOrder($activity_id,true);
                $tasksDto = [];
                foreach ($tasks as $task){
                    $task = $task->getTask();
                    $taskProgress = $this->journeyRepository->getActivityProgressForTask($child_id,$task->getId(),$activity_id,true);
                    $success = false;
                    $noOfTrials = 0;

                    if($taskProgress != null){
                        if($taskProgress->getFirstSuccess() != null)
                            $success = true;
                        if($taskProgress->getNo_of_trails() != null)
                            $noOfTrials = $taskProgress->getNo_of_trails();
                    }


                    $mission = $task->getMissions()->first();
                    if($mission != null)
                    {
                        array_push($tasksDto, ['id'=>$task->getId(),
                            'name'=>$mission->getName(),'success'=>$success,
                            'type'=>'mission','noOfTrials'=>$noOfTrials

                        ]);
                    }
                    else
                    {
                        $mission = $task->getMissionsHtml()->first();
                        if($mission != null)
                        {
                            array_push($tasksDto, ['id'=>$task->getId(),
                                'name'=>$mission->getTitle(),'success'=>$success,
                                'type'=>'html_mission','noOfTrials'=>$noOfTrials
                            ]);
                        }
                        else {
                            $quiz = $task->getQuizzes()->first();
                            array_push($tasksDto, ['id'=>$task->getId(),
                                'name'=>$quiz->getTitle(),'success'=>$success,
                                'type'=>$quiz->getType()->getName(),'noOfTrials'=>$noOfTrials

                            ]);
                        }
                    }
                }
                return $tasksDto;
            }
            else{
                throw new BadRequestException(trans("locale.activity_not_exist"));
            }

        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getChildStatisticsActivity($parent_id,$activity_id,$child_id){
        $exist = $this->checkParentChild($parent_id, $child_id);
        if($exist) {
            $result = [];
            $completed_tasks = 0;
            $tasks_count = 0;
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
            if($activity != null){
              $tasks_count = count($activity->getTasks());
              $solvedActivitiesProgress = $this->journeyRepository->getSolvedActivityProgress($child_id,$activity_id,true);
              $completed_tasks = count($solvedActivitiesProgress);
              $totalScore = 0;
              foreach ($solvedActivitiesProgress as $solvedActivityProgress){
                  $totalScore += $solvedActivityProgress->getScore();
              }

              $result['completed_tasks'] = $completed_tasks;
                $result['never_played_tasks'] = ($tasks_count - $completed_tasks);
                $result['progress'] = round(($completed_tasks/$tasks_count)*100) . "%";
                $result['total_score'] = $totalScore;
                return $result;
            }
            else
                throw new BadRequestException(trans("locale.activity_not_exist"));
        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getChildStatistics($parent_id, $journey_id, $child_id){
        $exist = $this->checkParentChild($parent_id, $child_id);
        if($exist){
            $result = [];
            $completed_tasks = 0;
            $tasks_count = 0;
            $journey = $this->journeyRepository->getJourneyById($journey_id);
            if($journey != null) {
                $child = $this->accountRepository->getUserById($child_id);
                $progress = $child->getTotalProgress();
                $adventures = $this->journeyRepository->getAdventuresByOrder($journey_id);
                foreach ($adventures as $akey => $adventure) {
                    $tasks = $this->journeyRepository->getTasksByOrder($journey_id, $adventure->id);
                    foreach ($tasks as $key => $task) {
                        if (count($progress) > 0) {
                            foreach ($progress as $singleProgress){
                                if($task->id  == $singleProgress->getTaskId() && $singleProgress->getFirstSuccess() != null){
                                    $completed_tasks++;
                                    break;
                                }
                            }
                        }
                    }
                    $tasks_count += count($tasks);
                }
                if (count($progress) > 0) {
                    $result['completed_tasks'] = $completed_tasks;
                    $result['never_played_tasks'] = ($tasks_count - $completed_tasks);
                    $result['progress'] = round($child->getJourneyProgressPercentage($journey)) . "%";
                    $result['total_score'] = $child->getJourneyScore($journey);
                } else {
                    $result['completed_tasks'] = 0;
                    $result['never_played_tasks'] = $tasks_count;
                    $result['progress'] = "0%";
                    $result['total_score'] = 0;
                }
                return $result;
            } else {
                throw new BadRequestException(trans('locale.journey_not_exist'));
            }
        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getChildFeedsActivity($parent_id, $activity_id, $child_id){
        $exist = $this->checkParentChild($parent_id, $child_id);
        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($exist) {
            $result = array();
            $counter = 0;
            $lastSolvedTask = $this->journeyRepository->getLastSolvedTaskinActivity($child_id,$activity_id,true);
            if($lastSolvedTask != null){
                $result[$counter]['flag'] = 0;
                $result[$counter]['task_type'] = $lastSolvedTask->getTask()->getTaskType();
                $mission = $lastSolvedTask->getTask()->getMissions()->first();
                if($mission != null) {
                    $result[$counter]['name'] = $mission->getName();
                }
                else {
                    $mission = $lastSolvedTask->getTask()->getMissionsHtml()->first();
                    if($mission != null) {
                        $result[$counter]['name'] = $mission->getTitle();
                    }
                    else {
                        $quiz = $lastSolvedTask->getTask()->getQuizzes()->first();
                        $result[$counter]['name'] = $quiz->getTitle();
                    }
                }
                $result[$counter]['date'] = \Carbon\Carbon::parse($this->journeyRepository->getActivityProgressForTask($child_id,$lastSolvedTask->getTask()->getId(),true))->format('Y-m-d');
                $counter++;
            }
            $played_task = $this->journeyRepository->getLastPlayedProgressActivity($child_id,$activity_id,true);
            if($played_task != null){
                $result[$counter]['flag'] = 1;
                $result[$counter]['date'] = \Carbon\Carbon::parse($played_task->updated_at)->format('Y-m-d');
                $counter++;
            }
            $stuckProgress = $this->journeyRepository->getStuckProgressActivity($child_id,$activity_id,true);
            if($stuckProgress != null && count($stuckProgress) > 0){
                foreach($stuckProgress as $stuckProgressOne){
                    $result[$counter]['flag'] = 2;
                    $result[$counter]['activityName'] = $activity->getName();
                    $result[$counter]['task_type'] = $stuckProgressOne->getTask()->getTaskType();
                    $result[$counter]['date'] = \Carbon\Carbon::parse($stuckProgressOne->updated_at)->format('Y-m-d');
                    $mission = $stuckProgressOne->getTask()->getMissions()->first();
                    if($mission != null) {
                        $result[$counter]['name'] = $mission->getName();
                    }
                    else {
                        $mission = $stuckProgressOne->getTask()->getMissionsHtml()->first();
                        if($mission != null) {
                            $result[$counter]['name'] = $mission->getTitle();
                        }
                        else {
                            $quiz = $stuckProgressOne->getTask()->getQuizzes()->first();
                            $result[$counter]['name'] = $quiz->getTitle();
                        }
                    }
                    $counter++;
                }
            }
            return $result;
        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getChildFeeds($parent_id, $journey_id, $child_id){
        $exist = $this->checkParentChild($parent_id, $child_id);
        if($exist){
            $result = [];
            $child = $this->accountRepository->getUserById($child_id);
            $journey = $this->journeyRepository->getJourneyById($journey_id);
            $quizzes = $journey->getQuizzesIds();
            $tutorials = $journey->getTutorialsIds();
            $solved_task = $this->journeyRepository->getLastSolvedTask($journey_id, $child_id);
            if($solved_task != null){
                $result[0]['flag'] = 0;
                $result[0]['adventure'] = $solved_task->getAdventure()->getOrder();
                $result[0]['task_type'] = $solved_task->getTask()->getTaskType();
                if($solved_task->getTask()->getTaskType() == 'mission'){
                    $result[0]['task'] = ($solved_task->getTask()->getOrder() - 1);
                }
                $result[0]['date'] = \Carbon\Carbon::parse(($solved_task->updated_at))->format('Y-m-d');
            }
            $played_task = $this->journeyRepository->getLastPlayedTask($journey_id, $child_id);
            if($played_task != null){
                $result[1]['flag'] = 1;
                $result[1]['date'] = \Carbon\Carbon::parse(($played_task->updated_at))->format('Y-m-d');
            }
            $progress = $child->getTotalProgress();
            if($progress != null && count($progress) > 0){
                $count = 2;
                foreach ($progress as $score){
                    if($score->getTask()->getTaskType() == 'quiz'){
                        if(in_array($score->getTaskId(), $quizzes)){
                            $quiz_weight = $score->getTask()->getQuizzes()->first()->getQuestionsWeight();
                            if(($quiz_weight['weight'] - $score->score) > 0){
                                $total_count = $quiz_weight['weight'] / $quiz_weight['q_weight'];
                                $result[$count]['flag'] = 2;
                                $result[$count]['adventure'] = $score->getAdventure()->getOrder();
                                $result[$count]['total_questions'] = $total_count;
                                $result[$count]['answer'] = $total_count - (($quiz_weight['weight'] - $score->score) / $quiz_weight['q_weight']);
                                $result[$count]['date'] = \Carbon\Carbon::parse(($score->updated_at))->format('Y-m-d');
                                $count++;
                            }
                        }
                    } elseif($score->getTask()->getTaskType() == 'tutorial'){
                        if(in_array($score->getTaskId(), $tutorials)){
                            $quiz_weight = $score->getTask()->getQuizzes()->first()->getQuestionsWeight();
                            if(($quiz_weight['weight'] - $score->score) > 0){
                                $total_count = $quiz_weight['weight'] / $quiz_weight['q_weight'];
                                $result[$count]['flag'] = 3;
                                $result[$count]['adventure'] = $score->getAdventure()->getOrder();
                                $result[$count]['total_questions'] = $total_count;
                                $result[$count]['answer'] = $total_count - (($quiz_weight['weight'] - $score->score) / $quiz_weight['q_weight']);
                                $result[$count]['date'] = \Carbon\Carbon::parse(($score->updated_at))->format('Y-m-d');
                                $count++;
                            }
                        }
                    }
                }
            }
            return $result;
        }
        throw new BadRequestException(trans('locale.no_parent_child'));
    }

    public function getMaxChildren($user_id){
        // for invited family
        $max_children = 0;
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $subscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        if(count($subscriptions) == 0)
            return ['max_children' => count($account->getChildren())];

        foreach ($subscriptions as $subscription){
            $invitation = $subscription->getInvitationSubscription()->first();
            if($invitation != null){
                $family_invitation = $invitation->getInvitation()->getFamilyInfo();
                if($family_invitation){
                    $max_children = $family_invitation->getMaxChildren();
                    return ['max_children' => $max_children];
                }
            }
        }
        return ['max_children' => $max_children];
    }

    public function getCountriesUsersCount(){
        $response = $this->accountRepository->getCountriesUsersCount();
        return $response;
    }

    public function unlockModelAnswer($user_id,$mission_id){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if ($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user_id,$mission_id);

        if($userModelAnswerUnlocked != null)
            throw new BadRequestException(trans('locale.model_answer_already_unlocked'));

        if($user->getCoins() < 10)
            throw new BadRequestException(trans('locale.not_enough_coins'));

        $this->accountRepository->removeUserScore($user);

        $newUserScore = new UserScore($user,$user->getExperience(),$user->getCoins() - 10);
        $this->accountRepository->addUserScore($newUserScore);
        $newUserModelAnswerUnlocked = new ModelAnswerUnlocked($mission,$user);
        $this->accountRepository->storeUserModelAnswerUnlocked($newUserModelAnswerUnlocked);
        $this->accountRepository->commitDatabaseTransaction();
        if($mission->getType() != "mcq"){
            return ['model_answer'=>$mission->showModelAnswer()];
        }
        else{
            $choice_ids = array();
            $missionQuestions = $mission->getMissionMcq()->getQuestions();
            foreach ($missionQuestions as $missionQuestion){
                $choices = $missionQuestion->getChoices();
                foreach($choices as $choice){
                    if($choice->isCorrect())
                        array_push($choice_ids,$choice->getId());
                }

            }
            return ['model_answer'=>$choice_ids];
        }

    }
    //endregion

    // new payment + new registration
    public function AddUser(UserDto $userDto,$checkEmail = true){
        $this->accountRepository->beginDatabaseTransaction();
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        if(isset($userDto->email) && $userDto->email != null && $checkEmail && $userDto->email != "dummymobile@robogarden.ca") {
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if ($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));
        }

        $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);
        if($userDto->social_id != null) {
            if(isset($userDto->email) && $userDto->email != null){
                $status = $this->accountRepository->getStatusByName('active');
            } else {
                $status = $this->accountRepository->getStatusByName('wait_activation');
            }
        }
        else {
            $status = $this->accountRepository->getStatusByName('wait_activation');
        }
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));

        $account = new Account($userDto, $account_type, $status);
        //$account is added for that user

        //Creating a new user for this account
        $user = new User($userDto, $account);

        //Creating a new role for this user
        $role = $this->accountRepository->getRoleByName($userDto->role);
        //$user_role = new UserRole($role);
        $user->addRole($role);
        //$user has a user role now

        //Store Account, User and User Role in database
        $this->accountRepository->AddUser($user);

        // add 60 coin for people who registered with social and has mail
        if($userDto->social_id != null && isset($userDto->email) && $userDto->email != null && $userDto->role != 'parent') {
            $userScore = $this->accountRepository->getUserScore($user->getId());
            if($userScore != null){
                $newCoins = $userScore->getCoins() + 60;
                $userScore->setCoins($newCoins);
            }
            else{
                $userScore = new UserScore($user, 0, 60);
            }
            $this->accountRepository->storeUserScore($userScore);
        }

        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = null;

        $plan = $this->accountRepository->getPlanByName($userDto->plan);
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        $subscription = new Subscription($subscriptionDto, $updated_plan, $account);
        $this->accountRepository->storeSubscription($subscription);

        //Firing event after creating the user (Sending Confirmation Mail)
        if ($userDto->social_id == null) {
            $event = new UserCreated($account, $user);
            $this->usercreatedhandle->handle($event);
            //Now the user has to activate his account
        }

        // check to unlock activities
        if ($userDto->stripe_token != null && $userDto->activity_id != null) {
            $this->purchaseActivity($user->getId(), $userDto->activity_id, $userDto->stripe_token);
        }

        $this->accountRepository->commitDatabaseTransaction();
        $user =  UserDtoMapper::CustomerMapper($user);

        if(isset($userDto->is_cookie)){
            $url = config('app.frontend_url_without_index');
            $urlAsp = $url . 'register';
            $fields = ['_token' => csrf_token(), 'username' => $userDto->username, 'password' => $userDto->password];
            $response = HttpMethods::post($urlAsp, $fields);
            $status_code = $response['info']['http_code'];
            if($status_code != 200)
                $this->accountRepository->rollBackDatabaseTransaction();

            $body =  $response['body'];
            $data = json_decode($body , true);
            $user->cookie = $data["cookie"];
            $user->full_cookie = $data["cookie_user_full"];
        }

        return $user;
    }

    public function getCurrentExtras($user_id){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $extras_ids = [];
        $extrasObjects = [];
        $counter = 0;
        foreach($subscriptions as $subscription){
            $unlockables = $subscription->getPlan()->getPlanUnlockables();
            foreach ($unlockables as $unlockable){
                $extraUnlockable = $unlockable->getUnlockable()->getExtraUnlockable();
                if($extraUnlockable != null){
                    $extra = $extraUnlockable->getExtra();
                    if(!in_array($extra->getId(),$extras_ids)){
                        array_push($extras,$extra->getId());
                        array_push($extrasObjects,Mapper::MapEntity(ExtraDto::class,$extra));
                        $extrasObjects[$counter]["locked"] = false;
                        if($subscription->getPlan()->getName() == "Free"){
                            $extrasObjects[$counter]["removeable"] = false;
                        }
                        else
                            $extrasObjects[$counter]["removeable"] = true;
                        $counter++;
                    }
                }
            }
        }
        $lockedExtras = $this->accountRepository->getExtrasNotInIds($extras_ids);
        foreach ($lockedExtras as $lockedExtra){
            array_push($extrasObjects,Mapper::MapEntity(ExtraDto::class,$lockedExtra));
            $extrasObjects[$counter]["removeable"] = true;
            $extrasObjects[$counter]["locked"] = true;
            $counter++;
        }
        return $extrasObjects;
    }

    public function checkIfHaveCreditCard($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        if($account->getStripeId() == null)
            return false;
        else
            return true;
    }

    public function getNumberOfChildren($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != "Family"){
            throw new BadRequestException(trans("locale.account_type_must_family"));
        }

        $noOfChildren = 1;
        $planPrice = null;
        $free = true;
        $paidSub = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())->first();
        $price = 0;
        if($paidSub != null){
            $invitationSubscription = $paidSub->getInvitationSubscription()->first();
            if($invitationSubscription != null){
                $free = false;
                $familyInvitation = $invitationSubscription->getInvitation()->getFamilyInfo();
                $noOfChildren = $familyInvitation->getMaxChildren();
            }
            else{
                $free = false;
                $childrenItems = $this->accountRepository->getChildItem($paidSub);
                $noOfChildren += $childrenItems ? $childrenItems->getQuantity() : 0;
                $parentPlanName = $paidSub->getPlan()->getName();
                $childPlanName = $parentPlanName."_child";
                $childPlan = $this->accountRepository->getPlanByName($childPlanName);
                $updated_plan = $this->accountRepository->getlastupdated($childPlan, $account->getAccountType()->getId());
                $price = $updated_plan->getCreditPerMonth();
            }
        }
        return ["noOfChildren" => $noOfChildren, "planPrice" => $price, "free" => $free];
    }

    public function updateUserEmail($user_id, $email){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $userDto = new UserDto();
        $userDto->email = $email;
        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if ($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));

        $user->setEmail($email);
        $this->accountRepository->storeUser($user);

        $event = new UserCreated($account, $user);
        $this->usercreatedhandle->handle($event);

        return trans('locale.email_updated');
    }

    public function checkStripeSubscription($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $use_stripe = $account->isStripeAccount();

        if(count($account->getAccountUnlockables()) > 0)
            $use_stripe = true;
        
        return ['use_stripe' => $use_stripe];
    }

    public function getChildrenCount($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $children = count($account->getChildren()) - 1;
        if($children < 0)
            $children = 0;

        return ['children_count' => $children];
    }

    public function loginRoboGardenFacebook(UserDto $userDto){
        $alreadyRegistered = $this->accountRepository->checkSocialUserRegistered($userDto->social_id, $userDto->social_track);
        if (!$alreadyRegistered) {
            // if ($userDto->email != null) {
            //     if (!$this->IsEmailAvailable($userDto)) {
            //         throw new BadRequestException(trans('locale.email_exists'));
            //     }
            // } else {
            //     $userDto->email = null;
            // }
            $userDto->username = $userDto->social_id;
            $userDto->password = $userDto->social_track;
            $userDto->accounttype = 'Individual';
            $userDto->plan = 'free_learner';
            $userDto->role = 'user';
            $this->AddUser($userDto,false);
        }

        $userLoggedDto = new UserLoginDto();
        $userLoggedDto->username = $userDto->social_id;
        $userLoggedDto->password = $userDto->social_track;
        return $this->Login($userLoggedDto);
    }
    
    public function getUserDefaultActivities($user_id, $is_mobile = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        $unlockedActivityIds = [];
        $unlockedActivities = [];

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        foreach ($subscriptions as $subscription){
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription != null){
                $invitation = $invitationSubscription->getInvitation();
                foreach ($invitation->getDefaultActivities() as $invitation_default_activity) {
                    if(!in_array($invitation_default_activity->getDefaultActivity()->getId(),$unlockedActivityIds)){
                        array_push($unlockedActivityIds,$invitation_default_activity->getDefaultActivity()->getId());
                        array_push($unlockedActivities,$invitation_default_activity->getDefaultActivity());
                    }
                }
                $plans = $invitation->getPlans();
                foreach ($plans as $plan) {
                    $unlockables = $plan->getPlanUnlockables();
                    foreach ($unlockables as $unlockable){
                        $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                        if ($activityUnlockable != null) {
                            $defaultActivity = $activityUnlockable->getDefaultActivity();
                            if ($defaultActivity != null) {
                                if (!in_array($defaultActivity->getId(), $unlockedActivityIds)) {
                                    array_push($unlockedActivityIds, $defaultActivity->getId());
                                    array_push($unlockedActivities, $defaultActivity);
                                }
                            }
                        }
                    }
                }
            }
            else{
                $plan = $subscription->getPlan();
                $unlockables = $plan->getPlanUnlockables();
                foreach ($unlockables as $unlockable){
                    if($is_mobile == $unlockable->getUnlockable()->isMobile()) {
                        $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                        if ($activityUnlockable != null) {
                            $defaultActivity = $activityUnlockable->getDefaultActivity();
                            if ($defaultActivity != null) {
                                if (!in_array($defaultActivity->getId(), $unlockedActivityIds)) {
                                    array_push($unlockedActivityIds, $defaultActivity->getId());
                                    array_push($unlockedActivities, $defaultActivity);
                                }
                            }
                        }
                    }
                }
            }
        }

        $unlockables = $account->getAccountUnlockables();
        foreach ($unlockables as $unlockable){
            if($is_mobile == $unlockable->getUnlockable()->isMobile()) {
                $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                if($activityUnlockable != null) {
                    $defaultActivity = $activityUnlockable->getDefaultActivity();
                    if ($defaultActivity != null) {
                        if (!in_array($defaultActivity->getId(), $unlockedActivityIds)) {
                            array_push($unlockedActivityIds, $defaultActivity->getId());
                            array_push($unlockedActivities, $defaultActivity);
                        }
                    }
                }
            }
        }
        return $unlockedActivities;
    }

    public function purchaseActivity($user_id,$activity_id,$stripe_token = null, $is_mobile = true){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2C())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.no_user_id'));

        if($stripe_token != null){
            if($account->getStripeId() != null){
                $newAccount = $this->updateCreditCardToken($account, $stripe_token);
                $this->accountRepository->storeAccount($newAccount);
            }
            else {
                $newAccount = $this->addCreditCardToken($account, $stripe_token);
                $this->accountRepository->storeAccount($newAccount);
            }
        }

        if($account->getStripeId() == null)
            throw new BadRequestException(trans("locale.payment_info_required"));

        $userDefaultActivities = $this->getUserDefaultActivities($user_id, $is_mobile);
        foreach ($userDefaultActivities as $userDefaultActivity){
            if($userDefaultActivity->getId() == $activity_id)
                throw new BadRequestException(trans('locale.already_have_access_to_activity'));
        }

        $activityPrice = $this->journeyRepository->getActivityPrice($account->getAccountType()->getId(),$activity_id,true);
        if($activityPrice == null)
            throw new BadRequestException(trans('locale.activity_price_not_exist'));

        $amount = $activityPrice->getPrice() - ($activityPrice->getPrice()*$activityPrice->getDiscount());

        $charge = $this->accountRepository->charge($amount,$account->getStripeId(),"Lifetime access for ".$activity->getEnglishName());

        // access for mobile
        $activityUnlockable = $this->journeyRepository->getDefaultActivityUnlockable($activity->getId());
        if($activityUnlockable == null)
            throw new BadRequestException(trans('locale.unlockable_doesn\'t_exist'));

        $unlockable = $activityUnlockable->getUnlockable();
        if($unlockable == null)
            throw new BadRequestException(trans('locale.unlockable_doesn\'t_exist'));

        $paidUnlockableMethod = $this->accountRepository->getUnlockableMethodByName('charge');

        $accountUnlockable = new AccountUnlockable($account, $unlockable, $paidUnlockableMethod, $charge->id);
        $this->accountRepository->storeAccountUnlockable($accountUnlockable);

        // access for web
        $activityUnlockable = $this->journeyRepository->getDefaultActivityUnlockable($activity->getId(), false);
        if($activityUnlockable == null)
            throw new BadRequestException(trans('locale.unlockable_doesn\'t_exist'));

        $unlockable = $activityUnlockable->getUnlockable();
        if($unlockable == null)
            throw new BadRequestException(trans('locale.unlockable_doesn\'t_exist'));

        $accountUnlockable = new AccountUnlockable($account, $unlockable, $paidUnlockableMethod, $charge->id);
        $this->accountRepository->storeAccountUnlockable($accountUnlockable);

        $this->accountRepository->commitDatabaseTransaction();
        return $charge;
    }

    public function checkUserActivation($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $status = $user->getAccount()->getStatus();
        if($status == 'active')
            return true;

        return false;
    }

    public function getUserData($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $this->accountRepository->beginDatabaseTransaction();
        $response = new ResponseObject();
        $response->user = $this->accountRepository->GetUser($user->getUsername());
        $response->RoboPalAuth = $this->authenticateRobopal($user->getId());
        $response->status = $user->getAccount()->getStatus();
        $response->country = $user->getAccount()->getCountry();
        $account = $user->getAccount();
        $response->use_stripe = $account->isStripeAccount();
        $response->account_type = $user->getAccount()->getAccountType()->getName();
        $response->invited = false;
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $response->trialEnd = '1970-01-01 00:00:00';
        if (count($subscriptions) == 0) {
            $users = $account->getUsers();
            $child = false;
            foreach ($user->getRoles() as $role) {
                if ($role->getName() == 'child')
                    $child = true;
            }
            if($user->getSupervisorEmail() != null && $child && count($users) == 1)
            {
                $status = $this->accountRepository->getStatusByName('suspended');
                if($status == null)
                    throw new BadRequestException(trans('locale.status_not_exist'));

                $account->setStatus($status);
                $this->accountRepository->storeAccount($account);
                throw new UnauthorizedException(trans('locale.trial_period_end'));
            }
        } else {
            $subscription = $subscriptions->last();
            $stripe_subscription = $subscription->getStripeSubscriptions()->first();
            if ($stripe_subscription != null)
                $response->trialEnd = $this->accountRepository->getTrialEnd($stripe_subscription);

            $invitation_subscription = $subscription->getInvitationSubscription()->first();
            if($invitation_subscription != null)
                $response->invited = true;
        }

        $lastSubscription = $this->accountRepository->getLastSubscriptionById($account->getId());
        $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
        if($invitationSubscription != null)
            $response->invited = true;

        $response->status_code = 200;
        $new_response = new ResponseObject();
        $new_response->Object = $response;
        $new_response->status_code = $response->status_code;
        $new_response->error_source = $response->error_source;
        $new_response->errorMessage = $response->errorMessage;
        $this->accountRepository->commitDatabaseTransaction();
        return $response;
    }

    public function saveGuestRegisteredScore($user_id, $coins, $xp){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $score = $user->getUserScore();
        if($score != null){
            $score->setCoins($coins);
            $score->setExperience($xp);
        } else {
            $score = new UserScore($user, $xp, $coins);
        }
        $this->accountRepository->storeUserScore($score);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.user_data_updated');
    }

    public function updateMobileUser($user_id, UserDto $userDto){
        $this->accountRepository->beginDatabaseTransaction();

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        if (isset($userDto->email) && $userDto->email != null) {
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if ($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));
        }

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $user->setUsername($userDto->username);
        $user->setEmail($userDto->email);
        $user->setPassword(bcrypt($userDto->password));
        if(isset($userDto->social_id)){
            $user->setSocialId($userDto->social_id);
            $user->setSocialTrack($userDto->social_track);
        }
        $this->accountRepository->storeUser($user);

        $userScore = $this->accountRepository->getUserScore($user->getId());
        if($userScore != null){
            $newCoins = $userScore->getCoins() + 60;
            $userScore->setCoins($newCoins);
        }
        else{
            $userScore = new UserScore($user, 0, 60);
        }
        $this->accountRepository->storeUserScore($userScore);

        $this->accountRepository->commitDatabaseTransaction();
        $user =  UserDtoMapper::CustomerMapper($user);
        return $user;
    }


    public function getUserNotifications($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $notifications = $user->getUserNotifications();

        $userNotifications= Mapper::MapEntityCollection(UserNotificationDto::class, $notifications);

        return $userNotifications;
    }

    public function deleteUserNotification($id)
    {
        $notification = $this->accountRepository->getUserNotificationById($id);
//        if($notification == null)
//            throw new BadRequestException(trans('locale.notification_not_exist')); //TODO translations
        if($notification !== null){
            $this->accountRepository->deleteUserNotification($notification);

            return trans("locale.notification_deleted");  //TODO translations

        }else{
            return trans("locale.notification_deleted_before");

        }

    }


    //    ===============     Superadmin      ===============

    public function addSuperAdminUser(UserDto $userDto, $checkEmail = true){
        $this->accountRepository->beginDatabaseTransaction();
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        if(isset($userDto->email) && $userDto->email != null && $checkEmail && $userDto->email != "dummymobile@robogarden.ca") {
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if ($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));
        }

        $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);

        $account = $this->accountRepository->getAccountByAccountType($account_type);

        //Creating a new user for this account
        $user = new User($userDto, $account, true);

        //Creating a new role for this user
        //dd($userDto->role);
        $role = $this->accountRepository->getRoleByName($userDto->role);
        //$user_role = new UserRole($role);
        $user->addRole($role);
        //$user has a user role now

        //Store Account, User and User Role in database
//        dd($user);
        $this->accountRepository->AddUser($user);

        $this->accountRepository->commitDatabaseTransaction();
        return $user;
    }

    public function markNotificationAsRead($user_id,$notification_id)
    {
        //Check User is exist
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        //Check if User ownes the notifications
        $notification = $this->accountRepository->getUserNotificationById($notification_id);
        if($notification->user_id == $user_id){
            //Mark it as read
            $this->accountRepository->markNotificationAsRead($notification);
            return 'success';
        }else{
            throw new BadRequestException(trans('locale.notification_not_belongto_user'));
        }

    }

}