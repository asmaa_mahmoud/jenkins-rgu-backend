<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class UserPositionDto
{
    public $PositionX;
    public $PositionY;
    public $Index;
}