<?php

namespace App\ApplicationLayer\Accounts\Dtos;


class UserRequestDto
{
    public $id;
    public $fname;
    public $lname;
    public $email;
    public $image_link;
    public $image;
    public $password;
    public $gender;
    public $age;
    public $username;
    public $grade;
    public $role;
    public $camps;
    public $camp_id;

}