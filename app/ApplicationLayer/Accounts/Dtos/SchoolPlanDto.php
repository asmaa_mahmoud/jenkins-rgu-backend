<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class SchoolPlanDto
{
    public $MaxNumberOfStudents;
    public $MaxNumberOfCourses;
}