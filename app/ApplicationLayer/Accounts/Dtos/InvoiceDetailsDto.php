<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class InvoiceDetailsDto
{
    public $id;
    public $amount_due;
    public $charge;
    public $customer;
    public $date;
    public $description;
    public $starting_balance;
    public $ending_balance;
    public $invoice_lines = [];
    public $subtotal;
    public $tax;
    public $tax_percent;
    public $total;
    public $paid;
    public $currency;
    public $plan;
}