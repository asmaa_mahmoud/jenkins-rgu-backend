<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class UserNotificationDto
{
    public $id;
    public $type;
    public $user_id;
    public $data;
    public $is_read;
    public $read_at;
    public $created_at;
    public $updated_at;
    
}