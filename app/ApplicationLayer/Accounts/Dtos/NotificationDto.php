<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class NotificationDto
{
    public $id;
    public $Title;
    public $Message;
    public $StartDate;
    public $EndDate;
    public $Type;
}