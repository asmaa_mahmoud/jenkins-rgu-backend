<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class GuestDto
{
    public $ip_address;
    public $coins;
    public $xp;
    public $name;

}