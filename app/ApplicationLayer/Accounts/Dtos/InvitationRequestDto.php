<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class InvitationRequestDto
{
    public $invitation_name;
    public $invitation_code;
    public $account_type;
    public $end_date;
    public $number_of_users;
}