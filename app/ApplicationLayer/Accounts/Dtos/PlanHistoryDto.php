<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class PlanHistoryDto
{
    public $Name;
    public $CreditPerMonth;
    public $CreditPerYear;
    public $CreditPerQuarter;
    public $MaximumNoJourneys;
}