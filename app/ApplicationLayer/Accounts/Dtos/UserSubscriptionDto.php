<?php

namespace App\ApplicationLayer\Accounts\Dtos;


class UserSubscriptionDto
{
		public $stripe_token;
        public $plan;
        public $period;
        public $user;
        public $accounttype_id;
        public $no_of_children;
}