<?php

namespace App\ApplicationLayer\Accounts\Dtos;

//use App\DomainModelLayer\AccountTypes\AccountType;

class UserDto
{
	public $id;
    public $fname;
    public $lname;
    public $email;
    public $supervisorEmail;
    public $image_link;
    public $gender;
    public $age;
    public $username;
    public $password;
    public $languageCode;
    public $social_id;
    public $social_track;
    public $score;
    public $accounttype;
    public $account;
    public $stripe_token;
    public $plan;

    public $role;
    public $period;
    public $country = null;

    public $extra_plans = [];
    public $activity_id;
    public $is_cookie;
    public $first_login_time;

}