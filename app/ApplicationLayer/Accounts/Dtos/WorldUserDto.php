<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class WorldUserDto
{
    public $name;
    public $image;
    public $address;
    public $phone;
    public $predictor;
}