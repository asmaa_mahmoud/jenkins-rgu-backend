<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 12/26/17
 * Time: 7:46 PM
 */

namespace App\ApplicationLayer\Accounts\Dtos;

class ExtraDto
{
    public $name;
    public $description;
    public $xpToUnlock;
    public $coinsToUnlock;
    public $costToUnlock;
}