<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 18-Mar-19
 * Time: 1:44 PM
 */

namespace App\ApplicationLayer\Accounts\Dtos;


class SuperAdminUserDto
{
    public $id;
    public $fname;
    public $lname;
    public $email;
    public $image_link;
//    public $gender;
    public $username;
    public $password;
    public $accounttype;
    public $account;
    public $role;
    public $is_admin;

    public $plan;
    public $age = null;
}