<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class CustomerInfoDto
{
    public $id;
    public $accountBalance;
    public $currency;
    public $description;
    public $cardBrand;
    public $country;
    public $expMonth;
    public $expYear;
    public $lastFour;
}