<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class InvoiceLineDto
{
    public $id;
    public $amount;
    public $currency;
    public $description;
    public $period_start;
    public $period_end;
    public $plan_id;
    public $plan_amount;
    public $plan_name;
    public $subscription;
    public $subscription_item;
    public $type;
}