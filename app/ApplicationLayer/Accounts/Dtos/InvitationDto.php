<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class InvitationDto
{
	public $Id;
    public $Code;
    public $Name;
    public $AccountType;
    public $EndDate;
    public $NumberOfUsers;
}