<?php

namespace App\ApplicationLayer\Accounts\Dtos;


class UserPlansDto
{
    public $Id;
    public $Name;
    public $CreditPerMonth;
    public $CreditPerYear;
    public $CreditPerQuarter;
    public $MaximumNoJourneys;
    public $SchoolInfo;

}