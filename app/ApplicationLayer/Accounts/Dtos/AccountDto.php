<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class AccountDto
{
    public $id;
    public $accounttype_id;
    public $Name;
    public $lname;
    public $fname;
    public $email;
    public $active;
}