<?php
/**
 * Created by PhpStorm.
 * User: hme1000000
 * Date: 8/3/2017
 * Time: 3:52 PM
 */

namespace App\ApplicationLayer\Accounts\Dtos;


class LanguageDto
{
    public $id;
    public $Name;
    public $Code;
}