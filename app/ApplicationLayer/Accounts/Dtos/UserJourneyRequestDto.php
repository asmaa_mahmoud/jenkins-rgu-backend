<?php

namespace App\ApplicationLayer\Accounts\Dtos;


class UserJourneyRequestDto
{
	public $user;
    public $journeys;
}