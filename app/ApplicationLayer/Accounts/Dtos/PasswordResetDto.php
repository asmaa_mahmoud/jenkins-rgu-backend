<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class PasswordResetDto
{
    public $id;
    public $email;
    public $token;
}