<?php
/**
 * Created by PhpStorm.
 * User: essam
 * Date: 12/2/2018
 * Time: 5:19 PM
 */

namespace App\ApplicationLayer\Accounts\Dtos;


class PushNotificationDto
{
    public $endpoint;
    public $public_key;
    public $auth_token;
}