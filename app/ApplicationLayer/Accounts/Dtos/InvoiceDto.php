<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class InvoiceDto
{
    public $id;
    public $date;
    public $amount;
    public $number_of_items;
    public $next_payment;
    public $tax;
    public $plan_name;
}