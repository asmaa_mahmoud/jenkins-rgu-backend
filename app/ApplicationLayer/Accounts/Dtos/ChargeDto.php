<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 4/2/18
 * Time: 2:27 PM
 */

namespace App\ApplicationLayer\Accounts\Dtos;


class ChargeDto
{
    public $id;
    public $date;
    public $amount;
    public $customer_id;
    public $currency;
}