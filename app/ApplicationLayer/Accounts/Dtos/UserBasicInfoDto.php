<?php

namespace App\ApplicationLayer\Accounts\Dtos;


class UserBasicInfoDto
{
	public $id;	
	public $fname;
    public $lname;
    public $email;
    public $username;
    public $image;
    public $socialId;
    public $accountType;
    public $accountTypeId;
    public $basicClassRoom;
    public $planType;
    public $stripe_token_exists;
}