<?php

namespace App\ApplicationLayer\Accounts\Dtos;

class SubscriptionDto
{
	public $id;
    public $StartDate;
    public $EndDate;
    public $StripeId;
    public $Period;
    public $PlanName;
    public $Active = false;
    public $Cancelled = false;
    public $stripeResult;
}