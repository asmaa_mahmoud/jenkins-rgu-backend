<?php

namespace App\ApplicationLayer\Schools;

use App\ApplicationLayer\Schools\Dtos\ActivityMinifiedDto;
use App\ApplicationLayer\Schools\Dtos\CampNamesDto;
use App\ApplicationLayer\Schools\Dtos\CourseAdventureDeadlinesDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\ApplicationLayer\Schools\Dtos\StudentPositionDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\CampActivityProgress;
use App\DomainModelLayer\Schools\Course;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Schools\StudentFinishCourse;
use App\DomainModelLayer\Schools\StudentPosition;
use App\Framework\Exceptions\CustomException;
use App\Helpers\Mapper;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\School;
use Illuminate\Pagination\Paginator;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Journeys\Dtos\ChoiceDto;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\DomainModelLayer\Schools\StudentProgress;
use Carbon\Carbon;
use App\ApplicationLayer\Schools\Dtos\CourseDto;
use App\ApplicationLayer\Schools\Dtos\AssignmentDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\DomainModelLayer\Schools\StudentClickMission;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Accounts\UserModelAnswer;

class StudentService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;

    }
    //endregion

    //region Functions    

    public function countStudents($user_id,$grade_name,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllStudents',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException("there is no grade with this name for this school");
            //dd($grade->getSchool()->getId() .'.....'. $school->getId());
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException("this grade is not in the school");
            $grade_id = $grade->getId();
        }


        $numberOfStudents = $this->schoolRepository->getAllStudents($account->getId(),null,$grade_id,$search,true);

        return ["count"=>$numberOfStudents];
    }

    public function getAllStudents($user_id,$start_from,$limit,$grade_name,$search)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllStudents',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException(trans('locale.grade_name_not_in_school'));
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans('locale.grade_not_in_school'));
            $grade_id = $grade->getId();
        }

        $students = $this->schoolRepository->getAllStudents($account->getId(),$limit,$grade_id,$search);
        return Mapper::MapEntityCollection(StudentDto::class,$students,[ClassroomDto::class,GradeDto::class]);
    }

    public function getStudentById($user_id,$id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllStudents',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $student = $this->accountRepository->getUserById($id);
        if($student == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        if($student->getAccount()->getId() != $user->getAccount()->getId()){
            throw new BadRequestException(trans("locale.student_school_mismatch"));
        }

        $studentRoles = $student->getRoles();
        $isStudent = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudent = true;
        }

        if(!$isStudent)
            throw new BadRequestException(trans("locale.student_wrong_role"));

        return Mapper::MapClass(StudentDto::class,$student,[ClassroomDto::class]);
    }

    public function updateStudent($user_id,StudentDto $studentDto)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $loggedInUser = $this->accountRepository->getUserById($user_id);
        if($loggedInUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('update_Student',$loggedInUser))
            throw new UnauthorizedException(trans("locale.no_permission_operation"));

        $school = $loggedInUser->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $student = $this->accountRepository->getUserById($studentDto->id);
        if($student == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        if($student->getUsername() != $studentDto->username){
            $existingUsername = $this->schoolRepository->UserNameAlreadyExist($studentDto->username);
            if($existingUsername)
                throw new BadRequestException(trans('locale.username_exists'));

        }



        if($student->getAccount()->getId() != $loggedInUser->getAccount()->getId()){
            throw new BadRequestException(trans("locale.student_school_mismatch"));
        }

        $studentRoles = $loggedInUser->getRoles();
        $isStudentLoggedIn = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudentLoggedIn = true;
        }

        $studentRoles = $student->getRoles();
        $isStudent = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudent = true;
        }

        if(!$isStudent)
            throw new BadRequestException(trans("locale.user_not_student"));


        if($isStudentLoggedIn && $isStudent && ($user_id != $studentDto->id))
            throw  new UnauthorizedException(trans("locale.cant_update_another_student_data"));

        $student->setFirstName($studentDto->fname);
        $student->setLastName($studentDto->lname);
        $student->setUsername($studentDto->username);


        if($studentDto->password != null)
            $student->setpassword(bcrypt($studentDto->password));

        $role = $this->accountRepository->getRoleByName('student');
        if($role == null)
            throw  new BadRequestException(trans("locale.role_not_found"));
        //  dd($studentDto->grade);
        if($studentDto->grade != null){

            $grade = $this->schoolRepository->getGradeByName($school,$studentDto->grade);
            if($grade == null)
                throw new BadRequestException(trans('locale.grade_name_not_in_school'));
            $studentGrade = $student->getGrade();
            if($studentGrade->getId() != $grade->getId()){
                $studentClassRooms = $student->getAllGroupMembers();
                if((count($studentClassRooms)>0) && $studentDto->classroom == null && ($studentDto->force == null || $studentDto->force == false))
                    throw new CustomException(trans("locale.student_is_assigned_classroom_unassign_first"),443);
                $student->removeGrade($studentGrade);
                $student->addGrade($grade);
                if($studentDto->classroom == null)
                {
                    $old_classroom = $student->getBasicClassRoom();

                    if($old_classroom != null)
                    {
                        $classroom_default_group_member = $this->schoolRepository->getDefaultGroupMember($old_classroom,$student,$role);
                        if($classroom_default_group_member == null)
                            throw new BadRequestException(trans("locale.no_default_group_member_with_specs"));
                        $this->schoolRepository->deleteGroupMember($classroom_default_group_member);
                    }
                }
            }
        }
        if($studentDto->classroom != null)
        {
            $classroom = $this->schoolRepository->getClassroomById($studentDto->classroom);
            if($classroom == null)
                throw  new BadRequestException(trans("locale.classroom_not_exist"));


            $old_classroom = $student->getBasicClassRoom();

            if($old_classroom != null)
            {
                if($old_classroom->getId() != $classroom->getId()){
                    if($student->getGrade()->getId() != $classroom->getGrade()->getId())
                        throw  new BadRequestException(trans("locale.classroom_grade_doesn't_match_student_grade"));
                    if($classroom->isMember($student->getId()))
                        throw new BadRequestException(trans("locale.user_in_classroom_with_id").$classroom->getId());
                    $classroom_default_group_member = $this->schoolRepository->getDefaultGroupMember($old_classroom,$student,$role);
                    if($classroom_default_group_member == null)
                        throw new BadRequestException(trans("locale.no_default_group_member_with_specs"));
                    $this->schoolRepository->deleteGroupMember($classroom_default_group_member);
                    $defaultGroup = $classroom->getDefaultGroup();
                    $groupMember = new GroupMember($defaultGroup,$student,$role);
                    $student->addGroupMember($groupMember);
                }
            }
            else{

                $defaultGroup = $classroom->getDefaultGroup();
                $groupMember = new GroupMember($defaultGroup,$student,$role);
                $student->addGroupMember($groupMember);
            }
        }
        $this->accountRepository->storeUser($student);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(StudentDto::class,$student,[ClassroomDto::class,GradeDto::class]);
    }

    public function deleteStudents($user_id,$students)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Student',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        foreach ($students as $student){
            $this->deleteStudent($school,$student);
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.students_delete_successfull");
    }

    public function deleteStudent(School $userSchool,$student_id)
    {
        $student = $this->accountRepository->getUserById($student_id);

        if($student == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        $studentSchool = $student->getAccount()->getSchool();
        if($studentSchool == null)
            throw new BadRequestException(trans("locale.student_no_school"));

        if($studentSchool->getId() != $userSchool->getId())
            throw new BadRequestException(trans("locale.student_school_mismatch"));

        $studentRoles = $student->getRoles();
        $isStudent = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudent = true;
        }
        if(!$isStudent)
            throw  new BadRequestException(trans("locale.student_wrong_role"));

        if(count($student->getStudentProgress()) > 0)
            throw  new BadRequestException(trans("locale.student_has_progress"));
        $this->accountRepository->deleteUser($student);
        $this->schoolRepository->deleteGroupMembers($student);

        return trans("locale.student_delete_successfull");
    }

    public function updateMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist"));
        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new BadRequestException(trans("locale.user_not_student"));

        if(!$user->hasCourse($course_id))
            throw new BadRequestException(trans("locale.user_not_have_course"));

        $previous_progress = $this->schoolRepository->getStudentProgrss($task->getId(),$user_id,$course_id,$adventure_id);
        $score = $mission->getWeight();
        if($previous_progress == null)
        {
            if($success_flag){
                //no of trialsScore
                $score += 5;
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        $score +=5;
                    }
                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    $score +=5;
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }

                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $mission->getWeight();
                }

                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,$score,1,$timeTaken,$timeTaken,$noOfBlocks,$noOfBlocks);
                if($task->getId() == $this->journeyRepository->getLastTaskInJourney($course->getJourney())->getId()){
                    $studentFinishCourse = new StudentFinishCourse($user,$course);
                    $this->schoolRepository->storeStudentFinishCourse($studentFinishCourse);
                }
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }

            else
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,0);
        }
        else
        {
            $no_trials = $previous_progress->getNo_of_trails() + 1;

            $first_success = $previous_progress->getFirstSuccess();

            if($success_flag && ($previous_progress->getFirstSuccess() == null)){
                $first_success = $no_trials;
                if($task->getId() == $this->journeyRepository->getLastTaskInJourney($course->getJourney())->getId()){
                    $studentFinishCourse = new StudentFinishCourse($user,$course);
                    $this->schoolRepository->storeStudentFinishCourse($studentFinishCourse);
                }
            }

            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    $score += 5;
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        $score +=5;
                    }
                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    $score +=5;
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }

            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_progress->getScore()){
                $addedScore = $score - $previous_progress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_progress->getScore()){
                $score = $previous_progress->getScore();
            }
            $bestTimeTaken = $previous_progress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_progress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }

            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $success_flag){
                $score = $mission->getWeight();
                if($previous_progress->getScore() > $score){
                    $score = $previous_progress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_progress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_progress->getScore();
                $addedScore = 0;
            }

            $this->schoolRepository->deleteStudentProgress($previous_progress);
            $student_progress = new StudentProgress($task,$user,$course,$adventure,$no_trials,$score,$first_success,$timeTaken,$bestTimeTaken,$noOfBlocks,$bestNoOfBlocks);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
        }
        $this->schoolRepository->storeStudentProgress($student_progress);
        return $this->getTotalScore($user->getId());
    }

    public function updateHTMLMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist"));
        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new BadRequestException(trans("locale.user_not_student"));

        if(!$user->hasCourse($course_id))
            throw new BadRequestException(trans("locale.user_not_have_course"));

        $previous_progress = $this->schoolRepository->getStudentProgrss($task->getId(),$user_id,$course_id,$adventure_id);
        $score = $mission->getWeight();
        if($previous_progress == null)
        {
            if($success_flag){
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,$score,1);
                if($task->getId() == $this->journeyRepository->getLastTaskInJourney($course->getJourney())->getId()){
                    $studentFinishCourse = new StudentFinishCourse($user,$course);
                    $this->schoolRepository->storeStudentFinishCourse($studentFinishCourse);
                }
            }

            else{
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,0);
            }

        }
        else
        {
            $no_trials = $previous_progress->getNo_of_trails() + 1;
            if($previous_progress->getScore() < $score)
                $new_score = $score;
            else
                $new_score = $previous_progress->getScore();
            $first_success = $previous_progress->getFirstSuccess();

            if($success_flag && ($previous_progress->getFirstSuccess() == null)){
                $first_success = $no_trials;

                if($task->getId() == $this->journeyRepository->getLastTaskInJourney($course->getJourney())->getId()){
                    $studentFinishCourse = new StudentFinishCourse($user,$course);
                    $this->schoolRepository->storeStudentFinishCourse($studentFinishCourse);
                }
            }

            if($new_score > $previous_progress->getScore())
            {
                $added_Score = $new_score - $previous_progress->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$added_Score,$user->getCoins()+($added_Score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }
            $this->schoolRepository->deleteStudentProgress($previous_progress);
            $student_progress = new StudentProgress($task,$user,$course,$adventure,$no_trials,$new_score,$first_success);
        }
        $this->schoolRepository->storeStudentProgress($student_progress);
        return $this->getTotalScore($user->getId());
    }

    public function updateQuizProgress($user_id,$quiz_id,$adventure_id,$course_id,$success_flag,$quiz_data)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));
        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));
        $passed = 0;
        $mark_counter = 0;
        $score = 0;

        $questions_answers = [];

        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans("locale.quiz_not_exist"));
        $task = $quiz->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new BadRequestException(trans("locale.user_not_student"));
        if(!$user->hasCourse($course_id))
            throw new BadRequestException(trans("locale.user_not_have_course"));

        foreach ($quiz_data as $question)
        {
            $question_instance = $this->journeyRepository->getQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans("locale.question_not_exist"));
            $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans("locale.choice_not_exist"));
            $choice_correct = Mapper::MapEntityCollection(ChoiceDto::class, $this->journeyRepository->getChoiceModelforQuestion($question['question_id']));
            $question_answer = ["question_id"=>$question['question_id'],
                "question_body"=>$question_instance->getBody(),
                "user_choice"=>$choice_instance->getText(),
                "choice_correct"=>$choice_correct[0]->text,
                "eval"=>false];
            if($choice_correct[0]->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question['weight'];
            }
            $questions_answers[] = $question_answer;
        }
        if($mark_counter >= (sizeof($quiz_data)/2))
            $passed = 1;

        $previous_progress = $this->schoolRepository->getStudentProgrss($task->getId(),$user_id,$course_id,$adventure_id);
        if($previous_progress == null)
        {
            if($passed){
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,$score,1);
                if($task->getId() == $this->journeyRepository->getLastTaskInJourney($course->getJourney())->getId()){
                    $studentFinishCourse = new StudentFinishCourse($user,$course);
                    $this->schoolRepository->storeStudentFinishCourse($studentFinishCourse);
                }
            }

            else
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,$score);
        }
        else
        {
            $no_trials = $previous_progress->getNo_of_trails() + 1;
            if($previous_progress->getScore() < $score)
                $new_score = $score;
            else
                $new_score = $previous_progress->getScore();
            $first_success = $previous_progress->getFirstSuccess();
            if($passed && ($previous_progress->getFirstSuccess() == null)){
                $first_success = $no_trials;
                if($task->getId() == $this->journeyRepository->getLastTaskInJourney($course->getJourney())->getId()){
                    $studentFinishCourse = new StudentFinishCourse($user,$course);
                    $this->schoolRepository->storeStudentFinishCourse($studentFinishCourse);
                }
            }
            if($new_score > $previous_progress->getScore())
            {
                $added_Score = $new_score - $previous_progress->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$added_Score,$user->getCoins()+($added_Score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }

            $this->schoolRepository->deleteStudentProgress($previous_progress);
            $student_progress = new StudentProgress($task,$user,$course,$adventure,$no_trials,$new_score,$first_success);
        }
        $this->schoolRepository->storeStudentProgress($student_progress);

        return ["passed"=>$passed,"questions_answers"=>$questions_answers,"new_score"=>$this->getTotalScore($user->getId())];
    }

    public function getTotalScore($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new BadRequestException(trans("locale.user_not_student"));

        return $user->getStudentScores();
    }

    public function getCourses($user_id,$limit,$start_from,$search)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new UnauthorizedException(trans("locale.user_not_student"));


        $courses_data = Mapper::MapEntityCollection(CourseDto::class,$user->getCourses($search),[JourneyDto::class,ClassroomDto::class,AssignmentDto::class,CourseAdventureDeadlinesDto::class]);
        foreach ($courses_data as $course_data) {
            $course_data->endsAt = Carbon::parse($course_data->endsAt)->toDateString();
            $course_data->startsAt = Carbon::parse($course_data->startsAt)->toDateString();
            $course_data->CourseProgress = $user->getCourseScores($course_data->id);
            $course_data->percentageProgress = ceil(($course_data->CourseProgress['missions_solved']/$course_data->TasksCount)*100);
            if($course_data->TasksCount == $course_data->CourseProgress['missions_solved'])
                $course_data->current_adventure['adventure_title'] = "Done";
            else
                $course_data->current_adventure = $this->getStudentAdventures($user,$course_data->id)["current_adventure"];
        }
        $paginated_courses = array_slice($courses_data,$start_from,$limit);

        return ["count" => count($courses_data), "courses" => $paginated_courses];

    }

    public function getCourseData($user_id,$course_id)
    {


        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));

        $journey = $course->getJourney();

        if($journey->getStatus() != 'published')
            throw new CustomException(trans("locale.journey_not_published"),277);
        $default_journey = $journey;

        $adventures = $this->journeyRepository->getAdventuresByOrder($default_journey->getId());
        $journey_data = [];
        $counter = 0;
        $journey_data['name'] = $journey->getName();
        $journey_data['id'] = $journey->getId();
        $journey_data['categories'] = array();
        foreach ($journey->getCategories() as $category){
            array_push($journey_data['categories'],['name'=>$category->getName()]);
        }


        foreach ($adventures as $key => $adventure) {
            $journey_data['adventures'][$counter]['id'] = $adventure->getId();
            $journey_data['adventures'][$counter]['name'] = $adventure->getTitle();
            $journey_data['adventures'][$counter]['description'] = $adventure->getDescription();
            $journey_data['adventures'][$counter]['order'] = $adventure->getOrder();
            $journey_data['adventures'][$counter]['roadmapImageURL'] = $adventure->getRoadMapImage();

            $tasks  = $this->journeyRepository->getTasksByOrder($default_journey->getId(),$adventure->getId());
            $counter_mission = 0;
            $counter_quiz = 0;
            $counter_task = 0;

            foreach ($tasks as $task) {
                if($task->getMissions()->count() > 0)
                {
                    $mission = $task->getMissions()->first();

                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['id'] = $mission->getId();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['name'] = $mission->getName();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['type'] = $mission->getType();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['blockly_rules'] = $mission->getBlocklyRules();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['order'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['description'] = $mission->getDescription();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['iconURL'] = $mission->getIconUrl();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_x'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_y'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['vpl_id'] = $mission->getVplId();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['scene_name'] = $mission->getSceneName();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['mission_type'] = $mission->getType();

                    if($user->is_teacher() || $user->is_schoolAdmin())
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = 0;
                    else
                    {
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = $this->isLockedTask($task, $course, $adventure, $user_id);
                    }
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['first_mission'] = ($counter == 0 && $counter_mission == 0? true:false);
                    $counter_mission++;
                }
                else if($task->getMissionsHtml()->count()>0){
                    $missionsHtml = $task->getMissionsHtml()->first();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['id'] = $missionsHtml->getId();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['name'] = $missionsHtml->getTitle();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['type'] = 'html';
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['order'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['description'] = $missionsHtml->getDescription();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_x'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_y'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['mission_type'] = 'html';
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['iconURL'] = $missionsHtml->getIconUrl();

                    if($user->is_teacher() || $user->is_schoolAdmin())
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = 0;
                    else
                    {
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = $this->isLockedTask($task, $course, $adventure, $user_id);
                    }
                    $journey_data['adventures'][$counter]['missions'][$counter_mission]['first_mission'] = ($counter == 0 && $counter_mission == 0? true:false);
                    $counter_mission++;
                }
                else
                {
                    if($task->getQuizzes()->count() > 0)
                    {
                        $quiz = $task->getQuizzes()->first();

                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['id'] = $quiz->getId();
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['Title'] = $quiz->getTitle();
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['iconURL'] = $quiz->geticonURL();
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['type'] = $quiz->getType()->getName();
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['position_x'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['position_y'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['order'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();

                        if($user->is_teacher() || $user->is_schoolAdmin())
                            $journey_data['adventures'][$counter]['quizzes'][$counter_mission]['locked'] = 0;
                        else
                        {
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['locked'] = $this->isLockedTask($task, $course, $adventure, $user_id);
                        }
                        $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['last_quiz'] = ($counter == (count($adventures) - 1) && $counter_task == (count($tasks) - 1)? true:false);
                        $counter_quiz++;
                    }
                }
                $counter_task++;
            }

            $counter++;
        }

        return ["journey_data" => $journey_data,"student_position" => $this->getStudentPosition($user_id,$course->getId())];
    }


    public function isLockedTask(Task $task, Course $course, Adventure $adventure, $user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        $journey = $course->getJourney();

        $first_task = $this->journeyRepository->getFirstTaskInJourney($journey->getId());

        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();

        $first_task_in_adventure = $this->journeyRepository->getFirstTaskInAdventure($journey->getId(),$adventure->getId());

        if($task->getId() != $first_task_in_adventure->getId())
        {
            $progress = $this->schoolRepository->getStudentProgressByOrder($user_id, intval($order) - 1,$course->getId(),$adventure->getId());
        }
        else
        {

            $pre_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() - 1,$journey->getId());

            if($pre_adventure == null)
                throw new BadRequestException(trans("locale.pre_adventure_not_exist"));

            $pre_task =  $this->journeyRepository->getLastTaskInAdventure($journey->getId(),$pre_adventure->getId());

            $pre_task_order = $this->journeyRepository->getTaskOrder($pre_task,$journey->getId(),$pre_adventure->getId())->getOrder();

            $progress = $this->schoolRepository->getStudentProgressByOrder($user_id, $pre_task_order,$course->getId(),$pre_adventure->getId());

        }
        return (($progress != null) && ($progress->getFirstSuccess() != null)? false:true);
    }

    public function getStudentPosition($user_id,$course_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));
        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));

        $journey = $course->getJourney();

        if(count($user->getCoursePosition($course_id)) == 0)
        {
            $adventures = $this->journeyRepository->getAdventuresByOrder($journey->getId());

            $adventure = $adventures->first();

            $task = $this->journeyRepository->getFirstTaskInJourney($journey->getId());

            $position_x = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();

            $position_y = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();

            $studentpositiondto = new StudentPositionDto();
            $studentpositiondto->PositionX = str_replace("px", "", $position_x);
            $studentpositiondto->PositionY = str_replace("px", "", $position_y);
            $studentpositiondto->Index = 0;

            $position = new StudentPosition($studentpositiondto,$user,$course,$adventure);

            $this->schoolRepository->storeStudentPosition($position);

            $studentpositiondto =  Mapper::MapClass(StudentPositionDto::class,$position);
        }
        else
        {
            $studentpositiondto = Mapper::MapClass(StudentPositionDto::class,$user->getCoursePosition($course_id)[0]);
        }

        if($studentpositiondto->Index == null)
            $studentpositiondto->Index = 0;

        return $studentpositiondto;
    }

    public function updateStudentPosition($user_id,$course_id,$adventure_id,$id,$type,$index)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));
        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));

        $journey = $course->getJourney();

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));

        if ($type == 'mission') {
            $mission = $this->journeyRepository->getMissionById($id);
            if ($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $task = $mission->getTask();
        }
        elseif($type == "html_mission"){
            $mission = $this->journeyRepository->getHtmlMissionbyId($id);
            if ($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $task = $mission->getTask();

        }
        else {
            $quiz = $this->journeyRepository->getquiz($id);
            if ($quiz == null)
                throw new BadRequestException(trans('locale.quiz_not_exist'));
            $task = $quiz->getTask();
        }

        $position_x = $this->journeyRepository->getTaskOrder($task, $journey->getId(), $adventure_id)->getPositionX();
        $position_y = $this->journeyRepository->getTaskOrder($task, $journey->getId(), $adventure_id)->getPositionY();

        $studentpositiondto = new StudentPositionDto();
        $studentpositiondto->PositionX = str_replace("px", "", $position_x);
        $studentpositiondto->PositionY = str_replace("px", "", $position_y);
        $studentpositiondto->Index = $index;
        $position = new StudentPosition($studentpositiondto, $user, $course,$adventure);
        $old_positions = $user->getCoursePosition($course_id);

        foreach ($old_positions as $old_position)
            $this->schoolRepository->deleteStudentPosition($old_position);

        $this->schoolRepository->storeStudentPosition($position);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.position_updated');
    }

    public function getTaskAuthorization($user_id,$course_id,$adventure_id,$id,$type,$recursive = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));



        if ($type == 'mission') {
            $mission = $this->journeyRepository->getMissionById($id);
            if ($mission == null)
                throw new BadRequestException(trans('locale.mission_not_exist'));

            $task = $mission->getTask();
        } else {
            $quiz = $this->journeyRepository->getquiz($id);
            if ($quiz == null)
                throw new BadRequestException(trans('locale.quiz_not_exist'));
            $task = $quiz->getTask();
        }
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));
        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));
        $missions = [];

        if($user->is_teacher() || $user->is_schoolAdmin()){
            $adventureTasks = $mission->getTask()->getTaskOrder()->getAdventure()->getTasks();
            foreach ($adventureTasks as $adventureTask){
                if($adventureTask->getMissions()->first() != null){
                    $mission = $adventureTask->getMissions()->first();
                    array_push($missions,array('name'=>$mission->getName(),'id'=>$mission->getId(),'locked'=>!$this->getTaskAuthorization($user_id,$course_id,$mission->getTask()->getTaskOrder()->getAdventure()->getId(),$mission->getId(),'mission',false)['task_authoriztion']));
                }
            }
            return ["task_authoriztion"=>true,"missions"=>$missions];
        }

        if(!($user->is_teacher() || $user->is_schoolAdmin()) && ($recursive && $type == 'mission')){
            $adventureTasks = $mission->getTask()->getTaskOrder()->getAdventure()->getTasks();
            foreach ($adventureTasks as $adventureTask){
                if($adventureTask->getMissions()->first() != null){
                    $mission = $adventureTask->getMissions()->first();
                    array_push($missions,array('name'=>$mission->getName(),'id'=>$mission->getId(),'locked'=>!$this->getTaskAuthorization($user_id,$course_id,$mission->getTask()->getTaskOrder()->getAdventure()->getId(),$mission->getId(),'mission',false)['task_authoriztion']));
                }
            }
        }


        return ["task_authoriztion"=>!$this->isLockedTask($task, $course, $adventure, $user_id),"missions"=>$missions];
    }

    public function getStudentAdventures(User $user,$course_id,$search = null,$status = "",$courseFoundSearch = true)
    {

        $current_adventure = null;
        $adventures = [];
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        $journey = $course->getJourney();

        if($journey->getStatus() != 'published')
            throw new CustomException(trans("locale.journey_not_published"),277);

        $adventures_data = $this->journeyRepository->getAdventuresByOrder($journey->getId());
        $adventure_counter = 0;
        //$done_flag = false;
        $adventure_current_gotten = false;
        foreach ($adventures_data as $adventure) {
            if($courseFoundSearch == true || $search == null || strpos(strtolower($adventure->getTitle()),strtolower($search)) !== false)
            {
                $progress = $this->schoolRepository->getStudentProgressInAdventure($user->getId(),$adventure->getId(),$course_id);
                $tasks_completed = 0;
                $totalTasks = count($adventure->getTasks());
                if(count($progress) > 0){
                    foreach ($progress as $singleProgress){
                        if($singleProgress->getFirstSuccess() != null)
                            $tasks_completed ++;
                    }
                }



                if(($adventure->getEndDateInCourse($course_id) < Carbon::now()) && ($tasks_completed != $totalTasks)){
                    $adventures[$adventure_counter] = ["adventure_id" => $adventure->getId(),"adventure_title" => $adventure->getTitle(),"status" => trans('locale.late'), "status_color" => "red", "deadline" =>Carbon::parse($adventure->getEndDateInCourse($course_id))->format('Y-m-d') ];
                    $adventures[$adventure_counter]["tasks_completed"] = $tasks_completed;
                    $adventures[$adventure_counter]["total_tasks"] = $totalTasks;
                }
                else if(($adventure->getEndDateInCourse($course_id) < Carbon::now()) && ($tasks_completed == $totalTasks)){
                    $adventures[$adventure_counter] = ["adventure_id" => $adventure->getId(),"adventure_title" => $adventure->getTitle(),"status" => trans('locale.done'), "status_color" => "green", "deadline" =>Carbon::parse($adventure->getEndDateInCourse($course_id))->format('Y-m-d') ];
                    $adventures[$adventure_counter]["tasks_completed"] = $tasks_completed;
                    $adventures[$adventure_counter]["total_tasks"] = $totalTasks;
                }
                else{
                    if($tasks_completed == $totalTasks){
                        $adventures[$adventure_counter] = ["adventure_id" => $adventure->getId(),"adventure_title" => $adventure->getTitle(),"status" => trans('locale.done'), "status_color" => "green", "deadline" =>Carbon::parse($adventure->getEndDateInCourse($course_id))->format('Y-m-d') ];
                        $adventures[$adventure_counter]["tasks_completed"] = $tasks_completed;
                        $adventures[$adventure_counter]["total_tasks"] = $totalTasks;
                    }
                    else{
                        if($course->getStartsAt() > Carbon::now()){
                            $adventures[$adventure_counter] = ["adventure_id" => $adventure->getId(),"adventure_title" => $adventure->getTitle(),"status" => trans('locale.upcoming'), "status_color" => "green", "deadline" =>Carbon::parse($adventure->getEndDateInCourse($course_id))->format('Y-m-d') ];
                            $adventures[$adventure_counter]["tasks_completed"] = $tasks_completed;
                            $adventures[$adventure_counter]["total_tasks"] = $totalTasks;
                        }
                        else if($adventure_current_gotten == false){
                            $adventures[$adventure_counter] = ["adventure_id" => $adventure->getId(),"adventure_title" => $adventure->getTitle(),"status" => trans('locale.current'), "status_color" => "green", "deadline" =>Carbon::parse($adventure->getEndDateInCourse($course_id))->format('Y-m-d') ];
                            $adventures[$adventure_counter]["tasks_completed"] = $tasks_completed;
                            $adventures[$adventure_counter]["total_tasks"] = $totalTasks;
                            $adventure_current_gotten = true;
                            $current_adventure = $adventures[$adventure_counter];
                        }
                        else{
                            $adventures[$adventure_counter] = ["adventure_id" => $adventure->getId(),"adventure_title" => $adventure->getTitle(),"status" => trans('locale.upcoming'), "status_color" => "green", "deadline" =>Carbon::parse($adventure->getEndDateInCourse($course_id))->format('Y-m-d') ];
                            $adventures[$adventure_counter]["tasks_completed"] = $tasks_completed;
                            $adventures[$adventure_counter]["total_tasks"] = $totalTasks;
                        }
                    }
                }
                $adventures[$adventure_counter]["adventure_order"] = $adventure->getOrder();
                $adventures[$adventure_counter]["journey_id"] = $journey->getId();
                $adventure_counter++;
            }
        }
        $adventuresx = array();
        foreach($adventures as $adventure){

            if($status == ""){
                array_push($adventuresx,$adventure);
            }
            else{
                if(strtolower($status) == strtolower($adventure['status'])){
                    array_push($adventuresx,$adventure);
                }
            }

        }
        return ["current_adventure" =>$current_adventure, "adventures" =>$adventuresx];
    }

    public function storeStudentMissionClick($user_id,$course_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));
        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));

        if(!$course->getClassroom()->getDefaultGroup()->isMember($user->getId()))
            throw new BadRequestException(trans("locale.student_doesn't_take_course"));

        if(!$this->accountRepository->isAuthorized('store_StudentClickMission',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));


        $studentMissionClick = $this->schoolRepository->getStoreStudentClickMissionByTimeAndUser($user,date("Y-m-d",time()));

        if($studentMissionClick == null){
            $studentMissionClick = new StudentClickMission($user,$course);

            $this->schoolRepository->storeStudentClickMission($studentMissionClick);
        }
        return trans("locale.student_click_mission_stored_successfully");
    }

    public function getNextTask($course_id,$adventure_id,$id,$type)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        $journey = $course->getJourney();

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));

        $task_index = $adventure->getOrder();

        if ($type == 'mission') {
            $mission = $this->journeyRepository->getMissionbyId($id);
            if($mission == null)
                throw new BadRequestException(trans("locale.mission_not_exist"));
            $task = $mission->getTask();
        }

        else{
            $quiz = $this->journeyRepository->getquiz($id);
            if($quiz == null)
                throw new BadRequestException(trans("locale.quiz_not_exist"));
            $task = $quiz->getTask();
        }

        $next_adventure_id = $adventure_id;

        $order = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure_id)->getOrder();

        $last_task = $this->journeyRepository->getLastTaskInAdventure($journey->getId(),$adventure_id);

        if($task->getId() != $last_task->getId())
        {
            $next_task = $this->journeyRepository->getTaskByOrder(intval($order) + 1,$journey->getId(),$adventure_id);
        }
        else {

            $next_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() + 1, $journey->getId());

            if ($next_adventure == null)
                throw new BadRequestException(trans("locale.next_adventure_not_exist"));

            $next_task = $this->journeyRepository->getFirstTaskInAdventure($journey->getId(), $next_adventure->getId());

            $task_index = $next_adventure->getOrder();

            $next_adventure_id = $next_adventure->getId();
        }

        if($next_task == null)
            throw new BadRequestException(trans("locale.next_task_not_exist"));

        $task_index--;

        $next_mission = $next_task->getMissions()->first();
        $next_mission_html = $next_task->getMissionsHtml()->first();
        if($next_mission != null)
        {
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(),"next_task_type"=>"mission","next_task_index" => $task_index, "next_task_adventure" => $next_adventure_id];
        }
        else if($next_mission_html != null){
            return ["next_task_id" => $next_mission_html->getId(), "next_task_name" => $next_mission_html->getTitle(),"next_task_type"=>"html_mission","next_task_index" => $task_index, "next_task_adventure" => $next_adventure_id];
        }
        else
        {
            $next_quiz = $next_task->getQuizzes()->first();

            return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(),"next_task_type"=>"quiz","next_task_index" => $task_index, "next_task_adventure" => $next_adventure_id];
        }
    }

    public function getNextTaskHtml($course_id,$adventure_id,$id,$type)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        $journey = $course->getJourney();

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));

        $task_index = $adventure->getOrder();

        if ($type == 'mission') {
            $mission = $this->journeyRepository->getHtmlMissionbyId($id);
            if($mission == null)
                throw new BadRequestException(trans("locale.mission_not_exist"));
            $task = $mission->getTask();
        }

        else{
            $quiz = $this->journeyRepository->getquiz($id);
            if($quiz == null)
                throw new BadRequestException(trans("locale.quiz_not_exist"));
            $task = $quiz->getTask();
        }

        $next_adventure_id = $adventure_id;

        $order = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure_id)->getOrder();

        $last_task = $this->journeyRepository->getLastTaskInAdventure($journey->getId(),$adventure_id);

        if($task->getId() != $last_task->getId())
        {
            $next_task = $this->journeyRepository->getTaskByOrder(intval($order) + 1,$journey->getId(),$adventure_id);
        }
        else {

            $next_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() + 1, $journey->getId());

            if ($next_adventure == null)
                throw new BadRequestException(trans("locale.next_adventure_not_exist"));

            $next_task = $this->journeyRepository->getFirstTaskInAdventure($journey->getId(), $next_adventure->getId());

            $task_index = $next_adventure->getOrder();

            $next_adventure_id = $next_adventure->getId();
        }

        if($next_task == null)
            throw new BadRequestException(trans("locale.next_task_not_exist"));

        $task_index--;

        $next_mission = $next_task->getMissions()->first();
        $next_mission_html = $next_task->getMissionsHtml()->first();
        if($next_mission != null)
        {
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(),"next_task_type"=>"mission","next_task_index" => $task_index, "next_task_adventure" => $next_adventure_id];
        }
        else if($next_mission_html != null){
            return ["next_task_id" => $next_mission_html->getId(), "next_task_name" => $next_mission_html->getTitle(),"next_task_type"=>"html_mission","next_task_index" => $task_index, "next_task_adventure" => $next_adventure_id];
        }
        else
        {
            $next_quiz = $next_task->getQuizzes()->first();

            return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(),"next_task_type"=>"quiz","next_task_index" => $task_index, "next_task_adventure" => $next_adventure_id];
        }
    }

    public function updateMCQMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$quiz_data,$selected_ids,$use_model_answer,$noOfBlocks,$timeTaken)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_not_exist"));

        if($course->getStartsAt() > Carbon::now())
            throw new BadRequestException(trans("locale.course_not_started"));
        if($course->getEndsAt() < Carbon::now())
            throw new BadRequestException(trans("locale.course_ended"));
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_not_exist"));
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans("locale.mission_not_exist"));
        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new BadRequestException(trans("locale.user_not_student"));

        if(!$user->hasCourse($course_id))
            throw new BadRequestException(trans("locale.user_not_have_course"));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $selected_ids_counter = 0;

        $questions_answers = [];
        $choice_correction = array();

        foreach ($quiz_data as $question)
        {
            $question_instance = $this->journeyRepository->getMcqQuestionById($question['question_id']);

            if($question_instance == null)
                throw new BadRequestException(trans("locale.question_not_exist"));
            $choice_instance = $this->journeyRepository->getMcqChoiceById($question['choice']);

            if($choice_instance == null)
                throw new BadRequestException(trans("locale.choice_not_exist"));
            if($use_model_answer && $use_model_answer != "false")
                $choice_correct = $this->journeyRepository->getMcqCorrectChooseChoice($question['question_id']);
            else
                $choice_correct = $this->journeyRepository->getMcqChoiceById($selected_ids[$selected_ids_counter]);
            $question_answer = ["question_id"=>$question['question_id'],
                //"question_body"=>$question_instance->getBody(),
                "user_choice"=>$choice_instance->choose,
                "choice_correct"=>$choice_correct->choose,
                "eval"=>false];
            if($choice_correct->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question_instance->getWeight();
            }
            else {
                $choice_message = $this->journeyRepository->getMcqChoiceById($question['choice']);
                $message = [
                    "choice_id" => $question['choice'],
                    "msg" => $choice_message->getCorrectionMsg()
                ];
                array_push($choice_correction, $message);
            }
            $questions_answers[] = $question_answer;
            $selected_ids_counter++;
        }

        // TO-DO Change Logic Of Success
        if($mark_counter >= (sizeof($quiz_data)/2))
            $passed = 1;

        $previous_progress = $this->schoolRepository->getStudentProgrss($task->getId(),$user_id,$course_id,$adventure_id);
        $baseScore = $score;
        if($previous_progress == null)
        {
            if($success_flag && $passed){
                $score += 5;
                //no of BlocksScore
//                if($mission->getType() != "text"){
//                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
//                        $score +=5;
//                    }
//                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    $score +=5;
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $baseScore;
                }
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,$score,1,$timeTaken,$timeTaken,$noOfBlocks,$noOfBlocks);
            }
            else
                $student_progress = new StudentProgress($task,$user,$course,$adventure,1,$score);
        }
        else
        {
            $no_trials = $previous_progress->getNo_of_trails() + 1;
            $first_success = $previous_progress->getFirstSuccess();
            if($success_flag && $passed && ($previous_progress->getFirstSuccess() == null))
                $first_success = $no_trials;
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    $score += 5;
                }
                //no of BlocksScore
//                if($mission->getType() != "text"){
//                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
//                        $score +=5;
//                    }
//                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    $score +=5;
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }

            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_progress->getScore()){
                $addedScore = $score - $previous_progress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_progress->getScore()){
                $score = $previous_progress->getScore();
            }
            $bestTimeTaken = $previous_progress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_progress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }
            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $passed){
                $score = $baseScore;
                if($previous_progress->getScore() > $score){
                    $score = $previous_progress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_progress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_progress->getScore();
                $addedScore = 0;
            }
            $this->schoolRepository->deleteStudentProgress($previous_progress);
            $student_progress = new StudentProgress($task,$user,$course,$adventure,$no_trials,$score,$first_success,$timeTaken,$bestTimeTaken,$noOfBlocks,$bestNoOfBlocks);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
        }
        $this->schoolRepository->storeStudentProgress($student_progress);
        return ["passed" => $passed, "questions_answers" => $questions_answers, "choice_correction" => $choice_correction];
    }

    public function getAdventures($user_id,$limit,$start_from,$search,$status= "")
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if(!$user->is_student())
            throw new BadRequestException(trans("locale.user_not_student"));
        $adventures = [];
        foreach ($user->getCourses() as $course) {
            $courseFound = true;
            if($search != null && (strpos(strtolower($course->getName()),strtolower($search)) === false)){
                $courseFound = false;
            }

            $course_adventures = $this->getStudentAdventures($user,$course->getId(),$search,$status,$courseFound)["adventures"];
            for($i = 0 ;$i<sizeof($course_adventures);$i++){
                $course_adventures[$i]['courseName'] = $course->getName();
            }
            $adventures = array_merge($adventures,$course_adventures);


        }
        $paginated_adventures = array_slice($adventures,$start_from,$limit);

        return ["count" => count($adventures), "courses" => $paginated_adventures];
    }

    public function getStudentCamps($user_id){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $camps = $this->schoolRepository->getStudentRunningCamps($student->getId());
        return Mapper::MapEntityCollection(CampNamesDto::class, $camps);
    }

    public function getStudentActivities($user_id, $camp_id, $start_from, $limit, $search){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $isInCamp = $this->schoolRepository->checkIfStudentInCamp($camp->getId(), $student->getId());
        if(!$isInCamp)
            throw new BadRequestException(trans('locale.student_not_in_camp'));

        $campStatus = $camp->getStatus();
        if($campStatus != 'running')
            throw new BadRequestException(trans('locale.not_active_camp'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $activitiesInCamp = $this->schoolRepository->getCampDefaultActivities($camp->getId(), $limit, $search);
        $schoolCamp = $camp->getSchoolCamp();
        if($schoolCamp == null)
            throw  new BadRequestException(trans("locale.camp_not_exist"));

        $data = array();
        $campStartDate = Carbon::createFromTimestamp($schoolCamp->getStartsAt());
        $campEndDate = Carbon::createFromTimestamp($schoolCamp->getCampDetail()->getEndsAt());
        array_push($data, ['campName' => $camp->getName(), 'campStartDate' => $schoolCamp->getStartsAt(), 'campEndDate' => $schoolCamp->getCampDetail()->getEndsAt(),
            'campDuration' => ($campEndDate->diffInDays($campStartDate)), 'activities' => array()]);
        foreach ($activitiesInCamp as $activity){
            $totalTasks = $activity->getTasksCount();
            $countSolvedTasks = $this->schoolRepository->getStudentSolvedActivityInCamp($camp->getId(), $activity->getId(), $student->getId(), true);
            $progress = ($countSolvedTasks / $totalTasks) * 100;
            array_push($data[0]['activities'], ['activityData' => Mapper::MapClass(ActivityMinifiedDto::class, $activity), 'activityProgress' => $progress . '%']);
        }
        return $data;
    }

    public function getStudentActivitiesCount($user_id, $camp_id, $search){
        $student = $this->accountRepository->getUserById($user_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $student->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $isInCamp = $this->schoolRepository->checkIfStudentInCamp($camp->getId(), $student->getId());
        if(!$isInCamp)
            throw new BadRequestException(trans('locale.student_not_in_camp'));

        $campStatus = $camp->getStatus();
        if($campStatus != 'running')
            throw new BadRequestException(trans('locale.not_active_camp'));

        $activitiesInCamp = $this->schoolRepository->getCampDefaultActivities($camp->getId(), null, $search, true);
        return $activitiesInCamp;
    }

    public function submitActivityCampQuiz($data, $id, $user_id,$camp_id,$activity_id,$is_default = true)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $questions_answers = [];
        $quiz = $this->journeyRepository->getquiz($id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        $task = $quiz->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            $choice_correct = Mapper::MapEntityCollection(ChoiceDto::class, $this->journeyRepository->getChoiceModelforQuestion($question['question_id']));
            $question_answer = ["question_id" => $question['question_id'],
                "question_body" => $question_instance->getBody(),
                "user_choice" => $choice_instance->getText(),
                "choice_correct" => $choice_correct[0]->text,
                "eval" => false];
            if($choice_correct[0]->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question['weight'];
            }
            $questions_answers[] = $question_answer;
        }
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $quiz_progress_object = $this->schoolRepository->getUserTaskProgressActivityCamp($quiz->getTask(), $user_id, $camp_id, $activity_id, $is_default);
        if($quiz_progress_object != null && $passed)
        {
            $trails = $quiz_progress_object->getNoTrials() + 1;
            $max_score = ($score > $quiz_progress_object->getScore()? $score:$quiz_progress_object->getScore());

            if($score > $quiz_progress_object->getScore()){
                $addedScore = $score - $quiz_progress_object->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }

            if($quiz_progress_object->getFirstSuccess() != null){
                $firstSuccess = $quiz_progress_object->getFirstSuccess();
            }
            else {
                $firstSuccess = 1;
            }
            $this->schoolRepository->deleteCampActivityProgress($quiz_progress_object);
            $task_progress = new CampActivityProgress($task,$user,$camp,$max_score,$trails,$firstSuccess);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->schoolRepository->storeCampActivityProgress($task_progress);
        }
        elseif($passed){
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
            $task_progress = new CampActivityProgress($task,$user,$camp,$score,1,1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->schoolRepository->storeCampActivityProgress($task_progress);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return ["passed" => $passed, "questions_answers" => $questions_answers];
    }

    public function checkOpenedModelAnswer($user, $mission_id){
        $returned = false;
        $isUser = false;
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;
            if($role->getName() == 'user' || $role->getName() == 'invited_Individual' )
                $isUser = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getJourneys();
            foreach ($journeys as $invJourney){
                if($invJourney->getJourney()->getId() == $mission->getTask()->getTaskOrder()->getJourney()->getId()){
                    if($isUser){
                        if($invJourney->getProgressLock() == 0){
                            $returned = true;
                        }
                    }
                    else{
                        $returned = true;
                    }
                }
            }
        }
        return $returned;
    }

    public function submitActivityCampMcqMission($data,$selected_ids,$use_model, $id, $user_id,$camp_id,$activity_id,$noOfBlocks,$timeTaken,$is_default = true){
        $this->accountRepository->beginDatabaseTransaction();

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $selected_ids_counter = 0;
        $questions_answers = [];

        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $task = $mission->getTask();
        $choice_correction = array();
        foreach ($data as $question) {
            $question_instance = $this->journeyRepository->getMcqQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getMcqChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            if($use_model && $use_model != "false")
                $choice_correct = $this->journeyRepository->getMcqCorrectChooseChoice($question['question_id']);
            else
                $choice_correct = $this->journeyRepository->getMcqChoiceById($selected_ids[$selected_ids_counter]);
            $question_answer = ["question_id" => $question['question_id'],
                "user_choice" => $choice_instance->choose,
                "choice_correct" => $choice_correct->choose,
                "eval" => false];

            if($choice_correct->id == $question['choice']) {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1;
                $score = $score + $question_instance->getWeight();
            }
            else {
                $choice_message = $this->journeyRepository->getMcqChoiceById($question['choice']);
                $message = [
                    "choice_id" => $question['choice'],
                    "msg" => $choice_message->getCorrectionMsg()
                ];
                array_push($choice_correction, $message);
            }
            $questions_answers[] = $question_answer;
            $selected_ids_counter++;
        }
        // TO-DO Change Logic Of Success
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());

        $baseScore = $score;
        $previous_missionprogress = $this->schoolRepository->getUserTaskProgressActivityCamp($mission->getTask(), $user_id, $camp_id, $activity_id, $is_default);
        if($previous_missionprogress == null){
            if($passed){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $baseScore;
                }
                $task_progress = new CampActivityProgress($task, $user, $camp, $score,1,1, $timeTaken, $timeTaken, $noOfBlocks, $noOfBlocks);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->schoolRepository->storeCampActivityProgress($task_progress);
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }
            else {
                $task_progress = new CampActivityProgress($task, $user, $camp,0,1);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->schoolRepository->storeCampActivityProgress($task_progress);
            }
        }
        else {
            $no_trials = $previous_missionprogress->getNoTrials() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($passed && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                //no of BlocksScore
//                if($mission->getType() != "text"){
//                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
//                        $score +=5;
//                    }
//                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }

            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }

            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $passed){
                $score = $baseScore;
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->schoolRepository->deleteCampActivityProgress($previous_missionprogress);
            $task_progress = new CampActivityProgress($task, $user, $camp, $score, $no_trials, $first_success, $timeTaken, $bestTimeTaken, $noOfBlocks, $bestNoOfBlocks);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->schoolRepository->storeCampActivityProgress($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

        }
        $this->journeyRepository->storeTask($task);
        $this->accountRepository->commitDatabaseTransaction();
        return ["passed" => $passed, "questions_answers" => $questions_answers, "choice_correction" => $choice_correction];
    }

    public function checkProgressUnlockedActivity($activity_id,$user_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($this->accountRepository->isAuthorized('unlock_AllTasks',$user))
            return true;

        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);

        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if($invitation_subscription == null)
            return false;

        $invitation = $invitation_subscription->getInvitation();
        if($is_default){
            foreach ($invitation->getDefaultActivities() as $invitation_default_activity) {
                if($invitation_default_activity->getDefaultActivity()->getId() == $activity_id && !$invitation_default_activity->getProgressLock())
                {
                    return true;
                }
            }
        }
        else{
            foreach ($invitation->getActivities() as $invitation_activity) {
                if($invitation_activity->getActivity()->getId() == $activity_id && !$invitation_activity->getProgressLock())
                {
                    return true;
                }
            }
        }
        return false;
    }

    public function isLockedTaskActivity(Task $task, $camp_id, $activity_id, $user_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->is_student())
            throw new BadRequestException(trans('locale.students_not_use_service'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $first_task = $this->journeyRepository->getFirstTaskInActivity($activity_id,$is_default);
        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->journeyRepository->getActivityTaskOrder($task,$activity_id,$is_default)->getOrder();
        if($task->getId() != $first_task->getId())
        {
            $latestActivityTaskOrder = $this->schoolRepository->getLastSolvedTaskInActivity($user_id,$camp_id,$activity_id,$is_default)->getOrder();
            if($order <= ($latestActivityTaskOrder+1))
                return false;
            else
                return true;
        }
        else {
            return false;
        }
    }

    public function checkQuizAuthorizationActivityCamp($user_id,$quiz_id,$camp_id,$activity_id,$is_default= true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $progress_unlocked = $this->checkProgressUnlockedActivity($activity_id,$user_id,$is_default);
        if($progress_unlocked)
            return ["quiz_authorization" => true];

        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        $task = $quiz->getTask();
        if($user->is_student())
            $locked = $this->isLockedTaskCamp($task, $camp_id, $activity_id, $user_id, $is_default);
        else
            $locked = $this->isLockedTaskActivity($task, $camp_id, $activity_id, $user_id, $is_default);
        return ["quiz_authorization" => !$locked];
    }

    public function checkMissionAuthorizationActivityCamp($user_id,$mission_id,$camp_id,$activity_id,$is_default= true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $progress_unlocked = $this->checkProgressUnlockedActivity($activity_id,$user_id,$is_default);
        if($progress_unlocked)
            return ["mission_authorization" => true];

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        if($user->is_student())
            $locked = $this->isLockedTaskCamp($task, $camp_id, $activity_id, $user_id, $is_default);
        else
            $locked = $this->isLockedTaskActivity($task, $camp_id, $activity_id, $user_id, $is_default);
        return ["mission_authorization" => !$locked];
    }

    public function deleteStudentCamp(School $userSchool, $student_id)
    {
        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans("locale.student_doesn't_exist"));

        $studentSchool = $student->getAccount()->getSchool();
        if($studentSchool == null)
            throw new BadRequestException(trans("locale.student_no_school"));

        if($studentSchool->getId() != $userSchool->getId())
            throw new BadRequestException(trans("locale.student_school_mismatch"));

        $studentRoles = $student->getRoles();
        $isStudent = false;
        foreach ($studentRoles as $studentRole){
            if($studentRole->getName() == "student")
                $isStudent = true;
        }
        if(!$isStudent)
            throw  new BadRequestException(trans("locale.student_wrong_role"));

        if(count($student->getCampProgress()) > 0)
            throw  new BadRequestException(trans("locale.student_has_progress_camp"));

        $this->accountRepository->deleteUser($student);
        $this->schoolRepository->deleteGroupMembers($student);
        return trans("locale.student_delete_successfull");
    }

    public function deleteStudentsCamp($user_id, $students){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Student', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        foreach ($students as $studentId){
            $this->deleteStudentCamp($school, $studentId);
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.students_delete_successfull");
    }

    public function isLockedTaskCamp(Task $task, $camp_id, $activity_id, $user_id, $is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans("locale.activity_id"). $activity->getId() .trans("locale.doesn't_exist"));

        $first_task = $this->journeyRepository->getFirstTaskInActivity($activity->getId());
        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->schoolRepository->getActivityTaskOrder($activity->getId(), $task->getId(), $is_default)->getOrder();
        $progress = $this->schoolRepository->getUserProgressByOrderInActivity($user_id, intval($order) - 1, $camp_id, $activity->getId(), $is_default);
        return (($progress != null) && ($progress->getFirstSuccess() != null) ? false : true);
    }

    //endregion
}