<?php

namespace App\ApplicationLayer\Schools;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Schools\Dtos\ChargeDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomNameDto;
use App\ApplicationLayer\Schools\Dtos\CustomPlanDto;
use App\ApplicationLayer\Schools\Dtos\DistributorChargeDto;
use App\ApplicationLayer\Schools\Dtos\DistributorSubscriptionDto;
use App\ApplicationLayer\Schools\Dtos\SchoolChargeLogDto;
use App\ApplicationLayer\Schools\Dtos\SchoolBundleDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\DomainModelLayer\Accounts\InvitationSubscription;
use App\DomainModelLayer\Schools\DistributorCharge;
use App\DomainModelLayer\Schools\DistributorChargeDetails;
use App\DomainModelLayer\Schools\DistributorChargeUpgrade;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Schools\SchoolCharge;
use App\DomainModelLayer\Schools\SchoolChargeDetails;
use App\DomainModelLayer\Schools\SchoolChargesLog;
use App\DomainModelLayer\Schools\SchoolChargeUpgrade;
use App\Framework\Exceptions\BadRequestException;
use App\DomainModelLayer\Accounts\Account;
use App\DomainModelLayer\Schools\Grade;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\Framework\Exceptions\CustomException;
use App\Framework\Exceptions\UnauthorizedException;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Mapper;
use App\ApplicationLayer\Accounts\Dtos\UserPlansDto;
use App\ApplicationLayer\Accounts\Dtos\SchoolPlanDto;
use App\ApplicationLayer\Accounts\Dtos\SubscriptionDto;
use App\DomainModelLayer\Schools\Distributor;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Schools\DistributorSubscription;
use App\DomainModelLayer\Schools\Country;
use App\ApplicationLayer\Schools\Dtos\DistributorDto;
use App\ApplicationLayer\Schools\Dtos\DistributorRequestDto;
use App\ApplicationLayer\Schools\Dtos\CountryDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use Carbon\Carbon;
use App\DomainModelLayer\Accounts\Subscription;
use App\DomainModelLayer\Accounts\SubscriptionItem;
use App\DomainModelLayer\Accounts\StripeSubscription;
use App\DomainModelLayer\Accounts\SubscriptionHistory;
use App\DomainModelLayer\Schools\StudentClickMission;
use App\DomainModelLayer\Accounts\UserCreated;
use App\DomainModelLayer\Accounts\UserCreatedHandle;
use Illuminate\Pagination\Paginator;
use App\ApplicationLayer\Accounts\Dtos\NotificationDto;
use App\DomainModelLayer\Accounts\Notification;
use App\DomainModelLayer\Accounts\NotificationTranslation;

class SchoolService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;

    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;

    }

    public function registerAdmin(UserDto $userDto,SchoolDto $schoolDto){
        $this->accountRepository->beginDatabaseTransaction();

        $userDto->accounttype = "School";
        $userDto->role = "school_admin";

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);
        if($userDto->social_id != null)
        {
            $status = $this->accountRepository->getStatusByName('locked');
        }
        else
            $status = $this->accountRepository->getStatusByName('locked');
        if($status == null)
            throw new BadRequestException(trans("locale.status_not_exist"));
        $account = new Account($userDto, $account_type,$status);
        $user = new User($userDto, $account);

        //Creating a new role for this user
        $role = $this->accountRepository->getRoleByName($userDto->role);
        //$user_role = new UserRole($role);
        $user->addRole($role);
        $this->accountRepository->addUser($user);

        //$user has a user role now

        //add school
        $country = $this->schoolRepository->getCountryByCode($schoolDto->country_code);
        if($country== null) throw new BadRequestException(trans("locale.country_not_exist"));

        $school = new School($account,$schoolDto);
        $school->setCountry($country);
        $this->schoolRepository->addSchool($school);
        $this->schoolRepository->addDefaultGrades($school);
        $distributor = $this->schoolRepository->getDistributorById($schoolDto->distributor_id);
        if($distributor== null) throw new BadRequestException(trans("locale.distributor_not_exist"));

        // get plan from name
        $plan = $this->accountRepository->getPlanByName($schoolDto->plan);
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        // get the last updated version of plan from plan history
        $updated_plan = $this->accountRepository->getlastupdated($plan,$account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        // check if user have already subscribed to this plan before
        if($this->accountRepository->checkSubscription($updated_plan,$account))
            throw new BadRequestException(trans("locale.subscribed_plan"));

        $old_subscription = $account->getSubscriptions()->first();

        if($old_subscription != null)
        {
            $old_subscription->setEndDate(Carbon::now());

            $this->accountRepository->storeSubscription($old_subscription);
        }

        // create a new subsciption dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = null;

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto,$updated_plan,$account);

        $use_stripe = false;

        if($distributor->getStripeCheck())
        {
            $use_stripe = true;

            if($schoolDto->stripe_token == null)
                throw new BadRequestException(trans("locale.payment_info_required"));

            $account = $this->addCreditCardToken($account,$schoolDto->stripe_token);
            $plan_period = $this->accountRepository->getPlanPeriodByName('yearly');
            if($plan_period == null)
                throw new BadRequestException(trans("locale.plan_period_not_exist"));
            $stripe_result = $this->accountRepository->handlePaidSubscription(null,$updated_plan->getName(),'yearly','School',$account->getStripeId(),0,$account->getCountry());

            $subscription->removeEndsAt();

            $stripe_subscription = new StripeSubscription($subscription,$plan_period,$stripe_result['subscription_id']);

            $subscription->addStripeSubscription($stripe_subscription);

            // update account with the new stripe id
            $subscription->account->setStripeId($stripe_result['customer_id']);
        }

        $current_code = $distributor->getId().Carbon::parse(Carbon::now())->timestamp.str_random(30);

        // $current_code = Hash::make($current_code_string);

        $upcoming_code= $distributor->getId().Carbon::parse(Carbon::now()->addYears(1))->timestamp.str_random(30);

        // $upcoming_code = Hash::make($upcoming_code_string);

        $distributor_subscription = new DistributorSubscription($subscription,$distributor,$current_code,$upcoming_code);

        $subscription->addDistributorSubscription($distributor_subscription);

        $account->addSubscription($subscription);

        // passing the subscription instance to subscription repository to be saved
        $this->accountRepository->storeAccount($account);

        $extra_plans_info = [];

        if(count($userDto->extra_plans) >  0)
        {
            foreach ($userDto->extra_plans as $extra_plan) {
                $result = ["name" => $extra_plan, "amount" => $this->addExtraPlan($user->getId(), $extra_plan, $use_stripe)["amount"]];
                $extra_plans_info[] = $result;
            }
        }

        $this->markUnsubscribeNotificationsAsRead($user);

        $this->schoolRepository->emailDistributor($distributor->getName(),$updated_plan->getName(),$updated_plan->getCreditPerYear(),'USD','Per Year',1,$distributor->getEmail(),$extra_plans_info,$user->getName(),$user->getEmail(),$school->getName(),$distributor_subscription->getCurrentCode(),"");
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        $this->schoolRepository->emailSubscription($user_name,$updated_plan->getName(),$updated_plan->getCreditPerYear(),'USD','Per Year',1,$user->getEmail(),0);
        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.school_account_created");

    }

    public function addTeacher($userId,UserDto $userDto){

        $this->accountRepository->beginDatabaseTransaction();
        $mainUser = $this->accountRepository->getUserById($userId);
        if($mainUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("create_Teacher",$mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));
        $userDto->role = "teacher";

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $account = $mainUser->getAccount();
        $user = new User($userDto, $account);

        $role = $this->accountRepository->getRoleByName($userDto->role);

        $user->addRole($role);

        $this->accountRepository->addUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return UserDtoMapper::CustomerMapper($user);
    }

    public function addStudent($userId,StudentDto $studentDto){

        $this->accountRepository->beginDatabaseTransaction();
        $mainUser = $this->accountRepository->getUserById($userId);
        if($mainUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("create_Student",$mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));
        $account = $mainUser->getAccount();
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.subscription_perios_ended"));
        $subscription = $subscriptions->last();
        $plan_info = $subscription->getPlan()->getSchoolInfo();
        $student_count = count($this->schoolRepository->getAllStudents($account->getId()));
        if($plan_info == null)
        {
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription == null){
                $schoolCharge = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                    ->first()->getSchoolCharge();
                if($schoolCharge == null){
                    $distibutorSubscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                        ->first()->getDistributorSubscriptions()->first();
                    if($distibutorSubscription == null)
                        throw new BadRequestException(trans("locale.school_plan_information_missing"));
                }
            }
            else{
                $plan_info = $invitationSubscription->getInvitation()->getSchoolInfo();
                if($plan_info == null)
                    throw new BadRequestException(trans("locale.school_plan_information_missing"));
            }

        }
        if($plan_info != null)
            $max_students = $plan_info->getMaxNumberOfStudents();
        else
            $max_students = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxStudents"];

        if($student_count >= $max_students)
            throw new BadRequestException(trans("locale.student_max_reached"));

        $studentDto->role = "student";

        $existingUsername = $this->schoolRepository->UserNameAlreadyExist($studentDto->username);
        if($existingUsername)
            throw new BadRequestException(trans('locale.username_exists'));

        $account = $mainUser->getAccount();
        $user = new User($studentDto, $account);

        $role = $this->accountRepository->getRoleByName($studentDto->role);
        $user->addRole($role);

        if($studentDto->grade == null)
            throw new BadRequestException(trans("locale.student_must_have_grade"));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $grade = $this->schoolRepository->getGradeByName($school,$studentDto->grade);

        if($grade == null)
            throw new BadRequestException(trans('locale.grade_name_not_in_school'));

        $user->addGrade($grade);

        if($studentDto->classroom_id != null)
        {
            $classroom = $this->schoolRepository->getClassroomById($studentDto->classroom_id);
            if($classroom == null)
                throw new BadRequestException(trans("locale.classroom_id").$studentDto->classroom_id.trans("locale.doesn't_exist"));
            if($classroom->isMember($user->getId()))
                throw new BadRequestException(trans("locale.user_in_classroom_with_id").$studentDto->classroom_id);
            $classroomGrade  = $classroom->getGrade();
            if($grade->getId() != $classroomGrade->getId())
                throw new BadRequestException(trans("locale.classroom_grade_doesn't_match_student_grade"));
            $defaultGroup = $classroom->getDefaultGroup();
            $groupMember = new GroupMember($defaultGroup,$user,$role);
            $user->addGroupMember($groupMember);

        }
        $this->accountRepository->addUser($user);
        $this->accountRepository->commitDatabaseTransaction();

        return Mapper::MapClass(StudentDto::class,$user,[ClassroomDto::class,GradeDto::class]);
    }

    public function addStudentsCSV($userId,$file){

        //check user
        $this->accountRepository->beginDatabaseTransaction();
        $mainUser = $this->accountRepository->getUserById($userId);
        $role = $this->accountRepository->getRoleByName("student");
        $account = $mainUser->getAccount();
        if($mainUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("create_Student",$mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));

        //check file
        $data = json_encode(Excel::load($file,function($reader){
            $results = $reader->get();
            return $results;
        }));
        $data = json_decode($data,true);

        $data = $data['parsed'];

        if(sizeof($data) == 0)
            throw new BadRequestException(trans("locale.file_empty"));

        if(sizeof(array_intersect(array_keys($data[0]),['firstname','lastname','username','password'])) != 4)
            throw new BadRequestException(trans("locale.first_row_need_params_student_csv"));

        $total = sizeof($data);

        $account = $mainUser->getAccount();
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.subscription_perios_ended"));
        $subscription = $subscriptions->last();
        $plan_info = $subscription->getPlan()->getSchoolInfo();
        if($plan_info == null)
        {
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription == null){
                $schoolCharge = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                    ->first()->getSchoolCharge();
                if($schoolCharge == null){
                    $distibutorSubscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                        ->first()->getDistributorSubscriptions()->first();
                    if($distibutorSubscription == null)
                        throw new BadRequestException(trans("locale.school_plan_information_missing"));
                }
            }
            else{
                $plan_info = $invitationSubscription->getInvitation()->getSchoolInfo();
                if($plan_info == null)
                    throw new BadRequestException(trans("locale.school_plan_information_missing"));
            }

        }
//        $students_count = count($this->schoolRepository->getAllStudents($account->getId()));
//
//        if($plan_info != null)
//            $max_students = $plan_info->getMaxNumberOfStudents();
//        else
//            $max_students = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxStudents"];
//
//        if($max_students == null)
//            $max_students = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxStudents"];
//
//        if(($students_count+$total) > $max_students)
//            throw new BadRequestException(trans("locale.csv_data_exceed_student_count"));

        $totalSuccess = 0;
        $totalFailure = 0;
        $errors = array();
        for($i = 0 ; $i<$total;$i++) {
            $data[$i]['firstname']  = trim($data[$i]['firstname']);
            $data[$i]['lastname']  = trim($data[$i]['lastname']);
            $data[$i]['username']  = trim($data[$i]['username']);
            $v = Validator::make($data[$i], [
                'firstname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'lastname' => 'sometimes|nullable|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'password' => 'required|min:6|max:24',

            ]);

            $errorsArray = array();
            if ($v->fails()) {
                $errorsArray = array_merge($errorsArray,$v->errors()->all());

            }

            $studentDto = new StudentDto();
            $studentDto->fname = $data[$i]['firstname'];
            $studentDto->lname = ($data[$i]['lastname'] != null? $data[$i]['lastname'] : "");
            $studentDto->username = $data[$i]['username'];
            $studentDto->password = $data[$i]['password'];
            $studentDto->role = "student";


            $existingUsername = $this->schoolRepository->UserNameAlreadyExist($studentDto->username);
            if($existingUsername)
                array_push($errorsArray,trans('locale.username_exists'));


            $school = $account->getSchool();
            if($school == null)
                throw new BadRequestException(trans('locale.no_school_attached'));


            if(sizeof($errorsArray)>0){
                $totalFailure++;
                array_push($errors, ['rownumber'=>$i+2,'errors'=>$errorsArray]);
            }
            else{

                $user = new User($studentDto, $account);
                $user->addRole($role);
                $this->accountRepository->addUser($user);
                $totalSuccess++;
            }

        }
        if($totalFailure == 0 && $total != 0 )
            $this->accountRepository->commitDatabaseTransaction();
        return ['total'=>$total,'totalSuccess'=>$totalSuccess,'totalFailure'=>$totalFailure,'errors'=>$errors];

    }

    public function addTeachersCSV($userId,$file){

        //check user
        $this->accountRepository->beginDatabaseTransaction();
        $mainUser = $this->accountRepository->getUserById($userId);
        $account = $mainUser->getAccount();
        $role = $this->accountRepository->getRoleByName("teacher");
        if($mainUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("create_Student",$mainUser))
            throw new BadRequestException(trans('locale.user_not_permission_create_teacher'));


        //check file
        $data = json_encode(Excel::load($file->getPath().'\\'.$file->getFilename(),function($reader){
            $results = $reader->get();
            return $results;
        },null,true));

        $data = json_decode($data,true);
        $data = $data['parsed'];
        if(sizeof($data) == 0)
            throw new BadRequestException(trans("locale.file_empty"));

        if(sizeof(array_intersect(array_keys($data[0]),['firstname','lastname','email','username','password'])) != 5)
            throw new BadRequestException("file must contain firstname,lastname,email,username,password as first row");

        $total = sizeof($data);
        $totalSuccess = 0;
        $totalFailure = 0;
        $errors = array();
        for($i = 0 ; $i<$total;$i++) {
            $v = Validator::make($data[$i], [
                'firstname' => 'required|min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'lastname' => 'min:2|max:15|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'username' => 'required|min:2|max:255|regex:/^[a-zA-Z0-9\x{0620}-\x{064A}_\\$@\\-]+$/u',
                'email' => 'required|email',
                'password' => 'required|min:6|max:24'
            ]);

            $errorsArray = array();
            if ($v->fails()) {
                $errorsArray = array_merge($errorsArray,$v->errors()->all());

            }

            $userDto = UserDtoMapper::CSVMapper($data[$i]);
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if($existingUserMail != null)
                array_push($errorsArray,"user with email already exists");
            $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
            if($existingUsername != null)
                array_push($errorsArray,trans('locale.username_exists'));

            if(sizeof($errorsArray)>0){
                $totalFailure++;
                array_push($errors, ['rownumber'=>$i+2,'errors'=>$errorsArray]);
            }
            else{
                $totalSuccess++;
                $userDto->role = "teacher";
                $user = new User($userDto, $account);
                $user->addRole($role);
                $this->accountRepository->addUser($user);
            }

        }
        if($totalFailure == 0 && $total != 0 )
            $this->accountRepository->commitDatabaseTransaction();
        return ['total'=>$total,'totalSuccess'=>$totalSuccess,'totalFailure'=>$totalFailure,'errors'=>$errors];
    }

    public function getSchoolPlans()
    {
        $accountType = $this->accountRepository->getAccountTypeByName('School');
        if($accountType== null) throw new BadRequestException(trans("locale.account_type_not_found"));
        return Mapper::MapEntityCollection(UserPlansDto::class,$this->accountRepository->getPlansByAccountType($accountType->id),[SchoolPlanDto::class]);
    }

    public function getAllDistributors()
    {
        return Mapper::MapEntityCollection(DistributorDto::class,$this->schoolRepository->getDistributors(),[CountryDto::class]);
    }

    public function getDistributorsByCountryCode($country_code)
    {
        $country_code = strtoupper($country_code);
        $country = $this->schoolRepository->getCountryByCode($country_code);
        if($country== null) throw new BadRequestException(trans("locale.country_not_exist"));
        $distributorObjects = $country->getDistributors();
        $distributors = array_merge(Mapper::MapEntityCollection(DistributorRequestDto::class,$distributorObjects),Mapper::MapEntityCollection(DistributorRequestDto::class,$this->schoolRepository->getDefaultDistributors()));
        // return ["country_distributors" => Mapper::MapEntityCollection(DistributorRequestDto::class,$country->getDistributors()), "default_distributors" => Mapper::MapEntityCollection(DistributorRequestDto::class,$this->schoolRepository->getDefaultDistributors())];
        $counter =0;
        foreach ($distributors as $distributor){
            $distributor->clicked = false;
        }
        return $distributors;
    }

    public function getAllCountries()
    {
        return Mapper::MapEntityCollection(CountryDto::class,$this->schoolRepository->getCountries());
    }

    public function addCreditCardToken(Account $account,$stripe_token)
    {
        $customer_id = $this->accountRepository->createStripeCustomer($stripe_token);
        $account->setStripeId($customer_id);

        return $account;
    }

    public function updateCreditCardToken(Account $account, $stripe_token)
    {
        $customer_id = $this->accountRepository->updateStripeCustomer($account->getStripeId(), $stripe_token);
        $account->setStripeId($customer_id);
        return $account;
    }

    public function markUnsubscribeNotificationsAsRead(User $user){
        $users = $user->getAccount()->getUsers();
        foreach ($users as $user) {
            $userNotifications = $this->accountRepository->getUserUnreadNotications($user->getId());
            foreach ($userNotifications as $userNotification) {
                $translations = $userNotification->getTranslations();
                foreach ($translations as $translation) {
                    if($translation->getLanguageCode() == "en" &&($translation->getTitle() == "Unsubscription" || $translation->getTitle() == "Parent Confirmation Requested"))

                    {
                        $userNotification->confirmed();
                        $this->accountRepository->storeNotification($userNotification);
                        break;
                    }
                }
            }
        }

    }

    public function handlePaidSubscription(Account $account,$period,PlanHistory $updated_plan,$stripe_token,User $user,$trial = false)
    {
        if($period == null)
            throw new BadRequestException(trans("locale.plan_period_missing"));
        $plan_period = $this->accountRepository->getPlanPeriodByName($period);
        if($plan_period == null)
            throw new BadRequestException(trans("locale.plan_period_not_exist"));
        // $plan_names = ['monthly','yearly','quarterly'];
        // if(! in_array($plan_period,$plan_names))
        //     throw new BadRequestException('Plan Period is wrong');
        // handle subscription on stripe
        $stripe_result = $this->accountRepository->handlePaidSubscription($stripe_token,$updated_plan->getName(),$period,$account->getAccountType()->getName(),$account->getStripeId(),$trial,$account->getCountry());

        $old_subscription = $account->getSubscriptions()->first();

        if($old_subscription != null)
        {
            $old_subscription->setEndDate(Carbon::now());

            $this->accountRepository->storeSubscription($old_subscription);
        }



        // create a new subsciption dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = null;

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto,$updated_plan,$account);

        $stripe_subscription = new StripeSubscription($subscription,$plan_period,$stripe_result['subscription_id']);

        $subscription->addStripeSubscription($stripe_subscription);

        // update account with the new stripe id
        $subscription->account->setStripeId($stripe_result['customer_id']);

        $account->addSubscription($subscription);

        // passing the subscription instance to subscription repository to be saved
        $this->accountRepository->storeAccount($account);

        // send an email to user through repository
        if($account->getAccountType()->getName() == 'Family')
        {
            $subscription_quantity = 2;
        }
        else
        {
            $subscription_quantity = 1;
        }
        // $this->accountRepository->emailSubscription($user->getName(),$updated_plan->getName(),$stripe_result['subscription_amount'],$stripe_result['subscription_currency'],'Per '.$stripe_result['subscription_interval'],$subscription_quantity,$user->getEmail(),$trial);
        $subscriptionDto->stripeResult = $stripe_result;

        return $subscriptionDto;
    }

    public function activateSchoolAccount($user_id,$code)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        $distributor_subscription = $this->schoolRepository->getLastDistributorSubscription($user_id);
        if($distributor_subscription == null)
            throw new BadRequestException(trans("locale.no_subscriptions_account"));

        if($distributor_subscription->getCurrentCode() != $code)
            throw new BadRequestException(trans("locale.distributor_code_invalid"));

        $status = $this->accountRepository->getStatusByName('wait_activation');
        if($status == null)
            throw new BadRequestException(trans("locale.something_wrong"));

        $account->setStatus($status);

        //check account active subscriptions
        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        if(count($activeSubscriptions) > 0)
            throw new BadRequestException(trans("locale.cant_use_sub_code_when_active_sub"));

        //remove active free subscription
        $freeSub = $this->accountRepository->getActiveFreeSubscription($account->getId());
        if($freeSub != null){
            $freeSub->setEndDate(Carbon::now());
            $this->accountRepository->storeSubscription($freeSub);
        }

        $subscription = $distributor_subscription->getSubscription();

        $subscription->setEndDate(Carbon::now()->addYears(1));

        $this->accountRepository->storeSubscription($subscription);
        $this->accountRepository->storeAccount($account);

        $end_date = Carbon::now()->addYears(1);
        $dto = new NotificationDto();
        $dto->StartDate = Carbon::parse($end_date)->subDays(7);
        $dto->EndDate = $end_date;
        $dto->Type = "warning";
        $notification = new Notification($dto, $user);
        $translations = $this->accountRepository->translateNotification('unsubscription', 'account_expire_at');
        foreach ($translations as $translation) {
            $dto->Title = $translation['title'];
            $dto->Message = $translation['message'] . " " . $end_date;
            $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
            $notification->addTranslation($notification_translation);
        }
        $user->addNotification($notification);
        $this->accountRepository->storeUser($user);

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.account_activated');
    }

    public function addExtraPlan($user_id, $plan_name, $use_stripe)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();

        $plan = $this->accountRepository->getPlanByName($plan_name);
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        // get the last updated version of plan from plan history
        $updated_plan = $this->accountRepository->getlastupdated($plan,$account->getAccountType()->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        $lastInvitation = $this->accountRepository->getLastSubscription($account)->getInvitationSubscription()->first();
        if($lastInvitation == null && count($subscriptions) == 0){
            throw new BadRequestException(trans("locale.invitation_expired_register"));
        }

        if(count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.subscription_period_ended"));

        $subscription = $subscriptions->last();

        $items = $subscription->getItems();

        foreach ($items as $item) {
            if($item->getPlan->getName() == $plan_name)
                throw new BadRequestException(trans("locale.you_have_item"));
        }

        $old_subscription = $subscription;

        if($use_stripe)
        {
            if($account->getStripeId() == null)
            {
                //if($stripe_token == null)
                throw new BadRequestException(trans("locale.have_credit_token"));
                // else
                //     $account = $this->addCreditCardToken($account, $stripe_token);
            }

            if($subscription->getStripeSubscriptions()->first() == null)
                throw new BadRequestException(trans("locale.not_add_extra_item_plans"));

            $stripe_subscription = $subscription->getStripeSubscriptions()->first();

            $stripe_item = $this->accountRepository->addSubscriptionItem($stripe_subscription,$updated_plan->getName(),'yearly',$account->getAccountType()->getName());

            $subscription_item = new SubscriptionItem($subscription,$updated_plan,$stripe_item->id);
        }

        else
        {
            $subscription_item = new SubscriptionItem($subscription,$updated_plan,'no_stripe');
            $subscription_item->setEndDate($subscription->getEndDate());
        }

        $subscription->addSubscriptionItem($subscription_item);

        $account->removeSubscription($old_subscription);

        $account->addSubscription($subscription);

        $this->accountRepository->storeAccount($account);

        $amount = $updated_plan->getCreditPerYear();

        return ["message" => trans('locale.extra_item_added'), "amount" => $amount];
    }

    public function renewSubscription($user_id,$code,$subscription_id)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        if(!$this->accountRepository->isAuthorized('renew_subscription',$user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $subscription = $this->accountRepository->getSubscriptionById($subscription_id);
        if($subscription == null)
            throw new BadRequestException(trans('locale.subscription_not_exist'));

        $distributor_subscription= $subscription->getDistributorSubscriptions()->first();

        if($code != $distributor_subscription->getUpcomingCode())
            throw new BadRequestException(trans('locale.distributor_code_invalid'));

        $distributor_subscription->setCurrentCode($distributor_subscription->getUpcomingCode());

        $upcoming_code_string= $distributor_subscription->getDistributor()->getId().Carbon::now()->addYears(1).str_random(30);

        $upcoming_code = Hash::make($upcoming_code_string);

        $distributor_subscription->setUpcomingCode($upcoming_code);

        $this->schoolRepository->storeDistributorSubscription($distributor_subscription);

//        if($subscription->getEndDate() > Carbon::now())
//        {
//            $subscription->setEndDate(Carbon::parse($subscription->getEndDate())->addYears(1));
//        }
        //else
        //{
        $subscription->setEndDate(Carbon::now()->addYears(1));
        //}

        $this->accountRepository->storeSubscription($subscription);

        $this->accountRepository->commitDatabaseTransaction();

        return trans('locale.subscription_renew_success');
    }

    public function upgradeSubscription($user_id,$code,$subscription_id,$plan_name)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        if(!$this->accountRepository->isAuthorized('upgrade_subscription',$user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $subscription = $this->accountRepository->getSubscriptionById($subscription_id);
        if($subscription == null)
            throw new BadRequestException(trans('locale.subscription_not_exist'));

        $distributor_subscription= $subscription->getDistributorSubscriptions()->first();

        if($code != $distributor_subscription->getUpcomingCode())
            throw new BadRequestException(trans('locale.distributor_code_invalid'));

        $upcoming_code_string= $distributor_subscription->getDistributor()->getId().Carbon::now()->addYears(1).str_random(30);

        $upcoming_code = Hash::make($upcoming_code_string);

        $distributor_subscription->setUpcomingCode($upcoming_code);

        $this->schoolRepository->storeDistributorSubscription($distributor_subscription);

        $old_plan = $subscription->getPlan();

        $plan = $this->accountRepository->getPlanByName($plan_name);
        if($plan == null)
            throw new BadRequestException(trans('locale.plan_not_exist'));

        // get the last updated version of plan from plan history
        $updated_plan = $this->accountRepository->getlastupdated($plan,$account->getAccountType()->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans('locale.updated_plan_not_exist'));

        if($old_plan->getCreditPerMonth() > $updated_plan->getCreditPerMonth())
            throw new BadRequestException(trans('locale.no_downgrade'));

        if($updated_plan->getSchoolInfo() != null){
            $max_students = $updated_plan->getSchoolInfo()->getMaxNumberOfStudents();
            $max_courses = $updated_plan->getSchoolInfo()->getMaxNumberOfCourses();
        }

        if($max_students == null || $max_courses == null){
            $maxs = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId());
            $max_students = $maxs["maxStudents"];
            $max_courses = $maxs["maxClassrooms"];

        }

        $student_count = count($this->schoolRepository->getAllStudents($account->getId()));
        if($student_count > $max_students)
            throw new BadRequestException(trans('locale.upgrade_max_students'));
        $courses_count = $account->getSchool()->getJourneysCount();
        if($courses_count > $max_courses)
            throw new BadRequestException(trans('locale.upgrade_max_courses'));
        $subscription_history = new SubscriptionHistory($subscription);

        $subscription->addHistory($subscription_history);

        $subscription->setPlan($updated_plan);

        $this->accountRepository->storeSubscription($subscription);

        $this->renewSubscription($user->getId(),$upcoming_code,$subscription->getId());

        // send an email to user through repository
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        $this->schoolRepository->emailSubscription($user_name,$updated_plan->getName(),$updated_plan->getCreditPerYear(),'USD','Per Year',1,$user->getEmail(),0);
        // $this->accountRepository->emailUpgrade($user->getName(),$updated_plan->getName(),$old_plan->getName(),$user->getEmail());

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.subscription_upgrade_success');
    }

    public function unsubscribe($user_id,$subscription_id) {}

    public function changeDistributor($user_id,$distributor_id,$subscription_id)
    {

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));

        $distributor = $this->schoolRepository->getDistributorById($distributor_id);
        if($distributor== null) throw new BadRequestException(trans('locale.distributor_not_exist'));

        $account = $user->getAccount();

        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        if(!$this->accountRepository->isAuthorized('change_distributor',$user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $subscription = $this->accountRepository->getSubscriptionById($subscription_id);
        if($subscription == null)
            throw new BadRequestException(trans('locale.subscription_not_exist'));

        $distributor_subscription= $subscription->getDistributorSubscriptions()->first();

        $distributor_subscription->setDistributor($distributor);

        $this->schoolRepository->storeDistributorSubscription($distributor_subscription);

        return trans('locale.distributor_change_success');

    }

    public function updateSchoolInfo($user_id,SchoolDto $schoolDto){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        // give the teacher the permission to update school info
//        if(!$this->accountRepository->isAuthorized('update_SchoolInfo',$user))
//            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($schoolDto->country_code !== null && $schoolDto->country_code !== ""){
            $country = $this->schoolRepository->getCountryByCode($schoolDto->country_code);
            if($country== null) throw new BadRequestException(trans("locale.country_not_exist"));
            $school->setCountry($country);
        }

        $school->setEmail($schoolDto->email);
        $school->setName($schoolDto->name);
        $school->setAddress($schoolDto->address);
        $school->setWebsite($schoolDto->website);
        $school->setSchoolLogoUrl($schoolDto->schoolLogoUrl);
        $school->setColorPaletteId($schoolDto->colorPaletteId);

        $this->schoolRepository->addSchool($school);
        return trans('locale.school_update_successfull');
    }

    public function updateAdminInfo($user_id,UserDto $userDto){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('update_AdminInfo',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($user->getUsername() != $userDto->username){
            $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
            if($existingUsername != null)
                throw new BadRequestException(trans('locale.username_exists'));

        }

        if($user->getEmail() != $userDto->email){
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if($existingUserMail != null)
                throw new BadRequestException(trans('locale.user_email_exists'));
        }

        $user->setFirstName($userDto->fname);
        $user->setLastName($userDto->lname);
        $user->setUsername($userDto->username);
        $user->setEmail($userDto->email);
        if($userDto->password != null)
            $user->setpassword(bcrypt($userDto->password));

        $this->accountRepository->storeUser($user);
        return UserDtoMapper::CustomerMapper($user);


    }

    public function getSchoolInfo($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

//        if(!$this->accountRepository->isAuthorized('update_SchoolInfo',$user))
//            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        
        if($school->getCountry() != null){
            $schoolMapper = Mapper::MapClass(SchoolDto::class,$school,[CountryDto::class]);
            $schoolMapper->profileImageUrl = $user->getImage();
            return $schoolMapper;
        }

        $schoolMapper = Mapper::MapClass(SchoolDto::class, $school);
        $schoolMapper->profileImageUrl = $user->getImage();
        $schoolMapper->country = ['Code' => null, 'Name' => null, 'DistributorsCount' => 0, 'Journey' => null];
        
        return $schoolMapper;
    }

    public function getAllGrades($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_SchoolGrades',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $grades = $school->getGrades();

        return Mapper::MapEntityCollection(GradeDto::class,$grades);
    }

    public function generalStatisticsAdmin($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.subscription_perios_ended"));

        $subscription = $subscriptions->last();
        $plan_info = $subscription->getPlan()->getSchoolInfo();
        if($plan_info == null)
        {
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription == null){
                $schoolCharge = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                    ->first()->getSchoolCharge();
                if($schoolCharge == null){
                    $distibutorSubscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                        ->first()->getDistributorSubscriptions()->first();
                    if($distibutorSubscription == null)
                        throw new BadRequestException(trans("locale.school_plan_information_missing"));
                }
            }
            else{
                $plan_info = $invitationSubscription->getInvitation()->getSchoolInfo();
                if($plan_info == null)
                    throw new BadRequestException(trans("locale.school_plan_information_missing"));
            }

        }

        $noOfStudents = $this->schoolRepository->getEnrolledStudent($account->getId(), true);
//        $noOfStudents = $this->accountRepository->getNumberOfUsersWithRole($account,'student');
        $noOfTeachers = $this->accountRepository->getNumberOfUsersWithRole($account,'teacher');
        if($plan_info != null){
            $maxNoOfClassroomJourneys = $plan_info->getMaxNumberOfCourses();
            $totalStudents = $plan_info->getMaxNumberOfStudents();
        }

        else{
            $maxs = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId());
            $totalStudents = $maxs["maxStudents"];
            $maxNoOfClassroomJourneys = $maxs["maxClassrooms"];
        }

        $classrooms = $school->getClassrooms();
        $noClassrooms = count($classrooms);

        $noOfClassroomJourneys = 0;
        foreach ($classrooms as $classroom){
            $noOfClassroomJourneys += count($classroom->getJourneys());
        }

        if($maxNoOfClassroomJourneys == null)
            $maxNoOfClassroomJourneys = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxClassrooms"];

        $remainingJourneysPercentage = round((($maxNoOfClassroomJourneys-$noOfClassroomJourneys)/$maxNoOfClassroomJourneys)*100,2);
        return ["no_students"=>$noOfStudents,"no_classrooms"=>$noClassrooms,"no_teachers"=>$noOfTeachers,"journeys_percentage"=>(100-$remainingJourneysPercentage).'%',"total_students"=>$totalStudents];
    }

    public function getTopStudents($user_id,$grade_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $grade = $this->schoolRepository->getGradeById($grade_id);
        if($grade == null)
            throw new BadRequestException(trans("locale.grade_doesn't_exist"));

        if($grade ->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.grade_no_in_school"));
        $data = array();
        $queryData = $this->schoolRepository->getTopStudents($account,$grade);

        foreach ($queryData as $student){
            $studentx = $this->accountRepository->getUserById($student->id);
            if($student->score  != 0 && $student->classroomname != null)
                array_push($data,["name"=>$student->first_name.' '.$student->last_name,"classroom"=>$student->classroomname,"coins"=>$student->coins,"score"=>$student->score,"progress"=> ceil($studentx->getProgressPercentage())."%"]);
        }

        return $data;
    }

    public function getAdminClassroomStatistics($user_id,$grade_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if(!$user->is_schoolAdmin())
            throw new BadRequestException("User isn't school admin");
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $grade = $this->schoolRepository->getGradeById($grade_id);
        if($grade == null)
            throw new BadRequestException(trans("locale.grade_doesn't_exist"));

        if($grade ->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.grade_no_in_school"));

        $classrooms = $school->getClassroomsInGrade($grade_id);
        $classrooms_data = [];
        foreach ($classrooms as $classroom) {
            $classrooms_data[] = ["classroom_name" => $classroom->getName(), "total_score" => $classroom->getTotalScore()];
        }
        return $classrooms_data;
    }

    public function getAdminActivityEvents($user_id,$from_time,$to_time){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $coursesCreatedActivity = $this->schoolRepository->getCourseCreatedInRange($school,$from_time,$to_time);
        $coursesEndedActivity = $this->schoolRepository->getCourseEndedInRange($school,$from_time,$to_time);
        $coursesFinishedByStudents = $this->schoolRepository->getFinishedCourseByAllStudentsInSchool($school);
        $eventsData = array();
        foreach ($coursesCreatedActivity as $courseCreatedActivity){
            array_push($eventsData,['flag' => 0,
                'teacher_name' => $courseCreatedActivity->getCreator() ? $courseCreatedActivity->getCreator()->getName() : null
                ,'date' => Carbon::parse($courseCreatedActivity->getCreatedAt())->format('Y-m-d')
                ,'course_name' => $courseCreatedActivity->getName()
                ,'classroom_name' => $courseCreatedActivity->getClassroom()->getName()]);
        }
        foreach($coursesEndedActivity as $courseEndedActivity){
            array_push($eventsData,['flag'=>1,'classroom_name'=>$courseEndedActivity->getClassroom()->getName()
                ,'course_name'=>$courseEndedActivity->getName(),'date'=>Carbon::parse($courseEndedActivity->getEndsAt())->format('Y-m-d')]);
        }
        foreach ($coursesFinishedByStudents as $courseFinishedByStudents){
            if(($courseFinishedByStudents->studentFinishCourse == $courseFinishedByStudents->studentsInCourse) && ($courseFinishedByStudents->studentsInCourse != 0)){
                $course = $this->schoolRepository->getCourseById($courseFinishedByStudents->course_id);
                array_push($eventsData,['flag'=>2,'classroom_name'=>$course->getClassroom()->getName()
                    ,'course_name'=>$course->getName(),'date'=>Carbon::parse($courseFinishedByStudents->time)->format('Y-m-d')]);
            }
        }

        if(sizeOf($eventsData) == 0){
            return [];
        }

        foreach ($eventsData as $key => $part) {
            $sort[$key] = strtotime($part['date']);
        }
        array_multisort($sort, SORT_DESC, $eventsData);
        $end_date = $eventsData[0]['date'];
        $start_date = Carbon::parse($end_date)->subMonth();
        return $this->extractBydate($eventsData,$start_date,$end_date);
        //return $eventsData;

    }

    public function date_compare($a, $b)
    {
        $t1 = strtotime($a['date']);
        $t2 = strtotime($b['date']);
        return $t1 - $t2;
    }

    public function getAdminMissionsChart($user_id,$from_time,$to_time){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $noOfMissionsOpened = $this->schoolRepository->getStudentMissionClickByTime($account,$from_time,$to_time);
        $noOfMissionsSolved = $this->schoolRepository->getStudentSolvedMissions($account,$from_time,$to_time);
        //dd($noOfMissionsOpened);
        $startDate = Carbon::parse($from_time);

        $endDate = Carbon::parse($to_time);
        $data = array();

        for($date = $startDate; $date->lte($endDate); $date->addDay()) {
            $noOfMissionsOpenedPerDayNumber = 0;
            $noOfMissionsSolvedPerDayNumber = 0;
            $currentDate = $date->format('Y-m-d');
            foreach ($noOfMissionsOpened as $noOfMissionsOpenedPerDay){
                if($noOfMissionsOpenedPerDay->time == $currentDate)
                    $noOfMissionsOpenedPerDayNumber = $noOfMissionsOpenedPerDay->number;
            }

            foreach ($noOfMissionsSolved as $noOfMissionsSolvedPerDay) {
                if($noOfMissionsSolvedPerDay->time == $currentDate)
                    $noOfMissionsSolvedPerDayNumber = $noOfMissionsSolvedPerDay->number;
            }
            array_push($data,array('date'=>$currentDate,'no_students_open_mission_page'=>$noOfMissionsOpenedPerDayNumber,'no_missions_solved'=>$noOfMissionsSolvedPerDayNumber));
        }
        return $data;
    }

    public function getClassroomsForAdmin($user_id,$grade_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllClassroomsAdmin',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        $grade = null;

        if($grade_id != null){
            $grade = $this->schoolRepository->getGradeById($grade_id);
            if($grade == null)
                throw  new BadRequestException(trans('locale.grade_name_not_in_school'));
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans('locale.grade_not_in_school'));
        }

        $classrooms = $this->schoolRepository->getClassroomsForAdmin($school,$grade);
        return Mapper::MapEntityCollection(ClassroomNameDto::class,$classrooms);
    }

    public function extractBydate($eventsData,$start_date,$end_date)
    {
        $events = [];
        foreach ($eventsData as $event) {
            if($event['date'] >= $start_date && $event['date'] <= $end_date)
                $events[] = $event;
        }
        return $events;
    }

    public function registerInvitedSchool(UserDto $userDto, SchoolDto $schoolDto, $invitation_code)
    {
        $invitation = $this->accountRepository->getInvitationByCode($invitation_code);
        if($invitation == null)
            throw new BadRequestException(trans("locale.invitation_not_exist"));

        $subscriptions = $invitation->getSubscriptions();

        if(count($subscriptions) == $invitation->getNumberOfUsers())
            throw new BadRequestException(trans("locale.invitation_reached_max_users"));

        $this->accountRepository->beginDatabaseTransaction();

        $userDto->accounttype = "School";
        $userDto->role = "school_admin";

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);
        if($userDto->social_id != null)
        {
            $status = $this->accountRepository->getStatusByName('active');
        }
        else
            $status = $this->accountRepository->getStatusByName('active');
        if($status == null)
            throw new BadRequestException(trans("locale.status_not_exist"));
        $account = new Account($userDto, $account_type,$status);
        $user = new User($userDto, $account);

        //Creating a new role for this user
        $role = $this->accountRepository->getRoleByName($userDto->role);
        //$user_role = new UserRole($role);
        $user->addRole($role);
        $this->accountRepository->addUser($user);

        //$user has a user role now

        //add school
        $country = $this->schoolRepository->getCountryByCode($schoolDto->country_code);
        if($country== null) throw new BadRequestException(trans("locale.country_not_exist"));

        $school = new School($account,$schoolDto);
        $school->setCountry($country);
        $this->schoolRepository->addSchool($school);
        $this->schoolRepository->addDefaultGrades($school);
        $distributor = $this->schoolRepository->getDefaultDistributors()->first();
        if($distributor== null) throw new BadRequestException(trans("locale.distributor_not_exist"));

        // get plan from name
        $plan = $this->accountRepository->getPlanByName('Invitation_school');
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        // get the last updated version of plan from plan history
        $updated_plan = $this->accountRepository->getlastupdated($plan,$account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        // check if user have already subscribed to this plan before
        if($this->accountRepository->checkSubscription($updated_plan,$account))
            throw new BadRequestException(trans("locale.subscribed_plan"));

        $old_subscription = $account->getSubscriptions()->first();

        if($old_subscription != null)
        {
            $old_subscription->setEndDate(Carbon::now());

            $this->accountRepository->storeSubscription($old_subscription);
        }

        // create a new subsciption dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = $invitation->getEndDate();

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto,$updated_plan,$account);

        $invitation_subscription = new InvitationSubscription($subscription,$invitation);

        $subscription->addInvitationSubscription($invitation_subscription);

        $current_code = $distributor->getId().Carbon::parse(Carbon::now())->timestamp.str_random(30);

        // $current_code = Hash::make($current_code_string);

        $upcoming_code= $distributor->getId().Carbon::parse(Carbon::now()->addYears(1))->timestamp.str_random(30);

        // $upcoming_code = Hash::make($upcoming_code_string);

        $distributor_subscription = new DistributorSubscription($subscription,$distributor,$current_code,$upcoming_code);

        $subscription->addDistributorSubscription($distributor_subscription);

        $account->addSubscription($subscription);

        // passing the subscription instance to subscription repository to be saved
        $this->accountRepository->storeAccount($account);

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.school_account_created");

    }

    public function AddUser(UserDto $userDto){
        $this->accountRepository->beginDatabaseTransaction();
        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if ($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if ($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));

        $account_type = $this->accountRepository->getAccountTypeByName($userDto->accounttype);
        $status = $this->accountRepository->getStatusByName('wait_activation');
        if($status == null)
            throw new BadRequestException(trans('locale.status_not_exist'));

        $account = new Account($userDto, $account_type, $status);
        //$account is added for that user

        //Creating a new user for this account
        $user = new User($userDto, $account);

        //Creating a new role for this user
        $role = $this->accountRepository->getRoleByName($userDto->role);
        //$user_role = new UserRole($role);
        $user->addRole($role);
        //$user has a user role now

        //Store Account, User and User Role in database
        $this->accountRepository->AddUser($user);
        //User is stored now with his role and account

        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = null;

        $plan = $this->accountRepository->getPlanByName($userDto->plan);
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        $updated_plan = $this->accountRepository->getlastupdated($plan, $account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        $subscription = new Subscription($subscriptionDto, $updated_plan, $account);
        $this->accountRepository->storeSubscription($subscription);

        if($userDto->accounttype == 'School'){
            $schoolDto = new SchoolDto;
            $schoolDto->name = $userDto->school_name;
            $schoolDto->schoolLogoUrl = "https://s3-us-west-2.amazonaws.com/robogarden-professional/robogarden_logo.png";
            $colorPalette = $this->accountRepository->getColorPalette(1);
            
            $school = new School($account, $colorPalette,$schoolDto);
            $this->schoolRepository->addSchool($school);
            $this->schoolRepository->addDefaultGrades($school);
        }
        $event = new UserCreatedHandle();
        $school_name = (($userDto->school_name != null && !empty($userDto->school_name)) ? $userDto->school_name : $user->getUsername());
        $event->sendEmail($account, $user, $school_name);

        $this->accountRepository->commitDatabaseTransaction();
        return UserDtoMapper::CustomerMapper($user);
    }

    public function subscribeCustom(CustomPlanDto $customDto){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($customDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        //check account credit card
        if($customDto->stripe_token != null){
            $account = $this->addCreditCardToken($account, $customDto->stripe_token);
        }
        else {
            if ($account->getStripeId() == null) {
                throw new BadRequestException(trans("locale.payment_info_required"));
            }
        }

        //remove active free subscription
        $freeSub = $this->accountRepository->getActiveFreeSubscription($account->getId());
        if($freeSub != null){
            $freeSub->setEndDate(Carbon::now());
            $this->accountRepository->storeSubscription($freeSub);
        }

        //store account
        $this->accountRepository->storeAccount($account);

        //check account active subscriptions
        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());

        if(count($activeSubscriptions) > 0)
            throw new BadRequestException(trans("locale.there_is_already_an_active_subscription"));

        //get current no of classroom journeys and students
        $noOfClassroomJourneysUsed = $account->getSchool()->getJourneysCount();
        $noOfStudents = $this->schoolRepository->getAllStudents($account->getId(),null,null,null, true);

        $plan = $this->accountRepository->getPlanByName('Custom');
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        // get the last updated version of plan from plan history
        $updated_plan = $this->accountRepository->getlastupdated($plan, $account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        $tax = $this->accountRepository->getTax($account);
        $school_name = $account->getSchool()->getName() ? $account->getSchool()->getName() : $user->getUsername();

        //check bundle if exist
        if($customDto->bundle_id != null){
            $bundle = $this->schoolRepository->getBundleById($customDto->bundle_id);
            if($bundle == null)
                throw new BadRequestException(trans("locale.bundle_not_exist"));

            //check bundle no of students and no of classroom journeys
            if($bundle->getNoStudents() < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));

            if($bundle->getNoClassroom() < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            //create chargeDto
            $from = Carbon::now()->startOfDay();
            $to = Carbon::now()->addYear()->startOfDay();
            $chargeDto = new ChargeDto();
            $chargeDto->customer_id = $account->getStripeId();
            $chargeDto->amount = ($bundle->getPrice() - ($bundle->getPrice()*$bundle->getDiscount()))*(1+$tax);
            $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

            // charge for custom plan
            $charge = $this->schoolRepository->handleStripeCharge($chargeDto);
            if($charge->failure_code != null)
                throw new BadRequestException($charge->failure_message);

            if($bundle->getName() == "Bronze" || $bundle->getName() == "Silver" || $bundle->getName() == "Golden" )
                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $chargeDto->amount,$bundle->getName()." plan",$bundle->getDiscount(),$from,$to,$tax));
            else
                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $chargeDto->amount,$bundle->getName()." bundle",$bundle->getDiscount(),$from,$to,$tax));

            // create a new subscription dto to be passed to subscription class
            $subscriptionDto = new SubscriptionDto;
            $subscriptionDto->StartDate = $from;
            $subscriptionDto->EndDate = $to;

            // create a new subscription instance
            $subscription = new Subscription($subscriptionDto, $updated_plan, $account);
            $schoolCharge = new SchoolCharge($subscription, $charge->id, $chargeDto->amount, $bundle->getNoStudents(), $bundle->getNoClassroom(), $chargeDto->description);
            if($bundle->getName() == "Bronze" || $bundle->getName() == "Silver" || $bundle->getName() == "Golden" )
                $schoolCharge->setBundle($bundle);

            if($customDto->plan_name == null)
                $customDto->plan_name = $bundle->getName();

            $schoolChargeDetails = new SchoolChargeDetails($schoolCharge, $bundle->getNoStudents(), $bundle->getNoClassroom(),$customDto->plan_name);

            // store everything
            $this->accountRepository->storeSubscription($subscription);
            $this->schoolRepository->storeSchoolCharge($schoolCharge);
            $this->schoolRepository->storeSchoolChargeDetails($schoolChargeDetails);

            // send mail
            $this->schoolRepository->sendCustomSubscriptionMail($school_name, $user->getEmail(), $bundle->getName(), $bundle->getNoStudents(), $bundle->getNoClassroom(), false);
        }
        //check if students and classrooms exist
        else if ($customDto->students != null && $customDto->classrooms != null)
        {
            $plans = $this->getAllPlanNames();
            if(isset($customDto->plan_name) && !empty($customDto->plan_name) && in_array($customDto->plan_name, $plans))
                throw new BadRequestException(trans("locale.plan_in_static"));

            if($customDto->plan_name == null)
                $customDto->plan_name = $updated_plan->getName();

            $from  = Carbon::now()->startOfDay();
            $to = Carbon::now()->addYear()->startOfDay();

            if($customDto->students < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
            if($customDto->classrooms < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            if($customDto->students % 10 != 0 || $customDto->students < 10){
                throw new BadRequestException(trans("locale.students_no_error"));
            }
            if($customDto->classrooms < 1){
                throw new BadRequestException(trans("locale.classrooms_no_error"));
            }
            $cost = 0;

            if($customDto->students > 0){
                $studentMainPrice = $this->schoolRepository->getSchoolPlanComponentCost('student', 10);
                if($studentMainPrice == null)
                    throw new BadRequestException(trans("locale.student_main_component_not_found"));

                $nearestDiscount = $this->schoolRepository->getNearestDiscount('student',$customDto->students);

                $multiplier = $customDto->students/10;
                $cost += (($multiplier*$studentMainPrice->getPrice()) - ($multiplier*$studentMainPrice->getPrice()*$nearestDiscount))*(1+$tax);
                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $multiplier*$studentMainPrice->getPrice(),$customDto->students." students ", $nearestDiscount, $from, $to,$tax));
            }
            if($customDto->classrooms > 0){
                $classroomMainPrice = $this->schoolRepository->getSchoolPlanComponentCost('classroom', 1);
                if($classroomMainPrice == null)
                    throw new BadRequestException(trans("locale.classroom_journey_main_component_not_found"));

                $nearestDiscount = $this->schoolRepository->getNearestDiscount('classroom',$customDto->classrooms);
                $cost += (($customDto->classrooms*$classroomMainPrice->getPrice()) - ($customDto->classrooms*$classroomMainPrice->getPrice()*$nearestDiscount))*(1+$tax);
                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $customDto->classrooms*$classroomMainPrice->getPrice(),$customDto->classrooms." classroom journeys ",$nearestDiscount,$from,$to,$tax));
            }

            //create chargeDto
            $chargeDto = new ChargeDto();
            $chargeDto->customer_id = $account->getStripeId();
            $chargeDto->amount = $cost;
            $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

            // charge for custom plan
            $charge = $this->schoolRepository->handleStripeCharge($chargeDto);
            if($charge->failure_code != null)
                throw new BadRequestException($charge->failure_message);


            // create a new subscription dto to be passed to subscription class
            $subscriptionDto = new SubscriptionDto;
            $subscriptionDto->StartDate = $from;
            $subscriptionDto->EndDate =  $to;

            // create a new subscription instance
            $subscription = new Subscription($subscriptionDto, $updated_plan, $account);
            $schoolCharge = new SchoolCharge($subscription, $charge->id, $chargeDto->amount, $customDto->students, $customDto->classrooms, $chargeDto->description);
            $schoolChargeDetails = new SchoolChargeDetails($schoolCharge, $customDto->students, $customDto->classrooms,$customDto->plan_name);

            //store everything
            $this->accountRepository->storeSubscription($subscription);
            $this->schoolRepository->storeSchoolCharge($schoolCharge);
            $this->schoolRepository->storeSchoolChargeDetails($schoolChargeDetails);

            // send mail
            $this->schoolRepository->sendCustomSubscriptionMail($school_name, $user->getEmail(), $customDto->plan_name, $customDto->students, $customDto->classrooms, false);
        }
        else {
            throw new BadRequestException(trans("locale.no_or_bundle_required"));
        }

        // close old notification and assign new one
        $this->markUnsubscribeNotificationsAsRead($user);
        $dto = new NotificationDto();
        $dto->StartDate = Carbon::parse($to)->subDays(7);
        $dto->EndDate = $to;
        $dto->Type = "warning";
        $notification = new Notification($dto, $user);
        $translations = $this->accountRepository->translateNotification('unsubscription', 'account_expire_at');
        foreach ($translations as $translation) {
            $dto->Title = $translation['title'];
            $dto->Message = $translation['message'] . " " . $to;
            $notification_translation = new NotificationTranslation($dto, $translation['language'], $notification);
            $notification->addTranslation($notification_translation);
        }
        $user->addNotification($notification);
        $this->accountRepository->storeUser($user);

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.school_account_created_stripe");
    }

    public function buildUpgradeCustomReceipt(CustomPlanDto $customDto){
        $user = $this->accountRepository->getUserById($customDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        //check account active subscriptions
        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        if(count($activeSubscriptions) == 0)
            throw new BadRequestException(trans('locale.subscribe_first'));

        if($activeSubscriptions->first()->getDistributorSubscriptions()->first() != null)
            throw new BadRequestException(trans("locale.cant_change_from_disributor_to_stripe"));

        $schoolCharge = $activeSubscriptions->first()->getSchoolCharge();

        if($schoolCharge == null)
            throw new BadRequestException(trans("locale.cant_upgrade_no_stripe"));

        //get current no of classroom journeys and students
        $noOfClassroomJourneysUsed = $account->getSchool()->getJourneysCount();
        $noOfStudents = $this->schoolRepository->getAllStudents($account->getId(),null,null,null,true);

        $tax = $this->accountRepository->getTax($account);
        $charges = [];
        //check bundle if exist
        if($customDto->bundle_id != null){
            $bundle = $this->schoolRepository->getBundleById($customDto->bundle_id);
            if($bundle == null)
                throw new BadRequestException(trans("locale.bundle_not_exist"));

            if($bundle->getName() == "Bronze" || $bundle->getName() == "Silver" || $bundle->getName() == "Golden"){
                $from = Carbon::now()->startOfDay();
                $to = Carbon::parse($activeSubscriptions->first()->getEndDate())->startOfDay();
                $differanceInDays = $to->diffInDays($from);

                if($differanceInDays <= 0)
                    throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

                if($bundle->getNoStudents() < $noOfStudents)
                    throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
                if($bundle->getNoClassroom() < $noOfClassroomJourneysUsed)
                    throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

                $factor = $differanceInDays/($to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay()));

                //payed Money
                $chargeDto = new ChargeDto();
                $chargeDto->customer_id = $account->getStripeId();
                $chargeDto->amount = (($bundle->getPrice() - $bundle->getPrice()*$bundle->getDiscount())*$factor);
                $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

                //RETURN MONEY
                $returnCost = 0;
                $satisfied = false;

                $latestBundleChargeUpgrade = $this->schoolRepository->getLatestBundleUpgrade($schoolCharge);
                if($latestBundleChargeUpgrade == null){
                    $differanceInDaysDivideTotal = $to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay());
                    $returnIntialCharge = $schoolCharge->getCost()*($differanceInDays/$differanceInDaysDivideTotal)*(1-$tax);
                    if($returnIntialCharge >= $chargeDto->amount){
                        $satisfied = true;
                        $returnIntialCharge = $chargeDto->amount;
                    }
                    array_push($charges,['return' => true, 'cost'=> ceil($returnIntialCharge), 'from' => $from, 'to' => $to,
                        'discount' => 0,
                        'description' => "Unused time for ".$schoolCharge->getNoStudents()." student and ".$schoolCharge->getNoClassroom()." classroom journey ",
                        'tax' => 0 ]);

                    $returnCost += $returnIntialCharge;
                    $charge = null;
                    if(!$satisfied){
                        $schoolChargeUpgrades = $schoolCharge->getChargeUpgrades();
                        foreach ($schoolChargeUpgrades as $schoolChargeUpgrade){
                            if(!$satisfied){
                                $differanceInDaysDivideTotal = $to->diffInDays(Carbon::parse($schoolChargeUpgrade->getStartDate())->startOfDay());
                                $return  = $schoolChargeUpgrade->getCost()*($differanceInDays/$differanceInDaysDivideTotal)*(1-$tax);
                                if($returnCost + $return >= $chargeDto->amount){
                                    $satisfied = true;
                                    $return = $chargeDto->amount -$returnCost;
                                }
                                array_push($charges,['return' => true, 'cost' => ceil($return), 'from' => $from, 'to' => $to,
                                    'discount' => 0,
                                    'description' => "Unused time for ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",
                                    'tax' => 0]);
                                $returnCost += $return;
                            }
                            else{
                                array_push($charges,['return' => true, 'cost' => 0, 'from' => $from, 'to' => $to,
                                    'discount' => 0,
                                    'description' => "Unused time for ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",
                                    'tax' => 0]);
                            }
                        }
                    }

                    $chargeDto->amount = ($chargeDto->amount - $returnCost)*(1+$tax);
                    array_push($charges,['return' => false, 'cost' => ceil($chargeDto->amount), 'from' => $from, 'to' => $to,
                        'discount' => $bundle->getDiscount(),
                        'description' => $bundle->getName()." plan",
                        'tax' => $tax]);
                }
                else{
                    $schoolChargeUpgrades = $this->schoolRepository->getAllUpgradesAfter($latestBundleChargeUpgrade->getId(),$schoolCharge);
                    foreach ($schoolChargeUpgrades as $schoolChargeUpgrade){
                        if(!$satisfied){
                            $differanceInDaysDivideTotal = $to->diffInDays(Carbon::parse($schoolChargeUpgrade->getStartDate())->startOfDay());
                            $return  = $schoolChargeUpgrade->getCost()*($differanceInDays/$differanceInDaysDivideTotal)*(1-$tax);
                            if($returnCost + $return >= $chargeDto->amount){
                                $satisfied = true;
                                $return = $chargeDto->amount -$returnCost;
                            }
                            array_push($charges,['return' => true, 'cost' => ceil($return), 'from'=>$from,'to'=>$to,
                                'discount' => 0,
                                'description' => "Unused time for ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",
                                'tax' => 0]);
                            $returnCost += $return;
                        }
                        else{
                            array_push($charges,['return' => true,'cost' => 0,'from' => $from, 'to' => $to,
                                'discount'=>0,
                                'description'=> "Unused time for ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",
                                'tax'=>0]);
                        }

                    }
                    $chargeDto->amount = ($chargeDto->amount - $returnCost)*(1+$tax);
                    array_push($charges,['return' => false,'cost' => ceil($chargeDto->amount), 'from' => $from, 'to' => $to,
                        'discount'=>$bundle->getDiscount(),
                        'description'=> $bundle->getName()." plan",
                        'tax'=>$tax]);
                }
            }
            else{
                $from = Carbon::now()->startOfDay();
                $to = Carbon::parse($activeSubscriptions->first()->getEndDate())->startOfDay();
                $differanceInDays = $to->diffInDays($from);

                if($differanceInDays <= 0)
                    throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

                $factor = $differanceInDays/($to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay()));

                if($bundle->getNoStudents()+$schoolCharge->getChargeDetail()->getNoStudents() < $noOfStudents)
                    throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
                if($bundle->getNoClassroom()+$schoolCharge->getChargeDetail()->getNoClassroom() < $noOfClassroomJourneysUsed)
                    throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));


                //create chargeDto
                $chargeDto = new ChargeDto();
                $chargeDto->customer_id = $account->getStripeId();
                $chargeDto->amount = ($bundle->getPrice() - ($bundle->getPrice()*$bundle->getDiscount())*$factor)*(1+$tax);
                $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

                array_push($charges,['return'=>false,'cost'=>ceil($chargeDto->amount),'from'=>$from,'to'=>$to,
                    'discount'=>$bundle->getDiscount(),'description'=>$bundle->getName()." bundle",'tax'=>$tax]);

            }
        }
        else if ($customDto->students  != null && $customDto->classrooms != null){
            $from = Carbon::now()->startOfDay();
            $to = Carbon::parse($activeSubscriptions->first()->getEndDate());
            $differanceInDays = $to->diffInDays($from);

            if($differanceInDays <= 0)
                throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

            $factor = $differanceInDays/($to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay()));


            if($customDto->students+$schoolCharge->getChargeDetail()->getNoStudents() < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
            if($customDto->classrooms+$schoolCharge->getChargeDetail()->getNoClassroom() < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            if($customDto->students % 10 != 0 || $customDto->students < 10){
                throw new BadRequestException(trans("locale.students_no_error"));
            }
            if($customDto->classrooms < 1){
                throw new BadRequestException(trans("locale.classrooms_no_error"));
            }
            $cost = 0;

            if($customDto->students > 0) {
                $studentMainPrice = $this->schoolRepository->getSchoolPlanComponentCost('student', 10);
                if ($studentMainPrice == null)
                    throw new BadRequestException(trans("locale.student_main_component_not_found"));

                $nearestDiscount = $this->schoolRepository->getNearestDiscount('student', $customDto->students);

                $multiplier = $customDto->students / 10;
                $cost += ((($multiplier * $studentMainPrice->getPrice()) - ($multiplier * $studentMainPrice->getPrice() * $nearestDiscount)) * $factor) * (1 + $tax);
                array_push($charges, ['return'=>false,'cost' => ceil(((($multiplier * $studentMainPrice->getPrice()) - ($multiplier * $studentMainPrice->getPrice() * $nearestDiscount)) * $factor) * (1 + $tax)),
                    'from' => $from, 'to' => $to, 'discount' => $nearestDiscount, 'description' => $customDto->students . " students ", 'tax' => $tax]);
            }
            if($customDto->classrooms > 0){
                $classroomMainPrice = $this->schoolRepository->getSchoolPlanComponentCost('classroom', 1);
                if($classroomMainPrice == null)
                    throw new BadRequestException(trans("locale.classroom_journey_main_component_not_found"));

                $nearestDiscount = $this->schoolRepository->getNearestDiscount('classroom',$customDto->classrooms);
                $cost += ((($customDto->classrooms*$classroomMainPrice->getPrice()) - ($customDto->classrooms*$classroomMainPrice->getPrice()*$nearestDiscount))*$factor)*(1+$tax);
                array_push($charges,['return' => false, 'cost' => ceil(((($customDto->classrooms * $classroomMainPrice->getPrice()) - ($customDto->classrooms * $classroomMainPrice->getPrice() * $nearestDiscount)) * $factor) * (1+$tax)),
                    'from' => $from,'to' => $to, 'discount' => $nearestDiscount, 'description' => $customDto->classrooms." classroom journeys ", 'tax' => $tax]);
            }
        }
        else{
            throw new BadRequestException(trans("locale.no_or_bundle_required"));
        }

        return $charges;
    }

    public function upgradeCustom(CustomPlanDto $customDto){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($customDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        //check account active subscriptions
        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        if(count($activeSubscriptions) == 0)
            throw new BadRequestException(trans('locale.subscribe_first'));

        if($activeSubscriptions->first()->getDistributorSubscriptions()->first() != null)
            throw new BadRequestException(trans("locale.cant_change_from_disributor_to_stripe"));

        //check account credit card
        if($customDto->stripe_token != null){
            $account = $this->updateCreditCardToken($account, $customDto->stripe_token);
        }
        else {
            if ($account->getStripeId() == null) {
                throw new BadRequestException(trans("locale.payment_info_required"));
            }
        }
        $this->accountRepository->storeAccount($account);

        $schoolCharge = $activeSubscriptions->first()->getSchoolCharge();

        if($schoolCharge == null)
            throw new BadRequestException(trans("locale.cant_upgrade_no_stripe"));

        //get current no of classroom journeys and students
        $noOfClassroomJourneysUsed = $account->getSchool()->getJourneysCount();
        $noOfStudents = $this->schoolRepository->getAllStudents($account->getId(),null,null,null,true);

        $tax = $this->accountRepository->getTax($account);
        $school_name = $account->getSchool()->getName() ? $account->getSchool()->getName() : $user->getUsername();

        //check bundle if exist
        if($customDto->bundle_id != null){
            $bundle = $this->schoolRepository->getBundleById($customDto->bundle_id);
            if($bundle == null)
                throw new BadRequestException(trans("locale.bundle_not_exist"));

            if($bundle->getName() == "Bronze" || $bundle->getName() == "Silver" || $bundle->getName() == "Golden"){
                $schoolChargeDetails = $schoolCharge->getChargeDetail();
                if($schoolChargeDetails->getName() == $bundle->getName())
                    throw new BadRequestException(trans("locale.cant_upgrade_to_same_plan"));

                $from = Carbon::now()->startOfDay();
                $to = Carbon::parse($activeSubscriptions->first()->getEndDate())->startOfDay();
                $differanceInDays = $to->diffInDays($from);

                if($differanceInDays <= 0)
                    throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

                if($bundle->getNoStudents() < $noOfStudents)
                    throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));

                if($bundle->getNoClassroom() < $noOfClassroomJourneysUsed)
                    throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

                $factor = $differanceInDays/($to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay()));

                //payed Money
                $chargeDto = new ChargeDto();
                $chargeDto->customer_id = $account->getStripeId();
                $chargeDto->amount = (($bundle->getPrice() - $bundle->getPrice()*$bundle->getDiscount())*$factor);
                $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

                //RETURN MONEY
                $returnCost = 0;
                $satisfied = false;
                $charge = null;

                $latestBundleChargeUpgrade = $this->schoolRepository->getLatestBundleUpgrade($schoolCharge);
                if($latestBundleChargeUpgrade == null){
                    $differanceInDaysDivideTotal = $to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay());
                    $returnIntialCharge = $schoolCharge->getCost()*($differanceInDays/$differanceInDaysDivideTotal)*(1-$tax);
                    if($returnIntialCharge >= $chargeDto->amount){
                        $satisfied = true;
                        $returnIntialCharge = $chargeDto->amount;
                    }
                    $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $returnIntialCharge,"Unused time for  ".$schoolCharge->getNoStudents()." student and ".$schoolCharge->getNoClassroom()." classroom journey ",0,$from,$to,0,1));
                    $returnCost += $returnIntialCharge;
                    if(!$satisfied){
                        $schoolChargeUpgrades = $schoolCharge->getChargeUpgrades();
                        foreach ($schoolChargeUpgrades as $schoolChargeUpgrade){
                            if(!$satisfied){
                                $differanceInDaysDivideTotal = $to->diffInDays(Carbon::parse($schoolChargeUpgrade->getStartDate())->startOfDay());
                                $return  = $schoolChargeUpgrade->getCost()*($differanceInDays/$differanceInDaysDivideTotal)*(1-$tax);
                                if($returnCost + $return >= $chargeDto->amount){
                                    $satisfied = true;
                                    $return = $chargeDto->amount -$returnCost;
                                }
                                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $return,"Unused time for  ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",0,$from,$to,0,1));
                                $returnCost += $return;
                            }
                            else{
                                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), 0,"Unused time for  ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",0,$from,$to,0,1));
                            }
                        }
                    }

                    $chargeDto->amount = ($chargeDto->amount - $returnCost)*(1+$tax);
                    $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $chargeDto->amount,$bundle->getName()." plan",$bundle->getDiscount(),$from,$to,$tax));
                    if(!$satisfied){
                        // charge for custom plan
                        $charge = $this->schoolRepository->handleStripeCharge($chargeDto);
                    }
                }
                else{
                    $schoolChargeUpgrades = $this->schoolRepository->getAllUpgradesAfter($latestBundleChargeUpgrade->getId(), $schoolCharge);
                    foreach ($schoolChargeUpgrades as $schoolChargeUpgrade){
                        if(!$satisfied){
                            $differanceInDaysDivideTotal = $to->diffInDays(Carbon::parse($schoolChargeUpgrade->getStartDate())->startOfDay());
                            $return  = $schoolChargeUpgrade->getCost()*($differanceInDays/$differanceInDaysDivideTotal)*(1-$tax);
                            if($returnCost + $return >= $chargeDto->amount){
                                $satisfied = true;
                                $return = $chargeDto->amount -$returnCost;
                            }
                            $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $return,"Unused time for  ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",0,$from,$to,0,1));
                            $returnCost += $return;
                        }
                        else{
                            $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), 0,"Unused time for  ".$schoolChargeUpgrade->getNoStudents()." student and ".$schoolChargeUpgrade->getNoClassroom()." classroom journey ",0,$from,$to,0,1));
                        }

                    }
                    $chargeDto->amount = ($chargeDto->amount - $returnCost)*(1+$tax);
                    $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $chargeDto->amount,$bundle->getName()." plan",$bundle->getDiscount(),$from,$to,$tax));
                    if(!$satisfied){
                        // charge for custom plan
                        $charge = $this->schoolRepository->handleStripeCharge($chargeDto);
                    }
                }
                $charge_id = null;
                if($charge != null)
                    $charge_id = $charge->id;
                $schoolChargeUpgrade = new SchoolChargeUpgrade($schoolCharge,$charge_id,$chargeDto->amount,$bundle->getNoStudents(),$bundle->getNoClassroom(), $chargeDto->description);
                $schoolChargeUpgrade->setBundle($bundle);
                $schoolChargeDetails = $schoolCharge->getChargeDetail();
                $schoolChargeDetails->setNoStudents($bundle->getNoStudents());
                $schoolChargeDetails->setNoClassroom($bundle->getNoClassroom());
                $schoolChargeDetails->setName($bundle->getName());
                if($customDto->plan_name != null)
                    $schoolChargeDetails->setName($customDto->plan_name);
                $this->schoolRepository->storeSchoolChargeDetails($schoolChargeDetails);
                $this->schoolRepository->storeSchoolChargeUpgrade($schoolChargeUpgrade);

            }
            else{
                $from = Carbon::now()->startOfDay();
                $to = Carbon::parse($activeSubscriptions->first()->getEndDate())->startOfDay();
                $differanceInDays = $to->diffInDays($from);

                if($differanceInDays <= 0)
                    throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

                $factor = $differanceInDays/($to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay()));

                if($bundle->getNoStudents()+$schoolCharge->getChargeDetail()->getNoStudents() < $noOfStudents)
                    throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
                if($bundle->getNoClassroom()+$schoolCharge->getChargeDetail()->getNoClassroom() < $noOfClassroomJourneysUsed)
                    throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

                //create chargeDto
                $chargeDto = new ChargeDto();
                $chargeDto->customer_id = $account->getStripeId();
                $chargeDto->amount = ceil($bundle->getPrice() - ($bundle->getPrice()*$bundle->getDiscount())*$factor);
                $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

                // charge for custom plan
                $charge = $this->schoolRepository->handleStripeCharge($chargeDto);

                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), $chargeDto->amount,$bundle->getName()." bundle",$bundle->getDiscount(),$from,$to,$tax));
                $schoolChargeUpgrade = new SchoolChargeUpgrade($schoolCharge,$charge->id,$chargeDto->amount,$customDto->students,$customDto->classrooms, $chargeDto->description);
                $schoolChargeDetails = $schoolCharge->getChargeDetail();
                if($customDto->plan_name != null)
                    $schoolChargeDetails->setName($customDto->plan_name);
                $schoolChargeDetails->setNoStudents($schoolChargeDetails->getNoStudents()+$bundle->getNoStudents());
                $schoolChargeDetails->setNoClassroom($schoolChargeDetails->getNoClassroom()+$bundle->getNoClassroom());
                $this->schoolRepository->storeSchoolChargeDetails($schoolChargeDetails);
                $this->schoolRepository->storeSchoolChargeUpgrade($schoolChargeUpgrade);

            }
            $this->schoolRepository->sendCustomSubscriptionMail($school_name, $user->getEmail(), null, $bundle->getNoStudents(), $bundle->getNoClassroom(), true);
        }
        else if ($customDto->students  != null && $customDto->classrooms != null){
            $from = Carbon::now()->startOfDay();
            $to = Carbon::parse($activeSubscriptions->first()->getEndDate())->startOfDay();
            $differanceInDays = $to->diffInDays($from);

            if($differanceInDays <= 0)
                throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

            $factor = $differanceInDays/($to->diffInDays(Carbon::parse($activeSubscriptions->first()->getStartDate())->startOfDay()));

            if($customDto->students+$schoolCharge->getChargeDetail()->getNoStudents() < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
            if($customDto->classrooms+$schoolCharge->getChargeDetail()->getNoClassroom() < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            if($customDto->students % 10 != 0 || $customDto->students < 10){
                throw new BadRequestException(trans("locale.students_no_error"));
            }
            if($customDto->classrooms < 1){
                throw new BadRequestException(trans("locale.classrooms_no_error"));
            }
            $cost = 0;

            if($customDto->students > 0){
                $studentMainPrice = $this->schoolRepository->getSchoolPlanComponentCost('student', 10);
                if($studentMainPrice == null)
                    throw new BadRequestException(trans("locale.student_main_component_not_found"));

                $nearestDiscount = $this->schoolRepository->getNearestDiscount('student',$customDto->students);

                $multiplier = $customDto->students/10;
                $cost += ((($multiplier*$studentMainPrice->getPrice()) - ($multiplier*$studentMainPrice->getPrice()*$nearestDiscount))*$factor)*(1+$tax);
                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), ((($multiplier*$studentMainPrice->getPrice()) - ($multiplier*$studentMainPrice->getPrice()*$nearestDiscount))*$factor)*(1+$tax),$customDto->students." students ",$nearestDiscount,$from,$to,$tax));
            }
            if($customDto->classrooms > 0){
                $classroomMainPrice = $this->schoolRepository->getSchoolPlanComponentCost('classroom', 1);
                if($classroomMainPrice == null)
                    throw new BadRequestException(trans("locale.classroom_journey_main_component_not_found"));

                $nearestDiscount = $this->schoolRepository->getNearestDiscount('classroom',$customDto->classrooms);
                $cost += ((($customDto->classrooms*$classroomMainPrice->getPrice()) - ($customDto->classrooms*$classroomMainPrice->getPrice()*$nearestDiscount))*$factor)*(1+$tax);
                $this->schoolRepository->storeSchoolChargesLog(new SchoolChargesLog($account->getSchool(), ((($customDto->classrooms*$classroomMainPrice->getPrice()) - ($customDto->classrooms*$classroomMainPrice->getPrice()*$nearestDiscount))*$factor)*(1+$tax),$customDto->classrooms." classroom journeys ",$nearestDiscount,$from,$to,$tax));
            }

            //create chargeDto
            $chargeDto = new ChargeDto();
            $chargeDto->customer_id = $account->getStripeId();
            $chargeDto->amount = $cost;
            $chargeDto->description = "Customer id no " . $account->getStripeId() . " charge with amount (" . $chargeDto->amount . " usd).";

            // charge for custom plan
            $charge = $this->schoolRepository->handleStripeCharge($chargeDto);
            if($charge->failure_code != null)
                throw new BadRequestException($charge->failure_message);

            $schoolChargeUpgrade = new SchoolChargeUpgrade($schoolCharge,$charge->id,$cost,$customDto->students,$customDto->classrooms, $chargeDto->description);
            $schoolChargeDetails = $schoolCharge->getChargeDetail();
            if($customDto->plan_name != null)
                $schoolChargeDetails->setName($customDto->plan_name);
            $new_students = $schoolChargeDetails->getNoStudents()+$customDto->students;
            $new_classrooms = $schoolChargeDetails->getNoClassroom()+$customDto->classrooms;
            $schoolChargeDetails->setNoStudents($new_students);
            $schoolChargeDetails->setNoClassroom($new_classrooms);
            $this->schoolRepository->storeSchoolChargeDetails($schoolChargeDetails);
            $this->schoolRepository->storeSchoolChargeUpgrade($schoolChargeUpgrade);

            $this->schoolRepository->sendCustomSubscriptionMail($school_name, $user->getEmail(), null, $new_students, $new_classrooms, true);
        }
        else{
            throw new BadRequestException(trans("locale.no_or_bundle_required"));
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.school_upgrade_successfull");
    }

    public function subscribeDistributor(DistributorSubscriptionDto $distributorDto){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($distributorDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        //check account active subscriptions
        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        if(count($activeSubscriptions) > 0)
            throw new BadRequestException(trans("locale.there_is_already_an_active_subscription"));

        $distributor = $this->schoolRepository->getDistributorById($distributorDto->distributor_id);
        if($distributor == null)
            throw new BadRequestException(trans("locale.distributor_not_exist"));

        //remove active free subscription
//        $freeSub = $this->accountRepository->getActiveFreeSubscription($account->getId());
//        if($freeSub != null){
//            $freeSub->setEndDate(Carbon::now());
//            $this->accountRepository->storeSubscription($freeSub);
//        }

        $plan = $this->accountRepository->getPlanByName('Custom');
        if($plan == null)
            throw new BadRequestException(trans("locale.plan_not_found"));

        // get the last updated version of plan from plan history
        $updated_plan = $this->accountRepository->getlastupdated($plan, $account_type->getId());
        if($updated_plan == null)
            throw new BadRequestException(trans("locale.version_plan_not_found"));

        //get current no of classroom journeys and students
        $noOfClassroomJourneysUsed = $account->getSchool()->getJourneysCount();
        $noOfStudents = $this->schoolRepository->getAllStudents($account->getId(),null,null,null, true);

        $status = $this->accountRepository->getStatusByName('wait_subscription_approval');
        if($status == null)
            throw new BadRequestException(trans("locale.something_wrong"));

        $account->setStatus($status);

        //check bundle if exist
        if($distributorDto->bundle_id != null){
            $bundle = $this->schoolRepository->getBundleById($distributorDto->bundle_id);
            if($bundle == null)
                throw new BadRequestException(trans("locale.bundle_not_exist"));

            //check bundle no of students and no of classroom journeys
            if($bundle->getNoStudents() < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));
            if($bundle->getNoClassroom() < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            if($distributorDto->plan_name == null)
                $distributorDto->plan_name = $bundle->getName();
        }
        //check if students and classrooms exist
        else if ($distributorDto->students != null && $distributorDto->classrooms != null)
        {
            $plans = $this->getAllPlanNames();
            if(isset($distributorDto->plan_name) && !empty($distributorDto->plan_name) && in_array($distributorDto->plan_name, $plans))
                throw new BadRequestException(trans("locale.plan_in_static"));

            if($distributorDto->students < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));

            if($distributorDto->classrooms < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            if($distributorDto->students % 10 != 0 || $distributorDto->students < 10){
                throw new BadRequestException(trans("locale.students_no_error"));
            }
            if($distributorDto->classrooms < 1){
                throw new BadRequestException(trans("locale.classrooms_no_error"));
            }
            if($distributorDto->plan_name == null)
                $distributorDto->plan_name =  $updated_plan->getName();
        }
        else {
            throw new BadRequestException(trans("locale.no_or_bundle_required"));
        }

        // create a new subscription dto to be passed to subscription class
        $subscriptionDto = new SubscriptionDto;
        $subscriptionDto->StartDate = Carbon::now();
        $subscriptionDto->EndDate = Carbon::now();

        // create a new subscription instance
        $subscription = new Subscription($subscriptionDto, $updated_plan, $account);
        $this->accountRepository->storeSubscription($subscription);

        $current_code = $distributor->getId().Carbon::parse(Carbon::now())->timestamp.str_random(30);
        $upcoming_code = $distributor->getId().Carbon::parse(Carbon::now()->addYears(1))->timestamp.str_random(30);
        $distributor_subscription = new DistributorSubscription($subscription, $distributor, $current_code, $upcoming_code);
        $this->schoolRepository->storeDistributorSubscription($distributor_subscription);

        if($distributorDto->bundle_id != null){
            $distributorCharge = new DistributorCharge($distributor_subscription, null, $bundle->getNoStudents(), $bundle->getNoClassroom());
            if ($bundle->getName() == "Bronze" || $bundle->getName() == "Silver" || $bundle->getName() == "Golden")
                $distributorCharge->setBundle($bundle);

            $distributorChargeDetails = new DistributorChargeDetails($distributorCharge, $bundle->getNoStudents(), $bundle->getNoClassroom(), $bundle->getName());
            $this->schoolRepository->storeDistributorCharge($distributorCharge);
            $this->schoolRepository->storeDistributorChargeDetails($distributorChargeDetails);
        }
        else{
            $distributorCharge = new DistributorCharge($distributor_subscription, null, $distributorDto->students, $distributorDto->classrooms);
            $distributorChargeDetails = new DistributorChargeDetails($distributorCharge, $distributorDto->students, $distributorDto->classrooms, $distributorDto->plan_name);
            $this->schoolRepository->storeDistributorChargeDetails($distributorChargeDetails);
        }

        $this->accountRepository->storeAccount($account);
        $this->markUnsubscribeNotificationsAsRead($user);

        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        if($distributorDto->bundle_id != null) {
            $this->schoolRepository->emailDistributor($distributor->getName(), $bundle->getName(), $bundle->getNoStudents(), $bundle->getNoClassroom(), $distributor->getEmail(), $user->getName() != null ? $user->getName():$user->getUsername(), $user->getEmail(), $school->getName(), $distributor_subscription->getCurrentCode(), "");
            $this->schoolRepository->emailSubscription($user_name, $bundle->getName(), $bundle->getNoStudents(), $bundle->getNoClassroom(),  $user->getEmail());
        } else {
            $this->schoolRepository->emailDistributor($distributor->getName(), $updated_plan->getName(), $distributorDto->students, $distributorDto->classrooms, $distributor->getEmail(), $user->getName() != null ? $user->getName():$user->getUsername(), $user->getEmail(), $school->getName(), $distributor_subscription->getCurrentCode(), "");
            $this->schoolRepository->emailSubscription($user_name, $updated_plan->getName(), $distributorDto->students, $distributorDto->classrooms,$user->getEmail());
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.school_account_created");
    }

    public function getSchoolInvoices($user_id, $limit, $start_from){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $logs = $this->schoolRepository->getSchoolInvoicesLog($school->getId(),$limit);
        return Mapper::MapEntityCollection(SchoolChargeLogDto::class, $logs);
    }

    public function getSchoolPlansBundle(){
        $plans = $this->schoolRepository->getSchoolPlanBundles();
        return Mapper::MapEntityCollection(SchoolBundleDto::class,$plans);

    }

    public function getSchoolPrices(){
        $pricesStudent = $this->schoolRepository->getSchoolPrices("student");
        $pricesClassroom = $this->schoolRepository->getSchoolPrices("classroom");

        $schoolStudentMainPrice = 0;
        $schoolStudentMainMultipler = 0;
        $schoolClassroomMainPrice = 0;
        $schoolClassroomMainMultipler = 0;

        $studentMainPriceSet = false;
        $classroomMainPriceSet = false;


        if(count($pricesStudent) == 0 || count($pricesClassroom) == 0)
            throw  new BadRequestException(trans("locale.no_school_prices"));

        $studentPricesArray = [];
        for ($i =0;$i<count($pricesStudent);$i++){
            if(!$studentMainPriceSet){
                $schoolStudentMainPrice =$pricesStudent[$i]["price"];
                $schoolStudentMainMultipler = $pricesStudent[$i]["quantity"];
                $studentMainPriceSet = true;
            }
            if($i == (count($pricesStudent)-1)){
                array_push($studentPricesArray,['from'=>$pricesStudent[$i]["quantity"],'to'=>'infinity','discount'=>$pricesStudent[$i]["discount"]]);
            }
            else{
                array_push($studentPricesArray,['from'=>$pricesStudent[$i]["quantity"],'to'=>$pricesStudent[$i+1]["quantity"]-$schoolStudentMainMultipler,'discount'=>$pricesStudent[$i]["discount"]]);
            }
        }

        $classroomPricesArray = [];
        for ($i =0;$i<count($pricesClassroom);$i++){
            if(!$classroomMainPriceSet){
                $schoolClassroomMainPrice =$pricesClassroom[$i]["price"];
                $schoolClassroomMainMultipler = $pricesClassroom[$i]["quantity"];
                $classroomMainPriceSet = true;
            }
            if($i == (count($pricesClassroom)-1)){
                array_push($classroomPricesArray,['from'=>$pricesClassroom[$i]["quantity"],'to'=>'infinity','discount'=>$pricesClassroom[$i]["discount"]]);
            }
            else{
                array_push($classroomPricesArray,['from'=>$pricesClassroom[$i]["quantity"],'to'=>$pricesClassroom[$i+1]["quantity"]-$schoolClassroomMainMultipler,'discount'=>$pricesClassroom[$i]["discount"]]);
            }
        }

        if($schoolStudentMainPrice == 0 || $schoolStudentMainMultipler == 0 || $schoolClassroomMainPrice == 0 || $schoolClassroomMainMultipler == 0)
            throw new BadRequestException(trans("locale.main_price_component_0"));

        return ['studentMainPrice'=> $schoolStudentMainPrice,'studentMultiplier'=>$schoolStudentMainMultipler
            ,'classroomMainPrice'=>$schoolClassroomMainPrice,'classroomMultipler'=>$schoolClassroomMainMultipler
            ,'studentPrices'=>$studentPricesArray,'classroomPrices'=>$classroomPricesArray];

    }

    public function getSubscriptionDetails($user_id){

        $result = [];
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $classroomsCount = $school->getJourneysCount();
        $studentCount = $account->getCountOfStudents();

        $subscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())->first();
        if($subscription == null)
        {
            $freeSubscription = $this->accountRepository->getActiveFreeSubscription($account->getId());
            if($freeSubscription != null){
                $result['plan_name'] = "Free";
                $result['start_date'] = explode(" ",$freeSubscription->getStartDate())[0];
                $result['end_date'] = null;
                $result['remaining_time'] = null;
                $result['remaining_students'] = $freeSubscription->getPlan()->getSchoolInfo()->getMaxNumberOfStudents() - $studentCount;
                $result['total_students'] = $freeSubscription->getPlan()->getSchoolInfo()->getMaxNumberOfStudents();
                $result['remaining_classrooms'] = $freeSubscription->getPlan()->getSchoolInfo()->getMaxNumberOfCourses() - $classroomsCount;
                $result['total_classrooms'] = $freeSubscription->getPlan()->getSchoolInfo()->getMaxNumberOfCourses();
                return $result;
            }
            else
                throw new BadRequestException(trans('locale.subscription_not_exist'));
        }


        $maxs = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId());
        $diffOfStudents = $maxs['maxStudents'] - $studentCount;
        if($diffOfStudents < 0)
            $diffOfStudents = 0;

        $diffOfClassrooms = $maxs['maxClassrooms'] - $classroomsCount;
        if($diffOfClassrooms < 0)
            $diffOfClassrooms = 0;

        $distributors = $subscription->getDistributorSubscriptions();
        $invitation = $subscription->getInvitationSubscription()->first();
        if ($invitation != null){
            $planName = trans('locale.invitation');
        }
        else if(count($distributors) > 0){
            $planName = $distributors->last()->getDistributorCharge()->getChargeDetail()->getName();
        }

        else {
            $planName = $subscription->schoolCharge()->getChargeDetail()->getName();
        }

        $result['plan_name'] = $planName;
        $result['start_date'] = explode(" ", $subscription->getStartDate())[0];
        $result['end_date'] = explode(" ", $subscription->getEndDate())[0];
        $result['remaining_time'] = Carbon::parse($subscription->getEndDate())->diffInDays(Carbon::now()->startOfDay());
        $result['remaining_students'] = $diffOfStudents;
        $result['total_students'] = $maxs['maxStudents'];
        $result['remaining_classrooms'] = $diffOfClassrooms;
        $result['total_classrooms'] = $maxs['maxClassrooms'];
        return $result;
    }

    public function upgradeDistributorSubscription(CustomPlanDto $customDto){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($customDto->user->id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('upgrade_subscription',$user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        $subscription = $activeSubscriptions->first();
        if($subscription == null)
            throw new UnauthorizedException(trans('locale.subscribe_first'));

        $distributorSubscription = $subscription->getDistributorSubscriptions()->first();
        if($distributorSubscription == null)
            throw new BadRequestException(trans("locale.cant_change_from_to_stripe_disributor"));

        $distributorCharge = $distributorSubscription->getDistributorCharge();
        if($distributorCharge == null)
            throw new BadRequestException(trans("locale.cant_change_from_disributor_to_stripe"));

        $distributor = $distributorSubscription->getDistributor();
        if($distributor == null)
            throw new BadRequestException(trans("locale.distributor_not_found"));

        $status = $this->accountRepository->getStatusByName('wait_upgrade_approval');
        if($status == null)
            throw new BadRequestException(trans("locale.something_wrong"));

        $account->setStatus($status);
        $this->accountRepository->storeAccount($account);

        $noOfClassroomJourneysUsed = $account->getSchool()->getJourneysCount();
        $noOfStudents = $account->getCountOfStudents();

        $from = Carbon::now()->startOfDay();
        $to = Carbon::parse($subscription->getEndDate())->startOfDay();
        $differanceInDays = $to->diffInDays($from);
        if($differanceInDays <= 0)
            throw new BadRequestException(trans("locale.differance_in_days_less_than_equal_zero"));

        if($customDto->bundle_id != null){
            $bundle = $this->schoolRepository->getBundleById($customDto->bundle_id);
            if($bundle == null)
                throw new BadRequestException(trans("locale.bundle_not_exist"));

            if($bundle->getNoStudents() < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));

            if($bundle->getNoClassroom() < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            $distributorChargeDetails = $distributorCharge->getChargeDetail();
            if($distributorChargeDetails->getName() == $bundle->getName())
                throw new BadRequestException(trans("locale.cant_upgrade_to_same_plan"));

            $this->schoolRepository->deleteUnusedDistributorUpgrades($distributorCharge->getId());

            $description = "Upgrade to bundle (" . $bundle->getName() . ") with (" . $bundle->getNoStudents() . " Student, " . $bundle->getNoClassroom() . " Classroom/Journeys).";
            $upgrade_code = $distributor->getId() . Carbon::parse(Carbon::now()->addYears(1))->timestamp.str_random(30);

            $distributorUpgrade = new DistributorChargeUpgrade($distributorCharge, null, $bundle->getNoStudents(), $bundle->getNoClassroom(), $description, $upgrade_code, $bundle->getName(), 0);
            $distributorUpgrade->setBundle($bundle);
            $this->schoolRepository->storeDistributorChargeUpgrade($distributorUpgrade);

            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $this->schoolRepository->emailDistributor($distributor->getName(), $bundle->getName(), $bundle->getNoStudents(), $bundle->getNoClassroom(), $distributor->getEmail(), $user->getName(), $user->getEmail(), $school->getName(), null, $distributorUpgrade->getUpgradeCode(), true);
            $this->schoolRepository->emailSubscription($user_name, $bundle->getName(), $bundle->getNoStudents(), $bundle->getNoClassroom(),  $user->getEmail(), null, true);
        }

        else if($customDto->students != null && $customDto->classrooms != null){
            $plan = $this->accountRepository->getPlanByName('Custom');
            if($plan == null)
                throw new BadRequestException(trans("locale.plan_not_found"));

            $plans = $this->getAllPlanNames();
            if(isset($customDto->plan_name) && !empty($customDto->plan_name) && in_array($customDto->plan_name, $plans))
                throw new BadRequestException(trans("locale.plan_in_static"));

            // get the last updated version of plan from plan history
            $updated_plan = $this->accountRepository->getlastupdated($plan, $account_type->getId());
            if($updated_plan == null)
                throw new BadRequestException(trans("locale.version_plan_not_found"));

            if($customDto->students + $distributorCharge->getChargeDetail()->getNoStudents() < $noOfStudents)
                throw new BadRequestException(trans("locale.bundle_no_student_must_equal_or_exceed"));

            if($customDto->classrooms + $distributorCharge->getChargeDetail()->getNoClassroom() < $noOfClassroomJourneysUsed)
                throw new BadRequestException(trans("locale.bundle_no_classroom_journey_must_equal_or_exceed"));

            if($customDto->students % 10 != 0 || $customDto->students < 10){
                throw new BadRequestException(trans("locale.students_no_error"));
            }

//            if($customDto->classrooms < 1){
//                throw new BadRequestException(trans("locale.classrooms_no_error"));
//            }

            $this->schoolRepository->deleteUnusedDistributorUpgrades($distributorCharge->getId());

            $description = "Upgrade to add (" . $customDto->students . " Student, " . $customDto->classrooms . " Classroom/Journeys).";
            $upgrade_code = $distributor->getId() . Carbon::parse(Carbon::now()->addYears(1))->timestamp.str_random(30);

            $distributorUpgrade = new DistributorChargeUpgrade($distributorCharge, null, $customDto->students, $customDto->classrooms, $description, $upgrade_code, $customDto->plan_name, 0);
            $this->schoolRepository->storeDistributorChargeUpgrade($distributorUpgrade);
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());

            $this->schoolRepository->emailDistributor($distributor->getName(), $updated_plan->getName(), $customDto->students, $customDto->classrooms, $distributor->getEmail(), $user->getName(), $user->getEmail(), $school->getName(), null, $distributorUpgrade->getUpgradeCode(), true);
            $this->schoolRepository->emailSubscription($user_name, $updated_plan->getName(), $customDto->students, $customDto->classrooms, $user->getEmail(), null, true);
        }
        else {
            throw new BadRequestException(trans("locale.no_or_bundle_required"));
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.school_account_created');
    }

    public function activateDistributorUpgrade($user_id, $code){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $status = $this->accountRepository->getStatusByName('wait_activation');
        if($status == null)
            throw new BadRequestException(trans("locale.something_wrong"));

        $account->setStatus($status);
        $this->accountRepository->storeAccount($account);

        if(!$this->accountRepository->isAuthorized('upgrade_subscription', $user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $activeSubscriptions = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId());
        $subscription = $activeSubscriptions->first();
        if($subscription == null)
            throw new UnauthorizedException(trans('locale.subscribe_first'));

        $distributorSubscription = $subscription->getDistributorSubscriptions()->first();
        if($distributorSubscription == null)
            throw new BadRequestException(trans("locale.cant_change_from_disributor_to_stripe"));

        $distributorCharge = $distributorSubscription->getDistributorCharge();
        if($distributorCharge == null)
            throw new BadRequestException(trans("locale.cant_change_from_disributor_to_stripe"));

        $distributor = $distributorSubscription->getDistributor();
        if($distributor == null)
            throw new BadRequestException(trans("locale.distributor_not_found"));

        $distributorLastUpgrade = $this->schoolRepository->getDistributorLastUpgrade($distributorCharge->getId());
        if($distributorLastUpgrade == null)
            throw new BadRequestException(trans("locale.no_upgrades_found"));

        if($distributorLastUpgrade->getUpgradeCode() != $code)
            throw new BadRequestException(trans('locale.distributor_code_invalid'));

        $distributorLastUpgrade->setUsed(1);
        $this->schoolRepository->storeDistributorChargeUpgrade($distributorLastUpgrade);

        $distributorDetails = $distributorCharge->getChargeDetail();
        if($distributorDetails == null)
            throw new UnauthorizedException(trans('locale.subscribe_first'));

        $distributorDetails->setName($distributorLastUpgrade->getPlanName());
        $bundle = $distributorLastUpgrade->getBundle();
        if(isset($bundle) && ($bundle->getName() == 'Silver' || $bundle->getName() == 'Bronze' || $bundle->getName() == 'Golden')){
            $distributorDetails->setNoStudents($bundle->getNoStudents());
            $distributorDetails->setNoClassroom($bundle->getNoClassroom());
        }
        else {
            $distributorDetails->setNoStudents($distributorDetails->getNoStudents() + $distributorLastUpgrade->getNoStudents());
            $distributorDetails->setNoClassroom($distributorDetails->getNoClassroom() + $distributorLastUpgrade->getNoClassroom());
        }
        $this->schoolRepository->storeDistributorChargeDetails($distributorDetails);

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.upgrade_activated');
    }

    public function checkSchoolStatus($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException("User is missing");

        $account = $user->getAccount();
        if($account == null)
            throw new BadRequestException("User account not found");

        $account_type = $account->getAccountType();
        if($account_type->getName() != 'School')
            throw new BadRequestException(trans('locale.no_school_account'));

        if($account->getStatus() == 'locked')
            throw new CustomException("You don't have access to this section yet. Please activate your account", 297);

        if($account->getStatus() == 'wait_subscription_approval')
            throw new CustomException("Wait subscription activation", 298);

        if($account->getStatus() == 'wait_upgrade_approval')
            throw new CustomException("Wait upgrade activation", 299);

        return trans('locale.user_already_active');
    }

    public function getAllPlanNames(){
        $allPlans = [];
        $plans = $this->accountRepository->getAllPlanNames();
        foreach ($plans as $plan){
            if(!in_array($plan->getName(), $allPlans)){
                array_push($allPlans, $plan->getName());
            }
        }
        return $allPlans;
    }

    public function getLastEndedSubscription($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException("User is missing");

        $account = $user->getAccount();
        if($account == null)
            throw new BadRequestException("User account not found");

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $ended = false;
        $result = [];
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0){
            $lastSubscription = $this->accountRepository->getLastSubscription($account);
            if($lastSubscription != null){
                $ended = true;
                $distributorSubscription = $lastSubscription->getDistributorSubscriptions()->first();
                if($distributorSubscription != null) {
                    $distributorDetails = $distributorSubscription->getDistributorCharge()->getChargeDetail();
                    $result['plan_name'] = $distributorDetails->getName();
                }
                else {
                    $schoolDetails = $lastSubscription->schoolCharge()->getChargeDetail();
                    $result['plan_name'] = $schoolDetails->getName();
                }
                $result['start_date'] = explode(" ", $lastSubscription->getStartDate())[0];
                $result['end_date'] = explode(" ", $lastSubscription->getEndDate())[0];
                $result['total_students'] = 0;
                $result['total_classrooms'] = 0;
                $result['remaining_students'] = 0;
                $result['remaining_classrooms'] = 0;
                $result['remaining_time'] = 0;
            }
        }
        return ["ended" => $ended, "subscription" => $result];
    }


}