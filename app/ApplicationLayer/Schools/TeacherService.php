<?php

namespace App\ApplicationLayer\Schools;

use App\ApplicationLayer\Journeys\Dtos\ActivityScreenshotDto;
use App\ApplicationLayer\Journeys\Dtos\DifficultyDto;
use App\ApplicationLayer\Journeys\Dtos\TagDto;
use App\ApplicationLayer\Schools\Dtos\ActivityDto;
use App\ApplicationLayer\Schools\Dtos\ActivityMinifiedDto;
use App\ApplicationLayer\Schools\Dtos\ActivityTranslationDto;
use App\ApplicationLayer\Schools\Dtos\CampDto;
use App\ApplicationLayer\Schools\Dtos\CampNamesDto;
use App\ApplicationLayer\Schools\Dtos\SchoolCampDto;
use App\ApplicationLayer\Schools\Dtos\StudentCampDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\ApplicationLayer\Schools\Dtos\TeacherCampDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;

use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Schools\CourseAdventureDeadlines;
use App\DomainModelLayer\Schools\GroupMember;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\Framework\Exceptions\CustomException;
use Carbon\Carbon;
use App\Helpers\Mapper;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Schools\Dtos\TeacherDto;
use App\ApplicationLayer\Schools\Dtos\CourseDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\DomainModelLayer\Schools\Course;
use App\ApplicationLayer\Journeys\Dtos\MinifiedJourneyDto;
use Illuminate\Pagination\Paginator;
use App\DomainModelLayer\Schools\School;
use App\DomainModelLayer\Accounts\User;
use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;

class TeacherService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;

    }
    //endregion

    //region Functions 
    public function countTeachers($user_id,$grade_name,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllTeachers',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException("there is no grade with this name for this school");
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException("this grade is not in the school");
            $grade_id = $grade->getId();
        }

        $numberOfTeachers = $this->schoolRepository->getAllTeachers($account->getId(),null,$grade_id,$search,true);
        return ["count"=>$numberOfTeachers];
    }

    public function getAllTeachers($user_id,$start_from,$limit,$grade_name,$search)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllTeachers',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException(trans('locale.grade_name_not_in_school'));
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans('locale.grade_not_in_school'));
            $grade_id = $grade->getId();
        }

        $teachers = $this->schoolRepository->getAllTeachers($account->getId(),$limit,$grade_id,$search);
        return Mapper::MapEntityCollection(TeacherDto::class,$teachers,[ClassroomDto::class]);
    } 

    public function getTeacherById($user_id,$id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_AllTeachers',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $teacher = $this->accountRepository->getUserById($id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.teacher_not_exist"));

        if($teacher->getAccount()->getId() != $user->getAccount()->getId()){
            throw new BadRequestException(trans('locale.teacher_school_mismatch'));
        }

        $teacherRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        if(!$isTeacher)
            throw new BadRequestException(trans("locale.student_wrong_role"));

        return Mapper::MapClass(TeacherDto::class,$teacher,[ClassroomDto::class]);
    }

    public function updateTeacher($user_id,UserDto $userDto,$classroom_ids){
        $this->accountRepository->beginDatabaseTransaction();

        $loggedInUser = $this->accountRepository->getUserById($user_id);
        if($loggedInUser == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('update_Teacher',$loggedInUser))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if(sizeOf($classroom_ids) > 0){
            if(!$this->accountRepository->isAuthorized('update_TeacherClassroom',$loggedInUser))
                throw new UnauthorizedException(trans('locale.no_permission_operation'));
        }



        $teacher = $this->accountRepository->getUserById($userDto->id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.teacher_not_exist"));

        if($teacher->getUsername() != $userDto->username){
            $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
            if($existingUsername != null)
                throw new BadRequestException(trans("locale.username_exists"));
        }

        if($teacher->getEmail() != $userDto->email){
            $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
            if($existingUserMail != null)
                throw new BadRequestException(trans("locale.user_email_exists"));
        }

        if($teacher->getAccount()->getId() != $loggedInUser->getAccount()->getId()){
            throw new BadRequestException(trans('locale.teacher_school_mismatch'));
        }

        $teacherRoles = $loggedInUser->getRoles();
        $isTeacherLoggedIn = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacherLoggedIn = true;
        }

        $teacherRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        if(!$isTeacher)
            throw new BadRequestException(trans("locale.user_teacher_not_provided"));


        if($isTeacherLoggedIn && $isTeacher && ($user_id != $userDto->id))
            throw  new UnauthorizedException(trans("locale.can_not_update_another_teacher_data"));

        $teacher->setFirstName($userDto->fname);
        $teacher->setLastName($userDto->lname);
        $teacher->setUsername($userDto->username);
        $teacher->setEmail($userDto->email);
        if($userDto->password != null)
            $teacher->setpassword(bcrypt($userDto->password));

        if(sizeOf($classroom_ids) > 0){
            $alreadyAssignedClassrooms = $teacher->getClassrooms();
            foreach ($alreadyAssignedClassrooms as $alreadyAssignedClassroom){
                $alreadyAssignedClassroom->removeUser($teacher);
                $this->schoolRepository->storeClassroom($alreadyAssignedClassroom);
            }
            foreach ($classroom_ids as $classroom_id){
                $classroom = $this->schoolRepository->getClassroomById($classroom_id);
                if($classroom == null)
                    throw new BadRequestException(trans("locale.classroom_doesn't_exist"));
                $classroom->addUser($teacher);
                $this->schoolRepository->storeClassroom($classroom);

            }
        }
        $this->accountRepository->storeUser($teacher);
        $this->accountRepository->commitDatabaseTransaction();

        return UserDtoMapper::CustomerMapper($teacher);
    }

    public function deleteTeachers($user_id,$teachers,$force = false)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Teacher',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        foreach ($teachers as $teacher){

            $this->deleteTeacher($school,$teacher,$force);

        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.teachers_delete_successfull");
    }

    public function deleteTeacher(School $userSchool,$teacher_id,$force = false)
    {

        $teacher = $this->accountRepository->getUserById($teacher_id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.teacher_not_exist"));
        $teacherSchool = $teacher->getAccount()->getSchool();
        if($teacherSchool == null)
            throw new BadRequestException(trans("locale.teacher_no_school"));
        if($teacherSchool->getId() != $userSchool->getId())
            throw new BadRequestException(trans("locale.teacher_school_mismatch"));

        $teacherRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }
        if(!$isTeacher)
            throw  new BadRequestException(trans("locale.teacher_wrong_role"));


        $checkMainTeacher = $this->checkIsMainTeacher($teacher);
        //dd($checkMainTeacher['main_teacher']);
        if(!$force  &&  $checkMainTeacher['main_teacher'])
            throw new CustomException(trans("locale.main_teacher_delete"),443);

        if($force && $checkMainTeacher['main_teacher']){
            foreach ($checkMainTeacher['classroomMainTeacherMembers'] as $classroomMainTeacherMembers){
                $classroom = $classroomMainTeacherMembers->getGroup()->getClassroom();

                $teachers  = $classroom->getMembersWithRole('teacher');
                $mainTeacherRole = $this->accountRepository->getRoleByName('main_teacher');
                $teacherRole = $this->accountRepository->getRoleByName('teacher');
                if(sizeof($teachers)>0){
                    $teacherInClassroom  = $teachers[0];
                    $defaultGroup = $classroom->getDefaultGroup();
                    $classroom_default_group_member = new GroupMember($defaultGroup,$teacherInClassroom,$mainTeacherRole);
                    $this->schoolRepository->deleteGroupMember($this->schoolRepository->getDefaultGroupMember($classroom,$teacherInClassroom,$teacherRole));
                    $defaultGroup->addGroupMember($classroom_default_group_member);
                    $this->schoolRepository->storeGroup($defaultGroup);
                }
                else{
                    $students = $classroom->getStudents();
                    $studentIdsArray = array();
                    foreach ($students as $student) {
                        array_push($studentIdsArray,$student->getId());
                    }
                    if(sizeOf($students)>0){
                        if($this->schoolRepository->getIfStudentsProgressedInClassroom($classroom,$studentIdsArray))
                            throw new BadRequestException(trans("locale.main_teacher_with_name").$teacher->getName().trans("locale.students_progressed"));
                    }
                }
            }
        }

        $this->accountRepository->deleteUser($teacher);
        $this->schoolRepository->deleteGroupMembers($teacher);

        return trans("locale.teacher_delete_successfull");
    }

    public function checkIsMainTeacher(User $teacher){
        $classroom_default_group_members = $teacher->getGroupMembers();

        $mainTeacher = false;
        $classroomMainTeacherMembers = array();
        foreach ($classroom_default_group_members as $classroom_default_group_member){

            if($classroom_default_group_member->getRole()->getName() == "main_teacher") {
                $mainTeacher = true;
                array_push($classroomMainTeacherMembers,$classroom_default_group_member);
            }
        }
        return ['main_teacher'=>$mainTeacher,'classroomMainTeacherMembers'=>$classroomMainTeacherMembers,];
    }

    public function getTeacherClassrooms($user_id,$start_from,$limit,$grade_name,$search){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherClassroom',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException(trans('locale.grade_name_not_in_school'));
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans('locale.grade_not_in_school'));
            $grade_id = $grade->getId();
        }

        $classrooms = $this->schoolRepository->getTeacherClassrooms($user,$limit,$grade_id,$search);

        $classroomsMapped =  Mapper::MapEntityCollection(ClassroomDto::class,$classrooms,[TeacherDto::class,JourneyDto::class]);

        foreach ($classroomsMapped as $classroomMapped){
            $classroomMapped->journeys = Mapper::MapEntityCollection(MinifiedJourneyDto::class,$classroomMapped->journeys,[JourneyCategoryDto::class]);
        }
        $counter = 0;
        foreach ($classrooms as $classroom) {
            # code...
            $classroomsMapped[$counter]->noOfCourses = count($classroom->getDefaultGroup()->getCourses());
            $counter++;
        }

        return $classroomsMapped;

    }

    public function countTeacherClassrooms($user_id,$grade_name,$search){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherClassroom',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException("there is no grade with this name for this school");
            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException("this grade is not in the school");
            $grade_id = $grade->getId();
        }

        $numberOfClassrooms = $this->schoolRepository->getTeacherClassrooms($user,null,$grade_id,$search,true);
        return ["count"=>$numberOfClassrooms];
    }

    public function getTeacherCourses($user_id,$start_from,$limit,$classroom_id,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherCourses',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }


        if($classroom_id != null){
            $classroom = $this->schoolRepository->getClassroomById($classroom_id);

            if($classroom == null)
                throw  new BadRequestException(trans("locale.classroom_doesn't_exist"));
            if($classroom->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));

        }

        $courses = $this->schoolRepository->getTeacherCourses($user,$limit,$classroom_id,$search);

        $courseDtos = Mapper::MapEntityCollection(CourseDto::class,$courses,[JourneyDto::class]);
        $counter = 0;
        foreach ($courses as $key => $course) {
            $courseDtos[$counter]->classroom = Mapper::MapClass(ClassroomDto::class,$course->getClassroom());
            $courseDtos[$counter]->startsAt = explode(' ',$course->getStartsAt())[0];
            $courseDtos[$counter]->endsAt = explode(' ',$course->getEndsAt())[0];
            $counter++;
        }
        return $courseDtos;
    }

    public function countTeacherCourses($user_id,$classroom_id,$search){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherCourses',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        if($classroom_id != null){
            $classroom = $this->schoolRepository->getClassroomById($classroom_id);

            if($classroom == null)
                throw  new BadRequestException(trans("locale.classroom_doesn't_exist"));
            if($classroom->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));

        }

        $numberOfCourses = $this->schoolRepository->getTeacherCourses($user,null,$classroom_id,$search,true);
        return ["count"=>$numberOfCourses];
    }

    public function createCourse($user_id,$courseRequestDto){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('create_Course',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $classroom = $this->schoolRepository->getClassroomById($courseRequestDto->classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));

        if($classroom->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));

        $defaultGroup = $classroom->getDefaultGroup();
        $mainTeacher = $defaultGroup-> getMainTeacher();
        if($mainTeacher->getId() != $user->getId())
            throw new BadRequestException(trans("locale.teacher_not_mainteacher"));

        $journey = $this->journeyRepository->getJourneyById($courseRequestDto->journey_id);
        if($journey == null)
            throw new BadRequestException(trans("locale.journey_not_exist"));

        $classroomJourneyExists = false;
        $classroomJourneys = $classroom->getJourneys();
        foreach ($classroomJourneys as $classroomJourney){
            if($classroomJourney->getId() == $journey->getId())
                $classroomJourneyExists = true;
        }

        if(!$classroomJourneyExists)
            throw new BadRequestException(trans("locale.journey_not_in_classroom"));


        $journeyAdventures = $journey->getAdventures();

        if(sizeof($courseRequestDto->adventures_deadlines) != sizeof($journeyAdventures))
            throw new BadRequestException(trans("locale.journey_adventure_deadlines_size_mismatch"));

        $courseRequestDto->endsAt = $courseRequestDto->adventures_deadlines[count($courseRequestDto->adventures_deadlines)-1]['endsAt'];
        $diff = Carbon::parse($courseRequestDto->endsAt)->diffInSeconds(Carbon::parse($courseRequestDto->startsAt),false);
        if($diff >0)
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

//        if($classroom->checkifThereisCourseWithJourneyInDefaultGroup($journey))
//            throw new BadRequestException(trans("locale.course_with_journey_exist"));

        if(!$this->schoolRepository->checkCourseNameUniqueInClassroom($classroom,$courseRequestDto->name))
            throw new BadRequestException(trans("locale.course_exist_name_in_classroom"));
        $courseRequestDto->startsAt = Carbon::parse($courseRequestDto->startsAt)->format('Y-m-d').' 00:00:00';
        $courseRequestDto->endsAt = Carbon::parse($courseRequestDto->endsAt)->format('Y-m-d').' 00:00:00';
        $course = new Course($journey,$user,$courseRequestDto,false);
        $this->schoolRepository->storeCourse($course);

        $courseRequestAdventureDeadlines = $courseRequestDto->adventures_deadlines;
        for($i= 0 ;$i<sizeof($courseRequestAdventureDeadlines);$i++){
            $adventure = $this->journeyRepository->getAdventureById($courseRequestAdventureDeadlines[$i]['id']);
            if($adventure == null)
                throw new BadRequestException(trans("locale.adventure_doesn't_exist"));
            $adventureJourneys = $adventure->getJourneys();
            $journeyBelongs = false;
            foreach ($adventureJourneys as $adventureJourney){
                if($adventureJourney->getId() == $journey->getId()){
                    $journeyBelongs =true;
                    break;
                }
            }
            if(!$journeyBelongs)
                throw new BadRequestException(trans("locale.adventure_doesn't_match_course_journey"));

            $courseAdventureDeadlines = $course->getAdventureDeadlines();
            if($courseAdventureDeadlines != null){
                foreach ($courseAdventureDeadlines as $courseAdventureDeadline){
                    if($courseAdventureDeadline->getAdventure()->getId() == $adventure->getId())
                        throw new BadRequestException(trans("locale.adventure_deadline_exists"));
                }
            }
            if($i == 0){
                $diff = Carbon::parse($courseRequestDto->startsAt)->diffInSeconds(Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt']),false);
                if($diff <0)
                    throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

                $course->addAdventureDeadlines(new CourseAdventureDeadlines($course,$adventure,Carbon::parse($courseRequestDto->startsAt)->format('Y-m-d').' 00:00:00',Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt'])->format('Y-m-d').' 00:00:00'));
            }
            else{
                $diff = Carbon::parse($courseRequestAdventureDeadlines[$i-1]['endsAt'])->diffInSeconds(Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt']),false);
                if($diff <0)
                    throw new BadRequestException(trans("locale.end_date_start_date_min_time"));
                $course->addAdventureDeadlines(new CourseAdventureDeadlines($course,$adventure,Carbon::parse($courseRequestAdventureDeadlines[$i-1]['endsAt'])->format('Y-m-d').' 00:00:00',Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt'])->format('Y-m-d').' 00:00:00'));
            }
        }
        $this->schoolRepository->storeCourse($course);
        $defaultGroup = $classroom->getDefaultGroup();
        $defaultGroup->addCourse($course);
        $this->schoolRepository->storeGroup($defaultGroup);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(CourseDto::class,$course,[JourneyDto::class,ClassroomDto::class]);

    }

    public function editCourse($user_id,$courseId,$courseRequestDto){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('create_Course',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $classroom = $this->schoolRepository->getClassroomById($courseRequestDto->classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));

        if($classroom->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));


        $course = $this->schoolRepository->getCourseById($courseId);
        if($course == null)
            throw new BadRequestException(trans("locale.course_doesn't_exist"));

        if($course->getClassroom()->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.course_doesn't_exist_school"));


        $defaultGroup = $classroom->getDefaultGroup();
        $mainTeacher = $defaultGroup-> getMainTeacher();
        if($mainTeacher->getId() != $user->getId())
            throw new BadRequestException(trans("locale.teacher_not_mainteacher"));

        $journey = $this->journeyRepository->getJourneyById($course->getJourney()->getId());
        if($journey == null)
            throw new BadRequestException(trans("locale.journey_not_exist"));


        $journeyAdventures = $journey->getAdventures();

        if(sizeof($courseRequestDto->adventures_deadlines) != sizeof($journeyAdventures))
            throw new BadRequestException(trans("locale.journey_adventure_deadlines_size_mismatch"));

        $courseRequestDto->startsAt = Carbon::parse($courseRequestDto->startsAt)->format('Y-m-d').' 00:00:00';
        
        if($course->getStartsAt() != $courseRequestDto->startsAt){
            if($course->checkIfAnyBodyProgressed())
                throw new BadRequestException(trans("locale.students_progressed_in_course"));
        }


        $this->schoolRepository->deleteCourseAdventureDeadlines($course);
        $course = $this->schoolRepository->getCourseById($courseId);

        $courseRequestDto->endsAt = $courseRequestDto->adventures_deadlines[count($courseRequestDto->adventures_deadlines)-1]['endsAt'];
        $diff = Carbon::parse($courseRequestDto->endsAt)->diffInSeconds(Carbon::parse($courseRequestDto->startsAt),false);
        if($diff >0)
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

        if($courseRequestDto->name != $course->getName()){
            if(!$this->schoolRepository->checkCourseNameUniqueInClassroom($classroom,$courseRequestDto->name))
                throw new BadRequestException(trans("locale.course_exist_name_in_classroom"));
        }

        $course->setName($courseRequestDto->name);
        $courseRequestDto->startsAt = Carbon::parse($courseRequestDto->startsAt)->format('Y-m-d').' 00:00:00';
        $courseRequestDto->endsAt = Carbon::parse($courseRequestDto->endsAt)->format('Y-m-d').' 00:00:00';
        $course->setEndsAt($courseRequestDto->endsAt);
        $course->setStartsAt($courseRequestDto->startsAt);

        $courseRequestAdventureDeadlines = $courseRequestDto->adventures_deadlines;
        for($i= 0 ;$i<sizeof($courseRequestAdventureDeadlines);$i++){
            $adventure = $this->journeyRepository->getAdventureById($courseRequestAdventureDeadlines[$i]['id']);
            if($adventure == null)
                throw new BadRequestException(trans("locale.adventure_doesn't_exist"));
            $adventureJourneys = $adventure->getJourneys();
            $journeyBelongs = false;
            foreach ($adventureJourneys as $adventureJourney){
                if($adventureJourney->getId() == $journey->getId()){
                    $journeyBelongs =true;
                    break;
                }
            }
            if(!$journeyBelongs)
                throw new BadRequestException(trans("locale.adventure_doesn't_match_course_journey"));

            $courseAdventureDeadlines = $course->getAdventureDeadlines();
            if($courseAdventureDeadlines != null){
                foreach ($courseAdventureDeadlines as $courseAdventureDeadline){
                    if($courseAdventureDeadline->getAdventure()->getId() == $adventure->getId())
                        throw new BadRequestException(trans("locale.adventure_deadline_exists"));
                }
            }
            if($i == 0){
                $diff = Carbon::parse($courseRequestDto->startsAt)->diffInSeconds(Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt']),false);
                if($diff <0)
                    throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

                $course->addAdventureDeadlines(new CourseAdventureDeadlines($course,$adventure,Carbon::parse($courseRequestDto->startsAt)->format('Y-m-d').' 00:00:00',Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt'])->format('Y-m-d').' 00:00:00'));
            }
            else{
                $diff = Carbon::parse($courseRequestAdventureDeadlines[$i-1]['endsAt'])->diffInSeconds(Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt']),false);
                if($diff <0)
                    throw new BadRequestException(trans("locale.end_date_start_date_min_time"));
                $course->addAdventureDeadlines(new CourseAdventureDeadlines($course,$adventure,Carbon::parse($courseRequestAdventureDeadlines[$i-1]['endsAt'])->format('Y-m-d').' 00:00:00',Carbon::parse($courseRequestAdventureDeadlines[$i]['endsAt'])->format('Y-m-d').' 00:00:00'));;
            }
        }
        $this->schoolRepository->storeCourse($course);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(CourseDto::class,$course,[JourneyDto::class,ClassroomDto::class]);
    }

    public function deleteCourse($user_id,$course_id){
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Course',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_doesn't_exist"));

        if($course->getClassroom()->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.course_doesn't_exist_school"));

        $defaultGroup = $course->getClassroom()->getDefaultGroup();
        $mainTeacher = $defaultGroup-> getMainTeacher();
        if($mainTeacher->getId() != $user->getId())
            throw new BadRequestException(trans("locale.teacher_not_mainteacher"));

        if($course->checkIfAnyBodyProgressed())
            throw new BadRequestException(trans("locale.students_progressed_in_course"));


        $courseGroups = $course->getGroups();
        foreach ($courseGroups as $courseGroup){
            $course->removeGroup($courseGroup);
        }
        $this->schoolRepository->storeCourse($course);
        $this->schoolRepository->deleteCourse($course);
        $this->schoolRepository->deleteCourseAdventureDeadlines($course);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.course_deleted');
    }

    public function getStudentsDataByCourseId($user_id,$course_id,$adventure_id,$start_from,$limit,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('show_StudentCourseData',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_doesn't_exist"));

        if($course->getClassroom()->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.course_doesn't_exist_school"));


        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $students = $this->schoolRepository->getStudentsInGroup($course->getClassroom()->getDefaultGroup(),$limit,$search);
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans("locale.adventure_doesn't_exist"));

        $jounreyExist = false;
        foreach ($adventure->getJourneys() as $jounrey){
            if($jounrey->getId() == $course->getJourney()->getId()){
                $jounreyExist = true;
                break;
            }
        }
        if(!$jounreyExist)
            throw new BadRequestException(trans("locale.adventure_doesn't_match_course_journey"));

        $tasks = $adventure->getTasks();
        //  dd($tasks);
        $studentsData = array();
        $counter =0;
        foreach ($students as $student){
            array_push($studentsData,['name'=>$student->getName(),'username'=>$student->getUsername(),'id'=>$student->getId(),'profile_image'=>$student->getImage(),'tasks'=>array()]);
            $unOrderTasks = array();
            foreach ($tasks as $task){
                if(count($task->getMissions()) == 0 && count($task->getMissionsHtml()) == 0 && count($task->getQuizzes()) == 0){
                    continue;
                }
                array_push($unOrderTasks,$student->getCourseDataForTask($course,$task));
            }
            $missionTasks  = array();
            $quizTasks = array();
            $tutorialTasks = array();
            foreach ($unOrderTasks as $unOrderTask){
                if($unOrderTask['type'] == "mission")
                    array_push($missionTasks,$unOrderTask);
                else if($unOrderTask['type'] =="quiz")
                    array_push($quizTasks,$unOrderTask);
                else if($unOrderTask['type'] == "tutorial")
                    array_push($tutorialTasks,$unOrderTask);
            }
            $studentsData[$counter]['tasks'] = array_merge($tutorialTasks,$missionTasks,$quizTasks);
            $counter++;
        }
        return $studentsData;
    }

    public function getStudentsDataByCourseIdCount($user_id,$course_id,$adventure_id,$search){
        return ["count"=>sizeof($this->getStudentsDataByCourseId($user_id,$course_id,$adventure_id,0,null,$search))];
    }

    public function getStudentDataByClassroomId($user_id,$classroom_id,$start_from,$limit,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('show_StudentClassroomData',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));


        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $students = $this->schoolRepository->getStudentsInGroup($classroom->getDefaultGroup(),$limit,$search);
        $studentsData = array();
        $counter =0;
        foreach ($students as $student){
            $courses = $classroom->getDefaultGroup()->getCourses();
            $mission_score = 0;
            $quiz_score= 0;
            foreach ($courses as $course){
                $data = $student->getCourseScores($course->getId());
                $mission_score +=  $data['mission_scores'];
                $quiz_score+= $data['quiz_scores'];
            }
            $totalScore = $quiz_score+$mission_score;
            array_push($studentsData,['name'=>$student->getName(),'username'=>$student->getUsername(),'id'=>$student->getId()
                ,'profile_image'=>$student->getImage(),'score'=>$totalScore,'mission_score'=>$mission_score
                ,'quiz_score'=>$quiz_score]);
            $counter++;
        }
        return $studentsData;

    }

    public function getStudentDataByClassroomIdCount($user_id,$classroom_id,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException("User doesn't Exist");

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException("You don't have a shool account");

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException("this account doesn't have a school attached to it");

        if(!$this->accountRepository->isAuthorized('show_StudentClassroomData',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));

        return ["count"=>$this->schoolRepository->getStudentsInGroup($classroom->getDefaultGroup(),null,$search,true)];
    }

    public function getStudentInfoById($user_id,$student_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('show_StudentMainInfo',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));


        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans("locale.student_not_exist"));

        $classroom = $student->getBasicClassRoom();
        $defaultGroup = $classroom->getDefaultGroup();
        $teachers = $defaultGroup->getMembersWithRole("teacher");
        $students = $defaultGroup->getStudents();
        $studentInGroup = false;
        $teacherInGroup = false;
        foreach($students as $groupStudent){
            if($groupStudent->getId() == $student->getId())
                $studentInGroup = true;
        }
        foreach ($teachers as $teacher){
            if($teacher->getId() == $user->getId())
                $teacherInGroup = true;
        }
        if(!$studentInGroup)
            throw new BadRequestException(trans("locale.student_not_in_group"));

        if(!$teacherInGroup){
            $teachers = $defaultGroup->getMembersWithRole("main_teacher");
            foreach ($teachers as $teacher){
                if($teacher->getId() == $user->getId())
                    $teacherInGroup = true;
            }
        }
        if(!$teacherInGroup)
            throw new BadRequestException(trans("locale.teacher_not_student_group"));


        $coursesArray = array();
        $courses = $defaultGroup->getCourses();
        $mission_score = 0;
        $quiz_score= 0;
        $missionsolved = 0;
        $missionsFailed = 0;
        $totalMissions = 0;
        foreach ($courses as $course)
        {
            array_push($coursesArray,$course->getName());
            $data = $student->getCourseScores($course->getId());
            $mission_score +=  $data['mission_scores'];
            $quiz_score+= $data['quiz_scores'];
            $missionsolved += $data['missions_solved'];
            $missionsFailed += $data['missions_failed'];
            $totalMissions += $course->getTasksCount();
        }
        $unsolvedMissions = $totalMissions-$missionsolved;
        $totalScore = $mission_score+$quiz_score;
        $percentageofMissionsSolved = 0;
        if($totalMissions != 0){
            $percentageofMissionsSolved = ceil(($missionsolved/$totalMissions)*100);
        }

        $rank = $student->getRankInClassroom($classroom);

        return ['username'=>$student->getUsername(),'name' => $student->getName(),'profile_image'=>$student->getImage(),'id'=>$student->getId(),
            'classroomName'=>$classroom->getName(),'courses'=>$coursesArray,'missionsSolved'=>$missionsolved,
            'unsolvedMissions'=>$unsolvedMissions,'missionsFailed'=>$missionsFailed,
            'totalScore'=>$totalScore,'rank'=>$rank,'percentageofMissionsSolved'=>$percentageofMissionsSolved];
    }

    public function getTopStudentsInClassroom($user_id,$classroom_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));

        if($classroom->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));

        if(!$classroom->getDefaultGroup()->isMember($user->getId()))
            throw new BadRequestException(trans("locale.teacher_not_in_classroom"));

        $data = array();
        $queryData = $this->schoolRepository->getTopStudentsInClassroom($account,$classroom);

        foreach ($queryData as $student){
            $studentx = $this->accountRepository->getUserById($student->id);
            $progress = $studentx->getProgressPercentage();
            if($student->score != 0)
                array_push($data,["name" => $student->first_name.' '.$student->last_name, "coins" => $student->coins, "score" => $student->score, "progress" => ceil($progress)."%"]);
        }

        return $data;
    }

    public function generalStatisticsTeacher($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $noOfStudents= 0;
        $noOfCourses = 0;
        $classrooms = $user->getClassrooms();
        foreach($classrooms as $classroom){
            $noOfStudents += sizeof($classroom->getDefaultGroup()->getStudents());
            $noOfCourses += sizeof($classroom->getDefaultGroup()->getCourses());
        }
        $noOfClassrooms = sizeOf($classrooms);
        return ['noOfStudents'=>$noOfStudents,'noOfCourses'=>$noOfCourses,'noOfClassrooms'=>$noOfClassrooms];
    }

    public function getClassroomScoreRange($user_id,$classroom_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));

        if($classroom ->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));

        if(!$classroom->getDefaultGroup()->isMember($user->getId()))
            throw new BadRequestException(trans("locale.teacher_not_in_classroom"));

        if(sizeof($classroom->getDefaultGroup()->getStudents()) == 0)
            throw new BadRequestException(trans("locale.classroom_no_students"));

        $queryData = $this->schoolRepository->getTopStudentsInClassroom($account,$classroom,null);

        $maxScore = $queryData[0]->score;
        $minScore = $queryData[sizeof($queryData)-1]->score;
        $noOfStudents = sizeof($queryData);
        if($minScore == 0 && $maxScore == 0)
            throw new BadRequestException(trans("locale.no_score_in_classroom"));
        if($minScore == $maxScore)
            return [["from"=>0,"to"=>$maxScore,"percentage"=>"100%",'noOfStudents'=>1]];
        else
        {
            $returnArray = array();
            $step = ($maxScore-$minScore)/5;
            $from  = $maxScore;
            $to = $maxScore-$step;

            for($i=0;$i<5;$i++){
                $noOfStudentsInRange = 0;
                for($j=0;$j<$noOfStudents;$j++){
                    if($queryData[$j]->score >= $to && $queryData[$j]->score <= $from)
                        $noOfStudentsInRange++;
                }
                array_push($returnArray,["from"=>$to,"to"=>$from,"percentage"=>round(($noOfStudentsInRange/$noOfStudents)*100,2),'noOfStudents'=>$noOfStudentsInRange]);
                $from = $to-1;
                if($i == 3)
                    $to = $minScore;
                else
                    $to = $from - $step;
            }
            return $returnArray;
        }

    }

    public function getTeacherMissionsChart($user_id,$from_time,$to_time)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if (!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));


        $classroom_ids = array();
        foreach ($user->getClassrooms() as $classroom)
            array_push($classroom_ids,$classroom->getId());

        $noOfMissionsOpened = null;
        $noOfMissionsSolved = null;
        if(sizeof($classroom_ids)>0){
            $noOfMissionsOpened = $this->schoolRepository->getStudentMissionClickByTime($account, $from_time, $to_time, $classroom_ids);
            $noOfMissionsSolved = $this->schoolRepository->getStudentSolvedMissions($account, $from_time, $to_time, $classroom_ids);
        }

        $startDate = Carbon::parse($from_time);

        $endDate = Carbon::parse($to_time);
        $data = array();

        for ($date = $startDate; $date->lte($endDate); $date->addDay()) {
            $noOfMissionsOpenedPerDayNumber = 0;
            $noOfMissionsSolvedPerDayNumber = 0;
            $currentDate = $date->format('Y-m-d');
            if($noOfMissionsOpened != null){
                foreach ($noOfMissionsOpened as $noOfMissionsOpenedPerDay) {
                    if ($noOfMissionsOpenedPerDay->time == $currentDate)
                        $noOfMissionsOpenedPerDayNumber = $noOfMissionsOpenedPerDay->number;
                }
            }
            if($noOfMissionsSolved != null){
                foreach ($noOfMissionsSolved as $noOfMissionsSolvedPerDay) {
                    if ($noOfMissionsSolvedPerDay->time == $currentDate)
                        $noOfMissionsSolvedPerDayNumber = $noOfMissionsSolvedPerDay->number;
                }
            }


            array_push($data, array('date' => $currentDate, 'no_students_open_mission_page' => $noOfMissionsOpenedPerDayNumber, 'no_missions_solved' => $noOfMissionsSolvedPerDayNumber));
        }
        return $data;
    }

    public function getTeacherActivityEvents($user_id,$from_time = null,$to_time = null){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if (!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $coursesAboutToStartActivity = $this->schoolRepository->getCourseAboutToStartForTeacher($user,$from_time,$to_time);
        $coursesEndedActivity = $this->schoolRepository->getCourseEndedInRangeForTeacher($user,$from_time,$to_time);
        $studentsFinishedCourse = $this->schoolRepository->getStudentsFinishCourseForTeacher($user);
        $eventsData = array();
        foreach ($coursesAboutToStartActivity as $courseAboutToStartActivity){
            array_push($eventsData,['flag'=>0
                ,'date'=>Carbon::parse($courseAboutToStartActivity->getStartsAt())->format('Y-m-d')
                ,'course_name'=>$courseAboutToStartActivity->getName()
                ,'classroom_name'=>$courseAboutToStartActivity->getClassroom()->getName()]);
        }
        foreach($coursesEndedActivity as $courseEndedActivity){
            array_push($eventsData,['flag'=>1,'classroom_name'=>$courseEndedActivity->getClassroom()->getName()
                ,'course_name'=>$courseEndedActivity->getName(),'date'=>Carbon::parse($courseEndedActivity->getEndsAt())->format('Y-m-d')]);
        }

        foreach($studentsFinishedCourse as $studentFinishedCourse){
            array_push($eventsData,['flag'=>2,'classroom_name'=>$studentFinishedCourse->classroom_name,
                'student_name'=>$studentFinishedCourse->name,'course_name'=>$studentFinishedCourse->course_name,'date'=>Carbon::parse($studentFinishedCourse->time)->format('Y-m-d')]);
        }

        if(sizeOf($eventsData) == 0){
            return [];
        }
        foreach ($eventsData as $key => $part) {
            $sort[$key] = strtotime($part['date']);
        }
        array_multisort($sort, SORT_DESC, $eventsData);
        $end_date = $eventsData[0]['date'];
        $start_date = end($eventsData)['date'];
        return $this->extractBydate($eventsData,$start_date,$end_date);
    }

    public function getStudentBlockingTasks($user_id,$student_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if(!$this->checkTeacherCanViewChild($user,$student))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $tasks_progress = $this->schoolRepository->getStudentBlockingTasks($student_id);
        $blocking_tasks = [];
        $counter = 0;
        foreach ($tasks_progress as $progress) {
            $mission = $progress->getTask()->getMissions()->first();
            if($mission != null)
            {
                $blocking_tasks[$counter]["type"] = "mission";
                $blocking_tasks[$counter]["id"] = $mission->getId();
                $blocking_tasks[$counter]["name"] = $mission->getName();
            }
            else{
                $quiz = $progress->getTask()->getQuizzes()->first();
                $blocking_tasks[$counter]["type"] = "quiz";
                $blocking_tasks[$counter]["id"] = $quiz->getId();
                $blocking_tasks[$counter]["name"] = $quiz->getTitle();
            }
            $blocking_tasks[$counter]["adventure"] = $progress->getAdventure()->getTitle();
            $blocking_tasks[$counter]["course"] = $progress->getCourse()->getName();
            $blocking_tasks[$counter]["last_date"] = Carbon::parse($progress->getDate())->toDateString();
            $blocking_tasks[$counter]["flag"] = 2;
            $counter++;
        }

        return $blocking_tasks;
    }

    public function checkTeacherCanViewChild(User $user,User $student)
    {
        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('show_StudentMainInfo',$user))
            throw new UnauthorizedException(trans("locale.no_permission"));

        $classroom = $student->getBasicClassRoom();
        $defaultGroup = $classroom->getDefaultGroup();
        $teachers = $defaultGroup->getMembersWithRole("teacher");
        $students = $defaultGroup->getStudents();
        $studentInGroup = false;
        $teacherInGroup = false;
        foreach($students as $groupStudent){
            if($groupStudent->getId() == $student->getId())
                $studentInGroup = true;
        }
        foreach ($teachers as $teacher){
            if($teacher->getId() == $user->getId())
                $teacherInGroup = true;
        }
        if(!$studentInGroup)
            throw new BadRequestException(trans("locale.student_not_in_group"));

        if(!$teacherInGroup){
            $teachers = $defaultGroup->getMembersWithRole("main_teacher");
            foreach ($teachers as $teacher){
                if($teacher->getId() == $user->getId())
                    $teacherInGroup = true;
            }
        }
        if(!$teacherInGroup)
            throw new BadRequestException(trans("locale.teacher_not_student_group"));

        return true;

    }

    public function getStudentAdventureDeadlinePassed($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

//        if (!$this->accountRepository->isAuthorized('show_StudentMainInfo', $user))
//            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $adventureDeadlines = $this->schoolRepository->getStudentAdventureDeadlinePassed($user);
        $eventsData = array();
        foreach ($adventureDeadlines as $adventureDeadline) {
            $adventureDeadlinex = $this->schoolRepository->getCourseAdventureDeadline($adventureDeadline->id);
            array_push($eventsData, ['flag' => 0
                ,'adventure_name'=>$adventureDeadlinex->getAdventure()->getTitle()
                , 'last_date' => Carbon::parse($adventureDeadlinex->getEndsAt())->format('Y-m-d')
                , 'course_name' => $adventureDeadlinex->getCourse()->getName()
                , 'classroom_name' => $adventureDeadlinex->getCourse()->getClassroom()->getName()]);
        }
        return $eventsData;
    }

    public function getStudentLastPlayedTask($user_id,$student_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if(!$this->checkTeacherCanViewChild($user,$student))
            throw new UnauthorizedException(trans("locale.no_permission"));
        $last_time = $this->schoolRepository->getStudentLastPlayedTask($student_id);
        if($last_time == null)
            return [];
        else
            return [["last_date" => $last_time->getTime(), "course" => $last_time->getCourse()->getName(), "flag" => 1]];
    }

    public function getStudentFeeds($user_id,$student_id)
    {
        $student_blocking_tasks = $this->getStudentBlockingTasks($user_id,$student_id);
        $student_adventure_deadline_passed = $this->getStudentAdventureDeadlinePassed($student_id);
        $student_last_task_played = $this->getStudentLastPlayedTask($user_id,$student_id);
        if(count($student_blocking_tasks) == 0 && count($student_adventure_deadline_passed) == 0 && count($student_last_task_played)== 0)
            return [];
        $feeds = array_merge($student_blocking_tasks,$student_adventure_deadline_passed,$student_last_task_played);
        foreach ($feeds as $key => $value) {
            $sort[$key] = strtotime($value['last_date']);
        }
        array_multisort($sort, SORT_DESC, $feeds);
        return $feeds;
    }

    public function extractBydate($eventsData,$start_date,$end_date)
    {
        $events = [];
        foreach ($eventsData as $event) {
            if($event['date'] >= $start_date && $event['date'] <= $end_date)
                $events[] = $event;
        }
        return $events;
    }
    //endregion

    // start teacher in camps
    public function checkSchoolData($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        return ['user' => $user, 'account' => $account, 'school' => $school];
    }

    public function generalTeacherStatisticsInCamp($user_id){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];
        $school = $data['school'];
        $noOfStudents = 0; $runningCamps = 0; $upcomingCamps = 0;

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $teacher))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $camps = $this->schoolRepository->getTeacherCamps($school->getId(), $teacher->getId());
        if(count($camps) > 0){
            $runningCamps = $this->schoolRepository->getRunningCampsInSchool($school->getId(), true, $teacher->getId());
            $upcomingCamps = $this->schoolRepository->getUpcomingCampsInSchool($school->getId(), true, $teacher->getId());
            $camps = $this->schoolRepository->getTeacherCamps($school->getId(), $teacher->getId());
            $students = [];
            $noOfStudents = 0;
            foreach ($camps as $camp){
                foreach ($camp->getStudents() as $student){
                    if(!in_array($student->getId(), $students)){
                        $noOfStudents++;
                        array_push($students, $student->getId());
                    }
                }
            }
        }
        return ["noStudents" => $noOfStudents, "runningCamps" => $runningCamps, "upcomingCamps" => $upcomingCamps];
    }

    public function getTopTenStudentsInCamp($user_id, $camp_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $active = $this->schoolRepository->checkIfActiveCamp($camp->getId());
        if(!$active)
            throw new BadRequestException(trans('locale.not_active_camp'));

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($camp->getSchoolCamp()->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $ifExists = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $user->getId());
        if(!$ifExists)
            throw new BadRequestException(trans("locale.teacher_not_in_camp"));

        $data = array();
        $queryData = $this->schoolRepository->getTopStudentsInCamp($account, $camp->getId());
        foreach ($queryData as $student){
            $studentx = $this->accountRepository->getUserById($student->id);
            if($student->score != 0){
                $scores = $studentx->getCampScores($camp->getId());
                $missionSolved = $scores['missions_solved'];
                $totalMissions = ($camp->getDefaultTasksCount() + $camp->getTasksCount());
                $percentageOfMissionsSolved = 0;
                if($totalMissions != 0){
                    $percentageOfMissionsSolved = ceil(($missionSolved/$totalMissions) * 100) . '%';
                }
                array_push($data, ["name" => $student->first_name.' '.$student->last_name, "camp" => $camp->getName(), "coins" => $student->coins,
                    "score" => $student->score, "progress" => $percentageOfMissionsSolved]);
            }
        }
        return $data;
    }

    public function getTeacherCampTasksChart($user_id, $from_time, $to_time){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];

        if(!$this->accountRepository->isAuthorized('get_TeacherDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $camp_ids = array();
        foreach ($user->getCamps() as $camp)
            array_push($camp_ids, $camp->getId());

        $noOfStudentSolveTask = null;
        $noOfTasksSolved = null;
        if(sizeof($camp_ids) > 0){
            $noOfStudentSolveTask = $this->schoolRepository->getStudentSolveTaskByTime($account, $from_time, $to_time, $camp_ids);
            $noOfTasksSolved = $this->schoolRepository->getTaskSolvedByStudentByTime($account, $from_time, $to_time, $camp_ids);
        }

        $startDate = Carbon::parse($from_time);
        $endDate = Carbon::parse($to_time);
        $data = array();
        for($date = $startDate; $date->lte($endDate); $date->addDay()) {
            $noOfStudentsSolveTask = 0;
            $noOfTasksSolvedPerDayNumber = 0;
            $currentDate = $date->format('Y-m-d');
            $count1 = 0; $count2 = 0;
            if($noOfStudentSolveTask != null){
                foreach ($noOfStudentSolveTask as $noOfStudentSolveTaskOne){
                    if($noOfStudentSolveTaskOne->timer == $currentDate){
                        $count1++;
                        if($count1 <= 1)
                            $noOfStudentsSolveTask = $noOfStudentSolveTaskOne->number;
                    }
                }
            }

            if($noOfTasksSolved != null){
                foreach ($noOfTasksSolved as $noOfTasksSolvedPerDay) {
                    if($noOfTasksSolvedPerDay->timer == $currentDate){
                        $count2++;
                        if($count2 <= 1)
                            $noOfTasksSolvedPerDayNumber = $noOfTasksSolvedPerDay->number;
                    }
                }
            }
            array_push($data, array('date' => $currentDate, 'no_students_solve_tasks' => $noOfStudentsSolveTask,
                'no_tasks_solved' => $noOfTasksSolvedPerDayNumber));
        }
        return $data;
    }

    public function getAllTeacherCampsByStatus($user_id, $status, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $camp_ids = array();
        $teacherCamps = $user->getCamps();
        foreach ($teacherCamps as $camp){
            array_push($camp_ids, $camp->getId());
        }

        if(count($camp_ids) > 0){
            $camps = $this->schoolRepository->getAllCampsByStatus($school->getId(), $status, $limit, $search, null, $camp_ids);
            $campsMapped = Mapper::MapEntityCollection(CampDto::class, $camps, [ActivityDto::class, ActivityDto::class]);
            $counter = 0;
            foreach ($camps as $camp){
                $campsMapped[$counter]->defaultActivities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getDefaultActivities(), [DifficultyDto::class]);
                $campsMapped[$counter]->activities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getActivities(), [DifficultyDto::class]);
                $campsMapped[$counter]->duration = Carbon::createFromTimestamp($camp->getEndsAt())->diffInDays(Carbon::createFromTimestamp($camp->getStartsAt()));
                $campsMapped[$counter]->noOfStudents = $camp->getCountOfStudents();
                $campsMapped[$counter]->noOfActivities = count($camp->getDefaultActivities()) + count($camp->getActivities());
                $counter++;
            }
            return $campsMapped;
        }
        return $camp_ids;
    }

    public function countTeacherCamps($user_id, $status, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $camp_ids = array();
        $teacherCamps = $user->getCamps();
        foreach ($teacherCamps as $camp){
            array_push($camp_ids, $camp->getId());
        }

        if(count($camp_ids) > 0){
            $numberOfCamps = $this->schoolRepository->getAllCampsByStatus($school->getId(), $status, null, $search,true, $camp_ids);
        }
        else{
            $numberOfCamps = 0;
        }
        return ["count" => $numberOfCamps];
    }

    public function getAllActiveTeacherCamps($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $camp_ids = array();
        $teacherCamps = $user->getCamps();
        foreach ($teacherCamps as $camp){
            array_push($camp_ids, $camp->getId());
        }

        if(count($camp_ids) > 0) {
            $camps = $this->schoolRepository->getActiveCampsInSchool($school->getId(), null, $camp_ids);
            $campsMapped = Mapper::MapEntityCollection(CampDto::class, $camps, [ActivityDto::class, ActivityDto::class]);
            foreach ($campsMapped as $campMapped){
                $campMapped->defaultActivities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getDefaultActivities(), [DifficultyDto::class]);
                $campMapped->activities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getActivities(), [DifficultyDto::class]);
                $campMapped->duration = Carbon::createFromTimestamp($camp->getEndsAt())->diffInDays(Carbon::createFromTimestamp($camp->getStartsAt()));
            }
            return $campsMapped;
        }
        return $camp_ids;
    }

    public function getAllActivitiesInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $activities = $this->schoolRepository->getAllDefaultActivitiesInCamp($camp->getId(), $limit, $search);
        $activitiesMapped = Mapper::MapEntityCollection(ActivityDto::class, $activities, [DifficultyDto::class, ActivityScreenshotDto::class, TagDto::class]);
        return $activitiesMapped;
    }

    public function countActivitiesInTeacherCamp($user_id, $camp_id, $search){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $countActivities = $this->schoolRepository->getAllDefaultActivitiesInCamp($camp->getId(), null, $search, true);
        return $countActivities;
    }

    public function getAllStudentsInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $students = $this->schoolRepository->getAllStudentsInCamp($camp->getId(), $limit, $search);
        $studentsMapped = Mapper::MapEntityCollection(StudentCampDto::class, $students);
        $counter = 0;
        foreach ($students as $student){
            $studentsMapped[$counter]->score = $student->getCampScores($camp_id)['total_score'];
            $counter++;
        }
        return $studentsMapped;
    }

    public function CountStudentsInTeacherCamp($user_id, $camp_id, $search){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $countStudents = $this->schoolRepository->getAllStudentsInCamp($camp->getId(), null, $search, true);
        return $countStudents;
    }

    public function getActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $ifInCamp = $this->schoolRepository->checkIfActivityInCamp($camp->getId(), $activity->getId());
        if($ifInCamp == null)
            throw new BadRequestException(trans('locale.activity_not_in_camp'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $studentsData = array();
        $students = $this->schoolRepository->getAllStudentsInCamp($camp->getId(), $limit, $search);
        array_push($studentsData, ['totalTasks' => $activity->getTasksCount(), 'data' => array()]);
        foreach ($students as $student){
            $completedTasks = $this->schoolRepository->getCompletedTasksInActivityInCamp($camp_id, $activity_id, $student->getId(), true);
            $remainingTasks = $activity->getTasksCount() - $completedTasks;
            $progress = ceil(($completedTasks * 100) / $activity->getTasksCount()) . '%';
            array_push($studentsData[0]['data'], ['name' => $student->getName(), 'username' => $student->getUsername(),
                'id' => $student->getId(), 'progress' => $progress, 'profile_image' => $student->getImage(),
                'remainingTasks' => $remainingTasks, 'completedTasks' => $completedTasks]);
        }
        return $studentsData;
    }

    public function countActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $search){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $ifInCamp = $this->schoolRepository->checkIfActivityInCamp($camp->getId(), $activity->getId());
        if($ifInCamp == null)
            throw new BadRequestException(trans('locale.activity_not_in_camp'));

        $count = $this->schoolRepository->getAllStudentsInCamp($camp->getId(), null, $search, true);
        return ['count' => $count];
    }

    public function getStudentTasksInActivity($user_id, $camp_id, $activity_id, $student_id){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $ifInCamp = $this->schoolRepository->checkIfActivityInCamp($camp->getId(), $activity->getId());
        if($ifInCamp == null)
            throw new BadRequestException(trans('locale.activity_not_in_camp'));

        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $studentInCamp = $this->schoolRepository->checkIfStudentInCamp($camp->getId(), $student->getId());
        if(!$studentInCamp)
            throw new BadRequestException(trans('locale.student_not_in_camp'));

        $counter = 0;
        $studentsData = array();
        $tasks = $this->journeyRepository->getAllTasksInActivity($activity->getId());

        array_push($studentsData, ['name' => $student->getName(), 'username' => $student->getUsername(), 'id' => $student->getId(),
            'profile_image' => $student->getImage(), 'tasks' => array()]);
        $unOrderTasks = array();
        foreach ($tasks as $task){
            if(count($task->getMissions()) == 0 && count($task->getQuizzes()) == 0){
                continue;
            }
            if(count($task->getMissions()) > 0)
                $taskName = $task->getMissions()->first()->getName();
            elseif(count($task->getQuizzes()) > 0)
                $taskName = $task->getQuizzes()->first()->getTitle();
            elseif(count($task->getMissionsHtml()) > 0)
                $taskName = $task->getMissionsHtml()->first()->getTitle();
            else
                $taskName = '';

            array_push($unOrderTasks, ['taskData' => $student->getActivityCampDataForTask($camp, $activity, $task), 'taskName' => $taskName]);
        }
        $missionTasks  = array();
        $quizTasks = array();
        $tutorialTasks = array();
        foreach ($unOrderTasks as $unOrderTask){
            if($unOrderTask['taskData']['type'] == "mission")
                array_push($missionTasks, $unOrderTask);
            else if($unOrderTask['taskData']['type'] == "quiz")
                array_push($quizTasks, $unOrderTask);
            else if($unOrderTask['taskData']['type'] == "tutorial")
                array_push($tutorialTasks, $unOrderTask);

            $studentsData[$counter]['tasks'] = array_merge($tutorialTasks, $missionTasks, $quizTasks);
        }
        return $studentsData;
    }

    public function getStudentInfoInCamp($user_id, $camp_id, $student_id){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $studentInCamp = $this->schoolRepository->checkIfStudentInCamp($camp->getId(), $student->getId());
        if(!$studentInCamp)
            throw new BadRequestException(trans('locale.student_not_in_camp'));

        $data = $student->getCampScores($camp->getId());
        $mission_score =  $data['mission_scores'];
        $quiz_score = $data['quiz_scores'];
        $missionSolved = $data['missions_solved'];
        $totalMissions = ($camp->getDefaultTasksCount() + $camp->getTasksCount());
        $totalScore = $mission_score + $quiz_score;
        $percentageOfMissionsSolved = 0;
        if($totalMissions != 0){
            $percentageOfMissionsSolved = ceil(($missionSolved/$totalMissions) * 100) . '%';
        }

        return ['username' => $student->getUsername(), 'profile_image' => $student->getImage(), 'camp_name' => $camp->getName(),
            'totalScore' => $totalScore, 'campProgress' => $percentageOfMissionsSolved, 'rank' => $student->getRankInCamp($camp)];
    }

    public function getStudentBlockingTasksInCamp($camp_id, $student_id){
        $tasksProgress = $this->schoolRepository->getStudentBlockingTasksInCamp($camp_id, $student_id);
        $blocking_tasks = [];
        $counter = 0;
        foreach ($tasksProgress as $progress) {
            $mission = $progress->getTask()->getMissions()->first();
            if($mission != null)
            {
                $blocking_tasks[$counter]["type"] = "mission";
                $blocking_tasks[$counter]["id"] = $mission->getId();
                $blocking_tasks[$counter]["name"] = $mission->getName();
            }
            else{
                $quiz = $progress->getTask()->getQuizzes()->first();
                $blocking_tasks[$counter]["type"] = "quiz";
                $blocking_tasks[$counter]["id"] = $quiz->getId();
                $blocking_tasks[$counter]["name"] = $quiz->getTitle();
            }
            $blocking_tasks[$counter]["camp"] = $progress->getCamp()->getName();
            $blocking_tasks[$counter]["activity"] = $progress->getDefaultActivity()->getName();
            $blocking_tasks[$counter]["last_date"] = Carbon::createFromTimestamp($progress->getUpdatedAt())->format('Y-m-d');
            $blocking_tasks[$counter]["flag"] = 2;
            $counter++;
        }
        return $blocking_tasks;
    }

    public function getStudentLastPlayedTaskInCamp($camp_id, $student_id){
        $last_time = $this->schoolRepository->getStudentLastPlayedTaskInCamp($camp_id, $student_id);
        if($last_time == null)
            return [];
        else
            return [["last_date" => $last_time->getCreatedAt(), "flag" => 1]];
    }

    public function getStudentFeedsInCamp($user_id, $camp_id, $student_id){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $student = $this->accountRepository->getUserById($student_id);
        if($student == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $studentInCamp = $this->schoolRepository->checkIfStudentInCamp($camp->getId(), $student->getId());
        if(!$studentInCamp)
            throw new BadRequestException(trans('locale.student_not_in_camp'));

        $student_blocking_tasks = $this->getStudentBlockingTasksInCamp($camp_id, $student_id);
        $student_last_task_played = $this->getStudentLastPlayedTaskInCamp($camp_id, $student_id);
        if(count($student_blocking_tasks) == 0 && count($student_last_task_played)== 0)
            return [];

        $feeds = array_merge($student_blocking_tasks, $student_last_task_played);
        foreach ($feeds as $key => $value) {
            $sort[$key] = strtotime($value['last_date']);
        }
        array_multisort($sort, SORT_DESC, $feeds);
        return $feeds;
    }

    public function getActivityCampStatistics($user_id, $camp_id, $activity_id){
        $data = $this->checkSchoolData($user_id);
        $teacher = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $inCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacher->getId());
        if(!$inCamp)
            throw new BadRequestException(trans('locale.teacher_not_in_camp'));

        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $ifInCamp = $this->schoolRepository->checkIfActivityInCamp($camp->getId(), $activity->getId());
        if($ifInCamp == null)
            throw new BadRequestException(trans('locale.activity_not_in_camp'));

        $countStudents = $camp->getCountOfStudents();
        $completedTasks = count($this->schoolRepository->getCompletedTasksInCampActivity($camp->getId(), $activity->getId()));
        if($countStudents == 0){
            $remainingTasks = 0;
        }
        else{
            $totalTasks = ($activity->getTasksCount()) * $countStudents;
            $remainingTasks = $totalTasks - $completedTasks;
        }

        return ["activity_name" => $activity->getName(), "noStudents" => $countStudents,
            "completedTasks" => $completedTasks, "remainingTasks" => $remainingTasks];
    }

    public function getRecentActivitiesInCamp($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $camp_ids = array();
        $teacherCamps = $user->getCamps();
        foreach ($teacherCamps as $camp){
            array_push($camp_ids, $camp->getId());
        }

        $eventsData = [];
        $upcomingCamps = $this->schoolRepository->getAllCampsByStatus($school->getId(), 'upcoming', null, null, null, $camp_ids);
        foreach ($upcomingCamps as $upcomingCamp){
            array_push($eventsData, ['flag' => 0
                ,'campId' => $upcomingCamp->getId()
                ,'campName' => $upcomingCamp->getName()
                ,'date' => $upcomingCamp->getSchoolCamp()->getStartsAt()
            ]);
        }

        $endedCamps = $this->schoolRepository->getAllCampsByStatus($school->getId(), 'ended', null, null, null, $camp_ids);
        foreach ($endedCamps as $endedCamp){
            array_push($eventsData, ['flag' => 1
                ,'campId' => $endedCamp->getId()
                ,'campName' => $endedCamp->getName()
                ,'date' => $endedCamp->getSchoolCamp()->getCampDetail()->getEndsAt()
            ]);
        }

        $progressedCamps = $this->schoolRepository->campsHasProgress($school->getId(), $camp_ids);
        if(count($progressedCamps) > 0){
            foreach ($progressedCamps as $progressedCamp){
                $campProgress = $this->schoolRepository->getLastProgressInCamp($progressedCamp->getId());
                array_push($eventsData, ['flag' => 2
                    ,'campId' => $progressedCamp->getId()
                    ,'campName' => $progressedCamp->getName()
                    ,'date' => Carbon::parse($campProgress->getUpdatedAt())->timestamp
                ]);
            }
        }
        return $eventsData;
    }

    public function getRunningTeacherCamps($user_id){
        $data = $this->checkSchoolData($user_id);
        $school = $data['school'];
        $runningCamps = $this->schoolRepository->getRunningCampsInSchool($school->getId(), null, $user_id);
        $campsMapped = Mapper::MapEntityCollection(CampNamesDto::class, $runningCamps);
        return $campsMapped;
    }
    // end teacher in camps


}