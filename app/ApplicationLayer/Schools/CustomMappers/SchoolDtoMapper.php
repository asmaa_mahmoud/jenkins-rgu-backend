<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/9/2017
 * Time: 5:10 PM
 */

namespace App\ApplicationLayer\Schools\CustomMappers;

use App\DomainModelLayer\Accounts\StripeEvent;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;

class SchoolDtoMapper
{

    public static function RequestMapper($request){
        $schoolDto = new SchoolDto();
        $schoolDto->name = $request['schoolName'];
        $schoolDto->country_code = $request['schoolCountry'];
        $schoolDto->email = $request['schoolEmail'];
        $schoolDto->address = $request['schoolAddress'];
        $schoolDto->website = $request['schoolWebsite'];
        $schoolDto->distributor_id = $request['distributor_id'];
        $schoolDto->stripe_token = $request['stripe_token'];
        $schoolDto->colorPaletteId = $request['schoolColorTheme'];
        $schoolDto->schoolLogoUrl = $request['schoolIconUrl'];
        return $schoolDto;
    }
}