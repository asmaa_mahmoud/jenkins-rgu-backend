<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 1/23/2017
 * Time: 3:34 PM
 */

namespace App\ApplicationLayer\Classrooms;


use App\DomainModelLayer\Classrooms\Classroom;
use App\ApplicationLayer\Grades\GradeDtoMapper;
use Illuminate\Http\Request;

abstract class ClassroomDtoMapper
{

    public static function MapClassroom($classrooms){
        $result = array();
        foreach($classrooms as $classroom){
            $classroomDto = new ClassroomDto();
            $classroomDto->name = $classroom->getName();
            $classroomDto->StudentsCount = $classroom->getStudentsCount();
            $classroomDto->CurriculumCount = $classroom->getCurriculumCount();
            $classroomDto->CreatedAt = $classroom->getCreatedAt();
            $classroomDto->Grade = $classroom->getGrade();
            $classroomDto->Admins = $classroom->getAdmins();
            $result[] = $classroomDto;
        }


        return $result;
    }

    public static function RequestMapper(Request $request){
        $classroomDto = new ClassroomDto();
        $classroomDto->name = $request->name;
        $classroomDto->Grade = $request->grade;
        if($request->has('user_id'))
            $classroomDto->creator_id = $request->user_id;
        return $classroomDto;


    }

    public static function MapArray(array $classrooms){
        
    }

    public static function MapClassDto(Classroom $classroom){
        $classroomDto = new ClassroomDto();
        $classroomDto->id = $classroom->getId();
        $classroomDto->name = $classroom->getName();
        $classroomDto->grade = GradeDtoMapper::MapClassDto($classroom->getGrade());
        $classroomDto->no_lessons = $classroom->getCurriculumCount();
        $classroomDto->no_students = $classroom->getStudentsCount();
        $classroomDto->average_progress = $classroom->getAverageProgress();
        $classroomDto->members = $classroom->getAllMembers();
        
        return $classroomDto;
    }
}