<?php

namespace App\ApplicationLayer\Grades;


use App\DomainModelLayer\Grades\Grade;
use Illuminate\Http\Request;

abstract class GradeDtoMapper
{
    public static function MapClassDto(Grade $grade){
        $gradeDto = new GradeDto();
        $gradeDto->name = $grade->getName();        
        return $gradeDto;
    }

        public static function MapArray($grades){
        $result = array();
        foreach ($grades as $grade){
            $gradeDto = new GradeDto(); 
            $gradeDto->name = $grade->getName();
            $result[] = $gradeDto;
        }
        return $result;
    }
    
}