<?php

namespace App\ApplicationLayer\Schools;


use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Journeys\Dtos\MinifiedJourneyDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\ApplicationLayer\Schools\Dtos\GroupRequestDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Schools\Classroom;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\CourseDto;
use App\ApplicationLayer\Journeys\Dtos\AdventureDto;
use App\ApplicationLayer\Schools\Dtos\CourseAdventureDeadlinesDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\Gees\Journey;
use App\Helpers\Mapper;
use Illuminate\Pagination\Paginator;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;
use App\ApplicationLayer\Schools\Dtos\TeacherDto;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Schools\GroupMember;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\DomainModelLayer\Schools\Group;


class CourseService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;

    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;


    }

    public function getCourseMainData($user_id,$course_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

//        if(!$this->accountRepository->isAuthorized('view_Course',$user))
//            throw new UnauthorizedException(trans("locale.no_permission"));

        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_doesn't_exist"));

        if($course->getClassroom()->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.course_doesn't_exist_school"));

        $defaultGroup = $course->getClassroom()->getDefaultGroup();
        if(!$defaultGroup->isMember($user->getId()))
            throw new BadRequestException(trans("locale.not_member_in_course"));
        $courseDto =  Mapper::MapClass(CourseDto::class,$course,[JourneyDto::class,ClassroomDto::class,CourseAdventureDeadlinesDto::class]);
        $courseDto->adventureDeadlines = Mapper::MapEntityCollection(CourseAdventureDeadlinesDto::class,$course->getAdventureDeadlines(),[CourseDto::class,AdventureDto::class]);
        $courseDto->isProgressed = $course->checkIfAnyBodyProgressed();
        return $courseDto;
    }

    public function getCourseStatistics($course_id)
    {
        $course = $this->schoolRepository->getCourseById($course_id);
        if($course == null)
            throw new BadRequestException(trans("locale.course_doesn't_exist"));

        $course_name = $course->getName();
        $no_students = 0;
        foreach ($course->getGroups() as $group) {
            $no_students += count($this->schoolRepository->getStudentsInGroup($group,null,null));
        }

        $no_quizzez = count($course->getQuizzes());
        $no_completed_missions = count($this->schoolRepository->getCompletedMissionsInCourse($course_id));
        if($no_students == 0){
            $no_never_played_missions = 0;
        }
        else{
            $total_tasks = ($course->getTasksCount()) * $no_students;
            $no_never_played_missions = $total_tasks - count($this->schoolRepository->getNeverPlayedMissionsInCourse($course_id));
        }

        return ["course_name" => $course_name, "no_students" => $no_students, "no_quizzez" => $no_quizzez, "no_completed_missions" => $no_completed_missions,
           "no_never_played_missions" => $no_never_played_missions];
    }

}