<?php


namespace App\ApplicationLayer\Schools\Dtos;


class ClassroomRequestDto
{
    public $name;
    public $grade;
    public $max_students;
    public $teachers;
    public $students;
    public $journeys;

}