<?php

namespace App\ApplicationLayer\Schools\Dtos;

class DistributorRequestDto
{
    public $id;
    public $Name;
    public $StripeCheck;
    public $image;
}