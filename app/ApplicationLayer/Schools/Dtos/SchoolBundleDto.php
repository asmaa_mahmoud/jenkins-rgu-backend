<?php

namespace App\ApplicationLayer\Schools\Dtos;



class SchoolBundleDto
{
    public $id;
    public $name;
    public $price;
    public $noStudents;
    public $noClassroom;
    public $discount;
    public $description;
}