<?php

namespace App\ApplicationLayer\Schools\Dtos;

class ChargeDto
{
    public $customer_id;
    public $amount;
    public $description;
}