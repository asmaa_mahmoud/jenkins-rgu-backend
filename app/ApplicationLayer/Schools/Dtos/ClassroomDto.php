<?php

namespace App\ApplicationLayer\Schools\Dtos;

class ClassroomDto
{
    public $id;
    public $Name;
    public $GradeName;
    public $NumberOfStudents;
    public $maxNumberOfStudents;
    public $mainTeacher;
    public $percentageOfStudentAssigned;
    public $journeys;
    public $unassignedJourneys;
}