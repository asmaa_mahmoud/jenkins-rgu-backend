<?php

namespace App\ApplicationLayer\Schools\Dtos;

class CourseDto
{
    public $id;
    public $name;
    public $TasksCount;
    public $endsAt;
    public $journey;
    public $noOfAssignments;
    public $classroom;
    public $startsAt;
    public $Assignments = [];
    public $adventureDeadlines = [];
}