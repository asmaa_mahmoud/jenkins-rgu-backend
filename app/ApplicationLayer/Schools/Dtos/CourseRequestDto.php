<?php

namespace App\ApplicationLayer\Schools\Dtos;

class CourseRequestDto
{
    public $name;
    public $endsAt;
    public $journey_id;
    public $startsAt;
    public $classroom_id;
    public $adventures_deadlines;
}