<?php

namespace App\ApplicationLayer\Schools\Dtos;

class DistributorDto
{
    public $id;
    public $Name;
    public $DefaultCheck;
    public $StripeCheck;
    public $Countries = [];
}	