<?php

namespace App\ApplicationLayer\Schools\Dtos;


class DistributorChargeDetailsDto
{
    public $students;
    public $classrooms;
    public $plan_name;
}