<?php

namespace App\ApplicationLayer\Schools\Dtos;

class StudentDto
{
    public $id;
    public $fname;
    public $lname;
    public $email;
    public $image_link;
    public $image;
    public $gender;
    public $age;
    public $username;
    public $role;
    public $classroom_id;
    public $basicClassRoom = 'object';
    public $grade;
    public $scores;
    public $studentScores;
    public $classRoomScores;
}