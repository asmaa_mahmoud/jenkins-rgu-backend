<?php

namespace App\ApplicationLayer\Schools\Dtos;


class CustomPlanDto
{
    public $students;
    public $classrooms;
    public $stripe_token;
    public $bundle_id;
    public $plan_name;
}