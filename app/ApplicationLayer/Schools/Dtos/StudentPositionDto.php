<?php
/**
 * Created by PhpStorm.
 * User: hme1000000
 * Date: 7/26/2017
 * Time: 4:10 PM
 */

namespace App\ApplicationLayer\Schools\Dtos;


class StudentPositionDto
{
    public $PositionX;
    public $PositionY;
    public $Index;
}