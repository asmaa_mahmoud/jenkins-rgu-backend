<?php

namespace App\ApplicationLayer\Schools\Dtos;



class SchoolDto
{
    public $name;
    public $country_code;
    public $email;
    public $address;
    public $website;
    public $distributor_id;
    public $stripe_token;
    public $plan;
    public $schoolLogoUrl;
    public $country;
    public $colorPaletteId;
}