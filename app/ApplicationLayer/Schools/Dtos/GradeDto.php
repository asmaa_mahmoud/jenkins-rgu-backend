<?php

namespace App\ApplicationLayer\Schools\Dtos;

class GradeDto
{
    public $name;
    public $id;
}	