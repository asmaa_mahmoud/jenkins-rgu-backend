<?php

namespace App\ApplicationLayer\Schools\Dtos;

class CampDto
{
    public $id;
    public $name;
    public $description;
    public $ageFrom;
    public $ageTo;
    public $iconUrl;
    public $startsAt;
    public $endsAt;
    public $status;
    public $activities;
    public $defaultActivities;

}