<?php

namespace App\ApplicationLayer\Schools\Dtos;

class ActivityDto
{
    public $id;
    public $name;
    public $description;
    public $ageFrom;
    public $ageTo;
    public $iconUrl;
    public $imageUrl;
    public $difficulty;
    public $screenshots = [];
    public $tags = [];

}