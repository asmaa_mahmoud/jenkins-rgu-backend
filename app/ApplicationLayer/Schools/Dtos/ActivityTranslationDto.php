<?php

namespace App\ApplicationLayer\Schools\Dtos;

class ActivityTranslationDto
{
    public $name;
    public $description;
    public $languageCode;
    public $summary;
    public $concepts;
    public $technicalDetails;
    public $prerequisites;
    public $videoUrl;
    public $activity;
    public $defaultActivity;

}