<?php

namespace App\ApplicationLayer\Schools\Dtos;

class CampRequestDto
{
    public $name;
    public $description;
    public $startdate;
    public $enddate;
    public $agefrom;
    public $ageto = NULL;
    public $numberofstudents;
    public $distributor_id;
    public $students = [];
    public $teachers = [];
    public $activities = [];
    public $iconUrl;
    public $change;

}