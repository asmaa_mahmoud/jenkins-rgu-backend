<?php

namespace App\ApplicationLayer\Schools\Dtos;

class CampNamesDto
{
    public $id;
    public $name;
}