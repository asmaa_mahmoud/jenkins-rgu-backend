<?php
/**
 * Created by PhpStorm.
 * User: hme1000000
 * Date: 7/25/2017
 * Time: 5:24 PM
 */

namespace App\ApplicationLayer\Schools\Dtos;


class AssignmentDto
{
    public $id;
    public $Name;
    public $EndDate;
    public $Type;
    public $StartDate;
}