<?php

namespace App\ApplicationLayer\Schools\Dtos;

class CountryDto
{
    public $Code;
    public $Name;
    public $DistributorsCount;
    public $Journey;
}	