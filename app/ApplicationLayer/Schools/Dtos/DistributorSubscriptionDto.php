<?php

namespace App\ApplicationLayer\Schools\Dtos;

class DistributorSubscriptionDto
{
    public $plan_name;
    public $distributor_id;
    public $bundle_id;
    public $students;
    public $classrooms;

}