<?php

namespace App\ApplicationLayer\Schools\Dtos;

class TeacherCampDto
{
    public $id;
    public $fname;
    public $lname;
    public $email;
    public $image_link;
    public $image;
    public $gender;
    public $age;
    public $username;
    public $role;
    public $camps = [];
}