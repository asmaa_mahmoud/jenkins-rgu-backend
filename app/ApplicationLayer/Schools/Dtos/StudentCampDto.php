<?php

namespace App\ApplicationLayer\Schools\Dtos;

class StudentCampDto
{
    public $id;
    public $fname;
    public $lname;
    public $email;
    public $image_link;
    public $image;
    public $gender;
    public $age;
    public $username;
    public $role;
    public $grade;
    public $camps = [];
    public $studentScores;
    public $campScore;

}