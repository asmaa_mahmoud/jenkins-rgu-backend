<?php
/**
 * Created by PhpStorm.
 * User: hatemmohamed
 * Date: 7/27/17
 * Time: 10:29 AM
 */

namespace App\ApplicationLayer\Schools\Dtos;

class CourseAdventureDeadlinesDto
{
    public $course;
    public $adventure;
    public $startsAt;
    public $endsAt;
}