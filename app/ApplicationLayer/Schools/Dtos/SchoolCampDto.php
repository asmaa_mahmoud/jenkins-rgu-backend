<?php

namespace App\ApplicationLayer\Schools\Dtos;

class SchoolCampDto
{
    public $school;
    public $camp;
    public $noOfStudents;
    public $noOfActivities;
    public $startsAt;
    public $endsAt;
    public $creator;
    public $locked;
    public $duration;


}