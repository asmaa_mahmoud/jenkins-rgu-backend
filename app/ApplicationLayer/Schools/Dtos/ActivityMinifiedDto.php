<?php

namespace App\ApplicationLayer\Schools\Dtos;

class ActivityMinifiedDto
{
    public $id;
    public $name;
    public $description;
    public $ageFrom;
    public $ageTo;
    public $iconUrl;
    public $imageUrl;
    public $difficulty;



}