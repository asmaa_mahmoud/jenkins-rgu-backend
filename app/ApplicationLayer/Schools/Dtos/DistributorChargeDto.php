<?php

namespace App\ApplicationLayer\Schools\Dtos;


class DistributorChargeDto
{
    public $students;
    public $classrooms;
    public $cost;
    public $description;
}