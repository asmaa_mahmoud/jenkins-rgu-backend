<?php

namespace App\ApplicationLayer\Schools\Dtos;


class SchoolChargeLogDto
{
    public $cost;
    public $from;
    public $to;
    public $discount;
    public $description;
    public $tax;
    public $returned;
}