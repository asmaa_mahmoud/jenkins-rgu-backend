<?php

namespace App\ApplicationLayer\Schools\Interfaces;

use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\UserRequestDto;
use App\ApplicationLayer\Schools\Dtos\CampRequestDto;
use App\ApplicationLayer\Schools\Dtos\CustomPlanDto;
use App\ApplicationLayer\Schools\Dtos\DistributorSubscriptionDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Schools\Classroom;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;

interface ISchoolMainService
{
    public function registerAdmin(UserDto $userDto,SchoolDto $schoolDto);
    public function addTeacher($userId,UserDto $userDto);
    public function addStudent($userId,StudentDto $studentDto);
    public function addStudentsCSV($userId,$file);
    public function addTeachersCSV($userId,$file);
    public function updateTeacher($user_id,UserDto $userDto,$classroom_ids);
    public function updateStudent($user_id,StudentDto $studentDto);


    public function addTeachersToClassroom($userId,Classroom $classroom,$teacherIds);
    public function addStudentsToClassroom($userId,Classroom $classroom,$studentIds);
    public function assignTeachersToClassRoom($userId,$classroomName,$teacherIds);
    public function assignStudentsToClassRoom($userId,$classroomName,$studentIds);
    public function createClassroom(ClassroomRequestDto $classroomDto,$user_id);
    public function updateClassroom($user_id,ClassroomRequestDto $classroomDto,$id,$force = false);
    public function UnassignTeacherFromClassroom($user_id,$classroom_id, $teacher_id);
    public function UnassignTeachersFromClassroom($user_id,$classroom_id, $teachers);
    public function UnassignStudentFromClassroom($user_id,$classroom_id, $student_id);
    public function UnassignStudentsFromClassroom($user_id,$classroom_id, $students);
    public function getUnEnrolledStudents($user_id,$classroom_id,$start_from,$limit,$search);
    public function getUnAssignedTeachers($user_id,$classroom_id,$start_from,$limit,$search);


    public function GetClassroom($user_id,$id);
    public function deleteClassroom($user_id, $classroom_id);
    public function getAllClassrooms($user_id,$start_from,$limit,$grade_name,$search);
    public function getStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    public function getTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search);


    public function getSchoolPlans();
    public function getAllDistributors();
    public function getDistributorsByCountryCode($country_code);
    public function getAllCountries();
    public function activateSchoolAccount($user_id,$code);
    public function getAllGrades($user_id);


    public function getAllTeachers($user_id,$start_from,$limit,$grade_name,$search);
    public function getAllStudents($user_id,$start_from,$limit,$grade_name,$search);
    public function getTeacherById($user_id,$id);
    public function getStudentById($user_id,$id);

    public function deleteTeachers($user_id,$teachers,$force = false);
    public function deleteStudents($user_id,$students);

    public function renewSubscription($user_id,$code,$subscription_id);
    public function upgradeSubscription($user_id,$code,$subscription_id,$plan_name);
    public function unsubscribe($user_id,$subscription_id);
    public function changeDistributor($user_id,$distributor_id,$subscription_id);

    public function updateSchoolInfo($user_id,SchoolDto $schoolDto);
    public function updateAdminInfo($user_id,UserDto $userDto);

    public function addMainRoleToTeacher($user_id,$classroom_id, $teacher_id);
    public function removeMainRoleFromTeacher($user_id,$classroom_id, $teacher_id);
    public function countTeachers($user_id,$grade_name,$search);
    public function countStudents($user_id,$grade_name,$search);
    public function getSchoolInfo($user_id);

    public function assignJourneysToClassroom($user_id,$classroom_id,$journeysIds);
    public function unassignJourneysFromClassroom($user_id,$classroom_id,$journeysIds);
    public function countClassrooms($user_id,$grade_name,$search);
    public function deleteClasrooms($user_id,$classrooms);
    public function getAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    public function getAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    public function setAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$unAssignedStudents,$assignedStudents);
    public function setAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$unAssignedTeachers,$assignedTeachers);


    public function getTeacherClassrooms($user_id,$start_from,$limit,$grade_name,$search);
    public function countTeacherClassrooms($user_id,$grade_name,$search);

    public function updateMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode= null);
    public function updateHTMLMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag);
    public function updateQuizProgress($user_id,$quiz_id,$adventure_id,$course_id,$success_flag,$quiz_data);
    public function getTotalScore($user_id);
    public function getCourses($user_id,$limit,$start_from,$search);
    public function getAdventures($user_id,$limit,$start_from,$search,$status= "");
    public function updateMCQMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$quiz_data,$selected_ids,$use_model_answer,$noOfBlocks = null,$timeTaken = null);


    public function createCourse($user_id,$courseRequestDto);
    public function editCourse($user_id,$courseId,$courseRequestDto);
    public function getStudentsDataByCourseId($user_id,$course_id,$adventure_id,$start_from,$limit,$search);
    public function getStudentDataByClassroomId($user_id,$classroom_id,$start_from,$limit,$search);
    public function getStudentInfoById($user_id,$student_id);


    public function getCourseData($user_id,$course_id);

    public function getStudentPosition($user_id,$course_id);
    public function updateStudentPosition($user_id,$course_id,$adventure_id,$id,$type,$index);
    public function getNextTask($course_id,$adventure_id,$id,$type);
    public function getTaskAuthorization($user_id,$course_id,$adventure_id,$id,$type,$recursive = true);
    public function getCourseMainData($user_id,$course_id);
    public function deleteCourse($user_id,$course_id);
    public function getStudentDataByClassroomIdCount($user_id,$classroom_id,$search);
    public function getStudentsDataByCourseIdCount($user_id,$course_id,$adventure_id,$search);

    public function generalStatisticsAdmin($user_id);
    public function getCourseStatistics($course_id);

    public function getAdminClassroomStatistics($user_id,$grade_id);
    public function getTopStudents($user_id,$grade_id);
    public function getTopStudentsInClassroom($user_id,$classroom_id);
    public function generalStatisticsTeacher($user_id);
    public function getClassroomScoreRange($user_id,$classroom_id);
    public function getAdminActivityEvents($user_id,$from_time,$to_time);
    public function getAdminMissionsChart($user_id,$from_time,$to_time);
    public function getTeacherMissionsChart($user_id,$from_time,$to_time);
    public function storeStudentMissionClick($user_id,$course_id);
    public function getTeacherActivityEvents($user_id,$from_time = null,$to_time = null);
    public function getStudentBlockingTasks($user_id,$student_id);
    public function getClassroomsForAdmin($user_id,$grade_id);
    public function getStudentLastPlayedTask($user_id,$student_id);
    public function getStudentFeeds($user_id,$student_id);

    public function registerInvitedSchool(UserDto $userDto, SchoolDto $schoolDto, $invitation_code);
    public function getNextTaskHtml($course_id,$adventure_id,$id,$type);
    public function AddUser(UserDto $userDto);
    public function subscribeDistributor(DistributorSubscriptionDto $distributorDto);
    public function subscribeCustom(CustomPlanDto $customDto);
    public function getSchoolInvoices($user_id, $limit, $start_from);
    public function getSchoolPlansBundle();
    public function getSchoolPrices();

    public function getSubscriptionDetails($user_id);
    public function upgradeCustom(CustomPlanDto $customDto);
    public function upgradeDistributorSubscription(CustomPlanDto $customDto);
    public function activateDistributorUpgrade($user_id, $code);
    public function buildUpgradeCustomReceipt(CustomPlanDto $customDto);
    public function checkSchoolStatus($user_id);
    public function getAllPlanNames();
    public function getLastEndedSubscription($user_id);
    public function getInvitedJourneys($user_id);
    // camps
    public function generalStatisticsCamp($user_id);
    public function getTopStudentsInCamp($user_id, $camp_id);
    public function getCampTasksChart($user_id, $from_time, $to_time);
    public function getAllCampsByStatus($user_id, $status, $start_from, $limit, $search);
    public function countCampsByStatus($user_id, $status, $search);
    public function deleteCamp($user_id, $camp_id);
    public function getCamp($user_id, $camp_id);
    public function getStudentsInCamp($user_id, $camp_id, $start_from, $limit, $search);
    public function getTeachersInCamp($user_id, $camp_id, $start_from, $limit, $search);
    public function createCamp(CampRequestDto $campDto, $user_id);
    public function getAllStudentsInCamps($user_id, $grade_name, $start_from, $limit, $search);
    public function getAllTeachersInCamps($user_id, $start_from, $limit, $search);
    public function getCountStudentsInCamps($user_id, $grade_name, $search);
    public function getCountTeachersInCamps($user_id, $search);
    public function addNewTeacherToCamp($user_id, UserDto $userDto);
    public function addNewStudentToCamp($user_id, UserDto $studentDto);
    public function updateStudentInCamp($user_id, StudentDto $studentDto);
    public function updateCamp($user_id, CampRequestDto $campRequestDto, $camp_id);
    public function setAssignedAndUnassignedStudentsInCamp($user_id, $camp_id, $unAssignedStudents, $assignedStudents);
    public function setAssignedAndUnassignedTeachersInCamp($user_id, $camp_id, $unAssignedTeachers, $assignedTeachers);
    public function getAllInActiveCampsNames($user_id);
    public function getAllActiveCampsNames($user_id);
    public function getCampActivityEvents($user_id);
    public function getCampsStatus($user_id);
    public function getRunningCamps($user_id);
    public function deleteCamps($user_id, $camp_ids);
    public function getActivityCampStatistics($user_id, $camp_id, $activity_id);
    public function getActivityInCamp($user_id, $camp_id, $activity_id, $is_default);
    public function getActivityTasksInCamp($user_id, $camp_id, $activity_id, $is_default);
    // teacher in camps
    public function generalTeacherStatisticsInCamp($user_id);
    public function getTopTenStudentsInCamp($user_id, $camp_id);
    public function getTeacherCampTasksChart($user_id, $from_time, $to_time);
    public function getAllTeacherCampsByStatus($user_id, $status, $start_from, $limit, $search);
    public function countTeacherCamps($user_id, $status, $search);
    public function getAllActiveTeacherCamps($user_id);
    public function getAllActivitiesInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search);
    public function countActivitiesInTeacherCamp($user_id, $camp_id, $search);
    public function getAllStudentsInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search);
    public function CountStudentsInTeacherCamp($user_id, $camp_id, $search);
    public function getActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $start_from, $limit, $search);
    public function countActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $search);
    public function getRecentActivitiesInCamp($user_id);
    public function getRunningTeacherCamps($user_id);
    public function activateCamp($user_id, $camp_id, $code);

    // students in camp
    public function getStudentCamps($user_id);
    public function getStudentTasksInActivity($user_id, $camp_id, $activity_id, $student_id);
    public function getStudentInfoInCamp($user_id, $camp_id, $student_id);
    public function getStudentFeedsInCamp($user_id, $camp_id, $student_id);
    public function getStudentActivities($user_id, $camp_id, $start_from, $limit, $search);
    public function getStudentActivitiesCount($user_id, $camp_id, $search);
    public function deleteStudentsCamp($user_id, $students);

}
