<?php

namespace App\ApplicationLayer\Schools;


use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Journeys\Dtos\MinifiedJourneyDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\ApplicationLayer\Schools\Dtos\GroupRequestDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Schools\Classroom;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\Gees\Journey;
use App\Helpers\Mapper;
use Illuminate\Pagination\Paginator;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\DomainModelLayer\Schools\ClassroomMember;
use App\DomainModelLayer\Schools\ClassroomJourney;
use App\ApplicationLayer\Schools\Dtos\TeacherDto;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\DomainModelLayer\Schools\GroupMember;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\DomainModelLayer\Schools\Group;


class ClassroomService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;

    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;


    }
    //endregion

    //region Functions

    public function getAllClassrooms($user_id,$start_from,$limit,$grade_name,$search)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $grade_id = null;
        if($grade_name != null) {
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw new BadRequestException(trans('locale.grade_name_not_in_school'));
            $grade_id = $grade->getId();
        }
        $classrooms = $this->schoolRepository->getAllClassrooms($school->getId(),$limit,$grade_id,$search);
        $classroomsMapped =  Mapper::MapEntityCollection(ClassroomDto::class,$classrooms,[TeacherDto::class,JourneyDto::class]);
        foreach ($classroomsMapped as $classroomMapped){
            $classroomMapped->journeys = Mapper::MapEntityCollection(MinifiedJourneyDto::class,$classroomMapped->journeys,[JourneyCategoryDto::class]);
        }
        return $classroomsMapped;
    }

    public function createClassroom(ClassroomRequestDto $classroomRequestDto,$user_id){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('create_Classroom',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $this->accountRepository->beginDatabaseTransaction();

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $grade = $this->schoolRepository->getGradeByName($school,$classroomRequestDto->grade);
        if($grade == null)
            throw new BadRequestException(trans('locale.grade_name_not_in_school'));


        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException("This user has no school");

        if(!$this->schoolRepository->checkClassroomNameUniqueInSchool($school,$classroomRequestDto->name))
            throw new BadRequestException(trans("locale.classroom_name_exist_in_school"));

        if($classroomRequestDto->max_students == null)
            $classroomRequestDto->max_students = 1500;

        $classroom = new Classroom($classroomRequestDto,$grade,$school);



        if($classroomRequestDto->journeys != null)
            $classroom = $this->addJourneysToClassroom($user_id,$classroom,$classroomRequestDto->journeys);

        $this->schoolRepository->storeClassroom($classroom);

        $groupRequestDto = new GroupRequestDto;
        $groupRequestDto->name = $classroom->getName().'_defaultgroup';
        $defaultGroup = new Group($classroom,$groupRequestDto,true);
        $this->schoolRepository->storeGroup($defaultGroup);

        if($classroomRequestDto->teachers != null)
            $classroom = $this->addTeachersToClassroom($user_id,$classroom,$classroomRequestDto->teachers,true);

        if($classroomRequestDto->students != null)
            $classroom = $this->addStudentsToClassroom($user_id,$classroom,$classroomRequestDto->students);
        $this->schoolRepository->storeClassroom($classroom);
        $this->accountRepository->commitDatabaseTransaction();

        $classroomMapped = Mapper::MapClass(ClassroomDto::class,$classroom,[TeacherDto::class,JourneyDto::class]);
        $classroomMapped->journeys = Mapper::MapEntityCollection(MinifiedJourneyDto::class,$classroomMapped->journeys,[JourneyCategoryDto::class]);
        return $classroomMapped;
    }

    public function updateClassroom($user_id,ClassroomRequestDto $classroomDto,$id,$force = false)
    {

        //dd('hi');
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('update_Classroom',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));


        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $grade = $this->schoolRepository->getGradeByName($school,$classroomDto->grade);
        if($grade == null)
            throw new BadRequestException(trans('locale.grade_name_not_in_school'));

        //$classroomDto->grade_id = $grade->id;
        $classroom = $this->schoolRepository->getClassroomById($id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        if($classroomDto->name != $classroom->getName()){
            if(!$this->schoolRepository->checkClassroomNameUniqueInSchool($school,$classroomDto->name))
                throw new BadRequestException(trans("locale.classroom_name_exist_in_school"));
        }

        $classroom->setName($classroomDto->name);
        if($classroom->getGrade()->getId() != $grade->getId()){
            $students = $classroom->getStudents();
            $studentIdsArray = array();
            foreach ($students as $student) {
                # code...
                array_push($studentIdsArray,$student->getId());
            }
            if(sizeof($students) == 0)
                $classroom->setGrade($grade);
            else if($this->schoolRepository->getIfStudentsProgressedInClassroom($classroom,$studentIdsArray))
                throw new BadRequestException(trans("locale.students_progressed_in_classroom"));
            else{
                if(!$force){
                    throw new BadRequestException(trans("locale.students_classroom_force"));
                }
                else{
                    $studentIds = array();
                    foreach ($students as $student)
                        array_push($studentIds,$student->getId());
                    $this->UnassignStudentsFromClassroom($user_id,$classroom->getId(),$studentIds);
                    $classroom->setGrade($grade);
                }
            }
        }
        $classroom->setGrade($grade);
        if($classroomDto->journeys != null)
            $this->addJourneysToClassroom($user_id,$classroom,$classroomDto->journeys);

        $this->schoolRepository->storeClassroom($classroom);

        return Mapper::MapClass(ClassroomDto::class,$classroom,[TeacherDto::class,JourneyDto::class]);
    }

    public function GetGrades()
    {
        return Mapper::MapEntityCollection(GradeDto::class,$this->gradeRepository->getAllGrades());
        //return GradeDtoMapper::MapArray($this->gradeRepository->getAllGrades());
        //return $this->gradeRepository->getAllGrades();
    }

    public function GetClassroom($user_id,$id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('view_Classroom',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $this->accountRepository->beginDatabaseTransaction();

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $classroom = $this->schoolRepository->getClassroomById($id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));
        $classroomMapped =  Mapper::MapClass(ClassroomDto::class, $classroom, [TeacherDto::class,JourneyDto::class]);

        $allJourneys  = $this->journeyRepository->GetJourneysCatalogue();
        $unassignedJourneys = array();
        $assignedJourneys = $classroom->getJourneys();
        foreach ($allJourneys as $journey)
        {
            $assigned = false;
            foreach ($assignedJourneys  as $assignedJourney){
                if($assignedJourney->getId() == $journey->getId()){
                    $assigned = true;
                }
            }
            if(!$assigned)
                array_push($unassignedJourneys, $journey);
        }
        $classroomMapped->journeys = Mapper::MapEntityCollection(MinifiedJourneyDto::class,$assignedJourneys,[JourneyCategoryDto::class]);
        $classroomMapped->unassignedJourneys = Mapper::MapEntityCollection(MinifiedJourneyDto::class,$unassignedJourneys,[JourneyCategoryDto::class]);
        return $classroomMapped;
    }

    public function addTeachersToClassroom($userId,$classroom,$teacherIds,$new = false){
        $user = $this->accountRepository->getUserById($userId);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized("add_TeacherToClassroom",$user))
            throw new BadRequestException(trans('locale.not_add_teacher_classroom'));

        if($new)
            $noOfTeachers = 0;
        else
            $noOfTeachers = sizeOf($this->getTeachersInClassroom($userId,$classroom->getId(),0,1,null));
        $classRoomEmpty = true;
        if($noOfTeachers>0)
            $classRoomEmpty = false;


        foreach ($teacherIds as $teacherId){

            $teacher = $this->accountRepository->getUserById($teacherId);

            if($teacher == null)
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans("locale.doesn't_exist"));
            $teacherRoles = $teacher->getRoles();
            $isTeacher = false;
            foreach ($teacherRoles as $teacherRole){
                if($teacherRole->getName() == "teacher")
                    $isTeacher = true;
            }
            if(!$isTeacher)
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans("locale.doesn't_exist"));
            if($classroom->isMember($teacher->getId()))
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans('locale.already_in_classroom'));

            if($classRoomEmpty){
                $role = $this->accountRepository->getRoleByName("main_teacher");
                if($role == null)
                    throw new BadRequestException(trans("locale.no_role_main_teacher"));

                $defaultGroup = $classroom->getDefaultGroup();
                $groupMember = new Groupmember($defaultGroup,$teacher,$role);
                //$defaultGroup->addGroupMember($groupMember);
                $this->schoolRepository->storeGroupMember($groupMember);
                $classRoomEmpty = false;
            }
            else{
                $role = $this->accountRepository->getRoleByName("teacher");
                if($role == null)
                    throw new BadRequestException(trans("locale.no_role_teacher"));
                $defaultGroup = $classroom->getDefaultGroup();
                $groupMember = new Groupmember($defaultGroup,$teacher,$role);
                //$defaultGroup->addGroupMember($groupMember);
                $this->schoolRepository->storeGroupMember($groupMember);
            }
        }

        return $classroom;

    }

    public function addStudentsToClassroom($userId,$classroom,$studentIds){
        $user = $this->accountRepository->getUserById($userId);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if(!$this->accountRepository->isAuthorized("add_StudentToClassroom",$user))
            throw new BadRequestException(trans('locale.not_authorized_add_student_classroom'));
        $userRoles = $user->getRoles();
        $isTeacher = false;

        foreach ($userRoles as $userRole){
            if($userRole->getName == "teacher")
                $isTeacher = true;
        }

        if($isTeacher){
            if(!$classroom->isMember($user->getId()))
                throw new BadRequestException(trans('locale.teacher is not in classroom'));
        }

        if($classroom->getMaxNumberOfStudents() < ($classroom->getNumberOfStudents()+ sizeof($studentIds)))
            throw new BadRequestException(trans('local.number_students_exceeds_max'). " "
                .($classroom->getMaxNumberOfStudents()-$classroom->getNumberOfStudents()));

        foreach ($studentIds as $studentId){
            $student = $this->accountRepository->getUserById($studentId);
            if($student == null)
                throw new BadRequestException(trans('locale.student_with_id'). " " .$studentId.trans("locale.doesn't_exist"));
            $studentRoles = $student->getRoles();
            $isStudent = false;
            foreach ($studentRoles as $studentRole){
                if($studentRole->getName() == "student")
                    $isStudent = true;
            }
            if(!$isStudent)
                throw new BadRequestException(trans('locale.student_with_id'). " " .$studentId.trans("locale.doesn't_exist"));
            if($student->getGrade()->getId() != $classroom->getGrade()->getId())
                throw new BadRequestException(trans('locale.student_grade_match_classroom'));
            if($classroom->isMember($student->getId()))
                throw new BadRequestException(trans('locale.student_with_id'). " " .$studentId. " " .trans('locale.already_in_classroom'));

            if(count($student->getGroupMembers())>0)
                throw new BadRequestException(trans('locale.student_assigned_one_classroom'));
            $role = $this->accountRepository->getRoleByName("student");
            if($role == null)
                throw new BadRequestException(trans('locale.no_role_same_student'));
            $defaultGroup = $classroom->getDefaultGroup();
            $groupMember = new GroupMember($defaultGroup,$student,$role);
            $defaultGroup->addGroupMember($groupMember);
            $this->schoolRepository->storeGroup($defaultGroup);
        }

        return $classroom;
    }

    public function assignTeachersToClassRoom($userId,$classroom_id,$teacherIds){
        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));

        $classroom =  $this->addTeachersToClassroom($userId,$classroom,$teacherIds);

        $this->schoolRepository->storeClassRoom($classroom);
        return trans('locale.teachers_added');
    }

    public function assignStudentsToClassRoom($userId,$classroom_id,$studentIds){
        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));
        $classroom =  $this->addStudentsToClassroom($userId,$classroom,$studentIds);

        $this->schoolRepository->storeClassRoom($classroom);
        return trans('locale.students_added');
    }

    public function deleteClassroom($user_id, $classroom_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('delete_Classroom',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));


        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));

        if($classroom->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));


        if(count($classroom->getJourneys()) == 0)
            $this->schoolRepository->deleteClassroom($classroom);
        else{
            $students = $classroom->getStudents();
            $studentIdsArray = array();
            foreach ($students as $student) {
                # code...
                array_push($studentIdsArray,$student->getId());
            }
            if(sizeOf($students)>0){
                if($this->schoolRepository->getIfStudentsProgressedInClassroom($classroom,$studentIdsArray))
                    throw new BadRequestException(trans("locale.students_progressed_in_classroom"));
                else
                    $this->schoolRepository->deleteClassroom($classroom);
            }
            else
                $this->schoolRepository->deleteClassroom($classroom);

        }

        return trans("locale.classroom_deleted");
    }

    public function getStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        if($user->getAccount()->getSchool()->getId() != $classroom->getSchool()->getId())
            throw new BadRequestException(trans("locale.classroom_not_same_school_user"));

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $students = $this->schoolRepository->getAllStudentsInClassroom($classroom->getId(),$limit,$search);
        return Mapper::MapEntityCollection(StudentDto::class,$students,[ClassroomDto::class,GradeDto::class]);
    }

    public function getTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        if($user->getAccount()->getSchool()->getId() != $classroom->getSchool()->getId())
            throw new BadRequestException(trans("locale.classroom_not_same_school_user"));

        if(!$this->accountRepository->isAuthorized('show_allClassroomTeachers',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $teachers = $this->schoolRepository->getAllTeachersInClassroom($classroom->getId(),$limit,$search);
        return Mapper::MapEntityCollection(UserDto::class,$teachers);
    }

    public function addMainRoleToTeacher($user_id,$classroom_id, $teacher_id)
    {
        $old_role = $this->accountRepository->getRoleByName("teacher");
        if($old_role == null)
            throw new BadRequestException(trans("locale.no_role_teacher"));

        $new_role = $this->accountRepository->getRoleByName("main_teacher");
        if($new_role == null)
            throw new BadRequestException(trans("locale.no_role_main_teacher"));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('add_MainRole',$user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $teacher = $this->accountRepository->getUserById($teacher_id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.teacher_not_exist"));

        $teacherRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        if(!$isTeacher)
            throw new BadRequestException(trans('locale.user_teacher_not_provided'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        $classroom_default_group_member = $this->schoolRepository->getDefaultGroupMember($classroom,$teacher,$old_role);
        if($classroom_default_group_member == null)
        {
            $classroom_group_member = $this->schoolRepository->getDefaultGroupMember($classroom,$teacher,$new_role);
            if($classroom_group_member != null)
                throw new BadRequestException(trans("locale.teacher_already_main_teacher"));
            else
                throw new BadRequestException(trans("locale.classroom_doesn't_contain_teacher"));
        }
        $classroom_default_group_member->setRole($new_role);

        $this->schoolRepository->storeGroupMember($classroom_default_group_member);

        return trans('locale.teacher_changed');
    }

    public function removeMainRoleFromTeacher($user_id,$classroom_id, $teacher_id)
    {
        $old_role = $this->accountRepository->getRoleByName("main_teacher");
        if($old_role == null)
            throw new BadRequestException(trans("locale.no_role_main_teacher"));

        $new_role = $this->accountRepository->getRoleByName("teacher");
        if($new_role == null)
            throw new BadRequestException(trans("locale.no_role_teacher"));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('remove_MainRole',$user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $teacher = $this->accountRepository->getUserById($teacher_id);
        if($teacher == null)
            throw new BadRequestException(trans("locale.teacher_not_exist"));

        $teacherRoles = $teacher->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        if(!$isTeacher)
            throw new BadRequestException(trans('locale.user_teacher_not_provided'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        $classroom_default_group_member = $this->schoolRepository->getDefaultGroupMember($classroom,$teacher,$old_role);
        if($classroom_default_group_member == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_contain_teacher"));

        if(count($classroom->getMembersWithRole('main_teacher')) == 1)
            throw new BadRequestException(trans("locale.classroom_must_have_one_main_teacher"));
        $classroom_default_group_member->setRole($new_role);
        $this->schoolRepository->storeGroupMember($classroom_default_group_member);
        return trans('locale.teacher_changed');
    }

    public function assignJourneysToClassroom($user_id,$classroom_id,$journeysIds){
        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));
        $classroom =  $this->addJourneysToClassroom($user_id,$classroom,$journeysIds);

        $this->schoolRepository->storeClassRoom($classroom);
        return trans('locale.journeys_added');
    }

    public function addJourneysToClassroom($user_id,Classroom $classroom,$journeysIds)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if(!$this->accountRepository->isAuthorized("add_JourneyToClassroom",$user))
            throw new BadRequestException(trans('locale.no_permission_operation'));

        $account = $user->getAccount();

        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if(count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.subscription_perios_ended"));
        $subscription = $subscriptions->last();
        $plan_info = $subscription->getPlan()->getSchoolInfo();
        $courses_count = $school->getJourneysCount() + count($journeysIds);
        if($plan_info == null)
        {
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription == null){
                $schoolCharge = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                    ->first()->getSchoolCharge();
                if($schoolCharge == null){
                    $distibutorSubscription = $this->accountRepository->getActiveSubscriptionsWithoutFree($account->getId())
                        ->first()->getDistributorSubscriptions()->first();
                    if($distibutorSubscription == null)
                        throw new BadRequestException(trans("locale.school_plan_information_missing"));
                }
            }
            else{
                $plan_info = $invitationSubscription->getInvitation()->getSchoolInfo();
                if($plan_info == null)
                    throw new BadRequestException(trans("locale.school_plan_information_missing"));
            }


        }
        if($plan_info != null)
            $max_courses  = $plan_info->getMaxNumberOfCourses();
        else{
            $max_courses = $this->schoolRepository->getMaxNoOfStudentsAndClassrooms($account->getId())["maxClassrooms"];
        }


        //dd($courses_count.'     '.$max_courses);
        if($courses_count > $max_courses)
            throw new BadRequestException(trans("locale.reached_max_classroom_journey"));

        foreach ($journeysIds as $journeysId){
            $journey = $this->journeyRepository->getJourneyById($journeysId);
            if($journey == null)
                throw new BadRequestException(trans("locale.journey_id").$journeysId.trans("locale.doesn't_exist"));
            $classroomJourney = $this->schoolRepository->getClassroomJourney($classroom->getId(),$journeysId);
            if($classroomJourney != null)
                throw new BadRequestException(trans("locale.journey_already_in_classroom"));
            if($journey->getStatus() != 'published')
                throw new CustomException(trans("locale.journey_not_published"),277);

            $classroomJourney = new ClassroomJourney($classroom,$journey);
            $classroom->addClassroomJourneys($classroomJourney);
        }

        return $classroom;
    }

    public function getUnEnrolledStudents($user_id,$classroom_id,$start_from,$limit,$search){

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));


        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        if($user->getAccount()->getSchool()->getId() != $classroom->getSchool()->getId())
            throw new BadRequestException(trans("locale.classroom_not_same_school_user"));

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $students  = $this->schoolRepository->getUnEnrolledStudents($user,$classroom,$limit,$search);


        return Mapper::MapEntityCollection(StudentDto::class,$students,[ClassroomDto::class,GradeDto::class]);

    }

    public function unassignJourneysFromClassroom($user_id,$classroom_id,$journeysIds)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if(!$this->accountRepository->isAuthorized("add_JourneyToClassroom",$user))
            throw new BadRequestException(trans('locale.no_permission_operation'));

        $account = $user->getAccount();
        foreach ($journeysIds as $journeysId){
            $journey = $this->journeyRepository->getJourneyById($journeysId);
            if($journey == null)
                throw new BadRequestException(trans("locale.journey_id").$journeysId.trans("locale.doesn't_exist"));
            $classroomJourney = $this->schoolRepository->getClassroomJourney($classroom_id,$journeysId);
            if($classroomJourney == null)
                throw new BadRequestException(trans("locale.journey_not_in_classroom"));
            $this->schoolRepository->deleteClassroomJourney($classroomJourney);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.journeys_unassigned');
    }

    public function getUnAssignedTeachers($user_id,$classroom_id,$start_from,$limit,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->getAccount()->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $user->getAccount()->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));
        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_not_exist"));

        if($user->getAccount()->getSchool()->getId() != $classroom->getSchool()->getId())
            throw new BadRequestException(trans("locale.classroom_not_same_school_user"));

        if(!$this->accountRepository->isAuthorized('show_allClassroomTeachers',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        if($limit != null){
            $page = intval($start_from/$limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $teachers = $this->schoolRepository->getUnAssignedTeachers($user,$classroom,$limit,$search);
        //dd($teachers);
        return Mapper::MapEntityCollection(TeacherDto::class,$teachers,[ClassroomDto::class,GradeDto::class]);
    }

    public function countClassrooms($user_id,$grade_name,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));
        $grade_id = null;
        if($grade_name != null) {
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw new BadRequestException("there is no grade with this name for this school");
            $grade_id = $grade->getId();
        }

        $numberOfClassrooms = $this->schoolRepository->getAllClassrooms($school->getId(),null,$grade_id,$search,true);
        return ["count"=>$numberOfClassrooms];
    }

    public function deleteClasrooms($user_id,$classrooms)
    {
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans('locale.no_school_attached'));

        if(!$this->accountRepository->isAuthorized('delete_Teacher',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        foreach ($classrooms as $classroom){
            $this->deleteClassroom($user_id,$classroom);
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.classrooms_deleted');
    }

    public function UnassignTeachersFromClassroom($user_id,$classroom_id, $teachers)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('unassign_Teacher',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));

        foreach ($teachers as $teacher) {
            $teacher = $this->accountRepository->getUserById($teacher);
            if($teacher == null)
                throw new BadRequestException(trans("locale.teacher_not_exist"));

            $teacherRoles = $teacher->getRoles();
            $isTeacher = false;
            foreach ($teacherRoles as $teacherRole){
                if($teacherRole->getName() == "teacher")
                    $isTeacher = true;
            }
            if(!$isTeacher)
                throw new BadRequestException(trans("locale.teacher_name").$teacher->getName().trans("locale.doesn't_exist"));

            if(!$classroom->isMember($teacher->getId()))
                throw new BadRequestException(trans("locale.teacher_name").$teacher->getName().trans("locale.not_in_classroom"));

//            if($teacher->hasRoleInClassRoom($classroom_id, 'main_teacher') && count($classroom->getMembersWithRole("teacher"))>0)
//                throw new BadRequestException(trans("locale.cant_unassign_main_teacher"));

            if($teacher->hasRoleInClassRoom($classroom_id, 'main_teacher'))
                throw new BadRequestException(trans("locale.cant_unassign_main_teacher"));

            $classroom_default_group_member = null;
            $role = null;
            if($teacher->hasRoleInClassRoom($classroom_id,'main_teacher'))
                $role  = $this->accountRepository->getRoleByName('main_teacher');
            else
                $role = $this->accountRepository->getRoleByName('teacher');

            if($role == null)
                throw new BadRequestException(trans("locale.no_role_with_this_name"));

            $classroom_default_group_member = $this->schoolRepository->getDefaultGroupMember($classroom,$teacher,$role);

            if($classroom_default_group_member == null)
                throw new BadRequestException(trans("locale.no_default_group_member_with_specs"));

            $this->schoolRepository->deleteGroupMember($classroom_default_group_member);
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.teachers_unassigned');
    }

    public function UnassignStudentsFromClassroom($user_id,$classroom_id, $students)
    {
        $this->accountRepository->beginDatabaseTransaction();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->accountRepository->isAuthorized('unassign_Student',$user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw  new BadRequestException(trans("locale.classroom_not_exist"));

        $studentsx = $classroom->getStudents();
        $studentIdsArray = array();
        foreach ($studentsx as $studentx) {
            array_push($studentIdsArray,$studentx->getId());
        }
//        if(sizeOf($students)>0){
//            if($this->schoolRepository->getIfStudentsProgressedInClassroom($classroom,$studentIdsArray))
//                throw new BadRequestException(trans("locale.students_progressed_in_classroom"));
//        }

        foreach ($students as $student) {
            $student = $this->accountRepository->getUserById($student);
            if($student == null)
                throw new BadRequestException(trans("locale.student_not_exist"));

            $studentRoles = $student->getRoles();
            $isStudent = false;
            foreach ($studentRoles as $studentRole){
                if($studentRole->getName() == "student")
                    $isStudent = true;
            }
            if(!$isStudent)
                throw new BadRequestException(trans("locale.student_name").$student->getName().trans("locale.doesn't_exist"));
            if(!$classroom->isMember($student->getId()))
                throw new BadRequestException(trans("locale.student_name").$student->getName().trans("locale.not_in_classroom"));


            $role = $this->accountRepository->getRoleByName('student');
            if($role == null)
                throw new BadRequestException(trans("locale.no_role_with_this_name"));

            $classroom_default_group_member = $this->schoolRepository->getDefaultGroupMember($classroom,$student,$role);
            if($classroom_default_group_member == null)
                throw new BadRequestException(trans("locale.no_default_group_member_with_specs"));

            $this->schoolRepository->deleteGroupMember($classroom_default_group_member);
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.students_unassigned_successfully");
    }

    public function getAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search)
    {
        $assignedStudents = $this->getStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
        $unassignedStudents = $this->getUnEnrolledStudents($user_id,$classroom_id,$start_from,$limit,$search);

        $classroom = $this->schoolRepository->getClassroomById($classroom_id);
        if($classroom == null)
            throw new BadRequestException(trans("locale.classroom_doesn't_exist"));

        foreach ($assignedStudents as $assignedStudent){
            $score = $this->schoolRepository->getStudentProgressInClassroom($assignedStudent->id, $classroom->getId());
            $assignedStudent->isAssigned = true;
            $assignedStudent->classroomScore = intval($score);
        }
        foreach ($unassignedStudents as $unassignedStudent){
            $unassignedStudent->isAssigned = false;
        }
        return array_merge($assignedStudents,$unassignedStudents);
    }

    public function  getAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $assignedTeachers = $this->getTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
        $unassignedTeachers = $this->getUnAssignedTeachers($user_id,$classroom_id,$start_from,$limit,$search);

        foreach ($assignedTeachers as $assignedTeacher){
            $assignedTeacher->isAssigned = true;
        }
        foreach ($unassignedTeachers as $unassignedTeacher){
            $unassignedTeacher->isAssigned = false;
        }
        return array_merge($assignedTeachers,$unassignedTeachers);
    }

    public function setAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$unAssignedStudents,$assignedStudents){
        $this->accountRepository->beginDatabaseTransaction();
        if($unAssignedStudents != null){
            $this->UnassignStudentsFromClassroom($user_id,$classroom_id,$unAssignedStudents);
        }
        if($assignedStudents != null){
            $this->assignStudentsToClassRoom($user_id,$classroom_id,$assignedStudents);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.enrollment_done");
    }

    public function setAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$unAssignedTeachers,$assignedTeachers){
        $this->accountRepository->beginDatabaseTransaction();
        if($assignedTeachers !=  null){
            $this->assignTeachersToClassRoom($user_id,$classroom_id,$assignedTeachers);
        }

        if($unAssignedTeachers != null){
            $this->UnassignTeachersFromClassroom($user_id,$classroom_id,$unAssignedTeachers);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.assignment_done");
    }

    public function getInvitedJourneys($user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if ($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        $allJourneys = array();
        $date = array();
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        if (count($subscriptions) == 0)
            throw new BadRequestException(trans("locale.no_subscriptions_account"));

        $lastSubscription = $this->accountRepository->getLastSubscription($account);
        if ($lastSubscription == null)
            throw new BadRequestException(trans("locale.no_subscriptions_account"));

        $invitationSubscription = $lastSubscription->getInvitationSubscription()->first();
        if ($invitationSubscription == null)
            throw new BadRequestException(trans("locale.subscription_not_invitation"));

        $journeys = $invitationSubscription->getInvitation()->getJourneys();
        foreach ($journeys as $key => $journey){
            $newJourney = Mapper::MapClass(JourneyDto::class, $journey->getJourney(), [JourneyCategoryDto::class]);
            $newJourney->gradeNumber = $journey->getJourney()->getData()->getGrade();
            $date[$key] = $journey->getJourney()->getCreationDate();
            array_push($allJourneys, $newJourney);
        }

        array_multisort($date, SORT_ASC, $allJourneys);
        return $allJourneys;
    }
    //endregion
}