<?php
namespace App\ApplicationLayer\Schools;

use App\ApplicationLayer\Accounts\CustomMappers\UserDtoMapper;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Accounts\Dtos\UserRequestDto;
use App\ApplicationLayer\Journeys\Dtos\ActivityScreenshotDto;
use App\ApplicationLayer\Journeys\Dtos\DifficultyDto;
use App\ApplicationLayer\Journeys\Dtos\TagDto;
use App\ApplicationLayer\Schools\Dtos\ActivityDto;
use App\ApplicationLayer\Schools\Dtos\ActivityMinifiedDto;
use App\ApplicationLayer\Schools\Dtos\ActivityTranslationDto;
use App\ApplicationLayer\Schools\Dtos\CampDto;
use App\ApplicationLayer\Schools\Dtos\CampNamesDto;
use App\ApplicationLayer\Schools\Dtos\CampRequestDto;
use App\ApplicationLayer\Schools\Dtos\GradeDto;
use App\ApplicationLayer\Schools\Dtos\SchoolCampDto;
use App\ApplicationLayer\Schools\Dtos\StudentCampDto;
use App\ApplicationLayer\Schools\Dtos\TeacherCampDto;
use App\ApplicationLayer\Schools\Dtos\TeacherDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Activity;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Schools\Camp;
use App\DomainModelLayer\Schools\CampActivity;
use App\DomainModelLayer\Schools\CampUser;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Schools\SchoolCamp;
use App\DomainModelLayer\Schools\SchoolCampDetail;
use App\DomainModelLayer\Schools\SchoolDistributorCamp;
use App\Helpers\Mapper;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Schools\Dtos\StudentDto;

class CampService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;
    }
    //endregion

    public function checkSchoolData($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if($account->getAccountType()->getName() != 'School')
            throw new BadRequestException(trans('locale.not_school_account'));

        $school = $account->getSchool();
        if($school == null)
            throw new BadRequestException(trans("locale.not_school_account"));

        return ['user' => $user, 'account' => $account, 'school' => $school];
    }

    public function generalStatisticsCamp($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $runningCamps = $this->schoolRepository->getRunningCampsInSchool($school->getId(), true);
        $upcomingCamps = $this->schoolRepository->getUpcomingCampsInSchool($school->getId(), true);
        $noOfStudents = $this->schoolRepository->getCountOfPeopleInActiveCamps($school->getId(), 'student');
        $noOfTeachers = $this->schoolRepository->getCountOfPeopleInActiveCamps($school->getId(), 'teacher');

        return ["noStudents" => $noOfStudents,
            "noTeachers" => $noOfTeachers,
            "runningCamps" => $runningCamps,
            "upcomingCamps" => $upcomingCamps];
    }

    public function getTopStudentsInCamp($user_id, $camp_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($camp->getSchoolCamp()->getSchool()->getId() != $school->getId())
            throw new BadRequestException(trans("locale.classroom_doesn't_exist_school"));

        $active = $this->schoolRepository->checkIfActiveCamp($camp->getId());
        if(!$active)
            throw new BadRequestException(trans('locale.not_active_camp'));

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $data = array();
        $queryData = $this->schoolRepository->getTopStudentsInCamp($account, $camp->getId());

        foreach ($queryData as $student){
            $studentx = $this->accountRepository->getUserById($student->id);
            if($student->score != 0){
                $scores = $studentx->getCampScores($camp->getId());
                $missionSolved = $scores['missions_solved'];
                $totalMissions = ($camp->getDefaultTasksCount() + $camp->getTasksCount());
                $percentageOfMissionsSolved = 0;
                if($totalMissions != 0){
                    $percentageOfMissionsSolved = ceil(($missionSolved/$totalMissions) * 100) . '%';
                }
                array_push($data, ["name" => $student->first_name.' '.$student->last_name, "camp" => $camp->getName(), "coins" => $student->coins,
                    "score" => $student->score, "progress" => $percentageOfMissionsSolved]);
            }
        }

        return $data;
    }

    public function getCampTasksChart($user_id, $from_time, $to_time){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $noOfStudentSolveTask = $this->schoolRepository->getStudentSolveTaskByTime($account, $from_time, $to_time);
        $noOfTasksSolved = $this->schoolRepository->getTaskSolvedByStudentByTime($account, $from_time, $to_time);
        $startDate = Carbon::parse($from_time);
        $endDate = Carbon::parse($to_time);
        $data = array();
        for($date = $startDate; $date->lte($endDate); $date->addDay()) {
            $noOfStudentsSolveTask = 0;
            $noOfTasksSolvedPerDayNumber = 0;
            $currentDate = $date->format('Y-m-d');
            $count1 = 0; $count2 = 0;
            foreach ($noOfStudentSolveTask as $noOfStudentSolveTaskOne){
                if($noOfStudentSolveTaskOne->timer == $currentDate){
                    $count1++;
                    if($count1 <= 1)
                        $noOfStudentsSolveTask = $noOfStudentSolveTaskOne->number;
                }
            }

            foreach ($noOfTasksSolved as $noOfTasksSolvedPerDay) {
                if($noOfTasksSolvedPerDay->timer == $currentDate){
                    $count2++;
                    if($count2 <= 1)
                        $noOfTasksSolvedPerDayNumber = $noOfTasksSolvedPerDay->number;
                }
            }
            array_push($data, array('date' => $currentDate, 'no_students_solve_tasks' => $noOfStudentsSolveTask,
                'no_tasks_solved' => $noOfTasksSolvedPerDayNumber));
        }
        return $data;
    }

    public function getCampActivityEvents($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AdminDashboard', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $eventsData = [];
        $upcomingCamps = $this->schoolRepository->getAllCampsByStatus($school->getId(), 'upcoming');
        foreach ($upcomingCamps as $upcomingCamp){
            array_push($eventsData, ['flag' => 0
                    ,'campId' => $upcomingCamp->getId()
                    ,'campName' => $upcomingCamp->getName()
                    ,'date' => $upcomingCamp->getSchoolCamp()->getStartsAt()
                ]);
        }

        $endedCamps = $this->schoolRepository->getAllCampsByStatus($school->getId(), 'ended');
        foreach ($endedCamps as $endedCamp){
            array_push($eventsData, ['flag' => 1
                ,'campId' => $endedCamp->getId()
                ,'campName' => $endedCamp->getName()
                ,'date' => $endedCamp->getSchoolCamp()->getCampDetail()->getEndsAt()
            ]);
        }

        $progressedCamps = $this->schoolRepository->campsHasProgress($school->getId());
        if(count($progressedCamps) > 0){
            foreach ($progressedCamps as $progressedCamp){
                $campProgress = $this->schoolRepository->getLastProgressInCamp($progressedCamp->getId());
                array_push($eventsData, ['flag' => 2
                    ,'campId' => $progressedCamp->getId()
                    ,'campName' => $progressedCamp->getName()
                    ,'date' => Carbon::parse($campProgress->getUpdatedAt())->timestamp
                ]);
            }
        }
        return $eventsData;
    }

    public function getAllCampsByStatus($user_id, $status, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $camps = $this->schoolRepository->getAllCampsByStatus($school->getId(), $status, $limit, $search);
        $campsMapped = Mapper::MapEntityCollection(CampDto::class, $camps, [ActivityDto::class, ActivityDto::class]);
        $counter = 0;
        foreach ($camps as $camp){
            $campsMapped[$counter]->defaultActivities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getDefaultActivities(), [DifficultyDto::class]);
            $campsMapped[$counter]->activities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getActivities(), [DifficultyDto::class]);
            $campsMapped[$counter]->duration = Carbon::createFromTimestamp(intval($camp->getEndsAt()))->diffInDays(Carbon::createFromTimestamp(intval($camp->getStartsAt())));
            $campsMapped[$counter]->noOfStudents = $camp->getCountOfStudents();
            $campsMapped[$counter]->noOfActivities = count($camp->getDefaultActivities()) + count($camp->getActivities());
            $counter++;
        }
        return $campsMapped;
    }

    public function countCampsByStatus($user_id, $status, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $numberOfCamps = $this->schoolRepository->getAllCampsByStatus($school->getId(), $status, null, $search,true);
        return ["count" => $numberOfCamps];
    }

    public function deleteCamp($user_id, $camp_id){
        $data = $this->checkSchoolData($user_id);
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $campSubscription = $camp->getSchoolCamp();
        if($campSubscription == null)
            throw new BadRequestException(trans("locale.no_camp_subscription"));

        if($campSubscription->getLocked() == 1)
            throw new BadRequestException(trans("locale.camp_cannot_delete"));

        $this->schoolRepository->deleteCamp($camp);
        return trans("locale.camp_deleted");
    }

    public function getCamp($user_id, $camp_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        if(!$this->accountRepository->isAuthorized('view_Classroom', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $campMapped = Mapper::MapClass(CampDto::class, $camp, [ActivityDto::class, ActivityDto::class]);
        $campMapped->students = Mapper::MapEntityCollection(StudentCampDto::class, $camp->getStudents(), [GradeDto::class, CampNamesDto::class]);
        $campMapped->teachers = Mapper::MapEntityCollection(TeacherCampDto::class, $camp->getTeachers(), [CampNamesDto::class]);
        $campMapped->actualStudents = $camp->getCountOfStudents();
        $campMapped->defaultActivities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getDefaultActivities(), [DifficultyDto::class]);
        $campMapped->activities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getActivities(), [DifficultyDto::class]);
        $campMapped->totalStudents = $camp->getSchoolCamp()->getCampDetail()->getNoOfStudents();
        return $campMapped;
    }

    public function getStudentsInCamp($user_id, $camp_id, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        if(!$this->accountRepository->isAuthorized('get_AllStudents', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $students = $this->schoolRepository->getAllStudents($account->getId(), $limit, null, $search);
        $studentsMapper = Mapper::MapEntityCollection(StudentCampDto::class, $students, [GradeDto::class, CampDto::class]);
        foreach ($studentsMapper as $studentMapper){
            $studentInCamp = $this->schoolRepository->checkIfStudentInCamp($camp->getId(), $studentMapper->id);
            if($studentInCamp)
                $studentMapper->enrolled = true;
            else
                $studentMapper->enrolled = false;
        }
        return ["students" => $studentsMapper];
    }

    public function getTeachersInCamp($user_id, $camp_id, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        if(!$this->accountRepository->isAuthorized('get_AllTeachers', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $teachers = $this->schoolRepository->getAllTeachers($account->getId(), $limit, null, $search);
        $teachersMapper = Mapper::MapEntityCollection(TeacherCampDto::class, $teachers, [CampDto::class]);
        foreach ($teachersMapper as $teacherMapper){
            $teacherInCamp = $this->schoolRepository->checkIfTeacherInCamp($camp->getId(), $teacherMapper->id);
            if($teacherInCamp)
                $teacherMapper->assigned = true;
            else
                $teacherMapper->assigned = false;
        }
        return ["teachers" => $teachersMapper];
    }

    public function createCamp(CampRequestDto $campDto, $user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('create_Classroom', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($user->getLastUserRole())
            $role = $user->getLastUserRole();
        else
            $role = 'school_admin';

        if($campDto->enddate <= $campDto->startdate)
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

        $this->accountRepository->beginDatabaseTransaction();

        if(!$this->schoolRepository->checkCampNameUniqueInSchool($school, $campDto->name))
            throw new BadRequestException(trans("locale.camp_name_exist_in_school"));

        $distributor = $this->schoolRepository->getDistributorById($campDto->distributor_id);
        if($distributor == null)
            throw new BadRequestException(trans("locale.distributor_not_exist"));

        if($campDto->ageto == "")
            $campDto->ageto = NULL;

        $camp = new Camp($campDto);
        $this->schoolRepository->storeCamp($camp);

        $schoolCamp = new SchoolCamp($school, $camp, $user, $campDto->numberofstudents, count($campDto->activities), $role, $campDto->startdate, $campDto->enddate);
        $this->schoolRepository->storeSchoolCamp($schoolCamp);

        $activationCode = $distributor->getId().Carbon::parse(Carbon::now())->timestamp.str_random(30);
        $distributorCamp = new SchoolDistributorCamp($schoolCamp, $distributor, $activationCode);
        $this->schoolRepository->storeSchoolCampDistributor($distributorCamp);

        $campDetails = new SchoolCampDetail($schoolCamp, $campDto->numberofstudents, count($campDto->activities), $campDto->enddate);
        $this->schoolRepository->storeSchoolCampDetail($campDetails);

        if($campDto->activities != null)
            $this->addActivityToCamp($user_id, $camp->getId(), $campDto->activities);

        if($campDto->students != null){
            if(count($campDto->students) > $campDto->numberofstudents){
                $campDto->numberofstudents = count($campDto->students);
            }
            $this->addStudentsToCamp($user_id, $camp->getId(), $campDto->students);
        }

        if($campDto->teachers != null)
            $this->addTeachersToCamp($user_id, $camp->getId(), $campDto->teachers);

        $campDto->startdate = Carbon::createFromTimestamp(intval($campDto->startdate))->format('Y-m-d h:i A');
        $campDto->enddate = Carbon::createFromTimestamp(intval($campDto->enddate))->format('Y-m-d h:i A');

        // send distributor mail & school mail
        $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
        $this->schoolRepository->emailCampDistributor($distributor->getName(), $campDto->name, $campDto->numberofstudents, count($campDto->activities), $distributor->getEmail(),
            $user_name, $user->getEmail(), $school->getName(), $activationCode, $campDto->startdate, $campDto->enddate);

        $this->schoolRepository->emailCampSchool($user_name, $campDto->numberofstudents, count($campDto->activities), $campDto->startdate, $campDto->enddate, $user->getEmail());

        $this->accountRepository->commitDatabaseTransaction();
        $campMapped = Mapper::MapClass(CampDto::class, $camp, [SchoolCampDto::class, ActivityDto::class, ActivityDto::class]);
        $campMapped->activities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getActivities());
        $campMapped->defaultActivities = Mapper::MapEntityCollection(ActivityMinifiedDto::class, $camp->getDefaultActivities());
        return $campMapped;
    }

    public function addTeachersToCamp($user_id, $camp_id, $teacherIds){
        $this->checkSchoolData($user_id);
        $this->accountRepository->beginDatabaseTransaction();
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        foreach ($teacherIds as $teacherId){
            $teacher = $this->accountRepository->getUserById($teacherId);
            if($teacher == null)
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans("locale.doesn't_exist"));

            $teacherRoles = $teacher->getRoles();
            $isTeacher = false;
            foreach ($teacherRoles as $teacherRole){
                if($teacherRole->getName() == "teacher")
                    $isTeacher = true;
            }
            if(!$isTeacher)
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans("locale.doesn't_exist"));

            if($camp->isMember($teacher->getId()))
                throw new BadRequestException(trans('locale.teacher_with_id')." ".$teacherId." ".trans('locale.already_in_camp'));

            $role = $this->accountRepository->getRoleByName("teacher");
            if($role == null)
                throw new BadRequestException(trans("locale.no_role_teacher"));

            $campUser = new CampUser($camp, $teacher, $role);
            $this->schoolRepository->storeCampUser($campUser);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return $camp;
    }

    public function addStudentsToCamp($user_id, $camp_id, $studentIds){
        $this->checkSchoolData($user_id);
        $this->accountRepository->beginDatabaseTransaction();
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        foreach ($studentIds as $studentId){
            $student = $this->accountRepository->getUserById($studentId);
            if($student == null)
                throw new BadRequestException(trans('locale.student_with_id')." ".$studentId." ".trans("locale.doesn't_exist"));

            $teacherRoles = $student->getRoles();
            $isStudent = false;
            foreach ($teacherRoles as $teacherRole){
                if($teacherRole->getName() == "student")
                    $isStudent = true;
            }
            if(!$isStudent)
                throw new BadRequestException(trans('locale.student_with_id')." ".$studentId." ".trans("locale.doesn't_exist"));

            if($camp->isMember($student->getId()))
                throw new BadRequestException(trans('locale.student_with_id')." ".$studentId." ".trans('locale.already_in_camp'));

            $role = $this->accountRepository->getRoleByName("student");
            if($role == null)
                throw new BadRequestException(trans("locale.no_role_same_student"));

            $campUser = new CampUser($camp, $student, $role);
            $this->schoolRepository->storeCampUser($campUser);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return $camp;
    }

    public function getAllStudentsInCamps($user_id, $grade_name, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school, $grade_name);
            if($grade == null)
                throw  new BadRequestException(trans('locale.grade_name_not_in_school'));

            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans('locale.grade_not_in_school'));

            $grade_id = $grade->getId();
        }

        $students = $this->schoolRepository->getAllStudentsInCamps($school->getId(), $grade_id, $limit, $search);
        return Mapper::MapEntityCollection(StudentCampDto::class, $students, [GradeDto::class, CampDto::class]);
    }

    public function getCountStudentsInCamps($user_id, $grade_name, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('show_allClassroomStudents', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $grade_id = null;
        if($grade_name != null){
            $grade = $this->schoolRepository->getGradeByName($school,$grade_name);
            if($grade == null)
                throw  new BadRequestException(trans('locale.grade_name_not_in_school'));

            if($grade->getSchool()->getId() != $school->getId())
                throw new BadRequestException(trans('locale.grade_not_in_school'));

            $grade_id = $grade->getId();
        }

        $countStudents = $this->schoolRepository->getAllStudentsInCamps($school->getId(), $grade_id,null, $search, true);
        return ['count' => $countStudents];
    }

    public function getAllTeachersInCamps($user_id, $start_from, $limit, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('show_allClassroomTeachers', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $teachers = $this->schoolRepository->getAllTeachersInCamps($school->getId(), $limit, $search);
        return Mapper::MapEntityCollection(TeacherCampDto::class, $teachers, [CampDto::class]);
    }

    public function getCountTeachersInCamps($user_id, $search){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('show_allClassroomTeachers', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $countTeachers = $this->schoolRepository->getAllTeachersInCamps($school->getId(), null, $search, true);
        return ['count' => $countTeachers];
    }

    public function addActivityToCamp($user_id, $camp_id, $activitiesIds){
        $this->checkSchoolData($user_id);
        $this->accountRepository->beginDatabaseTransaction();
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        foreach ($activitiesIds as $activityId){
            $activity = $this->schoolRepository->getDefaultActivityById($activityId);
            if($activity == null)
                throw new BadRequestException(trans("locale.activity_id"). $activityId .trans("locale.doesn't_exist"));

            $campActivity = $this->schoolRepository->getCampActivity($camp->getId(), $activityId);
            if($campActivity != null)
                throw new BadRequestException(trans("locale.activity_already_in_camp"));

            $lastOrder = $this->schoolRepository->getLastOrderInActivityCamp($camp->getId());
            if($lastOrder == null)
                $order = 1;
            else
                $order = ($lastOrder->getOrder() + 1);

            $campActivity = new CampActivity($camp, $order);
            $campActivity->setDefaultActivity($activity);
            $this->schoolRepository->storeCampActivity($campActivity);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return $camp;
    }

    public function addNewTeacherToCamp($user_id, UserDto $userDto){
        $data = $this->checkSchoolData($user_id);
        $MainUser = $data['user'];
        $account = $data['account'];

        $this->accountRepository->beginDatabaseTransaction();
        if(!$this->accountRepository->isAuthorized("create_Teacher", $MainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));

        $userDto->role = "teacher";

        $existingUserMail = $this->accountRepository->AlreadyRegistered($userDto);
        if($existingUserMail != null)
            throw new BadRequestException(trans('locale.user_email_exists'));

        $existingUsername = $this->accountRepository->UserNameAlreadyExist($userDto);
        if($existingUsername != null)
            throw new BadRequestException(trans('locale.username_exists'));

        $user = new User($userDto, $account);
        $role = $this->accountRepository->getRoleByName($userDto->role);
        $user->addRole($role);

        if($userDto->camps != null) {
            foreach ($userDto->camps as $camp_id) {
                $camp = $this->schoolRepository->getCampById($camp_id);
                if($camp == null)
                    throw new BadRequestException(trans('locale.camp_not_exist'));

                if($camp->isMember($user->getId()))
                    throw new BadRequestException(trans("locale.user_in_camp_with_id").$camp_id);

                $campUser = new CampUser($camp, $user, $role);
                $user->addCampUser($campUser);
            }
        }
        $this->accountRepository->addUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return UserDtoMapper::CustomerMapper($user);
    }

    public function addNewStudentToCamp($userId, UserDto $studentDto){
        $data = $this->checkSchoolData($userId);
        $mainUser = $data['user'];
        $account = $data['account'];
        $school = $data['school'];

        $this->accountRepository->beginDatabaseTransaction();
        if(!$this->accountRepository->isAuthorized("create_Student", $mainUser))
            throw new BadRequestException(trans("locale.no_permission_operation"));

        $studentDto->role = "student";

        $existingUsername = $this->schoolRepository->UserNameAlreadyExist($studentDto->username);
        if($existingUsername)
            throw new BadRequestException(trans('locale.username_exists'));

        $user = new User($studentDto, $account);

        $role = $this->accountRepository->getRoleByName($studentDto->role);
        $user->addRole($role);

        $this->accountRepository->addUser($user);
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(StudentCampDto::class, $user);
    }

    public function updateCamp($userId, CampRequestDto $campDto, $camp_id){
        $data = $this->checkSchoolData($userId);
        $user = $data['user'];
        $school = $data['school'];

        $this->accountRepository->beginDatabaseTransaction();

        if(!$this->accountRepository->isAuthorized('update_Classroom', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        if($campDto->enddate <= $campDto->startdate)
            throw new BadRequestException(trans("locale.end_date_start_date_min_time"));

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $status = $camp->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_camp"));

        if($status == 'upcoming' || $status == 'running')
            throw new BadRequestException(trans("locale.no_operation_in_active_camp"));

        if($campDto->name != $camp->getName()){
            if(!$this->schoolRepository->checkCampNameUniqueInSchool($school, $campDto->name))
                throw new BadRequestException(trans("locale.camp_name_exist_in_school"));
        }

        $camp->setName($campDto->name);
        $camp->setAgeFrom($campDto->agefrom);
        if($campDto->ageto != null)
            $camp->setAgeTo($campDto->ageto);

        $this->schoolRepository->storeCamp($camp);
        $changes = 0;

        if($campDto->change){
            // update camp activities
            if($campDto->activities != null){
                $changes++;
                $this->schoolRepository->deleteCampActivities($camp_id);
                $this->addActivityToCamp($userId, $camp_id, $campDto->activities);
            }
            // update camp users
            foreach ($camp->getCampUsers() as $campUser){
                $this->schoolRepository->deleteCampUsers($campUser);
            }
            if($campDto->students != null){
                $changes++;
                if(count($campDto->students) > $campDto->numberofstudents){
                    $campDto->numberofstudents = count($campDto->students);
                }
                $this->addStudentsToCamp($userId, $camp_id, $campDto->students);
            }
            if($campDto->teachers != null)
                $this->addTeachersToCamp($userId, $camp_id, $campDto->teachers);

            $schoolCamp = $camp->getSchoolCamp();
            if($schoolCamp == null)
                throw new BadRequestException(trans("locale.camp_not_exist"));

            $startDate = Carbon::createFromTimestamp(intval($campDto->startdate))->format('Y-m-d h:i A');
            $endDate = Carbon::createFromTimestamp(intval($campDto->enddate))->format('Y-m-d h:i A');

            $schoolCamp->setStartsAt($campDto->startdate);
            $schoolCamp->setEndsAt($campDto->enddate);
            $schoolCamp->setNoOfStudents($campDto->numberofstudents);
            $schoolCamp->setNoOfActivities(count($campDto->activities));
            $this->schoolRepository->storeSchoolCamp($schoolCamp);

            $distributorCamp = $schoolCamp->getDistributorCamp();
            if($distributorCamp == null)
                throw new BadRequestException(trans("locale.camp_not_exist"));

            if($changes > 0){
                $distributor = $distributorCamp->getDistributor();
                $activationCode = $distributor->getId().Carbon::parse(Carbon::now())->timestamp.str_random(30);
                $distributorCamp->setDistributor($distributor);
                $distributorCamp->setActivationCode($activationCode);
                $this->schoolRepository->storeSchoolCampDistributor($distributorCamp);

                $campDetails = $schoolCamp->getCampDetail();
                $campDetails->setEndsAt($campDto->enddate);
                $campDetails->setNoOfStudents($campDto->numberofstudents);
                $campDetails->setNoOfActivities(count($campDto->activities));
                $this->schoolRepository->storeSchoolCampDetail($campDetails);

                // send mails
                $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
                $this->schoolRepository->emailCampDistributor($distributor->getName(), $campDto->name, $campDto->numberofstudents, count($campDto->activities),
                    $distributor->getEmail(), $user_name, $user->getEmail(), $school->getName(), $activationCode, $startDate, $endDate);

                $this->schoolRepository->emailCampSchool($user_name, $campDto->numberofstudents, count($campDto->activities),
                    $startDate, $endDate, $user->getEmail());
            }
        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(CampDto::class, $camp, [ActivityDto::class, ActivityDto::class]);
    }

    public function setAssignedAndUnassignedStudentsInCamp($userId, $camp_id, $unAssignedStudents, $assignedStudents){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($userId);
        $user = $data['user'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw  new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $status = $camp->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_camp"));

/*        if($status == 'upcoming' || $status == 'running')
            throw new BadRequestException(trans("locale.no_operation_in_active_camp"));*/

        $role = $this->accountRepository->getRoleByName("student");
        if($role == null)
            throw new BadRequestException(trans("locale.no_role_same_student"));

        $schoolCamp = $camp->getSchoolCamp();
        if($schoolCamp == null)
            throw  new BadRequestException(trans("locale.camp_not_exist"));

        $countTotalStudents = $schoolCamp->getCampDetail()->getNoOfStudents();
        if(($countTotalStudents - count($unAssignedStudents)) < count($assignedStudents)){
            $user_name = (($user->getFirstName() != null && !empty($user->getFirstName())) ? $user->getName() : $user->getUsername());
            $distributorCamp = $schoolCamp->getDistributorCamp();
            $distributor = $distributorCamp->getDistributor();
            // new activation for new students
            $activationCode = $distributor->getId().Carbon::parse(Carbon::now())->timestamp.str_random(30);
            $distributorCamp->setActivationCode($activationCode);
            $this->schoolRepository->storeSchoolCampDistributor($distributorCamp);

            // send mails
            $this->schoolRepository->emailCampDistributor($distributor->getName(), $camp->getName(), count($assignedStudents), $schoolCamp->getCampDetail()->getNoOfActivities(),
                $distributor->getEmail(), $user_name, $user->getEmail(), $school->getName(), $activationCode, $schoolCamp->getStartsAt(), $schoolCamp->getCampDetail()->getEndsAt());

            $this->schoolRepository->emailCampSchool($user_name, count($assignedStudents), $schoolCamp->getCampDetail()->getNoOfActivities(),
                $schoolCamp->getStartsAt(), $schoolCamp->getCampDetail()->getEndsAt(), $user->getEmail());

            $schoolCamp->setNoOfStudents(count($assignedStudents));
            $this->schoolRepository->storeSchoolCamp($schoolCamp);

            $schoolCampDetail = $schoolCamp->getCampDetail();
            $schoolCampDetail->setNoOfStudents(count($assignedStudents));
            $this->schoolRepository->storeSchoolCampDetail($schoolCampDetail);
        }

        if(count($unAssignedStudents) > 0){
            foreach ($unAssignedStudents as $unAssignedStudent){
                $studentInCamp = $this->schoolRepository->checkIfStudentInCamp($camp_id, $unAssignedStudent);
                if($studentInCamp){
                    $campUser = $this->schoolRepository->getCampUser($camp_id, $unAssignedStudent);
                    $this->schoolRepository->deleteCampUser($campUser);
                }
            }
        }

        if(count($assignedStudents) > 0){
            foreach ($assignedStudents as $studentId){
                $student = $this->accountRepository->getUserById($studentId);
                if($student == null)
                    throw new BadRequestException(trans("locale.student_with_id") . $studentId . trans("locale.doesn't_exist"));

                $campUser = new CampUser($camp, $student, $role);
                $this->schoolRepository->storeCampUser($campUser);
            }
        }

        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.student_updated_successfully");
    }

    public function setAssignedAndUnassignedTeachersInCamp($userId, $camp_id, $unAssignedTeachers, $assignedTeachers){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($userId);
        $school = $data['school'];
        $user = $data['user'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw  new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $status = $camp->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.no_operation_in_ended_camp"));

/*        if($status == 'upcoming' || $status == 'running')
            throw new BadRequestException(trans("locale.no_operation_in_active_camp"));*/

        $role = $this->accountRepository->getRoleByName("teacher");
        if($role == null)
            throw new BadRequestException(trans("locale.no_role_same_student"));

        if(count($unAssignedTeachers) > 0){
            foreach ($unAssignedTeachers as $unAssignedTeacher){
                $teacherInCamp = $this->schoolRepository->checkIfTeacherInCamp($camp_id, $unAssignedTeacher);
                if($teacherInCamp){
                    $campUser = $this->schoolRepository->getCampUser($camp_id, $unAssignedTeacher);
                    $this->schoolRepository->deleteCampUser($campUser);
                }
            }
        }

        if(count($assignedTeachers) > 0){
            foreach ($assignedTeachers as $teacherId){
                $teacher = $this->accountRepository->getUserById($teacherId);
                if($teacher == null)
                    throw new BadRequestException(trans("locale.teacher_with_id") . $teacherId . trans("locale.doesn't_exist"));

                $campUser = new CampUser($camp, $teacher, $role);
                $this->schoolRepository->storeCampUser($campUser);
            }
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans("locale.teacher_updated_successfully");
    }

    public function getAllInActiveCampsNames($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $camps = $this->schoolRepository->getInActiveCampsInSchool($school->getId());
        $campsMapped = Mapper::MapEntityCollection(CampNamesDto::class, $camps);
        return $campsMapped;
    }

    public function getAllActiveCampsNames($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $camps = $this->schoolRepository->getActiveCampsInSchool($school->getId());
        $campsMapped = Mapper::MapEntityCollection(CampDto::class, $camps);
        $counter = 0;
        foreach ($camps as $camp){
            $startDate = Carbon::createFromTimestamp(intval($camp->getSchoolCamp()->getStartsAt()));
            $endDate = Carbon::createFromTimestamp(intval($camp->getSchoolCamp()->getCampDetail()->getEndsAt()));
            $campsMapped[$counter]->duration = $endDate->diffInDays($startDate);
            $counter++;
        }
        return $campsMapped;
    }

    public function getCampsStatus($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];

        $teacherRoles = $user->getRoles();
        $isTeacher = false;
        foreach ($teacherRoles as $teacherRole){
            if($teacherRole->getName() == "teacher")
                $isTeacher = true;
        }

        if($isTeacher)
            return ['ended', 'upcoming', 'running'];

        return ['locked', 'ended', 'upcoming', 'running'];
    }

    public function getRunningCamps($user_id){
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('get_AllClassrooms', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        $camps = $this->schoolRepository->getRunningCampsInSchool($school->getId());
        $campsMapped = Mapper::MapEntityCollection(CampNamesDto::class, $camps);
        return $campsMapped;
    }

    public function deleteCamps($user_id, $camps){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];

        if(!$this->accountRepository->isAuthorized('delete_Teacher', $user))
            throw new UnauthorizedException(trans('locale.no_permission_operation'));

        foreach ($camps as $camp){
            $this->deleteCamp($user_id, $camp);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.camps_deleted');
    }

    public function activateCamp($user_id, $camp_id, $code){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        if(!$this->accountRepository->isAuthorized('upgrade_subscription', $user))
            throw new UnauthorizedException(trans('locale.no_permission'));

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw  new BadRequestException(trans("locale.camp_not_exist"));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $status = $camp->getStatus();
        if($status == 'ended')
            throw new BadRequestException(trans("locale.has_ended_camp"));

        if($status == 'upcoming' || $status == 'running')
            throw new BadRequestException(trans("locale.not_active_camp"));

        $schoolCamp = $camp->getSchoolCamp();
        if($schoolCamp->getDistributorCamp() == null)
            throw new BadRequestException(trans("locale.no_camp_subscription"));

        $actualCode = $schoolCamp->getDistributorCamp()->getActivationCode();
        if($actualCode != $code)
            throw new BadRequestException(trans("locale.code_invalid"));

        $schoolCamp->setLocked(1);
        $this->schoolRepository->storeSchoolCamp($schoolCamp);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.camp_activated');
    }

    public function getActivityInCamp($user_id, $camp_id, $activity_id, $is_default = true){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $status = $camp->getStatus();
        if ($status == 'ended')
            throw new BadRequestException(trans('locale.has_ended_camp'));

//        if ($status == 'upcoming')
//            throw new BadRequestException(trans('locale.not_active_camp'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans("locale.activity_id"). $activity_id .trans("locale.doesn't_exist"));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $campActivity = $this->schoolRepository->getCampActivity($camp->getId(), $activity_id);
        if($campActivity == null)
            throw new BadRequestException(trans("locale.activity_already_in_camp"));

        $activityDto['id'] = $activity->getId();
        $activityDto['name'] = $activity->getName();
        $activityDto['difficulty'] = $activity->getDifficulty()->getName();
        $activityDto['difficultyTitle'] = $activity->getDifficulty()->getTitle();
        $activityDto['ageFrom'] = $activity->getAgeFrom();
        $activityDto['ageTo'] = $activity->getAgeTo();
        $activityDto['description'] = $activity->getDescription();
        $activityDto['imageUrl'] = $activity->getImageUrl();
        $activityDto['iconUrl'] = $activity->getIconUrl();
        $activityDto['tags'] = [];
        $activityDto['screenshots'] = [];
        foreach ($activity->getTags() as $tag){
            array_push($activityDto['tags'], $tag->getName());
        }
        foreach ($activity->getScreenshots() as $screenshot){
            array_push($activityDto['screenshots'], $screenshot->getScreenshotUrl());
        }
        return $activityDto;
    }

    public function hasAccessActivityInCamp($user_id, $camp_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $school = $account->getSchool();
        if($user->is_teacher()) {
            $teacherInCamp = $this->schoolRepository->checkIfTeacherInCamp($camp_id, $user_id);
            if(!$teacherInCamp)
                return false;
        }
        else if ($user->is_schoolAdmin()) {
            $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
            if(!$campInSchool)
                return false;
        }
        elseif ($user->is_student()){
            $studentInCamp = $this->schoolRepository->checkIfStudentInCamp($camp_id, $user_id);
            if(!$studentInCamp)
                return false;
        }
        else{
            return false;
        }
        return true;
    }

    public function isLockedTaskActivity(Task $task, $camp_id, $activity_id, $user_id, $is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->is_student())
            throw new BadRequestException(trans('locale.students_not_use_service'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $first_task = $this->journeyRepository->getFirstTaskInActivity($activity_id, $is_default);
        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->journeyRepository->getActivityTaskOrder($task, $activity_id, $is_default)->getOrder();
        if($task->getId() != $first_task->getId()) {
            $latestActivityTaskOrder = $this->schoolRepository->getLastSolvedTaskinActivity($user_id, $camp_id, $activity_id, $is_default);
            if($latestActivityTaskOrder == null)
                return true;
            $latestActivityTaskOrder = $latestActivityTaskOrder->getOrder();
            if($order <= ($latestActivityTaskOrder+1))
                return false;
            else
                return true;
        }
        else {
            return false;
        }
    }

    public function checkProgressUnlockedActivity($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if ($this->accountRepository->isAuthorized('unlock_AllTasks', $user))
            return true;

        return false;
    }

    public function getActivityTasksInCamp($user_id, $camp_id, $activity_id, $is_default = true){
        $this->accountRepository->beginDatabaseTransaction();
        $data = $this->checkSchoolData($user_id);
        $user = $data['user'];
        $school = $data['school'];

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $campInSchool = $this->schoolRepository->checkIfCampInSchool($camp_id, $school->getId());
        if(!$campInSchool)
            throw new BadRequestException(trans("locale.camp_doesnot_exist_school"));

        $status = $camp->getStatus();
        if ($status == 'ended')
            throw new BadRequestException(trans('locale.has_ended_camp'));

//        if ($status == 'upcoming')
//            throw new BadRequestException(trans('locale.not_active_camp'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans("locale.activity_id"). $activity_id .trans("locale.doesn't_exist"));

        if(!$activity->isB2B())
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $hasAccess = $this->hasAccessActivityInCamp($user_id, $camp_id);
        if(!$hasAccess)
            throw new BadRequestException(trans("locale.no_permission"));

        $tasks = $activity->getTasks();
        $tasksDto = [];

        $progressUnlocked = $this->checkProgressUnlockedActivity($user_id);
        foreach ($tasks as $task){
            $mission = $task->getMissions()->first();
            if($mission != null) {
                $locked = true;
                if($progressUnlocked)
                    $locked = false;
                else{
                    if($user->is_student())
                        $locked = $this->isLockedTask($task, $camp_id, $activity_id, $user_id, $is_default);
                    else
                        $locked = $this->isLockedTaskActivity($task, $camp_id, $activity_id, $user_id, $is_default);
                }

                array_push($tasksDto, [
                    'id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image' => $mission->getIconUrl(),
                    'locked' => $locked,
                    'xp' => $mission->getWeight(),
                    'coins' => $mission->getWeight()/5,
                    'type' => 'mission',
                    'mission_id' => $mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                ]);
            }
            else {
                $mission = $task->getMissionsHtml()->first();
                if($mission != null) {
                    $locked = true;
                    if($progressUnlocked)
                        $locked = false;
                    else{
                        if($user->is_student())
                            $locked = $this->isLockedTask($task, $camp_id, $activity_id, $user_id, $is_default);
                        else
                            $locked = $this->isLockedTaskActivity($task, $camp_id, $activity_id, $user_id, $is_default);
                    }

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'locked' => $locked,
                        'xp' => $mission->getWeight(),
                        'coins' => $mission->getWeight()/5,
                        'type' => 'html_mission',
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }
                else {
                    $quiz = $task->getQuizzes()->first();
                    $locked = true;
                    if ($progressUnlocked)
                        $locked = false;
                    else{
                        if($user->is_student())
                            $locked = $this->isLockedTask($task, $camp_id, $activity_id, $user_id, $is_default);
                        else
                            $locked = $this->isLockedTaskActivity($task, $camp_id, $activity_id, $user_id, $is_default);
                    }

                    array_push($tasksDto, [
                        'id' => $task->getId(),
                        'name' => $quiz->getTitle(),
                        'image' => $quiz->geticonURL(),
                        'locked' => $locked,
                        'xp' => $quiz->getQuestionsWeight()['weight'],
                        'coins' => $quiz->getQuestionsWeight()['weight']/5,
                        'type' => $quiz->getType()->getName(),
                        'quiz_id' => $quiz->getId(),
                        'order' => $quiz->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }
            }
        }
        return $tasksDto;
    }

    public function isLockedTask(Task $task, $camp_id, $activity_id, $user_id, $is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans("locale.user_not_exist"));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans("locale.activity_id"). $activity->getId() .trans("locale.doesn't_exist"));

        $first_task = $this->journeyRepository->getFirstTaskInActivity($activity->getId());
        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->schoolRepository->getActivityTaskOrder($activity->getId(), $task->getId(), $is_default)->getOrder();
        $progress = $this->schoolRepository->getUserProgressByOrderInActivity($user_id, intval($order) - 1, $camp_id, $activity->getId(), $is_default);
        return (($progress != null) && ($progress->getFirstSuccess() != null) ? false : true);
    }


}