<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:17 PM
 */

namespace App\ApplicationLayer\Schools;
use App\ApplicationLayer\Accounts\Dtos\UserRequestDto;
use App\ApplicationLayer\Journeys\ActivityService;
use App\ApplicationLayer\Schools\Dtos\CampRequestDto;
use App\ApplicationLayer\Schools\Dtos\CustomPlanDto;
use App\ApplicationLayer\Schools\Interfaces\ISchoolMainService;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Accounts\Dtos\UserDto;
use App\ApplicationLayer\Schools\Dtos\SchoolDto;
use App\DomainModelLayer\Schools\Classroom;
use App\ApplicationLayer\Schools\Dtos\StudentDto;
use App\ApplicationLayer\Schools\SchoolService;
use App\ApplicationLayer\Schools\StudentService;
use App\ApplicationLayer\Schools\Dtos\ClassroomDto;
use App\ApplicationLayer\Schools\Dtos\ClassroomRequestDto;
use App\ApplicationLayer\Schools\Dtos\DistributorSubscriptionDto;



class SchoolMainService implements ISchoolMainService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;

    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;

    }

    public function registerAdmin(UserDto $userDto,SchoolDto $schoolDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->registerAdmin($userDto,$schoolDto);
    }

    public function addTeacher($userId,UserDto $userDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->addTeacher($userId,$userDto);
    }

    public function addStudent($userId,StudentDto $student){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->addStudent($userId,$student);
    }

    public function addStudentsCSV($userId,$csvData){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->addStudentsCSV($userId,$csvData);
    }

    public function addTeachersCSV($userId,$csvData){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->addTeachersCSV($userId,$csvData);
    }

    public function createClassroom(ClassroomRequestDto $classroomDto,$user_id){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->createClassroom($classroomDto,$user_id);
    }

    public function addTeachersToClassroom($userId,Classroom $classroom,$teacherIds){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->addTeachersToClassroom($userId,$classroom,$teacherIds);
    }

    public function addStudentsToClassroom($userId,Classroom $classroom,$studentIds){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->addStudentsToClassroom($userId,$classroom,$studentIds);
    }

    public function assignTeachersToClassRoom($userId,$classroomName,$teacherIds){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->assignTeachersToClassRoom($userId,$classroomName,$teacherIds);
    }

    public function assignStudentsToClassRoom($userId,$classroomName,$studentIds){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->assignStudentsToClassRoom($userId,$classroomName,$studentIds);
    }

    public function UnassignTeacherFromClassroom($user_id,$classroom_id, $teacher_id)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->UnassignTeacherFromClassroom($user_id,$classroom_id, $teacher_id);
    }

    public function UnassignTeachersFromClassroom($user_id,$classroom_id, $teachers)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->UnassignTeachersFromClassroom($user_id,$classroom_id, $teachers);
    }

    public function UnassignStudentFromClassroom($user_id,$classroom_id, $student_id)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->UnassignStudentFromClassroom($user_id,$classroom_id, $student_id);
    }

    public function UnassignStudentsFromClassroom($user_id,$classroom_id, $students)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->UnassignStudentsFromClassroom($user_id,$classroom_id, $students);
    }

    public function deleteClassroom($user_id, $classroom_id)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->deleteClassroom($user_id, $classroom_id);
    }

    public function getSchoolPlans()
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getSchoolPlans();
    }

    public function getAllDistributors()
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getAllDistributors();
    }

    public function getDistributorsByCountryCode($country_code)
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getDistributorsByCountryCode($country_code);
    }

    public function getAllCountries()
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getAllCountries();
    }

    public function activateSchoolAccount($user_id,$code)
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->activateSchoolAccount($user_id,$code);
    }

    public function getAllClassrooms($user_id,$start_from,$limit,$grade_name,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getAllClassrooms($user_id,$start_from,$limit,$grade_name,$search);
    }

    public function getAllTeachers($user_id,$start_from,$limit,$grade_name,$search)
    {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getAllTeachers($user_id,$start_from,$limit,$grade_name,$search);
    }

    public function getAllStudents($user_id,$start_from,$limit,$grade_name,$search)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getAllStudents($user_id,$start_from,$limit,$grade_name,$search);
    }

    public function getTeacherById($user_id,$id)
    {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getTeacherById($user_id,$id);
    }

    public function getStudentById($user_id,$id)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getStudentById($user_id,$id);
    }

    public function updateTeacher($user_id,UserDto $userDto,$classroom_ids)
    {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->updateTeacher($user_id,$userDto,$classroom_ids);
    }

    public function updateStudent($user_id,StudentDto $studentDto)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->updateStudent($user_id,$studentDto);
    }

    public function deleteTeachers($user_id,$teachers,$force = false)
    {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->deleteTeachers($user_id,$teachers,$force);
    }

    public function deleteStudents($user_id,$students)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->deleteStudents($user_id,$students);

    }

    public function getStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    }

    public function getTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    }

    public function renewSubscription($user_id,$code,$subscription_id)
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->renewSubscription($user_id,$code,$subscription_id);
    }

    public function upgradeSubscription($user_id,$code,$subscription_id,$plan_name)
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->upgradeSubscription($user_id,$code,$subscription_id,$plan_name);
    }

    public function unsubscribe($user_id,$subscription_id)
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->unsubscribe($user_id,$subscription_id);
    }

    public function changeDistributor($user_id,$distributor_id,$subscription_id)
    {
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->changeDistributor($user_id,$distributor_id,$subscription_id);
    }

    public function updateSchoolInfo($user_id,SchoolDto $schoolDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->updateSchoolInfo($user_id,$schoolDto);
    }

    public function updateAdminInfo($user_id,UserDto $userDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->updateAdminInfo($user_id,$userDto);
    }

    public function addMainRoleToTeacher($user_id,$classroom_id, $teacher_id)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->addMainRoleToTeacher($user_id,$classroom_id, $teacher_id);
    }

    public function removeMainRoleFromTeacher($user_id,$classroom_id, $teacher_id)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->removeMainRoleFromTeacher($user_id,$classroom_id, $teacher_id);
    }

    public function countTeachers($user_id,$grade_name,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->countTeachers($user_id,$grade_name,$search);
    }

    public function countStudents($user_id,$grade_name,$search){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->countStudents($user_id,$grade_name,$search);
    }

    public function getSchoolInfo($user_id){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getSchoolInfo($user_id);
    }

    public function GetClassroom($user_id,$id){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->GetClassroom($user_id,$id);
    }

    public function getAllGrades($user_id){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getAllGrades($user_id);
    }

    public function updateClassroom($user_id,ClassroomRequestDto $classroomDto,$id,$force = false){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->updateClassroom($user_id,$classroomDto,$id,$force);
    }

    public function assignJourneysToClassroom($user_id,$classroom_id,$journeysIds)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->assignJourneysToClassroom($user_id,$classroom_id,$journeysIds);
    }

    public function unassignJourneysFromClassroom($user_id,$classroom_id,$journeysIds)
    {
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->unassignJourneysFromClassroom($user_id,$classroom_id,$journeysIds);
    }

    public function getUnEnrolledStudents($user_id,$classroom_id,$start_from,$limit,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getUnEnrolledStudents($user_id,$classroom_id,$start_from,$limit,$search);
    }

    public function getUnAssignedTeachers($user_id,$classroom_id,$start_from,$limit,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getUnAssignedTeachers($user_id,$classroom_id,$start_from,$limit,$search);
    }

    public function countClassrooms($user_id,$grade_name,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->countClassrooms($user_id,$grade_name,$search);
    }

    public function deleteClasrooms($user_id,$classrooms){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->deleteClasrooms($user_id,$classrooms);
    }

    public function getAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    }

    public function  getAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->getAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$start_from,$limit,$search);
    }

    public function setAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$unAssignedStudents,$assignedStudents){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->setAssignedAndUnassignedStudentsInClassroom($user_id,$classroom_id,$unAssignedStudents,$assignedStudents);
    }

    public function setAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$unAssignedTeachers,$assignedTeachers){
        $classroomService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $classroomService->setAssignedAndUnassignedTeachersInClassroom($user_id,$classroom_id,$unAssignedTeachers,$assignedTeachers);
    }

    public function getTeacherClassrooms($user_id,$start_from,$limit,$grade_name,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getTeacherClassrooms($user_id,$start_from,$limit,$grade_name,$search);
    }

    public function countTeacherClassrooms($user_id,$grade_name,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->countTeacherClassrooms($user_id,$grade_name,$search);
    }

    public function updateMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode= null)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->updateMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode);
    }

    public function updateHTMLMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->updateHTMLMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag);
    }

    public function updateQuizProgress($user_id,$quiz_id,$adventure_id,$course_id,$success_flag,$quiz_data)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->updateQuizProgress($user_id,$quiz_id,$adventure_id,$course_id,$success_flag,$quiz_data);
    }

    public function getTotalScore($user_id)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getTotalScore($user_id);
    }

    public function getCourses($user_id,$limit,$start_from,$search)
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->getCourses($user_id,$limit,$start_from,$search);
    }
    public function getAdventures($user_id,$limit,$start_from,$search,$status= "")
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->getAdventures($user_id,$limit,$start_from,$search,$status);
    }
    public function getTeacherCourses($user_id,$start_from,$limit,$classroom_id,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getTeacherCourses($user_id,$start_from,$limit,$classroom_id,$search);
    }

    public function countTeacherCourses($user_id,$classroom_id,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->countTeacherCourses($user_id,$classroom_id,$search);
    }

    public function createCourse($user_id,$courseRequestDto){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->createCourse($user_id,$courseRequestDto);
    }
    public function editCourse($user_id,$courseId,$courseRequestDto){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->editCourse($user_id,$courseId,$courseRequestDto);
    }

    public function getStudentsDataByCourseId($user_id,$course_id,$adventure_id,$start_from,$limit,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getStudentsDataByCourseId($user_id,$course_id,$adventure_id,$start_from,$limit,$search);
    }
    public function getStudentDataByClassroomId($user_id,$classroom_id,$start_from,$limit,$search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getStudentDataByClassroomId($user_id,$classroom_id,$start_from,$limit,$search);
    }


    public function  getStudentInfoById($user_id,$student_id)
    {
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getStudentInfoById($user_id, $student_id);
    }
    public function getCourseData($user_id,$course_id)
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->getCourseData($user_id,$course_id);
    }

    public function getStudentPosition($user_id,$course_id)
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->getStudentPosition($user_id,$course_id);
    }
    public function updateStudentPosition($user_id,$course_id,$adventure_id,$id,$type,$index)
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->updateStudentPosition($user_id,$course_id,$adventure_id,$id,$type,$index);
    }
    public function getNextTask($course_id,$adventure_id,$id,$type)
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->getNextTask($course_id,$adventure_id,$id,$type);
    }
    public function getTaskAuthorization($user_id,$course_id,$adventure_id,$id,$type,$recursive = true)
    {
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->getTaskAuthorization($user_id,$course_id,$adventure_id,$id,$type,$recursive);

    }
    public function getCourseMainData($user_id,$course_id){
        $courseService = new CourseService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $courseService->getCourseMainData($user_id,$course_id);
    }

    public function deleteCourse($user_id,$course_id){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->deleteCourse($user_id,$course_id);
    }

    public function getStudentDataByClassroomIdCount($user_id,$classroom_id,$search){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getStudentDataByClassroomIdCount($user_id,$classroom_id,$search);
    }

    public function getStudentsDataByCourseIdCount($user_id,$course_id,$adventure_id,$search){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getStudentsDataByCourseIdCount($user_id,$course_id,$adventure_id,$search);
    }


    public function generalStatisticsAdmin($user_id)
    {
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->generalStatisticsAdmin($user_id);
    }

    public function getCourseStatistics($course_id)
    {
        $courseService = new CourseService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $courseService->getCourseStatistics($course_id);
    }
    public function getTopStudents($user_id,$grade_id){
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->getTopStudents($user_id,$grade_id);
    }

    public function getAdminClassroomStatistics($user_id,$grade_id)
    {
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->getAdminClassroomStatistics($user_id,$grade_id);
    }

    public function getTopStudentsInClassroom($user_id,$classroom_id){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getTopStudentsInClassroom($user_id,$classroom_id);
    }

    public function generalStatisticsTeacher($user_id){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->generalStatisticsTeacher($user_id);
    }
    public function getClassroomScoreRange($user_id,$classroom_id){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getClassroomScoreRange($user_id,$classroom_id);
    }

    public function getAdminActivityEvents($user_id,$from_time,$to_time){
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->getAdminActivityEvents($user_id,$from_time,$to_time);
    }

    public function getAdminMissionsChart($user_id,$from_time,$to_time){
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->getAdminMissionsChart($user_id,$from_time,$to_time);
    }

    public function getTeacherMissionsChart($user_id,$from_time,$to_time){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getTeacherMissionsChart($user_id,$from_time,$to_time);
    }

    public function storeStudentMissionClick($user_id,$course_id){
        $studentService = new StudentService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $studentService->storeStudentMissionClick($user_id,$course_id);
    }
    public function getTeacherActivityEvents($user_id,$from_time = null,$to_time = null){
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getTeacherActivityEvents($user_id,$from_time,$to_time);
    }

    public function updateMCQMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$quiz_data,$selected_ids,$use_model_answer,$noOfBlocks = null,$timeTaken = null)
    {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->updateMCQMissionProgress($user_id,$mission_id,$adventure_id,$course_id,$success_flag,$quiz_data,$selected_ids,$use_model_answer,$noOfBlocks,$timeTaken);
    }

    public function getStudentBlockingTasks($user_id,$student_id)
    {
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getStudentBlockingTasks($user_id,$student_id);
    }

    public function getClassroomsForAdmin($user_id,$grade_id){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getClassroomsForAdmin($user_id,$grade_id);
    }

    public function getStudentLastPlayedTask($user_id,$student_id)
    {
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getStudentLastPlayedTask($user_id,$student_id);
    }

    public function getStudentFeeds($user_id,$student_id)
    {
        $teacherService = new TeacherService($this->accountRepository, $this->schoolRepository, $this->journeyRepository);
        return $teacherService->getStudentFeeds($user_id,$student_id);
    }

    public function registerInvitedSchool(UserDto $userDto, SchoolDto $schoolDto, $invitation_code)
    {
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->registerInvitedSchool($userDto, $schoolDto, $invitation_code);
    }

    public function getNextTaskHtml($course_id,$adventure_id,$id,$type){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getNextTaskHtml($course_id,$adventure_id,$id,$type);
    }

    public function AddUser(UserDto $userDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->AddUser($userDto);
    }

    public function subscribeDistributor(DistributorSubscriptionDto $distributorDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->subscribeDistributor($distributorDto);
    }

    public function subscribeCustom(CustomPlanDto $customDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->subscribeCustom($customDto);
    }

    public function getSchoolInvoices($user_id, $limit, $start_from)
    {
        $schoolService = new SchoolService($this->accountRepository, $this->schoolRepository);
        return $schoolService->getSchoolInvoices($user_id, $limit, $start_from);
    }

    public function getSchoolPlansBundle(){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getSchoolPlansBundle();
    }

    public function getSchoolPrices(){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getSchoolPrices();
    }

    public function getSubscriptionDetails($user_id){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getSubscriptionDetails($user_id);
    }

    public function upgradeCustom(CustomPlanDto $customDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->upgradeCustom($customDto);
    }

    public function upgradeDistributorSubscription(CustomPlanDto $customDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->upgradeDistributorSubscription($customDto);
    }

    public function activateDistributorUpgrade($user_id, $code){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->activateDistributorUpgrade($user_id, $code);
    }

    public function buildUpgradeCustomReceipt(CustomPlanDto $customDto){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->buildUpgradeCustomReceipt($customDto);
    }

    public function checkSchoolStatus($user_id){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->checkSchoolStatus($user_id);
    }

    public function getAllPlanNames(){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getAllPlanNames();
    }

    public function getLastEndedSubscription($user_id){
        $schoolService = new SchoolService($this->accountRepository,$this->schoolRepository);
        return $schoolService->getLastEndedSubscription($user_id);
    }

    public function getInvitedJourneys($user_id){
        $schoolService = new ClassroomService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $schoolService->getInvitedJourneys($user_id);
    }

    public function generalStatisticsCamp($user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->generalStatisticsCamp($user_id);
    }

    public function getCampsStatus($user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getCampsStatus($user_id);
    }

    public function getRunningCamps($user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getRunningCamps($user_id);
    }

    public function deleteCamps($user_id, $camp_ids){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->deleteCamps($user_id, $camp_ids);
    }

    public function getCampActivityEvents($user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getCampActivityEvents($user_id);
    }

    public function getTopStudentsInCamp($user_id, $camp_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getTopStudentsInCamp($user_id, $camp_id);
    }

    public function getCampTasksChart($user_id, $from_time, $to_time){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getCampTasksChart($user_id, $from_time, $to_time);
    }

    public function getAllCampsByStatus($user_id, $status, $start_from, $limit, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getAllCampsByStatus($user_id, $status, $start_from, $limit, $search);
    }

    public function countCampsByStatus($user_id, $status, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->countCampsByStatus($user_id, $status, $search);
    }

    public function deleteCamp($user_id, $camp_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->deleteCamp($user_id, $camp_id);
    }

    public function getCamp($user_id, $camp_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getCamp($user_id, $camp_id);
    }

    public function getStudentsInCamp($user_id, $camp_id, $start_from, $limit, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getStudentsInCamp($user_id, $camp_id, $start_from, $limit, $search);
    }

    public function getTeachersInCamp($user_id, $camp_id, $start_from, $limit, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getTeachersInCamp($user_id, $camp_id, $start_from, $limit, $search);
    }

    public function getAllStudentsInCamps($user_id, $grade_name, $start_from, $limit, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getAllStudentsInCamps($user_id, $grade_name, $start_from, $limit, $search);
    }

    public function getAllTeachersInCamps($user_id, $start_from, $limit, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getAllTeachersInCamps($user_id, $start_from, $limit, $search);
    }

    public function createCamp(CampRequestDto $campDto, $user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->createCamp($campDto, $user_id);
    }

    public function getCountStudentsInCamps($user_id, $grade_name, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getCountStudentsInCamps($user_id, $grade_name, $search);
    }

    public function getCountTeachersInCamps($user_id, $search){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getCountTeachersInCamps($user_id, $search);
    }

    public function addNewTeacherToCamp($user_id, UserDto $userDto){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->addNewTeacherToCamp($user_id, $userDto);
    }

    public function addNewStudentToCamp($user_id, UserDto $studentDto){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->addNewStudentToCamp($user_id, $studentDto);
    }

    public function updateStudentInCamp($user_id, StudentDto $studentDto){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->updateStudentInCamp($user_id, $studentDto);
    }

    public function updateCamp($user_id, CampRequestDto $campRequestDto, $camp_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->updateCamp($user_id, $campRequestDto, $camp_id);
    }

    public function setAssignedAndUnassignedStudentsInCamp($user_id, $camp_id, $unAssignedStudents, $assignedStudents){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->setAssignedAndUnassignedStudentsInCamp($user_id, $camp_id, $unAssignedStudents, $assignedStudents);
    }

    public function setAssignedAndUnassignedTeachersInCamp($user_id, $camp_id, $unAssignedTeachers, $assignedTeachers){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->setAssignedAndUnassignedTeachersInCamp($user_id, $camp_id, $unAssignedTeachers, $assignedTeachers);
    }

    public function getAllInActiveCampsNames($user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getAllInActiveCampsNames($user_id);
    }

    public function getAllActiveCampsNames($user_id){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getAllActiveCampsNames($user_id);
    }

    public function generalTeacherStatisticsInCamp($user_id){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->generalTeacherStatisticsInCamp($user_id);
    }

    public function getTopTenStudentsInCamp($user_id, $camp_id){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getTopTenStudentsInCamp($user_id, $camp_id);
    }

    public function getTeacherCampTasksChart($user_id, $from_time, $to_time){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getTeacherCampTasksChart($user_id, $from_time, $to_time);
    }

    public function getAllTeacherCampsByStatus($user_id, $status, $start_from, $limit, $search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getAllTeacherCampsByStatus($user_id, $status, $start_from, $limit, $search);
    }

    public function countTeacherCamps($user_id, $status, $search) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->countTeacherCamps($user_id, $status, $search);
    }

    public function getAllActiveTeacherCamps($user_id){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getAllActiveTeacherCamps($user_id);
    }

    public function getAllActivitiesInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getAllActivitiesInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search);
    }

    public function countActivitiesInTeacherCamp($user_id, $camp_id, $search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->countActivitiesInTeacherCamp($user_id, $camp_id, $search);
    }

    public function getAllStudentsInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getAllStudentsInTeacherCamp($user_id, $camp_id, $start_from, $limit, $search);
    }

    public function CountStudentsInTeacherCamp($user_id, $camp_id, $search) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->CountStudentsInTeacherCamp($user_id, $camp_id, $search);
    }

    public function getActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $start_from, $limit, $search) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $start_from, $limit, $search);
    }

    public function countActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $search) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->countActivityStudentsInTeacherCamp($user_id, $camp_id, $activity_id, $search);
    }

    public function getStudentTasksInActivity($user_id, $camp_id, $activity_id, $student_id) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getStudentTasksInActivity($user_id, $camp_id, $activity_id, $student_id);
    }

    public function getStudentInfoInCamp($user_id, $camp_id, $student_id) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getStudentInfoInCamp($user_id, $camp_id, $student_id);
    }

    public function getStudentFeedsInCamp($user_id, $camp_id, $student_id) {
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getStudentFeedsInCamp($user_id, $camp_id, $student_id);
    }

    public function getStudentCamps($user_id) {
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getStudentCamps($user_id);
    }

    public function getStudentActivities($user_id, $camp_id, $start_from, $limit, $search){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getStudentActivities($user_id, $camp_id, $start_from, $limit, $search);
    }

    public function getStudentActivitiesCount($user_id, $camp_id, $search){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->getStudentActivitiesCount($user_id, $camp_id, $search);
    }

    public function deleteStudentsCamp($user_id, $students){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->deleteStudentsCamp($user_id, $students);
    }

    public function getRecentActivitiesInCamp($user_id){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getRecentActivitiesInCamp($user_id);
    }

    public function getRunningTeacherCamps($user_id){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getRunningTeacherCamps($user_id);
    }

    public function getActivityCampStatistics($user_id, $camp_id, $activity_id){
        $teacherService = new TeacherService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $teacherService->getActivityCampStatistics($user_id, $camp_id, $activity_id);
    }

    public function getActivityInCamp($user_id, $camp_id, $activity_id, $is_default){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getActivityInCamp($user_id, $camp_id, $activity_id, $is_default);
    }

    public function getActivityTasksInCamp($user_id, $camp_id, $activity_id, $is_default){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->getActivityTasksInCamp($user_id, $camp_id, $activity_id, $is_default);
    }

    public function activateCamp($user_id, $camp_id, $code){
        $campService = new CampService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $campService->activateCamp($user_id, $camp_id, $code);
    }

}