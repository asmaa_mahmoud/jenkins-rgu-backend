<?php

namespace App\ApplicationLayer\Journeys;


use App\ApplicationLayer\Journeys\Dtos\ActivityScreenshotDto;
use App\ApplicationLayer\Journeys\Dtos\BlocklyCategoryDto;
use App\ApplicationLayer\Journeys\Dtos\FreeMissionMetaDto;
use App\ApplicationLayer\Journeys\Dtos\McqChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\MinecraftMissionStepDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\ApplicationLayer\Journeys\Dtos\MissionStepDto;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Journeys\MissionMcq;
use App\DomainModelLayer\Journeys\MissionSavingActivity;
use App\DomainModelLayer\Journeys\MissionText;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Journeys\TaskProgressActivity;
use App\DomainModelLayer\Schools\CampActivityProgress;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Schools\StudentProgress;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionSavingsDto;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\BlocksDefinitionsDto;
use App\ApplicationLayer\Journeys\Dtos\MissionToolboxDto;
use App\ApplicationLayer\Journeys\Dtos\BlocksDto;
use App\Framework\Exceptions\BadRequestException;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Journeys\MissionSavings;
use App\Infrastructure\Journeys\MissionRepository;

class MissionService
{
    private $journeyRepository;
    private $accountRepository;
    private $schoolRepository;

    public function __construct(IJourneyMainRepository $journeyRepository,IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository)
    {
        $this->journeyRepository = $journeyRepository;
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
    }

    public function getMissionbyId($id,$user_id = null,$course_id =null){
        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $missionDto =  Mapper::MapClass(MissionDto::class,$mission,[MissionScreensDto::class]);
        $missionDto->type = $mission->getType();
        if($missionDto->type == "text")
            $missionDto->textualBlocklyCode = $mission->showModelAnswer();

        if($mission->getProgrammingLanguageType() != null)
            $missionDto->programmingLanguageType =  $mission->getProgrammingLanguageType()->getName();
        $missionDto->blockly_rules = $mission->blockly_rules;
        if($user_id != null)
        {
            $user = $this->accountRepository->getUserById($user_id);
            if($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            $account = $user->getAccount();
            if ($account == null)
                throw new BadRequestException(trans('locale.no_user_id'));

            $modelAnswerUnlockedForUser = $this->journeyRepository->getModelAnswerUnlocked($mission->getId(),$user->getId());
            $userRoles = $user->getRoles();
            if($mission->getType() == "mcq"){
                $hasModelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());

                $missionDto->ModelAnswer = $mission->showModelAnswer();
                if($modelAnswerUnlockedForUser != null || $hasModelAnswer != false){
                    $choice_ids = array();
                    $missionQuestions = $mission->getMissionMcq()->getQuestions();
                    foreach ($missionQuestions as $missionQuestion){
                        $choices = $missionQuestion->getChoices();
                        foreach($choices as $choice){
                            if($choice->isCorrect())
                                array_push($choice_ids,$choice->getId());
                        }

                    }
                    $missionDto->model_choice_ids = $choice_ids;
                }
                else{
                    $missionDto->model_choice_ids = null;
                }
            }
            else if($modelAnswerUnlockedForUser == null){
                $missionDto->model_choice_ids = null;
                foreach ($userRoles as $role) {
                    $rolePermissions = $role->getPermissions();
                    foreach ($rolePermissions as $permission) {
                        if($permission->getName() == 'show_ModelAnswer')
                        {
                            $missionDto->ModelAnswer = $mission->showModelAnswer();
                            break;
                        }

                    }
                }
                if($this->checkOpenedModelAnswer($user, $mission->getId()))
                    $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
            else{
                $missionDto->model_choice_ids = null;
                $missionDto->ModelAnswer = $mission->showModelAnswer();
            }

        }
        else
        {
            $missionDto->model_choice_ids = null;
            $missionDto->ModelAnswer = $mission->showModelAnswer();
        }
        $adventureOrder = $mission->getTask()->getTaskOrder()->getAdventure()->getOrder();
        $journeyOrder = $missionDto->order +(($adventureOrder-1)*count($mission->getTask()->getTaskOrder()->getAdventure()->getTasks()));
        $missionDto->journeyOrder = $journeyOrder;
        $missionDto->descriptionSpeech = $mission->getDescriptionSpeech();
        $missionDto->HintModelAnswer = $mission->showModelAnswer();
        $missionDto->GoldenTime = $mission->getGoldenTime();
        $missionDto->MissionState = $mission->getMissionState();
        $missionDto->ModelAnswerNumberOfBlocks = $mission->getModelAnswerNumberOfBlocks();
        $userProgress = $this->journeyRepository->getUserTaskProgress($mission->getTask(),$user_id,$mission->getTask()->getTaskOrder()->getJourney()->getId(),$mission->getTask()->getTaskOrder()->getAdventure()->getId());

        if($userProgress == null && $course_id != null){
            $course = $this->schoolRepository->getCourseById($course_id);
            if($course == null){
                throw new BadRequestException(trans("locale.course_doesn't_exist"));
            }
            $userProgress = $this->schoolRepository->getStudentProgrss($mission->getTask()->getId(),$user_id,$course->getId(),$mission->getTask()->getTaskOrder()->getAdventure()->getId());
            if($userProgress == null){
                $missionDto->userTrials = 0;
            }
            else{
                $missionDto->userTrials = $userProgress->getNo_of_trails();
            }
        }
        else if($userProgress == null){
            $missionDto->userTrials = 0;
        }
        else{
            $missionDto->userTrials = $userProgress->getNo_of_trails();
        }

        $missionDto->maxNoOfTrials = 3;
        $missionDto->category_flag = $mission->getBlocksType();

        return $missionDto;
    }

    public function getActivityMissionbyId($id,$user_id = null,$is_default =true){
        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        if($user_id != null){
            $user = $this->accountRepository->getUserById($user_id);
            if ($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));
        }

        $openedTask = false;
        if($user_id != null) {
            $roles = $user->getRoles();
            foreach ($roles as $role) {
                if ($role->getName() == "parent")
                    $openedTask = true;
            }
        }

        $missionDto = Mapper::MapClass(MissionDto::class, $mission, [MissionScreensDto::class]);
        $missionDto->type = $mission->getType();
        if($missionDto->type == "text")
            $missionDto->textualBlocklyCode = $mission->showModelAnswer();

        if($mission->getProgrammingLanguageType() != null)
            $missionDto->programmingLanguageType =  $mission->getProgrammingLanguageType()->getName();
        $missionDto->blockly_rules = $mission->blockly_rules;
        if($user_id != null)
        {
            $user = $this->accountRepository->getUserById($user_id);
            if($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            $account = $user->getAccount();
            if ($account == null)
                throw new BadRequestException(trans('locale.no_user_id'));

            $modelAnswerUnlockedForUser = $this->journeyRepository->getModelAnswerUnlocked($mission->getId(),$user->getId());
            $userRoles = $user->getRoles();
            if($mission->getType() == "mcq"){
                $choice_ids = array();
                $missionQuestions = $mission->getMissionMcq()->getQuestions();
                foreach ($missionQuestions as $missionQuestion){
                    $choices = $missionQuestion->getChoices();
                    foreach($choices as $choice){
                        if($choice->isCorrect())
                            array_push($choice_ids, $choice->getId());
                    }

                }
                $hasModelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());
                $missionDto->ModelAnswer = $mission->showModelAnswer();
                if($openedTask){
                    $missionDto->model_choice_ids = $choice_ids;
                }
                else{
                    if($modelAnswerUnlockedForUser != null || $hasModelAnswer != false){
                        $missionDto->model_choice_ids = $choice_ids;
                    }
                    else{
                        $missionDto->model_choice_ids = null;
                    }
                }
            }
            else if($modelAnswerUnlockedForUser == null){
                $missionDto->model_choice_ids = null;
                foreach ($userRoles as $role) {
                    $rolePermissions = $role->getPermissions();
                    foreach ($rolePermissions as $permission) {
                        if($permission->getName() == 'show_ModelAnswer')
                        {
                            $missionDto->ModelAnswer = $mission->showModelAnswer();
                            break;
                        }

                    }
                }
                if($this->checkOpenedModelAnswer($user, $mission->getId()))
                    $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
            else{
                $missionDto->model_choice_ids = null;
                $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
        }
        else
        {
            $missionDto->model_choice_ids = null;
            $missionDto->ModelAnswer = $mission->showModelAnswer();
        }
        $missionDto->descriptionSpeech = $mission->getDescriptionSpeech();
        $missionDto->HintModelAnswer = $mission->showModelAnswer();
        $missionDto->GoldenTime = $mission->getGoldenTime();
        $missionDto->MissionState = $mission->getMissionState();
        $missionDto->ModelAnswerNumberOfBlocks = $mission->getModelAnswerNumberOfBlocks();
        $userProgress = $this->journeyRepository->getUserTaskProgressActivity($mission->getTask(),$user_id,$mission->getTask()->getActivityTask()->getDefaultActivity()->getId(),$is_default);

        if($userProgress == null){
            $missionDto->userTrials = 0;
        }
        else{
            $missionDto->userTrials = $userProgress->getNo_of_trails();
        }
        $missionDto->maxNoOfTrials = 3;
        $missionDto->sceneName = $mission->getSceneName();
        $missionDto->MinecraftSteps = Mapper::MapEntityCollection(MinecraftMissionStepDto::class, $mission->getMinecraftSteps());
        return $missionDto;
    }

    public function getCampActivityMissionbyId($id, $camp_id, $user_id = null, $is_default = true){
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        if($user_id != null){
            $user = $this->accountRepository->getUserById($user_id);
            if ($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));
        }

        $openedTask = false;
        if($user_id != null) {
            $roles = $user->getRoles();
            foreach ($roles as $role) {
                if ($role->getName() == "parent")
                    $openedTask = true;
            }
        }

        $missionDto =  Mapper::MapClass(MissionDto::class,$mission,[MissionScreensDto::class]);
        $missionDto->type = $mission->getType();
        if($missionDto->type == "text")
            $missionDto->textualBlocklyCode = $mission->showModelAnswer();

        if($mission->getProgrammingLanguageType() != null)
            $missionDto->programmingLanguageType =  $mission->getProgrammingLanguageType()->getName();
        $missionDto->blockly_rules = $mission->blockly_rules;
        if($user_id != null) {
            $user = $this->accountRepository->getUserById($user_id);
            if($user == null)
                throw new BadRequestException(trans('locale.user_not_exist'));

            $account = $user->getAccount();
            if ($account == null)
                throw new BadRequestException(trans('locale.no_user_id'));

            $modelAnswerUnlockedForUser = $this->journeyRepository->getModelAnswerUnlocked($mission->getId(),$user->getId());
            $userRoles = $user->getRoles();
            if($mission->getType() == "mcq"){
                $choice_ids = array();
                $missionQuestions = $mission->getMissionMcq()->getQuestions();
                foreach ($missionQuestions as $missionQuestion){
                    $choices = $missionQuestion->getChoices();
                    foreach($choices as $choice){
                        if($choice->isCorrect())
                            array_push($choice_ids, $choice->getId());
                    }

                }
                $hasModelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());
                $missionDto->ModelAnswer = $mission->showModelAnswer();
                if($openedTask){
                    $missionDto->model_choice_ids = $choice_ids;
                }
                else{
                    if($modelAnswerUnlockedForUser != null || $hasModelAnswer != false){
                        $missionDto->model_choice_ids = $choice_ids;
                    }
                    else{
                        $missionDto->model_choice_ids = null;
                    }
                }
            }
            else if($modelAnswerUnlockedForUser == null){
                $missionDto->model_choice_ids = null;
                foreach ($userRoles as $role) {
                    $rolePermissions = $role->getPermissions();
                    foreach ($rolePermissions as $permission) {
                        if($permission->getName() == 'show_ModelAnswer')
                        {
                            $missionDto->ModelAnswer = $mission->showModelAnswer();
                            break;
                        }

                    }
                }
                if($this->hasAccessToModelAnswer($user))
                    $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
            else{
                $missionDto->model_choice_ids = null;
                $missionDto->ModelAnswer = $mission->showModelAnswer();
            }
        }
        else {
            $missionDto->model_choice_ids = null;
            $missionDto->ModelAnswer = $mission->showModelAnswer();
        }
        $missionDto->descriptionSpeech = $mission->getDescriptionSpeech();
        $missionDto->HintModelAnswer = $mission->showModelAnswer();
        $missionDto->GoldenTime = $mission->getGoldenTime();
        $missionDto->MissionState = $mission->getMissionState();
        $missionDto->ModelAnswerNumberOfBlocks = $mission->getModelAnswerNumberOfBlocks();
        $userProgress = $this->schoolRepository->getUserTaskProgressActivityCamp($mission->getTask(), $user_id, $camp_id,
            $mission->getTask()->getActivityTask()->getDefaultActivity()->getId(),$is_default);
        $userProgress=null;
        if($userProgress == null){
            $missionDto->userTrials = 0;
        }
        else{
            $missionDto->userTrials = $userProgress->getNoTrials();
        }

        $missionDto->maxNoOfTrials = 3;
        $missionDto->sceneName = $mission->getSceneName();
        return $missionDto;
    }
    public function getCampActivityMissionbyHtmlId($id, $camp_id, $user_id = null, $is_default = true){
        //dd($user_id);
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $taskProgress = $this->schoolRepository->getUserTaskProgressActivityCamp($mission->getTask(),$user_id,$camp_id,
            $mission->getTask()->getActivityTask()->getDefaultActivity()->getId(),$is_default);
        //dd($taskProgress);
        $passed = false;
        if(isset($taskProgress) && $taskProgress->first_success)
            $passed = true;

        $missionDto =  Mapper::MapClass(MissionHtmlDto::class,$mission);
        $missionDto->weight = $mission->getWeight();
        $missionDto->link = $mission->getLink();
        $missionDto->passed = $passed;
        $missionDto->steps = array();
        foreach ($mission->getSteps() as $step){
            array_push($missionDto->steps,Mapper::MapClass(MissionStepDto::class,$step));
        }

        return $missionDto;
    }

    public function loadMissionSavedCode($mission_id,$user_id,$journey_id,$adventure_id){
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        return Mapper::MapEntityCollection(MissionSavingsDto::class,$this->journeyRepository->getUserSavedCode($mission,$user_id,$journey_id,$adventure_id));
    }

    public function loadMissionSavedCodeActivity($mission_id,$user_id,$activity_id,$is_default = true){
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity->getId(), $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        return Mapper::MapEntityCollection(MissionSavingsDto::class,$this->journeyRepository->getUserSavedCodeActivity($mission,$user_id,$activity_id,$is_default));
    }

    public function updateMissionSavedCode($mission_id,$user_id,$name,$xml,$journey_id,$adventure_id,$timeTaken){
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $previous_missionsaving = $this->journeyRepository->getUserSavedCodeByName($mission,$user_id,$name,$journey_id,$adventure_id);
        if($previous_missionsaving != null)
            throw new BadRequestException(trans('locale.code_same_name_exists'));
        $missionsaving = new MissionSavings($mission,$user,$name,$xml,$journey_id,$adventure_id,$timeTaken);
        $mission->addMissionSaving($missionsaving);
        $this->journeyRepository->storeMission($mission);
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function updateMissionSavedCodeActivity($mission_id,$user_id,$name,$xml,$activity_id,$timeTaken,$is_default = true){
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity->getId(), $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $previous_missionsaving = $this->journeyRepository->getUserSavedCodeByNameActivity($mission,$user_id,$name,$activity_id,$is_default);
        //return $previous_missionsaving;
        if($previous_missionsaving != null)
            throw new BadRequestException(trans('locale.code_same_name_exists'));
        $missionsaving = new MissionSavingActivity($mission,$user,$name,$xml,$timeTaken);
        if($is_default)
            $missionsaving->setDefaultActivity($activity);
        else
            $missionsaving->setActivity($activity);
        $mission->addMissionSavingActivity($missionsaving);
        $this->journeyRepository->storeMission($mission);
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function deleteMissionSavedCode($mission_id,$user_id,$name,$journey_id,$adventure_id)
    {
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $missionsaving = $this->journeyRepository->getUserSavedCodeByName($mission,$user_id,$name,$journey_id,$adventure_id);
        if($missionsaving == null)
            throw new BadRequestException(trans('locale.mission_saving_not_exist'));
        $mission->removeMissionSaving($missionsaving);
        $this->journeyRepository->deleteMissionSavings($missionsaving);
        $this->journeyRepository->storeMission($mission);
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function deleteMissionSavedCodeActivity($mission_id,$user_id,$name,$activity_id,$is_default = true)
    {
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity->getId(), $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $missionsaving = $this->journeyRepository->getUserSavedCodeByNameActivity($mission,$user_id,$name,$activity_id,$is_default);
        if($missionsaving == null)
            throw new BadRequestException(trans('locale.mission_saving_not_exist'));
        $this->journeyRepository->removeMissionSaving($missionsaving);
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function getMissionCategories($missionId){

        $mission = $this->journeyRepository->getMissionbyId($missionId);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        //$missionCategories = $mission->getBlocklyCategory();

        $missionCategories = $this->journeyRepository->getBlocklyCategory($missionId);

        //$missionCategories = $this->journeyRepository->getAllCategories();

        $mappedObject = Mapper::MapEntityCollection(MissionToolboxDto::class, $missionCategories,[BlocksDto::class]);
        // return $missionCategories;
        return $mappedObject;
    }

    public function updateMissionProgress($mission_id,$user_id,$journey_id,$adventure_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode){
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
        {
//            $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
//            if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        }

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_account_not_found'));

        $modelAnswer = false;
        $child = $this->checkIfInvitedChild($user->getId());
        if(!$child){
            $modelAnswer = $this->checkOpenedModelAnswer($user, $mission_id);
        }
        $previous_missionprogress = $this->journeyRepository->getUserTaskProgress($task,$user_id,$journey_id,$adventure_id);
        $score = $mission->getWeight();
        if($previous_missionprogress == null){
            if($success_flag){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $mission->getWeight();
                }
                $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,1,1,$timeTaken,$timeTaken,$noOfBlocks,$noOfBlocks);
                $this->journeyRepository->storeTaskProgress($task_progress);
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
                //store use code if new
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }

            }

            else
            {
                $task_progress = new TaskProgress($task,$user,0,$journey_id,$adventure_id,1);
                $task->addProgress($task_progress);
                $this->journeyRepository->storeTask($task);
            }
        }

        else
        {
            $no_trials = $previous_missionprogress->getNo_of_trails() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($success_flag && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer){
                        $score += 5;
                    }
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }
            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $success_flag){
                $score = $mission->getWeight();
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->journeyRepository->removeTaskProgress($previous_missionprogress);
            $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,$no_trials,$first_success,$timeTaken,$bestTimeTaken,$noOfBlocks,$bestNoOfBlocks);
            $this->journeyRepository->storeTaskProgress($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

        }
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function updateActivityMissionProgress($mission_id,$user_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$is_default = true){
        $this->accountRepository->beginDatabaseTransaction();
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity->getId(), $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
        {
//            $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
//            if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        }

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_account_not_found'));

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission_id);
        $previous_missionprogress = $this->journeyRepository->getUserTaskProgressActivity($task,$user_id,$activity_id,$is_default);

        $score = $mission->getWeight();
        if($previous_missionprogress == null){
            if($success_flag){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $mission->getWeight();
                }
                $task_progress = new TaskProgressActivity($task,$user,$score,1,1,$timeTaken,$timeTaken,$noOfBlocks,$noOfBlocks);

                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                //return $task_progress;
                $this->journeyRepository->storeTaskProgressActivity($task_progress);
                //return "hi2";
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);

                //store use code if new
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }

            }

            else
            {
                $task_progress = new TaskProgressActivity($task,$user,0,1);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->journeyRepository->storeTaskProgressActivity($task_progress);
            }
        }

        else
        {
            $no_trials = $previous_missionprogress->getNo_of_trails() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($success_flag && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer){
                        $score += 5;
                    }
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }
            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $success_flag){
                $score = $mission->getWeight();
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->journeyRepository->deleteTaskProgressActivity($previous_missionprogress);
            $task_progress = new TaskProgressActivity($task,$user,$score,$no_trials,$first_success,$timeTaken,$bestTimeTaken,$noOfBlocks,$bestNoOfBlocks);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->journeyRepository->storeTaskProgressActivity($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function checkOpenedModelAnswer($user, $mission_id){
        $returned = false;
        $isUser = false;
        $isStudent = false;
        $isChild = false;
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;

            if($role->getName() == 'user' || $role->getName() == 'invited_Individual')
                $isUser = true;

            if($role->getName() == 'student')
                $isStudent = true;

            if($role->getName() == 'child')
                $isChild = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getJourneys();
            if(count($journeys) > 0){
                foreach ($journeys as $invJourney){
                    if($mission->getTask()->getTaskOrder() != null){
                        if($invJourney->getJourney()->getId() == $mission->getTask()->getTaskOrder()->getJourney()->getId()){
                            if($isUser){
                                if($invJourney->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
            $activities = $invitation_subscription->getInvitation()->getDefaultActivities();
            if(count($activities) > 0){
                foreach ($activities as $invActivity){
                    if($mission->getTask()->getActivityTask() != null){
                        if($invActivity->getDefaultActivity()->getId() == $mission->getTask()->getActivityTask()->getDefaultActivity()->getId()){
                            if($isUser){
                                if($invActivity->getProgressLock() == 0){
                                    $returned = true;
                                }
                            }
                            else{
                                if($isStudent || $isChild)
                                    $returned = false;

                                else
                                    $returned = true;
                            }
                        }
                    }
                }
            }
        }
        return $returned;
    }

    public function updateHTMLMissionProgress($mission_id,$user_id,$journey_id,$adventure_id){
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));
        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
        {
//            $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
//            if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        }

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $score = $mission->getWeight();

        $previous_missionprogress = $this->journeyRepository->getUserTaskProgress($task,$user_id,$journey_id,$adventure_id);
        if($previous_missionprogress == null)
        {

            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,1, 1);
            $this->journeyRepository->storeTaskProgress($task_progress);
        }
        elseif ($previous_missionprogress->getScore() < $score)
        {

            $addedScore = $score - $previous_missionprogress->getScore();

            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $this->journeyRepository->removeTaskProgress($previous_missionprogress);
            $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,1, 1);
            $this->journeyRepository->storeTaskProgress($task_progress);

        }
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function updateActivityHTMLMissionProgress($mission_id,$user_id,$activity_id,$is_default = true){
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity->getId(), $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
        {
//            $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
//            if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        }

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $score = $mission->getWeight();

        $previous_missionprogress = $this->journeyRepository->getUserTaskProgressActivity($task,$user_id,$activity_id,$is_default);
        if($previous_missionprogress == null)
        {

            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $task_progress = new TaskProgressActivity($task,$user,$score,1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->journeyRepository->storeTaskProgressActivity($task_progress);
        }
        elseif ($previous_missionprogress->getScore() < $score)
        {

            $addedScore = $score - $previous_missionprogress->getScore();

            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

            $this->journeyRepository->deleteTaskProgressActivity($previous_missionprogress);
            $task_progress = new TaskProgressActivity($task,$user,$score,1, 1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->journeyRepository->storeTaskProgressActivity($task_progress);

        }
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function getBlocksDefinitions()
    {
        return Mapper::MapEntityCollection(BlocksDefinitionsDto::class,$this->missionRepository->getBlocksDefinitions(),[BlocksDto::class]);
    }

    public function updateLastMission($mission_id,$user_id,$journey_id,$adventure_id)
    {
        
    }

    public function getNextMission($mission_id,$journey_id,$adventure_id)
    {
       // dd($mission_id);
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));
        //return $journey;
        
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
            if($adventure == null)
                throw new BadRequestException(trans('locale.adventure_not_exist'));
//return $adventure;
        $task_index = $adventure->getOrder();
//return $task_index;
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        //dd($mission);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        //return $mission;
        $task = $mission->getTask();
        //return $task;

        $order = $this->journeyRepository->getTaskOrder($task,$journey_id,$adventure_id)->getOrder();
        //return $order;

        $last_task = $this->journeyRepository->getLastTaskInAdventure($journey_id,$adventure_id);

        if($task->getId() != $last_task->getId())
        {
            $next_task = $this->journeyRepository->getTaskByOrder(intval($order) + 1,$journey_id,$adventure_id);
        }
        else
        {

            $next_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() + 1,$journey_id);

            if($next_adventure == null)
                throw new BadRequestException(trans('locale.next_adventure_not_exist'));

            $next_task = $this->journeyRepository->getFirstTaskInAdventure($journey_id,$next_adventure->getId());

            $task_index = $next_adventure->getOrder();

        }


        if($next_task == null)
            throw new BadRequestException(trans('locale.next_task_not_exist'));

        // $default_journey = $journey;

        // $adventures = $this->journeyRepository->getAdventuresByOrder($default_journey->getId());
        // $counter = 0;

        // foreach ($adventures as $adventure) {
        //     if($adventure->getId() == $adventure_id)
        //     {
        //         $task_index = $counter;
        //         break;
        //     }

        //     $counter++;
        // }
        $task_index--;

        $next_mission = $next_task->getMissions()->first();

        if($next_mission != null)
        {
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(),"next_task_type"=>"mission","next_task_index" => $task_index];
        }
        else
        {
            $next_quiz = $next_task->getQuizzes()->first();

            return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(),"next_task_type"=>$next_quiz->getType()->getName(),"next_task_index" => $task_index];
        }
    }

    public function getNextTaskActivity($mission_id,$activity_id,$is_default = true)
    {
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $task = $mission->getTask();

        $order = $this->journeyRepository->getActivityTaskOrder($task,$activity_id,$is_default)->getOrder();

        $last_task = $this->journeyRepository->getLastTaskInActivity($activity_id,$is_default);

        if($task->getId() != $last_task->getId())
        {
            $next_task = $this->journeyRepository->getTaskInActivityByOrder(intval($order) + 1,$activity_id,$is_default);
            if($next_task == null)
                throw new BadRequestException(trans('locale.next_task_not_exist'));

            $next_mission = $next_task->getMissions()->first();

            if($next_mission != null)
            {
                return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(),"next_task_type"=>"mission"];
            }
            else
            {
                $next_mission = $next_task->getMissionsHtml()->first();

                if($next_mission != null)
                {
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(),"next_task_type"=>"html_mission"];
                }
                else {
                    $next_quiz = $next_task->getQuizzes()->first();
                    return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(), "next_task_type" => $next_quiz->getType()->getName()];
                }
            }
        }
        else
            return ["next_task_id" => null, "next_task_name" => null,"next_task_type"=>null];


    }

    public function getNextMissionHtml($mission_id,$journey_id,$adventure_id)
    {
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));

        $task_index = $adventure->getOrder();

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $task = $mission->getTask();

        $order = $this->journeyRepository->getTaskOrder($task,$journey_id,$adventure_id)->getOrder();

        $last_task = $this->journeyRepository->getLastTaskInAdventure($journey_id,$adventure_id);

        if($task->getId() != $last_task->getId())
        {
            $next_task = $this->journeyRepository->getTaskByOrder(intval($order) + 1,$journey_id,$adventure_id);
        }
        else
        {

            $next_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() + 1,$journey_id);

            if($next_adventure == null)
                throw new BadRequestException(trans('locale.next_adventure_not_exist'));

            $next_task = $this->journeyRepository->getFirstTaskInAdventure($journey_id,$next_adventure->getId());

            $task_index = $next_adventure->getOrder();

        }


        if($next_task == null)
            throw new BadRequestException(trans('locale.next_task_not_exist'));

        // $default_journey = $journey;

        // $adventures = $this->journeyRepository->getAdventuresByOrder($default_journey->getId());
        // $counter = 0;

        // foreach ($adventures as $adventure) {
        //     if($adventure->getId() == $adventure_id)
        //     {
        //         $task_index = $counter;
        //         break;
        //     }

        //     $counter++;
        // }
        $task_index--;

        $next_mission = $next_task->getMissionsHtml()->first();

        if($next_mission != null)
        {
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(),"next_task_type"=>"html_mission","next_task_index" => $task_index];
        }
        else
        {
            $next_quiz = $next_task->getQuizzes()->first();

            return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(),"next_task_type"=>$next_quiz->getType()->getName(),"next_task_index" => $task_index];
        }
    }

    public function getMissionMcqQuestions($id){
        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        if($mission->getType() != 'mcq')
            throw new BadRequestException(trans('locale.mission_not_mcq_mission'));
        $questions = $this->journeyRepository->getMissionMcqQuestions($mission->MissionMcq);
        foreach ($questions as $question){
            $question->choices = Mapper::MapClassArray(McqChoiceDto::class,$question->choices->toArray());
        }
        $mission->MissionMcq->questions = $questions;
        $mission->MissionMcq->model_answer = $mission->showModelAnswer();
        return $mission->MissionMcq;
    }

    public function getMissionTextQuestions($id)
    {
        $mission = $this->journeyRepository->getMissionbyId($id);
        $questions = $this->journeyRepository->getMissionTextQuestions($mission->MissionText());
        $responseArray = [];
        foreach ($questions as $question){
            $responseArray[] = $question;
        }
        return $responseArray;
    }

    public function submitMcqMission($data,$selected_ids,$use_model, $id, $user_id,$journey_id,$adventure_id,$noOfBlocks,$timeTaken)
    {
        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $selected_ids_counter = 0;

        $questions_answers = [];

        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $task = $mission->getTask();

        $choice_correction = array();

        foreach ($data as $question)
        {
            $question_instance = $this->journeyRepository->getMcqQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getMcqChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            if($use_model && $use_model != "false")
                $choice_correct = $this->journeyRepository->getMcqCorrectChooseChoice($question['question_id']);
            else
                $choice_correct = $this->journeyRepository->getMcqChoiceById($selected_ids[$selected_ids_counter]);
            $question_answer = ["question_id"=>$question['question_id'],
                //"question_body"=>$question_instance->getBody(),
                "user_choice"=>$choice_instance->choose,
                "choice_correct"=>$choice_correct->choose,
                "eval"=>false];

            if($choice_correct->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question_instance->getWeight();
            }
            else {
                $choice_message = $this->journeyRepository->getMcqChoiceById($question['choice']);
                $message = [
                    "choice_id" => $question['choice'],
                    "msg" => $choice_message->getCorrectionMsg()
                ];
                array_push($choice_correction, $message);
            }
            $questions_answers[] = $question_answer;
            $selected_ids_counter++;
        }
        // TO-DO Change Logic Of Success
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());

        $baseScore = $score;
        $previous_missionprogress = $this->journeyRepository->getUserMissionProgrss($mission,$user_id,$journey_id,$adventure_id);
        if($previous_missionprogress == null){
            if($passed){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $baseScore;
                }
                $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,1,1,$timeTaken,$timeTaken,$noOfBlocks,$noOfBlocks);
                $this->journeyRepository->storeTaskProgress($task_progress);
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);


            }

            else
            {
                $task_progress = new TaskProgress($task,$user,0,$journey_id,$adventure_id,1);
                $task->addProgress($task_progress);
                $this->journeyRepository->storeTask($task);
            }
        }

        else
        {
            $no_trials = $previous_missionprogress->getNo_of_trails() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($passed && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                //no of BlocksScore
//                if($mission->getType() != "text"){
//                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
//                        $score +=5;
//                    }
//                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }

            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }

            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $passed){
                $score = $baseScore;
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->journeyRepository->removeTaskProgress($previous_missionprogress);
            $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,$no_trials,$first_success,$timeTaken,$bestTimeTaken,$noOfBlocks,$bestNoOfBlocks);
            $this->journeyRepository->storeTaskProgress($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

        }
        $this->journeyRepository->storeTask($task);
        return ["passed" => $passed, "questions_answers" => $questions_answers, "choice_correction" => $choice_correction];

    }

    public function checkActivityB2Authority($user_id, $activity_id, $is_default = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $accountType = $account->getAccountType()->getName();
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = false;
        if($activity->isB2B()){
            if($accountType == 'School')
                $authority = true;
        }
        if($activity->isB2C()){
            if($accountType == 'Family' || $accountType == 'Individual')
                $authority = true;
        }
        return $authority;
    }

    public function submitActivityMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default = true, $is_mobile=true)
    {
        $mission = $this->journeyRepository->getMissionbyId($id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity->getId(), $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $selected_ids_counter = 0;
        $questions_answers = [];
        $task = $mission->getTask();
        $choice_correction = array();

        foreach ($data as $question)
        {
            $question_instance = $this->journeyRepository->getMcqQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));

            $choice_instance = $this->journeyRepository->getMcqChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));

            if($use_model && $use_model != "false")
                $choice_correct = $this->journeyRepository->getMcqCorrectChooseChoice($question['question_id']);
            else
                $choice_correct = $this->journeyRepository->getMcqChoiceById($selected_ids[$selected_ids_counter]);
            $question_answer = ["question_id"=>$question['question_id'],
                //"question_body"=>$question_instance->getBody(),
                "user_choice"=>$choice_instance->choose,
                "choice_correct"=>$choice_correct->choose,
                "eval"=>false];

            if($choice_correct->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question_instance->getWeight();
            }
            else {
                $choice_message = $this->journeyRepository->getMcqChoiceById($question['choice']);
                $message = [
                    "choice_id" => $question['choice'],
                    "msg" => $choice_message->getCorrectionMsg()
                ];
                array_push($choice_correction, $message);
            }
            $questions_answers[] = $question_answer;
            $selected_ids_counter++;
        }
        // TO-DO Change Logic Of Success
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission->getId());

        $baseScore = $score;
        $previous_missionprogress = $this->journeyRepository->getUserTaskProgressActivity($mission->getTask(),$user_id,$activity_id,$is_default);
        if($previous_missionprogress == null){
            if($passed){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $baseScore;
                }
                $task_progress = new TaskProgressActivity($task,$user,$score,1,1,$timeTaken,$timeTaken,$noOfBlocks,$noOfBlocks);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->journeyRepository->storeTaskProgressActivity($task_progress);
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);


            }

            else
            {
                $task_progress = new TaskProgressActivity($task,$user,0,1);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->journeyRepository->storeTaskProgressActivity($task_progress);
            }
        }

        else
        {
            $no_trials = $previous_missionprogress->getNo_of_trails() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($passed && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                //no of BlocksScore
//                if($mission->getType() != "text"){
//                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
//                        $score +=5;
//                    }
//                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }

            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }

            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $passed){
                $score = $baseScore;
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->journeyRepository->deleteTaskProgressActivity($previous_missionprogress);
            $task_progress = new TaskProgressActivity($task,$user,$score,$no_trials,$first_success,$timeTaken,$bestTimeTaken,$noOfBlocks,$bestNoOfBlocks);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->journeyRepository->storeTaskProgressActivity($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);

        }
        $this->journeyRepository->storeTask($task);
        return ["passed" => $passed, "questions_answers" => $questions_answers, "choice_correction" => $choice_correction];

    }

    public function getHtmlMissionbyId($user_id,$html_mission_id){

        $mission = $this->journeyRepository->getHtmlMissionbyId($html_mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.html_mission_not_exist'));
        $missionDto =  Mapper::MapClass(MissionHtmlDto::class,$mission);
        $missionDto->weight = $mission->getWeight();
        $missionDto->steps = array();
        foreach ($mission->getSteps() as $step){
            array_push($missionDto->steps,Mapper::MapClass(MissionStepDto::class,$step));
        }
        return $missionDto;
    }

    public function hasAccessToModelAnswer(User $user)
    {
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription == null)
            return false;
        $invitation = $invitation_subscription->getInvitation();
        $journey = $invitation->getJourneys()->first();
        if($journey == null)
            return false;
        if(!$journey->getProgressLock())
            return true;
        return false;
    }

    public function getFreeMissions()
    {
        $free_journeys = $this->journeyRepository->getAllFreeJourneys();
        if(count($free_journeys) == 0)
            return [];

        $tasks = $this->journeyRepository->getFreeJourneyTasks($free_journeys->first()->getId());
        $free_missions = [];
        $free_missionsCounter = 0;
        foreach ($tasks as $task) {
            $mission = $task->getTask()->getMissions()->first();
            if($mission != null){

                $free_missions[$free_missionsCounter] = Mapper::MapClass(FreeMissionMetaDto::class,$mission, [MissionScreensDto::class]);
                $mcq_questions = [];
                if($mission->MissionMcq != null)
                {
                    $questions = $this->journeyRepository->getMissionMcqQuestions($mission->MissionMcq);
                    foreach ($questions as $question){
                        $question->choices = Mapper::MapClassArray(McqChoiceDto::class,$question->choices->toArray());
                        $question->programmingLanguage = $question->getProgrammingLanguage();
                        $mcq_questions[] = $question;
                    }
                    $free_missions[$free_missionsCounter]->mcq_questions = $mcq_questions;
                }
                else
                    $free_missions[$free_missionsCounter]->mcq_questions = null;
                if($mission->MissionText != null)
                {
                    $questions = $this->journeyRepository->getMissionTextQuestions($mission->MissionText);
                    $missionTextQuestionsArray = [];
                    foreach ($questions as $question){
                        array_push($missionTextQuestionsArray,$question);
                    }
                    $free_missions[$free_missionsCounter]->text_questions = $missionTextQuestionsArray;
                }
                else
                    $free_missions[$free_missionsCounter]->text_questions = null;
                $free_missionsCounter++;
            }

        }
        return $free_missions;
    }

    public function getFreeMissionById($mission_id)
    {
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        if(!$this->journeyRepository->isFreeTask($mission->getTask()->getId()))
            throw new BadRequestException(trans('locale.not_free_mission'));
        $missionDto =  Mapper::MapClass(MissionDto::class,$mission,[MissionScreensDto::class]);
        $missionDto->type = $mission->getType();
        $missionDto->sceneName = $mission->getSceneName();
        $missionDto->textualBlocklyCode = $mission->showModelAnswer();
        if($mission->getProgrammingLanguageType() != null)
            $missionDto->programmingLanguageType =  $mission->getProgrammingLanguageType()->getName();
        $missionDto->blockly_rules = $mission->blockly_rules;
        $missionDto->ModelAnswer = $mission->showModelAnswer();
        //$adventureOrder = $mission->getTask()->getTaskOrder()->getAdventure()->getOrder();
        //$journeyOrder = $missionDto->order +(($adventureOrder-1)*count($mission->getTask()->getTaskOrder()->getAdventure()->getTasks()));
        //$missionDto->journeyOrder = $journeyOrder;
        $mcq_questions = [];
        if($mission->MissionMcq != null)
        {
            $questions = $this->journeyRepository->getMissionMcqQuestions($mission->MissionMcq);
            foreach ($questions as $question){
                $question->choices = Mapper::MapClassArray(McqChoiceDto::class,$question->choices->toArray());
                $question->programmingLanguage = $question->getProgrammingLanguage();
                $mcq_questions[] = $question;
            }
            $missionDto->mcq_questions = $mcq_questions;
        }
        else
            $missionDto->mcq_questions = $mcq_questions;
        if($mission->MissionText != null)
        {
            $missionTextQuestions = $this->journeyRepository->getMissionTextQuestions($mission->MissionText);
            $missionTextQuestionsArray = [];
            foreach ($missionTextQuestions as $missionTextQuestion){
                array_push($missionTextQuestionsArray,$missionTextQuestion);
            }
            $missionDto->text_questions = $missionTextQuestionsArray;
        }
        else
            $missionDto->text_questions = [];
        $missionDto->hint = $mission->getHint();
        return $missionDto;

    }

    public function sendCertificateEmail($email,$pdf_file,$level)
    {
        $this->journeyRepository->sendCertificateEmail($email,$pdf_file,$level);
        return trans('locale.certificate_send_success');
    }

    public function uploadCertificate($image_file)
    {
        $image_url = $this->journeyRepository->uploadCertificate($image_file);
        return ['image_url' =>$image_url,'message' => trans('locale.certificate_upload_success')];
    }

    public function updateActivityCampMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$is_default = true){
        $this->accountRepository->beginDatabaseTransaction();

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if ($account == null)
            throw new BadRequestException(trans('locale.user_account_not_found'));

        $modelAnswer = $this->checkOpenedModelAnswer($user, $mission_id);
        $previous_missionprogress = $this->schoolRepository->getUserTaskProgressActivityCamp($task, $user_id, $camp_id, $activity_id, $is_default);

        $score = $mission->getWeight();
        if($previous_missionprogress == null){
            if($success_flag){
                //no of trialsScore
                if(!$modelAnswer){
                    $score += 5;
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }

                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());
                if($userModelAnswerUnlocked != null){
                    $score = $mission->getWeight();
                }
                $task_progress = new CampActivityProgress($task, $user, $camp, $score,1,1, $timeTaken, $timeTaken, $noOfBlocks, $noOfBlocks);

                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                //return $task_progress;
                $this->schoolRepository->storeCampActivityProgress($task_progress);
                //return "hi2";
                //new user score
                $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);

                //store use code if new
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }
            else {
                $task_progress = new CampActivityProgress($task, $user, $camp,0,1);
                if($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
                $this->schoolRepository->storeCampActivityProgress($task_progress);
            }
        }
        else {
            $no_trials = $previous_missionprogress->getNoTrials() + 1;
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($success_flag && ($first_success == null)){
                $first_success = $no_trials;
            }
            if($first_success == null){
                $score = 0;
            }
            else{
                //no of trialsScore
                if($first_success <= 3){
                    if(!$modelAnswer){
                        $score += 5;
                    }
                }
                //no of BlocksScore
                if($mission->getType() != "text"){
                    if($noOfBlocks <= $mission->getModelAnswerNumberOfBlocks()){
                        if(!$modelAnswer) {
                            $score += 5;
                        }
                    }
                }
                //golden time Score
                if($timeTaken <= $mission->getGoldenTime()){
                    if(!$modelAnswer) {
                        $score += 5;
                    }
                }
                if($mission->getMissionState()->getRatio() != null){
                    $score = $score*$mission->getMissionState()->getRatio();
                }
                if($xmlCode != null){
                    $missionModelAnswer = $mission->showModelAnswer();
                    preg_match_all('/type="(\w+)"/', $missionModelAnswer, $modelAnswerBlocks);
                    preg_match_all('/type="(\w+)"/', $xmlCode, $userCodeBlocks);
                    if(count($userCodeBlocks)>0){
                        $found = true;
                        if(count($userCodeBlocks) != count($modelAnswerBlocks)){
                            $found = false;
                        }
                        else{
                            $counter = 0;
                            foreach ($userCodeBlocks as $userCodeBlock){
                                if($userCodeBlock != $modelAnswerBlocks[$counter]){
                                    $found = false;
                                    break;
                                }
                                $counter ++;
                            }
                        }
                        if(!$found){
                            $userModelAnswers = $this->accountRepository->getUserModelAnswers($user->getId(),$mission->getId());
                            foreach ($userModelAnswers as $userModelAnswer){
                                $found = true;
                                if(count($userCodeBlocks) != count($userModelAnswer)){
                                    $found = false;
                                }
                                else{
                                    $counter = 0;
                                    foreach ($userCodeBlocks as $userModelAnswer){
                                        if($userCodeBlock != $userModelAnswer[$counter]){
                                            $found = false;
                                            break;
                                        }
                                        $counter ++;
                                    }
                                }
                            }
                            if(!$found){
                                $this->accountRepository->storeUserModelAnswer(new UserModelAnswer($user,$mission,$xmlCode));
                            }
                        }
                    }
                }
            }
            //update user score if it needs to be updated
            $addedScore = 0;
            if($score > $previous_missionprogress->getScore()){
                $addedScore = $score - $previous_missionprogress->getScore();
            }
            /////////////////////////////////////////////
            //maintain best Score ,timeTaken,and No Of Blocks
            if($score < $previous_missionprogress->getScore()){
                $score = $previous_missionprogress->getScore();
            }
            $bestTimeTaken = $previous_missionprogress->getBestTaskDuration();
            if($timeTaken != null){
                if($timeTaken < $bestTimeTaken)
                    $bestTimeTaken = $timeTaken;
            }
            $bestNoOfBlocks = $previous_missionprogress->getBestBlocksNumber();
            if($noOfBlocks != null){
                if($noOfBlocks < $bestNoOfBlocks)
                    $bestNoOfBlocks = $noOfBlocks;
            }
            $userModelAnswerUnlocked = $this->accountRepository->getUserModelAnswerUnlocked($user->getId(),$mission->getId());

            if($userModelAnswerUnlocked != null && $success_flag){
                $score = $mission->getWeight();
                if($previous_missionprogress->getScore() > $score){
                    $score = $previous_missionprogress->getScore();
                    $addedScore = 0;
                }
                else{
                    $addedScore = $score - $previous_missionprogress->getScore();
                }

            }
            else if($userModelAnswerUnlocked != null){
                $score = $previous_missionprogress->getScore();
                $addedScore = 0;
            }
            ///////////////////////////////////////////////////
            $this->schoolRepository->deleteCampActivityProgress($previous_missionprogress);
            $task_progress = new CampActivityProgress($task, $user, $camp, $score, $no_trials, $first_success, $timeTaken, $bestTimeTaken, $noOfBlocks, $bestNoOfBlocks);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->schoolRepository->storeCampActivityProgress($task_progress);
            $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
        }
        $this->accountRepository->commitDatabaseTransaction();
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function updateActivityCampHTMLMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$is_default = true){
        $this->accountRepository->beginDatabaseTransaction();

        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $score = $mission->getWeight();
        $previous_missionprogress = $this->schoolRepository->getUserTaskProgressActivityCamp($task,$user_id,$camp_id,$activity_id,$is_default);
        //dd($previous_missionprogress);
        if($previous_missionprogress == null)
        {
            if($success_flag) {
                $userScore = new UserScore($user, $user->getExperience() + $score, $user->getCoins() + ($score / 5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);

                $task_progress = new CampActivityProgress($task, $user, $camp, $score, 1, 1);
                if ($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
              //  $this->schoolRepository->storeCampActivityProgress($task_progress);
            }else{
                $task_progress = new CampActivityProgress($task, $user, $camp, 0, 1);
                if ($is_default)
                    $task_progress->setDefaultActivity($activity);
                else
                    $task_progress->setActivity($activity);
            }
           // $this->schoolRepository->storeCampActivityProgress($task_progress);
        }
        else
        {
            $no_trials = $previous_missionprogress->getNo_of_trails() + 1;
            if($previous_missionprogress->getScore() < $score)
                $new_score = $score;
            else
                $new_score = $previous_missionprogress->getScore();
            $first_success = $previous_missionprogress->getFirstSuccess();

            if($success_flag && ($previous_missionprogress->getFirstSuccess() == null)){
                $first_success = $no_trials;

            }

            if($new_score > $previous_missionprogress->getScore())
            {
                $added_Score = $new_score - $previous_missionprogress->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$added_Score,$user->getCoins()+($added_Score/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }
            //$this->schoolRepository->deleteStudentProgress($previous_missionprogress);
            $this->schoolRepository->deleteCampActivityProgress($previous_missionprogress);
            //$student_progress = new StudentProgress($task,$user,$course,$adventure,$no_trials,$new_score,$first_success);
            $task_progress = new CampActivityProgress($task, $user, $camp, $score, $no_trials, $first_success);
            if ($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
        }
        $this->schoolRepository->storeCampActivityProgress($task_progress);
        $this->accountRepository->commitDatabaseTransaction();
        //dd($previous_missionprogress);
        return Mapper::MapClass(MissionDto::class,$mission);
    }

    public function getNextTaskActivityCamp($mission_id, $camp_id, $activity_id, $is_default = true){
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $order = $this->journeyRepository->getActivityTaskOrder($task, $activity_id, $is_default)->getOrder();
        $last_task = $this->journeyRepository->getLastTaskInActivity($activity_id, $is_default);

        if($task->getId() != $last_task->getId()) {
            $next_task = $this->journeyRepository->getTaskInActivityByOrder(intval($order) + 1, $activity_id, $is_default);
            if($next_task == null)
                throw new BadRequestException(trans('locale.next_task_not_exist'));

            $next_mission = $next_task->getMissions()->first();
            if($next_mission != null) {
                return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(), "next_task_type" => "mission"];
            }
            else {
                $next_mission = $next_task->getMissionsHtml()->first();
                if($next_mission != null) {
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "html_mission"];
                }
                else {
                    $next_quiz = $next_task->getQuizzes()->first();
                    return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(), "next_task_type" => "quiz"];
                }
            }
        }
        else
            return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
    }
    public function getNextTaskHtmlActivityCamp($mission_id, $camp_id, $activity_id, $is_default = true){
        $camp = $this->schoolRepository->getCampById($camp_id);
        if($camp == null)
            throw new BadRequestException(trans('locale.camp_not_exist'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $mission = $this->journeyRepository->getHtmlMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $order = $this->journeyRepository->getActivityTaskOrder($task, $activity_id, $is_default)->getOrder();
        $last_task = $this->journeyRepository->getLastTaskInActivity($activity_id, $is_default);

        if($task->getId() != $last_task->getId()) {
            $next_task = $this->journeyRepository->getTaskInActivityByOrder(intval($order) + 1, $activity_id, $is_default);
            if($next_task == null)
                throw new BadRequestException(trans('locale.next_task_not_exist'));

            $next_mission = $next_task->getMissions()->first();
            if($next_mission != null) {
                return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(), "next_task_type" => "mission"];
            }
            else {
                $next_mission = $next_task->getMissionsHtml()->first();
                if($next_mission != null) {
                    return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "html_mission"];
                }
                else {
                    $next_quiz = $next_task->getQuizzes()->first();
                    return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(), "next_task_type" => "quiz"];
                }
            }
        }
        else
            return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
    }

    public function checkIfInvitedChild($user_id){
        $child = false;
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.child_not_exist'));

        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $roles = $user->getRoles();
            foreach ($roles as $role) {
                if ($role->getName() == "child"){
                    $child = true;
                }
            }
        }
        return $child;
    }

    public function getFreeRoboClubMissions(){
        $free_journeys = $this->journeyRepository->getRoboClubFreeJourneys();
        if(count($free_journeys) == 0)
            return [];

        $allJourneysData = [];
        foreach ($free_journeys as $journey){
            $journeyData['id'] = $journey->getId();
            $journeyData['name'] = $journey->getName();
            $journeyData['image'] = $journey->getImageUrl();
            $journeyData['icon'] = $journey->getIconUrl();
            $journeyData['tasks'] = array();
            $tasks = $this->journeyRepository->getFreeJourneysTasksById($journey->getId());
            $mission = $tasks->first()->getTask()->getMissions()->first();
            if($mission != null)
                $journeyData['first_mission_id'] = $mission->getId();

            foreach ($tasks as $task) {
                $freeMission = new \stdClass();
                $mission = $task->getTask()->getMissions()->first();
                if($mission != null){
                    $freeMission = Mapper::MapClass(FreeMissionMetaDto::class, $mission, [MissionScreensDto::class]);
                    $mcq_questions = [];
                    if($mission->MissionMcq != null) {
                        $questions = $this->journeyRepository->getMissionMcqQuestions($mission->MissionMcq);
                        foreach ($questions as $question){
                            $question->choices = Mapper::MapClassArray(McqChoiceDto::class,$question->choices->toArray());
                            $question->programmingLanguage = $question->getProgrammingLanguage();
                            $mcq_questions[] = $question;
                        }
                        $freeMission->mcq_questions = $mcq_questions;
                    }
                    else{
                        $freeMission->mcq_questions = null;
                    }
                    if($mission->MissionText != null) {
                        $questions = $this->journeyRepository->getMissionTextQuestions($mission->MissionText);
                        $missionTextQuestionsArray = [];
                        foreach ($questions as $question){
                            array_push($missionTextQuestionsArray,$question);
                        }
                        $freeMission->text_questions = $missionTextQuestionsArray;
                    }
                    else{
                        $freeMission->text_questions = null;
                    }
                }
                array_push($journeyData['tasks'], $freeMission);
            }
            array_push($allJourneysData, $journeyData);
        }
        return $allJourneysData;
    }

}