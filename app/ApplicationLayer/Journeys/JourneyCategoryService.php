<?php

namespace App\ApplicationLayer\Journeys;


use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\Helpers\Mapper;

class JourneyCategoryService
{
    private $journeyRepository;

    public function __construct(IJourneyMainRepository $journeyRepository)
    {
        $this->journeyRepository = $journeyRepository;
    }

    public function GetAllCategories(){
        return Mapper::MapEntityCollection(JourneyCategoryDto::class,$this->journeyRepository->getAllCategories());
    }

    public function getCategoryJournies($id)
    {
    	// return Mapper::MapEntityCollection(JourneyDto::class,$this->journeyCategoryRepository->getCategoryJournies($id));
    }
}