<?php

namespace App\ApplicationLayer\Journeys\Interfaces;


interface IJourneyMainService
{
    public function getJourneyData($id,$user_id);
    public function getMissionMcqQuestions($id);
    public function getMissionTextQuestions($id);
    public function getMissionbyId($id,$user_id = null,$course_id =null);
    public function loadMissionSavedCode($mission_id,$user_id,$journey_id,$adventure_id);
    public function getMissionCategories($missionId);
    public function updateMissionSavedCode($mission_id,$user_id,$name,$xml,$journey_id,$adventure_id,$timeTaken);
    public function deleteMissionSavedCode($mission_id,$user_id,$name,$journey_id,$adventure_id);
    public function updateMissionProgress($mission_id,$user_id,$journey_id,$adventure_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode= null);
    public function updateHTMLMissionProgress($mission_id,$user_id,$journey_id,$adventure_id);
    public function getTutorials($id);
    public function GetQuiz($id,$activity_id,$round_id,$type,$user);
    public function submitQuiz($data, $id, $user_id,$journey_id,$adventure_id);
    public function getAllCategories();
    public function GetAllJourneys();
    public function getJourneysSummery();

    public function updateLastMission($mission_id,$user_id,$journey_id,$adventure_id);
    public function getNextMissionHtml($mission_id,$journey_id,$adventure_id);
    public function getNextMission($mission_id,$journey_id,$adventure_id);
    public function getNextQuiz($quiz_id,$journey_id,$adventure_id);

    public function checkMissionAuthorization($user_id,$mission_id,$journey_id,$adventure_id,$recursive = true);
    public function checkQuizAuthorization($user_id,$quiz_id,$journey_id,$adventure_id);
    public function submitMcqMission($data,$selected_ids,$use_model, $id, $user_id,$journey_id,$adventure_id,$noOfBlocks = null,$timeTaken = null);
    public function GetJourneyById($id);
    public function getJourneyAdventures($user_id,$journey_id);
    public function getHtmlMissionbyId($user_id,$html_mission_id);
    public function getFreeMissions();
    public function getFreeMissionById($mission_id);
    public function sendCertificateEmail($email,$pdf_file,$level);
    public function uploadCertificate($image_file);
    public function getActivityMissionbyId($id,$user_id = null,$is_default =true);
    public function getCampActivityMissionbyId($id, $camp_id, $user_id = null, $is_default = true);
    public function getCampActivityMissionHtmlbyId($id, $camp_id, $user_id = null, $is_default = true);
    public function updateActivityMissionProgress($mission_id,$user_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$is_default = true);
    public function updateActivityCampMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$is_default = true);
    public function updateActivityHTMLMissionProgress($mission_id,$user_id,$activity_id,$is_default = true);
    public function updateActivityCampHTMLMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$is_default = true);
    public function submitActivityMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default = true, $is_mobile = true);
    public function submitActivityCampMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default = true);
    public function submitActivityQuiz($data, $id, $user_id,$activity_id,$is_default = true);
    public function checkProgressUnlockedActivity($activity_id,$user_id,$is_default = true);
    public function checkMissionAuthorizationActivity($user_id,$mission_id,$activity_id,$is_default= true);
    public function checkQuizAuthorizationActivity($user_id,$quiz_id,$activity_id,$is_default= true);
    public function checkQuizAuthorizationActivityCamp($user_id,$quiz_id,$activity_id,$is_default= true);
    public function getNextTaskActivity($mission_id,$activity_id,$is_default = true);
    public function getNextTaskActivityCamp($mission_id,$camp_id,$activity_id,$is_default = true);
    public function getNextTaskHtmlActivityCamp($mission_id,$camp_id,$activity_id,$is_default = true);
    public function getAllActivitiesForB2C($user_id,$start_from, $limit, $search, $is_mobile = true);
    public function getAllMinecraftActivities($user_id,$start_from, $limit, $search, $is_mobile = true);
    public function getUserDefaultActivityIds($user_id,$is_mobile = true);
    public function getActivity($user_id,$activity_id,$is_default = true, $is_mobile = true);
    public function getActivityTasks($user_id,$activity_id,$is_default = true, $is_mobile = true);
    public function countActivitiesForB2C($user_id,$search);
    public function countAllMinecraftActivities($user_id,$search);
    public function getAllActivities($user_id, $start_from, $limit, $search);
    public function loadMissionSavedCodeActivity($mission_id,$user_id,$activity_id,$is_default = true);
    public function updateMissionSavedCodeActivity($mission_id,$user_id,$name,$xml,$activity_id,$timeTaken,$is_default = true);
    public function deleteMissionSavedCodeActivity($mission_id,$user_id,$name,$activity_id,$is_default = true);
    public function getAllActivitiesForB2CNonUser($start_from,$limit,$search);

    public function getFreeActivitiesAndJourneys();
    public function getFreeRoboClubMissions();
    public function unlockActivityWithCoins($user_id, $activity_id, $coins, $is_default = true, $is_mobile = true);
    public function getConfidence();
}