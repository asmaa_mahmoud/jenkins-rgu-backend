<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class DragChoiceDto
{
    
    public $id;
    public $text;
    public $correctCellOrder;
    
    
}