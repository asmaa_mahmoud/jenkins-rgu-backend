<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class ChoiceDto
{
    public $id;
    public $text;
    public $is_answer;
    public $question_id;
    public $explanation;
}