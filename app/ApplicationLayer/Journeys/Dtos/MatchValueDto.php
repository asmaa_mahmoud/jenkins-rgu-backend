<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MatchValueDto
{
    public $id;
    public $text;
    public $order;
    public $explanation;

}