<?php

namespace App\ApplicationLayer\Journeys\Dtos;

/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 12:34 PM
 */
class MissionToolboxDto
{
    public $name;
    public $blocks = [];
}