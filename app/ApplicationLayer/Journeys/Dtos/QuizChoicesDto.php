<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizChoicesDto
{
    public $id;
    public $question_id;
    public $text;
    public $is_answer;
    public $image;
    public $explanation;
}