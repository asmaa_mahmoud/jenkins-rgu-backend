<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MissionHtmlDto
{
    public $id;
    public $title;
    public $description;
    public $order;
    public $positionX;
    public $positionY;
    //public $steps = [];
    public $weight;
    public $moduleType;
    public $is_booster;
    public $isMiniProject;
    public $miniProjectType;
    public $withSteps;
    public $currentSection;
    public $sections= [];
    public $guide= [];


}