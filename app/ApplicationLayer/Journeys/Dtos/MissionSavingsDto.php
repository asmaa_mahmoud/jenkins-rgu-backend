<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MissionSavingsDto
{
    public $name;
    public $code;
    public $timetaken;
}