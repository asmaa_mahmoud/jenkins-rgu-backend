<?php
/**
 * Created by PhpStorm.
 * User: hme1000000
 * Date: 9/14/2017
 * Time: 1:52 PM
 */

namespace App\ApplicationLayer\Journeys\Dtos;


class FreeMissionDto
{
    public $id;
    public $name;
    public $order;
    public $weight;
    public $description;
    public $iconUrl;
    public $positionX;
    public $positionY;
    public $vplId;
    public $screenshots = [];
    public $ModelAnswer;
    public $type;
    public $programmingLanguageType;
    public $mcq;
    public $textual;
}