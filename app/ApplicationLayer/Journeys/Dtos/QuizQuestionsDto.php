<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizQuestionsDto
{
    public $id;
    public $weight;
    public $body;
    public $quizId;
    public $mainImage;
    public $initialImage;
    public $isEditor;
    public $editorCode;
    public $emulatorType;
    public $selectMany;
    public $choices = [];
}