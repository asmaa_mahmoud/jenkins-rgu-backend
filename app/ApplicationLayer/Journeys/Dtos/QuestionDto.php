<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuestionDto
{
    public $id;
    public $body;
    public $quiz_id;
    public $weight;
    public $choices;
}