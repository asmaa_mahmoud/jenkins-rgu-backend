<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MissionDto
{
    public $id;
    public $name;
    public $order;
    public $weight;
    public $description;
    public $iconUrl;
    public $positionX;
    public $positionY;
    public $vplId;
    public $screenshots = [];
    public $ModelAnswer;
    public $type;
    public $programmingLanguageType;
    public $sceneName;
    public $descriptionSpeech;
    public $GoldenTime;
    public $MissionState;
    public $ModelAnswerNumberOfBlocks;
}