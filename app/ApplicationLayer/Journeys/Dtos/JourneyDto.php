<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class JourneyDto
{
	public $Id;
    public $Name;
    public $CardIconURL;
    public $creationDate;
    public $Description;
    public $Summary;
    public $WhatYouWillLearn;
    public $Concept;
    public $VideoUrl;
    public $Scene;
    public $Status;
    public $Categories = [];
//    public $gradeNumber;
}