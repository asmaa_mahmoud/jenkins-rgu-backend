<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class JourneyMinifiedDto
{
    public $Id;
    public $Name;
    public $CardIconURL;

}