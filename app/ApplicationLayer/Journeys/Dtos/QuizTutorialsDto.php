<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizTutorialsDto
{
    public $imageUrl;
    public $videoUrl;
    public $description;
}