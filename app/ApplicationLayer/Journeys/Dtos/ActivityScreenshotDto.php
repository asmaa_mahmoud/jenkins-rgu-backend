<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class ActivityScreenshotDto
{
    public $id;
    public $screenshot_url;
}