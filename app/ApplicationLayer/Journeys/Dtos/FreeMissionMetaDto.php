<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class FreeMissionMetaDto
{
    public $id;
    public $name;
    public $videoUrl;
    public $weight;
    public $description;
    public $iconUrl;
    public $type;
    public $screenshots = [];
    public $programmingLanguageType;
}