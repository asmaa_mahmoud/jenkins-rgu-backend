<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class McqChoiceDto
{
    public $id;
    public $choose;
    public $is_correct;
    public $question_id;
}