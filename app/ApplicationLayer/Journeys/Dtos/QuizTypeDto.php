<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizTypeDto
{
    public $id;
    public $name;
}