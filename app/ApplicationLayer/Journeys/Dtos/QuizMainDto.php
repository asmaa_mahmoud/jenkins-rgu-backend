<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizMainDto
{
    public $id;
    public $taskId;
    public $title;
    public $adventureId;
}