<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class EmulatorTypeDto
{
    public $id;
    public $name;
}