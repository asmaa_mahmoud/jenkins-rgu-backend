<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 5:21 PM
 */

namespace App\ApplicationLayer\Journeys\Dtos;


class MissionAngularDto
{
    public $id;
    public $title;
    public $order;
    public $weight;
    public $description;
    public $iconUrl;
    public $screenshots = [];
    public $quiz_id;
    public $quiz_type;
    public $files = [];
    public $dependencies = [];
}