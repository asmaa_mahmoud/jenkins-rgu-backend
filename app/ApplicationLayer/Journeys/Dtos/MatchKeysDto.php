<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MatchKeysDto
{
    public $id;
    public $text;
    public $order;
    public $value_id;
    public $answered_value_id;

}