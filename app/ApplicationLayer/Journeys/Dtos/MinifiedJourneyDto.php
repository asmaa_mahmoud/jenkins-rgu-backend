<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MinifiedJourneyDto
{
    public $Id;
    public $Name;
    public $CardIconURL;
    public $Status;
    public $gradeNumber;
}