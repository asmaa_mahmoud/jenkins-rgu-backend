<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class ConfidenceDto
{
    public $value;
    public $name;
}