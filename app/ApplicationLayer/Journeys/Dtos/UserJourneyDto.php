<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class UserJourneyDto
{
    public $subscription_Id;
    public $journey;
}