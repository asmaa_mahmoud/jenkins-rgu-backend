<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizProgressDto
{
    public $id;
    public $quiz_score;
    public $no_of_trails;
}