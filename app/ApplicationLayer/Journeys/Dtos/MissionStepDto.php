<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MissionStepDto
{
    public $id;
    public $title;
    public $description;
    public $imageUrl;
    public $videoUrl;
    public $order;
    public $editorText;
    public $brickType;
    

}