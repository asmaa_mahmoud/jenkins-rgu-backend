<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MissionSectionDto
{
    public $id;
    public $title;
    public $type;
    public $bricks= [];
}