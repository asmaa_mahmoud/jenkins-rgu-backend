<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class StepContentDto
{
    public $id;
    public $brickType;
    public $content;

}