<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 3:47 PM
 */

namespace App\ApplicationLayer\Journeys\Dtos;


class MissionCodingDto
{
    public $id;
    public $title;
    public $order;
    public $weight;
    public $duration;
    public $description;
    public $iconUrl;
    public $vplId;
    public $programmingLanguage;
    public $descriptionSpeech;
    public $code;
    public $screenshots =[];
   }