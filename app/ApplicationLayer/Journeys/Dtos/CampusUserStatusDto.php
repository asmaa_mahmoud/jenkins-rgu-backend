<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class CampusUserStatusDto
{
    public $campus_id;
    public $rank;
    public $level;
    public $xp;
    // public $streak_xp;
    public $hide_blocks;


}