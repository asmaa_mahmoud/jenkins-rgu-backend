<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 23/10/2018
 * Time: 5:21 PM
 */

namespace App\ApplicationLayer\Journeys\Dtos;


class MissionAngularAnswersDto
{
    public $id;
    public $name;
    public $data;
}