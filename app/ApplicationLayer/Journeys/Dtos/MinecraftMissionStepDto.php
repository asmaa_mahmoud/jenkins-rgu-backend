<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class MinecraftMissionStepDto
{
    public $imageUrl;
    public $order;
    public $title;
    public $description;
}