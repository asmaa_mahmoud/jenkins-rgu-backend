<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class DefaultActivityB2CDto
{
    public $id;
    public $iconUrl;
    public $imageUrl;
    public $difficulty;
    public $name;
    public $screenshots;
    public $description;
    public $ageFrom;
    public $ageTo;
}