<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class DragCellDto
{
    
    public $id;
    public $showAnswer;
    public $text;
    public $order;
    public $correctChoiceId;
    public $userAnswer = null;

    
}