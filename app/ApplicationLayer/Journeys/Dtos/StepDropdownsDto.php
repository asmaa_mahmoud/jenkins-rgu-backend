<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class StepDropdownsDto
{
    public $id;
    public $rightExplanation;
    public $wrongExplanation;
    public $choices= [];
}