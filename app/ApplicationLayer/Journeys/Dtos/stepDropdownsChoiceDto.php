<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class stepDropdownsChoiceDto
{
    public $id;
    public $choice;
    public $isModelAnswer;
    public $explanation;
    public $order;
    public $selected = false;

}