<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class DragQuestionDto
{
    public $brickType;
    public $id;
    public $questionId;
    public $title;
    public $description;
    public $explanation;
    public $rightExplanation;
    public $wrongExplanation;
    public $dragCells= [];
    public $dragChoices= [];
}