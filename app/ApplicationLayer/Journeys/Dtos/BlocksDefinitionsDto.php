<?php

namespace App\ApplicationLayer\Journeys\Dtos;
use stdClass;

class BlocksDefinitionsDto
{
    public $block;
    public $blocklyJson;
    public $pythonJson;
    public $javascriptJson;
}