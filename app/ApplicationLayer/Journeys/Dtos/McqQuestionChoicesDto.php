<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class McqQuestionChoicesDto
{
    public $id;
    public $is_answer;
    public $text;
    public $explanation;
    public $wrongExplanation;
    public $selected = false;
}