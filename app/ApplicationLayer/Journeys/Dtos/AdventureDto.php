<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class AdventureDto
{
    public $id;
    public $name_english;
    public $journey_id;
    public $desctiption_english;
    public $link;
}