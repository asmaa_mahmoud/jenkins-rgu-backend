<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class StepMcqQuestionDto
{
    public $brickType;
    public $id;
    public $questionId;
    public $selectMany;
    public $body;
    public $choices = [];

}