<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class StepMatchQuestionDto
{
    public $brickType;
    public $id;
    public $questionId;
    public $title;
    public $description;
    public $rightExplanation;
    public $wrongExplanation;
        // public $keys = [];
        // public $values = [];

}