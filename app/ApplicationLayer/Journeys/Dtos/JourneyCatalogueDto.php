<?php
/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/12/2017
 * Time: 3:03 PM
 */

namespace App\ApplicationLayer\Journeys\Dtos;


class JourneyCatalogueDto
{
    public $id;
    public $name;
    public $description;
    public $cardIconURL;
}