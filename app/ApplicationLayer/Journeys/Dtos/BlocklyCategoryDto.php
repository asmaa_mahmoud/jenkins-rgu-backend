<?php

/**
 * Created by PhpStorm.
 * User: RVM-13
 * Date: 2/23/2017
 * Time: 3:31 PM
 */

namespace App\ApplicationLayer\Journeys\Dtos;

class BlocklyCategoryDto
{
    public $Name;
    public $Blocks;
}