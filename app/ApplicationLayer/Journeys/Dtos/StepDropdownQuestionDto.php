<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class StepDropdownQuestionDto
{
    public $brickType;
    public $id;
    public $questionId;
    public $title;
    public $description;
    public $dropdowns= [];
}