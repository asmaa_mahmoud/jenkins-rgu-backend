<?php

namespace App\ApplicationLayer\Journeys\Dtos;

class QuizDto
{
    public $id;
    public $title;
    public $quiz_type;
    public $link;
    public $top;
    public $left;
}