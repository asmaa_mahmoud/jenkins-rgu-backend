<?php

namespace App\ApplicationLayer\Journeys;

use Analogue\ORM\EntityMap;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\MissionHtmlSteps;

class QuestionTaskMap extends EntityMap {

    protected $table = 'question_task';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "question_task.deleted_at";


    public function question(QuestionTask $questionTask)
    {
        return $this->belongsTo($questionTask, Question::class , 'question_id', 'id');
    }


    public function step(QuestionTask $questionTask)
    {
        return $this->belongsTo($questionTask, MissionHtmlSteps::class , 'html_step_id', 'id');
    }

}