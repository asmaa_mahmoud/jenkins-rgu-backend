<?php

namespace App\ApplicationLayer\Journeys;

use App\ApplicationLayer\Journeys\Dtos\ChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\QuizProgressDto;
use App\ApplicationLayer\Journeys\Dtos\QuizTutorialsDto;
use App\ApplicationLayer\Journeys\Dtos\QuizMainDto;
use App\ApplicationLayer\Journeys\Dtos\QuizQuestionsDto;
use App\ApplicationLayer\Journeys\Dtos\QuizChoicesDto;
use App\ApplicationLayer\Journeys\Dtos\ConfidenceDto;
use App\ApplicationLayer\Journeys\Dtos\EmulatorTypeDto;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Journeys\TaskProgress;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Journeys\TaskProgressActivity;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use App\Framework\Exceptions\BadRequestException;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\ApplicationLayer\Common\LessonCommonService;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;


class QuizService
{
    private $journeyRepository;
    private $accountRepository;
    private $lessonCommonService;
    private $professionalRepository;


    public function __construct(IJourneyMainRepository $journeyRepository,IAccountMainRepository $accountRepository,IProfessionalMainRepository $professionalRepository=null)
    {
        $this->journeyRepository = $journeyRepository;
        $this->accountRepository = $accountRepository;
        $this->professionalRepository =$professionalRepository;
        if($professionalRepository != null)
            $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);
        

    }

    public function GetQuiz($id,$activity_id,$round_id,$type,$user)
    {
        
        $quiz = $this->journeyRepository->getquiz($id);
        $round=$this->professionalRepository->getRoundById($round_id);
        if($round == null)
            throw new BadRequestException(trans("locale.campus_round_not_exist"));

        $campus_id=$round->getCampus()->getId();
        $campus = $round->getCampus();
        $campusHasSubModules = $campus->hasSubModules();
        //Check Locker
        if($type == 'quiz' || $type == 'emulator'){
            $task = $quiz->getTask();
            $this->lessonCommonService->checkTaskLocker($task,$campus_id,$activity_id,$user->id,true,$campusHasSubModules);        
        }
        elseif($type == 'editor_mission'){
            
            $missionEditor = $this->professionalRepository->getEditorMissionByQuiz($campus_id, $activity_id, $id, true);
            $editorTask = $missionEditor->getTask();
            $this->lessonCommonService->checkTaskLocker($editorTask,$campus_id,$activity_id,$user->id,true,$campusHasSubModules);        
        }
        elseif($type == 'angular_mission'){
            
            $missionAngular = $this->professionalRepository->getAngularMissionByQuiz($campus_id, $activity_id, $id, true);
            $angularTask = $missionAngular->getTask();
            $this->lessonCommonService->checkTaskLocker($angularTask,$campus_id,$activity_id,$user->id,true,$campusHasSubModules);  
        }


        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));

        $quiz_info =  Mapper::MapClass(QuizMainDto::class,$quiz);
        $questionsByQuiz = $this->journeyRepository->getQuestionsByQuiz($quiz);
        
        $questions = Mapper::MapEntityCollection(QuizQuestionsDto::class,$questionsByQuiz,[QuizChoicesDto::class]);
        return ["quiz"=>$quiz_info,"questions"=>$questions];
    }

    public function getConfidence(){
        $confidenceOptions = $this->journeyRepository->getConfidence();
        $confidence = Mapper::MapEntityCollection(ConfidenceDto::class,$confidenceOptions);
        return $confidence;
    }

    public function getTutorials($id)
    {
        $quiz = $this->journeyRepository->getquiz($id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));
        return Mapper::MapEntityCollection(QuizTutorialsDto::class,$quiz->getTutorials());
    }

    public function submitQuiz($data, $id, $user_id,$journey_id,$adventure_id)
    {
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));
    
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));
        $passed = 0;
        $mark_counter = 0;
        $score = 0;

        $questions_answers = [];

        $quiz = $this->journeyRepository->getquiz($id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));
        $task = $quiz->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        foreach ($data as $question)
        {
            $question_instance = $this->journeyRepository->getQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));
            $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));
            $choice_correct = Mapper::MapEntityCollection(ChoiceDto::class, $this->journeyRepository->getChoiceModelforQuestion($question['question_id']));
            $question_answer = ["question_id" => $question['question_id'],
            "question_body" => $question_instance->getBody(),
            "user_choice" => $choice_instance->getText(),
            "choice_correct" => $choice_correct[0]->text,
            "eval" => false];
            if($choice_correct[0]->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question['weight'];
            }
            $questions_answers[] = $question_answer;
        }
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $quiz_progress_object = $this->journeyRepository->getprogressforquiz($id, $user_id,$journey_id,$adventure_id);
        if($quiz_progress_object != null && $passed)
        {
            $trails = $quiz_progress_object->getNo_of_trails() + 1;
            $max_score = ($score > $quiz_progress_object->getScore()? $score:$quiz_progress_object->getScore());

            if($score > $quiz_progress_object->getScore()){
                $addedScore = $score - $quiz_progress_object->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }

            if($quiz_progress_object->getFirstSuccess() != null){
                $firstSuccess = $quiz_progress_object->getFirstSuccess();
            }
            else {
                $firstSuccess = 1;
            }
            $this->journeyRepository->removeTaskProgress($quiz_progress_object);
            $task_progress = new TaskProgress($task,$user,$max_score,$journey_id,$adventure_id,$trails,$firstSuccess);
            $this->journeyRepository->storeTaskProgress($task_progress);
        }
        elseif($passed){
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
            $task_progress = new TaskProgress($task,$user,$score,$journey_id,$adventure_id,1,1);
            $this->journeyRepository->storeTaskProgress($task_progress);
        }
        return ["passed"=>$passed,"questions_answers"=>$questions_answers];
    }

    public function checkActivityB2Authority($user_id, $activity_id, $is_default = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $accountType = $account->getAccountType()->getName();
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = false;
        if($activity->isB2B()){
            if($accountType == 'School')
                $authority = true;
        }
        if($activity->isB2C()){
            if($accountType == 'Family' || $accountType == 'Individual')
                $authority = true;
        }
        return $authority;
    }

    public function submitActivityQuiz($data, $id, $user_id,$activity_id,$is_default = true)
    {
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $passed = 0;
        $mark_counter = 0;
        $score = 0;
        $questions_answers = [];
        $quiz = $this->journeyRepository->getquiz($id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));
        $task = $quiz->getTask();
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        foreach ($data as $question)
        {
            $question_instance = $this->journeyRepository->getQuestionById($question['question_id']);
            if($question_instance == null)
                throw new BadRequestException(trans('locale.question_not_exist'));
            $choice_instance = $this->journeyRepository->getChoiceById($question['choice']);
            if($choice_instance == null)
                throw new BadRequestException(trans('locale.choice_not_exist'));
            $choice_correct = Mapper::MapEntityCollection(ChoiceDto::class, $this->journeyRepository->getChoiceModelforQuestion($question['question_id']));
            $question_answer = ["question_id" => $question['question_id'],
                "question_body" => $question_instance->getBody(),
                "user_choice" => $choice_instance->getText(),
                "choice_correct" => $choice_correct[0]->text,
                "eval" => false];
            if($choice_correct[0]->id == $question['choice'])
            {
                $question_answer['eval'] = true;
                $mark_counter = $mark_counter + 1 ;
                $score = $score + $question['weight'];
            }
            $questions_answers[] = $question_answer;
        }
        if($mark_counter >= (sizeof($data)/2))
            $passed = 1;

        $quiz_progress_object = $this->journeyRepository->getUserTaskProgressActivity($task, $user_id,$activity_id,$is_default);
        if($quiz_progress_object != null && $passed)
        {
            $trails = $quiz_progress_object->getNo_of_trails() + 1;
            $max_score = ($score > $quiz_progress_object->getScore()? $score:$quiz_progress_object->getScore());

            if($score > $quiz_progress_object->getScore()){
                $addedScore = $score - $quiz_progress_object->getScore();
                $userScore = new UserScore($user,$user->getExperience()+$addedScore,$user->getCoins()+($addedScore/5));
                $this->accountRepository->removeUserScore($user);
                $this->accountRepository->addUserScore($userScore);
            }

            if($quiz_progress_object->getFirstSuccess() != null){
                $firstSuccess = $quiz_progress_object->getFirstSuccess();
            }
            else {
                $firstSuccess = 1;
            }
            $this->journeyRepository->deleteTaskProgressActivity($quiz_progress_object);
            $task_progress = new TaskProgressActivity($task,$user,$max_score,$trails,$firstSuccess);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->journeyRepository->storeTaskProgressActivity($task_progress);
        }
        elseif($passed){
            $userScore = new UserScore($user,$user->getExperience()+$score,$user->getCoins()+($score/5));
            $this->accountRepository->removeUserScore($user);
            $this->accountRepository->addUserScore($userScore);
            $task_progress = new TaskProgressActivity($task,$user,$score,1,1);
            if($is_default)
                $task_progress->setDefaultActivity($activity);
            else
                $task_progress->setActivity($activity);
            $this->journeyRepository->storeTaskProgressActivity($task_progress);
        }
        return ["passed"=>$passed,"questions_answers"=>$questions_answers];
    }

    public function getNextQuiz($quiz_id,$journey_id,$adventure_id)
    {
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
            if($adventure == null)
                throw new BadRequestException(trans('locale.adventure_not_exist'));

        $task_index = $adventure->getOrder();

        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));
        $task = $quiz->getTask();

        $order = $this->journeyRepository->getTaskOrder($task,$journey_id,$adventure_id)->getOrder();

        $last_task = $this->journeyRepository->getLastTaskInAdventure($journey_id,$adventure_id);

        if($task->getId() != $last_task->getId())
        {
            $next_task = $this->journeyRepository->getTaskByOrder(intval($order) + 1,$journey_id,$adventure_id);
        }
        else
        {

            $next_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() + 1,$journey_id);

            if($next_adventure == null)
                throw new BadRequestException(trans('locale.next_adventure_not_exist'));

            $next_task = $this->journeyRepository->getFirstTaskInAdventure($journey_id,$next_adventure->getId());

            $task_index = $next_adventure->getOrder();

        }

        if($next_task == null)
            throw new BadRequestException(trans('locale.next_task_not_exist'));

        //$default_journey = $journey;

        // $adventures = $this->journeyRepository->getAdventuresByOrder($default_journey->getId());

        // $task_index = 0;

        // $counter = 0;
        // $flag = false;
        // $next_adventure = 0;

        // foreach ($adventures as $adventure) {
        //     if($adventure->getId() == $adventure_id)
        //     {
        //         $task_index = $counter;
        //         $flag = true; 
        //         continue;               
        //     }
        //     if($flag)
        //     {
        //         $next_adventure = $adventure->getId();
        //         break;
        //     }


        //     $counter++;
        // }
        $task_index--;

        $next_mission = $next_task->getMissions()->first();
        $next_html_mission = $next_task->getMissionsHtml()->first();
        if($next_mission != null)
        {
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(),"next_task_type"=>"mission","next_task_index" => $task_index, "next_task_adventure" => $adventure_id];
        }

        else if($next_html_mission != null){
            return ["next_task_id" => $next_html_mission->getId(), "next_task_name" => $next_html_mission->getTitle(),"next_task_type"=>"html_mission","next_task_index" => $task_index, "next_task_adventure" => $adventure_id];
        }
        else
        {
            $next_quiz = $next_task->getQuizzes()->first();

            return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(),"next_task_type"=>"quiz","next_task_index" => $task_index, "next_task_adventure" => $next_adventure]; 
        }
    }
}