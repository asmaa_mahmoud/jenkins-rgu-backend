<?php

namespace App\ApplicationLayer\Journeys;

use App\ApplicationLayer\Journeys\Dtos\ActivityScreenshotDto;
use App\ApplicationLayer\Journeys\Dtos\DefaultActivityB2CDto;
use App\ApplicationLayer\Journeys\Dtos\DifficultyDto;
use App\ApplicationLayer\Journeys\Dtos\TagDto;
use App\ApplicationLayer\Schools\Dtos\ActivityDto;
use App\DomainModelLayer\Accounts\AccountUnlockable;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Journeys\Task;
use App\Helpers\Mapper;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;

class ActivityService
{
    //region Properties
    private $accountRepository;
    private $schoolRepository;
    private $journeyRepository;
    //endregion

    //region Constructor
    public function __construct(IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IJourneyMainRepository $journeyRepository){
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->journeyRepository = $journeyRepository;
    }
    //endregion

    public function checkData($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        return ['user' => $user];
    }

    public function checkActivityB2Authority($user_id, $activity_id, $is_default = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $accountType = $account->getAccountType()->getName();
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = false;
        if($activity->isB2B()){
            if($accountType == 'School')
                $authority = true;
        }
        if($activity->isB2C()){
            if($accountType == 'Family' || $accountType == 'Individual')
                $authority = true;
        }
        return $authority;
    }

    public function getAllActivities($user_id, $start_from, $limit, $search){
        $this->checkData($user_id);

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $activities = $this->journeyRepository->getAllActivities($limit, $search, null, true);
        $activitiesMapped = Mapper::MapEntityCollection(ActivityDto::class, $activities, [DifficultyDto::class, ActivityScreenshotDto::class, TagDto::class]);
        return $activitiesMapped;
    }

    public function countActivitiesForB2C($user_id,$search){

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        if(!($account->getAccountType()->getName() == "Individual" || $account->getAccountType()->getName() == "Family"))
            throw new BadRequestException(trans("locale.account_type_must_be_individual_or_homeschooler"));

        $noOfActivities = $this->journeyRepository->getAllActivities(null, $search,true, null, true);
        return ["count"=>$noOfActivities];
    }

    public function countAllMinecraftActivities($user_id,$search){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        if(!($account->getAccountType()->getName() == "Individual" || $account->getAccountType()->getName() == "Family"))
            throw new BadRequestException(trans("locale.account_type_must_be_individual_or_homeschooler"));

        $noOfActivities = $this->journeyRepository->getAllMineCraftActivities(null, $search,true, null, true);
        return ["count" => $noOfActivities];
    }

    public function getAllActivitiesForB2CNonUser($start_from,$limit,$search){

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $activitiesArray = [];
        $activities = $this->journeyRepository->getAllActivities($limit, $search, null, null, true);

        foreach ($activities as $activity){
//            $activityDto = Mapper::MapEntity(DefaultActivityB2CDto::class,$activity);

            $activityDto['id'] = $activity->getId();
            $activityDto['name'] = $activity->getName();
            $activityDto['difficulty'] = $activity->getDifficulty()->getName();
            $activityDto['difficultyTitle'] = $activity->getDifficulty()->getTitle();
            $activityDto['ageFrom'] = $activity->getAgeFrom();
            $activityDto['ageTo'] = $activity->getAgeTo();
            $activityDto['description'] = $activity->getDescription();
            $activityDto['imageUrl'] = $activity->getImageUrl();
            $activityDto['iconUrl'] = $activity->getIconUrl();
            $activityDto['tags'] = [];
            $activityDto['screenshots'] = [];
            foreach ($activity->getTags() as $tag){
                array_push($activityDto['tags'],$tag->getName());
            }
            foreach ($activity->getScreenshots() as $screenshot){
                array_push($activityDto['screenshots'],$screenshot->getScreenshotUrl());
            }

            $activityPriceFamily = $this->journeyRepository->getActivityPrice($this->accountRepository->getAccountTypeByName("Family")->getId(),$activity->getId(),true);
            $activityPriceIndividual = $this->journeyRepository->getActivityPrice($this->accountRepository->getAccountTypeByName("Individual")->getId(),$activity->getId(),true);
            $activityDto['priceFamily'] = $activityPriceFamily->getPrice();
            $activityDto['priceIndividual'] = $activityPriceIndividual->getPrice();
            $activityDto['discountFamily'] = $activityPriceFamily->getDiscount();
            $activityDto['discountIndividual'] = $activityPriceIndividual->getDiscount();
            array_push($activitiesArray,$activityDto);
        }

        return $activitiesArray;

    }

    public function getAllActivitiesForB2C($user_id, $start_from, $limit, $search, $is_mobile = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        if(!($account->getAccountType()->getName() == "Individual" || $account->getAccountType()->getName() == "Family"))
            throw new BadRequestException(trans("locale.account_type_must_be_individual_or_homeschooler"));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        
        $activitiesArray = [];
        $activities = $this->journeyRepository->getAllActivities($limit, $search, null, null, true);
        $userActivitiesIds = $this->getUserDefaultActivityIds($user_id, $is_mobile);

        foreach ($activities as $activity){
            $locked = true;
            foreach ($userActivitiesIds as $userActivitiesId){
                if($userActivitiesId == $activity->getId()){
                    $locked = false;
                    break;
                }
            }
            $activityUnlockable = $this->journeyRepository->getDefaultActivityUnlockable($activity->getId(), $is_mobile);
            $activityDto['id'] = $activity->getId();
            $activityDto['name'] = $activity->getName();
            $activityDto['difficulty'] = $activity->getDifficulty()->getName();
            $activityDto['difficultyTitle'] = $activity->getDifficulty()->getTitle();
            $activityDto['ageFrom'] = $activity->getAgeFrom();
            $activityDto['ageTo'] = $activity->getAgeTo();
            $activityDto['description'] = $activity->getDescription();
            $activityDto['imageUrl'] = $activity->getImageUrl();
            $activityDto['iconUrl'] = $activity->getIconUrl();
            $activityDto['unlocked_coins'] = false;
            $activityDto['unlocked_xp'] = false;
            $activityDto['tags'] = [];
            $activityDto['screenshots'] = [];
            foreach ($activity->getTags() as $tag){
                array_push($activityDto['tags'],$tag->getName());
            }
            foreach ($activity->getScreenshots() as $screenshot){
                array_push($activityDto['screenshots'], $screenshot->getScreenshotUrl());
            }
            $allScreenshots = array();
            foreach ($activity->getTasks() as $task){
                $mission = $task->getMissions()->first();
                if($mission != null) {
                    $screenshots = $mission->getScreenshots();
                    foreach ($screenshots as $screen){
                        array_push($allScreenshots, $screen->getUrl());
                    }
                }
                else {
                    $mission = $task->getMissionsHtml()->first();
                    if ($mission != null) {
                        $screenshots = $mission->getScreenshots();
                        foreach ($screenshots as $screen){
                            array_push($allScreenshots, $screen->getUrl());
                        }
                    }
                }
            }
            $activityDto['tasksScreenshots'] = $allScreenshots;
            $activityDto['locked'] = $locked;
            if($activityUnlockable != null){
                $activityDto['coins_to_unlock'] = $activityUnlockable->getUnlockable()->getCoinsToUnlock();
                $activityDto['xp_to_unlock'] = $activityUnlockable->getUnlockable()->getXpToUnlock();
                $accountUnlockable = $this->accountRepository->getAccountUnlockable($account->getId(), $activityUnlockable->getUnlockable()->getId());
                if($accountUnlockable != null){
                    if($accountUnlockable->getMethod()->getName() == 'coins')
                        $activityDto['unlocked_coins'] = true;
                    elseif($accountUnlockable->getMethod()->getName() == 'xp')
                        $activityDto['unlocked_xp'] = true;
                }
            }
            $activityPrice = $this->journeyRepository->getActivityPrice($account->getAccountType()->getId(),$activity->getId(),true);
            $activityDto['price'] = $activityPrice->getPrice();
            $activityDto['discount'] = $activityPrice->getDiscount();
            array_push($activitiesArray,$activityDto);
        }
        return $activitiesArray;
    }

    public function getAllMinecraftActivities($user_id,$start_from, $limit, $search, $is_mobile = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        if(!($account->getAccountType()->getName() == "Individual" || $account->getAccountType()->getName() == "Family"))
            throw new BadRequestException(trans("locale.account_type_must_be_individual_or_homeschooler"));

        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        $activitiesArray = [];
        $activities = $this->journeyRepository->getAllMineCraftActivities($limit, $search, null, null, true);
        $userActivitiesIds = $this->getUserDefaultActivityIds($user_id, $is_mobile);

        foreach ($activities as $activity){
            $locked = true;
            foreach ($userActivitiesIds as $userActivitiesId){
                if($userActivitiesId == $activity->getId()){
                    $locked = false;
                    break;
                }
            }
            $activityDto['id'] = $activity->getId();
            $activityDto['name'] = $activity->getName();
            $activityDto['difficulty'] = $activity->getDifficulty()->getName();
            $activityDto['difficultyTitle'] = $activity->getDifficulty()->getTitle();
            $activityDto['ageFrom'] = $activity->getAgeFrom();
            $activityDto['ageTo'] = $activity->getAgeTo();
            $activityDto['description'] = $activity->getDescription();
            $activityDto['imageUrl'] = $activity->getImageUrl();
            $activityDto['iconUrl'] = $activity->getIconUrl();
            $activityDto['tags'] = [];
            $activityDto['screenshots'] = [];
            foreach ($activity->getTags() as $tag){
                array_push($activityDto['tags'], $tag->getName());
            }
            foreach ($activity->getScreenshots() as $screenshot){
                array_push($activityDto['screenshots'], $screenshot->getScreenshotUrl());
            }
            $allScreenshots = array();
            foreach ($activity->getTasks() as $task){
                $mission = $task->getMissions()->first();
                if($mission != null) {
                    $screenshots = $mission->getScreenshots();
                    foreach ($screenshots as $screen){
                        array_push($allScreenshots, $screen->getUrl());
                    }
                }
            }
            $activityDto['tasksScreenshots'] = $allScreenshots;
            $activityDto['locked'] = $locked;
            $activityPrice = $this->journeyRepository->getActivityPrice($account->getAccountType()->getId(),$activity->getId(),true);
            $activityDto['price'] = $activityPrice->getPrice();
            $activityDto['discount'] = $activityPrice->getDiscount();
            array_push($activitiesArray, $activityDto);
        }
        return $activitiesArray;
    }

    public function getUserDefaultActivityIds($user_id, $is_mobile = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        $unlockedActivityIds = [];
        $is_mobile == true ? 1 : 0;

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        foreach ($subscriptions as $subscription){
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription != null){
                $invitation = $invitationSubscription->getInvitation();
                foreach ($invitation->getDefaultActivities() as $invitation_default_activity) {
                    if(!in_array($invitation_default_activity->getDefaultActivity()->getId(), $unlockedActivityIds)){
                        array_push($unlockedActivityIds, $invitation_default_activity->getDefaultActivity()->getId());
                    }
                }
                $plans = $invitation->getPlans();
                foreach ($plans as $plan) {
                    $unlockables = $plan->getPlanUnlockables();
                    foreach ($unlockables as $unlockable){
                        if($is_mobile == $unlockable->getUnlockable()->isMobile()){
                            $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                            if($activityUnlockable != null){
                                $defaultActivity = $activityUnlockable->getDefaultActivity();
                                if($defaultActivity != null){
                                    if(!in_array($defaultActivity->getId(),$unlockedActivityIds)){
                                        array_push($unlockedActivityIds,$defaultActivity->getId());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                $plan = $subscription->getPlan();
                $unlockables = $plan->getPlanUnlockables();
                foreach ($unlockables as $unlockable){
                    if($is_mobile == $unlockable->getUnlockable()->isMobile()) {
                        $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                        if ($activityUnlockable != null) {
                            $defaultActivity = $activityUnlockable->getDefaultActivity();
                            if ($defaultActivity != null) {
                                if (!in_array($defaultActivity->getId(), $unlockedActivityIds)) {
                                    array_push($unlockedActivityIds, $defaultActivity->getId());
                                }
                            }
                        }
                    }
                }
            }
        }

        $unlockables = $account->getAccountUnlockables();
        foreach ($unlockables as $unlockable){
            if($is_mobile == $unlockable->getUnlockable()->isMobile()) {
                $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                if($activityUnlockable != null){
                    $defaultActivity = $activityUnlockable->getDefaultActivity();
                    if($defaultActivity != null){
                        if(!in_array($defaultActivity->getId(),$unlockedActivityIds)){
                            array_push($unlockedActivityIds,$defaultActivity->getId());
                        }
                    }
                }
            }
        }
        return $unlockedActivityIds;
    }

    public function getActivity($user_id, $activity_id, $is_default = true, $is_mobile = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $userActivitiesIds = $this->getUserDefaultActivityIds($user_id, $is_mobile);
        $hasAccess = false;
        foreach ($userActivitiesIds as $userActivitiesId){
            if($activity_id == $userActivitiesId)
                $hasAccess = true;
        }
        if(!$hasAccess)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $activityUnlockable = $this->journeyRepository->getDefaultActivityUnlockable($activity->getId(), $is_mobile);

        $activityDto['id'] = $activity->getId();
        $activityDto['name'] = $activity->getName();
        $activityDto['difficulty'] = $activity->getDifficulty()->getName();
        $activityDto['difficultyTitle'] = $activity->getDifficulty()->getTitle();
        $activityDto['ageFrom'] = $activity->getAgeFrom();
        $activityDto['ageTo'] = $activity->getAgeTo();
        $activityDto['description'] = $activity->getDescription();
        $activityDto['imageUrl'] = $activity->getImageUrl();
        $activityDto['iconUrl'] = $activity->getIconUrl();
        $activityDto['unlocked_coins'] = false;
        $activityDto['unlocked_xp'] = false;
        $activityDto['tags'] = [];
        $activityDto['screenshots'] = [];
        foreach ($activity->getTags() as $tag){
            array_push($activityDto['tags'],$tag->getName());
        }
        foreach ($activity->getScreenshots() as $screenshot){
            array_push($activityDto['screenshots'],$screenshot->getScreenshotUrl());
        }
        $activityPrice = $this->journeyRepository->getActivityPrice($account->getAccountType()->getId(),$activity->getId(),$is_default);
        $activityDto['price'] = $activityPrice->getPrice();
        $activityDto['discount'] = $activityPrice->getDiscount();
        if($activityUnlockable != null){
            $activityDto['coins_to_unlock'] = $activityUnlockable->getUnlockable()->getCoinsToUnlock();
            $activityDto['xp_to_unlock'] = $activityUnlockable->getUnlockable()->getXpToUnlock();
            $accountUnlockable = $this->accountRepository->getAccountUnlockable($account->getId(), $activityUnlockable->getUnlockable()->getId());
            if($accountUnlockable != null){
                if($accountUnlockable->getMethod()->getName() == 'coins')
                    $activityDto['unlocked_coins'] = true;
                elseif($accountUnlockable->getMethod()->getName() == 'xp')
                    $activityDto['unlocked_xp'] = true;
            }
        }
        return $activityDto;
    }

    public function getActivityTasks($user_id, $activity_id, $is_default = true, $is_mobile = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $userActivitiesIds = $this->getUserDefaultActivityIds($user_id,$is_mobile);
        $hasAccess = false;
        foreach ($userActivitiesIds as $userActivitiesId){
            if($activity_id == $userActivitiesId)
                $hasAccess = true;
        }
        if(!$hasAccess)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $tasks = $activity->getTasks();
        $tasksDto = [];

        $progressUnlocked = $this->checkProgressUnlockedActivity($activity_id, $user_id, $is_default);
        foreach ($tasks as $task){
            $mission = $task->getMissions()->first();
            if($mission != null) {
                $locked = true;

                $minecraftactivities = $activity->getType();
                if($progressUnlocked || $minecraftactivities == 'minecraft')
                    $locked = false;
                else
                    $locked = $this->isLockedTaskActivity($task,$activity_id,$user_id,$is_default);

                $score= 0;
                $taskProgressActivity = $this->journeyRepository->getUserTaskProgressActivity($task,$user_id,$activity_id,$is_default);
                if($taskProgressActivity != null)
                    $score = $taskProgressActivity->getScore();
                array_push($tasksDto, ['id' => $task->getId(),
                    'name' => $mission->getName(),
                    'image'=>$mission->getIconUrl(),
                    'locked' => $locked,
                    'xp' => $mission->getWeight(),
                    'coins'=>$mission->getWeight()/5,
                    'type' => 'mission',
                    'mission_id'=>$mission->getId(),
                    'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default),
                    'score'=>$score
                ]);
            }
            else {
                $mission = $task->getMissionsHtml()->first();
                if($mission != null) {
                    $locked = true;

                    if($progressUnlocked)
                        $locked = false;
                    else
                        $locked = $this->isLockedTaskActivity($task,$activity_id,$user_id,$is_default);

                    array_push($tasksDto, ['id' => $task->getId(),
                        'name' => $mission->getTitle(),
                        'image' => $mission->getIconUrl(),
                        'locked' => $locked,
                        'xp' => $mission->getWeight(),
                        'coins' => $mission->getWeight()/5,
                        'type' => 'html_mission',
                        'mission_id' => $mission->getId(),
                        'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                    ]);
                }
                else {
                    $quiz = $task->getQuizzes()->first();
                    if($quiz != null) {
                        $locked = true;

                        if ($progressUnlocked)
                            $locked = false;
                        else
                            $locked = $this->isLockedTaskActivity($task, $activity_id, $user_id, $is_default);

                        array_push($tasksDto, ['id'=>$task->getId(),
                            'name' => $quiz->getTitle(),
                            'image' => $quiz->geticonURL(),
                            'locked' => $locked,
                            'xp' => $quiz->getQuestionsWeight()['weight'],
                            'coins' => $quiz->getQuestionsWeight()['weight']/5,
                            'type' => $quiz->getType()->getName(),
                            'quiz_id' => $quiz->getId(),
                            'order' => $mission->getTask()->getActivityOrder($activity_id, $is_default)
                        ]);
                    }
                }
            }
        }
        return $tasksDto;
    }

    public function checkProgressUnlockedActivity($activity_id,$user_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($this->accountRepository->isAuthorized('unlock_AllTasks',$user))
            return true;

        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);

        $invitation_subscription = $subscription->getInvitationSubscription()->first();

        if($invitation_subscription == null)
            return false;

        $invitation = $invitation_subscription->getInvitation();

        if($is_default){
            foreach ($invitation->getDefaultActivities() as $invitation_default_activity) {
                if($invitation_default_activity->getDefaultActivity()->getId() == $activity_id && !$invitation_default_activity->getProgressLock())
                {
                    return true;
                }
            }
        }
        else{
            foreach ($invitation->getActivities() as $invitation_activity) {
                if($invitation_activity->getActivity()->getId() == $activity_id && !$invitation_activity->getProgressLock())
                {
                    return true;
                }
            }
        }

        return false;

    }

    public function isLockedTaskActivity(Task $task, $activity_id, $user_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->is_student())
            throw new BadRequestException(trans('locale.students_not_use_service'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $first_task = $this->journeyRepository->getFirstTaskInActivity($activity_id,$is_default);

        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->journeyRepository->getActivityTaskOrder($task,$activity_id,$is_default)->getOrder();


        if($task->getId() != $first_task->getId())
        {
            $latestActivityTaskOrder = $this->journeyRepository->getLastSolvedTaskinActivity($user_id,$activity_id,$is_default);
            if($latestActivityTaskOrder == null)
                return true;
            $latestActivityTaskOrder = $latestActivityTaskOrder->getOrder();
            if($order <= ($latestActivityTaskOrder+1))
                return false;
            else
                return true;
        }
        else
        {
            return false;
        }
    }

    public function checkOpenedModelAnswer($user, $activity_id){
        $returned = false;
        $isUser = false;
        $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;
            if($role->getName() == 'user' || $role->getName() == 'invited_Individual' )
                $isUser = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getDefaultActivities();
            foreach ($journeys as $invJourney){
                if($invJourney->getDefaultActivity()->getId() == $activity_id){
                    if($isUser){
                        if($invJourney->getProgressLock() == 0){
                            $returned = true;
                        }
                    }
                    else{
                        $returned = true;
                    }
                }
            }
        }
        return $returned;
    }

    public function getFreeActivitiesAndJourneys(){
        $freeActivities = []; $freeJourneys = [];
        $activitiesIds = []; $journeysIds = [];
        $activities = $this->journeyRepository->getAllActivities(null, null, null, null, true);
        $plans = $this->accountRepository->getAllFreePlans();
        if(count($activities) > 0){
            foreach ($activities as $activity){
                $activityUnlockable = $this->journeyRepository->getDefaultActivityUnlockable($activity->getId(), false);
                foreach ($plans as $plan){
                    $activityPlan = $this->accountRepository->getPlanUnlockable($plan->getId(), $activityUnlockable->getUnlockable()->getId());
                    if($activityPlan != null){
                        if(!in_array($activity->getId(), $activitiesIds)){
                            $activityData['id'] = $activity->getId();
                            $activityData['name'] = $activity->getName();
                            $activityData['difficulty'] = $activity->getDifficulty()->getName();
                            $activityData['difficultyTitle'] = $activity->getDifficulty()->getTitle();
                            $activityData['ageFrom'] = $activity->getAgeFrom();
                            $activityData['ageTo'] = $activity->getAgeTo();
                            $activityData['description'] = $activity->getDescription();
                            $activityData['imageUrl'] = $activity->getImageUrl();
                            $activityData['iconUrl'] = $activity->getIconUrl();
                            $activityData['tags'] = [];
                            foreach ($activity->getTags() as $tag){
                                array_push($activityData['tags'],$tag->getName());
                            }
                            array_push($freeActivities, $activityData);
                            array_push($activitiesIds, $activity->getId());
                        }
                    }
                }
            }
        }

        $journeys = $this->journeyRepository->getAllJourneys();
        if(count($journeys) > 0){
            foreach ($journeys as $journey){
                $journeyUnlockable = $journey->getDefaultUnlockable();
                if($journeyUnlockable != null){
                    foreach ($plans as $plan) {
                        $journeyPlan = $this->accountRepository->getPlanUnlockable($plan->getId(), $journeyUnlockable->getUnlockable()->getId());
                        if ($journeyPlan != null) {
                            if (!in_array($journey->getId(), $journeysIds)) {
                                $journeyData['id'] = $journey->getId();
                                $journeyData['name'] = $journey->getName();
                                $journeyData['description'] = $journey->getDescription();
                                $journeyData['icon'] = $journey->getCardIconURL();
                                array_push($freeJourneys, $journeyData);
                                array_push($journeysIds, $journey->getId());
                            }
                        }
                    }
                }
            }
        }
        return ['activities' => $freeActivities, 'journeys' => $freeJourneys];
    }

    public function unlockActivityWithCoins($user_id, $activity_id, $coins, $is_default = true, $is_mobile = true){
        $this->accountRepository->beginDatabaseTransaction();

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        if($is_default) {
            $activityUnlockable = $this->journeyRepository->getDefaultActivityUnlockable($activity_id, $is_mobile);
        }
        else{
            $activity = $this->journeyRepository->getActivityById($activity_id);
            $activityUnlockable = $activity->getActivityUnlockable();
        }

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $userActivitiesIds = $this->getUserDefaultActivityIds($user_id, $is_mobile);
        $hasAccess = false;
        foreach ($userActivitiesIds as $userActivitiesId){
            if($activity_id == $userActivitiesId)
                $hasAccess = true;
        }
        if($hasAccess)
            throw new UnauthorizedException(trans('locale.have_access_to_activity'));

        $unlockable = $activityUnlockable->getUnlockable();
        if($coins) {
            $userCoins = $user->getCoins();
            if($userCoins < $unlockable->getCoinsToUnlock())
                throw new BadRequestException(trans('locale.not_enough_coins'));

            $method = $this->accountRepository->getUnlockableMethodByName('coins');

            $userScore = $user->getUserScore();
            $userScore->setCoins(($user->getCoins() - $unlockable->getCoinsToUnlock()));
        }
        else {
            $userXps = $user->getExperience();
            if($userXps < $unlockable->getXpToUnlock())
                throw new BadRequestException(trans('locale.no_enough_xp'));

            $method = $this->accountRepository->getUnlockableMethodByName('xp');

            $userScore = $user->getUserScore();
            $userScore->setExperience(($user->getExperience() - $unlockable->getXpToUnlock()));
        }

        $this->accountRepository->storeUserScore($userScore);
        $accountUnlockable = new AccountUnlockable($account, $unlockable, $method);
        $this->accountRepository->storeAccountUnlockable($accountUnlockable);
        $this->accountRepository->commitDatabaseTransaction();
        return trans('locale.activity_unlocked');
    }



}