<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:17 PM
 */

namespace App\ApplicationLayer\Journeys;


use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Framework\Exceptions\CustomException;
use App\ApplicationLayer\Journeys\Dtos\JourneyDto;
use App\ApplicationLayer\Journeys\Dtos\JourneyCategoryDto;
use App\Helpers\Mapper;
use App\DomainModelLayer\Accounts\UserPosition;
use App\ApplicationLayer\Accounts\Dtos\UserPositionDto;
use Carbon\Carbon;
use App\DomainModelLayer\Journeys\Journey;
use App\DomainModelLayer\Journeys\Adventure;
use App\DomainModelLayer\Journeys\Task;

class JourneyService
{
    private $journeyRepository;
    private $accountRepository;

    public function __construct(IJourneyMainRepository $journeyRepository, IAccountMainRepository $accountRepository)
    {
        $this->journeyRepository = $journeyRepository;
        $this->accountRepository = $accountRepository;
    }

    public function GetAllJourneys(){
        $journeysData = $this->journeyRepository->GetJourneysCatalogue();
        $journeys =  Mapper::MapEntityCollection(JourneyDto::class,$journeysData,[JourneyCategoryDto::class]);
        $counter = 0 ;
        foreach ($journeysData as $journeyData){
            $journeys[$counter]->gradeNumber = $journeyData->getData()->getGrade();
            $counter++;
        }
        $date = array();
        foreach ($journeys as $key => $row)
        {
            $date[$key] = $row->creationDate;
        }
        array_multisort($date, SORT_ASC, $journeys);
        return $journeys;
        //return $this->journeyRepository->GetJourneysCatalogue();
    }

    public function getJourneysSummery()
    {
        
    }

    public function GetJourneyById($id){
//        dd($this->journeyRepository->getJourneyById($id));
        return Mapper::MapClass(JourneyDto::class,$this->journeyRepository->getJourneyById($id));

    }

    public function getJourneyData($id,$user_id)
    {
        $journey = $this->journeyRepository->getJourneyById($id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$this->hasAccessJourney($id,$user_id))
            throw new UnauthorizedException(trans('locale.not_have_access_journey'));

        if($journey->getStatus() != 'published')
            throw new CustomException(trans('locale.journey_not_published'), 277);

        $default_journey = $journey;
        $adventures = $this->journeyRepository->getAdventuresByOrder($default_journey->getId());
        $journey_data = [];
        $counter = 0;
        $progress_unlocked = $this->checkProgressUnlocked($id,$user_id);
        $journey_data['name'] = $journey->getName();
        $journey_data['categories'] = array();

        foreach ($journey->getCategories() as $category){
            array_push($journey_data['categories'],['name'=>$category->getName()]);
        }
        foreach ($adventures as $key => $adventure) {
            $journey_data['adventures'][$counter]['id'] = $adventure->getId();
            $journey_data['adventures'][$counter]['name'] = $adventure->getTitle();
            $journey_data['adventures'][$counter]['description'] = $adventure->getDescription();
            $journey_data['adventures'][$counter]['order'] = $adventure->getOrder();
            $journey_data['adventures'][$counter]['roadmapImageURL'] = $adventure->getRoadMapImage();

            $tasks  = $this->journeyRepository->getTasksByOrder($default_journey->getId(),$adventure->getId());
            $counter_mission = 0;
            $counter_quiz = 0;
            $counter_task = 0;

            foreach ($tasks as $task) {
                //dd($task->getMissions());
                if($task != null){
                    if($task->getMissions()->count() > 0)
                    {
                        $mission = $task->getMissions()->first();

                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['id'] = $mission->getId();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['name'] = $mission->getName();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['type'] = $mission->getType();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['blockly_rules'] = $mission->getBlocklyRules();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['order'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['description'] = $mission->getDescription();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['iconURL'] = $mission->getIconUrl();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_x'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_y'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['vpl_id'] = $mission->getVplId();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['scene_name'] = $mission->getSceneName();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['mission_type'] = $mission->getType();

                        if($progress_unlocked)
                            $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = false;
                        else {
                            $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = $this->isLockedTask($task,$journey,$adventure,$user_id);
                        }
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['first_mission'] = ($counter == 0 && $counter_mission == 0? true:false);
                        $counter_mission++;
                    }
                    else if($task->getMissionsHtml()->count()>0){
                        $missionsHtml = $task->getMissionsHtml()->first();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['id'] = $missionsHtml->getId();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['name'] = $missionsHtml->getTitle();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['type'] = 'html';
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['order'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['description'] = $missionsHtml->getDescription();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_x'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['position_y'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['mission_type'] = 'html';
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['iconURL'] = $missionsHtml->getIconUrl();

                        if($progress_unlocked)
                            $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = false;
                        else {
                            $journey_data['adventures'][$counter]['missions'][$counter_mission]['locked'] = $this->isLockedTask($task,$journey,$adventure,$user_id);
                        }
                        $journey_data['adventures'][$counter]['missions'][$counter_mission]['first_mission'] = ($counter == 0 && $counter_mission == 0? true:false);
                        $counter_mission++;
                    }
                    else
                    {
                        if($task->getQuizzes()->count() > 0)
                        {
                            $quiz = $task->getQuizzes()->first();

                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['id'] = $quiz->getId();
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['Title'] = $quiz->getTitle();
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['iconURL'] = $quiz->geticonURL();
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['type'] = $quiz->getType()->getName();
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['position_x'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['position_y'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['order'] = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();

                            if ($progress_unlocked)
                                $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['locked'] = false;
                            else {
                                $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['locked'] = $this->isLockedTask($task, $journey, $adventure, $user_id);
                            }
                            $journey_data['adventures'][$counter]['quizzes'][$counter_quiz]['last_quiz'] = ($counter == (count($adventures) - 1) && $counter_task == (count($tasks) - 1)? true:false);
                            $counter_quiz++;
                        }
                    }
                    $counter_task++;
                }
            }
                 $counter++; 
        }
        return ["journey_data" => $journey_data, "user_position" => $this->getUserPosition($journey,$user_id)];
    }

    public function getJourneyCategories()
    {
        return Mapper::MapEntityCollection(JourneyCategoryDto::class,$this->journeyRepository->getJourneyCategories());
    }

    public function checkProgressUnlocked($journey_id,$user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($this->accountRepository->isAuthorized('unlock_AllTasks',$user))
            return true;

//        if($user->is_teacher() || $user->is_schoolAdmin())
//            return true;

        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);

        $invitation_subscription = $subscription->getInvitationSubscription()->first();

        if($invitation_subscription == null)
            return false;

        $invitation = $invitation_subscription->getInvitation();

        foreach ($invitation->getJourneys() as $invitation_journey) {
            if($invitation_journey->getJourney()->getId() == $journey_id && !$invitation_journey->getProgressLock())
            {
                return true;
            }
        }

        return false;

    }

    public function checkProgressUnlockedActivity($activity_id,$user_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($this->accountRepository->isAuthorized('unlock_AllTasks',$user))
            return true;

//        if($user->is_teacher() || $user->is_schoolAdmin())
//            return true;

        $account = $user->getAccount();
        $subscription = $this->accountRepository->getLastSubscription($account);

        $invitation_subscription = $subscription->getInvitationSubscription()->first();

        if($invitation_subscription == null)
            return false;

        $invitation = $invitation_subscription->getInvitation();

        if($is_default){
            foreach ($invitation->getDefaultActivities() as $invitation_default_activity) {
                if($invitation_default_activity->getDefaultActivity()->getId() == $activity_id && !$invitation_default_activity->getProgressLock())
                {
                    return true;
                }
            }
        }
        else{
            foreach ($invitation->getActivities() as $invitation_activity) {
                if($invitation_activity->getActivity()->getId() == $activity_id && !$invitation_activity->getProgressLock())
                {
                    return true;
                }
            }
        }

        return false;

    }

    public function getUserPosition(Journey $journey,$user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(count($user->getPosition($journey)) == 0)
        {
            $adventures = $this->journeyRepository->getAdventuresByOrder($journey->getId());

            $adventure = $adventures->first();

            $task = $this->journeyRepository->getFirstTaskInJourney($journey->getId());

            $position_x = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionX();

            $position_y = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getPositionY();

            $userpositiondto = new UserPositionDto();
            $userpositiondto->PositionX = str_replace("px", "", $position_x);
            $userpositiondto->PositionY = str_replace("px", "", $position_y);
            $userpositiondto->Index = 0;

            $position = new UserPosition($userpositiondto,$user,$journey);

            $user->addPosition($position);

            $this->accountRepository->storeUser($user);

            $userpositiondto =  Mapper::MapClass(UserPositionDto::class,$position);
        }
        else
        {
            $userpositiondto = Mapper::MapClass(UserPositionDto::class,$user->getPosition($journey)[0]);
        }

        if($userpositiondto->Index == null)
            $userpositiondto->Index = 0;

        return $userpositiondto;
    }

    public function hasAccessJourney($journey_id,$user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        if($user->is_teacher())
        {
            $groups = $user->getGroupMembers();
            foreach ($groups as $group) {
                $classroom = $group->getGroup()->getClassroom();
                $journeys = $classroom->getJourneys();
                foreach ($journeys as $journey) {
                    if($journey->getId() == $journey_id)
                        return true;
                }
            }
            return false;
        }

        $account = $user->getAccount();
        if($user->is_schoolAdmin())
        {
            $school = $account->getSchool();
            $classrooms = $school->getClassrooms();
            foreach ($classrooms as $classroom) {
                $journeys = $classroom->getJourneys();
                foreach ($journeys as $journey) {
                    if($journey->getId() == $journey_id)
                        return true;
                }
            }
        }
        //$subscription = $this->accountRepository->getLastSubscription($account);
        
        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        foreach ($subscriptions as $subscription) {
            $unlockables = $subscription->getPlan()->getPlanUnlockables();
            foreach ($unlockables as $unlockable){
                $journey_unlockable = $unlockable->getUnlockable()->getJourneyUnlockable();
                if($journey_unlockable != null){
                    $journey = $journey_unlockable->getJourney();
                    if ($journey->getId() == $journey_id)
                            return true;
                }
            }

            if($subscription->getStripeSubscriptions()->first() == null) {
                $invitation_subscription = $subscription->getInvitationSubscription()->first();

                if ($invitation_subscription != null) {

                    $invitation = $invitation_subscription->getInvitation();
                    $journeys = $invitation->getJourneys();

                    foreach ($journeys as $journey) {
                        if ($journey->getJourney()->getId() == $journey_id)
                            return true;
                    }

                    $plans = $invitation->getPlans();
                    foreach ($plans as $plan) {
                        $categories = $plan->getCategories();
                        foreach ($categories as $category) {
                            $journeys = $category->getJourneys();
                            foreach ($journeys as $journey) {
                                if ($journey->getJourney()->getId() == $journey_id)
                                    return true;
                            }
                        }
                    }
                }
            }

            else
            {
                $categories = $subscription->getPlan()->getCategories();

                foreach ($categories as $category) {
                    $journeys = $category->getJourneys();
                    foreach ($journeys as $journey) {
                        if($journey->getId() == $journey_id)
                            return true;
                    }
                }

                $items = $subscription->getItems();

                foreach ($items as $item) {
                    if($item->getEndDate() == null || $item->getEndDate() > Carbon::now())
                    {
                        $categories = $item->getPlan()->getCategories();
                        foreach ($categories as $category) {
                            $journeys = $category->getJourneys();
                            foreach ($journeys as $journey) {
                                if($journey->getId() == $journey_id)
                                    return true;
                            }
                        }
                    }                    
                }
            }
        }

        return false;
    }

    public function isLockedTask(Task $task, Journey $journey, Adventure $adventure, $user_id)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->is_student())
            throw new BadRequestException(trans('locale.students_not_use_service'));


        $first_task = $this->journeyRepository->getFirstTaskInJourney($journey->getId());

        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->journeyRepository->getTaskOrder($task,$journey->getId(),$adventure->getId())->getOrder();

        $first_task_in_adventure = $this->journeyRepository->getFirstTaskInAdventure($journey->getId(),$adventure->getId());

        if($task->getId() != $first_task_in_adventure->getId())
        {
            $progress = $this->journeyRepository->getTaskProgressByOrder($user_id, intval($order) - 1,$journey->getId(),$adventure->getId());
        }
        else
        {           

            $pre_adventure = $this->journeyRepository->getAdventureByOrder($adventure->getOrder() - 1,$journey->getId());

            if($pre_adventure == null)
                throw new BadRequestException(trans('locale.pre_adventure_not_exist'));

            $pre_task =  $this->journeyRepository->getLastTaskInAdventure($journey->getId(),$pre_adventure->getId());

            $pre_task_order = $this->journeyRepository->getTaskOrder($pre_task,$journey->getId(),$pre_adventure->getId())->getOrder();

            $progress = $this->journeyRepository->getTaskProgressByOrder($user_id, $pre_task_order,$journey->getId(),$pre_adventure->getId());

        }
        return (($progress != null)? false:true);
    }


    public function isLockedTaskActivity(Task $task, $activity_id, $user_id,$is_default = true)
    {
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if($user->is_student())
            throw new BadRequestException(trans('locale.students_not_use_service'));

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $first_task = $this->journeyRepository->getFirstTaskInActivity($activity_id,$is_default);

        if($first_task->getId() == $task->getId())
            return false;

        $order = $this->journeyRepository->getActivityTaskOrder($task,$activity_id,$is_default)->getOrder();


        if($task->getId() != $first_task->getId())
        {
            $latestActivityTaskOrder = $this->journeyRepository->getLastSolvedTaskinActivity($user_id,$activity_id,$is_default)->getOrder();
            if($order <= ($latestActivityTaskOrder+1))
                return false;
            else
                return true;
        }
        else
        {
            return false;
        }
    }

    public function getUserDefaultActivityIds($user_id){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $account = $user->getAccount();

        $unlockedActivityIds = [];

        $subscriptions = $this->accountRepository->getActiveSubscriptions($account->getId());
        foreach ($subscriptions as $subscription){
            $invitationSubscription = $subscription->getInvitationSubscription()->first();
            if($invitationSubscription != null){
                $invitation = $invitationSubscription->getInvitation();
                foreach ($invitation->getDefaultActivities() as $invitation_default_activity) {
                    if(!in_array($invitation_default_activity->getDefaultActivity()->getId(),$unlockedActivityIds)){
                        array_push($unlockedActivityIds,$invitation_default_activity->getDefaultActivity()->getId());
                    }
                }
                $plans = $invitation->getPlans();
                foreach ($plans as $plan) {
                    $unlockables = $plan->getPlanUnlockables();
                    foreach ($unlockables as $unlockable){
                        $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                        if($activityUnlockable != null){
                            $defaultActivity = $activityUnlockable->getDefaultActivity();
                            if($defaultActivity != null){
                                if(!in_array($defaultActivity->getId(),$unlockedActivityIds)){
                                    array_push($unlockedActivityIds,$defaultActivity->getId());
                                }
                            }
                        }
                    }
                }
            }
            else{
                $plan = $subscription->getPlan();
                $unlockables = $plan->getPlanUnlockables();
                foreach ($unlockables as $unlockable){
                    $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
                    if($activityUnlockable != null){
                        $defaultActivity = $activityUnlockable->getDefaultActivity();
                        if($defaultActivity != null){
                            if(!in_array($defaultActivity->getId(),$unlockedActivityIds)){
                                array_push($unlockedActivityIds,$defaultActivity->getId());
                            }
                        }
                    }
                }
            }
        }

        $unlockables = $account->getAccountUnlockables();
        foreach ($unlockables as $unlockable){
            $activityUnlockable = $unlockable->getUnlockable()->getActivityUnlockable();
            if($activityUnlockable != null){
                $defaultActivity = $activityUnlockable->getDefaultActivity();
                if($defaultActivity != null){
                    if(!in_array($defaultActivity->getId(),$unlockedActivityIds)){
                        array_push($unlockedActivityIds,$defaultActivity->getId());
                    }
                }
            }
        }
        return $unlockedActivityIds;
    }

    public function checkMissionAuthorization($user_id,$mission_id,$journey_id,$adventure_id,$recursive = true)

    {
        if(!$this->hasAccessJourney($journey_id, $user_id))
            return ["mission_authoriztion" => false];

        $progress_unlocked = $this->checkProgressUnlocked($journey_id,$user_id);

        if($progress_unlocked)
            return ["mission_authoriztion" => true];

        $user = $this->accountRepository->getUserById($user_id);
        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));

        $task = $mission->getTask();
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
        if($adventure == null)
            throw new BadRequestException(trans('locale.adventure_not_exist'));

        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        if($journey->getStatus() != 'published')
            throw new CustomException(trans('locale.journey_not_published'), 277);

        return ["mission_authoriztion" => !$this->isLockedTask($task,$journey,$adventure,$user_id)];
    }

    public function checkActivityB2Authority($user_id, $activity_id, $is_default = true){
        $user = $this->accountRepository->getUserById($user_id);
        if ($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        $account = $user->getAccount();
        $accountType = $account->getAccountType()->getName();
        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));

        $authority = false;
        if($activity->isB2B()){
            if($accountType == 'School')
                $authority = true;
        }
        if($activity->isB2C()){
            if($accountType == 'Family' || $accountType == 'Individual')
                $authority = true;
        }
        return $authority;
    }

    public function checkMissionAuthorizationActivity($user_id,$mission_id,$activity_id,$is_default= true)
    {
        $userActivitiesIds = $this->getUserDefaultActivityIds($user_id);
        $hasAccess = false;
        foreach ($userActivitiesIds as $userActivitiesId){
            if($activity_id == $userActivitiesId)
                $hasAccess = true;
        }
        if(!$hasAccess)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $progress_unlocked = $this->checkProgressUnlockedActivity($activity_id,$user_id,$is_default);
        if($progress_unlocked)
            return ["mission_authoriztion" => true];

        $mission = $this->journeyRepository->getMissionbyId($mission_id);
        if($mission == null)
            throw new BadRequestException(trans('locale.mission_not_exist'));
        $task = $mission->getTask();

        return ["mission_authoriztion"=>!$this->isLockedTaskActivity($task,$activity_id,$user_id,$is_default)];        
    }

    public function checkQuizAuthorization($user_id,$quiz_id,$journey_id,$adventure_id)
    {
        $progress_unlocked = $this->checkProgressUnlocked($journey_id,$user_id);
        if($progress_unlocked)
            return ["quiz_authoriztion"=>true];

        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));
        $task = $quiz->getTask();
        $adventure = $this->journeyRepository->getAdventureById($adventure_id);
            if($adventure == null)
                throw new BadRequestException(trans('locale.adventure_not_exist'));
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));
        return ["quiz_authoriztion"=>!$this->isLockedTask($task,$journey,$adventure,$user_id)];
    }

    public function checkQuizAuthorizationActivity($user_id,$quiz_id,$activity_id,$is_default= true)
    {
        $authority = $this->checkActivityB2Authority($user_id, $activity_id, $is_default);
        if(!$authority)
            throw new UnauthorizedException(trans('locale.not_have_access_activity'));

        $progress_unlocked = $this->checkProgressUnlockedActivity($activity_id,$user_id,$is_default);
        if($progress_unlocked)
            return ["quiz_authoriztion"=>true];

        $quiz = $this->journeyRepository->getquiz($quiz_id);
        if($quiz == null)
            throw new BadRequestException(trans('locale.quiz_not_exist'));
        $task = $quiz->getTask();

        return ["quiz_authoriztion"=>!$this->isLockedTaskActivity($task,$activity_id,$user_id,$is_default)];
    }

    public function getJourneyAdventures($user_id,$journey_id){
        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.no_journey_with_id'));

        $adventures = $journey->getAdventures();
        $adventuresData = array();
        foreach ($adventures as $adventure){
            array_push($adventuresData,['id'=>$adventure->getId(),'order'=>$adventure->getOrder()]);
        }
        if(sizeof($adventuresData)>0){
            usort($adventuresData, function ($item1, $item2) {
                if ($item1['order'] == $item2['order']) return 0;
                return $item1['order'] < $item2['order'] ? -1 : 1;
            });
        }
        return $adventuresData;
    }

    public function checkOpenedModelAnswer($user, $journey_id){
        $returned = false;
        $isUser = false;
        $journey = $this->journeyRepository->getJourneyById($journey_id);
        if($journey == null)
            throw new BadRequestException(trans('locale.journey_not_exist'));

        foreach ($user->getRoles() as $role) {
            if($role->getName() == 'teacher' || $role->getName() == 'school_admin' || $role->getName() == 'parent')
                $returned = true;
            if($role->getName() == 'user' || $role->getName() == 'invited_Individual' )
                $isUser = true;
        }
        $subscriptions = $this->accountRepository->getActiveSubscriptions($user->getAccount()->getId());
        $subscription = $subscriptions->last();
        $invitation_subscription = $subscription->getInvitationSubscription()->first();
        if ($invitation_subscription != null){
            $journeys = $invitation_subscription->getInvitation()->getJourneys();
            foreach ($journeys as $invJourney){
                if($invJourney->getJourney()->getId() == $journey_id){
                    if($isUser){
                        if($invJourney->getProgressLock() == 0){
                            $returned = true;
                        }
                    }
                    else{
                        $returned = true;
                    }
                }
            }
        }
        return $returned;
    }



}
