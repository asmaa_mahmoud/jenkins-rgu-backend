<?php
/**
 * Created by PhpStorm.
 * User: Hesham Eldeeb
 * Date: 0015, February 15, 2017
 * Time: 7:17 PM
 */

namespace App\ApplicationLayer\UserJourneys;


use App\DomainModelLayer\UserJourneys\IUserJourneyRepository;
use App\Helpers\Mapper;

class UserJourneyService implements IUserJourneyService
{
    private $userJourneyRepository;

    public function __construct(IUserJourneyRepository $userJourneyRepository)
    {
        $this->userJourneyRepository = $userJourneyRepository;
    }

    public function GetUserJourneys($userId){
        return Mapper::MapEntityCollection(UserJourneyDto::class,$this->userJourneyRepository->GetUserEnrolledJourneys($userId));
    }
}