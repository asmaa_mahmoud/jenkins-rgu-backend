<?php

namespace App\ApplicationLayer\Journeys;

use App\ApplicationLayer\Journeys\Interfaces\IJourneyMainService;
use App\ApplicationLayer\Schools\StudentService;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Schools\Repositories\ISchoolMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;

class JourneyMainService implements IJourneyMainService
{
	private $journeyRepository;
    private $accountRepository;
    private $schoolRepository;
    private $professionalRepository;


    public function __construct(IJourneyMainRepository $journeyRepository,IAccountMainRepository $accountRepository,ISchoolMainRepository $schoolRepository,IProfessionalMainRepository $professionalRepository)
    {
        $this->journeyRepository = $journeyRepository;
        $this->accountRepository = $accountRepository;
        $this->schoolRepository = $schoolRepository;
        $this->professionalRepository =$professionalRepository;

    }

    public function getJourneyData($id,$user_id)
    {
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->getJourneyData($id,$user_id);
    }

    public function getJourneysSummery()
    {
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->getJourneysSummery();
    }

    public function GetAllJourneys()
    {
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->GetAllJourneys();
    }

    public function getMissionbyId($id,$user_id = null,$course_id =null)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getMissionbyId($id,$user_id,$course_id);
    }

    public function loadMissionSavedCode($mission_id,$user_id,$journey_id,$adventure_id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->loadMissionSavedCode($mission_id,$user_id,$journey_id,$adventure_id);
    }

    public function getMissionCategories($missionId)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getMissionCategories($missionId);
    }

    public function updateMissionSavedCode($mission_id,$user_id,$name,$xml,$journey_id,$adventure_id,$timeTaken)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateMissionSavedCode($mission_id,$user_id,$name,$xml,$journey_id,$adventure_id,$timeTaken);
    }

    public function deleteMissionSavedCode($mission_id,$user_id,$name,$journey_id,$adventure_id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->deleteMissionSavedCode($mission_id,$user_id,$name,$journey_id,$adventure_id);
    }

    public function updateMissionProgress($mission_id,$user_id,$journey_id,$adventure_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode= null)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateMissionProgress($mission_id,$user_id,$journey_id,$adventure_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode);
    }

    public function updateHTMLMissionProgress($mission_id,$user_id,$journey_id,$adventure_id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateHTMLMissionProgress($mission_id,$user_id,$journey_id,$adventure_id);
    }

    public function getTutorials($id)
    {
        $quizService =new QuizService($this->journeyRepository,$this->accountRepository);
        return $quizService->getTutorials($id);
    }

    public function GetQuiz($id,$activity_id,$round_id,$type,$user)
    {
        $quizService =new QuizService($this->journeyRepository,$this->accountRepository,$this->professionalRepository);
        return $quizService->GetQuiz($id,$activity_id,$round_id,$type,$user);
    }

    public function submitQuiz($data, $id, $user_id,$journey_id,$adventure_id)
    {
        $quizService =new QuizService($this->journeyRepository,$this->accountRepository);
        return $quizService->submitQuiz($data, $id, $user_id,$journey_id,$adventure_id);
    }

    public function getAllCategories()
    {
        $JourneyCategoryService = new JourneyCategoryService($this->journeyRepository);
        return $JourneyCategoryService->getAllCategories();
    }

    public function updateLastMission($mission_id,$user_id,$journey_id,$adventure_id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateLastMission($mission_id,$user_id,$journey_id,$adventure_id);
    }

    public function getNextMission($mission_id,$journey_id,$adventure_id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getNextMission($mission_id,$journey_id,$adventure_id);
    }
    public function getNextMissionHtml($mission_id,$journey_id,$adventure_id){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getNextMissionHtml($mission_id,$journey_id,$adventure_id);
    }

    public function getNextQuiz($quiz_id,$journey_id,$adventure_id)
    {
        $quizService =new QuizService($this->journeyRepository,$this->accountRepository);
        return $quizService->getNextQuiz($quiz_id,$journey_id,$adventure_id);
    }

    public function checkMissionAuthorization($user_id,$mission_id,$journey_id,$adventure_id,$recursive = true)
    {
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->checkMissionAuthorization($user_id,$mission_id,$journey_id,$adventure_id,$recursive);
    }

    public function checkQuizAuthorization($user_id,$quiz_id,$journey_id,$adventure_id)
    {
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->checkQuizAuthorization($user_id,$quiz_id,$journey_id,$adventure_id);
    }

    public function getMissionMcqQuestions($id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getMissionMcqQuestions($id);
    }

    public function getMissionTextQuestions($id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getMissionTextQuestions($id);
    }

    public function submitMcqMission($data,$selected_ids,$use_model, $id, $user_id,$journey_id,$adventure_id,$noOfBlocks = null,$timeTaken = null){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->submitMcqMission($data,$selected_ids,$use_model, $id, $user_id,$journey_id,$adventure_id,$noOfBlocks,$timeTaken);
    }

    public function GetJourneyById($id)
    {
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->GetJourneyById($id);
    }
    public function getJourneyAdventures($user_id,$journey_id){
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService-> getJourneyAdventures($user_id,$journey_id);
    }

    public function getHtmlMissionbyId($user_id,$html_mission_id){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getHtmlMissionbyId($user_id,$html_mission_id);
    }

    public function getFreeMissions()
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getFreeMissions();
    }

    public function getFreeMissionById($mission_id)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getFreeMissionById($mission_id);
    }

    public function sendCertificateEmail($email,$pdf_file,$level)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->sendCertificateEmail($email,$pdf_file,$level);
    }

    public function uploadCertificate($image_file)
    {
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->uploadCertificate($image_file);
    }

    public function getActivityMissionbyId($id,$user_id = null,$is_default =true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getActivityMissionbyId($id,$user_id,$is_default);
    }

    public function getCampActivityMissionbyId($id, $camp_id,$user_id = null, $is_default =true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getCampActivityMissionbyId($id, $camp_id, $user_id, $is_default);
    }

    public function getCampActivityMissionHtmlbyId($id, $camp_id,$user_id = null, $is_default =true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getCampActivityMissionbyHtmlId($id, $camp_id, $user_id, $is_default);
    }

    public function updateActivityMissionProgress($mission_id,$user_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateActivityMissionProgress($mission_id,$user_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$is_default);
    }

    public function updateActivityCampMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$noOfBlocks = null,$timeTaken = null,$xmlCode = null,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateActivityCampMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$noOfBlocks,$timeTaken,$xmlCode,$is_default);
    }

    public function updateActivityHTMLMissionProgress($mission_id,$user_id,$activity_id,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateActivityHTMLMissionProgress($mission_id,$user_id,$activity_id,$is_default);
    }

    public function updateActivityCampHTMLMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateActivityCampHTMLMissionProgress($mission_id,$user_id,$camp_id,$activity_id,$success_flag,$is_default);
    }

    public function submitActivityMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default = true, $is_mobile=true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->submitActivityMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default, $is_mobile);
    }

    public function submitActivityCampMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default = true){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->submitActivityCampMcqMission($data,$selected_ids,$use_model, $id, $user_id,$activity_id,$noOfBlocks,$timeTaken,$is_default);
    }

    public function submitActivityQuiz($data, $id, $user_id,$activity_id,$is_default = true){
        $quizService = new QuizService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $quizService->submitActivityQuiz($data, $id, $user_id,$activity_id,$is_default);
    }

    public function submitActivityCampQuiz($data, $id, $user_id, $camp_id, $activity_id, $is_default = true){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->submitActivityCampQuiz($data, $id, $user_id, $camp_id, $activity_id, $is_default);
    }

    public function checkProgressUnlockedActivity($activity_id,$user_id,$is_default = true){
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->checkProgressUnlockedActivity($activity_id,$user_id,$is_default);
    }

    public function checkMissionAuthorizationActivity($user_id,$mission_id,$activity_id,$is_default= true){
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->checkMissionAuthorizationActivity($user_id,$mission_id,$activity_id,$is_default);
    }

    public function checkMissionAuthorizationActivityCamp($user_id,$mission_id,$activity_id,$is_default= true){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->checkMissionAuthorizationActivityCamp($user_id,$mission_id,$activity_id,$is_default);
    }

    public function checkQuizAuthorizationActivity($user_id,$quiz_id,$activity_id,$is_default= true){
        $journeyService = new JourneyService($this->journeyRepository,$this->accountRepository);
        return $journeyService->checkQuizAuthorizationActivity($user_id,$quiz_id,$activity_id,$is_default);
    }

    public function checkQuizAuthorizationActivityCamp($user_id,$quiz_id,$activity_id,$is_default= true){
        $studentService = new StudentService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $studentService->checkQuizAuthorizationActivityCamp($user_id,$quiz_id,$activity_id,$is_default);
    }

    public function getNextTaskActivity($mission_id,$activity_id,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getNextTaskActivity($mission_id,$activity_id,$is_default);
    }

    public function getNextTaskActivityCamp($mission_id,$camp_id,$activity_id,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getNextTaskActivityCamp($mission_id,$camp_id,$activity_id,$is_default);
    }

    public function getNextTaskHtmlActivityCamp($mission_id,$camp_id,$activity_id,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getNextTaskHtmlActivityCamp($mission_id,$camp_id,$activity_id,$is_default);
    }

    public function getAllActivitiesForB2C($user_id,$start_from, $limit, $search, $is_mobile = true){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getAllActivitiesForB2C($user_id,$start_from, $limit, $search,$is_mobile);
    }

    public function getAllMinecraftActivities($user_id,$start_from, $limit, $search, $is_mobile = true){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getAllMinecraftActivities($user_id,$start_from, $limit, $search, $is_mobile);
    }
    public function getAllActivities($user_id, $start_from, $limit, $search){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getAllActivities($user_id, $start_from, $limit, $search);
    }

    public function getUserDefaultActivityIds($user_id, $is_mobile = true){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getUserDefaultActivityIds($user_id, $is_mobile);
    }

    public function getActivity($user_id,$activity_id,$is_default = true, $is_mobile = true){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getActivity($user_id,$activity_id,$is_default, $is_mobile);
    }

    public function getActivityTasks($user_id,$activity_id,$is_default = true, $is_mobile = true){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getActivityTasks($user_id,$activity_id,$is_default, $is_mobile);
    }

    public function countActivitiesForB2C($user_id,$search){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->countActivitiesForB2C($user_id,$search);
    }

    public function countAllMinecraftActivities($user_id,$search){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->countAllMinecraftActivities($user_id,$search);
    }

    public function loadMissionSavedCodeActivity($mission_id,$user_id,$activity_id,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->loadMissionSavedCodeActivity($mission_id,$user_id,$activity_id,$is_default);
    }

    public function updateMissionSavedCodeActivity($mission_id,$user_id,$name,$xml,$activity_id,$timeTaken,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->updateMissionSavedCodeActivity($mission_id,$user_id,$name,$xml,$activity_id,$timeTaken,$is_default);
    }

    public function deleteMissionSavedCodeActivity($mission_id,$user_id,$name,$activity_id,$is_default = true){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->deleteMissionSavedCodeActivity($mission_id,$user_id,$name,$activity_id,$is_default);
    }

    public function getAllActivitiesForB2CNonUser($start_from,$limit,$search){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getAllActivitiesForB2CNonUser($start_from,$limit,$search);
    }

    public function getFreeActivitiesAndJourneys(){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->getFreeActivitiesAndJourneys();
    }

    public function getFreeRoboClubMissions(){
        $missionService = new MissionService($this->journeyRepository,$this->accountRepository,$this->schoolRepository);
        return $missionService->getFreeRoboClubMissions();
    }

    public function unlockActivityWithCoins($user_id, $activity_id, $coins, $is_default = true, $is_mobile = true){
        $activityService = new ActivityService($this->accountRepository,$this->schoolRepository,$this->journeyRepository);
        return $activityService->unlockActivityWithCoins($user_id, $activity_id, $coins, $is_default, $is_mobile);
    }

    public function getConfidence(){
        $quizService =new QuizService($this->journeyRepository,$this->accountRepository);
        return $quizService->getConfidence();
    }
}