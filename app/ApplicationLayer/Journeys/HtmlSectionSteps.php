<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\Entity;

class HtmlSectionSteps extends Entity
{

    public function getSteps()
    {
        return $this->steps;
    }


    public function getSections()
    {
        return $this->sections;
    }

  

}
