<?php

namespace App\ApplicationLayer\Journeys;

use Analogue\ORM\Entity;
use LaravelLocalization;
use App\DomainModelLayer\Journeys\Question;

class QuestionTask extends Entity
{

	public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }

    // public function getQuestion()
    // {
    //     return $this->question;
    // }


    // public function getStep()
    // {
    //     return $this->step;
    // }


}
