<?php

namespace App\DomainModelLayer\Journeys;

use Analogue\ORM\EntityMap;


class HtmlSectionStepsMap extends EntityMap {

    protected $table = 'html_section_step';
    public $timestamps = true;
    public $softDeletes = true;
    protected $deletedAtColumn = "html_section_step.deleted_at";


    public function steps(HtmlSectionSteps $htmlSectionSteps)
    {
        return $this->belongsTo($htmlSectionSteps, MissionHtmlSteps::class , 'mission_html_step_id', 'id');
    }

    public function sections(HtmlSectionSteps $htmlSectionSteps)
    {
        return $this->belongsTo($htmlSectionSteps, MissionHtmlSections::class , 'html_section_id', 'id')->orderBy('order','ASC');
    }


}