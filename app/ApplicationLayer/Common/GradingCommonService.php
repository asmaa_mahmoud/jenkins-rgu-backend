<?php

namespace App\ApplicationLayer\Common;

use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\CampusUserStatus;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Support\Facades\DB;

class GradingCommonService
{
    private $accountRepository;
    private $professionalRepository;

    const HTML_MISSION = 'html_mission';
    const QUIZ_MISSION = 'quiz_mission';
    const EDITOR_MISSION = 'editor_mission';
    const BLOCKLY_MISSION = 'blockly_mission';
    const BLOCKLY_MCQ_MISSION = 'blockly_mcq_mission';
    const PYTHON_MISSION = 'python_mission';
    const EVALUATED_MISSION = 'evaluated_mission';
    const HANDOUT_MISSION = 'handout_mission';

    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
    }

    // TODO::Remove updateScore from LessonCommonService after testing this

    public function updateScore(CampusActivityProgress $campusActivityProgress, $round,$gmt_difference, $taskType, $data = [], $updateProgressEvaluation = true){
        $user = $campusActivityProgress->getUser();
        $campus = $campusActivityProgress->getCampus();
        $fullMarkNotSolvedTask = $this->getTaskFullMarkInCampusRound($user, $campusActivityProgress, $round);
        $fullMark=$fullMarkNotSolvedTask;
        $fullMark=$fullMark===null?0:$fullMark;

        $gainedScore = $this->calculateGainedScore($campusActivityProgress, $fullMark, $taskType, $data);
        
        $gainedXps=$gainedScore['gainedXps'];
        $currentXps=$gainedScore['currentXps'];

        $oldEvaluation = $campusActivityProgress->getEvaluation();
        $netGainedScore = $gainedXps - $oldEvaluation;
        

        if($taskType == self::EVALUATED_MISSION){
            $updateProgressScore = false;
            $applyGainedScore = true;
        }else{
            $updateProgressScore = true;
            if(($netGainedScore > 0 && $fullMark > 0)||$fullMark==0 )
                $applyGainedScore = true;
            else
                $applyGainedScore = false;
        }

        if($applyGainedScore) {
            $campusActivityProgress = $this->updateCampusActivityProgress($campusActivityProgress, $gainedXps, $updateProgressEvaluation, $updateProgressScore,$fullMark);
            $campusUserStatus=$this->updateUserXpsAndRank($user, $campus, $gmt_difference,$netGainedScore);
        }else{
            $campusUserStatus=$this->professionalRepository->getCampusUserStatus($campus->id,$user->id);

        }

        $scoreData=$this->getUserScoreInCampus($campusUserStatus,$gmt_difference);

        return ["campusUserStatus"=>$campusUserStatus,"campusActivityProgress"=>$campusActivityProgress
            ,"fullMark"=>$fullMarkNotSolvedTask,"scoreData"=>$scoreData, "gainedXps"=>$gainedXps,"currentXps"=>$currentXps];
    }

    public function getTaskFullMarkInCampusRound(User $user, CampusActivityProgress $campusActivityProgress, CampusRound $round){
        $task = $campusActivityProgress->getTask();
        $campus = $campusActivityProgress->getCampus();
        $activity = $campusActivityProgress->getDefaultActivity();

        $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
        if($activityTask->booster_type=='1'){//booster_mission
            $mainActivityTask = $this->professionalRepository->getMainTaskByBoosterTask($task->getId(),$activity->getId());
            $mainTask=$mainActivityTask->getTask();
            $mainTaskProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($mainTask, $user->getId(), $campus->getId(), $activity->getId(),true);
            $mainTaskFullMark = $mainActivityTask->getXps();

            $mainTaskGainedScore=$mainTaskProgress->getEvaluation();
            if($mainTaskGainedScore===null)//This should be discussed if we would have booster for miniproject
                throw new BadRequestException(trans('locale.you_have_not_completed_main_mission_yet'));

            $boosterTaskFullMark=$mainTaskFullMark-$mainTaskGainedScore;
            return $boosterTaskFullMark;
        }else{
            $mainTaskFullMark = $activityTask->getXps();
            //Main Mission(has booster'2' or no booster'1')
            $boosterActivityTask = $this->professionalRepository->getBoosterTaskByMainTask($task->getId(),$activity->getId());
            if($boosterActivityTask!==null){
                //solved booster
                $boosterTask=$boosterActivityTask->getTask();
                $boosterTaskProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($boosterTask, $user->getId(), $campus->getId(), $activity->getId(),true);
                if($boosterTaskProgress!==null){
                    $boosterTaskEvaluation=$boosterTaskProgress->getEvaluation()!==null?$boosterTaskProgress->getEvaluation():0;
                    return $mainTaskFullMark-$boosterTaskEvaluation;
                }
            }
            //didn't have  booster or did't solve it
            return $mainTaskFullMark;
        }

    }

    private function calculateGainedScore(CampusActivityProgress $campusActivityProgress, $fullMark, $taskType, $data = []){
        switch ($taskType){
            case self::HTML_MISSION:
                return $this->calculateHTMLMissionScore($campusActivityProgress, $fullMark);

            case self::HANDOUT_MISSION:
                return $this->calculateHTMLMissionScore($campusActivityProgress, $fullMark);

            case self::QUIZ_MISSION:
                return $this->calculateQuizMissionScore($campusActivityProgress, $fullMark, $data);

            case self::EDITOR_MISSION:
                return $this->calculateEditorMissionScore($campusActivityProgress, $fullMark, $data);

            case self::BLOCKLY_MISSION:
                return $this->calculateBlocklyMissionScore($campusActivityProgress, $fullMark);

            case self::BLOCKLY_MCQ_MISSION:
                return $this->calculateBlocklyMCQMissionScore($campusActivityProgress, $fullMark);

            case self::PYTHON_MISSION:
                return $this->calculatePythonMissionScore($campusActivityProgress, $fullMark);

            case self::EVALUATED_MISSION:
                return $this->calculateEvaluatedMissionScore($campusActivityProgress, $fullMark, $data);

        }
    }

    private function calculateHTMLMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->first_success){
            return ['gainedXps'=>$fullMark,'currentXps'=>$fullMark];
        }

        return ['gainedXps'=>0,'currentXps'=>0];
    }
    private function calculateQuizMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark, $data){
        // first trial time to pass quiz => get full mark
        if(($campusActivityProgress->getNoTrials() == 1 || $campusActivityProgress->getFirstSuccess() == 1) && $data['passed']) {
            return ['gainedXps'=>$fullMark,'currentXps'=>$fullMark];
        }
        // first time to pass quiz but from other trials 
        elseif($campusActivityProgress->getFirstSuccess() == null && $data['passed']) {
            $trialsWeight = $this->professionalRepository->getScoringConstant('missionTrials_constant')->getValue();
            $trials = $campusActivityProgress->getNoTrials() - 1;
            $gainedXps = round($fullMark - $trials*$trialsWeight*$fullMark); // each wrong trial reduce max gained by trialsWeight
            return ['gainedXps'=>$gainedXps,'currentXps'=>$gainedXps];
        }
        // passed and solve quiz again
        elseif($campusActivityProgress->getFirstSuccess() > 1  && $data['passed']) {
            $trialsWeight = $this->professionalRepository->getScoringConstant('missionTrials_constant')->getValue();
            $trials = $campusActivityProgress->getFirstSuccess() - 1;
            $gainedXps = round($fullMark - $trials*$trialsWeight*$fullMark); // each wrong trial reduce max gained by trialsWeight
            return ['gainedXps'=>$gainedXps,'currentXps'=>$gainedXps];
        }
        else {
            return ['gainedXps'=>0,'currentXps'=>0];
        }

    }
    // still the same old way
    private function calculateEditorMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark, $data){
        if(!isset($data))
            throw new BadRequestException("Data missing for calculating editor mission score");

        if($campusActivityProgress->first_success){
            $correctAnsPercentage = $data['correctAnsCount'] / $data['questionsCount'];
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = 0;
            if($trials <= 3)
                $trialsWeight = $this->calculateTrialsWeight('trials_constant', $trials);
            //dd($correctAnsPercentage , $trialsWeight ,$fullMark,$campusActivityProgress->evaluation);
            $currentScore= round($correctAnsPercentage * $trialsWeight * $fullMark);
            if($currentScore <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentScore,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentScore,"gainedXps"=>$currentScore];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }
    // still the same old way
    private function calculateBlocklyMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->first_success){
            $usedBlocksCount = $campusActivityProgress->getNoBlocks();
            $mission = $campusActivityProgress->getTask()->getMissions()->first();
            $minBlocksCount = $mission->getModelAnswerNumberOfBlocks();
            $maxBlocksCount = $this->calculateMaxBlocksCount($minBlocksCount);

            $blocksCountWeight = $this->calculateBlocksCountWeight($usedBlocksCount, $maxBlocksCount, $minBlocksCount);
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = $this->calculateTrialsWeight('blocklyTrials_constant', $trials);

            $currentXps= round($blocksCountWeight * $trialsWeight * $fullMark);
            if($currentXps <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentXps,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentXps,"gainedXps"=>$currentXps];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }
    // still the same old way
    private function calculateBlocklyMCQMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->first_success){
            $usedBlocksCount = $campusActivityProgress->getNoBlocks();
            $mission = $campusActivityProgress->getTask()->getMissions()->first();
            $minBlocksCount = $mission->getModelAnswerNumberOfBlocks();
            $maxBlocksCount = $this->calculateMaxBlocksCount($minBlocksCount);

            $blocksCountWeight = $this->calculateBlocksCountWeight($usedBlocksCount, $maxBlocksCount, $minBlocksCount);
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = 0;
            if($trials <= 3)
                $trialsWeight = $this->calculateTrialsWeight('blocklyTrials_constant', $trials);

            $currentXps= round($blocksCountWeight * $trialsWeight * $fullMark);

            if($currentXps <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentXps,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentXps,"gainedXps"=>$currentXps];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }

    private function calculatePythonMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        $trialsWeight = $this->professionalRepository->getScoringConstant('missionTrials_constant')->getValue();
        $trials = $campusActivityProgress->first_success - 1;
        $gainedXps = round($fullMark - $trials*$trialsWeight*$fullMark); // each wrong trial reduce max gained by trialsWeight
        
        return ['gainedXps'=>$gainedXps,'currentXps'=>$gainedXps];
    }

    // Required data for calculating mini project score is:
    // - Instructor evaluation ... data['evaluation']
    private function calculateEvaluatedMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark, $data){
        if(!isset($data))
            throw new BadRequestException("Data missing for calculating evaluated mission score");
        
        // if($campusActivityProgress->getFirstSuccess() && $campusActivityProgress->getIsEvaluated()){
            $evaluation = $data['evaluation'];
            if($evaluation >= 1){
                $evaluation /= 100;
            }
            $gained = round($evaluation * $fullMark);
            return ["currentXps"=>$gained,"gainedXps"=>$gained];
        // }else{
        //     return 0;
        // }

    }

    private function calculateQuizTimePercentage(Quiz $quiz,$learnerQuizTimeDuration){
        $questionTime=$this->professionalRepository->getScoringConstant('defaultQuizQuestion_time')->value;
        $quizTimeConstant=$this->professionalRepository->getScoringConstant('time_constant')->value;
        $quizQuestionCount=count($quiz->getQuestionTasks());
        $quizTime=$questionTime*$quizQuestionCount;

        $userTimeUnit=floor($learnerQuizTimeDuration/$quizTime);

        $quizTimePercentage=exp(-$quizTimeConstant*$userTimeUnit);
        return $quizTimePercentage;
    }

    private function calculateTrialsWeight($trialsConstName, $trials){
        $trialsConstant = $this->professionalRepository->getScoringConstant($trialsConstName)->value;
        return exp(-1 * $trialsConstant * ($trials-1));
    }

    private function calculateMaxBlocksCount($minBlocksCount = 1){
        return 2 * $minBlocksCount;
    }

    private function calculateBlocksCountWeight($usedBlocksCount, $maxBlocksCount, $minBlocksCount){
        $avgBlocksCount = ($minBlocksCount + $maxBlocksCount) / 2;

        if($usedBlocksCount > $maxBlocksCount){
            return $this->professionalRepository->getScoringConstant('usedBlocks_score_low')->value;
        }elseif ($usedBlocksCount >= $avgBlocksCount){
            return $this->professionalRepository->getScoringConstant('usedBlocks_score_medium')->value;
        }else{
            return $this->professionalRepository->getScoringConstant('usedBlocks_score_high')->value;
        }
    }

    private function updateCampusActivityProgress(CampusActivityProgress $campusActivityProgress, $gainedScore, $updateEvaluation = true, $updateScore = true,$fullMark){
        if($updateScore)
            $campusActivityProgress->setScore($gainedScore);

        if($updateEvaluation)
            $campusActivityProgress->setEvaluation($gainedScore);

        if((($gainedScore > 0 &&$fullMark>0) ||($fullMark==0))&& $campusActivityProgress->getFirstSuccess()==null )
            $campusActivityProgress->setFirstSuccess($campusActivityProgress->getNoTrials());

        return $campusActivityProgress;
    }

    private function updateUserXpsAndRank(User $user, Campus $campus,$gmt_difference, $gainedScore){
        $campusUserStatus = $this->updateCampusUserStatus($user->getId(), $campus->getId(),$gainedScore,0);
        $campusUserStatus = $this->CheckUserCrownAndStatus($user->getId(), $campus->getId(),$gmt_difference);
        $userXps = $campusUserStatus->getXps();
        $rankLevelId = $this->calculateUserRankLevel($userXps, $campus);
        $campusUserStatus->setRankLevelId($rankLevelId);
        return $campusUserStatus;
    }

    private function updateCampusUserStatus($user_id,$campus_id,$gainedScore,$streak_xp = 0, $latestStreakStartId = null){
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);
        
        if($campusUserStatus) {
            $campusUserStatus->xp += $gainedScore;
        }else{
            $campusUserStatus = new CampusUserStatus($user_id,$campus_id,0,1);
        }
        $campusUserStatus->streak_xp += $streak_xp;//TODO:remove this

        $latestStreakPeriod = $this->professionalRepository->getLatestStreakPeriod($user_id, $campus_id);
        $latestStreakPeriodLength = $latestStreakPeriod->count();
        if($latestStreakPeriodLength == 7){
            $campusUserStatus->streak_xp += 1;//add crown for 7 day streak
        }

        if(isset($latestStreakStartId)){
            //$campusUserStatus->latestStreakStartId = $latestStreakStartId;

            DB::table('campus_user_status')->where('id', $campusUserStatus->id)->update(['latestStreakStartId' => $latestStreakStartId,'streak_xp'=>$campusUserStatus->streak_xp]);//Weird BUG

        }

        return $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
    }

    private function getUserScoreInCampus(CampusUserStatus $campusUserStatus,$gmt_difference){
        $rankLevel = $this->professionalRepository->getRankLevelById($campusUserStatus->rank_level_id);
        $this->CheckStudentCrown($campusUserStatus->user_id,$campusUserStatus->campus_id,$gmt_difference);
        
        //Check Crown to make Reset and notify frontend
        $scoreData = [];
        $scoreData['campus_id'] = $campusUserStatus->campus_id;
        $scoreData['rank'] = $rankLevel->rank->getName();
        $scoreData['level'] = $rankLevel->level->getName();
        $scoreData['xp'] = $campusUserStatus->xp;
        $scoreData['crowns'] = $campusUserStatus->streak_xp;
        // $scoreData['gained_crown'] = $this->CheckStudentCrown($campusUserStatus->user_id,$campusUserStatus->campus_id,$gmt_difference);
        $scoreData['xps_to_next_level'] = $this->calculateNeededXpsForNextRankLevel($campusUserStatus);

        return $scoreData;

    }

    private function CheckUserCrownAndStatus($user_id,$campus_id,$gmt_difference){

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);
        $weekDays = $this->professionalRepository->getStreaksWeek($user_id,$campus_id,$gmt_difference); 
            
        $streaksCounter = 0;
        foreach($weekDays as $day=>$status){
            if($status == true)
                $streaksCounter++;
        }
        if($streaksCounter == 7)
            $campusUserStatus->streak_xp += 1;


        return $this->professionalRepository->storeCampusUserStatus($campusUserStatus);   
    }

    private function calculateUserRankLevel($xps, Campus $campus){
        $rankLevelXps = $this->calculateRankLevelXps($campus);

        return ceil($xps/$rankLevelXps) > 0 ? ceil($xps/$rankLevelXps) : 1;
    }

    private function CheckStudentCrown($user_id,$campus_id,$gmt_difference){
        $latestStreakPeriod = $this->professionalRepository->getLatestStreakPeriod($user_id, $campus_id);
            if($latestStreakPeriod->count() >= 7 ){
                //Reset Streaks
                    $user = $this->accountRepository->getUserById($user_id);
                    $this->professionalRepository->resetStudentStreaks($user,$campus_id,$gmt_difference);
                return true;
            }
            return false;
    }

    private function calculateNeededXpsForNextRankLevel(CampusUserStatus $campusUserStatus){
        $campus = $campusUserStatus->getCampus();
        $rankLevelXps = $this->calculateRankLevelXps($campus);
        $userRankLevel = $campusUserStatus->rank_level_id;
        $nextRankLevelXps = $userRankLevel * $rankLevelXps;
        $userXps = $campusUserStatus->getXps();

        return $nextRankLevelXps - $userXps;
    }

    private function calculateRankLevelXps(Campus $campus){
        $totalCampusXps = $campus->getXps();
        $rankLevelsCount = $this->professionalRepository->countRankLevels();

        return $totalCampusXps / $rankLevelsCount;
    }
}