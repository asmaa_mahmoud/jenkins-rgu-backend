<?php
/**
 * Created by PhpStorm.
 * User: Lenovo02
 * Date: 9/16/2018
 * Time: 1:35 PM
 */
namespace App\ApplicationLayer\Common;

use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularFilesDto;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularDependenciesDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\MissionStepDto;
use App\ApplicationLayer\Journeys\Dtos\StepContentDto;
use App\ApplicationLayer\Journeys\Dtos\MissionSectionDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragCellDto;
use App\ApplicationLayer\Journeys\Dtos\DragChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownsDto;
use App\ApplicationLayer\Journeys\Dtos\stepDropdownsChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepMcqQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\McqQuestionChoicesDto;
use App\ApplicationLayer\Journeys\Dtos\StepMatchQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\MatchKeysDto;
use App\ApplicationLayer\Journeys\Dtos\MatchValueDto;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\MissionHtml;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Journeys\Question;
use App\ApplicationLayer\Professional\Dtos\ProjectDto;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\ApplicationLayer\Common\LessonCommonService;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\DomainModelLayer\Professional\Campus;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;
use App\Helpers\Mapper;
use Carbon\Carbon;
use App\ApplicationLayer\Journeys\Dtos\MissionAngularAnswersDto;


class TaskCommonService
{

    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;
    private $lessonService;
    private $lessonCommonService;
    private $campusService;


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;
        $this->lessonCommonService = new LessonCommonService($this->accountRepository, $this->professionalRepository, $this->journeyRepository);


    }
    /***COMMON FUNCTIONS***/
    //TODO:move to common service
    public function isPassedSubmodule($user,$round,$submodule){
       if(!$user->isStudent())
            return true;
        return $this->professionalRepository->hasFinishedSubmoduleTasks($user,$round->getCampus_id(),$submodule);
        
    }

    private function isBoostersOpenedInModule($user,$campus,$activity){
        $openBoosters=true;
        foreach ($activity->getMainTasks() as $mainTask) {
            $userProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($mainTask,$user->id,$campus->id,$activity->id,true);
            if($userProgress!==null&&$userProgress->first_success==null){
                $openBoosters=false;
                break;
            }
        }
        return $openBoosters;
    }

    private function checkNextTaskInModule($order,$activity_id, $is_default,$showClasses,$hideBlocks,$task=null){
        $next_task = $this->journeyRepository->getTaskInActivityByOrder($order, $activity_id, $is_default);
       
        $next_activity_task = $this->journeyRepository->getActivityTaskOrder($next_task, $activity_id,$is_default);
       
        if ($next_task == null)
            return null;
            // throw new BadRequestException(trans('locale.next_task_not_exist'));

        if ($task != null)
            $lastTaskType = $next_activity_task->booster_type == 1 ? 'Booster':'Main_Mission';

        //Check ShowClasses and HideBlocks
        
        $task_type = $next_task->getTaskNameAndType()['task_type'];

        if($task_type == 'mission' && $hideBlocks){
            return $this->checkNextTaskInModule($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
            
        }
        if($task_type == 'html_mission' && !$showClasses){
            return $this->checkNextTaskInModule($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
        }
        if($task_type == 'mini_project' && !$showClasses){
            return $this->checkNextTaskInModule($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
        }
        

        $nextTaskIsBooster = $next_activity_task->booster_type == 1 ? true:false;

        //If Booster --Next--> Booster else Main Mission --Next--> Main Mission
        if($lastTaskType == 'Booster'){

            if($nextTaskIsBooster){
                return $next_task;
            }else{
                return $this->checkNextTaskInModule($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
            }
        }elseif($lastTaskType == 'Main_Mission'){

            if(!$nextTaskIsBooster){
                return $next_task;
            }else{
                return $this->checkNextTaskInModule($order+1,$activity_id, $is_default,$showClasses,$hideBlocks,$task);
            }
        }

        return null;
    }
    private function checkNextTaskInSubmodule($order,$activity_id,$sub_module_id, $is_default,$showClasses,$hideBlocks,$task=null){
        $next_submodule_task = $this->professionalRepository->getSubmoduleTaskOrderByOrder($order, $sub_module_id);
        
        if ($next_submodule_task == null)
            return null;

        $next_task=$next_submodule_task->getTask();
        $next_activity_task = $this->journeyRepository->getActivityTaskOrder($next_task, $activity_id,$is_default);
       
        if ($task != null)
            $lastTaskType = $next_activity_task->booster_type == 1 ? 'Booster':'Main_Mission';

        //Check ShowClasses and HideBlocks
        
        $task_type = $next_task->getTaskNameAndType()['task_type'];

        if($task_type == 'mission' && $hideBlocks){
            return $this->checkNextTaskInSubmodule($order+1,$activity_id, $sub_module_id,$is_default,$showClasses,$hideBlocks,$task);
            
        }
        if($task_type == 'html_mission' && !$showClasses){
            return $this->checkNextTaskInSubmodule($order+1,$activity_id,$sub_module_id, $is_default,$showClasses,$hideBlocks,$task);
        }
        if($task_type == 'mini_project' && !$showClasses){
            return $this->checkNextTaskInSubmodule($order+1,$activity_id,$sub_module_id, $is_default,$showClasses,$hideBlocks,$task);
        }
        

        $nextTaskIsBooster = $next_activity_task->booster_type == 1 ? true:false;

        //If Booster --Next--> Booster else Main Mission --Next--> Main Mission
        if($lastTaskType == 'Booster'){

            if($nextTaskIsBooster){
                return $next_task;
            }else{
                return $this->checkNextTaskInSubmodule($order+1,$activity_id,$sub_module_id, $is_default,$showClasses,$hideBlocks,$task);
            }
        }elseif($lastTaskType == 'Main_Mission'){

            if(!$nextTaskIsBooster){
                return $next_task;
            }else{
                return $this->checkNextTaskInSubmodule($order+1,$activity_id,$sub_module_id, $is_default,$showClasses,$hideBlocks,$task);
            }
        }

        return null;
    }

    private function getNextTaskInModule($task, $last_task, $order, $activity_id,$is_default, $showClasses, $hideBlocks){
        if($task->getId() != $last_task->getId()) {
            $order=intval($order) + 1;
            $lastTaskOrder = $this->journeyRepository->getActivityTaskOrder($last_task, $activity_id, $is_default)->getOrder();
            while(true) {
               
                $next_task = $this->checkNextTaskInModule($order, $activity_id, $is_default,$showClasses,$hideBlocks,$task);    
                if($next_task == null)
                    return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
                $next_task_info=$this->getTaskInfoDto($next_task,$showClasses);

                if($next_task_info==null){//mean this is html_mission and showClasses=false or any case like thiss
                    $order++;
                    if($order>$lastTaskOrder){
                        return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
                    }

                }else{
                    return $next_task_info;
                }
            
            }
        }
        else
        {
            //$nextClass = $this->getNextClass($round_id, $activity_id, $is_default);
            return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
        }
    }

    private function getTaskInfoDto($task,$showClasses=true){
        if ($task->getMissions()->first() != null) {
            $next_mission = $task->getMissions()->first();
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getName(), "next_task_type" => "mission"];
        } elseif (($showClasses && $task->getMissionsHtml()->first() != null  && $task->getMissionsHtml()->first()->getIsMiniProject()=='0')||
            ($task->getMissionsHtml()->first() != null  &&$task->getMissionsHtml()->first()->getIsMiniProject()=='1')){
            $next_mission = $task->getMissionsHtml()->first();
            
            $handoutMissionFlag = $next_mission->htmlMissionType() == 'handout';
            
            if ($next_mission->getIsMiniProject() == '0' && !$handoutMissionFlag)
                return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "html_mission"];
            elseif ($next_mission->getIsMiniProject() == '0' && $handoutMissionFlag)
                return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "handout"];
            else
                return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "mini_project"];
        } elseif ($task->getMissionCoding() != null) {
            $next_mission = $task->getMissionCoding();
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "coding_mission"];
        } elseif ($task->getMissionEditor() != null) {
            $next_mission = $task->getMissionEditor();
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "editor_mission"];
        } elseif ($task->getMissionAngular() != null) {
            $next_mission = $task->getMissionAngular();
            return ["next_task_id" => $next_mission->getId(), "next_task_name" => $next_mission->getTitle(), "next_task_type" => "angular_mission"];
        } elseif($task->getQuizzes()->first() !=null) {
            $next_quiz = $task->getQuizzes()->first();
            return ["next_task_id" => $next_quiz->getId(), "next_task_name" => $next_quiz->getTitle(), "next_task_type" => $next_quiz->getType()->getName()];
        }else{
            return null;
        }
    }
    private function getNextTaskInSubModule($task, $last_task, $order, $activity_id,$sub_module_id, $is_default, $showClasses, $hideBlocks){
        if($task->getId() != $last_task->getId()) {
            $order=intval($order) + 1;
            $submodule=$this->professionalRepository->getSubmoduleById($sub_module_id);
            $lastTaskOrder = $this->professionalRepository->getSubmoduleTaskOrder($last_task->getId(), $submodule->getId())->getOrder();
            while(true) {
                
                $next_task = $this->checkNextTaskInSubmodule($order, $activity_id,$sub_module_id, $is_default,$showClasses,$hideBlocks,$task);    //
                
                if($next_task == null)
                    return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
                $next_task_info=$this->getTaskInfoDto($next_task,$showClasses);

                if($next_task_info==null){//mean this is html_mission and showClasses=false or any case like thiss
                    $order++;
                    if($order>$lastTaskOrder){
                        //$nextClass = $this->getNextClass($round_id, $activity_id, $is_default);
                        return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null/*, 'nextLesson' => $nextClass*/];
                    }

                }else{
                    return $next_task_info;
                }
            
            }
        }
        else
        {

            return ["next_task_id" => null, "next_task_name" => null, "next_task_type" => null];
        }
    }
    private function checkUserDataForNextTask($activity_id , $sub_module_id,$task, $round_id,$user_id=null,$is_default=true){

        $round = $this->professionalRepository->getRoundById($round_id);
        if ($round == null)
            throw new BadRequestException(trans('locale.round_not_exist').$round_id);
        $showClasses=$round->showClasses();

        $campus=$round->getCampus();
        $campusHasSubmodules=$campus->hasSubModules();
        if($campusHasSubmodules){
            $submodule=$this->professionalRepository->getSubmoduleById($sub_module_id);
            if ($submodule == null)
                throw new BadRequestException(trans('locale.submodule_not_exist'));
        }
       

        if($is_default)
            $activity = $this->journeyRepository->getDefaultActivityById($activity_id);
        else
            $activity = $this->journeyRepository->getActivityById($activity_id);

        if($activity == null)
            throw new BadRequestException(trans('locale.activity_not_exist'));
       
        if($campusHasSubmodules)
            $order = $this->professionalRepository->getSubmoduleTaskOrder($task->getId(), $submodule->getId())->getOrder();
        else
            $order = $this->journeyRepository->getActivityTaskOrder($task, $activity_id, $is_default)->getOrder();

        $user = $this->accountRepository->getUserById($user_id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));
        $isBoostersOpened=true;
        if($user->isStudent()){
            if($campusHasSubmodules)
                $isBoostersOpened=$this->isPassedSubmodule($user,$round,$submodule);
            else
                $isBoostersOpened=$this->isBoostersOpenedInModule($user,$campus,$activity);
        }
        if($campusHasSubmodules)
            $last_task = $this->professionalRepository->getLastMainTaskInSubmodule($submodule);//don't include 
        else
            $last_task = $this->journeyRepository->getLastMainTaskInActivity($activity_id, $is_default);//don't include 
        
        //Get hideBlocks status only for learners and always showen for admin and teacher
        if($user->isStudent()){
            $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus->getId(),$user_id);
            if($campusUserStatus != null){
                $hide_blocks = $campusUserStatus->hide_blocks == 1 ? true :false;    
            }else{
                $hide_blocks = false;
            }
        }else{
            $hide_blocks = false;
        }
        
        $data['last_task']=$last_task;
        $data['order']=$order;
        $data['showClasses']=$showClasses;
        $data['hideBlocks']=$hide_blocks;
        $data['round']=$round;
        $data['user']=$user;

        return $data;

    }
    private function getNextSubmoduleInfo($user,$round,$activity_id,$sub_module_id,$showClasses=true){
        $campus_id=$round->getCampus_id();
        $submodule=$this->professionalRepository->getSubmoduleById($sub_module_id);
        $module=$this->professionalRepository->getCampusClassId($campus_id,$activity_id,true);
        $nextSubmoduleOrder=$submodule->getOrder()+1;
        $nextSubmodule=$this->professionalRepository->getSubmoduleInModuleByOrder($module,$nextSubmoduleOrder);
        if($nextSubmodule===null)
            return ["id"=>null,"name"=>null,"description"=>null];
        else{
            $firstTask=$this->professionalRepository->getSubmoduleTaskOrderByOrder(1,$nextSubmodule->getId())->getTask();
            $firstTaskDto=$this->getTaskInfoDto($firstTask,$showClasses);
            $canOpenSubmodule=$this->isPassedSubmodule($user,$round,$submodule);
            return ["id"=>$nextSubmodule->getId(),"name"=>$nextSubmodule->getName(),"description"=>$nextSubmodule->getDescription(),"first_task_info"=>$firstTaskDto,"canOpenSubmodule"=>$canOpenSubmodule];
        }

    }

    public function getNextTaskInfo($activity_id ,$sub_module_id, $task_id,$round_id, $user_id,$is_default=true){
        $task = $this->journeyRepository->getTaskById($task_id);
        if($task == null)
            throw new BadRequestException(trans("locale.task_not_exist"));

        $data=$this->checkUserDataForNextTask($activity_id ,$sub_module_id, $task, $round_id,$user_id,$is_default);
        $last_task=$data['last_task'];
        $order=$data['order'];
        $showClasses=$data['showClasses'];
        $hideBlocks=$data['hideBlocks'];
        $round=$data['round'];
        $user=$data['user'];
        /*
            *makes actual logic to get next task
        */
        if($round->getCampus()->hasSubModules()){
            
            $nextTaskInfo= $this->getNextTaskInSubModule($task,$last_task,$order,$activity_id,$sub_module_id,$is_default,$showClasses,$hideBlocks);
            if($nextTaskInfo["next_task_id"]===null)
                $nextSubmoduleInfo=$this->getNextSubmoduleInfo($user,$round,$activity_id,$sub_module_id,$showClasses);
            else
                $nextSubmoduleInfo=null;
            $nextTaskInfo["nextSubmoduleInfo"]=$nextSubmoduleInfo;
            return $nextTaskInfo;
        }
        else
            return $this->getNextTaskInModule($task,$last_task,$order,$activity_id,$is_default,$showClasses,$hideBlocks);
    }
}