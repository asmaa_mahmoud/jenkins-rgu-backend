<?php
/**
 * User: Hossam
 * Date: 28/7/2019
 * Time: 01:11 PM
 */

namespace App\ApplicationLayer\Common;



use App\ApplicationLayer\Accounts\Dtos\SupportEmailDto;
use App\ApplicationLayer\Professional\Dtos\CampusActivityTaskDto;
use App\ApplicationLayer\Professional\Dtos\CampusClassDto;
use App\ApplicationLayer\Professional\Dtos\CampusDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundDto;
use App\ApplicationLayer\Professional\Dtos\CampusRoundNamesDto;


use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Accounts\User;
use App\DomainModelLayer\Accounts\UserNotification;
use App\DomainModelLayer\Journeys\CampusUserStatus;
use App\DomainModelLayer\Journeys\Quiz;
use App\DomainModelLayer\Journeys\Repositories\IJourneyMainRepository;
use App\DomainModelLayer\Journeys\Task;
use App\DomainModelLayer\Journeys\DefaultActivity;
use App\DomainModelLayer\Journeys\MatchKey;
use App\DomainModelLayer\Journeys\Question;
use App\DomainModelLayer\Journeys\MatchValue;
use App\DomainModelLayer\Journeys\MatchQuestion;
use App\DomainModelLayer\Journeys\Choice;
use App\DomainModelLayer\Journeys\DragCell;
use App\DomainModelLayer\Journeys\DragChoice;
use App\DomainModelLayer\Professional\CampusClass;
use App\DomainModelLayer\Professional\LearningPath;
use App\DomainModelLayer\Accounts\UserModelAnswer;
use App\DomainModelLayer\Accounts\UserScore;
use App\DomainModelLayer\Professional\Campus;
use App\DomainModelLayer\Professional\CampusRound;
use App\DomainModelLayer\Professional\CampusActivityProgress;
use App\DomainModelLayer\Professional\CampusActivityQuestion;
use App\DomainModelLayer\Professional\DragQuestionAnswer;
use App\DomainModelLayer\Professional\DropdownQuestionAnswer;
use App\DomainModelLayer\Professional\MatchQuestionAnswer;
use App\DomainModelLayer\Professional\McqQuestionAnswer;
use App\DomainModelLayer\Professional\Material;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Professional\Room;
use App\DomainModelLayer\Professional\TaskCurrentStep;
use App\DomainModelLayer\Professional\TaskLastSeen;
use App\Framework\Exceptions\BadRequestException;
use App\Framework\Exceptions\UnauthorizedException;

use function GuzzleHttp\Psr7\str;
use Illuminate\Pagination\Paginator;

use App\ApplicationLayer\Journeys\Dtos\ChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\MissionCodingDto;
use App\ApplicationLayer\Journeys\Dtos\MissionDto;
use App\ApplicationLayer\Journeys\Dtos\MissionEditorDto;
use App\ApplicationLayer\Journeys\Dtos\MissionHtmlDto;
use App\ApplicationLayer\Journeys\Dtos\MissionScreensDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\DragCellDto;
use App\ApplicationLayer\Journeys\Dtos\DragChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepDropdownsDto;
use App\ApplicationLayer\Journeys\Dtos\stepDropdownsChoiceDto;
use App\ApplicationLayer\Journeys\Dtos\StepMcqQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\McqQuestionChoicesDto;
use App\ApplicationLayer\Journeys\Dtos\StepMatchQuestionDto;
use App\ApplicationLayer\Journeys\Dtos\MatchKeysDto;
use App\ApplicationLayer\Journeys\Dtos\MatchValueDto;
use App\Helpers\LTI;
use App\Helpers\Mapper;
use App\Notifications\StudentStuck;
use App\Notifications\Submission;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Carbon\Carbon;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use Illuminate\Support\Facades\Notification;
use Ramsey\Uuid\Uuid;
use App\ApplicationLayer\Professional\Dtos\RoomDto;
use IMSGlobal\LTI\ToolProvider\Outcome;
use IMSGlobal\LTI\ToolProvider\ResourceLink;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use IMSGlobal\LTI\ToolProvider;

use Illuminate\Database\QueryException;
use DB;



class LessonCommonService
{
    private $accountRepository;
    private $professionalRepository;
    private $journeyRepository;

    const HTML_MISSION = 'html_mission';
    const QUIZ_MISSION = 'quiz_mission';
    const EDITOR_MISSION = 'editor_mission';
    const BLOCKLY_MISSION = 'blockly_mission';
    const BLOCKLY_MCQ_MISSION = 'blockly_mcq_mission';
    const PYTHON_MISSION = 'python_mission';
    const EVALUATED_MISSION = 'evaluated_mission';


    public function __construct(IAccountMainRepository $accountRepository, IProfessionalMainRepository $professionalRepository, IJourneyMainRepository $journeyRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->professionalRepository = $professionalRepository;
        $this->journeyRepository = $journeyRepository;

    }

    public function getUserTaskProgressData(Task $task, User $user, Campus $campus, DefaultActivity $activity,Question $question, $is_default=true,$create=true){

        $score=0;
        $firstSuccess=null;

        $missionProgress = $this->professionalRepository->getUserRoundTaskProgressActivity($task, $user->id, $campus->getId(), $activity->id, $is_default);
        if($missionProgress==null ){

            $missionProgress = new CampusActivityProgress($task,$user,$campus,$score,1,1,$score,$firstSuccess);
            $missionProgress->setDefaultActivity($activity);
            
            $this->professionalRepository->storeCampusActivityProgress($missionProgress);
        }

        $campusActivityQuestion=$this->professionalRepository->getCampusActivityQuestion($missionProgress,$question);
        if($campusActivityQuestion==null && $create){
            $campusActivityQuestion=new CampusActivityQuestion($missionProgress,$question);
            $this->professionalRepository->storeCampusActivityQuestion($campusActivityQuestion);

        }
        $progressData["missionProgress"]=$missionProgress;
        $progressData["campusActivityQuestion"]=$campusActivityQuestion;
        return $progressData;
    }

    public function checkIfSectionExist($missionCurrentSection,$mission_id){

        $currentSection=$missionCurrentSection->getCurrentSection();
        if($currentSection > 0){
            $sectionId=$missionCurrentSection->getSectionId();
            $sectionExist=$this->professionalRepository->getMissionSection($mission_id,$sectionId)->first();
            if($sectionExist != null){
                $currentSection=$currentSection;
            }else{
                $missionSections=$this->professionalRepository->getMissionSectionsCount($mission_id,false)->toArray(); 
                if(!empty($missionSections)){
                    $newCurrentSection=$currentSection-1;
                    for($i=$newCurrentSection;$i>=0;$newCurrentSection--){
                        if($newCurrentSection == 0)
                        {
                            $currentSection=$newCurrentSection;
                            break;
                        }else{
                            $missionOrder=$this->professionalRepository->getSectionWithOrder($mission_id,$newCurrentSection);

                            if($missionOrder != null)
                            {
                                $currentSection= $missionOrder->getOrder();
                                $sectionId= $missionOrder->getId(); 

                                $missionCurrentSection->setCurrentSection($currentSection);
                                $missionCurrentSection->setSectionId($sectionId);
                                $this->professionalRepository->storeUserTaskCurrentStep($missionCurrentSection);

                                break;
                            }
                        }
                    } 
                }else{
                    $currentSection=0;
                }  
            }
        }else{
        $currentSection=0;
        }

        return $currentSection;
    }


    public function getLearnerCampusProgress($learner_id,$round){
        $learner = $this->accountRepository->getUserById($learner_id);

        if($learner == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

            
        $solvedTasksCount = $this->professionalRepository->getSolvedTasksInRound($round,$learner);
        // $allTasksCount = $this->professionalRepository->getAllTasksInRound($round->campus_id);    
        $allTasksCount = $this->professionalRepository->getAvailaibleTasksInCampus($learner,$round,true);    
        $progress = round(($solvedTasksCount/$allTasksCount)*100);

        return ['all'=>$allTasksCount,'solved'=>$solvedTasksCount,'progress'=>$progress];

    }

    public function getLearnerClassProgress($learner_id,$campus_id,$activity_id,$is_default){
        $learner = $this->accountRepository->getUserById($learner_id);
        $campus = $this->professionalRepository->getCampusById($campus_id);

        //Get Campus Round
        $campusRoundId = $this->professionalRepository->getLearnerCampusUser($learner_id,$campus_id)->campus_round_id;
        $campusRound = $this->professionalRepository->getCampusRoundById($campusRoundId);

        if($learner == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if ($campus == null)
            throw new BadRequestException(trans('locale.campus_not_exist'));   
        
        $solvedClassesNum = $this->professionalRepository->getStudentSolvedTasksInClass($learner, $campus_id,$activity_id,$campusRound,$is_default, true);
        $campusClassesNum = $this->professionalRepository->getAllTasksInClass($campus_id,$activity_id,$learner,$campusRound,$is_default, true); 

        //Check if Module has an intro video
        $campusClass = $this->professionalRepository->getCampusClass($campus_id, $activity_id, $is_default);
        if($campusClass->lessonType == 2){
            $welcomeModule = $this->professionalRepository->getIntroDetails($campusRoundId);
            if($welcomeModule && $welcomeModule->introductory_video_url !=null){
                $solvedClassesNum+=1;
                $campusClassesNum+=1;
            }
        }

        $progress = intval(($solvedClassesNum/$campusClassesNum)*100);

        return ['all'=>$campusClassesNum,'solved'=>$solvedClassesNum,'progress'=>$progress];

    }

    public function updateCampusUserStatus($user_id,$campus_id,$gainedScore,$streak_xp = 0, $latestStreakStartId = null){
        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);
        
        if($campusUserStatus) {
            $campusUserStatus->xp += $gainedScore;
        }else{
            $campusUserStatus = new CampusUserStatus($user_id,$campus_id,0,1);
        }
        $campusUserStatus->streak_xp += $streak_xp;//TODO:remove this

        $latestStreakPeriod = $this->professionalRepository->getLatestStreakPeriod($user_id, $campus_id);
        $latestStreakPeriodLength = $latestStreakPeriod->count();
        if($latestStreakPeriodLength == 7){
            $campusUserStatus->streak_xp += 1;//add crown for 7 day streak
        }

        if(isset($latestStreakStartId)){
            //$campusUserStatus->latestStreakStartId = $latestStreakStartId;

            DB::table('campus_user_status')->where('id', $campusUserStatus->id)->update(['latestStreakStartId' => $latestStreakStartId,'streak_xp'=>$campusUserStatus->streak_xp]);//Weird BUG

        }

        return $this->professionalRepository->storeCampusUserStatus($campusUserStatus);
    }

    public function CheckUserCrownAndStatus($user_id,$campus_id,$gmt_difference){

        $campusUserStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id);
        $weekDays = $this->professionalRepository->getStreaksWeek($user_id,$campus_id,$gmt_difference); 
            
        $streaksCounter = 0;
        foreach($weekDays as $day=>$status){
            if($status == true)
                $streaksCounter++;
        }
        if($streaksCounter == 7)
            $campusUserStatus->streak_xp += 1;


        return $this->professionalRepository->storeCampusUserStatus($campusUserStatus);   
    }

    public function getPreviousTask($order,$activity_id,$hideBlocksStatus,$showClassesStatus){

        $previous_task = $this->journeyRepository->getTaskInActivityByOrder($order, $activity_id, true);

        if($hideBlocksStatus){
            if($previous_task->getTaskNameAndType()['task_type'] != 'mission'){
                return $previous_task;
            }else{
                 return $this->getPreviousTask($order-1,$activity_id,$hideBlocksStatus,$showClassesStatus);
            }
        }else{
            return $previous_task;
        }
    }

    public function checkTaskLocker($task,$campus_id,$activity_id,$user_id,$is_default,$campusHasSubModules){
        if($campusHasSubModules) {
            $taskModule = $this->professionalRepository->getCampusClass($campus_id, $activity_id, $is_default);
            $submodules = $taskModule->getSubmodules();
            foreach($submodules as $submodule) {
                if($this->professionalRepository->isSubmoduleHaveTask($submodule->getId(), $task->getId())) {
                    $taskSubmodule = $submodule;
                    break;
                }
            }
            if($this->checkTaskLockingInSubModules($user_id,$campus_id,$taskModule,$taskSubmodule)){
                throw new BadRequestException(trans("locale.task_locked"));
            }
        }
        else {
            $this->CheckTaskLockingInModules($task,$campus_id,$activity_id,$user_id,$is_default);
        }
    }

    private function checkTaskLockingInSubModules($user_id,$campus_id,$module,$submodule){
        $user = $this->accountRepository->getUserById($user_id);
        if(!$user->isStudent())
            return false;
        //Check if first submodule -->locked=true
        //Check if not first submodule -->check previous module locked or not(all missions are solved)
        $firstSubmoduleInModule=$this->professionalRepository->getFirstSubmoduleInModule($module);
        if($submodule->getId() !== $firstSubmoduleInModule->getId()){
            $previousSubModule=$this->professionalRepository->getSubmoduleInModuleByOrder($module,intval($submodule->getOrder())-1);
            $hasFinishedSubmoduleTasks=$this->professionalRepository->hasFinishedSubmoduleTasks($user,$campus_id,$previousSubModule);
            return !$hasFinishedSubmoduleTasks;

        }else{
            return false;
        }
    } 

    private function CheckTaskLockingInModules($task,$campus_id,$activity_id,$user_id,$is_default){
        // return true; // Unlock for development purpose
        //Check if Student Only 
        $user = $this->accountRepository->getUserById($user_id);
        if($user->isStudent()){
        //Check if preivous task was completed
        //$activity_task = $task->getActivityTask();
        $activity_task = $this->professionalRepository->getActivityTask($task->getId(),$activity_id);
        $order = $activity_task->order;

        //Check User Hide Blocks & ShowClasses Status
        $hideBlocksStatus = $this->professionalRepository->getCampusUserStatus($campus_id,$user_id)->hide_blocks == 1 ? true:false ;
        // $showClassesStatus = $round->showClasses();
  
        if($order > 1){
            // $activity_id = $activity_task->default_activity_id;
            // $previous_task = $this->journeyRepository->getTaskInActivityByOrder($order-1, $activity_id, true);
            $previous_task = $this->getPreviousTask($order-1,$activity_id,$hideBlocksStatus,true);

            $previous_task_activty = $this->journeyRepository->getActivityTaskOrder($previous_task, $activity_id, true);

            if($activity_task->booster_type!=="1"&&$previous_task_activty->booster_type=='1'){
                $previous_task=$this->journeyRepository->getTaskInActivityByOrder($order-2, $activity_id, true);
            }

            //Check User progress
            $user_progress = $this->professionalRepository->getUserRoundTaskProgressActivity($previous_task,$user_id,$campus_id,$activity_id,true);
            if($user_progress!=null){
                    //When isMiniProject url submited
                    
                    if(filter_var($user_progress->user_code, FILTER_VALIDATE_URL)){
                        $passed = true;
                    }else{
                        $passed=$user_progress->getFirstSuccess()!==null?true:false;
                    }

                }else{
                    $passed=false;
            }
            if($passed == false && $activity_task->booster_type != 1){
                throw new BadRequestException(trans("locale.task_locked"));
            }
           }
        }
    }

    // Scoring and Leveling
    public function updateScore(CampusActivityProgress $campusActivityProgress, $round,$gmt_difference, $taskType, $data = [], $updateProgressEvaluation = true){
        $user = $campusActivityProgress->getUser();
        $campus = $campusActivityProgress->getCampus();
        $campusUserStatus=$this->professionalRepository->getCampusUserStatus($campus->id,$user->id);
        $fullMarkNotSolvedTask = $this->getTaskFullMarkInCampusRound($user, $campusActivityProgress, $round);
        $fullMark=$fullMarkNotSolvedTask;
        /*if($campusUserStatus->xp+$fullMarkNotSolvedTask >= $campus->xps)
            $fullMark=$campusUserStatus->xp+$fullMarkNotSolvedTask - $campus->xps;*/
        if($campusUserStatus->xp >= $campus->xps)
            $fullMark=0;
        $fullMark=$fullMark===null?0:$fullMark;

        $gainedScore = $this->calculateGainedScore($campusActivityProgress, $fullMark, $taskType, $data);
        
        $gainedXps=$gainedScore['gainedXps'];
        $currentXps=$gainedScore['currentXps'];

        $oldEvaluation = $campusActivityProgress->getEvaluation();
        $netGainedScore = $gainedXps - $oldEvaluation;
        

        if($taskType == self::EVALUATED_MISSION){
            $updateProgressScore = false;
            $applyGainedScore = true;
        }else{
            $updateProgressScore = true;
            if(($netGainedScore > 0&& $fullMark>0)||$fullMark==0 )
                $applyGainedScore = true;
            else
                $applyGainedScore = false;
        }

        if($applyGainedScore) {
            $campusActivityProgress = $this->updateCampusActivityProgress($campusActivityProgress, $gainedXps, $updateProgressEvaluation, $updateProgressScore,$fullMark);
            $campusUserStatus=$this->updateUserXpsAndRank($user, $campus, $gmt_difference,$netGainedScore);
        }else{
            $campusUserStatus=$this->professionalRepository->getCampusUserStatus($campus->id,$user->id);

        }

        $scoreData=$this->getUserScoreInCampus($campusUserStatus,$gmt_difference);

        return ["campusUserStatus"=>$campusUserStatus,"campusActivityProgress"=>$campusActivityProgress
            ,"fullMark"=>$fullMarkNotSolvedTask,"scoreData"=>$scoreData, "gainedXps"=>$gainedXps,"currentXps"=>$currentXps];
    }


    public function CheckStudentCrown($user_id,$campus_id,$gmt_difference){
        $latestStreakPeriod = $this->professionalRepository->getLatestStreakPeriod($user_id, $campus_id);
            if($latestStreakPeriod->count() >= 7 ){
                //Reset Streaks
                    $user = $this->accountRepository->getUserById($user_id);
                    $this->professionalRepository->resetStudentStreaks($user,$campus_id,$gmt_difference);
                return true;
            }
            return false;
    }

    public function getUserScoreInCampus(CampusUserStatus $campusUserStatus,$gmt_difference){
        $rankLevel = $this->professionalRepository->getRankLevelById($campusUserStatus->rank_level_id);
        $this->CheckStudentCrown($campusUserStatus->user_id,$campusUserStatus->campus_id,$gmt_difference);
        
        //Check Crown to make Reset and notify frontend
        $scoreData = [];
        $scoreData['campus_id'] = $campusUserStatus->campus_id;
        $scoreData['rank'] = $rankLevel->rank->getName();
        $scoreData['level'] = $rankLevel->level->getName();
        $scoreData['xp'] = $campusUserStatus->xp;
        $scoreData['crowns'] = $campusUserStatus->streak_xp;
        // $scoreData['gained_crown'] = $this->CheckStudentCrown($campusUserStatus->user_id,$campusUserStatus->campus_id,$gmt_difference);
        $scoreData['xps_to_next_level'] = $this->calculateNeededXpsForNextRankLevel($campusUserStatus);

        return $scoreData;

    }
    public function getTaskFullMarkInCampusRound(User $user, CampusActivityProgress $campusActivityProgress, CampusRound $round){
        $task = $campusActivityProgress->getTask();
        $campus = $campusActivityProgress->getCampus();
        $activity = $campusActivityProgress->getDefaultActivity();

        $showLessons = $round->showClasses();
        $showBlocks = $user->showBlocks($campus->getId());
        $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
        if($activityTask->booster_type=='1'){//booster_mission
            $mainActivityTask = $this->professionalRepository->getMainTaskByBoosterTask($task->getId(),$activity->getId());
            $mainTask=$mainActivityTask->getTask();
            $mainTaskProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($mainTask, $user->getId(), $campus->getId(), $activity->getId(),true);

            $mainCampusActivityTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $mainTask->getId());
            $mainTaskFullMark=$mainCampusActivityTask->getTaskFullMark($showLessons, $showBlocks);
            // dd($mainTaskProgress);
            $mainTaskGainedScore=$mainTaskProgress->getEvaluation();
            if($mainTaskGainedScore===null)//This should be discussed if we would have booster for miniproject
                throw new BadRequestException(trans('locale.you_have_not_completed_main_mission_yet'));


            $boosterTaskFullMark=$mainTaskFullMark-$mainTaskGainedScore;
            return $boosterTaskFullMark;
        }else{
            $campusActivityTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $task->getId());
            $mainTaskFullMark = $campusActivityTask->getTaskFullMark($showLessons, $showBlocks);
            //Main Mission(has booster'2' or no booster'1')
            $boosterActivityTask = $this->professionalRepository->getBoosterTaskByMainTask($task->getId(),$activity->getId());
            if($boosterActivityTask!==null){
                //solved booster
                $boosterTask=$boosterActivityTask->getTask();
                $boosterTaskProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($boosterTask, $user->getId(), $campus->getId(), $activity->getId(),true);
                if($boosterTaskProgress!==null){
                    $boosterTaskEvaluation=$boosterTaskProgress->getEvaluation()!==null?$boosterTaskProgress->getEvaluation():0;
                    return $mainTaskFullMark-$boosterTaskEvaluation;
                }
            }
            //didn't have  booster or did't solve it
            return $mainTaskFullMark;
        }

    }

    public function getMainTaskFullMark(User $user, $task,$activity, CampusRound $round){
        
        $campus = $round->getCampus();
        
        $showLessons = $round->showClasses();
        $showBlocks = $user->showBlocks($campus->getId());
        $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
        
        $campusActivityTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $task->getId());
        $mainTaskFullMark = $campusActivityTask->getTaskFullMark($showLessons, $showBlocks);
        
        return $mainTaskFullMark;
        

    }

    public function getBoosterTaskFullMark(User $user, $task,$activity, CampusRound $round){
        $campus = $round->getCampus();
        
        $showLessons = $round->showClasses();
        $showBlocks = $user->showBlocks($campus->getId());
        $activityTask = $this->professionalRepository->getActivityTask($task->getId(),$activity->getId());
        if($activityTask->booster_type=='1'){//booster_mission
            $mainActivityTask = $this->professionalRepository->getMainTaskByBoosterTask($task->getId(),$activity->getId());
            $mainTask=$mainActivityTask->getTask();
            $mainTaskProgress=$this->professionalRepository->getUserRoundTaskProgressActivity($mainTask, $user->getId(), $campus->getId(), $activity->getId(),true);

            $mainCampusActivityTask = $this->professionalRepository->getCampusActivityTask($campus->getId(), $mainTask->getId());
            $mainTaskFullMark=$mainCampusActivityTask->getTaskFullMark($showLessons, $showBlocks);
            if($mainTaskProgress!==null && $mainTaskProgress->getEvaluation()!==null){
                $mainTaskGainedScore=$mainTaskProgress->getEvaluation();
                return $mainTaskFullMark-$mainTaskGainedScore;
            }
            else
                return $mainTaskFullMark;
            
        }

    }
    private function calculateGainedScore(CampusActivityProgress $campusActivityProgress, $fullMark, $taskType, $data = []){
        switch ($taskType){
            case self::HTML_MISSION:
                return $this->calculateHTMLMissionScore($campusActivityProgress, $fullMark);

            case self::QUIZ_MISSION:
                return $this->calculateQuizMissionScore($campusActivityProgress, $fullMark, $data);

            case self::EDITOR_MISSION:
                return $this->calculateEditorMissionScore($campusActivityProgress, $fullMark, $data);

            case self::BLOCKLY_MISSION:
                return $this->calculateBlocklyMissionScore($campusActivityProgress, $fullMark);

            case self::BLOCKLY_MCQ_MISSION:
                return $this->calculateBlocklyMCQMissionScore($campusActivityProgress, $fullMark);

            case self::PYTHON_MISSION:
                return $this->calculatePythonMissionScore($campusActivityProgress, $fullMark);

            case self::EVALUATED_MISSION:
                return $this->calculateEvaluatedMissionScore($campusActivityProgress, $fullMark, $data);

        }
    }
    private function updateCampusActivityProgress(CampusActivityProgress $campusActivityProgress, $gainedScore, $updateEvaluation = true, $updateScore = true,$fullMark){
        if($updateScore)
            $campusActivityProgress->setScore($gainedScore);

        if($updateEvaluation)
            $campusActivityProgress->setEvaluation($gainedScore);

        if((($gainedScore > 0 &&$fullMark>0) ||($fullMark==0))&& $campusActivityProgress->getFirstSuccess()==null )
            $campusActivityProgress->setFirstSuccess($campusActivityProgress->getNoTrials());

        return $campusActivityProgress;
    }
    public function updateUserXpsAndRank(User $user, Campus $campus,$gmt_difference, $gainedScore){
        $campusUserStatus = $this->updateCampusUserStatus($user->getId(), $campus->getId(),$gainedScore,0);
        $campusUserStatus = $this->CheckUserCrownAndStatus($user->getId(), $campus->getId(),$gmt_difference);
        $userXps = $campusUserStatus->getXps();
        if($userXps>$campus->getXps())
            $campusUserStatus->xp=$campus->getXps();
        $rankLevelId = $this->calculateUserRankLevel($userXps, $campus);
        $campusUserStatus->setRankLevelId($rankLevelId);
        return $campusUserStatus;
    }
    public function calculateUserRankLevel($xps, Campus $campus){
        $rankLevelXps = $this->calculateRankLevelXps($campus);

        return ceil($xps/$rankLevelXps) > 0 ? ceil($xps/$rankLevelXps) : 1;
    }

    private function calculateHTMLMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->first_success){
            return ['gainedXps'=>$fullMark,'currentXps'=>$fullMark];
        }

        return ['gainedXps'=>0,'currentXps'=>0];
    }
    private function calculateQuizMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark, $data){

        $quizCorrectnessAndConfidencePercentage=$data["quizCorrectnessAndConfidencePercentage"];
        $duration=$data["duration"];
        $quiz=$data["quiz"];

        $quizTimePercentage=$this->calculateQuizTimePercentage($quiz,$duration);

        $quizPercentage=$quizCorrectnessAndConfidencePercentage*$quizTimePercentage;

        $gainedXps=$quizPercentage*$fullMark;
        $currentXps=$gainedXps;
        $quizThresholdXps=$this->professionalRepository->getScoringConstant('defaultQuizThreshold_xps')->getValue();
        $previousLearnerGainedXps=$campusActivityProgress->getScore();
        $trials=$campusActivityProgress->getNoTrials();
        if($trials>1  && $gainedXps> $quizThresholdXps){
            if($previousLearnerGainedXps<$quizThresholdXps)
                $gainedXps=$quizThresholdXps;
            else
                $gainedXps=$previousLearnerGainedXps;

        }
        return ["currentXps"=>round($currentXps),"gainedXps"=>round($gainedXps)];
    }

    // Required data for calculating editor mission score is:
    // - Number of correctly answered questions ... data['correctAnsCount']
    // - Total number of questions ... data['questionsCount']
    private function calculateEditorMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark, $data){
        if(!isset($data))
            throw new BadRequestException("Data missing for calculating editor mission score");

        if($campusActivityProgress->first_success){
            $correctAnsPercentage = $data['correctAnsCount'] / $data['questionsCount'];
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = 0;
            if($trials <= 3)
                $trialsWeight = $this->calculateTrialsWeight('trials_constant', $trials);
            //dd($correctAnsPercentage , $trialsWeight ,$fullMark,$campusActivityProgress->evaluation);
            $currentScore= round($correctAnsPercentage * $trialsWeight * $fullMark);
            if($currentScore <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentScore,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentScore,"gainedXps"=>$currentScore];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }
    private function calculateBlocklyMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->first_success){
            $usedBlocksCount = $campusActivityProgress->getNoBlocks();
            $mission = $campusActivityProgress->getTask()->getMissions()->first();
            $minBlocksCount = $mission->getModelAnswerNumberOfBlocks();
            $maxBlocksCount = $this->calculateMaxBlocksCount($minBlocksCount);

            $blocksCountWeight = $this->calculateBlocksCountWeight($usedBlocksCount, $maxBlocksCount, $minBlocksCount);
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = $this->calculateTrialsWeight('blocklyTrials_constant', $trials);

            $currentXps= round($blocksCountWeight * $trialsWeight * $fullMark);
            if($currentXps <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentXps,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentXps,"gainedXps"=>$currentXps];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }
    private function calculateBlocklyMCQMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->first_success){
            $usedBlocksCount = $campusActivityProgress->getNoBlocks();
            $mission = $campusActivityProgress->getTask()->getMissions()->first();
            $minBlocksCount = $mission->getModelAnswerNumberOfBlocks();
            $maxBlocksCount = $this->calculateMaxBlocksCount($minBlocksCount);

            $blocksCountWeight = $this->calculateBlocksCountWeight($usedBlocksCount, $maxBlocksCount, $minBlocksCount);
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = 0;
            if($trials <= 3)
                $trialsWeight = $this->calculateTrialsWeight('blocklyTrials_constant', $trials);

            $currentXps= round($blocksCountWeight * $trialsWeight * $fullMark);

            if($currentXps <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentXps,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentXps,"gainedXps"=>$currentXps];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }
    private function calculatePythonMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark){
        if($campusActivityProgress->getFirstSuccess()){
            $trials = $campusActivityProgress->getNoTrials();
            $trialsWeight = $this->calculateTrialsWeight('pythonTrials_constant', $trials);

            $currentXps=round($trialsWeight * $fullMark);
            if($currentXps <$campusActivityProgress->evaluation)
                return ["currentXps"=>$currentXps,"gainedXps"=>$campusActivityProgress->evaluation];
            else
                return ["currentXps"=>$currentXps,"gainedXps"=>$currentXps];
        }

        return ["currentXps"=>0,"gainedXps"=>0];
    }

    // Required data for calculating mini project score is:
    // - Instructor evaluation ... data['evaluation']
    private function calculateEvaluatedMissionScore(CampusActivityProgress $campusActivityProgress, $fullMark, $data){
        if(!isset($data))
            throw new BadRequestException("Data missing for calculating evaluated mission score");
        
        // if($campusActivityProgress->getFirstSuccess() && $campusActivityProgress->getIsEvaluated()){
            $evaluation = $data['evaluation'];
            if($evaluation >= 1){
                $evaluation /= 100;
            }
            $gained = round($evaluation * $fullMark);
            return ["currentXps"=>$gained,"gainedXps"=>$gained];
        // }else{
        //     return 0;
        // }

    }

    private function calculateTrialsWeight($trialsConstName, $trials){
        $trialsConstant = $this->professionalRepository->getScoringConstant($trialsConstName)->value;
        return exp(-1 * $trialsConstant * ($trials-1));
    }
    private function calculateBlocksCountWeight($usedBlocksCount, $maxBlocksCount, $minBlocksCount){
        $avgBlocksCount = ($minBlocksCount + $maxBlocksCount) / 2;

        if($usedBlocksCount > $maxBlocksCount){
            return $this->professionalRepository->getScoringConstant('usedBlocks_score_low')->value;
        }elseif ($usedBlocksCount >= $avgBlocksCount){
            return $this->professionalRepository->getScoringConstant('usedBlocks_score_medium')->value;
        }else{
            return $this->professionalRepository->getScoringConstant('usedBlocks_score_high')->value;
        }
    }
    private function calculateMaxBlocksCount($minBlocksCount = 1){
        return 2 * $minBlocksCount;
    }
    private function calculateQuizTimePercentage(Quiz $quiz,$learnerQuizTimeDuration){
        $questionTime=$this->professionalRepository->getScoringConstant('defaultQuizQuestion_time')->value;
        $quizTimeConstant=$this->professionalRepository->getScoringConstant('time_constant')->value;
        $quizQuestionCount=count($quiz->getQuestionTasks());
        $quizTime=$questionTime*$quizQuestionCount;

        $userTimeUnit=floor($learnerQuizTimeDuration/$quizTime);

        $quizTimePercentage=exp(-$quizTimeConstant*$userTimeUnit);
        return $quizTimePercentage;
    }
    public function calculateNeededXpsForNextRankLevel(CampusUserStatus $campusUserStatus){
        $campus = $campusUserStatus->getCampus();
        $rankLevelXps = $this->calculateRankLevelXps($campus);
        $userRankLevel = $campusUserStatus->rank_level_id;
        $nextRankLevelXps = $userRankLevel * $rankLevelXps;
        $userXps = $campusUserStatus->getXps();

        return $nextRankLevelXps - $userXps;
    }
    private function calculateRankLevelXps(Campus $campus){
        $totalCampusXps = $campus->getXps();
        $rankLevelsCount = $this->professionalRepository->countRankLevels();

        return $totalCampusXps / $rankLevelsCount;
    }

    public function getDropdownStep($step,$user,$activity,$campus,$mission,$task)
    {
        
        $dropdown_question=$step->getQuestion();
        $stepDropdownQuestionDto = Mapper::MapClass(StepDropdownQuestionDto::class,$dropdown_question);
        $stepDropdownQuestionDto->questionId=$step->getStepQuestionId();
        $stepDropdownQuestionDto->title=$step->getTitle();
        $stepDropdownQuestionDto->brickType=$step->getStepQuestionType();
        $dropdowns=$dropdown_question->getDropdown();

        // check if user answer all dropdown question
        $solveAll = true;
        foreach($dropdowns as $d){
            if($user->isStudent()){
                $question = mapper(Question::class)->find($step->getStepQuestionId());
                $userData = $this->getUserTaskProgressData($task,$user,$campus,$activity,$question,true,false);
                $campusActivityQuestion = $this->professionalRepository->getCampusActivityQuestion($userData['missionProgress'],$question);
                if($campusActivityQuestion != null){
                    $campusActivityQuestionId=$campusActivityQuestion->id;
                    $questionAnswers = $this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,$step->getStepQuestionType());

                    $solved=false;
                    foreach($questionAnswers as $answer){
                        if($d->getId() == $answer['dropdown_id']){
                            $solved=true;
                        }
                    }
                    if($solved != true)
                    {
                        $solveAll=false;
                        break;
                    }
                }
            }
        }
        
        foreach($dropdowns as $d){
            $stepDropdownsDto = Mapper::MapClass(StepDropdownsDto::class,$d,[StepDropdownsChoiceDto::class]);
            $dropdowns_choice=$d->getChoices();
            // dd($stepDropdownsDto);
            if($user->isStudent()){
                //Check if student has answeres
                $question = mapper(Question::class)->find($step->getStepQuestionId());
                $userData = $this->getUserTaskProgressData($task,$user,$campus,$activity,$question,true,false);
                $campusActivityQuestion = $this->professionalRepository->getCampusActivityQuestion($userData['missionProgress'],$question);
                if($campusActivityQuestion != null){
                    $campusActivityQuestionId=$campusActivityQuestion->id;
                    $questionAnswers = $this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,$step->getStepQuestionType());
                    $index = 0;
                    foreach($dropdowns_choice as $dropChoice){
                        $stepDropdownsChoiceDto = Mapper::MapClass(StepDropdownsChoiceDto::class,$dropChoice);
                        $dropDownQuestionAnswer=$this->professionalRepository->getDropdownQuestionAnswer($userData["campusActivityQuestion"],$d);
                        
                        if($solveAll == true){
                            if($dropDownQuestionAnswer->dropdown_choice_id == $dropChoice->id)
                                 $stepDropdownsDto->choices[$index]->selected = true;
                            else
                                $stepDropdownsDto->choices[$index]->selected = false;
                        }else{
                             // delete answers of this question type
                            $this->accountRepository->beginDatabaseTransaction();
                            $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,$step->getStepQuestionType());
                            $this->accountRepository->commitDatabaseTransaction();
                        }
                        // array_push($stepDropdownsDto->choices,$stepDropdownsChoiceDto);
                        $index++;
                    }
                    if(!$solveAll){
                        $this->accountRepository->beginDatabaseTransaction();
                        $this->professionalRepository->deleteCampusActivityQuestion($campusActivityQuestion);
                        $this->accountRepository->commitDatabaseTransaction();
                    }
                    
                    // dd($stepDropdownsDto->choices);
                }
            }
            array_push($stepDropdownQuestionDto->dropdowns,$stepDropdownsDto);
        }
        // dd($stepDropdownQuestionDto);
        // dd($stepDropdownQuestionDto->dropdowns);
        return $stepDropdownQuestionDto;

    }
    public function getDragStep($step,$user,$activity,$campus,$mission,$task)
    {
        $dragQuestion=$step->getDragQuestion();
        
        $dragQuestionDto = Mapper::MapClass(DragQuestionDto::class,$dragQuestion,[DragCellDto::class,DragChoiceDto::class]);
        $dragQuestionDto->brickType="sequence_match";
        

        if($user->isStudent()){
            //Check if student has answeres 
            $question = mapper(Question::class)->find($step->getStepQuestionId());
            $userData = $this->getUserTaskProgressData($task,$user,$campus,$activity,$question,true,false);
            $campusActivityQuestion = $this->professionalRepository->getCampusActivityQuestion($userData['missionProgress'],$question);
            if($campusActivityQuestion != null){
                $campusActivityQuestionId=$campusActivityQuestion->id;
                $questionAnswers = $this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,$step->getStepQuestionType());
            

              // check if user answer all drag question
                $solveAll = true;
                if($questionAnswers != null){
                    $dragQuestionDto->dragChoices =[];
                    foreach($dragQuestionDto->dragCells as $index=>$cell)
                    {
                        if($cell->showAnswer==0){
                            $solved=false;
                            foreach($questionAnswers as $answer){
                                if($cell->id == $answer['drag_cell_id']){
                                    $solved=true;
                                }
                            }
                            if($solved != true)
                            {
                                $solveAll=false;
                                break;
                            }
                        }
                    }   
                }

                if($questionAnswers != null){
                    $dragQuestionDto->dragChoices =[];
                        foreach($dragQuestionDto->dragCells as $index=>$cell){
                            //Check if Cell has a dropped Choice
                            $dragChoiceId = $this->professionalRepository->getDragChoiceId($campusActivityQuestionId,$cell->id);
                            if($solveAll==true){
                                if($dragChoiceId != null){
                                    $dragChoice = $this->professionalRepository->getDragChoiceById($dragChoiceId);
                                    $cell->userAnswer = Mapper::MapClass(DragChoiceDto::class,$dragChoice);
                                }
                            }else{
                            // delete answers of this question type
                            $this->accountRepository->beginDatabaseTransaction();
                            $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,$step->getStepQuestionType());
                            $this->professionalRepository->deleteCampusActivityQuestion($campusActivityQuestion);
                            $this->accountRepository->commitDatabaseTransaction();
                            }    
                       }   
                }
            }
        }
        $dragQuestionDto->dragCellsCount=count($dragQuestion->getDragCells());
        return $dragQuestionDto;
    }

    public function getMcqQuestionStep($step,$user,$activity,$campus,$mission,$task)
    {
        $mcqQuestion=$step->getQuestion();
        $stepMcqQuestionDto = Mapper::MapClass(StepMcqQuestionDto::class,$mcqQuestion);
        $stepMcqQuestionDto->questionId=$step->getStepQuestionId();
        $stepMcqQuestionDto->brickType=$step->getStepQuestionType();

        $mcqQuestionChoices=$mcqQuestion->getMcqQuestionChoices();
        if($user->isStudent()){
            //Check if student has answeres 
            $question = mapper(Question::class)->find($step->getStepQuestionId());
            $userData = $this->getUserTaskProgressData($task,$user,$campus,$activity,$question,true,false);
            $campusActivityQuestion = $this->professionalRepository->getCampusActivityQuestion($userData['missionProgress'],$question);
            if($campusActivityQuestion != null){
                $campusActivityQuestionId=$campusActivityQuestion->id;
                $questionAnswers = $this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,$step->getStepQuestionType());
            }
        }
        foreach($mcqQuestionChoices as $mcqChoice){
            $mcqQuestionChoicesDto = Mapper::MapClass(McqQuestionChoicesDto::class,$mcqChoice);
            if($user->isStudent()){
            //apply answeres
                if($campusActivityQuestion != null){
                    foreach($questionAnswers as $answere){
                        if($answere['choice_id'] == $mcqChoice->id)
                            $mcqQuestionChoicesDto->selected = true;
                    }
                }
            }
            array_push($stepMcqQuestionDto->choices,$mcqQuestionChoicesDto);

        }

        return $stepMcqQuestionDto;

    }

    public function getMatchQuestionStep($step,$user,$activity,$campus,$mission,$task)
    {
        $matchQuestion=$step->getQuestion();
        $stepMatchQuestionDto = Mapper::MapClass(StepMatchQuestionDto::class,$matchQuestion);
        $stepMatchQuestionDto->questionId=$step->getStepQuestionId();
        $stepMatchQuestionDto->brickType=$step->getStepQuestionType();
        $stepMatchQuestionDto->title=$step->getTitle();
        $stepMatchQuestionDto->description=$step->getDescription();
        $stepMatchQuestionDto->rightExplanation=$step->getRightExplanation();
        $stepMatchQuestionDto->wrongExplanation=$step->getWrongExplanation();

        $matchKeys=$matchQuestion->getMatchKeys();
        $stepKeys=[];
        $stepValues=[];
        $stepKeyValPairs=[];

        // check if user answer all match question
        $solveAll = true;
        foreach($matchKeys as $index=>$key){ 
            if($user->isStudent()){
                $question = mapper(Question::class)->find($step->getStepQuestionId());
                $userData = $this->getUserTaskProgressData($task,$user,$campus,$activity,$question,true,false);
                $campusActivityQuestion = $this->professionalRepository->getCampusActivityQuestion($userData['missionProgress'],$question);

                if($campusActivityQuestion != null ){
                    $campusActivityQuestionId=$campusActivityQuestion->id;
                    $questionAnswers = $this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,$step->getStepQuestionType());
                    $solved=false;
                    foreach($questionAnswers as $answer){
                        if($index == $answer['key_id']){
                            $solved=true;
                        }
                    }
                    if($solved != true)
                    {
                        $solveAll=false;
                        break;
                    }
                }    
            }
        }

        foreach($matchKeys as $index=>$key){
            $matchKeysDto = Mapper::MapClass(MatchKeysDto::class,$key);
            $matchValue=$key->getMatchValue();
            $matchKeysDto->value_id=$matchValue->id;
            if($user->isStudent()){
                //Check if student has answeres for Match
                $question = mapper(Question::class)->find($step->getStepQuestionId());
                $userData = $this->getUserTaskProgressData($task,$user,$campus,$activity,$question,true,false);
                $campusActivityQuestion = $this->professionalRepository->getCampusActivityQuestion($userData['missionProgress'],$question);

                if($campusActivityQuestion != null ){
                    $campusActivityQuestionId=$campusActivityQuestion->id;
                    $QuestionAnswers = $this->professionalRepository->getQuestionAnswerById($campusActivityQuestionId,$step->getStepQuestionType());

                    if($solveAll == true){
                        foreach($QuestionAnswers as $answere){
                            if ($answere['key_id'] == $index)
                            $matchKeysDto->answered_value_id=$answere['value_id'];
                        }
                    }else{
                        // delete answers of this question type
                        $this->accountRepository->beginDatabaseTransaction();
                        $this->professionalRepository->deleteQuestionAnswer($campusActivityQuestionId,$step->getStepQuestionType());
                        $this->professionalRepository->deleteCampusActivityQuestion($campusActivityQuestion);
                        $this->accountRepository->commitDatabaseTransaction();

                    }
                }
            }
            array_push($stepKeys,$matchKeysDto);
        }
 
        $matchValues=$matchQuestion->getMatchValues();
        foreach($matchValues as $val){
                $matchValueDto = Mapper::MapClass(MatchValueDto::class,$val);
                array_push($stepValues,$matchValueDto);
        }
        //usort($stepKeys, array($this, 'sortByOrder'));
        //usort($stepValues, array($this, 'sortByOrder'));
        foreach ($stepKeys as $index => $key) {
            $stepKeyValPair["key"]=$key;
            $stepKeyValPair["value"]=$stepValues[$index];
            $stepKeyValPairs[]=$stepKeyValPair;
        }
        $stepMatchQuestionDto->keyValuePairs=$stepKeyValPairs;
        return $stepMatchQuestionDto;

    }
}