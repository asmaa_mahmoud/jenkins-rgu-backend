<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 5:37 PM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class TagReducedDto
{
// DTO for Tag to just reference and identify it

    public $id;
    public $name;
}