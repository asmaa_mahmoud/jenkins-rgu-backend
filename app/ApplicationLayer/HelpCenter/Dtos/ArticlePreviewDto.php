<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 26-Feb-19
 * Time: 4:37 PM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class ArticlePreviewDto
{
// DTO for Article when it's listed within a list of articles

    public $id;
    public $title;
    public $iconUrl;
    public $topics = [];
    public $tags = [];
    public $partSummary;
}