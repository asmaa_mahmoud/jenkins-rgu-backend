<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class ArticleDto
{
// DTO for Article when all its data is needed

    public $id;
    public $title;
    public $body;
    public $summary;
    public $iconUrl;
    public $isRecommended;
    public $nLikes;
    public $nDislikes;
    public $topics = [];
    public $tags = [];
}