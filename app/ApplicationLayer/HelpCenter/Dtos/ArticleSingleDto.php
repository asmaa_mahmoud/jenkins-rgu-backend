<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class ArticleSingleDto
{
// DTO for Article without its relations

    public $id;
    public $title;
    public $body;
    public $iconUrl;
    public $isRecommended;
    public $nLikes;
    public $nDislikes;
}