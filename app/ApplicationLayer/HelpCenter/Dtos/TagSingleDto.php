<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class TagSingleDto
{
// DTO for Tag without its relations

    public $id;
    public $name;
    public $iconUrl;
}