<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 5:36 PM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class TopicReducedDto
{
// DTO for Topic to just reference and identify it

    public $id;
    public $title;
}