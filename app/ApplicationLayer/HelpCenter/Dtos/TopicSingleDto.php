<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class TopicSingleDto
{
// DTO for Topic without its relations

    public $id;
    public $title;
    public $description;
    public $iconUrl;
    public $articlesCount;
    public $order;
}