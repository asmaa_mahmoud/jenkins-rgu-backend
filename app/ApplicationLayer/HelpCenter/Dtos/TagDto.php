<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:56 AM
 */

namespace App\ApplicationLayer\HelpCenter\Dtos;


class TagDto
{
// DTO for Article when all its data is needed

    public $id;
    public $name;
    public $iconUrl;
    public $articles = [];
}