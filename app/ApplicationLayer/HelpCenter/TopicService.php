<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 2:02 PM
 */

namespace App\ApplicationLayer\HelpCenter;


use App\ApplicationLayer\HelpCenter\Dtos\ArticleDto;
use App\ApplicationLayer\HelpCenter\Dtos\ArticlePreviewDto;
use App\ApplicationLayer\HelpCenter\Dtos\ArticleReducedDto;
use App\ApplicationLayer\HelpCenter\Dtos\TagReducedDto;
use App\ApplicationLayer\HelpCenter\Dtos\TopicDto;
use App\ApplicationLayer\HelpCenter\Dtos\TopicReducedDto;
use App\ApplicationLayer\HelpCenter\Dtos\TopicSingleDto;
use App\DomainModelLayer\HelpCenter\Repositories\IHelpCenterMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\HelpCenter\Topic;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Pagination\Paginator;


class TopicService{

    private $helpCenterRepository;

    public function __construct(IHelpCenterMainRepository $helpCenterMainRepository,IAccountMainRepository $accountRepository){
        $this->helpCenterRepository = $helpCenterMainRepository;
        $this->accountRepository = $accountRepository;

    }

    public function getAllTopics($details, $start_from, $limit,$search=null){
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $topics = $this->helpCenterRepository->getAllTopics($limit,$search);
        if($details){
            $topicsMapped = Mapper::MapEntityCollection(TopicDto::class, $topics, [ArticleReducedDto::class]);
        }else{
            $topicsMapped = Mapper::MapEntityCollection(TopicSingleDto::class, $topics);
        }

        return $topicsMapped;
    }


    public function getAllTopicsCount(){

        $topics = $this->helpCenterRepository->getAllTopicsCount();
        // $topicsMapped = Mapper::MapEntityCollection(TopicDto::class, $topics);

        return $topics;

    }

    public function getTopic($id, $start_from, $limit){
        // Get topic main data
        $topic = $this->helpCenterRepository->getTopic($id);
        $topicMapped = Mapper::MapEntityCollection(TopicDto::class, $topic, [ArticleReducedDto::class]);

        // Associate the topic with the full data of its articles.
        $topicArticlesIds = array_map(function($item){
            return $item->id;
        }, $topicMapped[0]->articles);
       
        
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $topicArticles = $this->helpCenterRepository->getArticles($topicArticlesIds, $limit);;
        $topicArticlesMapped = Mapper::MapEntityCollection(ArticlePreviewDto::class, $topicArticles,
            [TopicReducedDto::class, TagReducedDto::class]);
        $topicMapped[0]->articles = $topicArticlesMapped;

        return $topicMapped;
    }
    
    public function getTopicArticlesCount($id){
        $topic = $this->helpCenterRepository->getTopic($id)->first();
//        dd($topic->getArticlesCount());
            if($topic != null){
            return $topic->getArticlesCount();
           }else{
            return 0;
           }
       
    }

    public function getAllTopicsAdmin($start_from, $limit,$search){
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $topics = $this->helpCenterRepository->getAllTopics($limit,$search);
        $topicsMapped = Mapper::MapEntityCollection(TopicSingleDto::class, $topics);

        return $topicsMapped;
    }

    public function storeTopic($data, $id = null){
        if(isset($id)){
            $topic = $this->helpCenterRepository->getTopic($id)->first();
            $topic->setId($id);
        }else{
            $topic = new Topic();
        }

        if(isset($data->title)){
            $topic->setTitle($data->title);
        }elseif(!isset($id)){
            throw new BadRequestException(trans('locale.topic_title_required'));
        }

        if(isset($data->description)){
            $topic->setDescription($data->description);
        }/*elseif(!isset($id)){
            throw new BadRequestException(trans('local.topic_description_required'));
        }*/

        if(isset($data->iconUrl)){
            $topic->setIconUrl($data->iconUrl);
        }
        $currentTopics = $this->getAllTopicsAdmin(null,null,null);
        $lastOrder = 0;
        foreach($currentTopics as $topicInst){
            if($topicInst->order > $lastOrder)
                $lastOrder = $topicInst->order;
        }
        
        $topic->topic_order = $lastOrder+1;
        
        $topic = $this->helpCenterRepository->storeTopic($topic);
        $topicMapped = Mapper::MapClass(TopicSingleDto::class, $topic);
        return $topicMapped;
    }

    public function deleteTopic($id){
        $topic = $this->helpCenterRepository->getTopic($id)->first();

        if(isset($topic)){
            return $this->helpCenterRepository->deleteTopic($topic);
        }

        throw new BadRequestException(trans("locale.topic_doesn't_exist"));
    }

    public function getTopicsCount($search){
        $topicsCount = $this->helpCenterRepository->getTopicsCount($search);

        return ['topicsCount' => $topicsCount];
    }

    public function uploadTopicIcon($imageData){
        $amazon_url = $this->helpCenterRepository->uploadTopicIcon($imageData);

        return ['amazon_url' => $amazon_url];
    }

    public function rearrangeTopics($topics,$user,$rearrange){
        $user = $this->accountRepository->getUserById($user->id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$user->isSuperAdmin())
            throw new BadRequestException(trans("locale.no_permission"));

        $currentTopics = $this->getAllTopicsAdmin(null,null,null);

        if(!$rearrange){
            return $currentTopics;
        }

        $currentTopicsCount = count($currentTopics);

        if(count($topics) != $currentTopicsCount)
            throw new BadRequestException(trans("locale.you_must_submit_all_topics"));

        $topicsRearrange = $this->helpCenterRepository->rearrangeTopics($topics);
           
        return $topicsRearrange;
    }

}
