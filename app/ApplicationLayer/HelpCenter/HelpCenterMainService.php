<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:44 AM
 */

namespace App\ApplicationLayer\HelpCenter;


use App\ApplicationLayer\HelpCenter\Interfaces\IHelpCenterMainService;
use App\DomainModelLayer\HelpCenter\Repositories\IHelpCenterMainRepository;
use App\DomainModelLayer\Journeys\Tag;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;

class HelpCenterMainService implements IHelpCenterMainService{

    private $helpCenterRepository;
    private $professionalRepository;
    private $topicService;
    private $articleService;
    private $tagService;

    public function __construct(IHelpCenterMainRepository $helpCenterMainRepository, IProfessionalMainRepository $professionalMainRepository,IAccountMainRepository $accountRepository){
        $this->helpCenterRepository = $helpCenterMainRepository;
        $this->professionalRepository = $professionalMainRepository;
        $this->accountRepository = $accountRepository;
        $this->topicService = new TopicService($this->helpCenterRepository,$this->accountRepository);
        $this->articleService = new ArticleService($this->helpCenterRepository, $this->professionalRepository,$this->accountRepository);
        $this->tagService = new TagService($this->helpCenterRepository);
    }

//    =============== TOPICS ===============
    public function getAllTopics($details, $start_from, $limit,$search=null){
        return $this->topicService->getAllTopics($details, $start_from, $limit,$search);
    }

    public function getAllTopicsCount(){
        return $this->topicService->getAllTopicsCount();
    }

    public function getTopic($id, $start_from, $limit){
        return $this->topicService->getTopic($id, $start_from, $limit);
    }
    
    public function getTopicArticlesCount($id){
        return $this->topicService->getTopicArticlesCount($id);
    }

    public function getAllTopicsAdmin($start_from, $limit,$search){
        return $this->topicService->getAllTopicsAdmin($start_from, $limit,$search);
    }

    public function storeTopic($data){
        return $this->topicService->storeTopic($data);
    }

    public function updateTopic($data, $id){
        return $this->topicService->storeTopic($data, $id);
    }

    public function deleteTopic($id){
        return $this->topicService->deleteTopic($id);
    }

    public function getTopicsCount($search){
        return $this->topicService->getTopicsCount($search);
    }

    public function uploadTopicIcon($imageData){
        return $this->topicService->uploadTopicIcon($imageData);
    }

    public function rearrangeTopics($topics,$user,$rearrange){
        return $this->topicService->rearrangeTopics($topics,$user,$rearrange);
    }

//  =============================================


//    =============== ARTICLES ===============
    public function getArticle($id){
        return $this->articleService->getArticle($id);
    }

    public function getTopicArticle($topic_name){
        return $this->articleService->getTopicArticle($topic_name);
    }

    public function getArticles($ids){
        return $this->articleService->getArticles($ids);
    }

    public function searchForArticle($keyword, $start_from, $limit){
        return $this->articleService->searchForArticle($keyword, $start_from, $limit);
    }

      public function searchForCountArticle($keyword){
        return $this->articleService->searchForCountArticle($keyword);
    }

    public function getAllArticlesAdmin($start_from, $limit,$search){
        return $this->articleService->getAllArticlesAdmin($start_from, $limit,$search);
    }

    public function getArticleTags($id){
        return $this->articleService->getArticleTags($id);
    }

    public function storeArticle($data){
        return $this->articleService->storeArticle($data);
    }

    public function updateArticle($data, $id){
        return $this->articleService->storeArticle($data, $id);
    }

    public function deleteArticle($id){
        return $this->articleService->deleteArticle($id);
    }

    public function getArticlesCount($search){
        return $this->articleService->getArticlesCount($search);
    }

    public function uploadArticleIcon($imageData){
        return $this->articleService->uploadArticleIcon($imageData);
    }

    public function rearrangeArticles($topic_id,$user,$articles,$rearrange){
        return $this->articleService->rearrangeArticles($topic_id,$user,$articles,$rearrange);

    }

//  =============================================



//    =============== TAGS ===============
    public function getAllTags($start_from , $limit){
        return $this->tagService->getAllTags($start_from, $limit);
    }

    public function getTag($id, $start_from, $limit){
        return $this->tagService->getTag($id, $start_from, $limit);
    } 
    
    public function getTagArticlesCount($id){
        return $this->tagService->getTagArticlesCount($id);
    }

    public function getAllTagsAdmin($start_from, $limit,$search){
        return $this->tagService->getAllTagsAdmin($start_from, $limit,$search);
    }

    public function storeTag($data){
        return $this->tagService->storeTag($data);
    }

    public function updateTag($data, $id){
        return $this->tagService->storeTag($data, $id);
    }

    public function deleteTag($id){
        return $this->tagService->deleteTag($id);
    }

    public function getTagsCount($search){
        return $this->tagService->getTagsCount($search);
    }
//  =============================================

}