<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 26-Feb-19
 * Time: 10:45 AM
 */

namespace App\ApplicationLayer\HelpCenter;

use App\ApplicationLayer\HelpCenter\Dtos\ArticleDto;
use App\ApplicationLayer\HelpCenter\Dtos\ArticlePreviewDto;
use App\ApplicationLayer\HelpCenter\Dtos\ArticleReducedDto;
use App\ApplicationLayer\HelpCenter\Dtos\TagDto;
use App\ApplicationLayer\HelpCenter\Dtos\TagReducedDto;
use App\ApplicationLayer\HelpCenter\Dtos\TagSingleDto;
use App\ApplicationLayer\HelpCenter\Dtos\TopicReducedDto;
use App\DomainModelLayer\HelpCenter\Repositories\IHelpCenterMainRepository;
use App\DomainModelLayer\HelpCenter\Tag;
use App\Framework\Exceptions\BadRequestException;
use Illuminate\Pagination\Paginator;
use App\Helpers\Mapper;


class TagService{
    private $helpCenterRepository;

    public function __construct(IHelpCenterMainRepository $helpCenterMainRepository){
        $this->helpCenterRepository = $helpCenterMainRepository;
    }

    public function getAllTags($start_from, $limit){
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $tags = $this->helpCenterRepository->getAllTags($limit,$search=null);
        $tagsMapped = Mapper::MapEntityCollection(TagSingleDto::class, $tags);

        return $tagsMapped;
    }

    public function getTag($id, $start_from, $limit){
        // Get tag main data
        $tag = $this->helpCenterRepository->getTag($id)->first();

        $tagMapped = Mapper::MapClass(TagDto::class, $tag, [ArticleReducedDto::class]);

        // Associate the tag with the full data of its articles.
        $tagArticlesIds = array_map(function($item){
            return $item->id;
        }, $tagMapped->articles);

        
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $tagArticles = $this->helpCenterRepository->getArticles($tagArticlesIds, $limit);;
        $tagArticlesMapped = Mapper::MapEntityCollection(ArticlePreviewDto::class, $tagArticles,
            [TopicReducedDto::class, TagReducedDto::class]);
        $tagMapped->articles = $tagArticlesMapped;
        // $tagMapped[0]->articles = $tagArticlesMapped;

        return $tagMapped;
    }

     public function getTagArticlesCount($id){
         $tags = $this->helpCenterRepository->getTag($id)->first();
        // dd($tags);
        if($tags != null){
       return $tags->getArticlesCount();
       }else{
        return 0;
       }

   
         
     }
    
    public function getAllTagsAdmin($start_from, $limit,$search){
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $tags = $this->helpCenterRepository->getAllTags($limit,$search);
        $tagsMapped = Mapper::MapEntityCollection(TagSingleDto::class, $tags);

        return $tagsMapped;
    }

    public function storeTag($data, $id = null){
        if(isset($id)){
            $tag = $this->helpCenterRepository->getTag($id)->first();
            $tag->setId($id);
        }else{
            $tag = new Tag();
        }

        if(isset($data->name)){
            $tag->setName($data->name);
        }elseif(!isset($id)){
            throw new BadRequestException(trans('locale.tag_name_required'));
        }

        if(isset($data->iconUrl)){
            $tag->setIconUrl($data->iconUrl);
        }

        $tag = $this->helpCenterRepository->storeTag($tag);
        $tagMapped = Mapper::MapClass(TagSingleDto::class, $tag);
        return $tagMapped;
    }

    public function deleteTag($id){
        $tag = $this->helpCenterRepository->getTag($id)->first();

        if(isset($tag)){
            return $this->helpCenterRepository->deleteTag($tag);
        }

        throw new BadRequestException(trans("locale.tag_doesn't_exist"));
    }

    public function getTagsCount($search){
        $tagsCount = $this->helpCenterRepository->getTagsCount($search);

        return ['tagsCount' => $tagsCount];
    }
}