<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 25-Feb-19
 * Time: 10:47 AM
 */

namespace App\ApplicationLayer\HelpCenter\Interfaces;


interface IHelpCenterMainService{
    public function getAllTopics($details, $start_from, $limit,$search=null);
    public function getAllTopicsCount();
    public function getTopic($id, $start_from, $limit);
    public function getAllTopicsAdmin($start_from, $limit,$search);
    public function getTopicArticlesCount($id);
    public function storeTopic($data);
    public function updateTopic($data, $id);
    public function deleteTopic($id);
    public function getTopicsCount($search);
    public function uploadTopicIcon($imageData);
    public function rearrangeTopics($topics,$user,$rearrange);


    public function getArticle($id);
    public function getTopicArticle($topic_name);
    public function getArticles($ids);
    public function searchForArticle($keyword, $start_from, $limit);
    public function searchForCountArticle($keyword);
    public function getAllArticlesAdmin($start_from, $limit,$search);
    public function getArticleTags($id);
    public function storeArticle($data);
    public function updateArticle($data, $id);
    public function deleteArticle($id);
    public function getArticlesCount($search);
    public function uploadArticleIcon($imageData);
    public function rearrangeArticles($topic_id,$user,$articles,$rearrange);

    public function getAllTags($start_from, $limit);
    public function getTag($id, $start_from, $limit);
    public function getTagArticlesCount($id);
    public function getAllTagsAdmin($start_from, $limit,$search);
    public function storeTag($data);
    public function updateTag($data, $id);
    public function deleteTag($id);
    public function getTagsCount($search);

}