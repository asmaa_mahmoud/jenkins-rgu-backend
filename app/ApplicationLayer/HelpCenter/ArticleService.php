<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 26-Feb-19
 * Time: 10:44 AM
 */

namespace App\ApplicationLayer\HelpCenter;

use App\ApplicationLayer\HelpCenter\Dtos\ArticleDto;
use App\ApplicationLayer\HelpCenter\Dtos\ArticlePreviewDto;
use App\ApplicationLayer\HelpCenter\Dtos\ArticleSingleDto;
use App\ApplicationLayer\HelpCenter\Dtos\TagReducedDto;
use App\ApplicationLayer\HelpCenter\Dtos\TagSingleDto;
use App\ApplicationLayer\HelpCenter\Dtos\TopicReducedDto;
use App\DomainModelLayer\HelpCenter\Article;
use App\DomainModelLayer\HelpCenter\Repositories\IHelpCenterMainRepository;
use App\DomainModelLayer\Accounts\Repositories\IAccountMainRepository;
use App\DomainModelLayer\Professional\Repositories\IProfessionalMainRepository;
use App\Framework\Exceptions\BadRequestException;
use App\Helpers\Mapper;
use Illuminate\Pagination\Paginator;

class ArticleService{
    private $helpCenterRepository;
    private $professionalRepository;

    public function __construct(IHelpCenterMainRepository $helpCenterMainRepository,
                                IProfessionalMainRepository $professionalMainRepository,IAccountMainRepository $accountRepository){
        $this->helpCenterRepository = $helpCenterMainRepository;
        $this->professionalRepository = $professionalMainRepository;
        $this->accountRepository = $accountRepository;

    }

    public function getArticle($id){
        $article = $this->helpCenterRepository->getArticle($id);
        $articleMapped = Mapper::MapEntityCollection(ArticleDto::class, $article,
            [TopicReducedDto::class, TagReducedDto::class]);

        return $articleMapped;
    }


//    public function  getArticlee($id)
//    {
//        $article = $this->helpCenterRepository->getArticle($id);
//        $articleMap = Mapper::MapEntityCollection(ArticleDto::class,$article, [TopicReducedDto::class ,TagReducedDto::class]);
//        return $articleMap ;
//
//    }

    public function getTopicArticle($topic_name){
        
        $topicArticle = $this->helpCenterRepository->getTopicArticle($topic_name);
        $topicTitle = $this->helpCenterRepository->getTopicWithTitle($topic_name);
        $topicArticleMapped=[];
        if($topicArticle){
            $topicArticleMapped['articleId']= $topicArticle->getId();
            $topicArticleMapped['topicId']= $topicTitle->getId();
            $topicArticleMapped['title']= $topicArticle->getTitle();
            $topicArticleMapped['body']= $topicArticle->getBody();
            $topicArticleMapped['summary']= $topicArticle->getSummary();

        }

        return $topicArticleMapped;
    }

    public function getArticles($ids){
        $articles = $this->helpCenterRepository->getArticles($ids);
        $articlesMapped = Mapper::MapEntityCollection(ArticleDto::class, $articles,
            [TopicReducedDto::class, TagReducedDto::class]);

        return $articlesMapped;
    }



    public function getArticlesByTopicId($id){
        $articles = $this->helpCenterRepository->getArticles($id);
        $articlesMapped = Mapper::MapEntityCollection(ArticleDto::class, $articles,
            [TopicReducedDto::class, TagReducedDto::class]);

        return $articlesMapped;
    }

    public function searchForArticle($keyword, $start_from, $limit){
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        $count=false;
        $articles = $this->helpCenterRepository->getAllArticles($limit,$keyword,$count);

        $articlesMapped = Mapper::MapEntityCollection(ArticlePreviewDto::class, $articles,
            [TopicReducedDto::class, TagReducedDto::class]);

//        $searchResult = [];
//        $lowerCaseKeyword =  strtolower($keyword);
//        foreach ($articlesMapped as $article){
//            $lowerCaseTitle =  strtolower($article->title);
//            if(strpos($lowerCaseTitle, $lowerCaseKeyword) !== false){
//                array_push($searchResult, $article);
//            }else{
//                $match = false;
//                foreach ($article->tags as $tag){
//                    $lowerCaseTag = strtolower($tag->name);
//                    if($lowerCaseTag == $lowerCaseKeyword){
//                        array_push($searchResult, $article);
//                        $match = true;
//                        break;
//                    }
//                }
//                if(!$match){
//                    foreach ($article->topics as $topic){
//                        $lowerCaseTopic = strtolower($topic->title);
//                        if(strpos($lowerCaseTopic, $lowerCaseKeyword) !== false){
//                            array_push($searchResult, $article);
//                            break;
//                        }
//                    }
//                }
//            }
//        }

//        dd($searchResult);

//        return $searchResult;
        return $articlesMapped;
    }

    public function searchForCountArticle($search){
        $count=false;
        $limit=null;
         $articlesCount = $this->helpCenterRepository->getAllArticles($limit,$search,$count);
         return ['articlesCount' => $articlesCount];
    }

    
    public function getAllArticlesAdmin($start_from, $limit,$search){
        if($limit != null) {
            $page = intval($start_from / $limit) + 1;
            $currentPage = $page;
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
         $articles = $this->helpCenterRepository->getAllArticles($limit,$search);
        $articlesMapped = Mapper::MapEntityCollection(ArticleDto::class, $articles,[TopicReducedDto::class, TagReducedDto::class]);

        return $articlesMapped;
    }

    public function getArticleTags($id){
        $article = $this->helpCenterRepository->getArticle($id)->first();
        $articleMapped = Mapper::MapClass(ArticleDto::class, $article,
            [TopicReducedDto::class, TagReducedDto::class]);

        $articleTagsIds = array_map(function($item){
            return $item->id;
        }, $articleMapped->tags);

        $articleTags = $this->helpCenterRepository->getTags($articleTagsIds);
        $articleTagsMapped = Mapper::MapEntityCollection(TagSingleDto::class, $articleTags);

        return $articleTagsMapped;

//        return $articleMapped->tags;
    }

    public function storeArticle($data, $id = null){
       
        if(isset($id)){
            $article = $this->helpCenterRepository->getArticle($id)->first();
            $article->setId($id);
        }else{
            $article = new Article();
        }

        if(isset($data->title)){
            $article->setTitle($data->title);
        }elseif(!isset($id)){
            throw new BadRequestException(trans('locale.article_title_required'));
        }

        if(isset($data->body)){
            $article->setBody($data->body);
        }elseif(!isset($id)){
            throw new BadRequestException(trans('locale.article_body_required'));
        }

        if(isset($data->iconUrl)){
            $article->setIconUrl($data->iconUrl);
        }

        if(isset($data->summary)){
            $article->setSummary($data->summary);
        }

        if(isset($data->isRecommended)){
            $article->setIsRecommended($data->isRecommended);
        }

        if(isset($data->nLikes)){
            $article->setNLikes($data->nLikes);
        }

        if(isset($data->nDislikes)){
            $article->setNDislikes($data->nDislikes);
        }
        if(isset($data->topicsIds)){
            $topicsIds = $data->topicsIds;
            
            if($id){
                //Check current assigened-to topics
                foreach($article->topics as $topic){
                    if (($key = array_search($topic->id, $topicsIds)) !== false) {
                        unset($topicsIds[$key]);
                    }
                }
            }    
            
            
            $topics = $this->helpCenterRepository->getTopics($topicsIds);
            $article->setTopics($topics);
        }
        if(!empty($data->tagsIds)){
            foreach($data->tagsIds as $tagId){
                $tag = $this->helpCenterRepository->getTag($tagId);
                if($tag){
                    $article->setTags($tag);
                }
            }
        }

        if(isset($data->campusesIds)){
            $campusesIds = json_decode($data->campusesIds);
            if(count($campusesIds) > 0){
                $campuses = $this->professionalRepository->getAllCampuses($campusesIds);
                $article->setCampuses($campuses);
            }
        }

        $article = $this->helpCenterRepository->storeArticle($article);
        $articleMapped = Mapper::MapClass(ArticleDto::class, $article, [TopicReducedDto::class, TagReducedDto::class]);
        $articleMapped->campuses = [];
        foreach ($article->getCampuses() as $campus){
            array_push($articleMapped->campuses, (object)[
                'id' => $campus->getId(),
                'name' =>$campus->getName()
            ]);
        }
        
        // In case of new create
        if($id == null){
            foreach($topicsIds as $topicId){
                $articlesCount = count($this->helpCenterRepository->getTopicArticles($topicId));
                $this->helpCenterRepository->setArticleOrder($article->id,$topicId,$articlesCount+1);
            }
        }

        return $articleMapped;
    }

    public function deleteArticle($id){
        $article = $this->helpCenterRepository->getArticle($id)->first();

        if(isset($article)){
            return $this->helpCenterRepository->deleteArticle($article);
        }

        throw new BadRequestException(trans("locale.article_doesn't_exist"));
    }

    public function getArticlesCount($search){
        $articlesCount = $this->helpCenterRepository->getArticlesCount($search);

        return ['articlesCount' => $articlesCount];
    }

    public function uploadArticleIcon($imageData){
        $amazon_url = $this->helpCenterRepository->uploadArticleIcon($imageData);

        return ['amazon_url' => $amazon_url];
    }

    public function rearrangeArticles($topic_id,$user,$givenArticles,$rearrange){
        $user = $this->accountRepository->getUserById($user->id);
        if($user == null)
            throw new BadRequestException(trans('locale.user_not_exist'));

        if(!$user->isSuperAdmin())
            throw new BadRequestException(trans("locale.no_permission"));

        $articles = [];
        $currentTopicArticles =  $this->helpCenterRepository->getTopicArticles($topic_id);
        $order = 1;
        foreach($currentTopicArticles as $id=>$article){
            array_push($articles,['id'=>$id,'title'=>$article,'order'=>$order]);
            $order++;
        }

        if(!$rearrange){
            return $articles;
        }else{
            $currentTopicArticlesCount = count($currentTopicArticles);
            
            if(count($givenArticles) != $currentTopicArticlesCount)
                throw new BadRequestException(trans("locale.you_must_submit_all_articles"));

            $articlesRearrange = $this->helpCenterRepository->rearrangeArticles($givenArticles,$topic_id);
            
            return $articlesRearrange;
        }
    }



}