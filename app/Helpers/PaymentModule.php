<?php

namespace App\Helpers;

use App\Helpers\StripePayment;
use Config;

class PaymentModule
{

  //region Properties
    private $payment_engine;
    
    //endregion

    //region Constructor
    public function __construct(){
        $this->payment_engine = config('services.payment.engine');
    }
    //endregion

  public function createPlan($name,$interval,$interval_count,$amount,$currency)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::createPlan($name,$interval,$interval_count,$amount,$currency);
  }

  public function createCustomer($token = null)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::createCustomer($token);
  }

  public function updateCard($customer_id,$token)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::updateCard($customer_id,$token);
  }

  public function subscribe($stripe_plan,$customer_id,$trial,$country = null,$noOfChildren = 0,$childPlan = null,$coupon_id = null)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::subscribe($stripe_plan,$customer_id,$trial,$country,$noOfChildren,$childPlan,$coupon_id);
  }

  public function unsubscribe($subscription_id)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::unsubscribe($subscription_id);
  }

  public function retrieveSubscription($subscription_id)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::retrieveSubscription($subscription_id);
  }

  public function unsubscribeAtPeriodEnd($subscription_id)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::unsubscribeAtPeriodEnd($subscription_id);
  }

  public function changePlan($subscription_id,$new_plan_name)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::changePlan($subscription_id,$new_plan_name);
  }

  public function addSubscriptionItem($subscription_id,$plan_name,$end_trial = true,$quantity = 1)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::addSubscriptionItem($subscription_id,$plan_name,$end_trial,$quantity);
  }

  public function changeSubscriptionItemQuantity($stripe_id,$new_quantity = 1)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::changeSubscriptionItemQuantity($stripe_id,$new_quantity);
  }

  // Needs Database Modifications
  public function removeSubscriptionItem($item_id,$prorate = true)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::removeSubscriptionItem($item_id,$prorate);
  }

  public function retrieveCustomerInvoices($customer_id)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::retrieveCustomerInvoices($customer_id);
  }

  public function retrieveInvoiceDetails($invoice_id)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::retrieveInvoiceDetails($invoice_id);
  }

  public function getCustomerInfo($customerId)
  {
    if($this->payment_engine == 'stripe')
      return StripePayment::getCustomerInfo($customerId);
  }

  public function changeCountryForSubscription($subscription_id,$country){
    if($this->payment_engine == 'stripe')
      return StripePayment::changeCountryForSubscription($subscription_id,$country);
  }

  public function addCharge($amount, $customer_id, $description){
      if($this->payment_engine == 'stripe')
          return StripePayment::addCharge($amount, $customer_id, $description);
  }

  public function retrieveCustomerCharges($customer_id){
      if($this->payment_engine == 'stripe')
          return StripePayment::retrieveCustomerCharges($customer_id);
  }

  public function retrieveChargeDetails($chargeId){
      if($this->payment_engine == 'stripe')
          return StripePayment::retrieveChargeDetails($chargeId);
  }

}