<?php 
namespace App\Helpers;

//use Illuminate\Support\Facades\Session;

class HttpMethods {

    public static $key = 'eyJpdiI6IlwvMkhkSWNPTzhnOGlzbUVsNkpXTlB3PT0iLCJ2YWx1ZSI6InZJNUhOTE1VUEJ5RXR3ZmJtQ05lcjN3YkRZaGR5K1hBTERCcloySVhrd3c9IiwibWFjIjoiODQwM2NhZTY3NGU5NGEzMjQxZWNkOWZlMWU2ZDBkYzExY2EyM2FmN2RiY2ViZDM2OTFkNWVmZDc4ZjJhOTI2MSJ9' ;

    public static function  post($url,$fields){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        $temp = HttpMethods::$key;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth:$temp"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $body = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' =>$body , 'info'=> $info];
        curl_close ($ch);
        return $server_output;
    }

    public static function  get($url){
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $temp = HttpMethods::$key;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth:$temp"));

        $body = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' =>$body , 'info'=> $info];
        curl_close ($ch);
        return $server_output;
    }

    public static function  put($url,$fields=[]){
        $ch = curl_init();

        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        $temp = HttpMethods::$key;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth:$temp"));

        $body = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' =>$body , 'info'=> $info];
        curl_close ($ch);
        return $server_output;
    }

    public static function del($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $temp = HttpMethods::$key;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth:$temp"));
        $body = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' =>$body , 'info'=> $info];
        curl_close($ch);

        return $server_output;
    }

    public static function getWithParams($url,$fields)
    {
        $ch = curl_init();
        $fieldString = '?'.http_build_query($fields);
        $url = $url.$fieldString;
        //return $url;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,  CURLOPT_CUSTOMREQUEST ,"GET");

        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        // if (Session::has('Authorization')) {
        //     $temp = Session::get('Authorization');
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:$temp"));
        // }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $server_output = ['body' => $body, 'info' => $info];
        curl_close($ch);
        return $server_output;
    }
}