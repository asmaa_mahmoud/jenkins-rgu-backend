<?php

namespace App\Helpers;

use App\Framework\Exceptions\BadRequestException;
use Config;
use Stripe;

class StripePayment
{

  public  static function getTaxCountries()
  {
    return ['canada'];
  }

  public static function getTax(){
    return 5;
  }


  private static function setApiKey()
  {
    $api_key = config('services.stripe.key');
    \Stripe\Stripe::setApiKey($api_key);
  }

  private static function validateResponse($object)
  {
    if($object == null || !is_object($object))
            throw new BadRequestException('Error creating object on stripe: '.$object);
    if(!isset($object->id))
        throw new BadRequestException('Error getting id of object: '.$object);
  }

  public static function createPlan($name,$interval,$interval_count,$amount,$currency)
  {
    StripePayment::setApiKey();

    $plan = \Stripe\Plan::create(array(
              "amount" => $amount,
              "interval" => $interval,
              "name" => $name,
              "currency" => $currency,
              "id" => $name,
              "interval_count"=>$interval_count)
            );
    StripePayment::validateResponse($plan);

    return $plan;
  }

  public static function createCustomer($token = null)
  {
    StripePayment::setApiKey();
    if($token == null){
        $customer = \Stripe\Customer::create(array(
            "description" => "New Customer"
        ));
    }
    else{
        $customer = \Stripe\Customer::create(array(
            "description" => "New Customer",
            "source" => $token
        ));
    }
    
    StripePayment::validateResponse($customer);
    return $customer;
  }


  public static function getCustomerInfo($customerId){
      StripePayment::setApiKey();
      $customer = \Stripe\Customer::retrieve($customerId);
      StripePayment::validateResponse($customer);
      return $customer;
  }

  public static function updateCard($customer_id,$token)
  {
    StripePayment::setApiKey();

    $customer = \Stripe\Customer::retrieve($customer_id);
    StripePayment::validateResponse($customer);

    $customer->source = $token;
    $customer->save();
    return $customer;
  }

  public static function subscribe($stripe_plan,$customer_id,$trial,$country = null,$noOfChildren = 0,$childPlan = null,$coupon_id = null)
  {
    StripePayment::setApiKey(); 
    $tax = null;
    $customerArray['customer'] = $customer_id;
    if($coupon_id != null)
        $customerArray['coupon'] = $coupon_id;
    if($country != null)
    {
      if(in_array(strtolower($country),StripePayment::getTaxCountries()))
        $tax = StripePayment::getTax();
    }   
    if($trial)
    {
        if($noOfChildren > 0){
            $customerArray['tax_percent'] = $tax;
            $customerArray['items'] = [
                ["plan" => $stripe_plan],
                ["plan" =>$childPlan,
                    "quantity"=>$noOfChildren]
            ];
            $stripe_subscription = \Stripe\Subscription::create($customerArray);
        }
        else{
            $customerArray['tax_percent'] = $tax;
            $customerArray['plan'] = $stripe_plan;
            $stripe_subscription = \Stripe\Subscription::create($customerArray);
        }

    }

    else{
        if($noOfChildren > 0){
            $customerArray['tax_percent'] = $tax;
            $customerArray['trial_end'] = "now";
            $customerArray['items'] = [
                ["plan" => $stripe_plan],
                ["plan" =>$childPlan,
                    "quantity"=>$noOfChildren]
            ];
            $stripe_subscription = \Stripe\Subscription::create($customerArray);
        }
        else{
            $customerArray['tax_percent'] = $tax;
            $customerArray['plan'] = $stripe_plan;
            $customerArray['trial_end'] = "now";
            $stripe_subscription = \Stripe\Subscription::create($customerArray);
        }

    }
    
    StripePayment::validateResponse($stripe_subscription);
    $childItemId = null;

    if($noOfChildren > 0){
        $childItemId = $stripe_subscription->items->data[1]->id;
        return ['customer_id' => $customer_id,'subscription_id'=>$stripe_subscription->id
            ,'subscription_amount'=>$stripe_subscription->items->data[0]->plan->amount/100,'subscription_currency'=>$stripe_subscription->items->data[0]->plan->currency,
            'subscription_interval'=>$stripe_subscription->items->data[0]->plan->interval,'subscription_quantity'=>$stripe_subscription->items->data[0]->quantity,
            'stripeInvoice'=>$stripe_subscription,'childItemId'=>$childItemId];
    }
    else{
        return ['customer_id' => $customer_id,'subscription_id'=>$stripe_subscription->id
            ,'subscription_amount'=>$stripe_subscription->plan->amount/100,'subscription_currency'=>$stripe_subscription->plan->currency,
            'subscription_interval'=>$stripe_subscription->plan->interval,'subscription_quantity'=>$stripe_subscription->quantity,
            'stripeInvoice'=>$stripe_subscription];
    }

  }


  public static function changeCountryForSubscription($subscription_id,$country){

      StripePayment::setApiKey();
      $subscription = \Stripe\Subscription::retrieve($subscription_id);
      StripePayment::validateResponse($subscription);

      $tax = 0;
      if(in_array(strtolower($country),StripePayment::getTaxCountries()))
          $tax = StripePayment::getTax();

      $subscription->tax_percent = $tax;

      $subscription->save();
      return $subscription;
  }

  public static function unsubscribe($subscription_id)
  {
    StripePayment::setApiKey();

    $subscription = \Stripe\Subscription::retrieve($subscription_id);
    StripePayment::validateResponse($subscription);

    $subscription->cancel();
    return $subscription;
  }

  public static function retrieveSubscription($subscription_id)
  {
    StripePayment::setApiKey();

    $subscription = \Stripe\Subscription::retrieve($subscription_id);
    StripePayment::validateResponse($subscription);

    return $subscription;
  }

  public static function unsubscribeAtPeriodEnd($subscription_id)
  {
    StripePayment::setApiKey();

    $subscription = \Stripe\Subscription::retrieve($subscription_id);
    StripePayment::validateResponse($subscription);

    $subscription->cancel(array('at_period_end'=>true));
    return $subscription;
  }

  public static function changePlan($subscription_id,$new_plan_name)
  {
    StripePayment::setApiKey();
    $subscription = \Stripe\Subscription::retrieve($subscription_id);
    StripePayment::validateResponse($subscription);

    $item_id = $subscription->items->data[0]->id;

    $item = \Stripe\SubscriptionItem::retrieve($item_id);
    StripePayment::validateResponse($item);

    $item->plan = $new_plan_name;
    $item->save();
    
    $subscription->prorate = true;
    $subscription->trial_end  = "now";
    $subscription->save();
    return $item;
  }

  public static function addSubscriptionItem($subscription_id,$plan_name,$end_trial = true,$quantity = 1)
  {
    StripePayment::setApiKey();
    $subscription = \Stripe\Subscription::retrieve($subscription_id);

    if($end_trial)
    {
      $subscription->trial_end  = "now";
      $subscription->save();
    }
    $subscription_item = \Stripe\SubscriptionItem::create(array(
                            "subscription" => $subscription_id,
                            "plan" => $plan_name,
                            "quantity" => $quantity,
                            "prorate" => true,
                          ));

    StripePayment::validateResponse($subscription_item);

    return $subscription_item;
  }

  public static function changeSubscriptionItemQuantity($stripe_id,$new_quantity = 1)
  {
    StripePayment::setApiKey();

    $subscription_item = \Stripe\SubscriptionItem::retrieve($stripe_id);
    StripePayment::validateResponse($subscription_item);

    $subscription_item->quantity = $new_quantity + intval($subscription_item->quantity);
    $subscription_item->save();
    return $subscription_item;
  }

  // Needs Database Modifications
  public static function removeSubscriptionItem($item_id,$prorate = true)
  {
    StripePayment::setApiKey();

    $subscription_item = \Stripe\SubscriptionItem::retrieve($item_id);
    StripePayment::validateResponse($subscription_item);

    $subscription_item->prorate = $prorate;
    $subscription_item->delete();
    return $subscription_item;
  }

  public static function retrieveCustomerInvoices($customer_id)
  {
    StripePayment::setApiKey();

    $invoices = \Stripe\Invoice::all(array("customer" => $customer_id));

    return $invoices;
  }

  public static function retrieveInvoiceDetails($invoice_id)
  {
    StripePayment::setApiKey();

    $invoice = \Stripe\Invoice::retrieve($invoice_id);
    StripePayment::validateResponse($invoice);
    
    $invoice_details = \Stripe\Invoice::retrieve($invoice_id)->lines->all(array("limit" => 100));

    return ["invoice"=>$invoice,"details"=>$invoice_details];
  }

  public static function addCharge($amount, $customer_id, $description){

      StripePayment::setApiKey();

      $charge = \Stripe\Charge::create(array(
          "amount" => ceil($amount * 100),
          "currency" => "usd",
          "description" => $description,
          "customer" => $customer_id,
      ));
      return $charge;
  }

  public static function retrieveCustomerCharges($customerId){
      StripePayment::setApiKey();
      $charges = \Stripe\Charge::all(array("customer"=>$customerId,"limit" => 100));
      return $charges;
  }

    public static function retrieveChargeDetails($chargeId){
        StripePayment::setApiKey();
        return \Stripe\Charge::retrieve($chargeId);
    }


}