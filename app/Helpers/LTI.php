<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 18/02/2019
 * Time: 5:00 PM
 */

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use IMSGlobal\LTI\ToolProvider\DataConnector\DataConnector;



class LTI
{
    protected $data_connector;

    private $toolProvider = null;

    private $toolConsumer = null;

    public function __construct()
    {
        $db = DB::connection(config('connections.mysql.database'))->getPdo();
        $this->data_connector = DataConnector::getDataConnector('', $db, 'pdo');
    }

    public function getDataConnector() {
        return $this->data_connector;
    }


}