<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 18/02/2019
 * Time: 3:54 PM
 */

namespace App\Helpers;
use App\ApplicationLayer\Professional\Interfaces\IProfessionalMainService;
use IMSGlobal\LTI\Profile;
use IMSGlobal\LTI\ToolProvider;
use IMSGlobal\LTI\ToolProvider\Service;


class RobogardenToolProvider extends ToolProvider\ToolProvider
{
    private $professionalService;
    private $round_lti_id;
    private $status;
    private $request_data;
    function __construct($data_connector,IProfessionalMainService $professionalService,$round_lti_id,$request_data=null,$status="dev") {

        parent::__construct($data_connector);
        $this->professionalService=$professionalService;
        $this->round_lti_id=$round_lti_id;
        $this->status=$status;
        $this->request_data=$request_data;

    }

    function onLaunch() {
//dd($this);
        if($this->ok==true) {
            $consumer_id = $this->consumer->getRecordId();

            if ($this->status == "dev") {
                $userDto = LTIMapper::mapLTIUser($this->user);
                $userDto->username = isset($this->request_data['ext_d2l_username']) ? $this->request_data['ext_d2l_username'] : '';
                return $this->professionalService->routeConsumerUsers($consumer_id, $userDto, $this->round_lti_id);
            }elseif ($this->status == "test2") {
                /* $userDto = LTIMapper::mapLTIUser($this->user);
                 //$userDto->username = isset($this->request_data['ext_d2l_username']) ? $this->request_data['ext_d2l_username'] : '';
                 $username = isset($this->request_data['ext_d2l_username']) ? $this->request_data['ext_d2l_username'] : '';

                 if ($output=preg_match_all('/[\'^£%&*()}{#~?><>.,|=+¬]/', $username,$matches,PREG_OFFSET_CAPTURE)) {
                     $temp=0;

                     foreach ($matches[0] as $match){
                         //dd($match[1]);
                         $username = substr_replace($username, "_", $match[1]+$temp, 1);
                         $temp++;
                     }
                 }
                 $userDto->username=$username;

                 return $this->professionalService->routeConsumerUsers2($consumer_id, $userDto, $this->round_lti_id,$this->request_data);*/
            } elseif ($this->status == "test")
                return $this->professionalService->routeD2lLti($consumer_id, $this->request_data);

        }else{

        }

    }

    function onContentItem() {
        return "onContentItem";
    }

    function onDashboard() {
        return "onDashboard";
    }

    function onRegister() {
        return "onRegister";
    }

    function onError() {
        return "onError";
    }

}