<?php
/**
 * Created by PhpStorm.
 * User: ROBOGARDEN
 * Date: 24/02/2019
 * Time: 11:00 AM
 */

namespace App\Helpers;


use App\ApplicationLayer\Professional\Dtos\LTIUserDto;

abstract  class LTIMapper
{

    public static function mapLTIUser($source){

        $result = new LTIUserDto();
        $roles = array();
        $result->user_lti_id=isset($source->ltiUserId)?$source->ltiUserId:$source->user_id;
        $result->fname=isset($source->firstname)?$source->firstname:$source->lis_person_name_given;
        $result->lname=isset($source->lastname)?$source->lastname:$source->lis_person_name_family;
        $result->fullName=isset($source->fullname)?$source->fullname:$source->lis_person_name_full;
        $result->email=isset($source->email)?$source->email:$source->lis_person_contact_email_primary;
        $result->image=$source->image;
        $result->image_link=$source->image;

        if(is_array($source->roles)) {
            foreach ($source->roles as $ltiRole) {
                $roleArr = explode("/", $ltiRole);
                $system_role = end($roleArr);

                if (!in_array($system_role, $roles))
                    $roles[] = $system_role;
            }
        }else{
            $source_roles=explode(",",$source->roles);
            foreach ($source_roles as $ltiRole) {
                $roleArr = explode("/", $ltiRole);
                $system_role = end($roleArr);

                if (!in_array($system_role, $roles))
                    $roles[] = $system_role;
            }
        }

        $result->roles=$roles;
        return $result;

    }


}