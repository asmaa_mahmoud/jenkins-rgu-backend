<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('webhook:start', function () {
    $app_url = Config('app.url');
    $bbb_url = Config('app.bbb_url');
    $bbb_secret = Config('app.bbb_secret');
    $data = 'hooks/createcallbackURL=' . $app_url . '/professional/room/webhook' . $bbb_secret;
    $checksum = hash('sha1', $data);
    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL,$bbb_url . 'api/hooks/create?callbackURL='. $app_url. '/professional/room/webhook&checksum='. $checksum);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    $output=curl_exec($ch);

    #Log::debug($output);

    curl_close($ch);
})->describe('BBB webhook start');