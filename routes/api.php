<?php 

use Illuminate\Http\Request;
use Carbon\Carbon;
use Mailgun\Mailgun;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/', function () {
    return "RGU Serverless First Test";
});


// Route::group(
// [
//     'prefix' => LaravelLocalization::setLocale(),
//     'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
// ],

Route::post('inserthtmljourney','Controller@insertHtmlJourney');
Route::get('/updateGoldenTime','Controller@updateGoldenTimeForAllMission');
Route::get('/removerobopal','Controller@removeRoboPalItems');
Route::get('/migrateschools','Controller@schoolMigration');
Route::post('/calculateCourseLessonsTime','Professional\CampusController@calculateCourseLessonsTime');
Route::post('/calculateScore','Professional\CampusController@calculateScore');
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale()
    ],
    function()
    {

        Route::post('pusher/auth',function(){
            $pusher=new Pusher(env('PUSHER_KEY'),env('PUSHER_SECRET'),env('PUSHER_APP_ID'));
            $socket_id=$_POST['socket_id'];
            $channel_name=$_POST['channel_name'];
            $auth=$pusher->socket_auth($channel_name,$socket_id);
            return $auth;
        });

        Route::post('register/mobile', 'Accounts\UserController@registerMobileUser');
        Route::get('free/activities', 'Journeys\ActivityController@getFreeActivitiesAndJourneys');
        Route::get('free/hour_of_code', 'Journeys\FreeJourneyController@getFreeRoboClubMissions');
        // guest
        Route::get('guest/data', 'Accounts\GuestController@getGuestData');
        Route::post('guest/data', 'Accounts\GuestController@saveGuestData');
        Route::post('update/user/score', 'Accounts\UserController@saveGuestRegisteredScore');

        Route::get('/python/script', 'Controller@pythonScript');

        Route::group(['prefix' => 'world'], function (){
            Route::get('/featured/match', 'Accounts\WorldCupController@getFeaturedMatch');
            Route::get('/matches', 'Accounts\WorldCupController@getMatchesPerDay');
            Route::get('/submissions', 'Accounts\WorldCupController@getNoSubmissionInMatch');
            Route::get('/leaderboard', 'Accounts\WorldCupController@getLeaderBoardData');
            Route::get('/leaderboard/count', 'Accounts\WorldCupController@countLeaderBoardData');
            Route::get('/user/leaderboard', 'Accounts\WorldCupController@getUserInLeaderBoard');
            Route::get('/parameters', 'Accounts\WorldCupController@getAllParameters');
            Route::get('/user/prediction', 'Accounts\WorldCupController@getUserPrediction');
            Route::get('/user', 'Accounts\WorldCupController@getWorldUserData');
            Route::post('/user/update', 'Accounts\WorldCupController@updateWorldUserData');
            Route::post('/predict', 'Accounts\WorldCupController@predictWorldUser');
            Route::get('/update/user/score', 'Accounts\WorldCupController@updateUsersScore');
            Route::get('/share', 'Accounts\WorldCupController@getSharedPrediction');
            Route::get('/user/share', 'Accounts\WorldCupController@getUserSharedPredictionData');
        });

        Route::group(['prefix' => 'register'], function (){
            Route::post('/homeschooler', 'Accounts\UserController@homeSchoolerRegister');
            Route::post('/learner', 'Accounts\UserController@learnerRegister');
            Route::post('/school', 'Schools\AdminController@schoolRegister');
            Route::post('/superadmin', 'Accounts\UserController@superAdminRegister');
        });

        Route::get('countries/users','Accounts\UserController@getCountriesUsersCount');
        Route::post('coupon/user/register','Accounts\UserController@addUserCoupon');

        Route::post('reportbug','Accounts\UserController@reportBug');
        // ================================= Textual Missions ================================
        Route::get('mission/{id}/mcq_questions/{user_id?}', 'Journeys\MissionController@getMissionMcqQuestions');
        Route::get('mission/{id}/text_questions/{user_id?}', 'Journeys\MissionController@getMissionTextQuestions');
        Route::post('missionMcq/{id}/submit/{journey_id}/{adventure_id}','Journeys\MissionController@submitMcqMission');
        Route::post('missionMcq/{id}/activity/submit/{activity_id}','Journeys\MissionController@submitActivityMcqMission');


        Route::get('user/data', 'Accounts\UserController@getUserData');

        // ================================= NEW ROUTES ==================================
        //Route::get('journeys/{GradeNumber}','Controller@getJourneysByGradeNumber');
        Route::get('journeys/grades/data','Controller@getJourneysByGrades');
        Route::get('journeys/{id}/data','Journeys\JourneysController@getJourneyData');
        Route::get('journey/{journey_id}/adventures','Journeys\JourneysController@getJourneyAdventures');
        Route::resource('login', 'Accounts\LoginController');
        Route::resource('family','Accounts\FamilyController');
        Route::get('user/havecreditcard','Accounts\UserController@checkIfHaveCreditCard');
        Route::get('user/nochildren','Accounts\UserController@getNumberOfChildren');
        Route::get('user/scores','Accounts\UserController@getUserScores');
        // Route::get('user/scores/stats','Accounts\UserController@getUserScoresStatistics');
        Route::get('user/parent/request','Accounts\UserController@requestFromParent');
        Route::get('user/parent/request/activity','Accounts\UserController@requestFromParentActivity');
        Route::get('user/check/activation','Accounts\UserController@checkUserActivation');
        Route::resource('mission', 'Journeys\MissionController');
        Route::get('htmlmission/{html_mission_id}','Journeys\MissionController@getHtmlMission');
        Route::post('mission/data', 'Journeys\MissionController@getMissionData');
        Route::get('mission/{id}/data', 'Journeys\MissionController@getMission');
        Route::get('mission/activity/{id}/data', 'Journeys\MissionController@getMissionActivityData');
        Route::get('mission/{mission_id}/code/{journey_id}/{adventure_id}','Journeys\MissionController@loadSavedCode');
        Route::get('mission/activity/{mission_id}/code/{activity_id}','Journeys\MissionController@loadSavedCodeActivity');
        Route::get('mission/{id}/categories','Journeys\MissionController@getMissionCategories');
        Route::post('mission/code/update','Journeys\MissionController@updateSavedCode2');
        Route::post('mission/code/update/activity','Journeys\MissionController@updateSavedCode2Activity');
        Route::post('mission/code/delete','Journeys\MissionController@deleteSavedCode2');
        Route::post('mission/code/delete/activity','Journeys\MissionController@deleteSavedCode2Activity');
        Route::post('mission/authorize','Journeys\MissionController@checkMissionAuthorization');
        Route::post('mission/activity/authorize','Journeys\MissionController@checkMissionAuthorizationActivity');
        Route::post('mission/progress/update','Journeys\MissionController@updateMissionProgress2');
        Route::post('mission/activity/progress/update','Journeys\MissionController@updateActivityMissionProgress');
        Route::post('mission/html/progress/update','Journeys\MissionController@updateHTMLMissionProgress');
        Route::post('mission/activity/html/progress/update','Journeys\MissionController@updateActivityHTMLMissionProgress');
        Route::get('quiz/{id}/tutorials','Journeys\QuizController@getTutorials');
        Route::post('quiz/submit/{id}/{journey_id}/{adventure_id}', 'Journeys\QuizController@submit');
        Route::post('quiz/activity/submit/{id}/{activity_id}', 'Journeys\QuizController@submitActivityQuiz');
        Route::post('quiz/authorize','Journeys\QuizController@checkQuizAuthorization');
        Route::post('quiz/activity/authorize','Journeys\QuizController@checkQuizAuthorizationActivity');
        Route::get('quiz/confidence','Journeys\QuizController@getConfidence');
        Route::resource('quiz', 'Journeys\QuizController');
        Route::get('user/journeys','Accounts\UserController@getUserJourneys');
        Route::get('user/activities','Journeys\ActivityController@getAllActivities');
        Route::get('user/minecraft/activities','Journeys\ActivityController@getAllMinecraftActivities');
        Route::get('nonuser/activities','Journeys\ActivityController@getAllActivitiesForB2CNonUser');
        Route::post('user/purchase/activity','Accounts\UserController@purchaseActivity');
        Route::get('user/activities/count','Journeys\ActivityController@countAllActivities');
        Route::get('user/minecraft/activities/count','Journeys\ActivityController@countAllMinecraftActivities');
        Route::get('user/activity/{activity_id}','Journeys\ActivityController@getActivity');
        Route::get('user/activity/{activity_id}/tasks','Journeys\ActivityController@getActivityTasks');
        Route::post('user/unlock/activity/{activity_id}','Journeys\ActivityController@unlockActivityWithCoins');
        Route::get('user/journeys/all','Accounts\UserController@getUserJourneysAll');
        Route::post('user/journeys','Accounts\UserController@addUserJourney');
        Route::get('user/activation/{token}', 'Accounts\LoginController@userActivation');
        Route::post('user/activation/resend', 'Accounts\UserController@resendActivationMail');
        Route::post('user/parent/activate', 'Accounts\UserController@activateParent');
        Route::post('user/basicinfo','Accounts\UserController@updateBasicInfo');
        Route::post('user/password','Accounts\UserController@updatePassword');
        Route::post('user/requestcountrychange','Accounts\UserController@requestCountryChange');
        Route::post('user/image','Accounts\UserController@updateProfileImage');
        Route::get('user/basicinfo','Accounts\UserController@getBasicInfo');
        Route::get('plan/periods','Accounts\UserController@getAllPlanPeriods');
        Route::get('plan/{name}','Accounts\UserController@getPlanByName');
        Route::get('user/plans/extra/{accounttypename}','Accounts\UserController@getExtraPlans');
        Route::get('user/extras','Accounts\UserController@getCurrentExtras');
        Route::get('user/plans/{accounttypeid}', 'Accounts\UserController@getAvailablePlans');
        Route::get('user/newplans/{accounttypeid}', 'Accounts\UserController@getOrderedAvailablePlans');
        Route::get('user/plans/account/{accounttypename}','Accounts\UserController@getAvailablePlansByAccountName');
        Route::get('user/subscriptions', 'Accounts\UserController@getUserSubscriptions');
        Route::post('user/subscribe', 'Accounts\UserController@subscribe');
        Route::post('user/unsubscribe','Accounts\UserController@unsubscribe');
        Route::post('user/upgrade','Accounts\UserController@upgradePlan');
        Route::get('user/notifications','Accounts\UserController@getNotifications');
        Route::get('user/notifications/{id}/confirm','Accounts\UserController@confirmNotification');
        Route::get('user/notifications/{id}/delay','Accounts\UserController@delayNotification');
        Route::post('user/position/update','Accounts\UserController@updateUserPosition');
        Route::post('user/child','Accounts\UserController@addUserChild');
        Route::post('user/email/validate', 'Accounts\UserController@validateUserEmail');
        Route::post('user/username/validate', 'Accounts\UserController@validateUsername');
        Route::get('user/customerinfo','Accounts\UserController@customerInfo');
        Route::get('user/customerinvoices','Accounts\UserController@customerInvoices');
        Route::get('user/maxchildren','Accounts\UserController@getMaxChildren');
        Route::get('user/customerinvoices/{id}','Accounts\UserController@getCustomerInvoiceById');
        Route::get('user/customerinvoices/{id}/download','Accounts\UserController@downloadInvoicePDF');
        Route::post('user/support','Accounts\UserController@sendSupportEmail');
        Route::post('user/contactus','Accounts\UserController@sendContactUsEmail');
        Route::post('user/child/add','Accounts\UserController@addChildManually');
        Route::post('user/type','Accounts\UserController@getUserType');
        Route::get('user/convert/family','Accounts\UserController@convertToFamily');
        Route::post('user/reset/password','Accounts\UserController@resetPassword');
        Route::post('user/confirmreset/password','Accounts\UserController@confirmResetPassword');
        Route::resource('user', 'Accounts\UserController');
        Route::resource('types', 'Accounts\AccountTypeController');
        Route::get('journeys/summery', 'Journeys\JourneysController@getJourneysSummery');
        Route::resource('journeys', 'Journeys\JourneysController');
        Route::resource('journeycategory', 'Journeys\JourneyCategoryController');

        // login & register for mobile app
        Route::post('robogarden/social', 'Accounts\UserController@loginRoboGardenFacebook');

        Route::post('facebook', 'Accounts\UserController@loginFacebook');
        Route::post('twitter', 'Accounts\UserController@loginTwitter');
        Route::post('google', 'Accounts\UserController@loginGoogle');
        Route::post('user/email', 'Accounts\UserController@updateUserEmail');
        Route::get('user/children/count', 'Accounts\UserController@getChildrenCount');

        Route::get('invitation/validate/{code}','Accounts\InvitationController@validateCode');
        Route::post('invitation/register','Accounts\InvitationController@register');
        Route::resource('invitation','Accounts\InvitationController');
        Route::post('mission/last/update','Journeys\MissionController@updateLastMission');
        Route::post('mission/next', 'Journeys\MissionController@getNextMission');
        Route::post('task/activity/next', 'Journeys\MissionController@getNextTaskActivity');
        Route::post('missionhtml/next','Journeys\MissionController@getNextMissionHtml');
        Route::post('quiz/next', 'Journeys\MissionController@getNextQuiz');
        Route::get('user/robopal/authenticate','Accounts\UserController@authenticateRobopal');
        Route::post('user/rbf','Accounts\UserController@getRbf');
        Route::post('user/extra','Accounts\UserController@addExtraPlan');
        Route::post('user/extra/delete','Accounts\UserController@removeExtraPlan');
        Route::post('user/card','Accounts\UserController@updateUserCard');
        Route::get('languages','Accounts\UserController@getLanguages');
        Route::post('user/languages','Accounts\UserController@updateUserLanguages');

        Route::get('user/check/stripesubscription','Accounts\UserController@checkStripeSubscription');

        Route::post('user/feedbackus','Accounts\GuestController@index');

        Route::post('mission/camp/activity/progress/update', 'Journeys\MissionController@updateActivityCampMissionProgress');
        Route::post('mission/camp/activity/html/progress/update', 'Journeys\MissionController@updateActivityCampHTMLMissionProgress');
        Route::post('quiz/submit/camp/activity/{id}/{camp_id}/{activity_id}', 'Journeys\QuizController@submitActivityCampQuiz');
        Route::post('missionMcq/{id}/submit/camp/activity/{camp_id}/{activity_id}', 'Journeys\MissionController@submitActivityCampMcqMission');
        Route::post('quiz/camp/activity/authorize', 'Journeys\QuizController@checkQuizAuthorizationActivityCamp');
        Route::post('mission/camp/activity/authorize', 'Journeys\MissionController@checkMissionAuthorizationActivityCamp');
        Route::post('task/camp/activity/next', 'Journeys\MissionController@getNextTaskActivityCamp');
        Route::get('mission/camp/activity/{id}/data', 'Journeys\MissionController@getMissionActivityCampData');

        Route::post('taskhtml/camp/activity/next', 'Journeys\MissionController@getNextTaskHtmlActivityCamp');
        Route::get('missionhtml/camp/activity/{id}/data', 'Journeys\MissionController@getMissionHtmlActivityCampData');



        // user remove subscription
        Route::get('user/subscription/{action}/{user_id}', 'Accounts\UserController@UserSubscription');
        // homeschooling
        Route::group(['prefix' => 'homeschooling'], function () {
            Route::get('{child_id}', 'Accounts\FamilyController@getChildJourneyAndActivities');
            Route::get('progress/{journey_id}/{child_id}', 'Accounts\FamilyController@getChildProgress');
            Route::get('progress/activity/{activity_id}/{child_id}', 'Accounts\FamilyController@getChildProgressActivity');
            Route::get('statistics/{journey_id}/{child_id}', 'Accounts\FamilyController@getChildStatistics');
            Route::get('statistics/activity/{activity_id}/{child_id}', 'Accounts\FamilyController@getChildStatisticsActivity');
            Route::get('feeds/{journey_id}/{child_id}', 'Accounts\FamilyController@getChildFeeds');
            Route::get('feeds/activity/{activity_id}/{child_id}', 'Accounts\FamilyController@getChildFeedsActivity');
        });

        // ============================================================================


        //========================= School Routes =====================================
        Route::group(['prefix' => 'school'], function () {
            Route::get('planbundles','Schools\AdminController@getSchoolPlansBundle');
            Route::get('schoolprices','Schools\AdminController@getSchoolPrices');
            Route::post('subscribe/stripe','Schools\AdminController@subscribeCustom');
            Route::post('upgrade/stripe','Schools\AdminController@upgradeCustom');
            Route::post('upgrade/build/stripe','Schools\AdminController@buildUpgradeCustomReceipt');
            Route::post('subscribe/distributor','Schools\AdminController@subscribeDistributor');
            Route::post('upgrade/distributor','Schools\AdminController@upgradeDistributorSubscription');
            Route::post('upgrade/distributor/activate','Schools\AdminController@activateDistributorUpgrade');
            Route::post('invitation','Schools\AdminController@registerInvitedSchool');
            Route::get('countries','Schools\AdminController@getAllCountries');
            Route::get('plans','Schools\AdminController@getSchoolPlans');
            Route::get('distributors','Schools\AdminController@getAllDistributors');
            Route::get('distributors/{country_code}','Schools\AdminController@getDistributorsByCountryCode');
            Route::get('color_themes','Professional\ColorPaletteController@getAllColorPalettes');
            Route::post('update_logo','Accounts\UserController@updateSchoolLogo');
            Route::group(['prefix'=>'dummy'],function (){
                Route::get('addteachers','Schools\DummyController@addTeachers');
            });
            Route::group(['prefix' => 'admin'], function () {
                Route::get('classrooms','Schools\AdminController@getClassroomsForAdmin');
                Route::post('activate','Schools\AdminController@activateSchoolAccount');
                Route::get('checkstatus','Schools\AdminController@checkSchoolStatus');

//	    	Route::get('activate/{code}','Schools\AdminController@activateSchoolAccount');
//          Route::post('activate','Schools\AdminController@activateSchoolAccount2');
                Route::post('subscription/renew','Schools\AdminController@renewSubscription');
                Route::post('subscription/upgrade','Schools\AdminController@upgradeSubscription');
                Route::post('subscription/unsubscribe','Schools\AdminController@unsubscribe');
                Route::post('subscription/distributor/change','Schools\AdminController@changeDistributor');
                Route::post('school/update','Schools\AdminController@updateSchoolInfo');
                Route::get('school','Schools\AdminController@getSchoolInfo');
                Route::group(['prefix' => 'dashboard'], function () {
                    Route::get('/general','Schools\AdminController@generalStatisticsAdmin');
                    Route::get('/top_10','Schools\AdminController@getTopStudents');
                    Route::get('/classroom_score/{grade_id}','Schools\AdminController@getAdminClassroomStatistics');
                    Route::post('/missions','Schools\AdminController@getAdminMissionsChart');
                    Route::get('/activity','Schools\AdminController@getAdminActivityEvents');
                });
                Route::group(['prefix' => 'camp'], function () {
                    Route::group(['prefix' => 'dashboard'], function () {
                        Route::get('/general', 'Schools\CampController@generalStatisticsCamp');
                        Route::get('/top_10', 'Schools\CampController@getTopStudents');
                        Route::post('/missions', 'Schools\CampController@getCampTasksChart');
                        Route::get('/activity', 'Schools\CampController@getCampActivityEvents');
                    });
                });
            });
            Route::resource('admin', 'Schools\AdminController');
            Route::get('journeys/invited', 'Schools\AdminController@getInvitedJourneys');

            Route::group(['prefix' => 'teacher'], function () {
                Route::post('csv','Schools\TeacherController@addTeachersCSV');
                Route::post('delete','Schools\TeacherController@deleteTeachers');
                Route::get('count','Schools\TeacherController@count');
                Route::get('classrooms','Schools\TeacherController@getTeacherClassrooms');
                Route::get('classrooms/count','Schools\TeacherController@countTeacherClassrooms');
                Route::get('courses','Schools\TeacherController@getTeacherCourses');
                Route::get('courses/count','Schools\TeacherController@countTeacherCourses');
                Route::get('course/studentsdata','Schools\TeacherController@getStudentsDataByCourseId');
                Route::get('classroom/studentsdata','Schools\TeacherController@getStudentsDataByClassroomId');
                Route::post('studentinfo','Schools\TeacherController@getStudentInfoById');
                Route::get('course/studentsdata/count','Schools\TeacherController@getStudentsDataByCourseIdCount');
                Route::get('classroom/studentsdata/count','Schools\TeacherController@getStudentDataByClassroomIdCount');
                Route::group(['prefix' => 'dashboard'], function () {
                    Route::get('top_10','Schools\TeacherController@getTopStudentsInClassroom');
                    Route::get('/general','Schools\TeacherController@generalStatisticsTeacher');
                    Route::get('/classroomscorerange','Schools\TeacherController@getClassroomScoreRange');
                    Route::post('/missions','Schools\TeacherController@getTeacherMissionsChart');
                    Route::get('/activity','Schools\TeacherController@getTeacherActivityEvents');
                    Route::get('student/{student_id}/task/blocking','Schools\TeacherController@getStudentBlockingTasks');
                    Route::get('student/{student_id}/task/last','Schools\TeacherController@getStudentLastPlayedTask');
                    Route::get('student/{student_id}/feeds','Schools\TeacherController@getStudentFeeds');
                });
            });
            Route::resource('teacher', 'Schools\TeacherController');

            Route::group(['prefix' => 'student'], function () {
                Route::post('csv','Schools\StudentController@addStudentsCSV');
                Route::post('delete','Schools\StudentController@deleteStudents');
                Route::get('count','Schools\StudentController@count');
                Route::post('progress/mission','Schools\StudentController@updateMissionProgress');
                Route::post('progress/mission/html','Schools\StudentController@updateHTMLMissionProgress');
                Route::post('progress/quiz','Schools\StudentController@updateQuizProgress');
                Route::post('progress/mission/mcq','Schools\StudentController@updateMCQMissionProgress');
                Route::get('score','Schools\StudentController@getTotalScore');
                Route::get('courses','Schools\StudentController@getCourses');
                Route::get('adventures','Schools\StudentController@getAdventures');
                Route::get('courses/{course_id}/data','Schools\StudentController@getCourseData');
                Route::get('courses/{course_id}/position','Schools\StudentController@getStudentPosition');
                Route::post('courses/{course_id}/position','Schools\StudentController@updateStudentPosition');
                Route::post('task/authorize','Schools\StudentController@getTaskAuthorization');
                Route::post('studentclickmission','Schools\StudentController@storeStudentMissionClick');
                Route::post('task/next', 'Schools\StudentController@getNextTask');
                Route::post('taskhtml/next','Schools\StudentController@getNextTaskHtml');

            });
            Route::resource('student', 'Schools\StudentController');

            Route::group(['prefix' => 'classroom'], function () {
                Route::post('{classroom_id}/teacher','Schools\TeacherController@addTeachersToClassroom');
                Route::post('{classroom_id}/teacher/unassign','Schools\TeacherController@UnassignTeachersFromClassroom');
                Route::post('{classroom_id}/teacher/main/add','Schools\TeacherController@addMainRoleToTeacher');
                Route::post('{classroom_id}/teacher/main/remove','Schools\TeacherController@removeMainRoleFromTeacher');
                Route::post('{classroom_id}/student','Schools\StudentController@addStudentsToClassroom');
                Route::post('{classroom_id}/student/unassign','Schools\StudentController@UnassignStudentsFromClassroom');
                Route::get('{classroom_id}/teacherindex','Schools\ClassroomController@indexClassRoomTeachers');
                Route::get('{classroom_id}/studentindex','Schools\ClassroomController@indexClassRoomStudents');
                Route::post('{classroom_id}/journey','Schools\ClassroomController@assignJourneysToClassroom');
                Route::post('{classroom_id}/journey/unassign','Schools\ClassroomController@unassignJourneysFromClassroom');
                Route::get('{classroom_id}/unassignedteacher','Schools\ClassroomController@getUnAssignedTeachers');
                Route::get('{classroom_id}/unenrolledstudent','Schools\ClassroomController@getUnEnrolledStudents');
                Route::post('{classroom_id}/classroomassignment','Schools\ClassroomController@setAssignedAndUnassignedTeachersInClassroom');
                Route::post('{classroom_id}/classroomenrolment','Schools\ClassroomController@setAssignedAndUnassignedStudentsInClassroom');
                Route::get('{classroom_id}/classroomenrolment','Schools\ClassroomController@getAssignedAndUnassignedStudentsInClassroom');
                Route::get('{classroom_id}/classroomassignment','Schools\ClassroomController@getAssignedAndUnassignedTeachersInClassroom');
                Route::get('count','Schools\ClassroomController@count');
                Route::post('delete','Schools\ClassroomController@deleteClassrooms');
            });

            Route::resource('classroom', 'Schools\ClassroomController');

            Route::get('camps/inactive', 'Schools\CampController@getAllInActiveCamps');
            Route::get('camps/active', 'Schools\CampController@getAllActiveCamps');

            Route::group(['prefix' => 'camp'], function () {
                Route::get('status', 'Schools\CampController@getCampsStatus');
                Route::get('running', 'Schools\CampController@getRunningCamps');
                Route::get('count', 'Schools\CampController@countCamps');
                Route::get('{camp_id}/students', 'Schools\CampController@getStudentsInCamp');
                Route::get('{camp_id}/teachers', 'Schools\CampController@getTeachersInCamp');
                Route::get('student', 'Schools\CampController@getAllStudentsInCamps');
                Route::get('student/count', 'Schools\CampController@getCountStudentsInCamps');
                Route::get('teacher', 'Schools\CampController@getAllTeachersInCamps');
                Route::get('teacher/count', 'Schools\CampController@getCountteachersInCamps');
                Route::post('addteacher', 'Schools\CampController@addTeacher');
                Route::post('addstudent', 'Schools\CampController@addStudent');
                Route::post('{id}/campenrolment', 'Schools\CampController@setAssignedAndUnassignedStudentsInCamp');
                Route::post('{id}/campassignment', 'Schools\CampController@setAssignedAndUnassignedTeachersInCamp');
                Route::post('delete', 'Schools\CampController@deleteCamps');
                Route::get('activity/{camp_id}/{activity_id}', 'Schools\CampController@getActivityInCamp');
                Route::get('activity/{camp_id}/{activity_id}/tasks','Schools\CampController@getActivityTasksInCamp');
                Route::get('activate/{camp_id}', 'Schools\CampController@activateCamp');

                Route::group(['prefix' => 'teacher'], function () {
                    Route::group(['prefix' => 'dashboard'], function () {
                        Route::get('/general', 'Schools\TeacherController@generalTeacherStatisticsInCamp');
                        Route::get('/top_10', 'Schools\TeacherController@getTopStudentsInCamp');
                        Route::post('/missions', 'Schools\TeacherController@getCampTasksChart');
                        Route::get('/active', 'Schools\TeacherController@getAllActiveTeacherCamps');
                        Route::get('/activity', 'Schools\TeacherController@getRecentActivitiesInCamp');
                    });
                    Route::group(['prefix' => 'activity'], function () {
                        Route::get('/studentsdata', 'Schools\TeacherController@getActivityStudentsInTeacherCamp');
                        Route::get('/studentsdata/count', 'Schools\TeacherController@countActivityStudentsInTeacherCamp');
                        Route::get('/student/tasks', 'Schools\TeacherController@getStudentTasksInActivity');
                        Route::get('/{id}/statistics', 'Schools\TeacherController@getActivityCampStatistics');
                    });

                    Route::get('/running', 'Schools\TeacherController@getRunningTeacherCamps');
                    Route::get('/getcamps', 'Schools\TeacherController@getAllTeacherCamps');
                    Route::get('/countcamps', 'Schools\TeacherController@getAllTeacherCampsCount');
                    Route::get('/activities', 'Schools\TeacherController@getAllActivitiesInTeacherCamp');
                    Route::get('/activities/count', 'Schools\TeacherController@countActivitiesInTeacherCamp');
                    Route::get('/studentsdata', 'Schools\TeacherController@getAllStudentsInTeacherCamp');
                    Route::get('/studentsdata/count', 'Schools\TeacherController@CountStudentsInTeacherCamp');
                    Route::get('/studentinfo', 'Schools\TeacherController@getStudentInfoInCamp');
                    Route::get('student/{student_id}/feeds','Schools\TeacherController@getStudentFeedsInCamp');
                });

                Route::group(['prefix' => 'student'], function () {
                    Route::post('/delete', 'Schools\StudentController@deleteStudentsCamp');
                    Route::get('/camps', 'Schools\StudentController@getStudentCamps');
                    Route::get('/activities', 'Schools\StudentController@getStudentActivities');
                    Route::get('/activities/count', 'Schools\StudentController@getStudentActivitiesCount');
                });

            });

            Route::resource('camp', 'Schools\CampController');

            Route::resource('activities', 'Journeys\ActivityController');

            Route::group(['prefix'=>'course'],function(){
                Route::get('{course_id}/statistics','Schools\CourseController@getCourseStatistics');
            });

            Route::resource('group', 'Schools\GroupController');

            Route::group(['prefix'=>'Group'],function(){

            });

            Route::resource('course','Schools\CourseController');
            Route::resource('grade','Schools\GradeController');

            Route::get('schoolinvoices', 'Schools\AdminController@getSchoolInvoices');

            Route::get('subscriptiondetails', 'Schools\AdminController@getSubscriptionDetails');
            Route::get('restrictedplans', 'Schools\AdminController@getAllPlanNames');
            Route::get('endedsubscription', 'Schools\AdminController@getLastEndedSubscription');


        });
        //================================================================================

        //========================= free journeys routes ========================

        Route::group(['prefix' => 'free/journeys'], function () {
            Route::get('missions','Journeys\MissionController@getFreeMissions');
            Route::get('missions/{id}','Journeys\MissionController@getFreeMissionById');
            Route::post('progress/encrypt','Journeys\MissionController@encryptProgress');
            Route::post('progress/decrypt','Journeys\MissionController@decryptProgress');
            Route::post('certificate/send','Journeys\MissionController@sendCertificateEmail');
            Route::post('certificate/upload','Journeys\MissionController@uploadCertificate');
        });

        // ======================== landing page routes =========================
        Route::get('worlds','Controller@getAllWorlds');
        Route::get('worlds/{id}','Controller@getWorldById');
        Route::get('journeys/details/{id}','Controller@getJourneyDetailsById');
        Route::get('adventure/details/{adventure_id}','Controller@getAdventureDetailsById');
        Route::get('mission/details/{task_id}','Controller@getMissionDetailsByTaskId');
        Route::post('rfq','Controller@storeRFQ');
        Route::get('all_activities', 'LandingPageController@getAllActivities');
        Route::get('activity/mission/{id}', 'LandingPageController@getTaskById');
        Route::get('activity/{name}', 'LandingPageController@showActivity');
        //====================================================================

        // Route::get('/sendmail', function () {//tested
        //     //return view('radio');

        //    # First, instantiate the SDK with your API credentials and define your domain.
        // 	$mg = new Mailgun("key-dcc30ccea7e2af04344a5edf72d330e9");
        // 	$mg->setSslEnabled(false);
        // 	$domain = "dev.robogarden.ca";

        // 	# Now, compose and send your message.
        // 	$mg->sendMessage($domain, array('from'    => 'hme@dev.robogarden.ca.com',
        // 	                                'to'      => 'hussein.mabrouk93@gmail.com',
        // 	                                'subject' => 'Mailgun Test #1',
        // 	                                'text'    => 'It is so Mailgun Test #1.'));
        // });

        Route::get('/user', function (Request $request) {
            return $request->user();
        })->middleware('auth:api');

        Route::post('user/unlockmodelanswer','Accounts\UserController@unlockModelAnswer');
        Route::get('blocks/definitions','BlocklyController@getBlocksDefinitions');
        //Route::resource('cart', 'CartController');
        Route::resource('student', 'StudentController');
        Route::get('classroom/grades', 'ClassroomController@getGrades');
        Route::resource('classroom', 'ClassroomController');
        Route::resource('grade', 'GradeController');
        //Route::post('customer/add', 'CustomerController@add');
        Route::get('journeycategory/{id}/journeys', 'JourneyCategoryController@getCategoryJournies');
        Route::get('userjourneys/{userid}','UserJourneysController@getUserJourneys');
        Route::put('mission/{mission_id}/code','MissionController@updateSavedCode');
        Route::put('mission/{mission_id}/progress','MissionController@updateMissionProgress');
        Route::delete('mission/{mission_id}/code/{name}','MissionController@deleteSavedCode');


        // ======================== Professional routes =========================
        //Route::get('/round/submission_notifications', 'Professional\TeacherController@getTeacherSubmissionsNotifications');
        Route::group(['prefix' =>'d2l'], function()
        {
            Route::post('/launch', 'Professional\LTIController@routeLtiCourse2');
        });
        Route::group(['prefix' => 'professional'], function () {

            Route::get('product/assets', 'Professional\ProductController@getProductAssets');
           // search module
            Route::get('searchclasses/count', 'Professional\CampusController@searchForCountClasses');
            Route::get('searchcampus/count', 'Professional\CampusController@searchForCountCampuses');
            Route::get('/search', 'Professional\CampusController@getProfessionalSearch');
            Route::get('/searchLessons', 'Professional\CampusController@getlessonsSearch');
            Route::get('/searchtasks', 'Professional\CampusController@searchForTasks');
            Route::get('/searchtasks/count', 'Professional\CampusController@searchForCountTasks');
            Route::get('/search/all', 'Professional\CampusController@searchForAll');

            Route::get('/user/notifications', 'Accounts\UserController@getUserNotifications');
            Route::post('/room/webhook', 'Professional\TeacherController@roomWebHook');
            Route::get('/user/notifications/{id}/delete', 'Accounts\UserController@deleteUserNotification');
            Route::post('/user/notifications/{id}/read', 'Accounts\UserController@markNotificationAsRead');
            Route::get('/html/blockly/categories', 'Professional\MissionController@getHtmlBlocklyCategories');
            Route::post('/solve/task/notification', 'Professional\TeacherController@sendSolveTaskNotifications');
            Route::post('/practice/mission/data', 'Professional\MissionController@getPracticeMission');

            //Assessment (roboscale)
            Route::group(['prefix' =>'assessment'], function()
            {
                Route::get('/questions/{question_id}','Professional\RoboscaleController@getQuestion');
                Route::get('/questions','Professional\RoboscaleController@getAllQuestions');
                Route::get('/answers/{answer_id}','Professional\RoboscaleController@getAnswer');
                Route::get('/answers','Professional\RoboscaleController@getAllAnswers');
                //Route::post('/submit','Professional\RoboscaleController@submit');
                Route::post('/submit/{category_id}/answers','Professional\RoboscaleController@submit');
                Route::get('/category/{category_id}/questions','Professional\RoboscaleController@getCategoryQuestions');
                Route::get('/categories','Professional\RoboscaleController@getAllCategories');
                Route::get('/submission','Professional\RoboscaleController@getUserResult');

                Route::get('/category_code/{category_code}','Professional\RoboscaleController@getCategorySubmission');

                Route::get('/category_id/{category_id}/section','Professional\RoboscaleController@getSectionSubmissions');
                Route::post('/category/{category_id}/delete','Professional\RoboscaleController@deleteCategoryPostSubmissions');
            });

            //learning Paths
            Route::group(['prefix' =>'learningpath'], function(){

                Route::get('/{id}/data','Professional\LearningPathController@getLearningPathById');
                Route::get('/all','Professional\LearningPathController@getAllLearningPath');
                Route::get('/student/learningpath','Professional\LearningPathController@getStudentLearningPath');
                Route::get('/stages','Professional\LearningPathController@getLearnPathStages');
                Route::get('/test/stages','Professional\LearningPathController@getTestLearnPathStages');
                Route::get('/stage/{learning_path_id}/{stage_number}/activities','Professional\LearningPathController@getStageActivities');
                Route::get('/activity/{learning_path_id}/{activity_id}','Professional\LearningPathController@getActivity');
                Route::get('/activity/{learning_path_id}/{activity_id}/tasks','Professional\LearningPathController@getActivityTasks');
                Route::post('/mission/progress/update', 'Professional\MissionController@updateActivityMissionProgress');
                Route::post('/mission/html/progress/update', 'Professional\MissionController@updateActivityHTMLMissionProgress');
                Route::post('/missionMcq/submit', 'Professional\MissionController@submitActivityMcqMission');
                Route::post('/quiz/submit', 'Professional\MissionController@submitActivityQuiz');
                Route::post('/mission_coding/progress/update', 'Professional\MissionController@updateActivityMissionCodingProgress');
                Route::post('/mission_editor/progress/update', 'Professional\MissionController@updateActivityMissionEditorProgress');

                Route::get('/student/learningpath/{learning_path_id}/lessons','Professional\LearningPathController@getStudentLearningPathLessons');
            });

            //mission data
            Route::group(['prefix' =>'activity'], function(){
                Route::post('/task/next/info','Professional\SubmoduleController@getNextTaskInfo');

                Route::post('/taskhtml/data','Professional\StudentController@getHtmlMissionData');
                Route::post('/taskhtml/next','Professional\StudentController@getHtmlMissionNext');
                Route::post('/mission/data','Professional\StudentController@getMissionData');
                Route::post('/task/next','Professional\StudentController@getMissionNext');
                Route::post('/mission/authorize','Professional\StudentController@checkMissionAuthorization');
                Route::post('/mission_coding/data', 'Professional\StudentController@getCodingMissionData');
                Route::post('/mission_coding/next','Professional\StudentController@getCodingMissionNext');
                Route::post('/mission_editor/data', 'Professional\StudentController@getEditorMissionData');
                Route::post('/mission_editor/next','Professional\StudentController@getEditorMissionNext');
                Route::post('/mission_angular/data', 'Professional\StudentController@getAngularMissionData');
                Route::post('/mission_angular/next', 'Professional\StudentController@getAngularMissionNext');
                Route::post('/quiz/next','Professional\StudentController@getQuizNext');
                Route::post('/class/next','Professional\StudentController@getNextClass');
                Route::post('/project/data','Professional\StudentController@getProjectData');
                Route::post('/pdf/data','Professional\StudentController@getHandoutMissionData');
                Route::post('/pdf/next','Professional\StudentController@getHandoutMissionNext');

                //HTML Missions
                Route::group(['prefix' => 'taskhtml'], function () {
                    Route::post('/drag/submit','Professional\LessonController@submitLessonDragQuestion');                    
                    Route::post('/dropdown/submit','Professional\LessonController@submitLessonDropdownQuestion');                    
                    Route::post('/mcq/submit','Professional\LessonController@submitLessonMcqQuestion');                    
                    Route::post('/match/submit','Professional\LessonController@submitLessonMatchQuestion');                    
                });

            });
            Route::group(['prefix' => 'lti'], function () {
                Route::post('/d2l','Professional\LTIController@routeD2lLti');# calgary demo
            });

            Route::group(['prefix' => 'school'], function () {
                Route::group(['prefix' => 'campus'], function(){
                    Route::get('class/{campus_id}/{activity_id}/data', 'Professional\CampusController@getCampusClassId');
                    Route::get('/{campus_id}/dictionary', 'Professional\CampusController@getCampusDictionary');
                    Route::post('/lti/course','Professional\LTIController@routeLtiCourse');
                    Route::post('/lti/course/2','Professional\LTIController@routeLtiCourse2');
                    Route::post('/d2l/lti/course','Professional\LTIController@authenticateLTIUser');
                    Route::post('/d2l/lti/synch','Professional\LTIController@ltiSynchBack');
                    Route::post('/mission/authorize','Professional\StudentController@checkCampusMissionAuthorization');
                    Route::get('/all','Professional\CampusController@getAllCampuses');
                    Route::get('support/all','Professional\CampusController@getSupportCampuses');
                    Route::get('/class/all','Professional\CampusController@getAllClasses');
                    Route::get('/task/all','Professional\CampusController@getAllTasks');
                    Route::get('/{campus_id}/classes','Professional\CampusController@getCampusClasses');
                    Route::get('rounds/calendar','Professional\AdminController@getCalenderData');
                    Route::post('rounds/calendars','Professional\AdminController@updateCalenderData');
                    Route::post('rounds/module/calendar','Professional\AdminController@updateCalendarDataInModule');
                    Route::post('user/support','Professional\CampusController@sendProfessionalSupportEmail');
                    Route::get('/classes/{campus_id}', 'Professional\CampusController@getCampusClassesData');//for admin and teacher used for support
                    Route::get('/class/{class_id}/tasks', 'Professional\CampusController@getCampusClassTasks');//for admin and teacher used for support
                    Route::post('round/{round_id}/edit' ,  'Professional\AdminController@editCampusRound');
                    Route::post('round/class/tasks/update', 'Professional\CampusController@updateRoundClassTasks');
                    Route::get('round/{round_id}/{class_id}/tasks', 'Professional\CampusController@getRoundClassTasks');
                    Route::post('/class/materials', 'Professional\CampusController@getMaterialsClass');

                    Route::post('/round/class/room', 'Professional\CampusController@createRoom');
                    Route::post('/round/class/room/update', 'Professional\CampusController@updateRoom');

                    Route::get('round/{round_id}/info', 'Professional\CampusController@getCampusInfo');
                    Route::get('round/{round_id}/objectives', 'Professional\CampusController@getCampusObjectives');
                    Route::get('round/{round_id}/module/{module_id}/submodule', 'Professional\CampusController@getModuleSubModules');
                    Route::get('round/{round_id}/module/{module_id}/submodule/{sub_module_id}/task', 'Professional\CampusController@getSubModuleTasks');

                    Route::get('/round/{round_id}/activity/{activity_id}/tasks', 'Professional\CampusController@getClassTasks');

                    Route::get('/round/{round_id}/welcome/{activity_id}', 'Professional\CampusController@getWelcomeTasks');
                    Route::post('/{campus_id}/submodule/create', 'Professional\CampusController@createSubModules');

                    Route::group(['prefix' => 'student'], function () {
                        
                        Route::post('/round/{round_id}/module/{module_id}/submodule/{sub_module_id}/mission/give_up', 'Professional\CampusController@studentGiveUpMission');
                        Route::post('/round/{round_id}/module/{module_id}/mission/give_up', 'Professional\CampusController@studentGiveUpMissionGeneral');
                        Route::post('/round/{round_id}/unlock', 'Professional\CampusController@unlockStudentRound');
                        Route::post('/day/tip', 'Professional\CampusController@updateDayTipStatus');
                        Route::post('/loginTime', 'Professional\CampusController@setFirstLoginTime');
                        Route::get('/streaks','Professional\CampusController@getStudentStreaks');
                        Route::get('/round/{round_id}/current_step', 'Professional\CampusController@updateStudentCurrentStep');
                        Route::get('/round/{round_id}/current_section', 'Professional\CampusController@updateStudentCurrentSection');
                        Route::get('/rounds', 'Professional\CampusController@getStudentRunningRounds');
                        Route::get('/round/{round_id}/class', 'Professional\CampusController@getStudentRoundClassesData');
                        Route::get('/round/{round_id}/score', 'Professional\CampusController@getStudentCampusStatusData');
                        Route::get('/round/{round_id}/score/statistics', 'Professional\CampusController@getStudentCampusScoreStatistics');
                        Route::get('/score', 'Professional\CampusController@getAllStudentCampusStatusData');
                        Route::get('/round/{round_id}/class/{class_id}/tasks', 'Professional\CampusController@getStudentClassTasks');
                        Route::post('/round/classes', 'Professional\CampusController@getStudentRoundClasses');
                        Route::get('/round/{round_id}/progress', 'Professional\CampusController@getStudentProgress');
                        Route::post('/round/{round_id}/last-seen', 'Professional\CampusController@updateStudentLastSeen');


                        Route::group(['prefix' => 'round'], function () {

                            Route::post('/pdf/progress/update', 'Professional\CampusController@updateRoundActivityMissionHandoutProgress');
                            Route::post('/mission/progress/update', 'Professional\CampusController@updateRoundActivityMissionProgress');
                            Route::post('/mission/html/progress/update', 'Professional\CampusController@updateRoundActivityHTMLMissionProgress');
                            Route::post('/mini-project/progress/update', 'Professional\CampusController@updateRoundActivityMiniProjectProgress');
                            Route::post('/missionMcq/submit', 'Professional\CampusController@submitRoundActivityMcqMission');
                            Route::post('/quiz/submit', 'Professional\CampusController@submitRoundActivityQuiz');
                            Route::post('/mini-project-questionnaire/submit', 'Professional\CampusController@submitRoundActivityMiniProjectQuiz');
                            Route::post('/mission_coding/progress/update', 'Professional\CampusController@updateRoundActivityMissionCodingProgress');
                            Route::post('/mission_editor/progress/update', 'Professional\CampusController@updateRoundActivityMissionEditorProgress');
                            Route::post('/mission_angular/progress/update', 'Professional\CampusController@updateRoundActivityMissionAngularProgress');
                        });
                    });

                    Route::group(['prefix' => 'teacher'], function () {
                        Route::get('/round/submission_notifications', 'Professional\TeacherController@getTeacherSubmissionsNotifications');
                        Route::get('/round/{round_id}', 'Professional\TeacherController@getCampusRound');
                        Route::get('/', 'Professional\TeacherController@getCampuses');
                        Route::post('/round', 'Professional\TeacherController@updateCampusRound');
                        Route::get('/round/{round_id}/students', 'Professional\TeacherController@getStudentsInRound');
                        Route::get('/round/{round_id}/classes', 'Professional\TeacherController@getRoundClasses');
                        Route::post('/round/class', 'Professional\TeacherController@getRoundClass');
                        Route::post('/round/class/submissions', 'Professional\TeacherController@getRoundClassSubmissions');
                        Route::get('/round/class/submissions/count', 'Professional\TeacherController@getRoundClassSubmissionsCount');

                        Route::post('/student/submission', 'Professional\TeacherController@getStudentSubmission');
                        Route::post('/student/evaluate', 'Professional\TeacherController@updateStudentSubmissionEvaluation');

                        Route::get('/rounds', 'Professional\TeacherController@getTeacherRoundsByStatus');
                        Route::post('/round/classes', 'Professional\TeacherController@updateRoundClass');
                        Route::get('/round/submission_notifications', 'Professional\TeacherController@getTeacherSubmissionsNotifications');
                        Route::get('/round/{round_id}/class/{class_id}/sessions', 'Professional\TeacherController@getRoundClassRooms');

                        Route::get('/class/{class_id}/activity', 'Professional\TeacherController@getClassActivity');
                        Route::get('/class/{class_id}/tasks', 'Professional\TeacherController@getClassTasks');
                        Route::post('/class/material/upload', 'Professional\TeacherController@uploadMaterialToClass');
                        Route::post('/class/material/delete', 'Professional\TeacherController@deleteMaterialFromClass');


                        Route::group(['prefix' => 'dashboard'], function () {
                            Route::get('/general', 'Professional\TeacherController@generalTeacherStatisticsInCampuses');
                            Route::get('/top_10', 'Professional\TeacherController@getTopStudentsInCampusRound');
                            Route::post('/chart', 'Professional\TeacherController@getTeacherSubmissionsChart');
                            Route::get('/recentActivity', 'Professional\TeacherController@getTeacherRecentActivity');
                        });
                    });

                    Route::group(['prefix' => 'admin'], function () {
                        Route::get('/{campus_id}/rounds', 'Professional\AdminController@getCampusRounds'); // not used
                        Route::post('/classes', 'Professional\AdminController@getClassTasksAndRooms');
                        Route::get('/class/{class_id}/tasks', 'Professional\AdminController@getClassTasksAndRooms');//USED
                        Route::post('/student/enroll', 'Professional\AdminController@setAssignedAndUnassignedStudentsInCampusRound');
                        Route::post('/class/teacher/enroll', 'Professional\AdminController@setAssignedAndUnassignedTeachersInCampusRound');
                        Route::post('/round/add', 'Professional\AdminController@addCampusRound');
                        Route::get('/round/data/{round_id}', 'Professional\AdminController@getRoundDataWithoutHtml');

                        Route::get('/round/{round_id}', 'Professional\AdminController@getRoundData');

                        Route::post('/round', 'Professional\AdminController@updateCampusRound');
                        Route::get('/rounds', 'Professional\AdminController@getAdminRoundsByStatus');

                        Route::get('/round/{round_id}/students/enroll', 'Professional\AdminController@getStudentsInRoundToEnroll');
                        Route::get('/round/{round_id}/teachers/enroll', 'Professional\AdminController@getTeachersInRoundToEnroll');

                        Route::post('/delete/round', 'Professional\AdminController@deleteCampusRound');

                        Route::get('/{campus_id}/students', 'Professional\AdminController@getCampusAdminStudents');
                        Route::get('/{campus_id}/students/count', 'Professional\AdminController@getCampusAdminStudentsCount');

                        Route::group(['prefix' => 'dashboard'], function () {
                            Route::get('/general', 'Professional\AdminController@generalAdminStatisticsInCampuses');
                            Route::post('/chart', 'Professional\AdminController@getAdminSubmissionsChart');
                            Route::get('/recentActivity', 'Professional\AdminController@getAdminRecentActivity');
                        });
                    });
                    Route::get('/{campus_id}/details', 'Professional\CampusController@getCourseDetails');
                    Route::get('/{campus_id}/details/count', 'Professional\CampusController@getCourseDetailsCount');

                    Route::get('/{campus_id}/module/{module_id}/learners', 'Professional\CampusController@getCourseLearners');
                    Route::get('/{campus_id}/module/{module_id}/learners/count', 'Professional\CampusController@getCourseLearnersCount');
                    
                    Route::post('/hideblocks/update', 'Professional\CampusController@updateHideBlocks');
                    Route::post('/hideblocks/bulk_update', 'Professional\CampusController@bulkUpdateHideBlocks');

                    Route::get('/{campus_id}/module/{module_id}/details', 'Professional\CampusController@getCourseModuleDetails');
                    Route::get('/{campus_id}/module/{module_id}/details/count', 'Professional\CampusController@getCourseModuleDetailsCount');

                    Route::get('/{campus_id}/module/{module_id}/task/{task_id}/learners', 'Professional\CampusController@getModuleMissionLearners');
                    Route::get('/{campus_id}/module/{module_id}/task/{task_id}/learners/count', 'Professional\CampusController@getModuleMissionLearnersCount');

                    Route::group(['prefix' => 'round'], function () {
                        Route::post('/{round_id}/video','Professional\CampusController@saveWatchedVideoFlag');
                        Route::get('/{round_id}/introductory_video','Professional\CampusController@getIntroDetails');
                       
                        //Admin 3.8
                        Route::get('/{round_id}/student/{student_id}/progress', 'Professional\CampusController@getLearnerProgress');
                        Route::get('/{round_id}/learners/progress', 'Professional\CampusController@getRoundLearnersProgress');
                        Route::get('/{round_id}/learners/progress/count', 'Professional\CampusController@getRoundLearnersProgressCount');
                    });

                });
                Route::group(['prefix' => 'admin'], function () {
                    Route::post('/teacher/add', 'Professional\AdminController@addTeacher');
                    Route::post('/student/add', 'Professional\AdminController@addStudent');
                    Route::post('/student/csv', 'Professional\AdminController@addStudentsCSV');

                    Route::post('/student/{student_id}', 'Professional\AdminController@updateCampusStudent');
                    Route::post('/teacher/{teacher_id}', 'Professional\AdminController@updateCampusTeacher');
                    Route::get('/students', 'Professional\AdminController@getAdminStudents');
                    Route::get('/teachers', 'Professional\AdminController@getAdminTeachers');

                    Route::get('/students/count', 'Professional\AdminController@getAdminStudentsCount');
                    Route::get('/teachers/count', 'Professional\AdminController@getAdminTeachersCount');

                    Route::post('/students/delete', 'Professional\AdminController@deleteStudents');
                    Route::post('/teachers/delete', 'Professional\AdminController@deleteTeachers');

                });

            });

            //BBB
            Route::group(['prefix' =>'bbb'], function(){
                Route::post('/test', 'Professional\BBBController@testBBB');
                Route::post('/rooms', 'Professional\BBBController@getRooms');
                Route::post('/room/create','Professional\BBBController@create');
                Route::get('/room/{room_id}/join','Professional\BBBController@join');
                Route::post('/room/{room_id}/start','Professional\BBBController@start');
                Route::get('/room/{room_id}/end','Professional\BBBController@end');
                Route::get('/room/{room_id}/records','Professional\BBBController@getRecords');
                Route::get('/session/{session_id}/joined','Professional\BBBController@joined');
            });


            //Push Notifications
            Route::group(['prefix' =>'push'], function(){
                Route::post('/update', 'Professional\PushNotificationSubController@setPushSubscription');
                Route::put('/update', 'Professional\PushNotificationSubController@updatePushSubscription');
                Route::delete('/update', 'Professional\PushNotificationSubController@deletePushSubscription');
                Route::post('/test', 'Professional\PushNotificationSubController@test');
                Route::post('/send', 'Professional\PushNotificationSubController@send');
            });
        });

        // Help Center

        Route::group(['prefix' => 'help', 'namespace' => 'HelpCenter'], function (){
            Route::group(['prefix' => "topics"], function (){
                Route::get('/', 'HelpCenterController@getAllTopics')->name('help.getAllTopics');
                Route::get('/count', 'HelpCenterController@getAllTopicsCount')->name('help.getAllTopicsCount');
                Route::get('/details', 'HelpCenterController@getAllTopicsDetails')->name('help.getAllTopicsWithDetails');

                Route::get('/{id}', 'HelpCenterController@getTopic')->name('help.getTopic');
                Route::get('/{id}/count', 'HelpCenterController@getTopicArticlesCount')->name('help.getTopicArticlesCount');
            });

            Route::group(['prefix' => "articles"], function (){
                Route::get('/', 'HelpCenterController@searchForArticle')->name('help.searchForArticle');
                Route::get('/count', 'HelpCenterController@searchForCountArticle')->name('help.searchForCountArticle');
                Route::get('/{id}', 'HelpCenterController@getArticle')->name('help.getArticle');     
                Route::get('/topic/blogs', 'HelpCenterController@getTopicArticle')->name('help.getArticleOfTopic');
            });

            Route::group(['prefix' => "tags"], function (){
                Route::get('/', 'HelpCenterController@getAllTags')->name('help.getAllTags');
                Route::get('/{id}', 'HelpCenterController@getTag')->name('help.getTag');
                
                 Route::get('/{id}/count', 'HelpCenterController@getTagArticlesCount')->name('help.getTagArticlesCount');
            });

            Route::group(['prefix' => 'admin'], function (){
                Route::get('topics/count', 'HelpCenterTopicController@count');
                Route::post('topic/{topic_id}/articles/rearrange', 'HelpCenterArticleController@articlesRearrange');
                Route::get('topic/{topic_id}/articles/rearrange', 'HelpCenterArticleController@articlesRearrangeList');
                Route::post('topics/rearrange', 'HelpCenterTopicController@rearrange');
                Route::get('topics/rearrange', 'HelpCenterTopicController@rearrangeList');
                Route::post('topics/upload_icon', 'HelpCenterTopicController@uploadIcon');
                Route::resource('topics', 'HelpCenterTopicController');
                Route::post('topics/{topic}', 'HelpCenterTopicController@update');
                Route::post('articles/upload_icon', 'HelpCenterArticleController@uploadIcon');
                Route::get('articles/count', 'HelpCenterArticleController@count');
                Route::resource('articles', 'HelpCenterArticleController');
                Route::post('articles/{article}', 'HelpCenterArticleController@update');
                Route::get('articles/{article}/tags', 'HelpCenterArticleController@getArticleTags');
                Route::get('tags/count', 'HelpCenterTagController@count');
                Route::resource('tags', 'HelpCenterTagController');
                Route::post('tags/{tag}', 'HelpCenterTagController@update');
            });

        });

    });



/*
 * Start Teacher Module
 */
Route::group(['prefix'=>'school/gees'],function(){
    Route::post('account','Gees\SchoolAdminController@createSchoolAccount');
    Route::post('getuserdata','Gees\TeacherController@getUserData');
    Route::post('getClassRoomsByTeacherId','Gees\ClassRoomController@getClassRoomsByTeacherId');
    Route::post('createClassRoom','Gees\SchoolAdminController@createClassRoom');
    Route::post('createStudent','Gees\SchoolAdminController@createStudent');
    Route::post('createTeacher','Gees\SchoolAdminController@createTeacher');
    Route::post('createClassroom','Gees\SchoolAdminController@createClassroom');
    Route::post('getTeachersByAdminId','Gees\SchoolAdminController@getTeachersByAdminId');
    Route::post('getStudentsByAdminId','Gees\SchoolAdminController@getStudentsByAdminId');
    Route::post('uploadStudents','Gees\SchoolAdminController@uploadStudents');
    Route::post('deleteStudent','Gees\SchoolAdminController@deleteStudent');
    Route::post('deleteClassroom','Gees\SchoolAdminController@deleteClassroom');
    Route::post('getClassRoomsByAdminId','Gees\ClassRoomController@getClassRoomsByAdminId');
    Route::post('getAllGrades','Gees\SchoolAdminController@getAllGrades');
    Route::post('getJourneysByAdminId','Gees\SchoolAdminController@getJourneysByAdminId');
    Route::post('assignTeacher','Gees\SchoolAdminController@assignTeacher');
    Route::post('enrollStudent','Gees\SchoolAdminController@enrollStudent');
    Route::get('countries','Gees\SchoolAdminController@getCountries');
    Route::get('distributors/{country_name?}','Gees\SchoolAdminController@getDistributors');
    Route::get('plans','Gees\SchoolAdminController@getSchoolPlansSpecial');
    Route::get('account/activate/{username}','Gees\SchoolAdminController@isActivatedAccount');
    Route::post('account/activate','Gees\SchoolAdminController@activateAccount');
    Route::get('classroom/{id}/missions/played','Gees\TeacherController@getPlayedMissions');
    Route::get('classroom/{id}/missions/notplayed','Gees\TeacherController@getUnplayedMissions');
    Route::get('classroom/{id}/journeys','Gees\TeacherController@getClassroomJourneys');
    Route::get('classroom/adventures/{id}/missions','Gees\TeacherController@getClassroomMissions');
    Route::post('journeyName','Gees\SchoolAdminController@getJourneyName');
    Route::post('test','Gees\TeacherController@test');
});
// Reports' Routes

Route::post('adventures','Gees\MissionsController@getAdventuresForJourneys');
Route::post('notPlayedMissions','Gees\MissionsController@getNotPlayedMissions');
Route::post('playedMissions','Gees\MissionsController@getPlayedMissionsByAllStudents');
Route::post('studentMissionsQuizzes','Gees\MissionsController@getStudentWithMissionsAndQuizzes');
Route::post('studentIds','Gees\MissionsController@getStudentIdsInClass');

/*
 * End Teacher Module
 */

// ======================== migration routes =========================
Route::get('mapjourney/{id}','Controller@mapJourney');
Route::get('mapinvitedusers','TempController@mapInvitedUsers');
Route::get('mapusers','Controller@mapUsers');
Route::get('mapusersprod','Controller@mapUsersProduction');
Route::get('mapjourneycsv/{id}','Controller@mapJourneyCSV');
//====================================================================

//========================= stripe routes ============================
Route::post('handle/stripe/test','Accounts\StripeController@handleStripeTestEvents');
Route::post('handle/stripe/live','Accounts\StripeController@handleStripeLiveEvents');
//====================================================================

Route::get('getstripetoken', function () {
    \Stripe\Stripe::setApiKey("sk_test_E9dY2oQyjRRO9VHdvRv7dLcu");

    return \Stripe\Token::create(array(
        "card" => array(
            "number" => "4242424242424242",
            "exp_month" => 4,
            "exp_year" => 2018,
            "cvc" => "314"
        )
    ))->id;




    //$customClaims = ['child_id' => 1];

    //$payload = JWTFactory::make($customClaims);

    //    $payload = JWTFactory::sub(123)->aud('child')->child(['id' => 1])->make();

    //    $child_token = JWTAuth::encode($payload);

    //    $cc = JWTAuth::decode($child_token)->toArray()['child']['id'];

    // return $cc;

    // \Stripe\Plan::create(array(
    //             "amount" => 499,
    //             "interval" => 'month',
    //             "name" => 'Blockly_monthly_child',
    //             "currency" => 'usd',
    //             "id" => 'Blockly_monthly_child')
    //           );

    // \Stripe\Plan::create(array(
    //             "amount" => 1499,
    //             "interval" => 'month',
    //             "name" => 'Blockly_Family_monthly',
    //             "currency" => 'usd',
    //             "id" => 'Blockly_Family_monthly')
    //           );

    // \Stripe\Plan::create(array(
    //             "amount" => 499,
    //             "interval" => 'month',
    //             "name" => 'Textual_monthly_child',
    //             "currency" => 'usd',
    //             "id" => 'Textual_monthly_child')
    //           );

    // \Stripe\Plan::create(array(
    //             "amount" => 1499,
    //             "interval" => 'month',
    //             "name" => 'Textual_Family_monthly',
    //             "currency" => 'usd',
    //             "id" => 'Textual_Family_monthly')
    //           );

    // \Stripe\Plan::create(array(
    //             "amount" => 599,
    //             "interval" => 'month',
    //             "name" => 'Blockly_Textual_monthly_child',
    //             "currency" => 'usd',
    //             "id" => 'Blockly_Textual_monthly_child')
    //           );

    // \Stripe\Plan::create(array(
    //             "amount" => 2999,
    //             "interval" => 'month',
    //             "name" => 'Blockly_Textual_Family_monthly',
    //             "currency" => 'usd',
    //             "id" => 'Blockly_Textual_Family_monthly')
    //           );

    // return 'done';



});

Route::get('getvplurl', function () {
    return "http://dev.robogarden.ca/dev/HD/individual/vpl/refactor/mod/vpl/";

});

Route::post('loadxlsxfile','Controller@loadXLSXFile');
Route::get('createInvitationCodes', function () {
    // for($i=0;$i<12;$i++)
    // {
    // 	$invitation_id = DB::table('invitation')->insertGetId([
    // 			'code' => 'RG_Telefonica'.sprintf("%'.02d",$i+1),
    // 			'name' => 'RG_Telefonica',
    // 			'account_type_id' => 7,
    // 			'ends_at' => '2017-09-30',
    // 			'number_of_users' => 1
    // 		]
    // 	);

    // 	DB::table('invitation_journey')->insert([
    // 			'invitation_id' => $invitation_id,
    // 			'progress_lock' => 0,
    // 			'journey_id'=> 1
    // 		]
    // 	);
    // }

    // for($i=0;$i<10;$i++)
    // {
    // 	$invitation_id = DB::table('invitation')->insertGetId([
    // 			'code' => 'RG_Santillana'.sprintf("%'.02d",$i+1),
    // 			'name' => 'RG_Santillana',
    // 			'account_type_id' => 7,
    // 			'ends_at' => '2017-09-30',
    // 			'number_of_users' => 1
    // 		]
    // 	);

    // 	DB::table('invitation_journey')->insert([
    // 			'invitation_id' => $invitation_id,
    // 			'progress_lock' => 0,
    // 			'journey_id'=> 1
    // 		]
    // 	);
    // }


    // $invitation_id = DB::table('invitation')->insertGetId([
    // 		'code' => 'RG_Hydralab',
    // 		'name' => 'RG_Hydralab',
    // 		'account_type_id' => 7,
    // 		'ends_at' => '2017-09-30',
    // 		'number_of_users' => 5
    // 	]
    // );

    // DB::table('invitation_journey')->insert([
    // 		'invitation_id' => $invitation_id,
    // 		'progress_lock' => 0,
    // 		'journey_id'=> 1
    // 	]
    // );

    // for($i=0;$i<1000;$i++)
    // {
    // 	$invitation_id = DB::table('invitation')->insertGetId([
    // 			'code' => 'RG_Dist'.sprintf("%'.03d",$i+1),
    // 			'name' => 'RG_Dist',
    // 			'account_type_id' => 7,
    // 			'ends_at' => '2017-08-30',
    // 			'number_of_users' => 2
    // 		]
    // 	);

    // 	DB::table('invitation_journey')->insert([
    // 			'invitation_id' => $invitation_id,
    // 			'progress_lock' => 0,
    // 			'journey_id'=> 1
    // 		]
    // 	);
    // }

    for($i=0;$i<100;$i++)
    {
        $invitation_id = DB::table('invitation')->insertGetId([
                'code' => 'RG_Ghana_School'.sprintf("%'.02d",$i+1),
                'name' => 'RG_Ghana_School',
                'account_type_id' => 7,
                'ends_at' => '2017-12-31',
                'number_of_users' => 1
            ]
        );

        DB::table('invitation_journey')->insert([
                'invitation_id' => $invitation_id,
                'progress_lock' => 0,
                'journey_id'=> 1
            ]
        );
    }

    return "Invitation Codes successfully created";

});

Route::get('addfreeplan', 'Controller@AddFreePlanToUsers');


Route::post('/subscribe_to_mailing_list/','Controller@subscribe_to_mailing_list');

Route::get('/unsubscribe_from_mailing_list/{mail?}','Controller@unsubscribe_from_mailing_list');

Route::get('/get_mailing_list_subscribers_mails','Controller@get_mailing_list_subscribers_mails');

//World cup subscription
Route::post('/worldcup/subscribe','Controller@subscribe_to_world_cup');
Route::get('/worldcup/unsubscribe/{mail}','Controller@unsubscribe_from_world_cup');
Route::get('/worldcup/subscribers','Controller@get_world_cup_subscribers_mails');
Route::get('/worldcup/predictall','Accounts\WorldCupController@predictWorldCupAutomatic');

//Administrative Tasks
Route::group(['prefix' => 'administrative_tasks'], function (){
    Route::post('/clean_dropdown','Professional\CampusController@cleanDropdown');
    Route::post('/progress_fix','Professional\CampusController@progressFix');
    Route::post('/lastseen_fix','Professional\CampusController@lastSeenFix');
    Route::post('/python_fix','Professional\CampusController@pythonFix');
    Route::post('/eval_fix','Professional\CampusController@evalFix');
    Route::post('/editorbug_check','Professional\CampusController@checkEditorMissionBug');
    Route::post('/searchvault_create','Professional\CampusController@createSearchVault');
    Route::post('/searchvaultcourses_create','Professional\CampusController@createSearchCoursesVault');
});